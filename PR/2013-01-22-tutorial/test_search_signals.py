from pyCDB.client import CDBClient as cdb_client

cdb = cdb_client()

# Get all generic signal references
refs = cdb.get_generic_signal_references()
# Python magic - search for signals whose name contains efit
# use lower for case insesitive search
efit_refs = filter(lambda r: 'efit' in r['generic_signal_name'].lower(), refs)
# see the results
for r in efit_refs:
    print('ID: %i, name: %s, alias: %s' % (r['generic_signal_id'], r['generic_signal_name'], r['alias']))

# search for efit results
data_refs = cdb.get_signal_references(generic_signal_id=efit_refs[0]['generic_signal_id'])
rec_nums = [r['record_number'] for r in data_refs]
# use set to get unique record numbers
print('%i unique records, %i total data signals with "%s"' % (len(set(rec_nums)), len(rec_nums), efit_refs[0]['generic_signal_name']))

