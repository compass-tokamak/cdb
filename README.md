CDB - Compass DataBase
======================

Compass DataBase (CDB) is a versatile lightweight system designed for storing experimental data,
originally designed for the COMPASS tokamak, operated by IPP ASCR Prague, Czech Republic.
It can be equally well used for any tokamak or generally any experiment that repeatedly produces data.
The core is implemented in Python, HDF5 and MySQL are used as data and metadata back-ends.
CDB is vendor and platform independent and can be easily scaled and distributed.

The data is directly stored and retrieved using a standard NAS (Network Attached
Storage), hence independent of the particular technology; the description of the
data (the metadata) is recorded in a relational database. Database structure is
general and enables the inclusion of multi-dimensional data signals in multiple
revisions (no data is overwritten).
This design is inherently distributed as the work is off-loaded to the clients.
Both NAS and database can be implemented and
optimized for fast local access as well as secure remote access.

CDB is implemented in Python language; bindings for
Java, C/C++, IDL and Matlab are provided.
Independent data acquisitions systems as well as nodes managed by FireSignal
are all integrated using CDB.

An automated data post-processing server is a part of CDB. Based on dependency rules,
the server executes, in parallel if possible, prescribed post-processing tasks.

The documentation can be found at [http://cdb.readthedocs.io](http://cdb.readthedocs.io). CDB is described in a paper "Integrated Data Acquisition, Storage, Retrieval and Processing Using the COMPASS DataBase (CDB)", available from [arXiv](http://arxiv.org/abs/1403.7928) and published in Fusion Engineering and Design, [DOI:10.1016/j.fusengdes.2014.03.032](http://dx.doi.org/10.1016/j.fusengdes.2014.03.032).

CDB is distributed under MIT licence (see [LICENSE](./LICENSE)).

CDB uses following versioning patterns:

* Release:
  `<major>.<minor>.<patch>`
  (e.g. `1.1.0`)
* Post-release built by the CI/CD pipeline:
  `<major>.<minor>.<patch>.post<build_number>`
  (e.g. `1.1.0.post12345`)
* Post-release built outside the CI/CD pipeline:
  `<major>.<minor>.<patch>+local`
  (e.g. `1.1.0+local`)


## Running automated tests

Automated tests can be executed using docker.
Use of docker is recommended because it does not require any
other dependency than docker (e.g. Docker Desktop).
The solution uses docker compose profiles which might not be present
in order versions. Please, update docker if necessary.

1) Start test environment:
   ```
   docker compose --profile test up --build -d
   ```

   * The command will build the images (if not built already)
     and start the test environment inside the container `pycdb-test-1`.
   * The container mounts source code as volume. So, the source code
     can be modified outside the container without the need to rebuild it
     or restart it.
   * The container will keep running so that the developer can execute
     tests repeatedly and evaluate outcome/temporary files if any.

2) Execute tests inside the docker container:
   ```
   docker exec -it pycdb-test-1 pytest pyCDB/tests
   ```

   * Logs of the container `pycdb-test-1` contain test output.
     By default, the logs will mirror into the console. Access it
     either through docker GUI or using
     ```
     docker logs pycdb-test-1
     ```
   * Test use a database which runs locally (i.e. codac3 is not affected).
     You can access the database through browser using URL
     [http://localhost:9000/](http://localhost:9000/).
     Use credentitals mentioned (env. var. `MYSQL_*`)
     in [docker-compose.yml](docker-compose.yml) to log in.

3) Once completed, stop the containers.
   You can use following command to remove the containers (this is optional):
   ```
   docker compose --profile test down
   ```


## Publishing a new version

This section describes publishing a versioned package to the internal
package distribution centre at the institute.

CI/CD pipeline uses GIT tags to identify a versioned build. Format of the tag is
`v<major>.<minor>.<patch>` (e.g., `v1.1.0`). The tag (if defined) has to match
a version number defined inside `setup.py`. Otherwise, the build will fail.
Thus, the approach to setup the build is following:

1) Update version number in `setup.py`. Commit and deliver to the master branch.

2) Using the web interface, create the tag corresponding to the version number
   in `setup.py`. This will start a tagged CI/CD pipeline which will publish
   the versioned package.
