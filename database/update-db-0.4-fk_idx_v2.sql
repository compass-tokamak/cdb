SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `CDB`.`channel_setup` DROP FOREIGN KEY `fk_physical_setup_generic_signals1` ;

ALTER TABLE `CDB`.`channel_setup` 
  ADD CONSTRAINT `fk_physical_setup_generic_signals1`
  FOREIGN KEY (`attached_generic_signal_id` )
  REFERENCES `CDB`.`generic_signals` (`generic_signal_id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
, ADD INDEX `fk_channel_setup_1_idx` (`computer_id` ASC, `board_id` ASC, `channel_id` ASC) 
, ADD INDEX `fk_physical_setup_generic_signals1_idx` (`attached_generic_signal_id` ASC) 
, DROP INDEX `fk_physical_setup_generic_signals1_idx` 
, DROP INDEX `fk_channel_setup_1_idx` ;

ALTER TABLE `CDB`.`data_files` 
ADD INDEX `shot_number_idx` (`record_number` ASC) 
, ADD INDEX `fk_data_files_1_idx` (`data_source_id` ASC) 
, DROP INDEX `fk_data_files_1_idx` 
, DROP INDEX `shot_number_idx` ;

ALTER TABLE `CDB`.`data_signals` 
ADD INDEX `fk_data_file_id_idx` (`data_file_id` ASC) 
, ADD INDEX `fk_generic_channel_id_idx` (`generic_signal_id` ASC) 
, ADD INDEX `fk_record_number_idx` (`record_number` ASC) 
, ADD INDEX `fk_data_signals_1_idx` (`computer_id` ASC, `board_id` ASC, `channel_id` ASC) 
, DROP INDEX `fk_data_signals_1_idx` 
, DROP INDEX `fk_record_number_idx` 
, DROP INDEX `fk_generic_channel_id_idx` 
, DROP INDEX `fk_data_file_id_idx` ;

ALTER TABLE `CDB`.`file_status` 
ADD INDEX `fk_file_status_1_idx` (`data_file_id` ASC) 
, DROP INDEX `fk_file_status_1` ;

ALTER TABLE `CDB`.`generic_signals` 
ADD INDEX `fk_generic_signals_1_idx` (`data_source_id` ASC) 
, ADD INDEX `fk_generic_signals_2_idx` (`time_axis_id` ASC) 
, ADD INDEX `fk_generic_signals_3_idx` (`axis1_id` ASC) 
, ADD INDEX `fk_generic_signals_4_idx` (`axis2_id` ASC) 
, ADD INDEX `fk_generic_signals_5_idx` (`axis3_id` ASC) 
, ADD INDEX `fk_generic_signals_6_idx` (`axis4_id` ASC) 
, ADD INDEX `fk_generic_signals_7_idx` (`axis5_id` ASC) 
, ADD INDEX `fk_generic_signals_8_idx` (`axis6_id` ASC) 
, DROP INDEX `fk_generic_signals_1` ;

ALTER TABLE `CDB`.`DAQ_channels` 
ADD INDEX `fk_detach_ids_generic_signals1_idx` (`default_generic_signal_id` ASC) 
, ADD INDEX `fk_physical_channels_1_idx` (`computer_id` ASC) 
, DROP INDEX `fk_physical_channels_1` 
, DROP INDEX `fk_detach_ids_generic_signals1` ;

ALTER TABLE `CDB`.`record_directories` 
ADD INDEX `fk_record_directories_1_idx` (`record_number` ASC) 
, DROP INDEX `fk_record_directories_1` ;

ALTER TABLE `CDB`.`FireSignal_Event_IDs` 
ADD INDEX `fk_FireSignal_Event_IDs_1_idx` (`record_number` ASC) 
, DROP INDEX `fk_FireSignal_Event_IDs_1` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
