SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE  TABLE IF NOT EXISTS `CDB`.`postproc_rules` (
  `rule_id` INT(11) NOT NULL AUTO_INCREMENT,
  `rule_name` VARCHAR(45) NOT NULL ,
  `description` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`rule_id`) ,
  UNIQUE INDEX `rule_name_UNIQUE` (`rule_name` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE  TABLE IF NOT EXISTS `CDB`.`rule_outputs` (
  `rule_id` INT(11) NOT NULL ,
  `generic_signal_id` INT(11) NOT NULL ,
  `note` VARCHAR(140) NULL DEFAULT NULL ,
  PRIMARY KEY (`rule_id`, `generic_signal_id`) ,
  INDEX `fk_rule_outputs_1_idx` (`rule_id` ASC) ,
  INDEX `fk_rule_outputs_2_idx` (`generic_signal_id` ASC) ,
  CONSTRAINT `fk_rule_outputs_1`
    FOREIGN KEY (`rule_id` )
    REFERENCES `CDB`.`postproc_rules` (`rule_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rule_outputs_2`
    FOREIGN KEY (`generic_signal_id` )
    REFERENCES `CDB`.`generic_signals` (`generic_signal_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE  TABLE IF NOT EXISTS `CDB`.`rule_inputs` (
  `rule_id` INT(11) NOT NULL ,
  `generic_signal_id` INT(11) NOT NULL ,
  `note` VARCHAR(140) NULL DEFAULT NULL ,
  PRIMARY KEY (`rule_id`, `generic_signal_id`) ,
  INDEX `fk_rule_inputs_1_idx` (`rule_id` ASC) ,
  INDEX `fk_rule_inputs_2_idx` (`generic_signal_id` ASC) ,
  CONSTRAINT `fk_rule_inputs_1`
    FOREIGN KEY (`rule_id` )
    REFERENCES `CDB`.`postproc_rules` (`rule_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rule_inputs_2`
    FOREIGN KEY (`generic_signal_id` )
    REFERENCES `CDB`.`generic_signals` (`generic_signal_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE  TABLE IF NOT EXISTS `CDB`.`postproc_log` (
  `rule_id` INT(11) NOT NULL ,
  `record_number` INT(11) NOT NULL ,
  `timestamp` DATETIME NOT NULL ,
  `input_signal_revisions` VARCHAR(250) NULL DEFAULT NULL COMMENT 'gs_id:revision, ...' ,
  `success` TINYINT(1) NOT NULL ,
  `log` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`timestamp`, `rule_id`, `record_number`) ,
  INDEX `fk_postproc_log_1_idx` (`rule_id` ASC) ,
  INDEX `fk_postproc_log_2_idx` (`record_number` ASC) ,
  CONSTRAINT `fk_postproc_log_1`
    FOREIGN KEY (`rule_id` )
    REFERENCES `CDB`.`postproc_rules` (`rule_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_postproc_log_2`
    FOREIGN KEY (`record_number` )
    REFERENCES `CDB`.`shot_database` (`record_number` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
