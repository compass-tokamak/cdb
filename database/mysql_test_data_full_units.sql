SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

USE `CDB`;

SET foreign_key_checks = 0;

TRUNCATE `channel_setup`;
TRUNCATE `DAQ_channels`;
TRUNCATE `data_files`;
TRUNCATE `data_signals`;
TRUNCATE `data_sources`;
TRUNCATE `da_computers`;
TRUNCATE `file_status`;
TRUNCATE `FireSignal_Event_IDs`;
TRUNCATE `generic_signals`;
TRUNCATE `physical_quantities`;
TRUNCATE `record_directories`;
TRUNCATE `shot_database`;
TRUNCATE `units`;

--
-- Dumping data for table `da_computers`
--

INSERT INTO `da_computers` (`computer_id`, `location`, `description`) VALUES
(1, '112', 'ATCA1'),
(2, '112', 'ATCA2');

-- --------------------------------------------------------

--
-- Dumping data for table `data_sources`
--

INSERT INTO `data_sources` (`data_source_id`, `name`, `description`, `subdirectory`) VALUES
(1, 'Interferometry', 'Interferometry (test source)', 'interferometry'),
(2, 'ATCA', NULL, 'ATCA'),
(3, 'SDAS_import', 'Imported data from SDAS database', 'SDAS_import');

-- --------------------------------------------------------

--
-- Dumping data for table `generic_signals`
--

INSERT INTO `generic_signals` (`generic_signal_id`, `generic_signal_name`, `alias`, `first_record_number`, `last_record_number`, `data_source_id`, `time_axis_id`, `axis1_id`, `axis2_id`, `axis3_id`, `axis4_id`, `axis5_id`, `axis6_id`, `physical_quantity_id`, `units_id`, `description`, `signal_type`) VALUES
(1, 'electron density', 'ne', 0, -1, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, 2, 3, 'electron density (test signal)', 'FILE'),
(2, 'interferometry time signal', 't_ne', 0, -1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 5, 'test time signal', 'LINEAR'),
(3, 'test linear signal', NULL, 0, -1, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 'LINEAR'),
(4, 'PCIE_ATCA_ADC_01.BOARD_1.CHANNEL_001', NULL, 0, -1, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 'FILE'),
(5, 'PCIE_ATCA_ADC_01.BOARD_1.CHANNEL_002', NULL, 0, -1, 3, 830, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 'FILE');

--
-- Dumping data for table `DAQ_channels`
--

INSERT INTO `DAQ_channels` (`computer_id`, `board_id`, `channel_id`, `default_generic_signal_id`, `note`, `nodeuniqueid`, `hardwareuniqueid`, `parameteruniqueid`) VALUES
(1, 1, 1, 4, '', 'ATCA_01', 'BOARD_01', 'CHANNEL_01'),
(1, 1, 2, 5, '', NULL, NULL, NULL);
-- --------------------------------------------------------

--
-- Dumping data for table `channel_setup`
--

INSERT INTO `channel_setup` (`attached_generic_signal_id`, `attach_time`, `computer_id`, `board_id`, `channel_id`, `uid`, `note`) VALUES
(4, '2011-08-09 18:05:12', 1, 1, 1, NULL, NULL),
(5, '2011-08-09 18:05:12', 1, 1, 2, NULL, NULL);

--
-- Dumping data for table `physical_quantities`
--

INSERT INTO `physical_quantities` (`id`, `name`, `description`, `units_SI`, `units_CGS`) VALUES
(1, 'undefined', 'undefined', 1, 1),
(2, 'density', 'particle density', 3, 4),
(3, 'time', 'time', 5, 5);

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `plain`, `latex`, `coef_SI`) VALUES
(1, 'arbitrary', 'a.u.', 'a.u.', 1),
(2, '10^19 m-3', '10^19 m-3', '10^{19}m^{-3}', 1e19),
(3, 'm-3', 'm-3', 'm^{-3}', 1),
(4, 'cm-3', 'cm-3', 'cm^{-3}', 1000000),
(5, 'second', 's', 's', 1);

-- --------------------------------------------------------

SET foreign_key_checks = 1;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
