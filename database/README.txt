CDB SQL database setup
----------------------

1. Install and start MySQL (MariaDB) server
2. Create the CDB database

mysql -u root -p < CDB_schema.sql

3. Create CDB users and the testing database - modify passwords for production use

mysql -u root -p < CDB_users.sql
