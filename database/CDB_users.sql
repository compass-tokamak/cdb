GRANT USAGE ON *.* TO 'CDB'@'%' IDENTIFIED BY 'cdb_data';
GRANT SELECT ON `CDB`.* TO 'CDB'@'%';
GRANT INSERT ON `CDB`.`signal_setup` TO 'CDB'@'%';
GRANT INSERT ON `CDB`.`da_computers` TO 'CDB'@'%';
GRANT INSERT ON `CDB`.`channel_setup` TO 'CDB'@'%';
GRANT INSERT ON `CDB`.`data_files` TO 'CDB'@'%';
GRANT INSERT ON `CDB`.`record_directories` TO 'CDB'@'%';
GRANT INSERT ON `CDB`.`rule_inputs` TO 'CDB'@'%';
GRANT INSERT ON `CDB`.`file_status` TO 'CDB'@'%';
GRANT INSERT ON `CDB`.`rule_outputs` TO 'CDB'@'%';
GRANT INSERT ON `CDB`.`DAQ_channels` TO 'CDB'@'%';
GRANT INSERT ON `CDB`.`postproc_log` TO 'CDB'@'%';
GRANT INSERT ON `CDB`.`data_signal_parameters` TO 'CDB'@'%';
GRANT INSERT ON `CDB`.`data_signals` TO 'CDB'@'%';
GRANT INSERT ON `CDB`.`postproc_rules` TO 'CDB'@'%';
GRANT INSERT ON `CDB`.`generic_signals` TO 'CDB'@'%';
GRANT INSERT ON `CDB`.`shot_database_pub` TO 'CDB'@'%';
GRANT INSERT ON `CDB`.`data_sources` TO 'CDB'@'%';
-- `CDB`@`localhost` permissions
GRANT USAGE ON *.* TO 'CDB'@'localhost' IDENTIFIED BY 'cdb_data';
GRANT SELECT, LOCK TABLES, SHOW VIEW ON `CDB`.* TO 'CDB'@'localhost';
GRANT INSERT ON `CDB`.`data_signal_parameters` TO 'CDB'@'localhost';
GRANT INSERT ON `CDB`.`channel_setup` TO 'CDB'@'localhost';
GRANT INSERT ON `CDB`.`shot_database_pub` TO 'CDB'@'localhost';
GRANT INSERT ON `CDB`.`postproc_log` TO 'CDB'@'localhost';
GRANT INSERT ON `CDB`.`record_directories` TO 'CDB'@'localhost';
GRANT INSERT ON `CDB`.`data_files` TO 'CDB'@'localhost';
GRANT INSERT ON `CDB`.`data_signals` TO 'CDB'@'localhost';
GRANT INSERT ON `CDB`.`generic_signals` TO 'CDB'@'localhost';
GRANT INSERT ON `CDB`.`signal_setup` TO 'CDB'@'localhost';
GRANT INSERT ON `CDB`.`file_status` TO 'CDB'@'localhost';
GRANT INSERT ON `CDB`.`da_computers` TO 'CDB'@'localhost';
GRANT INSERT, UPDATE (description, active, kwargs, func, rule_name) ON `CDB`.`postproc_rules` TO 'CDB'@'localhost';
GRANT INSERT ON `CDB`.`DAQ_channels` TO 'CDB'@'localhost';
GRANT INSERT ON `CDB`.`rule_inputs` TO 'CDB'@'localhost';
GRANT INSERT ON `CDB`.`data_sources` TO 'CDB'@'localhost';
GRANT INSERT ON `CDB`.`rule_outputs` TO 'CDB'@'localhost';
-- `CDB_exp`@`%` permissions
GRANT USAGE ON *.* TO 'CDB_exp'@'%' IDENTIFIED BY 'cdb_exp';
GRANT SELECT, INSERT, TRIGGER ON `CDB`.* TO 'CDB_exp'@'%';
GRANT SELECT, INSERT, TRIGGER ON `CDB_test`.* TO 'CDB_exp'@'%';
-- `CDB_exp`@`localhost` permissions
GRANT USAGE ON *.* TO 'CDB_exp'@'localhost' IDENTIFIED BY 'cdb_exp';
GRANT SELECT, INSERT, TRIGGER ON `CDB`.* TO 'CDB_exp'@'localhost';
GRANT SELECT, INSERT, TRIGGER ON `CDB_test`.* TO 'CDB_exp'@'localhost';

-- For unit tests

CREATE DATABASE IF NOT EXISTS `CDB_test` DEFAULT CHARACTER SET utf8 COLLATE utf8_czech_ci;

GRANT USAGE ON *.* TO 'CDB_test'@'localhost' IDENTIFIED BY 'cdb_data';
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES, CREATE VIEW, EVENT, TRIGGER, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, EXECUTE ON `CDB_test`.* TO 'CDB_test'@'localhost';
