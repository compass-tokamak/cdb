--
-- Table structure for table `shot_database_pub`
--

CREATE TABLE IF NOT EXISTS `shot_database_pub` (
  `record_number` int(11) NOT NULL,
  `record_time` datetime NOT NULL,
  `record_type` enum('VOID','MODEL') NOT NULL,
  `description` text,
  PRIMARY KEY (`record_number`),
  UNIQUE KEY `record_number_UNIQUE` (`record_number`),
  KEY `shot_time_INDEX` (`record_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `shot_database_pub`
--
DROP TRIGGER IF EXISTS `copy_to_shot_database`;
DELIMITER //
CREATE DEFINER = CDB_exp TRIGGER `copy_to_shot_database` BEFORE INSERT ON `shot_database_pub`
 FOR EACH ROW INSERT INTO  `CDB`.`shot_database`
(`record_number`,`record_time`, `record_type`, `description`)
VALUES
(NEW.record_number,NEW.record_time, NEW.record_type, NEW.description)
//
DELIMITER ;


--
-- PRIVILEGES
--
-- exp CDB_user

-- CREATE USER IF NOT EXISTS 'CDB_exp'@'localhost' IDENTIFIED BY  'cmpsSQLdata';
GRANT USAGE ON * . * TO  'CDB_exp'@'localhost' IDENTIFIED BY  'cmpsSQLdata' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;




GRANT SELECT ON  `CDB` . * TO  'CDB'@'%' IDENTIFIED BY  'cmpsSQLdata';
GRANT SELECT ON  `CDB` . * TO  'CDB'@'localhost' IDENTIFIED BY  'cmpsSQLdata';


REVOKE ALL PRIVILEGES ON  `CDB` . * FROM  'CDB'@'localhost';
GRANT SELECT ON  `CDB` . * TO  'CDB'@'localhost';

-- write signals
GRANT INSERT ON  `CDB`.`data_files` TO  'CDB'@'localhost';
GRANT INSERT ON  `CDB`.`file_status` TO  'CDB'@'localhost';
GRANT INSERT ON  `CDB`.`data_signals` TO  'CDB'@'localhost';
GRANT INSERT ON  `CDB`.`data_signal_parameters` TO  'CDB'@'localhost';
GRANT INSERT ON  `CDB`.`signal_setup` TO  'CDB'@'localhost';
-- create VOID and MODEL records
GRANT INSERT ON  `CDB`.`shot_database_pub` TO  'CDB'@'localhost';
GRANT INSERT ON  `CDB`.`record_directories` TO  'CDB'@'localhost';
-- manage data acquisition
GRANT INSERT ON  `CDB`.`da_computers` TO  'CDB'@'localhost';
GRANT INSERT ON  `CDB`.`DAQ_channels` TO  'CDB'@'localhost';
GRANT INSERT ON  `CDB`.`channel_setup` TO  'CDB'@'localhost';
-- manage generic signals and data sources
GRANT INSERT ON  `CDB`.`data_sources` TO  'CDB'@'localhost';
GRANT INSERT ON  `CDB`.`generic_signals` TO  'CDB'@'localhost';
-- manage postprocessing
GRANT INSERT ON  `CDB`.`postproc_log` TO  'CDB'@'localhost';
GRANT INSERT ON  `CDB`.`postproc_rules` TO  'CDB'@'localhost';
GRANT INSERT ON  `CDB`.`rule_inputs` TO  'CDB'@'localhost';
GRANT INSERT ON  `CDB`.`rule_outputs` TO  'CDB'@'localhost';


GRANT SELECT ON `CDB`.* TO 'CDB_exp'@'localhost';
GRANT INSERT ON `CDB`.* TO 'CDB_exp'@'localhost';


-- exp CDB_user

-- CREATE USER 'CDB_exp'@'%' IDENTIFIED BY  'cmpsSQLdata';
GRANT USAGE ON * . * TO  'CDB_exp'@'%' IDENTIFIED BY  'cmpsSQLdata' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;






REVOKE ALL PRIVILEGES ON  `CDB` . * FROM  'CDB'@'%';
GRANT SELECT ON  `CDB` . * TO  'CDB'@'%';

-- write signals
GRANT INSERT ON  `CDB`.`data_files` TO  'CDB'@'%';
GRANT INSERT ON  `CDB`.`file_status` TO  'CDB'@'%';
GRANT INSERT ON  `CDB`.`data_signals` TO  'CDB'@'%';
GRANT INSERT ON  `CDB`.`data_signal_parameters` TO  'CDB'@'%';
GRANT INSERT ON  `CDB`.`signal_setup` TO  'CDB'@'%';
-- create VOID and MODEL records
GRANT INSERT ON  `CDB`.`shot_database_pub` TO  'CDB'@'%';
GRANT INSERT ON  `CDB`.`record_directories` TO  'CDB'@'%';
-- manage data acquisition
GRANT INSERT ON  `CDB`.`da_computers` TO  'CDB'@'%';
GRANT INSERT ON  `CDB`.`DAQ_channels` TO  'CDB'@'%';
GRANT INSERT ON  `CDB`.`channel_setup` TO  'CDB'@'%';
-- manage generic signals and data sources
GRANT INSERT ON  `CDB`.`data_sources` TO  'CDB'@'%';
GRANT INSERT ON  `CDB`.`generic_signals` TO  'CDB'@'%';
-- manage postprocessing
GRANT INSERT ON  `CDB`.`postproc_log` TO  'CDB'@'%';
GRANT INSERT ON  `CDB`.`postproc_rules` TO  'CDB'@'%';
GRANT INSERT ON  `CDB`.`rule_inputs` TO  'CDB'@'%';
GRANT INSERT ON  `CDB`.`rule_outputs` TO  'CDB'@'%';


GRANT SELECT ON `CDB`.* TO 'CDB_exp'@'%';
GRANT INSERT ON `CDB`.* TO 'CDB_exp'@'%';









--
-- AXES
--

ALTER TABLE  `generic_signals` ADD  `axis4_id` INT NULL DEFAULT NULL AFTER  `axis3_id` ,
ADD  `axis5_id` INT NULL AFTER  `axis4_id` ,
ADD  `axis6_id` INT NULL AFTER  `axis5_id` ;

ALTER TABLE  `data_signals` CHANGE  `time_axis_revision`  `time_axis_revision` SMALLINT( 6 ) NOT NULL DEFAULT  '1',
CHANGE  `axis1_revision`  `axis1_revision` SMALLINT( 6 ) NOT NULL DEFAULT  '1',
CHANGE  `axis2_revision`  `axis2_revision` SMALLINT( 6 ) NOT NULL DEFAULT  '1',
CHANGE  `axis3_revision`  `axis3_revision` SMALLINT( 6 ) NOT NULL DEFAULT  '1';

ALTER TABLE  `data_signals` 
ADD  `axis4_revision` SMALLINT( 6 ) NOT NULL DEFAULT  '1' AFTER  `axis3_revision`,
ADD  `axis5_revision` SMALLINT( 6 ) NOT NULL DEFAULT  '1' AFTER  `axis4_revision`,
ADD  `axis6_revision` SMALLINT( 6 ) NOT NULL DEFAULT  '1' AFTER  `axis5_revision`;
