#!/usr/bin/env python
#
# Manually write MARTe data to CDB.
#
# (C) Jan Pipek, IPP AS CR, 2013
#
# Useful when the node does not understand and write them.
# Use `store_marte_data` function for that (see its documentation.)
import urllib.request, urllib.error, urllib.parse
import re
import numpy as np

# Load the modules
from pyCDB import client
cdb = client.CDBClient()

def store_marte_data(generic_signal_id, record_number, signal_name=None, file_name=None, data_type=None):
    '''Store manually obtained MARTe data to CDB.

    :param generic_signal_id: Generic signal ID belonging to the channel
    :param record_number: Record to which the signal belongs
    ...

    Input data can be in a number of formats base on the presence of parameters:
        signal_name - download them from atca web page
        file_name - read them from the file in a text format
        file_name + data_type - read them from a file in binary format
    
    Data types (any that numpy accepts):
        u4 - unsigned integer, i4 - signed integer, f4 - float, f8 - double, etc.

    Examples:
        store_marte_data(2387, 6000, signal_name="RECEIVEDFROMFPGA")
        store_marte_data(2387, 6322, file_name="/compass/home/pipek/RECEIVEDFROMFPGA6322.txt")
        store_marte_data(2387, 6319, file_name="/compass/home/pipek/RECEIVEDFROMFPGA6319.txt", data_type="u4")
    '''
    data = parse_marte_data(get_marte_data(signal_name, file_name), data_type)
    gs_ref = cdb.get_generic_signal_references(generic_signal_id=generic_signal_id)[0]
    data_file_key = gs_ref["generic_signal_name"].split(".")[-1]
    
    df_args = {
        "record_number" : record_number,
        "collection_name" : gs_ref["generic_signal_name"],
        "data_source_id" : gs_ref["data_source_id"],
    }
    print(df_args)
    data_file = cdb.new_data_file(**df_args)
    data_file_id = data_file["data_file_id"]
    
    h5_file = h5py.File(data_file['full_path'],'w')
    h5_file.create_dataset(data_file_key, data=data)
    h5_file.close()
    
    cdb.set_file_ready(data_file_id=data_file_id)
    
    ds_args = {
        "record_number" : record_number,
        "generic_signal_id" : generic_signal_id,
        "data_file_id" : data_file_id,
        "data_file_key" : data_file_key,
        "note" : "Manually added from downloaded data"
    }
    return cdb.store_signal(**ds_args)

def marte_channel_names(record_number=None):
    '''Get all MARTe channel name stored in a shot.

    Example:
        marte_channel_names(6300)
    '''
    def get_generic_signal_name(ref):
        return cdb.get_generic_signal_references(ref["generic_signal_id"])[0]["generic_signal_name"]
    
    refs = cdb.get_signal_references(record_number=record_number, computer_name="marte", revision=-1)
    return [ get_generic_signal_name(ref) for ref in refs ]

def get_marte_data(signal_name=None, file_name=None):
    '''Get MARTe data from web or from a file.

    :param signal_name: The name the signal has on the MARTe web.
    :param file_name: The file name from which to read.

    You have to specify just one of the parameters.

    Examples:
        get_marte_data("")
    '''
    if signal_name:
        url = "http://atca1:8084/HTTPSignalServer/?SignalRequest={}&binaryDownloadMode=false".format(signal_name)
        return urllib.request.urlopen(url).read()
    elif file_name:
        with open(file_name, "r") as f:
            return f.read()
    else:
        raise Exception("You have to specify signal_name of file_name.")

def parse_marte_data(data, data_type=None):
    '''Parse a string to meaningful numeric data.

    :param data_type: If set, the string is treated as binary representation of this type.

    It data_type is not set, self-describing text format (that contains type information) is assumed.
    '''
    # Binary format
    if data_type:
        return np.fromstring(data, dtype=data_type)
    
    # Text format
    type_re = re.compile(r"DataType\s=\s(\w+)\s")
    data_type = type_re.search(data).groups()[0]
    
    values_re = re.compile("\{(.*)\}", re.DOTALL)
    values = values_re.search(data).groups()[0].split()
    
    if data_type == 'uint32':
        return np.array([value for value in values], dtype="u4")
    elif data_type == 'float':
        return np.array([value for value in values], dtype="f4")
    else:
        raise Exception("Unknown data type: {}. Please implement it.".format(data_type))

