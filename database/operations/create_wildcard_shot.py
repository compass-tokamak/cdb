#!/usr/bin/python3
"""
Creates shot to be used with wildcard generic signals.
"""

from pyCDB.pyCDBBase import CDBException
from pyCDB import DAQClient

cdb = DAQClient.CDBDAQClient()

cdb.create_record(record_number=0,description='Wildcard shot (generic signals with wildcard flag will always return signals from this record).')
