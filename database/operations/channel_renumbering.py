#!/usr/bin/env python
#
# Renumbering of channels, boards and computers in the DB to make sense.
# (although they are not intended for direct use).
#
# (C) Jan Pipek 2013
#
# Please understand that you will need UPDATE privileges for the database.
#
# WARNING:
#   This procedure is very dangerous and not fully tested yet.
#   Do not run it on data you will need later.
#
# IDEA:
#   The procedure should be done in two steps:
#   1) All computer_id's, board_id's and channel_id's are changed to "safe values"
#   2) Computer after computer, you have to "move" them back to reasonable ones.
#
# USAGE:
# - escape(computer_id) will change computer_id, board_id, channel_id to high values
#      that won't collide with any values that you really want to use. (for 1 computer)
# - escape() will do the same for all computers
# - escape_boards(computer_id) will do the same but just for board and channel ids
# - move_computer(old_id, new_id) will change the computer_id in all occurrences (all tables).
# - move_boards(computer_id, parse_board_names) will renumber all boards and channels.
# - move_computer_and_boards(old_id, new_id, parse_names) combines last two methods
# 
# SAFETY:
# - escape does not check if target ids are free. You should make sure first.
# - move_* methods first check if the target numbers are free before doing any harm.
from pyCDB import DAQClient
import re

client = DAQClient.CDBDAQClient()

tables = [ "channel_setup", "DAQ_channels", "data_signals" ]   # Tables with board_id & channel_id
tables_with_computer_id = tables + [ "da_computers" ]          # Tables with computer_id

### PUBLIC METHODS ###
def escape(computer_id = None):
	"""Change all primary keys to safe values (that won't collide).

	Adds 10000 to computer_id's, board_id's and channel_id's. 
	"""
	if computer_id:
		condition = " WHERE computer_id = %d" % computer_id
	else:
		condition = ""

	escape_boards(computer_id)

	cursor = get_cursor()
	for table in tables_with_computer_id:
		sql = "UPDATE %s SET computer_id = computer_id + 10000 %s; " % ( table, condition )
		print(sql)
		cursor.execute( sql )
	close_cursor(cursor)

def escape_boards(computer_id=None):
	if computer_id:
		condition = " WHERE computer_id = %d" % computer_id
	else:
		condition = ""
	cursor = get_cursor()
	for table in tables:
		sql = "UPDATE %s SET board_id = board_id + 10000, channel_id = channel_id + 10000 %s; " % ( table, condition )
		print(sql)
		cursor.execute( sql )
	close_cursor(cursor)

def list_computers():
	sql = "SELECT computer_id, computer_name FROM da_computers"
	cursor = get_cursor()
	cursor.execute( sql )
	rows = cursor.fetchall()
	close_cursor( cursor )
	print(rows)

def move_computer(old_id, new_id):
	"""Change computer_id into another.

	Does not change board_id's & channel_id's (see `move_boards`).
	"""
	# begin transaction
	check_computer_existence(new_id)

	if new_id > 32767:
		raise Exception("Invalid new computer id %d, maximum is 32767." % new_id)

	cursor = get_cursor()
	for table in tables_with_computer_id:
		sql = "UPDATE %s SET computer_id = %d WHERE computer_id = %d; " % ( table, new_id, old_id )
		print(sql)
		cursor.execute( sql )

def move_computer_and_boards(old_id, new_id, parse_names):
	"""Combines move_computer and move_boards."""
	move_computer(old_id, new_id)
	move_boards(new_id, parse_names)

def move_boards(computer_id, parse_names):
	"""Change board & channel ids for one computer.

	If (all) target board numbers are available, changes the board_id's.
	After changing each board number, it also tries to renumber its channels (either all or none).

	Thus the DB should be consistent after each step.

	Parameters:
	-----------
	parse_names - whether we should deduce board & channel numbers from their names.
	"""
	mapping = get_board_mapping(computer_id, parse_names)
	for board in mapping:
		check_board_existence(computer_id, board[1])

	for board in mapping:
		move_board(computer_id, board[0], board[1], parse_names)	

def move_board(computer_id, old_board_id, new_board_id, parse_names = False):
	cursor = get_cursor()
	for table in tables:
		sql = "UPDATE %s SET board_id = %d WHERE board_id = %d AND computer_id = %d; " % ( table, new_board_id, old_board_id, computer_id )
		print(sql)
		cursor.execute( sql )
	close_cursor(cursor)

	channel_mapping = get_channel_mapping(computer_id, new_board_id, parse_names)

	for channel in channel_mapping:
		check_channel_existence(computer_id, new_board_id, channel[1])

	cursor = get_cursor()
	for table in tables:
		for channel in channel_mapping:
			sql = "UPDATE %s SET channel_id = %d WHERE channel_id = %d AND board_id = %d AND computer_id = %d; " % ( table, channel[1], channel[0], new_board_id, computer_id )
			print(sql)
			cursor.execute( sql )
			cursor.fetchall()
	close_cursor(cursor)

### COMPUTER, BOARD & PARAMETER METHODS ###
def get_channel_mapping(computer, board, parse_channel_names = False):
	"""Return ((old_id, new_id), ...)"""
	sql = "SELECT DISTINCT(channel_id), parameteruniqueid FROM `DAQ_channels` WHERE computer_id = %d AND board_id = %d" % ( computer, board)
	cursor = get_cursor()
	cursor.execute(sql)
	channels = cursor.fetchall()
	close_cursor(cursor)

	if len(channels) == 0:
		return [ ]
	elif parse_channel_names:
		return [ (b[0], parse_name( b[1] )) for b in channels ]
	else:
		return [ (channels[i][0], i+1) for i in range(0, len(channels)) ]

def get_board_mapping(computer, parse_board_names = False):
	"""Get a tuple of tuples ((old_board_id, new_board_id), ...).

	Parameters:
	-----------
	parse_board_names - if False, boards are number sequentially
	"""
	sql = "SELECT DISTINCT(board_id), hardwareuniqueid FROM `DAQ_channels` WHERE computer_id = %s" % computer
	cursor = get_cursor()
	cursor.execute(sql)
	boards = cursor.fetchall()
	close_cursor(cursor)

	if len(boards) == 0:
		sql = "SELECT COUNT(*) AS c FROM DAQ_channels WHERE computer_id = %d LIMIT 1" % computer
		if row_exists(sql):
			return []
		else:
			raise Exception("Computer id does not exist: %d" % computer)
	elif parse_board_names:
		return [ (b[0], parse_name( b[1] )) for b in boards ]
	else:
		return [ (boards[i][0], i+1) for i in range(0, len(boards)) ]

def parse_name(name):
	"""Deduce numerical id from the name (for channels or boards).

	Returns the deduced number.
	If nothing is found, throw an exception.
	"""
	m = re.search('([0-9]+)$', name)
	if m and len(m.groups()) > 0:
		return int(m.group(1))
	else:
		raise Exception("Cannot parse name: %s" % name)

def check_computer_existence(computer_id):
	"""Check if target computer_id is available."""
	sql = "SELECT COUNT(*) AS c FROM DAQ_channels WHERE computer_id = %d LIMIT 1" % computer_id
	if row_exists(sql):
		raise Exception("Computer id already exists: %d" % computer_id)

def check_board_existence(computer_id, board_id):
	"""Check if target computer_id, board_id is available."""
	sql = "SELECT COUNT(*) AS c FROM DAQ_channels WHERE computer_id = %d AND board_id = %d LIMIT 1" % (computer_id, board_id )
	if row_exists(sql):
		raise Exception("Board id already exists: %d, %d" % (computer_id, board_id ) )

def check_channel_existence(computer_id, board_id, channel_id):
	"""Check if target computer_id, board_id, channel_id is available."""
	sql = "SELECT COUNT(*) AS c FROM DAQ_channels WHERE computer_id = %d AND board_id = %d AND channel_id = %d LIMIT 1" % (computer_id, board_id, channel_id)
	if row_exists(sql):
		raise Exception("Channel id already exists: %d, %d, %d") % (computer_id, board_id, channel_id)

### DATABASE HELPER METHODS ###
def get_cursor():
	"""Get a cursor.

	The cursor won't check foreign keys consistence.
	Close it with close_cursor() to enable foreign keys again.
	"""
	cursor = client.db.cursor()
	sql = "SET foreign_key_checks = 0"
	cursor.execute(sql)
	cursor.fetchall()
	return cursor

def close_cursor(cursor):
	# cursor.commit()
	sql = "SET foreign_key_checks = 1"
	cursor.execute(sql)
	cursor.fetchall()
	cursor.close()

def row_exists(sql):
	"""Check if at least one row exists (specified by SQL)."""
	cursor = get_cursor()
	cursor.execute(sql)
	row = cursor.fetchone()
	close_cursor(cursor)
	return row[0] > 0

if __name__ == "__main__":
	print("Please use this file interactively from within python or ipython console.")
	print("Before you do it, have a look at the source code and ensure that you know what you are doing.")
