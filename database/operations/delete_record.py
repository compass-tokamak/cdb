from pyCDB import DAQClient

cdb = DAQClient.CDBDAQClient()

def delete_all_data_files(record):
	sql = "SELECT * FROM data_files WHERE record_number = {}".format(record)
	data_file_ids = [ df["data_file_id"] for df in cdb.query(sql) ]
	print(data_file_ids)
	for data_file_id in data_file_ids:
		cdb.delete("file_status", { "data_file_id" : data_file_id})
		cdb.delete("data_files", { "data_file_id" : data_file_id})

def delete_all_signals(record):
	signals = cdb.get_signal_references(record_number=record)
	print(len(signals))
	for signal in signals:
		cdb.delete("data_signals", { "record_number" : record, "generic_signal_id" : signal["generic_signal_id"]})

def delete_record(record):
	delete_all_signals(record)
	delete_all_data_files(record)
	cdb.delete("record_directories", { "record_number" : record})
	cdb.delete("shot_database", {"record_number" : record})
	
