;program for testing of correspondence between new and old database
;Written by V.Weinzettl
;Adapted by J.Pipek

;shot_list=long([3467]);,2820,2900,3166])
shot_list=long([3613])
;shot_list=long([3412,3415,3416,3417,3418,3419,3435,3436,3437,3538])
;shot_list=long([3314,3315,3329,3412,3415,3416,3417,3418,3419,3435,3436,3437,3538])
;shot_list=long([3792,3797,3839,3467])
atca_list=[1]
ATCA_factor=2.d *2.048d *5.0306d /2.d^32 ;or 1.d


cdb=cdb_client()

verbose=0

if verbose eq 1 then begin
  window, xsize=10, ysize=10, /pixmap & wdelete, !d.window  ;corrects the first window colors on Linux (IDL bug causing white on white figure)
  device,decomposed=1,retain=2 & !P.color='000000'X & !P.background='FFFFFF'X & !X.style=1 & !Y.style=1 & !P.MULTI=[0,1,4] & !P.charsize=2
  window,0, xsize=1200, ysize=900
  endif
filename='_report_new_old_database_comparison.txt'


for shot_i=1, n_elements(shot_list) do begin
  shot=shot_list[shot_i-1]
  print, 'Shot: ', shot
  for atca_i=1, n_elements(atca_list) do begin
    atca=atca_list[atca_i-1]
    print, 'ATCA: ', atca
    case atca of 
      1: board_list=[1,2,4,5,9,10,11,13,12,3]
      2: board_list=[4,5,6]
      endcase
    for board_i=1, n_elements(board_list) do begin
      board=board_list[board_i-1]
      print, 'Board: ',board
      channel_list=1+indgen(32)
      for channel_i=1, 32 do begin; n_elements(channel_list) do begin
        channel=channel_list[channel_i-1]
        print, 'Channel: ', channel

        openw, 1, '~/dir_for_transfer/newCDB/results/shot_'+strcompress(string(shot),/remove_all)+'_a'+string(atca, format='(I1)')+'b'+strcompress(string(board),/remove_all)+'c'+strcompress(string(channel),/remove_all)+filename
        printf,1, 'Shot: ', shot
        printf,1, 'ATCA: ', atca
        printf,1, 'Board: ',board
        printf,1, 'Channel: ', channel
        
getCODAS, data=data, time=time, device_type='ATCA', shot=shot, atca=atca, board=board, channel=channel, oATCA=oATCA, error_msg=error_msg,db='old_DB'
if error_msg ne '' then begin
  print, 'Data from old database not ready'
  printf,1, 'Data from old database not ready'
  ;stop
  endif
  
  
if channel lt 10 then zero_txt='0' else zero_txt=''
data_name="DAQ:atca"+string(atca, format='(I1)')+"/"+strcompress(string(board),/remove_all)+"/"+strcompress(string(channel),/remove_all)+':'+strcompress(string(shot),/remove_all)+'[DAV]'
datastructure=cdb.get_signal( data_name)
if datastructure.isset eq 1 then error_msgCDB='' else error_msgCDB='CDB data are not available'
if error_msgCDB ne '' then begin
  print, 'Data from CDB not ready' 
  printf,1, 'Data from CDB not ready'
  ;stop
  endif else begin
  dataCDB=datastructure.data;*ATCA_factor
  timeCDB=datastructure.time_axis.data;*1000.d
  endelse

if verbose eq 1 then begin
  if error_msg eq '' then begin
    plot, time, data, title='Old database #'+strcompress(string(shot),/remove_all)+'_a'+string(atca, format='(I1)')+'b'+strcompress(string(board),/remove_all)+'c'+strcompress(string(channel),/remove_all),/nodata
    oplot, time, data, color='0000ff'x
    endif
      
  if error_msgCDB eq '' then begin
    plot, timeCDB, dataCDB, title='New database',/nodata
    oplot, timeCDB, dataCDB, color='0000ff'x
    endif
  endif
    
  if (error_msg eq '') and (error_msgCDB eq '') then begin
    same_size_signals= (n_elements(data) eq n_elements(dataCDB))
    if same_size_signals then same_size_signals_txt='Yes' else same_size_signals_txt='No'
    print,'Signal data of the same size? ',same_size_signals_txt
    printf,1,'Signal data of the same size? ',same_size_signals_txt
    if not(same_size_signals) then begin 
      print, 'Number of signal samples in old database: ', n_elements(data)
      printf,1, 'Number of signal samples in old database: ', n_elements(data)
      print, 'Number of signal samples in CDB: ', n_elements(dataCDB)
      printf,1, 'Number of signal samples in CDB: ', n_elements(dataCDB)
      endif
    same_size_times= (n_elements(time) eq n_elements(timeCDB))
    if same_size_times then same_size_times_txt='Yes' else same_size_times_txt='No'
    print,'Time axis of the same size? ',same_size_times_txt
    printf,1,'Time axis of the same size? ',same_size_times_txt
    if not(same_size_times) then begin 
      print, 'Number of time samples in old database: ', n_elements(time)
      printf,1, 'Number of time samples in old database: ', n_elements(time)
      print, 'Number of time samples in CDB: ', n_elements(timeCDB)
      printf,1, 'Number of time samples in CDB: ', n_elements(timeCDB)
      
      ;optional test of agreement for measurements longer than 1 s
      print,total(abs(time(*)-timeCDB(0L:n_elements(time)-1L)))/double(n_elements(time)-1L)
      print,total(abs(data(*)-dataCDB(0L:n_elements(data)-1L)))/double(n_elements(data)-1L)
      endif    
    if same_size_times and same_size_signals then begin  
      difference_signal=data-dataCDB
      difference_time=time-timeCDB
      avg_diff_time=total(difference_time)/double(n_elements(difference_time))
      possible_time_shift=time(0)-timeCDB(0)
      avg_diff_signal=total(difference_signal)/double(n_elements(difference_signal))
      max_diff_signal=max(abs(difference_signal))
      max_diff_time=max(abs(difference_time))
      time_tolerance=1.d-10 ;[s]
      signal_tolerance=1.d-10 ;[V]
      same_times=(max_diff_time le time_tolerance) & if same_times then same_times_txt='Yes' else same_times_txt='No'
      same_signals=(max_diff_signal le signal_tolerance) & if same_signals then same_signals_txt='Yes' else same_signals_txt='No'
      
      if verbose eq 1 then begin
        plot, difference_signal, title='Signal difference', xmargin=[16,2],/nodata
        oplot, difference_signal, color='ff0000'x
        plot, difference_time, title='Time difference', xmargin=[16,2], /nodata
        oplot, difference_time, color='ff0000'x
        wait, 0.01
        write_png,'~/dir_for_transfer/newCDB/results/shot_'+strcompress(string(shot),/remove_all)+'_a'+string(atca, format='(I1)')+'b'+strcompress(string(board),/remove_all)+'c'+strcompress(string(channel),/remove_all)+'.png',tvrd(true=1)
        endif
         
      print, 'Time axes are the same [ms]?   ', same_times_txt
      printf,1, 'Time axes are the same [ms]?   ', same_times_txt
      if not(same_times) then begin
        print, 'avg. diff.= ', string(avg_diff_time, format='(F16.10)'),    ', max. diff.= ', string(max_diff_time, format='(F16.10)'), ', probable time shift= ', string(possible_time_shift, format='(F16.10)')
        printf,1, 'avg. diff.= ', string(avg_diff_time, format='(F16.10)'),    ', max. diff.= ', string(max_diff_time, format='(F16.10)'), ', probable time shift= ', string(possible_time_shift, format='(F16.10)')
        endif
      print, 'Signal data are the same [V]? ', same_signals_txt
      printf,1, 'Signal data are the same [V]? ', same_signals_txt
      if not(same_signals) then begin
        print, 'avg. diff.= ', string(avg_diff_signal, format='(F16.10)'), ', max. diff.= ', string(max_diff_signal, format='(F16.10)')
        printf,1, 'avg. diff.= ', string(avg_diff_signal, format='(F16.10)'), ', max. diff.= ', string(max_diff_signal, format='(F16.10)')
        endif
      if not(same_times) or not(same_signals) then begin
        temp=dialog_message('Data are not the same in old and new databases!',/error) 
         stop
         endif
    endif
  endif

print, ' '
close,1
wait, 0.01
endfor
endfor
endfor
endfor
!P.MULTI=[0,1,1]
end