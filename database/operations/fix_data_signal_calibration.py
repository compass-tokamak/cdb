#!/usr/bin/env python
#
# Fixing calibration constants for stored data signals.
#
# (C) Jan Pipek 2013
#
# Firesignal (before fixing) wrote data with incorrect calibration constants.
# Instead of filling both coefficient and coefficient_V2unit, their product
# was stored in coefficient and 1.0 in coefficient_V2unit (Issue #278 in redmine)
#
# Although motivated by this problem, functions from this module are general
#
# Terminology:
# - difference: data_signal and attachment coefficients have same product but differ in terms.
# - conflict: data_signal and attahchment coefficients differ and have different product as well.
import numpy
from pyCDB import DAQClient

client = DAQClient.CDBDAQClient()

def is_different(signal, attachment):
    '''Check if there is a difference in calibration between data_signal and channel attachment.'''
    return signal["coefficient"] != attachment["coefficient_lev2V"] or signal["coefficient_V2unit"] != attachment["coefficient_V2unit"]

def is_conflict(signal, attachment):
    '''Check if the product of coefficients is the same (with tolerance) for both signal and attachment.'''
    product_signal = signal["coefficient"] * signal["coefficient_V2unit"]
    product_attachment = attachment["coefficient_lev2V"] * attachment["coefficient_V2unit"]
    return not(numpy.allclose(product_signal, product_attachment, 1e-8, 1e-9) or attachment["coefficient_V2unit"] == 0)

def find_matching(signals, attachments, filter):
    '''Match data_signals and channel_attachments.

    Returns a list of (signal, attachment) pairs. Signals with no attachment
    are ignored.
    '''
    attachment_dict = { a["attached_generic_signal_id"] : a for a in attachments }
    def get_attachment(signal):
        return attachment_dict.get(signal["generic_signal_id"]) 
    signals_with_attachment = ((signal, get_attachment(signal)) for signal in signals if get_attachment(signal))
    return [ pair for pair in signals_with_attachment if list(filter(*pair)) ]

def analyse_record(record_number):
    '''Create lists of differences and conflicts for a shot.'''
    result = {}
    signals = client.get_signal_references(record_number=record_number, revision=-1)
    attachments = client.get_attachment_table(record_number=record_number)
    result["differences"] = find_matching(signals, attachments, is_different)   
    result["conflicts"] = find_matching(signals, attachments, is_conflict)
    return result

def show_difference(signal, attachment):
    '''Print the difference between signal and the attachment.'''
    print("%d - Diff:" % signal["generic_signal_id"])
    print("Data signal:     {:e}     {:e}      {:e}".format(
        signal["coefficient"], signal["coefficient_V2unit"], signal["coefficient"] * signal["coefficient_V2unit"] ))
    print("Attachment:      {:e}     {:e}      {:e}".format(
        attachment["coefficient_lev2V"], attachment["coefficient_V2unit"], attachment["coefficient_lev2V"] * attachment["coefficient_V2unit"] ))
    print()   

def show_differences(record_number):
    '''Print info about all conflicts and differences for a record.'''
    analysis = analyse_record(record_number)
    print("*** Conflicts ***")
    for (signal, attachment) in analysis["conflicts"]:
        show_difference(signal, attachment)

    print()
    print()
    print("*** Differences ***")
    for (signal, attachment) in analysis["differences"]:
        show_difference(signal, attachment)

def check_records(min = 0, max=10000):
    '''Display summary of differences and conflicts for a range of records.'''
    sql = "SELECT shot_database.* FROM shot_database WHERE record_type='EXP' AND record_number > {} AND record_number < {}".format(min, max)
    record_numbers = [ row["record_number"] for row in client.query(sql) ]
    for record_number in record_numbers:
        analysis = analyse_record(record_number)
        print("Record {}: {} conflicts, {} differences".format(record_number, len(analysis["conflicts"]), len(analysis["differences"])))

def fix_data_signal(signal, attachment):
    '''Create a new revision of data signal with correct coefficients.

    First checks for difference, does not fix already correct signals.
    '''
    if is_different(signal, attachment):
        client.update_signal(
            generic_signal_id=signal["generic_signal_id"], record_number=signal["record_number"],
            coefficient=attachment["coefficient_lev2V"], coefficient_V2unit=attachment["coefficient_V2unit"],
            note='Fixed calibration coefficients (Pipek)'
        )

def fix_shot(record_number, ignore_conflicts=False):
    '''Find all differing channels and fix them.

    :params ignore_conflicts: If true, don't check for conflicts and fix them as well.
    '''
    analysis = analyse_record(record_number)
    if len(analysis["conflicts"]) > 0 and not ignore_conflicts:
        raise Exception("Cannot fix record with conflicts")
    else:
        fixed = 0
        for (signal, attachment) in analysis["differences"]:
            fix_data_signal(signal, attachment)
            fixed += 1
        if ignore_conflicts:
            for (signal, attachment) in analysis["conflicts"]:
                fix_data_signal(signal, attachment)
                fixed += 1
    print("Fixed shot %i, %i data signals affected." % (record_number, fixed))

def fix_shots(shots):
    '''Fix multiple shots.

    :param shots: enumerator of shot numbers
    '''
    for shot in shots:
        if client.record_exists(record_number = shot):
            fix_shot(shot)



