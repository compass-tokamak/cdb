#!/usr/bin/env python
#
# Script that first checks presence of all signals and then runs the copying.
#
from convert_old_db import *
import sys

def do_range(shots):
	if not (len(sys.argv) == 4 and sys.argv[3] == '1'):
		if not check_range(shots):
			return
	convert(shots)

if __name__ == "__main__":
	if len(sys.argv) == 2:
		do_range((int(sys.argv[1]),))
	elif len(sys.argv) >= 3:
		min_ = int(sys.argv[1])
		max_ = int(sys.argv[2])
		if min_ > max_:
			step = -1
		else:
			step = 1
		do_range(list(range(min_, max_, step)))
	else:
		print("Shot range not specified.")
