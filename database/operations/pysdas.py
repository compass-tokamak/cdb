#!/usr/bin/env python
#
# Module for by-passing SDAS.
#
# (C) Jan Pipek, IPP AS CR, 2013
#

import datetime
import psycopg2
import os
import itertools
from xml.dom import minidom
import numpy as np
import itertools

conn = None
cur = None

def _delta_in_ms(delta):
    seconds = delta.seconds

    if delta.days != 0:
        seconds += 86400 * delta.days

    if abs(seconds) > 2:
        attempts = (-3600, 3600, 7200, -7200)
        for attempt in attempts:
            if abs(seconds - attempt) < 2:
                seconds = seconds - attempt

    return seconds * 1000 + delta.microseconds / 1000.

class SignalFragment(object):
    '''One part of the signal data.'''
    def __init__(self, iterable, nodeuniqueid=None, hardwareuniqueid=None):
        self.data = list(iterable)
        self.node = nodeuniqueid
        self.hardware = hardwareuniqueid

    def __getitem__(self, index):
        return self.data[index]

    @property
    def event_id(self):
        return self[5][0]

    @property
    def event(self):
        return get_event(self.event_id)

    @property
    def tstart(self):
        return self[1]

    @property
    def tstart_ps(self):
        return self[2]

    @property
    def tend(self):
        return self[3]

    @property
    def tend_ps(self):
        return self[4]

    @property
    def channel(self):
        return self[0]

    @property
    def time0(self):
        return _delta_in_ms(self.tstart - self.event.tstart) + self.tstart_ps / 1000000000.

    @property
    def in_file(self):
        return get_file_location(self.node, self.hardware, self, extension="bin")

    def read_data(self, dtype):
        import numpy as np
        path = self.in_file

        if not os.path.exists(path):
            raise Exception("File %s does not exist." % path)
        with open(path) as f:
            return np.fromfile(f, dtype=dtype)

class Signal(object):
    '''Signal consisting (possibly) of multiple time fragments.'''
    def __init__(self, fragments):
        self.fragments = list(sorted(fragments, key=lambda s: s.time0))
        self._data = None

    @property
    def hardware_info(self):
        return get_hardware(self.node, self.hardware)

    @property
    def dtype(self):
        mime_type = self.mime_type
        if mime_type == "data/short_array":
            return ">i2"
        elif mime_type == "data/int_array":
            return ">i4"
        elif mime_type == "data/float_array":
            return ">f4"
        else:
            return None

    def __getitem__(self, index):
        return self.fragments[index]

    @property
    def mime_type(self):
        dom = minidom.parseString(self.hardware_info.xml)
        params = dom.getElementsByTagName('parameter')
        if len(params):
            for param in params:
                if param.getAttribute('uniqueID').lower() == self.channel.lower():
                    return param.getAttribute('mimeType')
            else:
                raise Exception("Unknown MIME type for " + self.name )

    @property
    def event_id(self):
        return self.fragments[0].event_id

    @property
    def event(self):
        return self.fragments[0].event

    @property
    def channel(self):
        return self.fragments[0].channel

    @property
    def node(self):
        return self.fragments[0].node

    @property
    def hardware(self):
        return self.fragments[0].hardware

    @property
    def FS_id(self):
        return(self.node, self.hardware, self.channel)

    @property
    def time0(self):
        return self.fragments[0].time0

    @property
    def time_coefficient(self):
        return self.length / len(self.data)

    @property
    def length(self):
        first = self.fragments[0]
        last = self.fragments[-1]
        return _delta_in_ms(last.tend - first.tstart) + ( last.tend_ps - first.tstart_ps) / 1000000000.

    @property
    def data(self):
        if self._data == None:
            if not self.is_complete():
                raise Exception("Data not complete.")
            datas = [ s.read_data(self.dtype) for s in self.fragments ]
            self._data = np.concatenate(datas)
        return self._data

    def is_empty(self):
        return len(self.fragments) == 0 or len(self.data) == 0

    def is_complete(self):
        for i, sig in enumerate(self.fragments[1:]):
            previous = self.fragments[i]
            if previous.tend != sig.tstart:
                return False
            if previous.tend_ps != sig.tstart_ps:
                return False
        return True

    def in_files(self):
        return [ s.in_file for s in self.fragments ]

    def write_hdf5(self, path, dataset_name = None):
        import h5py
        import numpy as np

        if not dataset_name:
            dataset_name = self.channel

        h5_file = h5py.File(path,'w')
        h5_file.create_dataset(dataset_name, data=self.data)
        h5_file.close()

    @property
    def name(self):
        return "%s.%s.%s" % (self.node, self.hardware, self.channel)

    @property
    def str_id(self):
        return "%s:%d" % (self.name, self.event_id)

    def __str__(self):
        s = "[Signal: %s:%d (%d signals)" % (self.channel, self.event_id, len(self.fragments))
        if not self.is_complete():
            s += " BROKEN"
        return s + "]"

    def __repr__(self):
        return "<pysdas.Signal('%s', %d)>" % ( self.name, self.event_id)

class Event(list):
    '''A shot'''
    @property
    def tstart(self):
        return self[2]

class HardwareInfo(object):
    '''Hardware as stored in hardware_description table.'''
    def __repr__(self):
        return "HardwareInfo<%s, %s>" % (self.nodeuniqueid, self.hardwareuniqueid)

    def __init__(self, table_row):
        self.nodeuniqueid = table_row[0]
        self.hardwareuniqueid = table_row[1]
        self.xml = table_row[2][:]

def connect():
    global conn, cur
    conn = psycopg2.connect("dbname=fsdb user=fsdbadmin host=codac1 password=c25t0r")
    cur = conn.cursor()

def run_sql(sql):
    if not conn or not cur:
        connect()
    cur.execute(sql)
    res = cur.fetchall()
    # print "SQL: %s" % sql
    return res

def get_event(shot):
    return Event(run_sql("SELECT * FROM events WHERE id=%d" % shot)[0])

def get_hardware(nodeuniqueid, hardwareuniqueid):
    sql = "SELECT * FROM hardware_description WHERE nodeuniqueid='%s' AND hardwareuniqueid='%s'" % (nodeuniqueid, hardwareuniqueid)
    return HardwareInfo(run_sql(sql)[0])

_hws = None # Cache
def get_all_hardwares(reload=False):
    '''All hardwares known to the SQL database.

    The list is cached unless 'reload' argument is True.
    '''
    global _hws
    if not _hws or reload:
        _hws = [ HardwareInfo(info) for info in run_sql("SELECT * FROM hardware_description") ]
    return _hws


def get_all_signals(shot):
    '''Get all signals for a shot.'''

    hws = get_all_hardwares()
    signals = [ get_signals(node=hw.nodeuniqueid, hardware=hw.hardwareuniqueid, shot=shot) for hw in hws ]
    return list(itertools.chain(*signals))

def get_signals(node, hardware, channel=None, shot=None):
    '''Get signals based for a board on specific criteria.'''
    board_name = "%s_%s" % (node, hardware)
    sql = "SELECT * FROM %s" % board_name.lower()
    if shot:
        sql += " WHERE %d = ANY(eventid)" % shot
    if channel:
        sql += " AND parameteruniqueid = '%s'" % channel
    fragments = [SignalFragment(data, node, hardware) for data in run_sql(sql)]
    fragments = sorted(fragments, key=lambda s: (s.event_id, s.channel))
    return [ Signal(values) for key, values in itertools.groupby(fragments, lambda s: (s.event_id, s.channel)) ]

def get_signal(name, shot):
    '''Shortcut for getting signal.'''
    name_frags = name.split(".")
    signals = get_signals(*name_frags, shot=shot)
    if signals:
        return get_signals(*name_frags, shot=shot)[0]
    else:
        return None

def hash_date_string(d):
    '''Hash string as described in Java.

    s[0]*31^(n-1) + s[1]*31^(n-2) + ... + s[n-1]
    '''
    hash = 0
    for c in d:
        hash *= 31
        hash += ord(c)
        hash = hash % 2 ** 32
    if hash >= 2 ** 31:
        hash = hash % 2**32 - 2**32
    else:
        hash = hash % 2**32
    # print "Hash %s=%s" % (d, hash)
    return hash

def hash_date(d):
    string = d.strftime("%d-%m-%Y")
    return hash_date_string(string)

# Set this to SDAS data parent directory
base_dir="~/sshfs/codac1/codac/fsignal_bin_data/"

def get_file_location(nodeuniqueid, hardwareuniqueid, signal, extension="bin"):
    hw_path = os.path.join(base_dir, nodeuniqueid, hardwareuniqueid)
    rec_path = os.path.join(hw_path, "A" + str(hash_date(signal[1])))
    date_string = signal[1].strftime("%d-%m-%Y")
    time_string = "%d_%d_%d" % (signal[1].hour, signal[1].minute, signal[1].second)
    us = signal[1].microsecond
    ps = signal[2]
    ms = us // 1000
    us = us % 1000
    ns = ps // 1000
    ps = ps % 1000
    time_string = time_string + ".%d.%d.%d.%d" % (ms, us, ns, ps)
    file_name = "%s_%s_%s.%s" % (signal[0], date_string, time_string, extension)
    return os.path.expanduser(os.path.join(rec_path, file_name))
