#!/usr/bin/env python
#
# Functions for copying from old to new database.
#
# (C) Jan Pipek 2013
#
# This module loads necessary clients for SDAS (sdas) and CDB (cdb).
# In itself, it does not run anything. You should import it and 
# use its functions:
#
# convert(record_number) - copy all data of a record from SDAS to CDB
# create_channels(dotted channel names) - create channels in CDB (limited functionality)
#
# Convert can be run safely. It checks for the existence of generic signals
# and channels before storing anything => for each shot either nothing or everything.
# (if no other errors arise)
#
# The script uses customized SDAS client (see imports...). Without it,
# several channels would be truncated to 8 MB length.

import time
import sys
import os

# Use the version of SDAS included in CDB
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../../FireSignal")))
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../../FireSignal/sdas/core")))
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../../FireSignal/sdas/core/common")))

from sdas.core.client.SDASClient import SDASClient, Data

from pyCDB import DAQClient
import datetime
import h5py
import numpy as np

sdas_host = 'codac1.tok.ipp.cas.cz'
sdas_port = 8888

cdb_host = 'codac2.tok.ipp.cas.cz'
cdb_user = 'CDB_exp'

eventUniqueId = '0x0000'

import socket
socket.setdefaulttimeout(10)  

sdas = SDASClient(sdas_host, sdas_port)
cdb = DAQClient.CDBDAQClient(host = cdb_host, user = cdb_user)

if sdas.searchMinEventNumber(): 
    print('Connection established.')

def merge_data(data_array):
    """Combine data that were stored in more parts to one dataset."""
    complete_data = Data(data_array[0])
    
    for i in range(1, len(data_array)):
        print(("Merging data part %d..." % (i + 1)))
        part = data_array[i]
        if (complete_data["tend"] != part["tstart"]):
            raise Exception("non-continuous data")
        complete_data["tend"] = part["tend"]
        complete_data.data = np.concatenate(( complete_data.data, part.data ))
    return complete_data

def convert_timestamp(timestamp):
    """ Converts timestamp from SDAS to python datetime """
    year = timestamp["date"]["year"]
    month = timestamp["date"]["month"] + 1 # 0-based in Java :-/
    day = timestamp["date"]["day"]

    hours = timestamp["time"]["hours"]
    minutes = timestamp["time"]["minutes"]
    seconds = timestamp["time"]["seconds"]
    micros = timestamp["time"]["millis"] * 1000 + timestamp["time"]["micros"]
    return datetime.datetime (year, month, day, hours, minutes, seconds, micros)

def get_channel(channel_name):
    """Read channel data (we need the numeric values)"""
    try:
        (nodeuniqueid, hardwareuniqueid, parameteruniqueid) = channel_name.split(".")
    except:
        raise Exception("Cannot split channel name `{}`.".format(channel_name))
    try:
        res = cdb.FS2CDB_ref(nodeuniqueid, hardwareuniqueid, parameteruniqueid)
    except:
        res = None
    return res

def timedelta_to_ms(start, stop):
    """ Returns time difference of two datatimes in milliseconds."""
    delta = stop - start
    timedelta = delta.seconds * 1000. + delta.microseconds / 1000. + delta.days * 86400000.
    # Fix wrong daylight savings (+- 1 or 2 hours)
    if timedelta > 7180000. and timedelta < 7220000.:
        timedelta -= 7200000.
    elif timedelta > 3580000. and timedelta < 3620000.:
        timedelta -= 3600000.
    elif timedelta > -7220000. and timedelta < -7180000.:
        timedelta += 7200000.
    if timedelta > -3620000. and timedelta < -3580000.:
        timedelta += 3600000.
    return timedelta

def write(gs_ref, channel, record_number, data):
    """Write one channel with properly pre-loaded generic signal"""
    computer_id = channel["computer_id"]
    board_id = channel["board_id"]
    channel_id = channel["channel_id"]

    event_timestamp = convert_timestamp(data["events"][0]["tstamp"])

    if cdb.get_signal_references(record_number=record_number, generic_signal_id=gs_ref["generic_signal_id"]):
        print(("Signal already exists: %s for shot %d, skipping..." % (gs_ref["generic_signal_name"], record_number)))
        return

    data_source_id = gs_ref["data_source_id"]

    data_start = convert_timestamp(data["tstart"])
    data_end = convert_timestamp(data["tend"])

    time0 = timedelta_to_ms(event_timestamp, data_start)
    
    data_length = timedelta_to_ms(data_start, data_end)
    samples = data.data.shape[0]

    time_coefficient = data_length / samples # in ms
    coefficient = 1.0

    if time0 < -2000.0 or time0 > 2000.0:
        # time0 = 0
        raise Exception("Invalid time 0: %d" % time0)

    if time_coefficient > 1.0 or time_coefficient < 1e-6:
        if computer_id in [ 1, 2, 5, 6 ]:
            time_coefficient = 0.0005
        else:
            raise Exception("Invalid time coefficient: %d" % time_coefficient)

    time_axis_id = gs_ref["time_axis_id"]

    data_file = cdb.new_data_file(gs_ref["generic_signal_name"], data_source_id = data_source_id, record_number = record_number)
    
    h5_file = h5py.File(data_file['full_path'],'w')
    h5_file.create_dataset(gs_ref["generic_signal_name"], data = data.data)
    h5_file.close()

    # Machine-dependent coefficients
    if time_axis_id == 1: # ATCA 1 or 2
        coefficient = 2.0 * 2.048 * 5.0306 / (2 ** 32)
        time_coefficient = 0.0005
    if "DTACQ216" in gs_ref["generic_signal_name"]: # 10V range used
        coefficient = 20.0 / (2 ** 16)

    # Store time axis (if there is one)
    if time_axis_id and not cdb.get_signal_references(record_number=record_number, generic_signal_id=time_axis_id):
        time_axis = cdb.get_generic_signal_references(generic_signal_id=time_axis_id)[0]
        if time_axis["signal_type"] == "LINEAR":
            cdb.store_signal(time_axis_id, record_number=record_number, coefficient=time_coefficient, note="Converted from SDAS")

    cdb.set_file_ready(data_file_id = data_file["data_file_id"])
    cdb.store_signal(
        gs_ref["generic_signal_id"], record_number = record_number, data_file_id = data_file["data_file_id"], data_file_key = gs_ref["generic_signal_name"], time0 = time0,
        computer_id = computer_id, board_id = board_id, channel_id = channel_id, note="Converted from SDAS",
        coefficient = coefficient
        )
    # write attributes

def fix_channel_name(channel_name):
    """ Return correct generic signal name.
    
    Channel names returned from SDAS use underscore instead of dots for
    separating node and hardware name.
    """
    if ('CFN_FAST_NODE' in channel_name):
        return channel_name.replace('_TR','.TR')
    elif ('MARTE_NODE' in channel_name):
        return channel_name.replace('MARTE_NODE_','MARTE_NODE.')        
    elif ('PCIE_ATCA_ADC_03_FPGA' in channel_name):
        return channel_name.replace('_FPGA', '.FPGA')
    elif ('PCIE_ATCA_ADC' in channel_name):
        return channel_name.replace('_BOARD','.BOARD')
    elif ('PCI_EPN_TIMING' in channel_name): # Not used
        return channel_name.replace('_BOARD','.BOARD')    
    elif ('DTACQ216_' in channel_name):
        return channel_name.replace('_BOARD','.BOARD')   
    elif 'TEST_JAVA_' in channel_name:
        return channel_name.replace('_HW', '.HW') 

def create_channel(channel, ignore_existing=False):
    """Create a channel based on its generic_signal name.

    The name has to consist of three dot separated values.
    At least one channel for the same computer and board has to exist.
    data_source_id and time_axis_id are taken from a first non-linear channel of the board.

    This basicly applies to MARTe channels, for others the occurence 
    is at least suspicious.
    """
    (nodeuniqueid, hardwareuniqueid, parameteruniqueid) = channel.split(".")
    generic_signals = cdb.get_generic_signal_references(generic_signal_name = channel)
    if len(generic_signals) > 0:
        if ignore_existing:
            cdb.create_DAQ_channels(nodeuniqueid, hardwareuniqueid, parameteruniqueid, generic_signal_id=generic_signals[0]["generic_signal_id"])
            return
        else:
            raise Exception("Generic signal exists: %s" % channel)

    from_same_board = cdb.find_generic_signals("%s.%s" % (nodeuniqueid, hardwareuniqueid) )       
    try:
        template = [c for c in from_same_board if ( c["signal_type"] == "FILE" and c["time_axis_id"] )] [0]
        time_axis_id = template["time_axis_id"]
        data_source_id = template["data_source_id"]
    except:
        raise Exception("Cannot find good template (channel from the same board) for {}.".format(channel))

    cdb.create_DAQ_channels(nodeuniqueid, hardwareuniqueid, parameteruniqueid, data_source_id, time_axis_id=time_axis_id)

    print(("Channel created: %s" % channel))

def create_channels(list_of_channels, ignore_existing=False):
    """Create a set of channels.

    See create_channel.
    """
    for channel in list_of_channels:
        create_channel(channel, ignore_existing)

def get_channel_names(shot_number):
    """Read all channel names for a shot."""
    channel_names = set(sdas.searchDataByEvent(eventUniqueId, shot_number))
    return [ fix_channel_name(name) for name in channel_names ]

# CDB caching
cached_gs = {}
cached_channels = {}

def get_channel_and_generic_signal(channel_name):
    """Reads channel and generic signal for a channel name from SDAS.

    Uses caching mechanism.
    """
    if not channel_name in cached_channels:
        channel = get_channel(channel_name)
        if not channel:
            channel = None
        else:
            cached_channels[channel_name] = channel
    else:
        channel = cached_channels[channel_name]

    if not channel_name in cached_gs:
        gs_refs = cdb.get_generic_signal_references(generic_signal_name = channel_name)
    
        if not len(gs_refs) == 1: 
            gs = None
        else:
            cached_gs[channel_name] = gs_refs[0]
            gs = gs_refs[0]
    else:
        gs = cached_gs[channel_name]

    return (channel, gs)

def get_channels_and_generic_signals(shot_number):
    """Read all channel names, channels and generic signals for the shot."""
    global cached_gs
    global cached_channels

    generic_signals = {}
    channels = {}

    missing_signals = []
    missing_channels = []

    channel_names = get_channel_names(shot_number)

    for channel_name in channel_names:
        if not channel_name:
            missing_channels.append("None")
            missing_signals.append("None")
            continue

        (channel, gs) = get_channel_and_generic_signal(channel_name)
        if not gs:
            missing_signals.append(channel_name)
        generic_signals[channel_name] = gs

        if not channel:
            missing_channels.append(channel_name)
        channels[channel_name] = channel

        if not sdas.parameterExists(channel_name, eventUniqueId, shot_number):
            raise Exception("Data don't exist for: %s" % channel_name)

        sys.stdout.flush()
    if len(missing_signals) or len(missing_channels):
        return { "error" : True, "missing_signals" : missing_signals, "missing_channels" : missing_channels, "channel_count" : len(channel_names) }
    else:
        return { "error" : None, "channels" : channels, "generic_signals" : generic_signals, "channel_names" : channel_names, "channel_count" : len(channel_names)}

def report_errors(conflict_data):
    """Print conflict details."""
    missing_signals = conflict_data["missing_signals"]
    missing_channels = conflict_data["missing_channels"]

    print("Not consistent")
    if missing_signals:
        print("Missing generic signals:")
        print(missing_signals)
    if missing_channels:
        print("Missing channels:")
        print(missing_channels)
    print("You can create channels using create_channels() function if you know what you are doing.")

def check_shot(shot_number):
    """Check if data for shot can be converted.

    If not, print a list of conflicts.
    """
    data = get_channels_and_generic_signals(shot_number)
    print(("{} channels found.".format(data["channel_count"])))
    if (data["error"]):
        report_errors(data)
        return False
    else:
        return True

def check_range(shots):
    ok = True
    for shot in shots:
        print(("Checking {}".format(shot)))
        if not check_shot(shot):
            ok = False
        else:
            print("Ok")
    return ok

def convert_signal(shot_number, channel_name, channel=None, generic_signal=None):
    global sdas
    global cdb
    
    if channel == None:
        channel = get_channel(channel_name)
    if generic_signal == None:
        generic_signal = cdb.get_generic_signal_references(generic_signal_name = channel_name)[0]
    if cdb.get_signal_references(record_number=shot_number, generic_signal_id=generic_signal["generic_signal_id"]):
        print(("Signal already exists: %s for shot %d" % (generic_signal["generic_signal_name"], shot_number)))
        return

    # Give SDAS three attempts to return three same data sets
    # and write them correctly to CDB.
    checks = 3
    for j in range(checks):
        sleep_length = .3 * 2 ** j # Interval to wait before trying to reconnect
        try:
            data_arrays = []
            for i in range(checks):
                try:
                    data_array = sdas.getData(channel_name, eventUniqueId, shot_number)
                except: # Try reloading SDAS client
                    print(("Reconnecting in {} seconds...".format(sleep_length)))
                    time.sleep(sleep_length)
                    sdas = SDASClient(sdas_host, sdas_port)
                    data_array = sdas.getData(channel_name, eventUniqueId, shot_number)
                data_arrays.append(data_array)
            if data_arrays[0][0].data[0] == data_arrays[1][0].data[0] and data_arrays[0][0].data[0] == data_arrays[2][0].data[0]:
                data = merge_data(data_arrays[0])
                try:
                    write(generic_signal, channel, shot_number, data)
                except: # Try reloading CDB
                    print(("Reconnecting in {} seconds...".format(sleep_length)))
                    cdb = DAQClient.CDBDAQClient(host = cdb_host, user = cdb_user)
                    time.sleep(sleep_length)
                    write(generic_signal, channel, shot_number, data)
                break
            else:
                raise Exception("Inconsistency in signal: " + channel_name)
        except Exception as ex:
            print(("Error when getting signal {}:{}, attempt {} of {}".format(channel_name, shot_number, j+1, checks)))
            if j == checks - 1:
                with open("errors.txt", "a") as error_file:
                    error_file.write("{}:{} - {}\n".format(channel_name, shot_number, ex))
                    # raise ex
            else:
                continue
    return True

def create_shot(record_number):
    """Create record row in the DB.

    """
    event = sdas.searchEventsByEventNumber(record_number)[0]
    event_timestamp = convert_timestamp(event['tstamp'])
    if not cdb.record_exists(record_number = record_number):
        print(("Creating record %d" % record_number))
        cdb.create_record(record_number = record_number, record_time = event_timestamp)    

def convert(shots):
    """Copy all signals for several records.
    
    :param shot: can be either int or a sequence of int's.
    
    Presence of generic signals and channels in CDB is checked before any writing.

    SDAS client fails from time to time, so we wait till three reads one after another
    return same values (and reasonable values).
    """

    if (type(shots) == int):
        shots = [ shots ]
    for shot_number in shots:
        print((time.ctime())) # 'Mon Oct 18 13:35:29 2010'
        print(("\n\nShot %d" % shot_number))
        print("---------")
       
        data = get_channels_and_generic_signals(shot_number)

        if (data["error"]):
            report_errors(data)
            with open("errors.txt", "a") as error_file:
                error_file.write("Inconsistent data for shot {}".format(shot_number))
        else:
            try:
                create_shot(shot_number)
                generic_signals = data["generic_signals"]
                channels = data["channels"]
                channel_names = data["channel_names"]
                print("\nConsistent :-)")

                converted = 0
                for channel_name in channel_names:
                    converted += 1
                    # Timing gives no data
                    # ATCA3 can't be read for some reason
                    if 'PCI_EPN_TIMING' in channel_name or 'PCIE_ATCA_ADC_03' in channel_name:
                        print(("Skipping %s..." % channel_name))
                        continue
                    print(("%s (%d/%d in %d)..." % ( channel_name, converted, len(channel_names), shot_number )))
                    convert_signal(shot_number, channel_name, channels[channel_name], generic_signals[channel_name])
                    sys.stdout.flush()

                print(("Converted {} channels for shot {}".format(converted, shot_number)))
            except Exception as ex:
                with open("errors.txt", "a") as error_file:
                    error_file.write("Shot:{} - {}\n".format(shot_number, ex))
                    continue

def find_missing_channels(shot_range, verbose=False):
    missing_channels = []
    for shot in shot_range:
        if verbose:
            print(("Checking {}".format(shot)))
        data = get_channels_and_generic_signals(shot)
        if data["error"]:
            for channel in data["missing_channels"]:
                if not channel in missing_channels:
                    if verbose:
                        print(channel)
                    missing_channels.append(channel)
    return sorted(missing_channels)
