#!/usr/bin/env python
import pysdas
# from convert_old_db import *

from pyCDB import DAQClient
cdb_host = 'codac2.tok.ipp.cas.cz'
cdb_user = 'CDB_exp'
cdb = DAQClient.CDBDAQClient(host = cdb_host, user = cdb_user)

def write(signal, accept_wrong_time=False):
    '''Write one signal obtained from pysdas.
    
    Returns True if something was written, False otherwise.
    '''
    record_number = signal.event_id
    gs_name = ".".join(signal.FS_id)

    try:
        if not signal.is_complete():
            print("Signal not complete: %s for shot %d, skipping..." % (gs_name, record_number))
            return False

        if signal.is_empty():
            print("Signal is empty: %s for shot %d, skipping..." % (gs_name, record_number))
            return False

        if not signal.length:
            print("Signal has zero length: %s for shot %d, skipping..." % (gs_name, record_number))
            return False

    except Exception as ex:
        print(ex)
        return False

    if not cdb.record_exists(record_number):
        print("Creating record %d" % record_number)
        event = pysdas.get_event(record_number)
        cdb.create_record(record_number=record_number, record_time=event.tstart)

    gs_ref = cdb.get_generic_signal_references(generic_signal_name=gs_name)[0]

    if cdb.get_signal_references(record_number=record_number, generic_signal_id=gs_ref["generic_signal_id"], revision=-1):
        print("Signal already exists: %s for shot %d, skipping..." % (gs_ref["generic_signal_name"], record_number))
        return False

    channel = cdb.FS2CDB_ref(*signal.FS_id)

    computer_id = channel["computer_id"]
    board_id = channel["board_id"]
    channel_id = channel["channel_id"]

    coefficient = 1.0
    time0 = signal.time0
    time_coefficient = signal.time_coefficient

    # print record_number, time_coefficient

    data_source_id = gs_ref["data_source_id"]
    
    if time0 < -2000.0 or time0 > 2000.0:
        # time0 = 0
        attempts = (time0 + 3600000, time0 - 3600000)
        if accept_wrong_time:
            print("Invalid time 0: %d" % time0)
        else:
            raise Exception("Invalid time 0: %d" % time0)

    if time_coefficient > 1.0 or time_coefficient < 1e-7:
        if accept_wrong_time:
            print("Invalid time coefficient for %s: %d" % (gs_name, time_coefficient))
        else:
            raise Exception("Invalid time coefficient for %s: %d" % (gs_name, time_coefficient))

    time_axis_id = gs_ref["time_axis_id"]

    data_file = cdb.new_data_file(gs_ref["generic_signal_name"], data_source_id = data_source_id, record_number = record_number)

    try:
        signal.write_hdf5(data_file['full_path'], gs_ref["generic_signal_name"])
        cdb.set_file_ready(data_file_id = data_file["data_file_id"])
    except IOError:
        print("Error writing to %s." % data_file['full_path'])
        raise

    # Machine-dependent coefficients
    if computer_id in (1, 2): # ATCA 1 or 2
        coefficient = 2.0 * 2.048 * 5.0306 / (2 ** 32)
        time_coefficient = 0.0005
    elif computer_id == 216: # 10V range used
        coefficient = 20.0 / (2 ** 16)
    elif computer_id == 3:
        coefficient = 0.00013427734375

    # Store time axis (if there is one)
    if time_axis_id and not cdb.get_signal_references(record_number=record_number, generic_signal_id=time_axis_id, revision=-1):
        time_axis = cdb.get_generic_signal_references(generic_signal_id=time_axis_id)[0]
        print("Writing time axis: %s" + time_axis["generic_signal_name"])
        if time_axis["signal_type"] == "LINEAR":
            cdb.store_signal(time_axis_id, record_number=record_number, coefficient=time_coefficient, note="Converted from SDAS [copy_atca.py]")

    cdb.store_signal(
        gs_ref["generic_signal_id"], record_number = record_number, data_file_id = data_file["data_file_id"], data_file_key = gs_ref["generic_signal_name"], time0 = time0,
        computer_id = computer_id, board_id = board_id, channel_id = channel_id, note="Converted from SDAS",
        coefficient = coefficient
        )
    print("Signal successfully written: %s for shot %d." % (gs_ref["generic_signal_name"], record_number))

    return True