#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Create SI units in the database.

Can be run safely, as only units not present are inserted.
"""

from pyCDB.client import CDBClient
from pyCDB import units

import os

def run():
    if os.getenv("CDB_USER", None) != "root":
        print("Requires DB write privileges.")
        return

    cdb = CDBClient()
    si = cdb.get_unit_system("SI")
    if si:
        si_id = si["unit_system_id"]
    else:
        si_id = cdb.insert('unit_systems', {'unit_system_name': 'SI', 'editable' : False}, return_inserted_id=True)

    def add_si_unit(name):
        dims = units.get_dimension(name)
        try:
            cdb.add_unit(si_id, name, **dims)
            print(("Unit `%s` added." % name))
        except:
            print(("Unit `%s` not added, probably exists." % name))

    # Basic units
    add_si_unit("m")
    add_si_unit("kg")
    add_si_unit("s")
    add_si_unit("A")
    add_si_unit("K")
    add_si_unit("mol")
    add_si_unit("cd")

    # Derived
    add_si_unit("Hz")
    add_si_unit("N")
    add_si_unit("Pa")
    add_si_unit("J")
    add_si_unit("W")
    add_si_unit("C")
    add_si_unit("V")
    add_si_unit("F")
    add_si_unit("S")
    add_si_unit("Wb")
    add_si_unit("T")
    add_si_unit("H")
    add_si_unit("ohm")

    # Combined
    add_si_unit("V/m")
    add_si_unit("J/kg")
    add_si_unit("J/mol")

    # PROBLEMS
    # add_si_unit(u"Ω")    # Parse error in pint
    # add_si_unit("lm")    # Unknown
    # add_si_unit("lx")    # Unknown

if __name__ == "__main__":
    run()



