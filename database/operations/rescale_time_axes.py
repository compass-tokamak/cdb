from pyCDB import client
cdb = client.CDBClient()

records = [ 5899, 5909, 5943 ] 
computers = [ 1, 2, 216 ]

t0 = {}
t1 = {}

t0[0] = 930
t1[0] = 1230

t0[1] = 930.2785
t1[1] = t0[1] + 300.086

t0[2] = 930.0330
t1[2] = t0[2] + 300.0095

t0[216] = 929.99997
t1[216] = t0[216] + 299.98695

note = "Time axis corrected according to spring 2013 calibration."

def corrected_time(computer, time):
    return t0[0] + (( time - t0[computer] ) / ( t1[computer] - t0[computer] )) * (t1[0] - t0[0])
    
def corrected_coefficient(computer, coefficient):
    return coefficient * (t1[0] - t0[0]) / (t1[computer] - t0[computer])

def update_signals(computer, record, revision):
    if not computer in computers:
        raise Exception("Time rescaling not enabled for computer_id={}".format(computer))
    signals = cdb.get_signal_references(computer_id=computer, record_number=record, revision=revision)
    gss = { signal["generic_signal_id"] : cdb.get_generic_signal_references(generic_signal_id=signal["generic_signal_id"])[0] for signal in signals }
    axes_ids = { gs["time_axis_id"] for gs in list(gss.values()) }
    axis_revs = {}
    
    for axis_id in axes_ids:
        if not axis_id:
            continue
        axis_signal = cdb.get_signal_references(record_number=record, generic_signal_id=axis_id)[0]
        if not (axis_signal["note"] and note in axis_signal["note"]):
            new_coefficient = corrected_coefficient(computer, axis_signal["coefficient"])
            new_axis = cdb.update_signal(generic_signal_id=axis_id,
                                    record_number=record,
                                    coefficient=new_coefficient,
                                    note=note
            )[0]
            axis_revs[axis_id] = new_axis["revision"]
        else:
            axis_revs[axis_id] = axis_signal["revision"]
            
    for signal in signals:
        if not (signal["note"] and note in signal["note"]):
            if signal["time_axis_id"]:
                time_axis_id = signal["time_axis_id"]
            else:
                gs = gss.get(signal["generic_signal_id"])
                time_axis_id = gs["time_axis_id"] 
            if not time_axis_id:
                continue
            axis_rev = axis_revs[time_axis_id]
            new_time0 = corrected_time(computer, signal["time0"])
            cdb.update_signal(generic_signal_id=signal["generic_signal_id"],
                              record_number=record,
                              time0=new_time0,
                              time_axis_revision=axis_rev,
                              note=note)
        

