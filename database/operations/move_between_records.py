import shutil
import copy
from pyCDB import client

def move(old_record, new_record=None, computer_id=None, signals=None, delete=True, note=None):
    '''Move data that were stored in a wrong shot.

    :param old_record: 
    :param new_record: The shot to which the data should belong. 
    :param computer_id: 
    :param signals: A list of 
    :param delete: Delete signals from the old shot.
    :param note: Optional note describing the reason (Reasonable default is provided.)

    Signals can be provided as a list (of references) or as a combination computer_id/shot.
    If new_record is not present, the data are just deleted.

    Does not take time_axis into account!
    '''
    cdb = client.CDBClient()
    if signals is None and computer_id:
        signals = cdb.get_signal_references(record_number=old_record, computer_id=computer_id, revision=-1)

    for signal in signals:
        data_file = cdb.get_data_file_reference(data_file_id=signal["data_file_id"])
        old_path = data_file["full_path"]
        
        # create new signal\n",
        ds_kwargs = copy.copy(signal)
        ds_kwargs["record_number"] = new_record
        if note:
            ds_kwargs["note"] = note
        elif new_record:
            ds_kwargs["note"] = "Data moved from shot {} to shot {}".format(old_record, new_record)
        else:
            ds_kwargs["note"] = "Data deleted from shot {}".format(old_record)
        
        if new_record:                # If None, just delete
            # create new data file
            df_kwargs = {}
            df_kwargs["record_number"] = new_record
            df_kwargs["collection_name"] = data_file["collection_name"]
            df_kwargs["data_source_id"] = data_file["data_source_id"]
            df_kwargs["file_format"] = data_file["data_format"]
            
            new_df = cdb.new_data_file(**df_kwargs)
            new_path = new_df["full_path"]
            shutil.copy(old_path, new_path)
            cdb.set_file_ready(new_df["data_file_id"])

            ds_kwargs["data_file_id"] = new_df["data_file_id"]
            cdb.store_signal(**ds_kwargs)
            
        if not signal["deleted"]:     # Just to make sure
            cdb.delete_signal(generic_signal_id=signal["generic_signal_id"], record_number=old_record, note=ds_kwargs["note"])