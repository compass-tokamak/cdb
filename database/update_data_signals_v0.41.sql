SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0; SET
@OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0; SET
@OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `CDB`.`channel_setup` ADD COLUMN `time_axis_id` INT(11) NULL
DEFAULT NULL  AFTER `coefficient_V2unit` , DROP FOREIGN KEY
`fk_physical_setup_generic_signals1` ;

ALTER TABLE `CDB`.`channel_setup`    ADD CONSTRAINT
`fk_physical_setup_generic_signals1`   FOREIGN KEY
(`attached_generic_signal_id` )   REFERENCES `CDB`.`generic_signals`
(`generic_signal_id` )   ON DELETE NO ACTION   ON UPDATE NO ACTION;

ALTER TABLE `CDB`.`da_computers` ADD COLUMN `computer_name` VARCHAR(45) NOT
NULL  AFTER `computer_id`  , ADD INDEX `computer_name_idx` (`computer_name`
ASC) ;

ALTER TABLE `CDB`.`data_signals` CHANGE COLUMN `coefficient` `coefficient`
DOUBLE NULL DEFAULT '1' COMMENT 'corresponds to coefficient_lev2V in
channel_setup'  , ADD COLUMN `coefficient_V2unit` DOUBLE NULL DEFAULT '1'
AFTER `offset` , ADD COLUMN `time_axis_id` INT(11) NULL DEFAULT NULL COMMENT
'time_axis_id is set when different to the generic signal, e.g. when connected
to a different DAQ\n'  AFTER `coefficient_V2unit` , ADD COLUMN `data_quality`
ENUM('UNKNOWN','POOR','GOOD','VALIDATED') NOT NULL DEFAULT 'UNKNOWN'  AFTER
`channel_id` , ADD COLUMN `deleted` TINYINT(1) NOT NULL DEFAULT 0  AFTER
`data_quality` ;



SET SQL_MODE=@OLD_SQL_MODE; SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
