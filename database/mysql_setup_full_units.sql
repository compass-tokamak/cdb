-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 05, 2012 at 01:58 PM
-- Server version: 5.5.23
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `CDB`
--
DROP SCHEMA IF EXISTS `CDB`;
CREATE SCHEMA `CDB` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

GRANT SELECT , INSERT ON  `CDB` . * TO  'CDB'@'%' IDENTIFIED BY  'cmpsSQLdata';
GRANT SELECT , INSERT ON  `CDB` . * TO  'CDB'@'localhost' IDENTIFIED BY  'cmpsSQLdata';

USE `CDB`;

-- --------------------------------------------------------
--
-- Stand-in structure for view `channel_attachments`
--
CREATE TABLE `channel_attachments` (
`computer_id` smallint(6)
,`board_id` smallint(6)
,`channel_id` smallint(6)
,`default_generic_signal_id` int(11)
,`attached_generic_signal_id` int(11)
,`attach_time` datetime
,`offset` double
,`coefficient_lev2V` double
,`coefficient_V2unit` double
,`uid` int(11)
,`note` varchar(140)
,`attached_signal_name` varchar(100)
,`is_attached` int(1)
);
-- --------------------------------------------------------

--
-- Table structure for table `channel_setup`
--

CREATE TABLE `channel_setup` (
  `attached_generic_signal_id` int(11) NOT NULL,
  `attach_time` datetime NOT NULL,
  `computer_id` smallint(6) NOT NULL,
  `board_id` smallint(6) NOT NULL,
  `channel_id` smallint(6) NOT NULL,
  `uid` int(11) DEFAULT NULL COMMENT 'user id of the event creator',
  `note` varchar(140) DEFAULT NULL,
  `coefficient_lev2V` double DEFAULT '1' COMMENT 'conversion from channel level (raw) to Volts',
  `offset` double DEFAULT '0' COMMENT 'offset in channel level (raw) units',
  `coefficient_V2unit` double DEFAULT '1' COMMENT 'conversion factor from Volts to physical units (of the attached generic signal)',
  PRIMARY KEY (`attached_generic_signal_id`,`attach_time`),
  KEY `channel_id_index` (`computer_id`,`board_id`,`channel_id`),
  KEY `fk_channel_setup_1` (`computer_id`,`board_id`,`channel_id`),
  KEY `fk_physical_setup_generic_signals1` (`attached_generic_signal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Record and shot numbers. Includes data directories (relative';

-- --------------------------------------------------------

--
-- Table structure for table `DAQ_channels`
--

CREATE TABLE `DAQ_channels` (
  `computer_id` smallint(6) NOT NULL,
  `board_id` smallint(6) NOT NULL,
  `channel_id` smallint(6) NOT NULL,
  `default_generic_signal_id` int(11) NOT NULL,
  `note` varchar(140) DEFAULT NULL,
  `nodeuniqueid` varchar(45) DEFAULT NULL COMMENT 'FireSignal node id',
  `hardwareuniqueid` varchar(45) DEFAULT NULL COMMENT 'FireSignal hardware id',
  `parameteruniqueid` varchar(45) DEFAULT NULL COMMENT 'FireSignal parameter id ',
  PRIMARY KEY (`computer_id`,`board_id`,`channel_id`),
  UNIQUE KEY `generic_signal_id_UNIQUE` (`default_generic_signal_id`),
  UNIQUE KEY `FS_id_index` (`nodeuniqueid`,`hardwareuniqueid`,`parameteruniqueid`),
  KEY `fk_detach_ids_generic_signals1` (`default_generic_signal_id`),
  KEY `fk_physical_channels_1` (`computer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of A/D channels, identified by computer, board and chan';

-- --------------------------------------------------------

--
-- Table structure for table `data_files`
--

CREATE TABLE `data_files` (
  `data_file_id` int(11) NOT NULL AUTO_INCREMENT,
  `data_source_id` smallint(6) NOT NULL,
  `record_number` int(11) NOT NULL,
  `data_format` enum('HDF5','NETCDF4','GENERIC') NOT NULL,
  `collection_name` varchar(90) NOT NULL COMMENT 'filename = collection_name.revision.ext',
  `revision` smallint(6) NOT NULL DEFAULT '1',
  `file_name` varchar(100) NOT NULL,
  PRIMARY KEY (`data_file_id`),
  UNIQUE KEY `file_UNIQUE` (`record_number`,`data_source_id`,`collection_name`,`revision`),
  KEY `data_source_id` (`data_source_id`),
  KEY `shot_number` (`record_number`),
  KEY `fk_data_files_1` (`data_source_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of data files, i.e. files that are physically on disks.';

-- --------------------------------------------------------

--
-- Table structure for table `data_signals`
--

CREATE TABLE `data_signals` (
  `record_number` int(11) NOT NULL,
  `generic_signal_id` int(11) NOT NULL,
  `revision` smallint(6) NOT NULL DEFAULT '1',
  `timestamp` datetime NOT NULL,
  `data_file_id` int(11) DEFAULT NULL,
  `data_file_key` varchar(30) DEFAULT NULL COMMENT 'placement in the data file',
  `time0` double DEFAULT '0',
  `coefficient` double DEFAULT '1',
  `offset` double DEFAULT '0',
  `time_axis_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis1_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis2_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis3_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis4_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis5_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis6_revision` smallint(6) NOT NULL DEFAULT '1',
  `note` varchar(140) DEFAULT NULL,
  `computer_id` smallint(6) DEFAULT NULL,
  `board_id` smallint(6) DEFAULT NULL,
  `channel_id` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`record_number`,`generic_signal_id`,`revision`),
  KEY `fk_data_file_id` (`data_file_id`),
  KEY `fk_generic_channel_id` (`generic_signal_id`),
  KEY `fk_record_number` (`record_number`),
  KEY `fk_data_signals_1` (`computer_id`,`board_id`,`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Information of signals for particular records (instances of ';

-- --------------------------------------------------------

--
-- Table structure for table `data_sources`
--

CREATE TABLE `data_sources` (
  `data_source_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text,
  `subdirectory` varchar(45) NOT NULL,
  PRIMARY KEY (`data_source_id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `filename_base_UNIQUE` (`subdirectory`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='List of data sources (providers) and their descriptions.';

-- --------------------------------------------------------

--
-- Table structure for table `da_computers`
--

CREATE TABLE `da_computers` (
  `computer_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `location` varchar(45) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`computer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='List of data acquisition computers.';

-- --------------------------------------------------------

--
-- Table structure for table `file_status`
--

CREATE TABLE `file_status` (
  `data_file_id` int(11) NOT NULL,
  `file_ready` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`data_file_id`),
  KEY `fk_file_status_1` (`data_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Indicates whether file can be read (is ready).';

-- --------------------------------------------------------

--
-- Table structure for table `FireSignal_Event_IDs`
--

CREATE TABLE `FireSignal_Event_IDs` (
  `event_number` int(11) NOT NULL,
  `event_id` varchar(200) NOT NULL,
  `record_number` int(11) NOT NULL,
  PRIMARY KEY (`event_number`,`event_id`),
  UNIQUE KEY `record_number_UNIQUE` (`record_number`),
  KEY `fk_FireSignal_Event_IDs_1` (`record_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `generic_signals`
--

CREATE TABLE `generic_signals` (
  `generic_signal_id` int(11) NOT NULL AUTO_INCREMENT,
  `generic_signal_name` varchar(100) NOT NULL,
  `alias` varchar(30) DEFAULT NULL,
  `first_record_number` int(11) NOT NULL DEFAULT '1' COMMENT 'channel validity from first to last shot no.',
  `last_record_number` int(11) NOT NULL DEFAULT '-1' COMMENT '-1 for infinite validity',
  `data_source_id` smallint(6) NOT NULL,
  `time_axis_id` int(11) DEFAULT NULL,
  `axis1_id` int(11) DEFAULT NULL,
  `axis2_id` int(11) DEFAULT NULL,
  `axis3_id` int(11) DEFAULT NULL,
  `axis4_id` int(11) DEFAULT NULL,
  `axis5_id` int(11) DEFAULT NULL,
  `axis6_id` int(11) DEFAULT NULL,
  `physical_quantity_id` int(11) NOT NULL,
  `units_id` int(11) NOT NULL,
  `description` text,
  `signal_type` enum('FILE','LINEAR') DEFAULT 'FILE',
  PRIMARY KEY (`generic_signal_id`),
  UNIQUE KEY `idgeneric_channels_UNIQUE` (`generic_signal_id`),
  UNIQUE KEY `id_name_source_id` (`generic_signal_name`,`data_source_id`),
  UNIQUE KEY `alias_UNQ` (`alias`),
  KEY `fk_generic_signals_1` (`data_source_id`),
  KEY `fk_generic_signals_2` (`time_axis_id`),
  KEY `fk_generic_signals_3` (`axis1_id`),
  KEY `fk_generic_signals_4` (`axis2_id`),
  KEY `fk_generic_signals_5` (`axis3_id`),
  KEY `fk_generic_signals_6` (`axis4_id`),
  KEY `fk_generic_signals_7` (`axis5_id`),
  KEY `fk_generic_signals_8` (`axis6_id`),
  KEY `fk_generic_signals_9` (`units_id`),
  KEY `fk_generic_signals_10` (`physical_quantity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='List of signal names, i.e. physical quantities, with units a';

-- --------------------------------------------------------

--
-- Stand-in structure for view `last_attach_time`
--
CREATE TABLE `last_attach_time` (
`computer_id` smallint(6)
,`board_id` smallint(6)
,`channel_id` smallint(6)
,`attach_time` datetime
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `last_channel_attach`
--
CREATE TABLE `last_channel_attach` (
`computer_id` smallint(6)
,`board_id` smallint(6)
,`channel_id` smallint(6)
,`attach_time` datetime
,`attached_generic_signal_id` int(11)
,`offset` double
,`coefficient_lev2V` double
,`coefficient_V2unit` double
,`uid` int(11)
,`note` varchar(140)
);
-- --------------------------------------------------------

--
-- Table structure for table `physical_quantities`
--

CREATE TABLE `physical_quantities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` text,
  `units_SI` int(11) DEFAULT NULL,
  `units_CGS` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `fk_physical_quantities_1` (`units_SI`),
  KEY `fk_physical_quantities_2` (`units_CGS`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `record_directories`
--

CREATE TABLE `record_directories` (
  `record_number` int(11) NOT NULL,
  `data_directory` varchar(100) NOT NULL,
  PRIMARY KEY (`record_number`),
  UNIQUE KEY `record_number_UNIQUE` (`record_number`),
  UNIQUE KEY `data_directory_UNIQUE` (`data_directory`),
  KEY `fk_record_directories_1` (`record_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `shot_database`
--

CREATE TABLE `shot_database` (
  `record_number` int(11) NOT NULL,
  `record_time` datetime NOT NULL,
  `record_type` enum('EXP','VOID','MODEL') NOT NULL,
  `description` text,
  PRIMARY KEY (`record_number`),
  UNIQUE KEY `record_number_UNIQUE` (`record_number`),
  KEY `shot_time_INDEX` (`record_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `plain` varchar(45) DEFAULT NULL COMMENT 'plain text representation',
  `latex` varchar(45) DEFAULT NULL COMMENT 'latex formatted',
  `coef_SI` double DEFAULT NULL COMMENT 'conversion coefficient to SI units',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure for view `channel_attachments`
--
DROP TABLE IF EXISTS `channel_attachments`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `channel_attachments` AS select `DAQ_channels`.`computer_id` AS `computer_id`,`DAQ_channels`.`board_id` AS `board_id`,`DAQ_channels`.`channel_id` AS `channel_id`,`DAQ_channels`.`default_generic_signal_id` AS `default_generic_signal_id`,`last_channel_attach`.`attached_generic_signal_id` AS `attached_generic_signal_id`,`last_channel_attach`.`attach_time` AS `attach_time`,`last_channel_attach`.`offset` AS `offset`,`last_channel_attach`.`coefficient_lev2V` AS `coefficient_lev2V`,`last_channel_attach`.`coefficient_V2unit` AS `coefficient_V2unit`,`last_channel_attach`.`uid` AS `uid`,`last_channel_attach`.`note` AS `note`,`generic_signals`.`generic_signal_name` AS `attached_signal_name`,(`DAQ_channels`.`default_generic_signal_id` <> `last_channel_attach`.`attached_generic_signal_id`) AS `is_attached` from ((`DAQ_channels` join `last_channel_attach`) join `generic_signals`) where ((`DAQ_channels`.`computer_id` = `last_channel_attach`.`computer_id`) and (`DAQ_channels`.`board_id` = `last_channel_attach`.`board_id`) and (`DAQ_channels`.`channel_id` = `last_channel_attach`.`channel_id`) and (`generic_signals`.`generic_signal_id` = `last_channel_attach`.`attached_generic_signal_id`));

-- --------------------------------------------------------

--
-- Structure for view `last_attach_time`
--
DROP TABLE IF EXISTS `last_attach_time`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `last_attach_time` AS select `channel_setup`.`computer_id` AS `computer_id`,`channel_setup`.`board_id` AS `board_id`,`channel_setup`.`channel_id` AS `channel_id`,max(`channel_setup`.`attach_time`) AS `attach_time` from `channel_setup` group by `channel_setup`.`computer_id`,`channel_setup`.`board_id`,`channel_setup`.`channel_id`;

-- --------------------------------------------------------

--
-- Structure for view `last_channel_attach`
--
DROP TABLE IF EXISTS `last_channel_attach`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `last_channel_attach` AS select `channel_setup`.`computer_id` AS `computer_id`,`channel_setup`.`board_id` AS `board_id`,`channel_setup`.`channel_id` AS `channel_id`,`channel_setup`.`attach_time` AS `attach_time`,`channel_setup`.`attached_generic_signal_id` AS `attached_generic_signal_id`,`channel_setup`.`offset` AS `offset`,`channel_setup`.`coefficient_lev2V` AS `coefficient_lev2V`,`channel_setup`.`coefficient_V2unit` AS `coefficient_V2unit`,`channel_setup`.`uid` AS `uid`,`channel_setup`.`note` AS `note` from (`channel_setup` join `last_attach_time` on(((`channel_setup`.`computer_id` = `last_attach_time`.`computer_id`) and (`channel_setup`.`board_id` = `last_attach_time`.`board_id`) and (`channel_setup`.`channel_id` = `last_attach_time`.`channel_id`) and (`channel_setup`.`attach_time` = `last_attach_time`.`attach_time`))));

--
-- Constraints for dumped tables
--

--
-- Constraints for table `channel_setup`
--
ALTER TABLE `channel_setup`
  ADD CONSTRAINT `fk_channel_setup_1` FOREIGN KEY (`computer_id`, `board_id`, `channel_id`) REFERENCES `DAQ_channels` (`computer_id`, `board_id`, `channel_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_physical_setup_generic_signals1` FOREIGN KEY (`attached_generic_signal_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `DAQ_channels`
--
ALTER TABLE `DAQ_channels`
  ADD CONSTRAINT `fk_detach_ids_generic_signals1` FOREIGN KEY (`default_generic_signal_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_physical_channels_1` FOREIGN KEY (`computer_id`) REFERENCES `da_computers` (`computer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `data_files`
--
ALTER TABLE `data_files`
  ADD CONSTRAINT `fk_data_files_1` FOREIGN KEY (`data_source_id`) REFERENCES `data_sources` (`data_source_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `shot_number` FOREIGN KEY (`record_number`) REFERENCES `shot_database` (`record_number`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `data_signals`
--
ALTER TABLE `data_signals`
  ADD CONSTRAINT `fk_data_file_id` FOREIGN KEY (`data_file_id`) REFERENCES `data_files` (`data_file_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_data_signals_1` FOREIGN KEY (`computer_id`, `board_id`, `channel_id`) REFERENCES `DAQ_channels` (`computer_id`, `board_id`, `channel_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_generic_channel_id` FOREIGN KEY (`generic_signal_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_record_number` FOREIGN KEY (`record_number`) REFERENCES `shot_database` (`record_number`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `file_status`
--
ALTER TABLE `file_status`
  ADD CONSTRAINT `fk_file_status_1` FOREIGN KEY (`data_file_id`) REFERENCES `data_files` (`data_file_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `FireSignal_Event_IDs`
--
ALTER TABLE `FireSignal_Event_IDs`
  ADD CONSTRAINT `fk_FireSignal_Event_IDs_1` FOREIGN KEY (`record_number`) REFERENCES `shot_database` (`record_number`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `generic_signals`
--
ALTER TABLE `generic_signals`
  ADD CONSTRAINT `fk_generic_signals_9` FOREIGN KEY (`units_id`) REFERENCES `units` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_generic_signals_10` FOREIGN KEY (`physical_quantity_id`) REFERENCES `physical_quantities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_generic_signals_1` FOREIGN KEY (`data_source_id`) REFERENCES `data_sources` (`data_source_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_generic_signals_2` FOREIGN KEY (`time_axis_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_generic_signals_3` FOREIGN KEY (`axis1_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_generic_signals_4` FOREIGN KEY (`axis2_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_generic_signals_5` FOREIGN KEY (`axis3_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_generic_signals_6` FOREIGN KEY (`axis4_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_generic_signals_7` FOREIGN KEY (`axis5_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_generic_signals_8` FOREIGN KEY (`axis6_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `physical_quantities`
--
ALTER TABLE `physical_quantities`
  ADD CONSTRAINT `fk_physical_quantities_1` FOREIGN KEY (`units_SI`) REFERENCES `units` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_physical_quantities_2` FOREIGN KEY (`units_CGS`) REFERENCES `units` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `record_directories`
--
ALTER TABLE `record_directories`
  ADD CONSTRAINT `fk_record_directories_1` FOREIGN KEY (`record_number`) REFERENCES `shot_database` (`record_number`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
