#!/usr/bin/env python

from setuptools import setup
import os

this_dir = os.path.dirname(__file__)
with open(os.path.join(this_dir, 'README.md'), encoding='utf8') as f:
    long_description = f.read()

with open(os.path.join(this_dir, 'requirements.txt'), encoding='utf8') as f:
    install_requires = [r for r in (r.strip() for r in f.readlines()) if r != '' and not r.startswith('-r')]

version = '1.0.0'
ci_commit_tag = os.environ.get('CI_COMMIT_TAG')
ci_pipeline_id = os.environ.get('CI_PIPELINE_ID')
if ci_commit_tag is not None:
    assert ci_commit_tag.endswith(version), 'Version does not match tag {}: update the version first.'.format(version, ci_commit_tag)
elif ci_pipeline_id is not None:
    version = '{}.post{}'.format(version, ci_pipeline_id)
else:
    version = '{}+local'.format(version)

setup(name='pycdb-compass',
      version=version,
      description='IPP-CZ CDB Client',
      long_description=long_description,
      long_description_content_type='text/markdown',
      author='Czech Acad Sci, Inst Plasma Phys',
      author_email='software@ipp.cas.cz',
      url='https://bitbucket.org/compass-tokamak/cdb',
      license_files=['LICENSE'],
      install_requires=install_requires,
      packages=['pyCDB', 'pyCDB.libpostproc'],
      package_dir={'': 'src'},
      package_data={
          '': [ os.path.join(this_dir, 'requirements.txt') ]
      }
)
