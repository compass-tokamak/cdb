#!/usr/bin/env python
# Simple mailer to send warnings from python

import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import cdb_cache
import logging
import os

mail_from = "itcrowd-warning@tok.ipp.cas.cz"
mail_to = "itcrowd-warning@tok.ipp.cas.cz"
#mail_to = "fridrich@ipp.cas.cz"

def send_mail(from_mail, to_mail, subject, text, attachments=[]):
    """Simple message sender"""

    # Create the container (outer) email message.
    msg = MIMEMultipart()
    msg['Subject'] = subject
    # me == the sender's email address
    # family = the list of all recipients' email addresses
    msg['From'] = from_mail
    msg['To'] = to_mail
    content = MIMEText(text, 'plain')
    msg.attach(content)
    # Attach file list
    if attachments:
        for aname in attachments:
            with open(aname) as fp:
                attachment = MIMEText(fp.read())
                attachment.add_header(
                    'Content-Disposition', 'attachment', filename=os.path.basename(aname))
                msg.attach(attachment)

    # Send the email via our own SMTP server.
    s = smtplib.SMTP('mail.tok.ipp.cas.cz')
    s.sendmail(from_mail, to_mail, msg.as_string())
    s.quit()

if __name__ == "__main__":
    # Determine free space
    import subprocess
    df = subprocess.Popen("df -h".split(), stdout=subprocess.PIPE)
    df_output = df.communicate()[0]
    space = "NaN"
    for line in df_output.split("\n"):
        line_split = line.split()
        if line:
            if line_split[-1] == "/data":
                space = line_split[3]
                break
    
    # The low space warning is sent here:
    BODY = "There is only %s of free space in CDB cache.\n\nOutput of \"df -h\":\n\n" %space
    BODY += df_output
    SUBJ = 'CDB cache out of space'
    logging.warning(BODY)
    send_mail(mail_from, mail_to, SUBJ, BODY)
