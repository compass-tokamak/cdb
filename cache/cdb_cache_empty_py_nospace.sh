#!/bin/bash

# free space in MB
FREE=`df -Pm /data/ | awk '/[0-9]%/{print $(NF-2)}'`
if [[ $FREE -lt 51200 ]]; then
  if [[ $FREE -lt 25600 ]]; then
    echo 'cache space critically low'
    /root/bin/cdb_cache_lowspace.py
  else
    echo 'low cache space'
    /root/bin/mailer.py
  fi
fi
