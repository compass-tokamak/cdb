# Common methods for cdb_cache_copy and cdb_cache_empty
# proper version
import os
import subprocess
import stat
import errno
import logging
import logging.handlers
import warnings
warnings.filterwarnings("ignore", "tempnam")
import shutil
import sys
import signal
from lockfile import LockFile

compress_files = False

warnings.filterwarnings("ignore", "tempnam")

lock_file = LockFile("/var/run/cdb_cache_lock")

pid_file = "/var/run/cdb_cache.pid"

cache_base_dir = '/data'
cache_dir = os.path.join(cache_base_dir, 'CDB_cache') + os.path.sep
final_dir = os.path.join(cache_base_dir, 'CDB_final') + os.path.sep
move_dir = os.path.join(cache_dir, '.to_be_deleted') + os.path.sep
h5repack_dir = os.path.join(cache_base_dir, 'CDB_h5repack') + os.path.sep
log_file = '/var/log/cdb_cache.log'

class InfoFilter(logging.Filter):
    """Splits info and error logs
    """
    def filter(self, rec):
        return rec.levelno in (logging.DEBUG, logging.INFO)

#logging.basicConfig(level=logging.INFO,
                    #format='%(asctime)s %(message)s')
logger = logging.getLogger('')
_handler = logging.handlers.TimedRotatingFileHandler(log_file, when='W6', backupCount=10)
_handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
_handler.setLevel(logging.INFO)
#_handler.setLevel(logging.DEBUG)
logger.addHandler(_handler)

_warning_handler = logging.StreamHandler(stream=sys.stderr)
_warning_handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
_warning_handler.setLevel(logging.WARNING)
logger.addHandler(_warning_handler)

_info_handler = logging.StreamHandler(stream=sys.stdout)
_info_handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
_info_handler.addFilter(InfoFilter())
_info_handler.setLevel(logging.INFO)
logger.addHandler(_info_handler)

logger.setLevel( logging.INFO )

def create_pid_file(filename=pid_file):
    logger.debug("Creating PID file at %s" % filename)
    if os.path.exists(filename):
        logger.error("PID file %s already exists" % pid_file)
        sys.exit(-1)
    with open(filename, "w") as f:
        f.write(str(os.getpid()))

def remove_pid_file(filename=pid_file):
    logger.debug("Removing PID file at %s" % filename)
    if os.path.exists(filename):
        try:
            os.remove(filename)
        except Exception:
            logger.error("Could not remove the PID file %s" % pid_file)

def kill_process_from_file(filename=pid_file):
    logger.debug("Trying to kill process with PID from file %s" % filename)
    if os.path.exists(filename):
        logger.warning("PID file %s found!" % pid_file)
        with open(filename) as f:
            pid = int(f.read())
            try:
                os.kill(pid, signal.SIGTERM)
            except Exception:
                pass
            logger.warning("process with PID %s has been killed" % pid)
        remove_pid_file(filename)

def exit_handler(signum, frame):
    logger.debug("Exiting by external signal.")
    #if lock_file.is_locked():
        #lock_file.release()
    sys.exit()



def makedirs(name, mode=2047):
    """makedirs(path [, mode=2047])

    Super-mkdir; create a leaf directory and all intermediate ones.
    Works like mkdir, except that any intermediate path segment (not
    just the rightmost) will be created if it does not exist.  This is
    recursive.

    """
    head, tail = os.path.split(name)
    if not tail:
        head, tail = os.path.split(head)
    if head and tail and not os.path.exists(head):
        try:
            makedirs(head, mode)
        except OSError as e:
            # be happy if someone already created the path
            if e.errno != errno.EEXIST:
                raise
        if tail == os.curdir:           # xxx/newdir/. exists if xxx/newdir exists
            return
    try:
        if not os.path.isdir(name):
            os.makedirs(name)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise
    try:
        os.chmod(name, mode)  # mkdir mode seems buggy for 0o3777
    except OSError as e:
        logger.debug('could not chmod %s' % name)


def _default_pack_filter(file_name):
    file_name = file_name.lower()
    if "edicam" in file_name:
        return False
    if "efit" in file_name:
        return False
    if "ris" in file_name:
        return False
    # ... Add more ...
    return file_name.endswith(".h5")


def h5repack(src_dir, dest_dir, file_name, file_filter=_default_pack_filter, compress_files=compress_files):
    '''

    :param source:
    :param dest:
    '''
    # makedirs(dest_dir)
    src_path = os.path.join(src_dir, file_name)
    dest_path = os.path.join(dest_dir, file_name)
    if not os.path.isfile(src_path):
        raise Exception("h5repack: %s is not an existing file." % src_path)

    makedirs(os.path.dirname(dest_path))
    
    
    if file_filter(file_name) and compress_files:
        args = ['h5repack', '-i', src_path, '-o', dest_path, '-f', 'GZIP=1']
    else:
        args = ['cp', src_path, dest_path]

    process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    h5r_out = []
    for l in process.stdout:
        h5r_out.append(l)
        pass
    process.stdout.close()

    if not os.path.isfile(dest_path):
        logger.error('h5repack error for %s:\n%s' % (src_path, '\n'.join(h5r_out)))
        logger.warning('copying probably corrupt file %s to %s' % (src_path, dest_path))
        try:
            shutil.copy(src_path, dest_path)
        except Exception:
            logger.fatal('cannot copy %s to %s' % (src_path, dest_path))

    if os.path.isfile(dest_path):
        # Make it read-only
        try:
            mode = os.stat(dest_path)[stat.ST_MODE]
            os.chmod(dest_path, mode & ~stat.S_IWUSR & ~stat.S_IWGRP & ~stat.S_IWOTH)
        except Exception:
            logger.error('cannot chmod %s' % (dest_path))


def find_args(directory):
    """Arguments for the find command.

    Writable files older than 20 minutes
    """
    return ['find', '-L', directory, '-type', 'f', '!', '-perm', '/u=w,g=w,o=w',
            '!', '-newermt', 'now - 30min', '-printf', '%P\\n']


def find_files(directory):
    """Find all files that match criteria.

    Runs external find command and processes the output.
    Returns the relative file paths
    """
    args = find_args(directory)
    p = subprocess.Popen(args, stdout=subprocess.PIPE)
    fnames = [fname.strip() for fname in p.stdout]
    p.stdout.close()
    return fnames


def write_temp_file(files):
    """Write a list of files to a new temporary file to be used by rsync.

    Returns the name of the file
    """
    from_file = open(os.tempnam(), 'w')
    from_file.write('\0'.join(files))
    from_file.close()
    return from_file.name


def run_rsync(args):
    """Run the rsync process and go through its stdout."""
    p_rsync = subprocess.Popen(args, stdout=subprocess.PIPE)
    for l in p_rsync.stdout:
        # print l
        pass
    p_rsync.stdout.close()
