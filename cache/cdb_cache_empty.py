#!/usr/bin/env python
#
# Move all files older than 20 minutes from cache to .to_be_deleted directory.
#
# In the second step, move all files from .to_be_deleted (excluding those that were just moved)
#  using rsync to cdb_final.

import os
import os.path
import sys
import shutil
import glob
from time import sleep
import logging
from cdb_cache import find_files, cache_dir, move_dir, final_dir
from cdb_cache import h5repack, h5repack_dir, makedirs
from cdb_cache import lock_file, exit_handler, create_pid_file, remove_pid_file
import signal

def resolve_conflict(f_in, fname, directory):
    """If there is a conflict between source and target file, rename the source and return it.
    """
    fbase, ext = os.path.splitext(fname)
    for i in range(100):
        fname_i = fbase + '_conflict_%02i' % i + ext
        f_out_i = os.path.join(directory, fname_i)
        if not os.path.exists(f_out_i):
            # TODO: log conflict
            break
    else:
        logging.critical("too many conflicts - overwriting")
        pass
    try:
        os.rename(f_in, f_out_i)
    except Exception:
        logging.error('Cannot rename %s to %s' % (f_in, f_out_i))
        raise
    return fname_i


def select_files_for_rsync(from_dir, to_dir, fnames):
    """Select all files that have to be rsynced and resolve conflicts."""
    from_fnames = []
    for fname in fnames:
        f_in = os.path.join(from_dir, fname)
        f_out = os.path.join(to_dir, fname)
        if os.path.exists(f_out):
            # this is a conflict
            logging.critical('CONFLICT: %s already exists, NOT MOVING %s' % (f_out, f_in))
            pass
            # raise Exception('file conflict detected')
            # fname = resolve_conflict(f_in, fname, from_dir)
        else:
            from_fnames.append(fname)
    return from_fnames


def move_from_cache_to_deleted():
    """Move files matching criteria from cache to .to_be_deleted.
    """

    # Find files (skip .to_be_deleted)
    fnames = find_files(cache_dir)
    fnames = (fname for fname in fnames if not glob.fnmatch.fnmatch(fname, '.to_be_deleted/*'))
    fnames = select_files_for_rsync(cache_dir, move_dir, fnames)

    logging.info('starting h5repack on %d files' % (len(fnames)))
    for fname in fnames:
        if not os.path.isfile(os.path.join(final_dir, fname)):
            # print('h5repack(%s, %s, %s)' % (cache_dir, h5repack_dir, fname))
            h5repack(cache_dir, h5repack_dir, fname)
            try:
                makedirs(os.path.dirname(os.path.join(final_dir, fname)))
                shutil.move(os.path.join(h5repack_dir, fname), os.path.join(final_dir, fname))
            except Exception as e:
                logging.critical('cannot move %s to %s, exception %s' % (
                                 os.path.join(h5repack_dir, fname),
                                 os.path.join(final_dir, fname), e))

        try:
            makedirs(os.path.dirname(os.path.join(move_dir, fname)))
            shutil.move(os.path.join(cache_dir, fname), os.path.join(move_dir, fname))
        except Exception as e:
            logging.critical('cannot move %s to %s, exception %s' % (
                             os.path.join(cache_dir, fname),
                             os.path.join(move_dir, fname), e))

    return fnames


def sync_deleted_to_final(just_moved):
    """Move files matching criteria from .to_be_deleted to final.

    Files that have been moved in the current run of the script are left intact
    Someone may have them open. They will live till the next time this script is run.
    """

    # Find files (skip just_moved)
    fnames = find_files(move_dir)
    fnames = (fname for fname in fnames if fname not in just_moved)
    # TODO: conflicts should be resolved diffrently, legitimate newer version can exist in move_dir
    # 1? rename older files
    # 2? check append only modifications
    # fnames = select_files_for_rsync(move_dir, final_dir, fnames)

    for fname in fnames:
        # print('os.remove(%s)' % os.path.join(move_dir, fname))
        try:
            os.remove(os.path.join(move_dir, fname))
        except Exception:
            logging.error('Cannot delete %s' % os.path.join(move_dir, fname))


if __name__ == "__main__":
    # register cleanup function
    signal.signal(signal.SIGTERM, exit_handler)
    # acquire lock
    try:
        logging.debug('Acquiring lock file')
        lock_file.acquire(timeout=10)
    except Exception:
        logging.error('Lock could not be acquired -> ending')
        raise
    try:
        create_pid_file()
    except Exception:
        logging.error("Creating PID file failed -> ending")
        raise
    try:
        # Repeat twice with 20 min delay
        for n in range(2):
            # Run in two phases
            # 1. Move from cdb_cache to .to_be deleted
            logging.info('move_from_cache_to_deleted started')
            just_moved = move_from_cache_to_deleted()
            logging.info('move_from_cache_to_deleted finished')
            logging.debug('moved files = \n%s\n' % '\n'.join(just_moved))

            # 2. Move all files from .to_be_deleted to cdb_final
            logging.info('sync_deleted_to_final(just_moved) started')
            sync_deleted_to_final(just_moved)
            logging.info('sync_deleted_to_final(just_moved) finished')

            # sleep for 20 min
            logging.info('sleeping for 20 minutes')
            sleep(20 * 60)
    except BaseException as e:
        logging.critical('%s occured: %s' % (e.__class__.__name__, str(e)))
        raise
    finally:
        lock_file.release()
        logging.info('finished, lock file released')
        remove_pid_file()
        logging.info('PID file removed')
