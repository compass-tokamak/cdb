# cdb_cache.py
Common methods for cdb_cache_copy and cdb_cache_empty

# mailer.py
Common module for sending notifications - to be imported in other scripts

# cdb_cache_copy.py
Copy all files in the cache older than 20 minutes to its final destination
There is no conflict resolution (i.e. if the target path of copying exists, nothing is copied or moved).

# cdb_cache_empty.py
Move all files older than 20 minutes from cache to .to_be_deleted directory.
In the second step, move all files from .to_be_deleted (excluding those that were just moved) using rsync to cdb_final.

# cdb_cache_empty_py_nospace.sh
Checks empty space on /data/, and if necessary, runs cdb_cache_lowspace.py, and/or send warning.

# cdb_cache_lowspace.py
Cleans CDB cache by moving 10GB of the oldest files to cdb_final, the list of moved files is generated and sent by email.

# cdb_cron_py
Current crontab, to be copied in /etc/cron.d

# cdb_rm_empty_dirs.sh
Removes empty directories in /data/CDB_cache and /data/CDB_h5repack

# cdb_rw_dirs.sh
Sets 3777 mode on /data/CDB_cache/* directories
