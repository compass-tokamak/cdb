#!/usr/bin/env python
# Cleans CDB cache by moving 10GB of the oldest files to cdb_final, the
# list of moved files is generated

import subprocess
import os
import glob
import sys
import shutil
from cdb_cache import makedirs, cache_dir, final_dir, lock_file, kill_process_from_file
import logging
import datetime
from mailer import send_mail, mail_from, mail_to
import lockfile

def get_file_info(_fname):
    fname = _fname.strip()
    fstat = os.stat(fname)
    return (fname, fstat.st_size, fstat.st_mtime)


def find_oldest_files(directory, size_to_move=51200):
    """Finds the oldest files in a given directory of a given size"""
    # find read-only files:
    size_to_move *= 2**20  # convert size to bytes
    find_args = ['find', directory, '-perm', '/u=r,g=r,o=r', '-type', 'f']
    stat_args = ['xargs', 'stat', "--format=\"%%n %%s %%Y\""]
    p = subprocess.Popen(find_args, stdout=subprocess.PIPE)

    file_list = (get_file_info(l) for l in p.stdout)
    total_size = 0
    files_to_move = []
    for fname, size, mtime in sorted(file_list, key=lambda finfo: finfo[2]):
        files_to_move.append(fname)
        total_size += size
        if total_size >= size_to_move:
            break
    return files_to_move


def move_files(file_list, _src_dir, _dest_dir):
    """Moves files in file_list from src_dir to dest_dir, creates sub directory if necessary"""
    src_dir = os.path.abspath(_src_dir)
    dest_dir = os.path.abspath(_dest_dir)
    for fname in file_list:
        # Check if file is in subdirectory and exists
        src_path = os.path.abspath(fname)
        if not src_path.startswith(src_dir) or not os.path.exists(src_path):
            logging.warning(
                "%s not found or not in subdirectory %s" % (fname, src_dir))
        dest_path = src_path.replace(src_dir, dest_dir)

        # if base directory of file does not exist, create it
        dest_base_dir = os.path.dirname(dest_path)
        if not os.path.isdir(dest_base_dir):
            # print "mkdir %s" % dest_base_dir
            makedirs(dest_base_dir)

        # print "mv %s %s" % (src_path, dest_path)
        shutil.move(src_path, dest_path)


if __name__ == "__main__":
    #size_to_move = 10240
    size_to_move = 25600
    logging.warning("Running %s" % __file__)
    lstdir = "/var/log/"

    # send mail with file as attachment
    BODY = "Starting emergency CDB cache cleaning."
    SUBJ = 'Emergency CDB cache cleaning started'
    send_mail(mail_from, mail_to, SUBJ, BODY)

    # if the pid file exists, try to kill its creator's process and remove lock
    try:
        # send mail with file as attachment
        BODY = "Lock file is already locked, trying to kill concurent process."
        SUBJ = 'Lock file warning'
        ATT = []
        send_mail(mail_from, mail_to, SUBJ, BODY, ATT)
        kill_process_from_file()
    except Exception:
        logging.error("Got Exception in kill_process_from_file() !")
    #acquire lock
    try:
        logging.debug('Acquiring lock file')
        lock_file.acquire(timeout=10)
    #except lockfile.AlreadyLocked, lockfile.LockTimeout:
        #logging.warning('Killing concurent process with PID %s', lock_file.pid)
        #os.kill(lock_file.pid, signal.SIGTERM)
    except Exception:
        logging.error('Lock could not be acquired -> ending')
        raise
    try:
        # get current datetime info -- to be used in file list name
        d = datetime.datetime.now()

        # generate file list
        file_list = find_oldest_files(cache_dir, size_to_move)

        lstname = "%smoved_files_%s.txt" % (lstdir, d.isoformat())
        with open(lstname, "w") as lst:
            lst.write("\n".join(file_list))

        # move files
        move_files(file_list, cache_dir, final_dir)

        # send mail with file as attachment
        BODY = "The list of moved files can be found in the attached file."
        SUBJ = 'Emergency CDB cache cleaning finished'
        ATT = [lstname]
        send_mail(mail_from, mail_to, SUBJ, BODY, ATT)

        # log this event
        logging.warning(
            "The CDB cache cleaning finished. List of moved files can be found in file %s" % lstname)

    except BaseException as e:
        logging.critical('%s occured: %s' % (e.__class__.__name__, str(e)))
        raise
    finally:
        lock_file.release()
        logging.info('finished, lock file released')
