#!/usr/bin/env python
#
# Copy all files in the cache older than 20 minutes to its final destination
#
# There is no conflict resolution (i.e. if the target path of copying exists,
#  nothing is copied or moved).

import os
import os.path
import glob
import sys
import shutil
import logging
from cdb_cache import find_files, cache_dir, final_dir, h5repack, h5repack_dir, makedirs
from cdb_cache import lock_file, create_pid_file, remove_pid_file, exit_handler, compress_files
import signal


def copy_from_cache_to_final():
    """Copy all files matching criteria from cache to final."""

    # Find files to copy
    logging.info('find_file(%s)' % (cache_dir))
    from_fnames = find_files(cache_dir)
    # print(list(from_fnames))

    # Skip .to_be_deleted directory
    from_fnames = [fname for fname in from_fnames
                   if not glob.fnmatch.fnmatch(fname, '.to_be_deleted/*')]
    logging.info('found %d file to copy' % (len(from_fnames)))

    # Select only files that don't exist in the final dir (conflicts solved in cdb_cache_empty)
    from_fnames = [fname for fname in from_fnames
                   if not os.path.exists(os.path.join(final_dir, fname))]

    logging.info('starting h5repack on %d files' % (len(from_fnames)))
    for fname in from_fnames:
        if not os.path.isfile(os.path.join(final_dir, fname)):
            # print('h5repack(%s, %s, %s)' % (cache_dir, h5repack_dir, fname))
            # h5repack(cache_dir, final_dir, fname)
            # TODO: use h5repack_dir intenally in h5repack
            if compress_files:
                h5repack(cache_dir, h5repack_dir, fname)
                makedirs(os.path.dirname(os.path.join(final_dir, fname)))
                shutil.move(os.path.join(h5repack_dir, fname), os.path.join(final_dir, fname))
            else:
                makedirs(os.path.dirname(os.path.join(final_dir, fname)))
                shutil.copy(os.path.join(cache_dir, fname), os.path.join(final_dir, fname))

if __name__ == "__main__":
    # register cleanup function
    signal.signal(signal.SIGTERM, exit_handler)
    # acquire lock
    try:
        logging.debug('Acquiring lock file')
        lock_file.acquire(timeout=10)
    except Exception:
        logging.error('Lock could not be acquired -> ending')
        raise
    try:
        create_pid_file()
    except Exception:
        logging.error("Creating PID file failed -> ending")
        raise
    try:
        copy_from_cache_to_final()
    finally:
        lock_file.release()
        logging.info('finished, lock file released')
        remove_pid_file()
        logging.info('PID file removed')
