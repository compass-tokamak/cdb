#!/bin/bash

# chmod 3777 /data/CDB_cache/*

# this can get slow if empty directories are left
cache_base_dir=/data
cache_dir=$cache_base_dir/CDB_cache/
final_dir=$cache_base_dir/CDB_final/
move_dir=$cache_dir/.to_be_deleted/

find $cache_dir -type d -print0 | xargs -0 chmod 3777
