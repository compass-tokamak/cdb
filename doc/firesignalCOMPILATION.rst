==========================
Compilation of FireSignal:
==========================

in NetBeans IDE -> new project from resources -> choose all java structure

You will neeed many libraries. Usually they are common and publicly accesible.

SDAS Core Libraries (SDAS.jar) and SDAS Client (SDASClient.jar) you can find here:
http://metis.ipfn.ist.utl.pt/CODAC/SDAS/Download



Example: LAST RECORD NUMBER IN GUI CLIENT
=========================================

export LD_PRELOAD=/usr/lib/libpython2.6.so.1.0			..needs (at least) pymsql
export PYTHONPATH=$PYTHONPATH:/home/rrr/workspace/pyCDB/src/

java -Djava.library.path=/home/rrr/FS_PT/libs/:/usr/local/lib/:/usr/lib -jar ./dist/FS.jar	
	..location of c-libs

NOTE: Done just in FS.jar X not as FireSignalClient.jpnl
