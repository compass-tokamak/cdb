.. java:import:: com.google.gson Gson

.. java:import:: cz.cas.ipp.compass.jycdb.util Data

.. java:import:: cz.cas.ipp.compass.jycdb.util Hdf5Utils

.. java:import:: cz.cas.ipp.compass.jycdb.util Hdf5WriteException

.. java:import:: cz.cas.ipp.compass.jycdb.util ParameterList

.. java:import:: java.io File

.. java:import:: java.io FileInputStream

.. java:import:: java.io FileOutputStream

.. java:import:: java.io IOException

.. java:import:: java.nio.channels FileChannel

.. java:import:: java.util LinkedHashMap

FSNodeWriter
============

.. java:package:: cz.cas.ipp.compass.jycdb
   :noindex:

.. java:type:: public class FSNodeWriter

   Methods for FireSignal nodes. It is mostly a facade over CDBClient methods to simplify development of nodes that communicate directly with CDB. How to write a signal? 1) For each record, create an instance of FSNodeWriter (you have to know generic signal id as well). 2) Create a file using createDataFile(). (see variants of this method) 3) Write data (as many times as you want). a] Using writeData(). Please, notice that you have to be aware of the data offset in HDF5 (or write them in one step). b] If you have an existing HDF5 file, use copyHdf5(). 4) Tell DB that the file is ready using setFileReady(). (if you changed it) 5) Write all axes using writeAxis(index). index=0 for time axis, index=1,2,3,... for other axes. (can be called multiple times for the same axis). 6) Write signal using writeSignal().

   :author: pipek

Fields
------
gson
^^^^

.. java:field::  Gson gson
   :outertype: FSNodeWriter

Constructors
------------
FSNodeWriter
^^^^^^^^^^^^

.. java:constructor:: public FSNodeWriter(long genericSignalId, long recordNumber, String collectionName, String fileKey) throws CDBException
   :outertype: FSNodeWriter

   :param collectionName: Name of the file without prefixes.
   :param fileKey: Name of the dataset in HDF5 file.
   :throws cz.cas.ipp.compass.jycdb.CDBException:

FSNodeWriter
^^^^^^^^^^^^

.. java:constructor:: public FSNodeWriter(long computerId, long boardId, long channelId, long recordNumber, String collectionName, String fileKey) throws CDBException
   :outertype: FSNodeWriter

Methods
-------
addDaqParameter
^^^^^^^^^^^^^^^

.. java:method:: public void addDaqParameter(String key, Object value)
   :outertype: FSNodeWriter

addGenericSignalParameter
^^^^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: public void addGenericSignalParameter(String key, Object value)
   :outertype: FSNodeWriter

copyHdf5
^^^^^^^^

.. java:method:: public void copyHdf5(String path) throws IOException
   :outertype: FSNodeWriter

   Copy an existing HDF5 file to the correct destination.

   :param path: Local path of the HDF5 file.
   :throws IOException: If the destination exist, copying is prevented and exception thrown.

createDataFile
^^^^^^^^^^^^^^

.. java:method:: public void createDataFile() throws CDBException
   :outertype: FSNodeWriter

   Create new data file (row in `data_files`).

   :throws CDBException: In this default version, an already existing file with requested properties is taken as error.

createDataFile
^^^^^^^^^^^^^^

.. java:method:: public boolean createDataFile(boolean okIfExists, boolean createIfExists) throws CDBException
   :outertype: FSNodeWriter

   Create new data file (row in `data_files`).

   :param okIfExists: If false, an exception is thrown if a file with the same collection name, data source and record number exists
   :param createIfExists: If true and a file already exists, create a new one (otherwise the existing one is returned).
   :throws CDBException:
   :return: true if a file was created, false if nothing happens.

getDataFile
^^^^^^^^^^^

.. java:method:: public DataFile getDataFile()
   :outertype: FSNodeWriter

getFileKey
^^^^^^^^^^

.. java:method:: public String getFileKey()
   :outertype: FSNodeWriter

getGenericSignal
^^^^^^^^^^^^^^^^

.. java:method:: public GenericSignal getGenericSignal()
   :outertype: FSNodeWriter

getRecordNumber
^^^^^^^^^^^^^^^

.. java:method:: public long getRecordNumber()
   :outertype: FSNodeWriter

setFileReady
^^^^^^^^^^^^

.. java:method:: public void setFileReady()
   :outertype: FSNodeWriter

   Tell CDB that you have finished writing data. After this, you cannot write more data.

setRequireChannelAttachment
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: public void setRequireChannelAttachment(boolean require)
   :outertype: FSNodeWriter

   Set whether writing should succeed (assuming value 1.0 for coefficients) when channel attachment not found. It has to be specified explicitely because it is quite probable that non-existence marks a bug.

writeAxis
^^^^^^^^^

.. java:method:: public void writeAxis(int axis, double coefficient, double offset) throws CDBException
   :outertype: FSNodeWriter

   Write one of the axes.

   :param axis: Index of the axis (0=time, 1,2,3,...=other axes)

writeData
^^^^^^^^^

.. java:method:: public void writeData(Data data, long[] dataOffset) throws Hdf5WriteException, CDBException
   :outertype: FSNodeWriter

   Write data (with possible offset to append).

   :param data: Data to be written.
   :param dataOffset: Zero-based index of the first data element. Size is calculated automatically. If data Offset is null, starts from beginning (fails with existing file and dataset).
   :throws cz.cas.ipp.compass.jycdb.util.Hdf5WriteException:

writeNotAttachedSignal
^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: public void writeNotAttachedSignal(double time0, double coefficient) throws CDBException
   :outertype: FSNodeWriter

   Write signal info to CDB for signals that have no DAQ attachment. Usage is similar to writeSignal.

writeSignal
^^^^^^^^^^^

.. java:method:: public void writeSignal(double time0) throws CDBException
   :outertype: FSNodeWriter

   Write signal info to CDB. Call this after you have finished with writing data and axes. Can be used only for generic signal with valid DAQ attachment.

