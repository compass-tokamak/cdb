.. java:import:: cz.cas.ipp.compass.jycdb.util DictionaryAdapter

.. java:import:: cz.cas.ipp.compass.jycdb.util JsonUtils

.. java:import:: org.python.core PyObject

SignalParameters
================

.. java:package:: cz.cas.ipp.compass.jycdb
   :noindex:

.. java:type:: public class SignalParameters extends DictionaryAdapter

   Signal parameters (both GS & DAQ-based). These are strings interpreted as JSON. Strings are accessible using getDAQParametersString() & getGenericSignalParametersString(), Parsed JSON objects (as trees) are accessible using getDAQParameters() & getGenericSignalParameters().

   :author: pipek

Methods
-------
create
^^^^^^

.. java:method:: public static SignalParameters create(PyObject object)
   :outertype: SignalParameters

getDAQParameters
^^^^^^^^^^^^^^^^

.. java:method:: public Object getDAQParameters()
   :outertype: SignalParameters

getDAQParametersString
^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: public String getDAQParametersString()
   :outertype: SignalParameters

getGenericSignalParameters
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: public Object getGenericSignalParameters()
   :outertype: SignalParameters

getGenericSignalParametersString
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: public String getGenericSignalParametersString()
   :outertype: SignalParameters

