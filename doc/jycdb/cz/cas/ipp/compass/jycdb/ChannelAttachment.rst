.. java:import:: cz.cas.ipp.compass.jycdb.util DictionaryAdapter

.. java:import:: cz.cas.ipp.compass.jycdb.util JsonUtils

.. java:import:: org.python.core PyObject

ChannelAttachment
=================

.. java:package:: cz.cas.ipp.compass.jycdb
   :noindex:

.. java:type:: public class ChannelAttachment extends DictionaryAdapter

Methods
-------
create
^^^^^^

.. java:method:: public static ChannelAttachment create(PyObject object)
   :outertype: ChannelAttachment

getAttachedGenericSignalId
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: public long getAttachedGenericSignalId()
   :outertype: ChannelAttachment

getCoefficientLev2V
^^^^^^^^^^^^^^^^^^^

.. java:method:: public double getCoefficientLev2V()
   :outertype: ChannelAttachment

getCoefficientV2Unit
^^^^^^^^^^^^^^^^^^^^

.. java:method:: public double getCoefficientV2Unit()
   :outertype: ChannelAttachment

getParameters
^^^^^^^^^^^^^

.. java:method:: public Object getParameters()
   :outertype: ChannelAttachment

getParametersString
^^^^^^^^^^^^^^^^^^^

.. java:method:: public String getParametersString()
   :outertype: ChannelAttachment

