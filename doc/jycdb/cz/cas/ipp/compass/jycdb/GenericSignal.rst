.. java:import:: cz.cas.ipp.compass.jycdb.util DictionaryAdapter

.. java:import:: cz.cas.ipp.compass.jycdb.util PythonUtils

.. java:import:: org.python.core PyObject

GenericSignal
=============

.. java:package:: cz.cas.ipp.compass.jycdb
   :noindex:

.. java:type:: public class GenericSignal extends DictionaryAdapter

   Wrapper around rows of `generic_signals` table.

   :author: honza

Fields
------
FILE
^^^^

.. java:field:: public static final String FILE
   :outertype: GenericSignal

LINEAR
^^^^^^

.. java:field:: public static final String LINEAR
   :outertype: GenericSignal

MAX_AXES
^^^^^^^^

.. java:field:: public static int MAX_AXES
   :outertype: GenericSignal

VALID_TYPES
^^^^^^^^^^^

.. java:field:: public static final String[] VALID_TYPES
   :outertype: GenericSignal

Methods
-------
create
^^^^^^

.. java:method:: public static GenericSignal create(PyObject object)
   :outertype: GenericSignal

getAlias
^^^^^^^^

.. java:method:: public String getAlias()
   :outertype: GenericSignal

getAxis
^^^^^^^

.. java:method:: public GenericSignal getAxis(int index) throws CDBException
   :outertype: GenericSignal

getAxisId
^^^^^^^^^

.. java:method:: public long getAxisId(int index) throws CDBException
   :outertype: GenericSignal

   Get id of one of the axes (time or spatial).

   :param index: Number of the axis (starting with 1). If 0, time axis is returned.
   :throws CDBException:
   :return: 0 if there is no axis.

getDataSourceId
^^^^^^^^^^^^^^^

.. java:method:: public long getDataSourceId()
   :outertype: GenericSignal

getDescription
^^^^^^^^^^^^^^

.. java:method::  String getDescription()
   :outertype: GenericSignal

getId
^^^^^

.. java:method:: public long getId()
   :outertype: GenericSignal

getLastRecordNumber
^^^^^^^^^^^^^^^^^^^

.. java:method:: public long getLastRecordNumber()
   :outertype: GenericSignal

getName
^^^^^^^

.. java:method:: public String getName()
   :outertype: GenericSignal

getSignalType
^^^^^^^^^^^^^

.. java:method:: public String getSignalType()
   :outertype: GenericSignal

getTimeAxis
^^^^^^^^^^^

.. java:method:: public GenericSignal getTimeAxis() throws CDBException
   :outertype: GenericSignal

getTimeAxisId
^^^^^^^^^^^^^

.. java:method:: public long getTimeAxisId() throws CDBException
   :outertype: GenericSignal

getUnits
^^^^^^^^

.. java:method:: public String getUnits()
   :outertype: GenericSignal

isFile
^^^^^^

.. java:method:: public boolean isFile()
   :outertype: GenericSignal

isLinear
^^^^^^^^

.. java:method:: public boolean isLinear()
   :outertype: GenericSignal

isValidType
^^^^^^^^^^^

.. java:method:: public static boolean isValidType(String signalType)
   :outertype: GenericSignal

   Check if a signal type is valid. Only "LINEAR" and "FILE" are accepted now.

