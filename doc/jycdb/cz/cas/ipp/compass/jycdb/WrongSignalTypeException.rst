WrongSignalTypeException
========================

.. java:package:: cz.cas.ipp.compass.jycdb
   :noindex:

.. java:type:: public class WrongSignalTypeException extends CDBException

   Signal type is not valid for its intended use. It can be either LINEAR or FILE with different operations available.

   :author: pipek

Constructors
------------
WrongSignalTypeException
^^^^^^^^^^^^^^^^^^^^^^^^

.. java:constructor:: public WrongSignalTypeException(String message)
   :outertype: WrongSignalTypeException

