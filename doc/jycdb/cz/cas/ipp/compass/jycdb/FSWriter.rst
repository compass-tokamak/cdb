.. java:import:: cz.cas.ipp.compass.jycdb.util ParameterList

.. java:import:: cz.cas.ipp.compass.jycdb.util PythonAdapter

.. java:import:: cz.cas.ipp.compass.jycdb.util PythonUtils

.. java:import:: org.python.util PythonInterpreter

FSWriter
========

.. java:package:: cz.cas.ipp.compass.jycdb
   :noindex:

.. java:type:: public class FSWriter extends PythonAdapter

   Java wrapper for quick storing of signal data to CDB in database controller. See: fs_writer.py Warning: This class is meant to be used only in FireSignal database controller! Not elsewhere, even the nodes. Unexpected behaviour may result.

   :author: pipek

Constructors
------------
FSWriter
^^^^^^^^

.. java:constructor:: public FSWriter(CDBClient client, String nodeId, String hardwareId, String parameterId, long recordNumber, double time0, double sampleLength) throws CDBConnectionException
   :outertype: FSWriter

Methods
-------
getFileKey
^^^^^^^^^^

.. java:method:: public String getFileKey()
   :outertype: FSWriter

   Get the name of the dataset to write to.

getFilePath
^^^^^^^^^^^

.. java:method:: public String getFilePath()
   :outertype: FSWriter

   Get the full path where to write the file.

readSignalInfo
^^^^^^^^^^^^^^

.. java:method:: public void readSignalInfo()
   :outertype: FSWriter

   1st step - read all information we need for file storage.

signalExists
^^^^^^^^^^^^

.. java:method:: public boolean signalExists()
   :outertype: FSWriter

   Is the signal already stored in the database?

storeSignalAndFile
^^^^^^^^^^^^^^^^^^

.. java:method:: public void storeSignalAndFile()
   :outertype: FSWriter

   2nd step - write all that is to be written to database. This step should be undertaken only if signal does not exist.

