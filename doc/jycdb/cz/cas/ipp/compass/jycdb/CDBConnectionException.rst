CDBConnectionException
======================

.. java:package:: cz.cas.ipp.compass.jycdb
   :noindex:

.. java:type:: public class CDBConnectionException extends CDBException

   The connection to database does not work. This means that the pyCDB was correctly loaded but there is a problem inside it or between it and MySQL.

   :author: pipek

Constructors
------------
CDBConnectionException
^^^^^^^^^^^^^^^^^^^^^^

.. java:constructor:: public CDBConnectionException(String message)
   :outertype: CDBConnectionException

