.. java:import:: cz.cas.ipp.compass.jycdb.util ArrayUtils

.. java:import:: cz.cas.ipp.compass.jycdb.util DictionaryAdapter

.. java:import:: org.python.core PyObject

SignalCalibration
=================

.. java:package:: cz.cas.ipp.compass.jycdb
   :noindex:

.. java:type:: public class SignalCalibration extends DictionaryAdapter

   Signal calibration. Meaning: value = (data + offset) * coefficient

   :author: pipek

Fields
------
DAQ_VOLTS
^^^^^^^^^

.. java:field:: public static final String DAQ_VOLTS
   :outertype: SignalCalibration

RAW
^^^

.. java:field:: public static final String RAW
   :outertype: SignalCalibration

Methods
-------
apply
^^^^^

.. java:method:: public double[] apply(double[] array)
   :outertype: SignalCalibration

   If you have an array of data, apply calibration to it. Returns new array.

create
^^^^^^

.. java:method:: public static SignalCalibration create(PyObject object)
   :outertype: SignalCalibration

getCoefficient
^^^^^^^^^^^^^^

.. java:method:: public double getCoefficient()
   :outertype: SignalCalibration

getOffset
^^^^^^^^^

.. java:method:: public double getOffset()
   :outertype: SignalCalibration

getUnits
^^^^^^^^

.. java:method:: public String getUnits()
   :outertype: SignalCalibration

