.. java:import:: java.util SortedMap

.. java:import:: java.util TreeMap

.. java:import:: org.python.core PyObject

Signal
======

.. java:package:: cz.cas.ipp.compass.jycdb
   :noindex:

.. java:type:: public class Signal extends PythonAdapter

   Class mimicking the CDBSignal Python class. In a few aspects, it is more object-oriented and lazy-evaluated to simplify manipulation in MATLAB/IDL.

   :author: pipek

Constructors
------------
Signal
^^^^^^

.. java:constructor:: public Signal(SignalReference signalRef, String units)
   :outertype: Signal

Signal
^^^^^^

.. java:constructor:: protected Signal(Signal parent, int nthChild, PythonAdapter signalTree, DictionaryAdapter unitFactorTree)
   :outertype: Signal

   Constructor for dependent objects from signal tree with unit factor.

   :param parent: Signal that uses this as an axis.
   :param nthChild: The order of this signal among axes in the parent signal.
   :param signalTree: Signal subtree.
   :param unitFactorTree: Unit factor subtree.

Signal
^^^^^^

.. java:constructor:: protected Signal(Signal parent, int nthChild, PythonAdapter signalTree, String units)
   :outertype: Signal

   Constructor for dependent objects from signal tree without unit factor.

   :param parent: Signal that uses this as an axis.
   :param nthChild: The order of this signal among axes in the parent signal.
   :param signalTree: Signal subtree.
   :param units: Most times, this will be "default", but any other value can used.

Methods
-------
getAxes
^^^^^^^

.. java:method:: public SortedMap<String, Signal> getAxes() throws CDBException
   :outertype: Signal

   A map of all existing axes.

   :throws CDBException: Non-existent axes are omitted.
   :return: A dictionary of [ axis_name, Signal object ] values.

getAxis
^^^^^^^

.. java:method:: public Signal getAxis(int i) throws CDBException
   :outertype: Signal

   An axis as Signal instance.

   :param i: 0-time axis, N-axisN
   :throws CDBException:
   :return: null if not found

getData
^^^^^^^

.. java:method:: public synchronized Data getData() throws CDBException
   :outertype: Signal

   Numerical data of the signal. Note: this method is not used in bindings using JyCDB (IDL/MATLAB) due to efficiency. Try using native implementations of file reading.

getDataFile
^^^^^^^^^^^

.. java:method:: public DataFile getDataFile() throws CDBException
   :outertype: Signal

getDataFileId
^^^^^^^^^^^^^

.. java:method:: public long getDataFileId()
   :outertype: Signal

getDescription
^^^^^^^^^^^^^^

.. java:method:: public String getDescription() throws CDBException
   :outertype: Signal

   Generic signal description.

getGenericSignal
^^^^^^^^^^^^^^^^

.. java:method:: public GenericSignal getGenericSignal() throws CDBException
   :outertype: Signal

getGenericSignalId
^^^^^^^^^^^^^^^^^^

.. java:method:: public long getGenericSignalId() throws CDBException
   :outertype: Signal

getGenericSignalReference
^^^^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: public GenericSignal getGenericSignalReference() throws CDBException
   :outertype: Signal

getName
^^^^^^^

.. java:method:: public String getName() throws CDBException
   :outertype: Signal

   Generic signal name.

getParameters
^^^^^^^^^^^^^

.. java:method:: public SignalParameters getParameters() throws CDBException
   :outertype: Signal

getPythonObject
^^^^^^^^^^^^^^^

.. java:method:: @Override public PyObject getPythonObject()
   :outertype: Signal

   Return the Python object of CDBSignal class.

   :return: null if CDBSignal not found. Note: This method catches exceptions thus hiding problems.

getRecordNumber
^^^^^^^^^^^^^^^

.. java:method:: public long getRecordNumber() throws CDBException
   :outertype: Signal

getSignalCalibration
^^^^^^^^^^^^^^^^^^^^

.. java:method:: public SignalCalibration getSignalCalibration(boolean includeUnitFactor) throws CDBException
   :outertype: Signal

   Signal calibration for default/DAV/RAW.

   :throws CDBException:

getSignalReference
^^^^^^^^^^^^^^^^^^

.. java:method:: public SignalReference getSignalReference()
   :outertype: Signal

getTimeAxis
^^^^^^^^^^^

.. java:method:: public Signal getTimeAxis() throws CDBException
   :outertype: Signal

   Time axis as Signal instance. Just shortcut to getAxis method.

getUnit
^^^^^^^

.. java:method:: public String getUnit() throws CDBException
   :outertype: Signal

   Unit name.

   :throws CDBException: RAW/DAQ/default GS units or units from factor tree (if requested).

getUnitFactor
^^^^^^^^^^^^^

.. java:method:: public double getUnitFactor() throws CDBException
   :outertype: Signal

   Unit factor, by which the default signal has to be multiplied.

   :throws CDBException: If DAV/RAW/default are selected, this factor is 1.0. If another unit/unit system is selected, this is a conversion from the default unit to the selected one. Internally, this reflects unit_factor_tree returned from Python.

hasUnitFactor
^^^^^^^^^^^^^

.. java:method:: public boolean hasUnitFactor()
   :outertype: Signal

   Whether the signal has associated unit factor tree. This is true only if unit or unit system were requested.

isDAV
^^^^^

.. java:method:: public boolean isDAV()
   :outertype: Signal

isFile
^^^^^^

.. java:method:: public boolean isFile() throws CDBException
   :outertype: Signal

isLinear
^^^^^^^^

.. java:method:: public boolean isLinear() throws CDBException
   :outertype: Signal

isRaw
^^^^^

.. java:method:: public boolean isRaw()
   :outertype: Signal

