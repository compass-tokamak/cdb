CDBException
============

.. java:package:: cz.cas.ipp.compass.jycdb
   :noindex:

.. java:type:: public class CDBException extends Exception

   Some problem with CDB. Common parent for more specific exception classes.

   :author: pipek

Constructors
------------
CDBException
^^^^^^^^^^^^

.. java:constructor:: public CDBException(String message)
   :outertype: CDBException

