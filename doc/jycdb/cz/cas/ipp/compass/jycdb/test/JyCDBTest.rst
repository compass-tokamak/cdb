.. java:import:: java.util Random

.. java:import:: java.util.logging Level

.. java:import:: java.util.logging Logger

JyCDBTest
=========

.. java:package:: cz.cas.ipp.compass.jycdb.test
   :noindex:

.. java:type:: public class JyCDBTest

   :author: pipek

Methods
-------
fsWriterTest
^^^^^^^^^^^^

.. java:method:: public static void fsWriterTest()
   :outertype: JyCDBTest

instanceTest
^^^^^^^^^^^^

.. java:method:: public static void instanceTest()
   :outertype: JyCDBTest

main
^^^^

.. java:method:: public static void main(String[] args)
   :outertype: JyCDBTest

