.. java:import:: cz.cas.ipp.compass.jycdb.util DictionaryAdapter

.. java:import:: cz.cas.ipp.compass.jycdb.util ParameterList

.. java:import:: cz.cas.ipp.compass.jycdb.util PythonAdapter

.. java:import:: cz.cas.ipp.compass.jycdb.util PythonUtils

.. java:import:: java.util ArrayList

.. java:import:: java.util List

.. java:import:: java.util Map

.. java:import:: java.util WeakHashMap

.. java:import:: org.python.core Py

.. java:import:: org.python.core PyException

.. java:import:: org.python.core PyObject

.. java:import:: org.python.util PythonInterpreter

CDBClient
=========

.. java:package:: cz.cas.ipp.compass.jycdb
   :noindex:

.. java:type:: public class CDBClient extends PythonAdapter

   Jython-based adapter of python CDBClient. Threading: Each thread should acquire an instance of this class using getInstance(). Multiple calls in one thread result in returning the same object. There is no need of closing the connections, it happens automatically in garbage collector (this is forced when we run out of connections). However, you should not pass reference to one instance among different threads (if you cannot assure the threads will not use it concurrently). The methods map to Client/DAQClient python methods as closely as possible. Several methods have overloaded versions: 1) a totally generic one that accepts ParameterList (a "dictionary" of values) 2,...) more specific versions with frequently used sets of parameters. See pyCDB documentation.

   :author: pipek

Methods
-------
FS2CDB_ref
^^^^^^^^^^

.. java:method:: public void FS2CDB_ref(String nodeUniqueId, String hardwareUniqueId, String parameterUniqueId)
   :outertype: CDBClient

   Return the CDB DAQ_channel reference of a FireSignal channel

   :param nodeUniqueId:
   :param hardwareUniqueId:
   :param parameterUniqueId:

checkConnection
^^^^^^^^^^^^^^^

.. java:method:: public boolean checkConnection()
   :outertype: CDBClient

createComputer
^^^^^^^^^^^^^^

.. java:method:: public Long createComputer(String computerName, String description, String location)
   :outertype: CDBClient

createComputer
^^^^^^^^^^^^^^

.. java:method:: public Long createComputer(String computerName, String description)
   :outertype: CDBClient

createComputer
^^^^^^^^^^^^^^

.. java:method:: public Long createComputer(String computerName)
   :outertype: CDBClient

createDAQChannel
^^^^^^^^^^^^^^^^

.. java:method:: public void createDAQChannel(String nodeId, String hardwareId, String parameterId, long dataSourceId, Long[] axisIds)
   :outertype: CDBClient

createDAQChannel
^^^^^^^^^^^^^^^^

.. java:method:: public void createDAQChannel(ParameterList parameters)
   :outertype: CDBClient

createGenericSignal
^^^^^^^^^^^^^^^^^^^

.. java:method:: public void createGenericSignal(String name, String alias, long dataSourceId, Long[] axisIds, String units, String signalType) throws WrongSignalTypeException
   :outertype: CDBClient

createGenericSignal
^^^^^^^^^^^^^^^^^^^

.. java:method:: public void createGenericSignal(ParameterList parameters)
   :outertype: CDBClient

createGenericSignalWhichReturnsLong
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: public Long createGenericSignalWhichReturnsLong(String name, String alias, long dataSourceId, Long[] axisIds, String units, String signalType) throws WrongSignalTypeException
   :outertype: CDBClient

createGenericSignalWhichReturnsLong
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: public Long createGenericSignalWhichReturnsLong(ParameterList parameters)
   :outertype: CDBClient

createRecord
^^^^^^^^^^^^

.. java:method:: public long createRecord(Map<String, PyObject> parameters)
   :outertype: CDBClient

createRecord
^^^^^^^^^^^^

.. java:method:: public long createRecord(String recordType)
   :outertype: CDBClient

createRecord
^^^^^^^^^^^^

.. java:method:: public long createRecord(long fsEventNumber, String fsEventID)
   :outertype: CDBClient

   Creates new FireSignal record with forced record number equivalent to fsEventNumber.

   :param fsEventID: event identification "0x0000" -
   :param fsEventNumber: firesignal shot number

createRecord
^^^^^^^^^^^^

.. java:method:: public long createRecord(long fsEventNumber, String fsEventID, String recordType)
   :outertype: CDBClient

deleteSignal
^^^^^^^^^^^^

.. java:method:: public void deleteSignal(ParameterList parameters)
   :outertype: CDBClient

finalize
^^^^^^^^

.. java:method:: @Override public void finalize() throws Throwable
   :outertype: CDBClient

   Even if finalize is not always called, try to close the connection.

findGenericSignals
^^^^^^^^^^^^^^^^^^

.. java:method:: public List<GenericSignal> findGenericSignals(String aliasOrName, boolean useRegexp)
   :outertype: CDBClient

getAttachment
^^^^^^^^^^^^^

.. java:method:: public ChannelAttachment getAttachment(long genericSignalId)
   :outertype: CDBClient

getAttachmentTable
^^^^^^^^^^^^^^^^^^

.. java:method:: public List<ChannelAttachment> getAttachmentTable(ParameterList parameters)
   :outertype: CDBClient

getAttachmentTable
^^^^^^^^^^^^^^^^^^

.. java:method:: public ChannelAttachment getAttachmentTable(long computerId, long boardId, long channelId)
   :outertype: CDBClient

getComputerId
^^^^^^^^^^^^^

.. java:method:: public Long getComputerId(String computerName, String description)
   :outertype: CDBClient

getComputerId
^^^^^^^^^^^^^

.. java:method:: public Long getComputerId(String computerName)
   :outertype: CDBClient

getDataFile
^^^^^^^^^^^

.. java:method:: public DataFile getDataFile(ParameterList parameters)
   :outertype: CDBClient

getDataFile
^^^^^^^^^^^

.. java:method:: public DataFile getDataFile(long dataFileId)
   :outertype: CDBClient

getDataSourceId
^^^^^^^^^^^^^^^

.. java:method:: public Long getDataSourceId(String dataSourceName)
   :outertype: CDBClient

getFSSignal
^^^^^^^^^^^

.. java:method:: public GenericSignal getFSSignal(String nodeId, String hardwareId, String parameterId)
   :outertype: CDBClient

   Get current generic signal attached to node, board & parameter.

   :return: null if not found. Use Firesignal names.

getGenericSignal
^^^^^^^^^^^^^^^^

.. java:method:: public GenericSignal getGenericSignal(long genericSignalId)
   :outertype: CDBClient

getGenericSignal
^^^^^^^^^^^^^^^^

.. java:method:: public GenericSignal getGenericSignal(String strid) throws CDBException
   :outertype: CDBClient

getGenericSignals
^^^^^^^^^^^^^^^^^

.. java:method:: public List<GenericSignal> getGenericSignals(ParameterList parameters)
   :outertype: CDBClient

getGenericSignals
^^^^^^^^^^^^^^^^^

.. java:method:: public List<GenericSignal> getGenericSignals(String strid)
   :outertype: CDBClient

getInstance
^^^^^^^^^^^

.. java:method:: public static synchronized CDBClient getInstance() throws CDBException
   :outertype: CDBClient

   Get CDB client specific for current thread. It can serve a few hundred connections before problem arises. However, at that moment, there is a slight pause during which we try to close all unused connections (by hinting garbage connection) and obtain the connection for the second time. Only after this it eventually fails. Note: you should not pass CDBClient instances among threads or undefined behaviour results. But it is possible if you know what you are doing (e.g. in FS database controller).

   :return: null if not connected.

getInstance
^^^^^^^^^^^

.. java:method:: public static synchronized CDBClient getInstance(ParameterList parameters) throws CDBException
   :outertype: CDBClient

   Get CDB Client with specific construction parameters.

   :param parameters: (host, user, passwd, db, port, log_level, data_root). If any of the parameters is not specified, default value (from env. variables, etc.) is taken. Warning: This version of constructor is not recommended for general use. It doesn't use any clever method of preserving connections, memory, thread safety etc.

getRecordNumberFromFSEvent
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: public long getRecordNumberFromFSEvent(long eventNumber, String eventId)
   :outertype: CDBClient

getSignal
^^^^^^^^^

.. java:method:: public Signal getSignal(String strid, String variant) throws CDBException
   :outertype: CDBClient

getSignal
^^^^^^^^^

.. java:method:: public Signal getSignal(String strid) throws CDBException
   :outertype: CDBClient

getSignal
^^^^^^^^^

.. java:method:: public Signal getSignal(long recordNumber, long genericSignalId) throws CDBException
   :outertype: CDBClient

getSignal
^^^^^^^^^

.. java:method:: public Signal getSignal(ParameterList parameters) throws CDBException
   :outertype: CDBClient

getSignalCalibration
^^^^^^^^^^^^^^^^^^^^

.. java:method:: public SignalCalibration getSignalCalibration(String strId)
   :outertype: CDBClient

getSignalCalibration
^^^^^^^^^^^^^^^^^^^^

.. java:method:: public SignalCalibration getSignalCalibration(ParameterList parameters)
   :outertype: CDBClient

getSignalParameters
^^^^^^^^^^^^^^^^^^^

.. java:method:: public SignalParameters getSignalParameters(ParameterList parameters)
   :outertype: CDBClient

getSignalReference
^^^^^^^^^^^^^^^^^^

.. java:method:: public SignalReference getSignalReference(long recordNumber, long genericSignalId, long revision)
   :outertype: CDBClient

getSignalReference
^^^^^^^^^^^^^^^^^^

.. java:method:: public SignalReference getSignalReference(long recordNumber, long genericSignalId)
   :outertype: CDBClient

getSignalReference
^^^^^^^^^^^^^^^^^^

.. java:method:: public SignalReference getSignalReference(String strid) throws CDBException
   :outertype: CDBClient

getSignalReferences
^^^^^^^^^^^^^^^^^^^

.. java:method:: public List<SignalReference> getSignalReferences(ParameterList parameters)
   :outertype: CDBClient

insert
^^^^^^

.. java:method:: public Long insert(String tableName, ParameterList fields, Boolean returnInsertedId, Boolean checkStructure)
   :outertype: CDBClient

insert
^^^^^^

.. java:method:: public Long insert(String tableName, ParameterList fields, Boolean returnInsertedId)
   :outertype: CDBClient

insert
^^^^^^

.. java:method:: public Long insert(String tableName, ParameterList fields)
   :outertype: CDBClient

lastRecordNumber
^^^^^^^^^^^^^^^^

.. java:method:: public long lastRecordNumber(String recordType)
   :outertype: CDBClient

lastShotNumber
^^^^^^^^^^^^^^

.. java:method:: public long lastShotNumber()
   :outertype: CDBClient

   Last shot (or experimental record) number.

newDataFile
^^^^^^^^^^^

.. java:method:: public DataFile newDataFile(ParameterList parameters)
   :outertype: CDBClient

newDataFile
^^^^^^^^^^^

.. java:method:: public DataFile newDataFile(String collectionName, long recordNumber, long dataSourceId)
   :outertype: CDBClient

recordExists
^^^^^^^^^^^^

.. java:method:: public boolean recordExists(long recordNumber)
   :outertype: CDBClient

   Check whether a record exists.

setFileReady
^^^^^^^^^^^^

.. java:method:: public void setFileReady(long fileId)
   :outertype: CDBClient

storeLinearSignal
^^^^^^^^^^^^^^^^^

.. java:method:: public SignalReference storeLinearSignal(long genericSignalId, long recordNumber, double time0, double coefficient, double offset)
   :outertype: CDBClient

   Store signal of type LINEAR.

storeSignal
^^^^^^^^^^^

.. java:method:: public SignalReference storeSignal(ParameterList parameters)
   :outertype: CDBClient

storeSignal
^^^^^^^^^^^

.. java:method:: public SignalReference storeSignal(long genericSignalId, long recordNumber, String dataFileKey, long dataFileId, double time0, double coefficient, double offset, double coefficient_V2unit)
   :outertype: CDBClient

   Store signal of type FILE. Underlying Python method ensures that computer, board and channel ids are correctly set.

storeSignal
^^^^^^^^^^^

.. java:method:: public SignalReference storeSignal(long genericSignalId, long recordNumber, String dataFileKey, long dataFileId, double time0)
   :outertype: CDBClient

   Store signal of type FILE. Underlying Python method ensures that computer, board and channel ids are correctly set.

updateSignal
^^^^^^^^^^^^

.. java:method:: public void updateSignal(ParameterList parameters)
   :outertype: CDBClient

   Update signal.

   :param parameters: As few parameters as needed.

