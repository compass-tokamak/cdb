.. java:import:: cz.cas.ipp.compass.jycdb CDBClient

.. java:import:: cz.cas.ipp.compass.jycdb CDBException

.. java:import:: cz.cas.ipp.compass.jycdb GenericSignal

.. java:import:: cz.cas.ipp.compass.jycdb Signal

.. java:import:: java.util.logging Level

.. java:import:: java.util.logging Logger

.. java:import:: javax.swing JFrame

.. java:import:: org.jfree.chart ChartPanel

.. java:import:: org.jfree.chart JFreeChart

.. java:import:: org.jfree.chart.axis NumberAxis

.. java:import:: org.jfree.chart.plot XYPlot

.. java:import:: org.jfree.chart.renderer.xy SamplingXYLineRenderer

.. java:import:: org.jfree.chart.renderer.xy StandardXYItemRenderer

.. java:import:: org.jfree.data.xy DefaultXYDataset

DataPlotter
===========

.. java:package:: cz.cas.ipp.compass.jycdb.plotting
   :noindex:

.. java:type:: public class DataPlotter extends JFrame

Constructors
------------
DataPlotter
^^^^^^^^^^^

.. java:constructor:: public DataPlotter()
   :outertype: DataPlotter

Methods
-------
appendChart
^^^^^^^^^^^

.. java:method:: public static JFreeChart appendChart(JFreeChart oldChart, Signal signal) throws CDBException
   :outertype: DataPlotter

createChart
^^^^^^^^^^^

.. java:method:: public static JFreeChart createChart(Signal signal) throws CDBException
   :outertype: DataPlotter

main
^^^^

.. java:method:: public static void main(String[] args)
   :outertype: DataPlotter

