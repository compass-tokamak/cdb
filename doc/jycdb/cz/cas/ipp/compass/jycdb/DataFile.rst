.. java:import:: cz.cas.ipp.compass.jycdb.util DictionaryAdapter

.. java:import:: org.python.core PyObject

DataFile
========

.. java:package:: cz.cas.ipp.compass.jycdb
   :noindex:

.. java:type:: public class DataFile extends DictionaryAdapter

   Wrapper around rows of `data_files` table.

   :author: honza

Methods
-------
create
^^^^^^

.. java:method:: public static DataFile create(PyObject object)
   :outertype: DataFile

getCollectionName
^^^^^^^^^^^^^^^^^

.. java:method:: public String getCollectionName()
   :outertype: DataFile

getFileName
^^^^^^^^^^^

.. java:method:: public String getFileName()
   :outertype: DataFile

getFullPath
^^^^^^^^^^^

.. java:method:: public String getFullPath()
   :outertype: DataFile

   Full path to the file containing data.

getId
^^^^^

.. java:method:: public long getId()
   :outertype: DataFile

