.. java:import:: cz.cas.ipp.compass.jycdb CDBException

WrongDimensionsException
========================

.. java:package:: cz.cas.ipp.compass.jycdb.util
   :noindex:

.. java:type:: public class WrongDimensionsException extends CDBException

   :author: pipek

Constructors
------------
WrongDimensionsException
^^^^^^^^^^^^^^^^^^^^^^^^

.. java:constructor:: public WrongDimensionsException(String message)
   :outertype: WrongDimensionsException

