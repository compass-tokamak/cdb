.. java:import:: cz.cas.ipp.compass.jycdb CDBException

UnknownDataTypeException
========================

.. java:package:: cz.cas.ipp.compass.jycdb.util
   :noindex:

.. java:type:: public class UnknownDataTypeException extends CDBException

   :author: pipek

Constructors
------------
UnknownDataTypeException
^^^^^^^^^^^^^^^^^^^^^^^^

.. java:constructor:: public UnknownDataTypeException(String message)
   :outertype: UnknownDataTypeException

