.. java:import:: cz.cas.ipp.compass.jycdb CDBException

Hdf5WriteException
==================

.. java:package:: cz.cas.ipp.compass.jycdb.util
   :noindex:

.. java:type:: public class Hdf5WriteException extends CDBException

   An error when writing to HDF5.

   :author: pipek

Constructors
------------
Hdf5WriteException
^^^^^^^^^^^^^^^^^^

.. java:constructor:: public Hdf5WriteException(String message)
   :outertype: Hdf5WriteException

