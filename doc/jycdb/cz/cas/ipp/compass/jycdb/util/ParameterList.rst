.. java:import:: java.sql Date

.. java:import:: java.sql Timestamp

.. java:import:: java.util HashMap

.. java:import:: java.util Map

.. java:import:: org.python.core Py

.. java:import:: org.python.core PyDictionary

.. java:import:: org.python.core PyObject

ParameterList
=============

.. java:package:: cz.cas.ipp.compass.jycdb.util
   :noindex:

.. java:type:: public class ParameterList extends HashMap<String, PyObject>

   List of parameters for python methods that accepts (via put method) some of the basic types in addition to default PyObject. Various put methods just simplify adding of native objects.

Methods
-------
put
^^^

.. java:method:: public String put(String key, String value)
   :outertype: ParameterList

put
^^^

.. java:method:: public long put(String key, long value)
   :outertype: ParameterList

put
^^^

.. java:method:: public int put(String key, int value)
   :outertype: ParameterList

put
^^^

.. java:method:: public Date put(String key, java.sql.Date value)
   :outertype: ParameterList

put
^^^

.. java:method:: public boolean put(String key, boolean value)
   :outertype: ParameterList

put
^^^

.. java:method:: public double put(String key, double value)
   :outertype: ParameterList

put
^^^

.. java:method:: public Timestamp put(String key, Timestamp value)
   :outertype: ParameterList

put
^^^

.. java:method:: public short put(String key, short value)
   :outertype: ParameterList

put
^^^

.. java:method:: public ParameterList put(String key, ParameterList value)
   :outertype: ParameterList

