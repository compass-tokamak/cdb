.. java:import:: java.util Map

.. java:import:: org.python.core Py

.. java:import:: org.python.core PyException

.. java:import:: org.python.core PyObject

PythonAdapter
=============

.. java:package:: cz.cas.ipp.compass.jycdb.util
   :noindex:

.. java:type:: public class PythonAdapter

   Adapter of a PyObject that offers some shortcut methods for invoking etc. Motivation: The Jython API is not as elegant as would be desirable. However, with proper understanding of PyObject, this class would not be necessary.

   :author: pipek

Fields
------
pyObject
^^^^^^^^

.. java:field:: protected PyObject pyObject
   :outertype: PythonAdapter

Constructors
------------
PythonAdapter
^^^^^^^^^^^^^

.. java:constructor:: public PythonAdapter(PyObject object)
   :outertype: PythonAdapter

Methods
-------
getAttribute
^^^^^^^^^^^^

.. java:method:: public PyObject getAttribute(String name)
   :outertype: PythonAdapter

   Get the attribute of PyObject.

   :param name: Name of the attribute.
   :return: The attribute as PyObject or null (if attribute not present).

getPythonObject
^^^^^^^^^^^^^^^

.. java:method:: public PyObject getPythonObject()
   :outertype: PythonAdapter

   Get the unwrapped PyObject.

hasAttribute
^^^^^^^^^^^^

.. java:method:: public boolean hasAttribute(String name)
   :outertype: PythonAdapter

invoke
^^^^^^

.. java:method:: public PyObject invoke(String method, Map<String, PyObject> parameters)
   :outertype: PythonAdapter

   Call a method on PyObject with parameters.

invoke
^^^^^^

.. java:method:: public PyObject invoke(String method)
   :outertype: PythonAdapter

   Call a method on PyObject without parameters.

   :param method: Name of the method (callable attribute) to call on the object.
   :return: Result of the call as PyObject

