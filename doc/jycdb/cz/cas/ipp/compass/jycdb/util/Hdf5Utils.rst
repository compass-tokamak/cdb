.. java:import:: java.io File

.. java:import:: java.util Arrays

.. java:import:: java.util.logging Level

.. java:import:: java.util.logging Logger

.. java:import:: ncsa.hdf.hdf5lib H5

.. java:import:: ncsa.hdf.hdf5lib HDF5Constants

.. java:import:: ncsa.hdf.hdf5lib.exceptions HDF5Exception

.. java:import:: ncsa.hdf.hdf5lib.exceptions HDF5LibraryException

.. java:import:: ncsa.hdf.object Dataset

.. java:import:: ncsa.hdf.object Datatype

.. java:import:: ncsa.hdf.object.h5 H5File

Hdf5Utils
=========

.. java:package:: cz.cas.ipp.compass.jycdb.util
   :noindex:

.. java:type:: public class Hdf5Utils

   Utilities for reading/writing HDF5 files. Methods readData, writeData and writeData

   :author: pipek

Methods
-------
readFileData
^^^^^^^^^^^^

.. java:method:: public static Data readFileData(String filePath, String fileKey) throws UnknownDataTypeException, Hdf5ReadException, WrongDimensionsException
   :outertype: Hdf5Utils

   Read data from file. Up to 3-D arrays are supported. (Higher rank would require reimplementation of Data class)

   :param filePath: Path to the file.
   :param fileKey: Name of the dataset in the file.

writeData
^^^^^^^^^

.. java:method:: public static void writeData(String filePath, String fileKey, Data data) throws Hdf5WriteException, UnknownDataTypeException
   :outertype: Hdf5Utils

   Write (from beginning) new HDF5 dataset.

   :param filePath: Path to the file (may or may not exist).
   :param fileKey: Name of the dataset (error if it exists).
   :param data: Wrapped data (see class Data).

writeData
^^^^^^^^^

.. java:method:: public static void writeData(String filePath, String fileKey, Data data, long[] offset) throws UnknownDataTypeException, Hdf5WriteException
   :outertype: Hdf5Utils

   Write (or append) data to an existing HDF5 dataset.

   :param filePath: Path to the file.
   :param fileKey: Name of the (existing) dataset.
   :param data: Wrapped data (see class Data).
   :param offset: Offset (in dimensions) from which start with writing data. If offset == null, new data set is created. You have to be sure that your offset is correct. The dataset is enlarged if possible but you can overwrite existing data if you are not careful.

writeData
^^^^^^^^^

.. java:method:: public static void writeData(String filePath, String fileKey, int dataType, long[] dimensions, Object rawData, long[] offset) throws Hdf5WriteException
   :outertype: Hdf5Utils

   Write data to a HDF5 dataset (generic version). This generic version is available as a public method, still it is more convenient (and practical) to use writeData(String, String, Data) or writeData.

   :param filePath: Path to the file (may or may not exist).
   :param fileKey: Name of the dataset.
   :param dataType: HDF5 data type.
   :param dimensions: The dimensions of the data array.
   :param rawData: The data array of any format.
   :param offset: Offset of data (null => create new dataset)

