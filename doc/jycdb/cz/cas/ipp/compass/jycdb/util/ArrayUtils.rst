.. java:import:: java.lang.reflect Array

.. java:import:: java.util Arrays

ArrayUtils
==========

.. java:package:: cz.cas.ipp.compass.jycdb.util
   :noindex:

.. java:type:: public class ArrayUtils

   Utils for quick manipulation with (numeric) arrays. Warning: most methods work only on rectangular arrays.

   :author: pipek

Methods
-------
add
^^^

.. java:method:: public static double[] add(double[] array, double offset)
   :outertype: ArrayUtils

   Add a constant to all elements of an array.

   :return: a new array

asDoubleArray
^^^^^^^^^^^^^

.. java:method:: public static double[] asDoubleArray(Object array) throws UnknownDataTypeException
   :outertype: ArrayUtils

   Cast all values of an array to double (runtime version).

   :param array: Array of allowed type.
   :throws UnknownDataTypeException:

asDoubleArray
^^^^^^^^^^^^^

.. java:method:: public static double[] asDoubleArray(long[] array)
   :outertype: ArrayUtils

   Cast all values of an array to double.

asDoubleArray
^^^^^^^^^^^^^

.. java:method:: public static double[] asDoubleArray(int[] array)
   :outertype: ArrayUtils

   Cast all values of an array to double.

asDoubleArray
^^^^^^^^^^^^^

.. java:method:: public static double[] asDoubleArray(short[] array)
   :outertype: ArrayUtils

   Cast all values of an array to double.

asDoubleArray
^^^^^^^^^^^^^

.. java:method:: public static double[] asDoubleArray(float[] array)
   :outertype: ArrayUtils

   Cast all values of an array to double.

asIntArray
^^^^^^^^^^

.. java:method:: public static int[] asIntArray(long[] array)
   :outertype: ArrayUtils

   Cast all values of an array to int.

asLongArray
^^^^^^^^^^^

.. java:method:: public static long[] asLongArray(int[] array)
   :outertype: ArrayUtils

   Cast all values of an array to long.

dimensions
^^^^^^^^^^

.. java:method:: public static long[] dimensions(Object array) throws WrongDimensionsException
   :outertype: ArrayUtils

   Get the dimensions along all axes of an array. Works only on rectangular arrays.

get
^^^

.. java:method:: public static Object get(Object array, int[] index)
   :outertype: ArrayUtils

   Get an element from a multidimensional array. Works on all arrays.

linearTransform
^^^^^^^^^^^^^^^

.. java:method:: public static double[] linearTransform(double[] array, double multiplyBy, double add)
   :outertype: ArrayUtils

   Apply a linear transformation y = ax + b on an array.

   :param multiplyBy: Multiplication constant
   :param add: Addition constant
   :return: a new array

linearTransform
^^^^^^^^^^^^^^^

.. java:method:: public static Object linearTransform(Object array, double multiplyBy, double add) throws UnknownDataTypeException
   :outertype: ArrayUtils

   A general linear transformation of multidimensional array.

   :param array: - N-dimensional rectangular array of double/long/int/short/float
   :param multiplyBy: - multiplicative factor
   :param add: - additive factor
   :return: - N-dimensional array of the same shape as input

max
^^^

.. java:method:: public static double max(double[] array)
   :outertype: ArrayUtils

   Maximum value found in the array.

min
^^^

.. java:method:: public static double min(double[] array)
   :outertype: ArrayUtils

   Minimum value found in the array.

multiply
^^^^^^^^

.. java:method:: public static double[] multiply(double[] array, double coefficient)
   :outertype: ArrayUtils

   Multiply all elements of an array by a constant.

   :return: a new array

product
^^^^^^^

.. java:method:: public static long product(int[] array)
   :outertype: ArrayUtils

product
^^^^^^^

.. java:method:: public static long product(long[] array)
   :outertype: ArrayUtils

range
^^^^^

.. java:method:: public static int[] range(int xmin, int xmax)
   :outertype: ArrayUtils

rank
^^^^

.. java:method:: public static int rank(Object array) throws WrongDimensionsException
   :outertype: ArrayUtils

   Get the number of dimensions of an array. Works only on rectangular arrays.

reshape
^^^^^^^

.. java:method:: public static Object reshape(Object array, int[] newDimensions) throws WrongDimensionsException
   :outertype: ArrayUtils

   Reshape array. Create a new array with the same total size but with different dimensions. Last index is moving fastest, while the elements are copied one after another. Works only on rectangular arrays.

   :param array:
   :param newDimensions:
   :return: new array

set
^^^

.. java:method:: public static void set(Object array, int[] index, Object value)
   :outertype: ArrayUtils

   Set an element in a multidimensional array. Works on all arrays.

size
^^^^

.. java:method:: public static long size(Object array) throws WrongDimensionsException
   :outertype: ArrayUtils

   Total number of items in array. Works only on rectangular arrays.

