.. java:import:: cz.cas.ipp.compass.jycdb CDBException

Hdf5ReadException
=================

.. java:package:: cz.cas.ipp.compass.jycdb.util
   :noindex:

.. java:type:: public class Hdf5ReadException extends CDBException

   :author: pipek

Constructors
------------
Hdf5ReadException
^^^^^^^^^^^^^^^^^

.. java:constructor:: public Hdf5ReadException(String message)
   :outertype: Hdf5ReadException

