.. java:import:: java.lang.reflect Array

.. java:import:: java.lang.reflect InvocationTargetException

.. java:import:: java.lang.reflect Method

.. java:import:: java.util Arrays

.. java:import:: java.util.logging Level

.. java:import:: java.util.logging Logger

Data
====

.. java:package:: cz.cas.ipp.compass.jycdb.util
   :noindex:

.. java:type:: public class Data

   Wrapper around data. It enables us to live with just one copy of most methods and pass data around independently of its dimension and numerical type. It can be constructed using: 1) one of the 6x3 constructors with explicit array types 2) a general constructor using Object and dimensionality data specified by you.

   :author: pipek

Fields
------
DOUBLE
^^^^^^

.. java:field:: public static final int DOUBLE
   :outertype: Data

FLOAT
^^^^^

.. java:field:: public static final int FLOAT
   :outertype: Data

INT16
^^^^^

.. java:field:: public static final int INT16
   :outertype: Data

INT32
^^^^^

.. java:field:: public static final int INT32
   :outertype: Data

INT64
^^^^^

.. java:field:: public static final int INT64
   :outertype: Data

INT8
^^^^

.. java:field:: public static final int INT8
   :outertype: Data

MAX_RANK
^^^^^^^^

.. java:field:: public static final int MAX_RANK
   :outertype: Data

Constructors
------------
Data
^^^^

.. java:constructor:: public Data(byte[] data)
   :outertype: Data

Data
^^^^

.. java:constructor:: public Data(byte[][] data)
   :outertype: Data

Data
^^^^

.. java:constructor:: public Data(byte[][][] data)
   :outertype: Data

Data
^^^^

.. java:constructor:: public Data(short[] data)
   :outertype: Data

Data
^^^^

.. java:constructor:: public Data(short[][] data)
   :outertype: Data

Data
^^^^

.. java:constructor:: public Data(short[][][] data)
   :outertype: Data

Data
^^^^

.. java:constructor:: public Data(int[] data)
   :outertype: Data

Data
^^^^

.. java:constructor:: public Data(int[][] data)
   :outertype: Data

Data
^^^^

.. java:constructor:: public Data(int[][][] data)
   :outertype: Data

Data
^^^^

.. java:constructor:: public Data(long[] data)
   :outertype: Data

Data
^^^^

.. java:constructor:: public Data(long[][] data)
   :outertype: Data

Data
^^^^

.. java:constructor:: public Data(long[][][] data)
   :outertype: Data

Data
^^^^

.. java:constructor:: public Data(double[] data)
   :outertype: Data

Data
^^^^

.. java:constructor:: public Data(double[][] data)
   :outertype: Data

Data
^^^^

.. java:constructor:: public Data(double[][][] data)
   :outertype: Data

Data
^^^^

.. java:constructor:: public Data(float[] data)
   :outertype: Data

Data
^^^^

.. java:constructor:: public Data(float[][] data)
   :outertype: Data

Data
^^^^

.. java:constructor:: public Data(float[][][] data)
   :outertype: Data

Data
^^^^

.. java:constructor:: public Data(Object data, int type, long[] dimensions) throws WrongDimensionsException
   :outertype: Data

   Generic constructor.

   :param data: The correct array object (or null)
   :param type: Type of data in terms of this class constants.
   :param dimensions: Length along all dimensions. Gets copied. This is useful if we obtain data from external source, we know its properties but we don't want to cast them unnecessarily (like read from HDF5). Data needn't be specified (in such case a new array is created.)

Methods
-------
asDoubleArray1D
^^^^^^^^^^^^^^^

.. java:method:: public double[] asDoubleArray1D() throws WrongDimensionsException, UnknownDataTypeException
   :outertype: Data

   Get a 1-D double array representation of data. Works for 1-D data. For doubles, it simply returns, for others, it converts them using ArrayUtils.asDoubleArray() (see).

   :throws WrongDimensionsException: if data are not 1-D.
   :throws UnknownDataTypeException: if conversion to double is not supported by underlying procedure.

createRawObject
^^^^^^^^^^^^^^^

.. java:method:: public static Object createRawObject(int type, long[] dimensions) throws WrongDimensionsException
   :outertype: Data

   Create a multidimensional array object of a selected type and dimensions.

flatten
^^^^^^^

.. java:method:: public boolean flatten() throws WrongDimensionsException
   :outertype: Data

   Remove dimensions that have length 1. Preserves 1D arrays. Makes changes only if there is a trivial dimension.

   :return: true if there was a change, false otherwise. Motivation: Our HDF5 file sometimes have Nx1 arrays instead of N arrays.

from1DArray
^^^^^^^^^^^

.. java:method:: public static Data from1DArray(Object array, int type, long[] dimensions) throws WrongDimensionsException
   :outertype: Data

   Reshapes 1-D array to a multidimensional array of correct dimensions. Motivation: HDF5 library returns only 1-D arrays. In C order.

getDimensions
^^^^^^^^^^^^^

.. java:method:: public long[] getDimensions()
   :outertype: Data

   Get length of the data along all dimensions.

getRank
^^^^^^^

.. java:method:: public int getRank()
   :outertype: Data

   Get the number of dimensions. i.e. double[] => 1, short[][] => 2 etc.

getRawData
^^^^^^^^^^

.. java:method:: public Object getRawData()
   :outertype: Data

   Get the original array used for the initialization of this object. No type information, you have to cast it yourself.

getType
^^^^^^^

.. java:method:: public int getType()
   :outertype: Data

   Get the type of stored array (in terms of constants defined in this class). Our data types differ from HDF5 so that this class is not dependent on HDF5 library. Hdf5Utils class contains conversion routines.

linearTransform
^^^^^^^^^^^^^^^

.. java:method:: public Data linearTransform(double mul, double add) throws WrongDimensionsException, UnknownDataTypeException
   :outertype: Data

