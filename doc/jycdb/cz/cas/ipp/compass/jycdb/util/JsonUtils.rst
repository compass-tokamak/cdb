.. java:import:: com.google.gson JsonArray

.. java:import:: com.google.gson JsonElement

.. java:import:: com.google.gson JsonObject

.. java:import:: com.google.gson JsonParser

.. java:import:: com.google.gson JsonPrimitive

.. java:import:: java.util Map

.. java:import:: java.util SortedMap

.. java:import:: java.util TreeMap

JsonUtils
=========

.. java:package:: cz.cas.ipp.compass.jycdb.util
   :noindex:

.. java:type:: public class JsonUtils

   Utility to read and write data from/to JSON format.

   :author: pipek

Methods
-------
asNative
^^^^^^^^

.. java:method:: public static Object asNative(JsonElement element)
   :outertype: JsonUtils

   Transform JSON element into native object. Works recursively: - double, boolean, string - native - arrays => Object[] - objects => TreeMap

parseNative
^^^^^^^^^^^

.. java:method:: public static Object parseNative(String source)
   :outertype: JsonUtils

   Take JSON string and turn it into native objects.

