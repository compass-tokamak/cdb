.. java:import:: java.util.logging Level

.. java:import:: java.util.logging Logger

DebugUtils
==========

.. java:package:: cz.cas.ipp.compass.jycdb.util
   :noindex:

.. java:type:: public class DebugUtils

   Utilities helping with debugging.

   :author: pipek

Methods
-------
logWithDuration
^^^^^^^^^^^^^^^

.. java:method:: public static void logWithDuration(String message)
   :outertype: DebugUtils

   Print a message alongside with the number of nanoseconds since last call of this method.

   :param message: Message to log

logWithDuration
^^^^^^^^^^^^^^^

.. java:method:: public static void logWithDuration(String message, Logger logger)
   :outertype: DebugUtils

   Print a message alongside with the number of nanoseconds since last call of this method.

   :param message: Message to log
   :param logger: Logger to use

