.. java:import:: java.util Map

.. java:import:: java.util Set

.. java:import:: java.util SortedMap

.. java:import:: java.util TreeMap

.. java:import:: org.python.core Py

.. java:import:: org.python.core PyObject

.. java:import:: org.python.util PythonInterpreter

PythonUtils
===========

.. java:package:: cz.cas.ipp.compass.jycdb.util
   :noindex:

.. java:type:: public class PythonUtils

   Utilities for easier manipulation with Python through Jython.

   :author: pipek

Methods
-------
asNative
^^^^^^^^

.. java:method:: public static Object asNative(PyObject object)
   :outertype: PythonUtils

   Interpret the python object as a native Java one.

   :return: Object of the correct type. It has to be cast to be useful. Numbers converts to numbers (long or double). Dicts converts to TreeMap. Tuples and lists converts to Object[]. Dates converts to Date. NoneType converts to null. In case it does not know a type, it returns its string representation along with ! and type name. (e.g. "!representation of my weird type{WeirdType}")

getInterpreter
^^^^^^^^^^^^^^

.. java:method:: public static synchronized PythonInterpreter getInterpreter()
   :outertype: PythonUtils

   Get a single copy of python interpreter. More of them should not be needed.

invoke
^^^^^^

.. java:method:: public static PyObject invoke(PyObject object, String method, Map<String, PyObject> parameters)
   :outertype: PythonUtils

   Invoke a method on a Python object with named parameters. It calls `invoke` method present in Jython, it only allows easier manipulation with parameters (enabling this to be called with ParameterList).

newObject
^^^^^^^^^

.. java:method:: public static PyObject newObject(String className, Map<String, PyObject> parameters)
   :outertype: PythonUtils

   Create a new instance of a named python class.

