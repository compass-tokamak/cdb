.. java:import:: java.util SortedMap

.. java:import:: org.python.core Py

.. java:import:: org.python.core PyObject

DictionaryAdapter
=================

.. java:package:: cz.cas.ipp.compass.jycdb.util
   :noindex:

.. java:type:: public class DictionaryAdapter extends PythonAdapter

   Object that is represented by a python dictionary (or OrderedDict) and usually obtained as a row from database. Each of the classes should implement static method create(PyObject) which returns null if underlying Python object is None. There should be no public constructor in child classes.

Constructors
------------
DictionaryAdapter
^^^^^^^^^^^^^^^^^

.. java:constructor:: public DictionaryAdapter(PyObject object)
   :outertype: DictionaryAdapter

Methods
-------
asMap
^^^^^

.. java:method:: public SortedMap asMap()
   :outertype: DictionaryAdapter

   Map python dictionary to java map. Motivation: Binding for MATLAB.

get
^^^

.. java:method:: public PyObject get(String key)
   :outertype: DictionaryAdapter

   Get dictionary value indexed by key as python object.

getDictKeys
^^^^^^^^^^^

.. java:method:: public String[] getDictKeys()
   :outertype: DictionaryAdapter

   Get list of dictionary keys. Motivation: Binding for IDL.

   :return: Array of keys in the order they are stored in by Python.

getDictValue
^^^^^^^^^^^^

.. java:method:: public Object getDictValue(String key)
   :outertype: DictionaryAdapter

   Get dictionary value indexed by key as "Java native" object. Motivation: Binding for IDL.

   **See also:** :java:ref:`PythonUtils.asNative(org.python.core.PyObject)`

getDictValueAsDouble
^^^^^^^^^^^^^^^^^^^^

.. java:method:: public double getDictValueAsDouble(String key)
   :outertype: DictionaryAdapter

   Get dictionary value indexed by key as double. Motivation: Binding for IDL.

getDictValueAsLong
^^^^^^^^^^^^^^^^^^

.. java:method:: public long getDictValueAsLong(String key)
   :outertype: DictionaryAdapter

   Get dictionary value indexed by key as long. Motivation: Binding for IDL.

getDictValueAsString
^^^^^^^^^^^^^^^^^^^^

.. java:method:: public String getDictValueAsString(String key)
   :outertype: DictionaryAdapter

   Get dictionary value indexed by key as string. Motivation: Binding for IDL.

