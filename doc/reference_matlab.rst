.. _matlab-cdb-reference:

Matlab CDB reference
====================

.. class:: cdb_client()

  CDB client class.

  .. method:: get_signal(str_id)

    :param str_id: CDB string id (see :ref:`signal-ids`)

    Get CDB signal by string id.
     
    Returns a structure withthe CDB signal, including the data and the description (references).

Matlab errors
-------------

- ``CDB:JyCDBError``
- ``CDB:SignalError``
- ``CDB:InputError``
- ``CDB:StoreError``
