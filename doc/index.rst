
.. pyCDB documentation master file, created by
   sphinx-quickstart on Wed May 18 21:31:20 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CDB documentation!
=================================

Contents:

.. toctree::
   :maxdepth: 3

   description
   installation
   configuration
   usage
   CDB_primer_for_admins.ipynb
   reference
   reference_matlab
   firesignal
   firesignalCOMPILATION


JyCDB
=====
.. toctree::
   :maxdepth: 3

   jycdb/packages

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

