=============
Configuration
=============

CDB configuration parameters are sought in:

#. environment variables

#. ./.CDBrc

#. $HOME/.CDBrc

CDBrc file explanation, environment variables have the same names::

	# CDB configuration file
	#
	# Edit and save to .CDBrc
	#
	# Possible directories, ordered by ascending priority
	# - $CDB_PATH/..
	# - user home directory
	# - current directory
	# - alternatively, CDB can be configured with environment variables

	# Database configuration
	[database]
	# Root data directory
	CDB_DATA_ROOT = /var/local/CDB

	# MySQL host name or IP (:port)
	CDB_HOST = localhost
	# MySQL user name
	CDB_USER = CDB_test
	# MySQL password
	CDB_PASSWORD = cdb_data
	# MySQL database name
	CDB_DB = CDB_test

	# Logging configuration
	[logging]
	# Screen logging level: INFO, DEBUG, WARNING, ERROR, NOTSET
	CDB_LOG_LEVEL = WARNING
	# File log file name
	CDB_LOG_FILE =
	# File log level
	CDB_FILE_LOG_LEVEL = INFO

	# logbook configuration (optional)
	[logbook]
	CDB_LOGBOOK_HOST = localhost
	CDB_LOGBOOK_USER = CDB
	CDB_LOGBOOK_PASSWORD = cdb_data
	CDB_LOGBOOK_DB = logbook

    # Continuous signal configuration (optional)
	[continuous_signal]
	#CDB_EPICS_ARCHIVER_APPLIANCE_URL = http://epicstest.tok.ipp.cas.cz:17668
