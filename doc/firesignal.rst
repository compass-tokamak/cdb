=====================================
Installation of FireSignal with pyCDB
=====================================

Installation steps
------------------
.. index::
   single: execution; context
   module: __main__
   module: sys
   triple: module; search; path

INSTALLATION OF POSTGRES DB: 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Exactly follow the instuction at IPFN IST web page: http://metis.ipfn.ist.utl.pt/index.php?title=CODAC/FireSignal/Databases/PostgreSQL_%2b_FileSystem

* Useful commands for wokr witl POSTGRES:

 * sudo -u postgres psql 
 * \\d 
 * \\l 
 * \\c genericdb 
 * SELECT * FROM events;
 * \\q 


HOW TO CREATE NEW CDB DATABASE?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

mysql -u root -p

GRANT ALL PRIVILEGES ON *.* TO 'CDB'@'localhost' WITH GRANT OPTION;
CREATE USER 'CDB'@'localhost' IDENTIFIED BY 'cmpsSQLdata' ;

mysql -h localhost -u CDB -p < localhost.sql
    with password in “.CDBrc” file

TO CHECK IT:    connect CDB
        show tables; 

copy file .CDBrc to your home directory

mkdir /home/rrr/CDB_data

FINAL CHECK of SUCCESFULL OPERATION OF pyCDB:
python pyCDB.py    → should some nice draw graph




INSTALLATION OF FIRESIGNAL:
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Source:
svn co svn://baco.ipfn.ist.utl.pt/scad/trunk

/trunk/java/dist$ sudo ./firesignal-1.1-linux-installer.bin

IN INSTALLER:
installed to: /home/rrr/FS_PT
locate javac  
/home/rrr/FS/jdk1.6.0_27/jre/       ..better to use sun's implementation
10.136.245.226
1050
PostgreSQL + filesystem
/home/rrr/CDB_myDATA
Database(Posgres)
genericdbadmin
xxx
genericdb
localhost
5432

TO RUN CENTRAL SERVER:
^^^^^^^^^^^^^^^^^^^^^^
sudo ./fsignal start

etc. (e.g sudo ./fsignal_test_node start)

INSTALLATION OF FIRESIGNAL GUI CLIENT:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

sudo ./firesignaljws-1.1-linux-installer.bin

/usr/share/mini-httpd/html/XXX

http://localhost/XXX    – improve setting of http server
127.0.0.1 or localhost
1050
ok to the rest

TO RUN IT:

/home/rrr/FS/jdk1.6.0_27/jre/bin/javaws FireSignalClient.jnlp

sudo ./fsignal_test_node start
Burn button → OK → you should see the data in the “Data” panel

HDF5 plugin:
^^^^^^^^^^^^

jep-2.4     compilovat s original java

change scripts :
    /home/rrr/firesignal-1.1/dbcontroller/DBScript 
    /home/rrr/firesignal-1.1/server/FSServerScript

    4x change IP
    2x export LD_PRELOAD=/usr/lib/libpython2.6.so.1.0  ( P by which was compiled jep, ldd..)
    2x FS_PT path
    1x #:../libs/libjhdf5.so
    Xx parth firesignal-1.1 ...

copy libraries:
    sudo cp /home/rrr/firesignal-1.1/libs/jhdf5.jar /home/rrr/FS_PT/libs/ 
    sudo cp /home/rrr/firesignal-1.1/libs/libjhdf5.so /home/rrr/FS_PT/libs/ 
    sudo cp /home/rrr/firesignal-1.1/libs/hdf5test.jar /home/rrr/FS_PT/libs/  → fsPqsqlHDF5.jar in mail 19.10.2011
    sudo cp /home/rrr/firesignal-1.1/libs/jep.jar /home/rrr/FS_PT/libs/

rrr@rrr-laptop:~/FS_PT/init$ sudo ./fsignal start


TO GET INFOS ABOUT START OF FS:
either see the logs 

or

sudo gedit ../server/StartFSServer &
comment lines:
/home/rrr/FS_PT/server/FSServerScript \\
#1>>/dev/null \\
#2>>/dev/null
        
         sudo gedit ../dbcontroller/StartDBController &

Nota Bene: How many instances of FSs are running?
-------------------------------------------------

If you experience some very strange/unexpected behaviout of FS mybe the error can be cause by that more than one instance of firesignal are sunnig.

ps aux | grep java
