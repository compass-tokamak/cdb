=======
 Usage
=======

Reading data
------------

Basic example in Python
^^^^^^^^^^^^^^^^^^^^^^^

First import pyCDB and connect to the database::

    from pyCDB.client import CDBClient
    cdb = CDBClient()

Retrieve references for some generic signal::

    generic_signal_name = 'electron density'
    # get the full generic signal reference (all columns)
    generic_signal_refs = cdb.get_generic_signal_references(generic_signal_name = generic_signal_name)
    # get signal references 
    signal_refs = cdb.get_signal_references(record_number=-1,generic_signal_id=generic_signal_refs[0]['generic_signal_id'])

Now get the signal data, including description and axes, using :func:`pyCDB.client.CDBClient.get_signal`::

    sig = cdb.get_signal(signal_ref=signal_refs[-1])

Matlab
^^^^^^

Use :class:`cdb_client` to instantiate a client::

	cdb = cdb_client();

To get the signal data including axes use :func:`cdb_client.cdb_get_signal` ::

	signal = cdb.get_signal(str_id)

.. _signal-ids:

Signal id's
^^^^^^^^^^^

*Generic signal* is uniquely identified either by

* numeric id (generic_signal_id) 
* alias + record number
* name (generic_signal_name) + data source id + record number

CDB interface supports generic signal string id's in the following forms:

* *alias_or_name* - search by alias first, if no match is found search by name (>1 results possible)
* *name/data_source_id* - generic signal name followed by '/' and data_source_id (data source name or numeric id)
* *generic_signal_id* - numeric id of the generic signal

See also :func:`pyCDB.client.decode_generic_signal_strid`.

*Data signal* (single data set) is uniquely identified by

* generic signal id (numeric) + record number + revision

Data signal string id is in the form of:: 

  id_type:generic_signal_string_id:record_number:revision[units]

For example, this refers to EFIT psi 2D, shot 4073, last revision::

  CDB:psi_2D:4073:-1[default]

which is equivalent to::

  psi_2D:4073

See also :func:`pyCDB.client.decode_signal_strid`.

.. _linear-signals-description:

Linear signals with get_signal_data
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Described below is the logic for linear signals, implemented in :func:`pyCDB.client.CDBClient.get_signal_data`.

* number of axes > 1 --> **unresolved**
* number of axes = 1

  * axis is linear

    * axis is time_axis

      * time_limit provided: n_samples = int(ceil((time_limit - offset) / coefficient))
      * n_samples not provided --> **unresolved**
      * **result** = offset + coefficient*(axis_data[0..n_samples] - time0)

    * axis is not time_axis

      * n_samples not provided --> **unresolved**
      * **result** = offset + coefficient*axis_data[0..n_samples]

  * axis contains data

    * axis is time_axis

      * **result** = offset + coefficient*(axis_data - time0)

    * axis is not time_axis

      * **result** = offset + coefficient*axis_data

* number of axes = 0

  * n_samples and x0 provided (x0 is optional, defaults to 0)
    
    * **result** = x0 + [0,1, .., n_samples-1]*coefficient

  * x0 and x1 provided
    
    * **result** = [x0, x0+coefficient, x0+2*coefficient, .., x1]

  * otherwise
  
    * **result = function**:  f(i) = offset + coefficient * i


Writing data
------------

First create a new data file record (go to the last step for liner signals),
providing data source id, record number and collection (a base name
for the data file chosen by the user)::

    file_ref = cdb.new_data_file(collection_name, data_source_id =
               data_source_id, record_number = record_number, \
	       file_format = "HDF5")

Now you can create the file and fill with data, e.g.::

    import h5py
    fh5 = h5py.File(file_ref['full_path'],'w')
    grp_name = 'raw data'
    data_file_key = grp_name + '/' + generic_signal_name
    f_grp = fh5.create_group(grp_name)
    f_grp.create_dataset(generic_signal_name, data=numpy_data)
    fh5.close()

One has to say when the file is ready (for reading)::

    cdb.set_file_ready(file_ref['data_file_id'])

Finally, the CDB database must know what signals are stored in the
file. Linear signals are created solely by this step::

    cdb.store_signal(generic_signal_id, \
                       record_number=record_number, \
                       data_file_id=file_ref['data_file_id'], \
		       data_file_key = data_file_key,\
                       offset=0.5, coefficient=1.3, time0=-0.2, \
		       computer_id=1, board_id=1, channel_id=1, \
                       note='my first stored signal')
