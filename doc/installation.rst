============
Installation
============

Dependencies
------------
For pyCDB

* Python 2.7+
* MySQLdb or pymysql
* numpy
* h5py
* pint
* matplotlib (for the example only)

For JyCDB - Java, Matlab, IDL clients

* Jython

For cyCDB (the C-interface)

* Cython (tested with versions 0.14 and 0.15)

Clone CDB repository
--------------------
CDB's Mercurial repository is hosted at `bitbucket.org/compass-tokamak/cdb
<https://bitbucket.org/compass-tokamak/cdb>`_. Simply clone it via

.. code-block:: shell

	hg clone https://bitbucket.org/compass-tokamak/cdb


pyCDB
-----
Just place pyCDB on Python path and ``import pyCDB``

If pyCDB is not on Python path, it is possible to modify the path:

.. code-block:: shell

	export CDB_PATH=/path/to/cdb/src && export PYTHONPATH=$PYTHONPATH:/path/to/cdb/src


cyCDB
-----
Run ``make`` in the src directory. This step is optional.

Database
--------
CDB needs MySQL database. Import *database/mysql_setup.sql* to create
the *CDB* database structure, run

.. code-block:: shell

	mysql -u root -p < database/CDB_schema.sql

To create CDB users and the testing database (modify passwords for production use), run

.. code-block:: shell

	mysql -u root -p < CDB_users.sql
