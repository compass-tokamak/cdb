#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: Jakub Urban
# @Date:   2015-04-02 15:10:51
# @Last Modified by:   Jakub Urban
# @Last Modified time: 2015-05-20 11:38:47

from pyCDB.client import CDBClient, CDBSignal
# import logging
import os
import numpy as np
import re
from collections import OrderedDict
from pyCDB.slicing import str_to_slice
import time


# def _logger():
#     logger = logging.getLogger('MDSplus')
#     logger.propagate = False
#     while logger.handlers:
#         logger.removeHandler(logger.handlers[-1])
#     logger.setLevel(logging.DEBUG)
#     # create console handler with a higher log level
#     ch = logging.StreamHandler()
#     ch.setLevel(logging.DEBUG)
#     # create formatter and add it to the handlers
#     formatter = logging.Formatter('%(asctime)s - %(name)-12s: %(levelname)-8s %(message)s')
#     ch.setFormatter(formatter)
#     logger.addHandler(ch)
#     # create file handler which logs even debug messages
#     fh = logging.FileHandler(os.path.expanduser('~/mdsplus.log'))
#     fh.setLevel(logging.DEBUG)
#     formatter = logging.Formatter('%(asctime)s - %(name)s:%(funcName)s:%(lineno)d: %(levelname)-8s %(message)s')
#     fh.setFormatter(formatter)
#     # add the handlers to the logger
#     logger.addHandler(fh)
#     return logger

# logger = _logger()

def axis_str_to_index(_str):
    """ Gets index of axis by its name """
    index = 0
    if _str[-1].isdigit():
        index = int(_str[-1])
    return index

class myLogger(object):
    text = ""

    def __init__(self, name="MDSplus"):
        self.name = name
        self.text = ""

    def debug(self, text):
        msg = "{} {} {}: {}".format(self.name, time.ctime(), "debug", text)
        print(msg)
        self.text += msg
        self.text += "\n"

    def error(self, text):
        msg = "{} {} {}: {}".format(self.name, time.ctime(), "error", text)
        print(msg)
        self.text += msg
        self.text += "\n"

    def __del__(self):
        try:
            log_file = open("mdsplus.log", "w")
            log_file.write(self.text)
            log_file.close()
            print("Successfully written...")
        except IOError as inst:
            print("I/O error({}): {}".format(inst.args[0], inst.args[1]))


logger = myLogger("MDSPlus")


class Connection(object):

    """MDSplus-like Connection class"""

    _cdb = None

    @property
    def cdb(self):
        if self.__class__._cdb is None:
            self.__class__._cdb = CDBClient(log_level=10)
        return self.__class__._cdb

    # special commands regular expressions
    # TODO add exceptions here (?)
    _re = OrderedDict()
    _re['shot'] = re.compile(r'_shot ?= ?([0-9]+)')
    variables = {}
    def __init__(self,  hostspec):
        logger.debug('Connection({})'.format(hostspec))
        super(Connection, self).__init__()
        self.hostspec = hostspec
        self.shot = None
        self.tree = None
        self.path = None
        self.signal = None
        self.str_ind = None
        self.last_signal = None
        self.last_str_ind = None

    def __setattr__(self, name, value):
        logger.debug("setting {}={}".format(name, value))
        super(Connection, self).__setattr__(name, value)

    def openTree(self, tree, shot):
        logger.debug('Connection.openTree({}, {})'.format(tree, shot))
        self.tree = tree
        self.shot = shot

    def setDefault(self, path):
        logger.debug('Connection.setDefault({})'.format(path))
        self.path = path

    def get(self, expr, *args, **kwargs):
        logger.debug('Connection.get({},{})'.format(expr, args))
        logger.debug('args: {}'.format(args))
        # try to set shot from variables
        try:
            self.shot = self.variables["_shot"]
        except KeyError:
            pass
        # TODO detect an evaluate expressions??
        # first try to extract brackets
        self.str_ind = None
        # Here, we can handle shitty commands with request in args
        if len(args) > 0 and expr.startswith("_piscopevar"):
            expr = args[0]
        m = re.match(r'(.*)\[(.*)\]', expr)
        if m is not None:
            expr = m.group(1)
            self.str_ind = m.group(2)
            logger.debug(
                "brackets detected in expression: [{}]".format(self.str_ind))
        logger.debug("expr: {}".format(expr))
        # default return value
        data = Data(1)
        for case, rep in self._re.items():
            # special commands are treated here
            m = re.match(rep, expr)
            if not m:
                continue
            if case == 'shot':
                self.shot = int(m.groups()[0])
                logger.debug('shot set to {}'.format(self.shot))
                break
        else:
            # case of no special command --> get_signal
            if ('()' in expr or
                    'reset_' in expr):
                # pass
                logger.debug('non-CDB expr detected, returning 1')
                logger.debug('expr: {}'.format(expr))
                logger.debug('args: {}'.format(args))
            elif expr.startswith('_'):
                logger.debug("variables detected: {}".format(expr))
                for e in expr.split(";"):
                    if "=" in e:
                        key, val = e.split("=")
                        key = key.strip()
                        val = val.strip()
                        self.variables[key.strip()] = val.strip()
                        logger.debug("Adding variable: {} = {}".format(key, val))
                        if key.strip() == "_shot":
                            self.shot = int(val)
                            logger.debug("Shot set to {}".format(self.shot))
                    else:
                        logger.debug("Returning vatiable {} = {}".format(key, self.variables.get(key)))
                        data = Data(self.variables.get(key, 1))
                logger.debug("piscope vars: {}".format(self.variables))
            # setting the shot number
            elif self.shot is None:
                logger.error('shot = None')
                raise Exception('shot = none in get')
            # happens if only y or z data are specified
            elif expr == "dim_of(_piscopevar)":
                logger.debug("x_expr")
                try:
                    sig = self.cdb.get_signal(self.last_signal)
                    # axis2 should be first because of 2D data
                    if hasattr(sig, "axis2"):
                        sig = getattr(sig, "axis2")
                    elif hasattr(sig, "time_axis"):
                        sig = getattr(sig, "time_axis")
                    else:
                        pass
                    if self.last_str_ind is None:
                        self.last_str_ind = "..."
                    slices = self.last_str_ind.split(",")
                    logger.debug("SLICES: {}".format(slices))
                    slices_digit = [i for i in slices if i.isdigit()]
                    slices_nondigit = [i for i in slices if not i.isdigit()]
                    if sig.data.ndim == 1:
                        str_axis = slices_nondigit[0]
                    elif sig.data.ndim == 2:
                        str_axis = "{},{}".format(
                            slices[0], slices_nondigit[1])
                    else:
                        raise Exception(
                            "3D axes are not supported, please slice them manually")
                    logger.debug("STR_AXIS DEDUCED: {}".format(str_axis))
                    data = Data(sig, str_axis)
                    if self.str_ind is not None:
                        self.last_str_ind = self.str_ind
                # This is too broad!!!!!!!!!!
                except Exception:
                    pass
            # again, this happens if only z data are filled in mds session
            # ploting image/contour
            elif expr == "dim_of(_piscopevar,1)":
                logger.debug("y_expr")
                try:
                    sig = self.cdb.get_signal(self.last_signal)
                    sig = getattr(sig, "axis1")
                    if self.last_str_ind is None:
                        self.last_str_ind = "..."
                    slices = self.last_str_ind.split(",")
                    logger.debug("SLICES: {}".format(slices))
                    slices_digit = [i for i in slices if i.isdigit()]
                    slices_nondigit = [i for i in slices if not i.isdigit()]
                    if sig.data.ndim == 1:
                        str_axis = slices_nondigit[1]
                    elif sig.data.ndim == 2:
                        str_axis = "{},{}".format(
                            slices[0], slices_nondigit[0])
                    else:
                        raise Exception(
                            "3D axes are not supported, please slice them manually")
                    logger.debug("STR_AXIS DEDUCED: {}".format(str_axis))
                    data = Data(sig, str_axis)
                    if self.str_ind is not None:
                        self.last_str_ind = self.str_ind
                except Exception:
                    pass
            elif (expr.startswith("_piscopevar") or
                    "execute" in expr):
                logger.error(
                    "Expression starts with _piscopevar, but was not reset!!!")
                # should not happen any more, just log for the case
                # This case occurs if the axes are not specified it is usually followed by x_expr and y_expr cases.
                ##_arg = args[0]
                ##logger.debug("execute() in expr: {}".format(expr))
                ##str_id = "{}:{}".format(_arg,self.shot)
                ##logger.debug('str_id: {}'.format(str_id))
                # try:
                ##sig = self.cdb.get_signal(str_id)
                ##data = Data(sig, self.str_ind)
                ##self.last_signal = str_id
                ##self.last_str_ind = self.str_ind
                # except Exception:
                # pass
            # if the user enters signal.[time_]axis[0-9]
            elif (".axis" in expr or
                  ".time_axis" in expr):
                str_axis = expr.split(".")[-1]
                expr = expr.replace(".{}".format(str_axis), "")
                str_id = "{}:{}".format(expr, self.shot)
                logger.debug('axis detected: {}.{}'.format(str_id, str_axis))
                logger.debug("last_str_ind was: {}, current str_ind is: {}".format(
                    self.last_str_ind, self.str_ind))
                str_axis_shape = ":"
                if self.str_ind is None:
                    if self.last_str_ind is not None:
                        try:
                            i = axis_str_to_index(str_axis)
                            str_axis_shape = self.last_str_ind.split(",")[i]
                        except IndexError:
                            str_axis_shape = ":"
                try:
                    sig = self.cdb.get_signal(str_id)
                    sig = getattr(sig, str_axis)
                    data = Data(sig, str_axis_shape)
                    self.last_signal = str_id
                except Exception:
                    pass
            # Nothing special - get data
            else:
                str_id = '{}:{}'.format(expr, self.shot)
                if self.str_ind is not None:
                    str_id += "[{}]".format(self.str_ind)
                logger.debug('str_id: {}'.format(str_id))
                try:
                    sig = self.cdb.get_signal(str_id)
                    data = Data(sig, self.str_ind)
                    self.last_signal = str_id
                    self.last_str_ind = self.str_ind
                except Exception:
                    pass
        return data


class Tree(object):
    """MDSPlus-like Tree"""

    def __init__(self, arg):
        super(Tree, self).__init__()
        logger.debug("Tree.__init__({})".format(arg))

    def getNode(arg):
        logger.debug("Tree.getNode({})".format(arg))

    def setDefault(arg):
        logger.debug("Tree.setDefault({})".format(arg))


class Data(object):
    """MDSplus-like Data"""

    def __init__(self, signal, str_ind=None):
        logger.debug("Data.__init__({}, {})".format(signal, str_ind))
        super(Data, self).__init__()
        self.cdb_signal = None
        if isinstance(signal, CDBSignal):
            self.cdb_signal = signal
        else:
            self._data = signal
        self.str_ind = str_ind

    def execute(expr):
        logger.debug("Data.execute({})".format(expr))

    def data(self):
        if self.cdb_signal:
            return self.cdb_signal.data
        else:
            return np.asanyarray(self._data)
