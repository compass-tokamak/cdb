"""Module for CDB access"""
module CDB

export CDBSignal, cdb_signal

import PyCall.@pyimport, PyCall.PyObject, PyCall.PyArray

@pyimport pyCDB.client as client
cdb = client.CDBClient()

"""CDB signal."""
mutable struct CDBSignal
    data
    time_axis::CDBSignal
    axis1::CDBSignal
    axis2::CDBSignal
    axis3::CDBSignal
    axis4::CDBSignal
    axis5::CDBSignal
    axis6::CDBSignal
    CDBSignal() = new()
end

function CDBSignal(py_signal::PyObject)
    sig = CDBSignal()
    sig.data = PyArray(py_signal["data"])
    if haskey(py_signal, "time_axis")
        sig.time_axis = CDBSignal(py_signal["time_axis"])
    end
    if haskey(py_signal, "axis1")
        sig.axis1 = CDBSignal(py_signal["axis1"])
    end
    if haskey(py_signal, "axis2")
        sig.axis2 = CDBSignal(py_signal["axis2"])
    end
    if haskey(py_signal, "axis3")
        sig.axis3 = CDBSignal(py_signal["axis3"])
    end
    if haskey(py_signal, "axis4")
        sig.axis4 = CDBSignal(py_signal["axis4"])
    end
    if haskey(py_signal, "axis5")
        sig.axis5 = CDBSignal(py_signal["axis5"])
        end    
    if haskey(py_signal, "axis6")
        sig.axis6 = CDBSignal(py_signal["axis6"])
        end  
    sig
end

"""Read signal using pyCDB with the `str_id`."""
function get_signal(str_id)
    cdb_sig = cdb[:get_signal](str_id)
    CDBSignal(cdb_sig)
end

end