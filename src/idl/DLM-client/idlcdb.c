#include <stdio.h>
#include <string.h>
#include "idl_export.h"
#include "cyCDB_api.h"

/* prototype for IDL_Load */
int IDL_Load( void );

/*
 * Define message codes and their corresponding printf(3) format
 * strings. Note that message codes start at zero and each one is
 * one less that the previous one. Codes must be monotonic and
 * contiguous.
 */


static IDL_MSG_DEF msg_arr[] =
{
#define M_TM_INPRO                       0
  {  "M_TM_INPRO",   "%NThis is from a loadable module procedure." },
#define M_TM_INFUN                       -1  
  {  "M_TM_INFUN",   "%NThis is from a loadable module function." },
#define M_TM_NOSTRID                       -2  
  {  "M_TM_NOSTRID",   "%NThe argument passed does not seem to be a 'str_id'.\nAll other ways of calling are OBSOLETE!" },
#define M_TM_ERROR                       -3  
  {  "M_TM_ERROR",   "%NpyCDB returned ERROR. (details were sent to the terminal window)\nReturning a structure with the field ISSET = 0" }, 
};
/*
 * The load function fills in this message block handle with the
 * opaque handle to the message block used for this module. The other
 * routines can then use it to throw errors from this block.
 */
static IDL_MSG_BLOCK msg_block;


/* Implementation of the TESTFUN IDL function */
static IDL_VPTR get_full_signal_ref(int argc, IDL_VPTR *argv)
{
  /* Initialize Python */
  if (!Py_IsInitialized()) {
    Py_Initialize();
    int py_argc = 0;
    char* py_argv="";
    /* Segmentation fault if PySys_SetArgv not called! */
    PySys_SetArgv(py_argc,&py_argv); 
    /* cyCDB initialization */
    initcyCDB();
    import_cyCDB();
    PyRun_SimpleString("print 'Python was inicialized and cyCDB loaded..'");
    printf("\n");
    /* cyCDB working now */
  }


  //~ struct data_sources_t data_source;
  IDL_MessageFromBlock(msg_block, M_TM_INFUN, IDL_MSG_RET);

   IDL_ENSURE_STRING(argv[0]);
//   IDL_ENSURE_SCALAR(argv[0]); 
  char *signal = IDL_STRING_STR(&(argv[0]->value.str));
  fprintf(stdout,"Passed argumnet is:\t%s\n", signal);

//==================
  IDL_LONG *idptr;
  IDL_MEMINT n;
  int record_number = -5, rev = -6;
  IDL_VPTR idCvt;
  
  /* Extract data.  This way we don't demand scalar input, just simple 
     input.  We take the first input value */

  if (argv[1]->type != IDL_TYP_LONG) {
    idCvt = IDL_BasicTypeConversion(1, &(argv[1]), IDL_TYP_LONG);
    IDL_VarGetData(idCvt, &n, (char **) &idptr, IDL_TRUE);

    record_number = idptr[0];
    IDL_Deltmp(idCvt);
  } else {
    IDL_VarGetData(argv[1], &n, (char **) &idptr, IDL_TRUE);
    record_number = idptr[0];
  }
  fprintf(stdout,"Record number is:\t%d\n", record_number);
// ===========
  if (argv[2]->type != IDL_TYP_LONG) {
    idCvt = IDL_BasicTypeConversion(1, &(argv[2]), IDL_TYP_LONG);
    IDL_VarGetData(idCvt, &n, (char **) &idptr, IDL_TRUE);

    rev = idptr[0];
    IDL_Deltmp(idCvt);
  } else {
    IDL_VarGetData(argv[2], &n, (char **) &idptr, IDL_TRUE);
    rev = idptr[0];
  }
  fprintf(stdout,"Revision is:\t%d\n", rev);
// ===========
  static struct signal_data_ref_t       signal_ref;
  //signal_ref =  ("ne", 1, -1);
  signal_ref = cdb_get_signal_data_ref(signal, record_number, rev);

  struct cdb_signal_t cdb_signal;
  printf("cdb_get_signal\n");
  int raw = 0;
  cdb_signal = cdb_get_signal(signal, record_number, rev, raw);
  printf("got signal rev.: %i,\ndesc.: %s\n",cdb_signal.revision,cdb_signal.description);

  int nn;
  if (cdb_signal.ndim>0) {
    nn = cdb_signal.dims[0];
  } else {
    nn = 1;
  }
  printf("velikost pole z pyCDB:    %d\n", nn);
  IDL_MEMINT DATA_dims[] = { 1, nn };


  //signal_ref =  ("ne", 1, -1);
  signal_ref = cdb_get_signal_data_ref(signal,record_number , rev);

  printf("\n");

//=======================================================================================
// init
static IDL_STRUCT_TAG_DEF data_files_t_tags[] = {
  { "COLLECTION_NAME", 0, (void *) IDL_TYP_STRING},
  { "DATA_FORMAT", 0, (void *) IDL_TYP_STRING},
  { "RECORD_NUMBER", 0, (void *) IDL_TYP_LONG},
  { "FILE_NAME", 0, (void *) IDL_TYP_STRING},
  { "DATA_FILE_ID", 0, (void *) IDL_TYP_LONG},
  { "DATA_SOURCE_ID", 0, (void *) IDL_TYP_INT},
  { "REVISION", 0, (void *) IDL_TYP_INT},
  {0}};
void *data_files_t_sdef = IDL_MakeStruct("DATA_FILES_T", data_files_t_tags);


static IDL_STRUCT_TAG_DEF data_signals_t_tags[] = {
  { "COEFFICIENT", 0, (void *) IDL_TYP_DOUBLE},
  { "AXIS3_REVISION", 0, (void *) IDL_TYP_INT},
  { "BOARD_ID", 0, (void *) IDL_TYP_INT},
  { "REVISION", 0, (void *) IDL_TYP_INT},
  { "COMPUTER_ID", 0, (void *) IDL_TYP_INT},
  { "TIMESTAMP", 0, (void *) IDL_TYP_DOUBLE},
  { "AXIS2_REVISION", 0, (void *) IDL_TYP_INT},
  { "TIME0", 0, (void *) IDL_TYP_DOUBLE},
  { "AXIS1_REVISION", 0, (void *) IDL_TYP_INT},
  { "NOTE", 0, (void *) IDL_TYP_STRING},
  { "CHANNEL_ID", 0, (void *) IDL_TYP_INT},
  { "RECORD_NUMBER", 0, (void *) IDL_TYP_LONG},
  { "OFFSET", 0, (void *) IDL_TYP_DOUBLE},
  { "GENERIC_SIGNAL_ID", 0, (void *) IDL_TYP_LONG},
  { "DATA_FILE_KEY", 0, (void *) IDL_TYP_STRING},
  { "TIME_AXIS_REVISION", 0, (void *) IDL_TYP_INT},
  { "DATA_FILE_ID", 0, (void *) IDL_TYP_LONG},
  {0}};
void *data_signals_t_sdef = IDL_MakeStruct("DATA_SIGNALS_T", data_signals_t_tags);


static IDL_STRUCT_TAG_DEF generic_signals_t_tags[] = {
  { "GENERIC_SIGNAL_NAME", 0, (void *) IDL_TYP_STRING},
  { "DESCRIPTION", 0, (void *) IDL_TYP_STRING},
  { "LAST_RECORD_NUMBER", 0, (void *) IDL_TYP_LONG},
  { "FIRST_RECORD_NUMBER", 0, (void *) IDL_TYP_LONG},
  { "AXIS3_ID", 0, (void *) IDL_TYP_LONG},
  { "TIME_AXIS_ID", 0, (void *) IDL_TYP_LONG},
  { "AXIS2_ID", 0, (void *) IDL_TYP_LONG},
  { "AXIS1_ID", 0, (void *) IDL_TYP_LONG},
  { "ALIAS", 0, (void *) IDL_TYP_STRING},
  { "UNITS", 0, (void *) IDL_TYP_STRING},
  { "GENERIC_SIGNAL_ID", 0, (void *) IDL_TYP_LONG},
  { "DATA_SOURCE_ID", 0, (void *) IDL_TYP_INT},
  { "SIGNAL_TYPE", 0, (void *) IDL_TYP_STRING},
  {0}};
void *generic_signals_t_sdef = IDL_MakeStruct("GENERIC_SIGNALS_T", generic_signals_t_tags);

static IDL_SREF  generic_signals_t_sdef_array;

IDL_STRUCT_TAG_DEF signal_data_ref_t_tags[] = {
  { "ISSET", 0, (void *) IDL_TYP_INT},
  { "RECORD_NUMBER", 0, (void *) IDL_TYP_LONG},
  { "FULL_FILE_PATH", 0, (void *) IDL_TYP_STRING},
  { "DATA_FILE_REF", 0, (void *) NULL},
  { "DATA_SIGNAL_REF", 0, (void *) NULL},
  { "GENERIC_SIGNAL_REF", 0, (void *) NULL},
  {0}};

signal_data_ref_t_tags[3].type = data_files_t_sdef;
signal_data_ref_t_tags[4].type = data_signals_t_sdef;
signal_data_ref_t_tags[5].type = generic_signals_t_sdef;

void *signal_data_ref_t_sdef = IDL_MakeStruct("SIGNAL_DATA_REF_T", signal_data_ref_t_tags);


// fill
IDL_VPTR signal_ref_IDLptr;
static IDL_MEMINT one = 1;
IDL_MEMINT offset;
char *signal_ref_IDLdata = IDL_MakeTempStruct(signal_data_ref_t_sdef, 1, &one, &signal_ref_IDLptr, 0);

offset = IDL_StructTagInfoByName(signal_data_ref_t_sdef, "ISSET", IDL_MSG_LONGJMP, NULL);
IDL_INT *signal_data_ref_t_sdef_ISSET;
signal_data_ref_t_sdef_ISSET = (IDL_INT *) (signal_ref_IDLdata + offset);
*signal_data_ref_t_sdef_ISSET = signal_ref.isset;


offset = IDL_StructTagInfoByName(signal_data_ref_t_sdef, "RECORD_NUMBER", IDL_MSG_LONGJMP, NULL);
IDL_LONG *signal_data_ref_t_sdef_RECORD_NUMBER;
signal_data_ref_t_sdef_RECORD_NUMBER = (IDL_LONG *) (signal_ref_IDLdata + offset);
*signal_data_ref_t_sdef_RECORD_NUMBER = signal_ref.record_number;

offset = IDL_StructTagInfoByName(signal_data_ref_t_sdef, "FULL_FILE_PATH", IDL_MSG_LONGJMP, NULL);
IDL_STRING *signal_data_ref_t_sdef_FULL_FILE_PATH;
signal_data_ref_t_sdef_FULL_FILE_PATH = (IDL_STRING *) (signal_ref_IDLdata + offset);
IDL_StrStore(signal_data_ref_t_sdef_FULL_FILE_PATH, signal_ref.full_file_path);

offset = IDL_StructTagInfoByName(signal_data_ref_t_sdef, "DATA_FILE_REF", IDL_MSG_LONGJMP, NULL);
char *signal_data_ref_t_sdef_DATA_FILE_REF;
signal_data_ref_t_sdef_DATA_FILE_REF = (char *) (signal_ref_IDLdata + offset);


offset = IDL_StructTagInfoByName(data_files_t_sdef, "COLLECTION_NAME", IDL_MSG_LONGJMP, NULL);
IDL_STRING *data_files_t_sdef_COLLECTION_NAME;
data_files_t_sdef_COLLECTION_NAME = (IDL_STRING *) (signal_data_ref_t_sdef_DATA_FILE_REF + offset);
IDL_StrStore(data_files_t_sdef_COLLECTION_NAME, signal_ref.data_file_ref.collection_name);

offset = IDL_StructTagInfoByName(data_files_t_sdef, "DATA_FORMAT", IDL_MSG_LONGJMP, NULL);
IDL_STRING *data_files_t_sdef_DATA_FORMAT;
data_files_t_sdef_DATA_FORMAT = (IDL_STRING *) (signal_data_ref_t_sdef_DATA_FILE_REF + offset);
IDL_StrStore(data_files_t_sdef_DATA_FORMAT, signal_ref.data_file_ref.data_format);

offset = IDL_StructTagInfoByName(data_files_t_sdef, "RECORD_NUMBER", IDL_MSG_LONGJMP, NULL);
IDL_LONG *data_files_t_sdef_RECORD_NUMBER;
data_files_t_sdef_RECORD_NUMBER = (IDL_LONG *) (signal_data_ref_t_sdef_DATA_FILE_REF + offset);
*data_files_t_sdef_RECORD_NUMBER = signal_ref.data_file_ref.record_number;

offset = IDL_StructTagInfoByName(data_files_t_sdef, "FILE_NAME", IDL_MSG_LONGJMP, NULL);
IDL_STRING *data_files_t_sdef_FILE_NAME;
data_files_t_sdef_FILE_NAME = (IDL_STRING *) (signal_data_ref_t_sdef_DATA_FILE_REF + offset);
IDL_StrStore(data_files_t_sdef_FILE_NAME, signal_ref.data_file_ref.file_name);

offset = IDL_StructTagInfoByName(data_files_t_sdef, "DATA_FILE_ID", IDL_MSG_LONGJMP, NULL);
IDL_LONG *data_files_t_sdef_DATA_FILE_ID;
data_files_t_sdef_DATA_FILE_ID = (IDL_LONG *) (signal_data_ref_t_sdef_DATA_FILE_REF + offset);
*data_files_t_sdef_DATA_FILE_ID = signal_ref.data_file_ref.data_file_id;
offset = IDL_StructTagInfoByName(data_files_t_sdef, "DATA_SOURCE_ID", IDL_MSG_LONGJMP, NULL);
IDL_INT *data_files_t_sdef_DATA_SOURCE_ID;
data_files_t_sdef_DATA_SOURCE_ID = (IDL_INT *) (signal_data_ref_t_sdef_DATA_FILE_REF + offset);
*data_files_t_sdef_DATA_SOURCE_ID = signal_ref.data_file_ref.data_source_id;

offset = IDL_StructTagInfoByName(data_files_t_sdef, "REVISION", IDL_MSG_LONGJMP, NULL);
IDL_INT *data_files_t_sdef_REVISION;
data_files_t_sdef_REVISION = (IDL_INT *) (signal_data_ref_t_sdef_DATA_FILE_REF + offset);
*data_files_t_sdef_REVISION = signal_ref.data_file_ref.revision;

offset = IDL_StructTagInfoByName(signal_data_ref_t_sdef, "DATA_SIGNAL_REF", IDL_MSG_LONGJMP, NULL);
char *signal_data_ref_t_sdef_DATA_SIGNAL_REF;
signal_data_ref_t_sdef_DATA_SIGNAL_REF = (char *) (signal_ref_IDLdata + offset);


offset = IDL_StructTagInfoByName(data_signals_t_sdef, "COEFFICIENT", IDL_MSG_LONGJMP, NULL);
double *data_signals_t_sdef_COEFFICIENT;
data_signals_t_sdef_COEFFICIENT = (double *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_COEFFICIENT = signal_ref.data_signal_ref.coefficient;

offset = IDL_StructTagInfoByName(data_signals_t_sdef, "AXIS3_REVISION", IDL_MSG_LONGJMP, NULL);
IDL_INT *data_signals_t_sdef_AXIS3_REVISION;
data_signals_t_sdef_AXIS3_REVISION = (IDL_INT *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_AXIS3_REVISION = signal_ref.data_signal_ref.axis3_revision;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "BOARD_ID", IDL_MSG_LONGJMP, NULL);
IDL_INT *data_signals_t_sdef_BOARD_ID;
data_signals_t_sdef_BOARD_ID = (IDL_INT *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_BOARD_ID = signal_ref.data_signal_ref.board_id;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "REVISION", IDL_MSG_LONGJMP, NULL);
IDL_INT *data_signals_t_sdef_REVISION;
data_signals_t_sdef_REVISION = (IDL_INT *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_REVISION = signal_ref.data_signal_ref.revision;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "COMPUTER_ID", IDL_MSG_LONGJMP, NULL);
IDL_INT *data_signals_t_sdef_COMPUTER_ID;
data_signals_t_sdef_COMPUTER_ID = (IDL_INT *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_COMPUTER_ID = signal_ref.data_signal_ref.computer_id;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "TIMESTAMP", IDL_MSG_LONGJMP, NULL);
double *data_signals_t_sdef_TIMESTAMP;
data_signals_t_sdef_TIMESTAMP = (double *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_TIMESTAMP = signal_ref.data_signal_ref.timestamp;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "AXIS2_REVISION", IDL_MSG_LONGJMP, NULL);
IDL_INT *data_signals_t_sdef_AXIS2_REVISION;
data_signals_t_sdef_AXIS2_REVISION = (IDL_INT *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_AXIS2_REVISION = signal_ref.data_signal_ref.axis2_revision;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "TIME0", IDL_MSG_LONGJMP, NULL);
double *data_signals_t_sdef_TIME0;
data_signals_t_sdef_TIME0 = (double *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_TIME0 = signal_ref.data_signal_ref.time0;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "AXIS1_REVISION", IDL_MSG_LONGJMP, NULL);
IDL_INT *data_signals_t_sdef_AXIS1_REVISION;
data_signals_t_sdef_AXIS1_REVISION = (IDL_INT *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_AXIS1_REVISION = signal_ref.data_signal_ref.axis1_revision;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "NOTE", IDL_MSG_LONGJMP, NULL);
IDL_STRING *data_signals_t_sdef_NOTE;
data_signals_t_sdef_NOTE = (IDL_STRING *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
IDL_StrStore(data_signals_t_sdef_NOTE, signal_ref.data_signal_ref.note);
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "CHANNEL_ID", IDL_MSG_LONGJMP, NULL);
IDL_INT *data_signals_t_sdef_CHANNEL_ID;
data_signals_t_sdef_CHANNEL_ID = (IDL_INT *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_CHANNEL_ID = signal_ref.data_signal_ref.channel_id;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "RECORD_NUMBER", IDL_MSG_LONGJMP, NULL);
IDL_LONG *data_signals_t_sdef_RECORD_NUMBER;
data_signals_t_sdef_RECORD_NUMBER = (IDL_LONG *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_RECORD_NUMBER = signal_ref.data_signal_ref.record_number;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "OFFSET", IDL_MSG_LONGJMP, NULL);
double *data_signals_t_sdef_OFFSET;
data_signals_t_sdef_OFFSET = (double *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_OFFSET = signal_ref.data_signal_ref.offset;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "GENERIC_SIGNAL_ID", IDL_MSG_LONGJMP, NULL);
IDL_LONG *data_signals_t_sdef_GENERIC_SIGNAL_ID;
data_signals_t_sdef_GENERIC_SIGNAL_ID = (IDL_LONG *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_GENERIC_SIGNAL_ID = signal_ref.data_signal_ref.generic_signal_id;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "DATA_FILE_KEY", IDL_MSG_LONGJMP, NULL);
IDL_STRING *data_signals_t_sdef_DATA_FILE_KEY;
data_signals_t_sdef_DATA_FILE_KEY = (IDL_STRING *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
IDL_StrStore(data_signals_t_sdef_DATA_FILE_KEY, signal_ref.data_signal_ref.data_file_key);
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "TIME_AXIS_REVISION", IDL_MSG_LONGJMP, NULL);
IDL_INT *data_signals_t_sdef_TIME_AXIS_REVISION;
data_signals_t_sdef_TIME_AXIS_REVISION = (IDL_INT *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_TIME_AXIS_REVISION = signal_ref.data_signal_ref.time_axis_revision;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "DATA_FILE_ID", IDL_MSG_LONGJMP, NULL);
IDL_LONG *data_signals_t_sdef_DATA_FILE_ID;
data_signals_t_sdef_DATA_FILE_ID = (IDL_LONG *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_DATA_FILE_ID = signal_ref.data_signal_ref.data_file_id;

offset = IDL_StructTagInfoByName(signal_data_ref_t_sdef, "GENERIC_SIGNAL_REF", IDL_MSG_LONGJMP, NULL);
char *signal_data_ref_t_sdef_GENERIC_SIGNAL_REF;
signal_data_ref_t_sdef_GENERIC_SIGNAL_REF = (char *) (signal_ref_IDLdata + offset);

offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "GENERIC_SIGNAL_NAME", IDL_MSG_LONGJMP, NULL);
IDL_STRING *generic_signals_t_sdef_GENERIC_SIGNAL_NAME;
generic_signals_t_sdef_GENERIC_SIGNAL_NAME = (IDL_STRING *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
IDL_StrStore(generic_signals_t_sdef_GENERIC_SIGNAL_NAME, signal_ref.generic_signal_ref.generic_signal_name);
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "DESCRIPTION", IDL_MSG_LONGJMP, NULL);
IDL_STRING *generic_signals_t_sdef_DESCRIPTION;
generic_signals_t_sdef_DESCRIPTION = (IDL_STRING *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
IDL_StrStore(generic_signals_t_sdef_DESCRIPTION, signal_ref.generic_signal_ref.description);
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "LAST_RECORD_NUMBER", IDL_MSG_LONGJMP, NULL);
IDL_LONG *generic_signals_t_sdef_LAST_RECORD_NUMBER;
generic_signals_t_sdef_LAST_RECORD_NUMBER = (IDL_LONG *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
*generic_signals_t_sdef_LAST_RECORD_NUMBER = signal_ref.generic_signal_ref.last_record_number;
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "FIRST_RECORD_NUMBER", IDL_MSG_LONGJMP, NULL);
IDL_LONG *generic_signals_t_sdef_FIRST_RECORD_NUMBER;
generic_signals_t_sdef_FIRST_RECORD_NUMBER = (IDL_LONG *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
*generic_signals_t_sdef_FIRST_RECORD_NUMBER = signal_ref.generic_signal_ref.first_record_number;
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "AXIS3_ID", IDL_MSG_LONGJMP, NULL);
IDL_LONG *generic_signals_t_sdef_AXIS3_ID;
generic_signals_t_sdef_AXIS3_ID = (IDL_LONG *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
*generic_signals_t_sdef_AXIS3_ID = signal_ref.generic_signal_ref.axis3_id;
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "TIME_AXIS_ID", IDL_MSG_LONGJMP, NULL);
IDL_LONG *generic_signals_t_sdef_TIME_AXIS_ID;
generic_signals_t_sdef_TIME_AXIS_ID = (IDL_LONG *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
*generic_signals_t_sdef_TIME_AXIS_ID = signal_ref.generic_signal_ref.time_axis_id;
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "AXIS2_ID", IDL_MSG_LONGJMP, NULL);
IDL_LONG *generic_signals_t_sdef_AXIS2_ID;
generic_signals_t_sdef_AXIS2_ID = (IDL_LONG *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
*generic_signals_t_sdef_AXIS2_ID = signal_ref.generic_signal_ref.axis2_id;
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "AXIS1_ID", IDL_MSG_LONGJMP, NULL);
IDL_LONG *generic_signals_t_sdef_AXIS1_ID;
generic_signals_t_sdef_AXIS1_ID = (IDL_LONG *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
*generic_signals_t_sdef_AXIS1_ID = signal_ref.generic_signal_ref.axis1_id;
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "ALIAS", IDL_MSG_LONGJMP, NULL);
IDL_STRING *generic_signals_t_sdef_ALIAS;
generic_signals_t_sdef_ALIAS = (IDL_STRING *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
IDL_StrStore(generic_signals_t_sdef_ALIAS, signal_ref.generic_signal_ref.alias);
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "UNITS", IDL_MSG_LONGJMP, NULL);
IDL_STRING *generic_signals_t_sdef_UNITS;
generic_signals_t_sdef_UNITS = (IDL_STRING *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
IDL_StrStore(generic_signals_t_sdef_UNITS, signal_ref.generic_signal_ref.units);
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "GENERIC_SIGNAL_ID", IDL_MSG_LONGJMP, NULL);
IDL_LONG *generic_signals_t_sdef_GENERIC_SIGNAL_ID;
generic_signals_t_sdef_GENERIC_SIGNAL_ID = (IDL_LONG *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
*generic_signals_t_sdef_GENERIC_SIGNAL_ID = signal_ref.generic_signal_ref.generic_signal_id;
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "DATA_SOURCE_ID", IDL_MSG_LONGJMP, NULL);
IDL_INT *generic_signals_t_sdef_DATA_SOURCE_ID;
generic_signals_t_sdef_DATA_SOURCE_ID = (IDL_INT *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
*generic_signals_t_sdef_DATA_SOURCE_ID = signal_ref.generic_signal_ref.data_source_id;
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "SIGNAL_TYPE", IDL_MSG_LONGJMP, NULL);
IDL_STRING *generic_signals_t_sdef_SIGNAL_TYPE;
generic_signals_t_sdef_SIGNAL_TYPE = (IDL_STRING *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
IDL_StrStore(generic_signals_t_sdef_SIGNAL_TYPE, signal_ref.generic_signal_ref.signal_type);

return signal_ref_IDLptr;
}


static IDL_VPTR get_signal(int argc, IDL_VPTR *argv, char *argk)
{
 /* Initialize Python */
  if (!Py_IsInitialized()) {
    Py_Initialize();
    int py_argc = 0;
    char* py_argv="";
    /* Segmentation fault if PySys_SetArgv not called! */
    PySys_SetArgv(py_argc,&py_argv); 
    /* cyCDB initialization */
    initcyCDB();
    import_cyCDB();
    PyRun_SimpleString("print 'Python was inicialized and cyCDB loaded..'");
    printf("\n");
    /* cyCDB working now */
  }

  static struct signal_data_ref_t signal_ref;
  struct cdb_signal_t cdb_signal;
       
//KEYWORD PROCESSING

    typedef struct {
        IDL_KW_RESULT_FIRST_FIELD; /* Must be first entry in struct */
        IDL_LONG raw;
    } KW_RESULT;
    static IDL_KW_PAR kw_pars[] = {
        IDL_KW_FAST_SCAN,
        { "RAW", IDL_TYP_LONG, 1, IDL_KW_ZERO, NULL, IDL_KW_OFFSETOF(raw)},
        { NULL } /* List must be NULL terminated */
    };
    KW_RESULT kw; /* Variable which will hold the keyword values */
    (void) IDL_KWProcessByOffset(argc, argv, argk, kw_pars, (IDL_VPTR *) 0, 1, &kw);
    /* The body of your routine */
    int raw = kw.raw;
    
    fprintf(stdout, "raw = %i\n", raw);


//ARGUMENTS PROCESSING
   IDL_ENSURE_STRING(argv[0]);
  char *signal = IDL_STRING_STR(&(argv[0]->value.str));
  fprintf(stdout,"Passed argument is:\t%s\n", signal);
  
  if (strpbrk(signal, ":")) {
    fprintf(stdout, "Passed argument contains ':' -> assumed to be a str_id.\n");
    signal_ref = cdb_get_signal_data_ref_by_strid(signal);
	cdb_signal = cdb_get_signal_by_strid(signal);
  } else {
    IDL_MessageFromBlock(msg_block, M_TM_NOSTRID, IDL_MSG_RET);  
    //==================
      IDL_LONG *idptr;
      IDL_MEMINT n;
      int record_number = -5, rev = -6;
      //int raw = 0;
      IDL_VPTR idCvt;
      if (argv[1]->type != IDL_TYP_LONG) {
        idCvt = IDL_BasicTypeConversion(1, &(argv[1]), IDL_TYP_LONG);
        IDL_VarGetData(idCvt, &n, (char **) &idptr, IDL_TRUE);
        record_number = idptr[0];
        IDL_Deltmp(idCvt);
      } else {
        IDL_VarGetData(argv[1], &n, (char **) &idptr, IDL_TRUE);
        record_number = idptr[0];
      }
      fprintf(stdout,"Record number is:\t%d\n", record_number);
    // ===========
      if (argv[2]->type != IDL_TYP_LONG) {
        idCvt = IDL_BasicTypeConversion(1, &(argv[2]), IDL_TYP_LONG);
        IDL_VarGetData(idCvt, &n, (char **) &idptr, IDL_TRUE);
        rev = idptr[0];
        IDL_Deltmp(idCvt);
      } else {
        IDL_VarGetData(argv[2], &n, (char **) &idptr, IDL_TRUE);
        rev = idptr[0];
      }
      fprintf(stdout,"Revision is:\t%d\n", rev);
    // ===========
  	signal_ref = cdb_get_signal_data_ref(signal, record_number, rev);
    cdb_signal = cdb_get_signal(signal, record_number, rev, raw);
 }

 IDL_KW_FREE; //cleanup after the keyword processing
      
 if (signal_ref.isset != 1 || cdb_signal.isset != 1) {
	printf("pyCDB returned an error.\n");
	IDL_STRUCT_TAG_DEF error_tags[] = {
	  { "ISSET", 0, (void *) IDL_TYP_INT},
	  {0}};
	void *error_sdef = IDL_MakeStruct("ERROR_STRUCTURE", error_tags);

	IDL_VPTR error_IDLptr;
	static IDL_MEMINT one = 1;
	IDL_MEMINT offset;
	char *error_IDLdata = IDL_MakeTempStruct(error_sdef, 1, &one, &error_IDLptr, 1);

	offset = IDL_StructTagInfoByName(error_sdef, "ISSET", IDL_MSG_LONGJMP, NULL);
	IDL_INT *error_sdef_ISSET;
	error_sdef_ISSET = (IDL_INT *) (error_IDLdata + offset);
	*error_sdef_ISSET = 0;

	//printf("Returning a structure with the field ISSET = 0\n");
	IDL_MessageFromBlock(msg_block, M_TM_ERROR, IDL_MSG_RET);
	return error_IDLptr;
}



  printf("got signal rev.: %i,\ndesc.: %s\n",cdb_signal.revision,cdb_signal.description);

  int nn;
  if (cdb_signal.ndim>0) {
    nn = cdb_signal.dims[0];
  } else {
    nn = 1;
  }
  printf("velikost pole z pyCDB:    %d\n", nn);
  IDL_MEMINT DATA_dims[] = { 1, nn };

//=======================================================================================
// init
static IDL_STRUCT_TAG_DEF data_files_t_tags[] = {
  { "COLLECTION_NAME", 0, (void *) IDL_TYP_STRING},
  { "DATA_FORMAT", 0, (void *) IDL_TYP_STRING},
  { "RECORD_NUMBER", 0, (void *) IDL_TYP_LONG},
  { "FILE_NAME", 0, (void *) IDL_TYP_STRING},
  { "DATA_FILE_ID", 0, (void *) IDL_TYP_LONG},
  { "DATA_SOURCE_ID", 0, (void *) IDL_TYP_INT},
  { "REVISION", 0, (void *) IDL_TYP_INT},
  {0}};
void *data_files_t_sdef = IDL_MakeStruct("DATA_FILES_T", data_files_t_tags);


static IDL_STRUCT_TAG_DEF data_signals_t_tags[] = {
  { "COEFFICIENT", 0, (void *) IDL_TYP_DOUBLE},
  { "AXIS3_REVISION", 0, (void *) IDL_TYP_INT},
  { "BOARD_ID", 0, (void *) IDL_TYP_INT},
  { "REVISION", 0, (void *) IDL_TYP_INT},
  { "COMPUTER_ID", 0, (void *) IDL_TYP_INT},
  { "TIMESTAMP", 0, (void *) IDL_TYP_DOUBLE},
  { "AXIS2_REVISION", 0, (void *) IDL_TYP_INT},
  { "TIME0", 0, (void *) IDL_TYP_DOUBLE},
  { "AXIS1_REVISION", 0, (void *) IDL_TYP_INT},
  { "NOTE", 0, (void *) IDL_TYP_STRING},
  { "CHANNEL_ID", 0, (void *) IDL_TYP_INT},
  { "RECORD_NUMBER", 0, (void *) IDL_TYP_LONG},
  { "OFFSET", 0, (void *) IDL_TYP_DOUBLE},
  { "GENERIC_SIGNAL_ID", 0, (void *) IDL_TYP_LONG},
  { "DATA_FILE_KEY", 0, (void *) IDL_TYP_STRING},
  { "TIME_AXIS_REVISION", 0, (void *) IDL_TYP_INT},
  { "DATA_FILE_ID", 0, (void *) IDL_TYP_LONG},
  {0}};
void *data_signals_t_sdef = IDL_MakeStruct("DATA_SIGNALS_T", data_signals_t_tags);


static IDL_STRUCT_TAG_DEF generic_signals_t_tags[] = {
  { "GENERIC_SIGNAL_NAME", 0, (void *) IDL_TYP_STRING},
  { "DESCRIPTION", 0, (void *) IDL_TYP_STRING},
  { "LAST_RECORD_NUMBER", 0, (void *) IDL_TYP_LONG},
  { "FIRST_RECORD_NUMBER", 0, (void *) IDL_TYP_LONG},
  { "AXIS3_ID", 0, (void *) IDL_TYP_LONG},
  { "TIME_AXIS_ID", 0, (void *) IDL_TYP_LONG},
  { "AXIS2_ID", 0, (void *) IDL_TYP_LONG},
  { "AXIS1_ID", 0, (void *) IDL_TYP_LONG},
  { "ALIAS", 0, (void *) IDL_TYP_STRING},
  { "UNITS", 0, (void *) IDL_TYP_STRING},
  { "GENERIC_SIGNAL_ID", 0, (void *) IDL_TYP_LONG},
  { "DATA_SOURCE_ID", 0, (void *) IDL_TYP_INT},
  { "SIGNAL_TYPE", 0, (void *) IDL_TYP_STRING},
  {0}};
void *generic_signals_t_sdef = IDL_MakeStruct("GENERIC_SIGNALS_T", generic_signals_t_tags);

static IDL_SREF  generic_signals_t_sdef_array;

IDL_STRUCT_TAG_DEF signal_data_ref_t_tags[] = {
  { "ISSET", 0, (void *) IDL_TYP_INT},
  { "RECORD_NUMBER", 0, (void *) IDL_TYP_LONG},
  { "FULL_FILE_PATH", 0, (void *) IDL_TYP_STRING},
  { "DATA_FILE_REF", 0, (void *) NULL},
  { "DATA_SIGNAL_REF", 0, (void *) NULL},
  { "GENERIC_SIGNAL_REF", 0, (void *) NULL},
  { "DATA", DATA_dims, (void *) IDL_TYP_DOUBLE },
  { "TIME", DATA_dims, (void *) IDL_TYP_DOUBLE },
  { "TIME_AXIS_DATA", DATA_dims, (void *) IDL_TYP_DOUBLE },
  {0}};

signal_data_ref_t_tags[3].type = data_files_t_sdef;
signal_data_ref_t_tags[4].type = data_signals_t_sdef;
signal_data_ref_t_tags[5].type = generic_signals_t_sdef;

void *signal_data_ref_t_sdef = IDL_MakeStruct(NULL, signal_data_ref_t_tags);  // ANONYMOUS STRUCTURE!!!!
//void *signal_data_ref_t_sdef = IDL_MakeStruct("SIGNAL_DATA_REF_T", signal_data_ref_t_tags);

// fill
IDL_VPTR signal_ref_IDLptr;
static IDL_MEMINT one = 1;
IDL_MEMINT offset;
char *signal_ref_IDLdata = IDL_MakeTempStruct(signal_data_ref_t_sdef, 1, &one, &signal_ref_IDLptr, 1);
 
 

offset = IDL_StructTagInfoByName(signal_data_ref_t_sdef, "ISSET", IDL_MSG_LONGJMP, NULL);
IDL_INT *signal_data_ref_t_sdef_ISSET;
signal_data_ref_t_sdef_ISSET = (IDL_INT *) (signal_ref_IDLdata + offset);
*signal_data_ref_t_sdef_ISSET = signal_ref.isset;


offset = IDL_StructTagInfoByName(signal_data_ref_t_sdef, "RECORD_NUMBER", IDL_MSG_LONGJMP, NULL);
IDL_LONG *signal_data_ref_t_sdef_RECORD_NUMBER;
signal_data_ref_t_sdef_RECORD_NUMBER = (IDL_LONG *) (signal_ref_IDLdata + offset);
*signal_data_ref_t_sdef_RECORD_NUMBER = signal_ref.record_number;

offset = IDL_StructTagInfoByName(signal_data_ref_t_sdef, "FULL_FILE_PATH", IDL_MSG_LONGJMP, NULL);
IDL_STRING *signal_data_ref_t_sdef_FULL_FILE_PATH;
signal_data_ref_t_sdef_FULL_FILE_PATH = (IDL_STRING *) (signal_ref_IDLdata + offset);
IDL_StrStore(signal_data_ref_t_sdef_FULL_FILE_PATH, signal_ref.full_file_path);

printf("signal_ref.full_file_path = %s\n", signal_ref.full_file_path);

// #####################################################################
offset = IDL_StructTagInfoByName(signal_data_ref_t_sdef, "DATA", IDL_MSG_LONGJMP, NULL);
double *signal_data_ref_t_sdef_DATA;
signal_data_ref_t_sdef_DATA = (double *) (signal_ref_IDLdata + offset);
memcpy((void *)signal_data_ref_t_sdef_DATA, (void *)cdb_signal.data, nn*sizeof(double));


offset = IDL_StructTagInfoByName(signal_data_ref_t_sdef, "TIME", IDL_MSG_LONGJMP, NULL);
double *signal_data_ref_t_sdef_TIME;
signal_data_ref_t_sdef_TIME = (double *) (signal_ref_IDLdata + offset);
memcpy((void *)signal_data_ref_t_sdef_TIME, (void *)(*cdb_signal.time_axis).data, nn*sizeof(double));

offset = IDL_StructTagInfoByName(signal_data_ref_t_sdef, "TIME_AXIS_DATA", IDL_MSG_LONGJMP, NULL);
double *signal_data_ref_t_sdef_TIME_AXIS_DATA;
signal_data_ref_t_sdef_TIME_AXIS_DATA = (double *) (signal_ref_IDLdata + offset);
memcpy((void *)signal_data_ref_t_sdef_TIME_AXIS_DATA, (void *)(*cdb_signal.time_axis).data, nn*sizeof(double));
// #####################################################################



offset = IDL_StructTagInfoByName(signal_data_ref_t_sdef, "DATA_FILE_REF", IDL_MSG_LONGJMP, NULL);
char *signal_data_ref_t_sdef_DATA_FILE_REF;
signal_data_ref_t_sdef_DATA_FILE_REF = (char *) (signal_ref_IDLdata + offset);


offset = IDL_StructTagInfoByName(data_files_t_sdef, "COLLECTION_NAME", IDL_MSG_LONGJMP, NULL);
IDL_STRING *data_files_t_sdef_COLLECTION_NAME;
data_files_t_sdef_COLLECTION_NAME = (IDL_STRING *) (signal_data_ref_t_sdef_DATA_FILE_REF + offset);
IDL_StrStore(data_files_t_sdef_COLLECTION_NAME, signal_ref.data_file_ref.collection_name);

offset = IDL_StructTagInfoByName(data_files_t_sdef, "DATA_FORMAT", IDL_MSG_LONGJMP, NULL);
IDL_STRING *data_files_t_sdef_DATA_FORMAT;
data_files_t_sdef_DATA_FORMAT = (IDL_STRING *) (signal_data_ref_t_sdef_DATA_FILE_REF + offset);
IDL_StrStore(data_files_t_sdef_DATA_FORMAT, signal_ref.data_file_ref.data_format);

offset = IDL_StructTagInfoByName(data_files_t_sdef, "RECORD_NUMBER", IDL_MSG_LONGJMP, NULL);
IDL_LONG *data_files_t_sdef_RECORD_NUMBER;
data_files_t_sdef_RECORD_NUMBER = (IDL_LONG *) (signal_data_ref_t_sdef_DATA_FILE_REF + offset);
*data_files_t_sdef_RECORD_NUMBER = signal_ref.data_file_ref.record_number;

offset = IDL_StructTagInfoByName(data_files_t_sdef, "FILE_NAME", IDL_MSG_LONGJMP, NULL);
IDL_STRING *data_files_t_sdef_FILE_NAME;
data_files_t_sdef_FILE_NAME = (IDL_STRING *) (signal_data_ref_t_sdef_DATA_FILE_REF + offset);
IDL_StrStore(data_files_t_sdef_FILE_NAME, signal_ref.data_file_ref.file_name);

offset = IDL_StructTagInfoByName(data_files_t_sdef, "DATA_FILE_ID", IDL_MSG_LONGJMP, NULL);
IDL_LONG *data_files_t_sdef_DATA_FILE_ID;
data_files_t_sdef_DATA_FILE_ID = (IDL_LONG *) (signal_data_ref_t_sdef_DATA_FILE_REF + offset);
*data_files_t_sdef_DATA_FILE_ID = signal_ref.data_file_ref.data_file_id;
offset = IDL_StructTagInfoByName(data_files_t_sdef, "DATA_SOURCE_ID", IDL_MSG_LONGJMP, NULL);
IDL_INT *data_files_t_sdef_DATA_SOURCE_ID;
data_files_t_sdef_DATA_SOURCE_ID = (IDL_INT *) (signal_data_ref_t_sdef_DATA_FILE_REF + offset);
*data_files_t_sdef_DATA_SOURCE_ID = signal_ref.data_file_ref.data_source_id;

offset = IDL_StructTagInfoByName(data_files_t_sdef, "REVISION", IDL_MSG_LONGJMP, NULL);
IDL_INT *data_files_t_sdef_REVISION;
data_files_t_sdef_REVISION = (IDL_INT *) (signal_data_ref_t_sdef_DATA_FILE_REF + offset);
*data_files_t_sdef_REVISION = signal_ref.data_file_ref.revision;

offset = IDL_StructTagInfoByName(signal_data_ref_t_sdef, "DATA_SIGNAL_REF", IDL_MSG_LONGJMP, NULL);
char *signal_data_ref_t_sdef_DATA_SIGNAL_REF;
signal_data_ref_t_sdef_DATA_SIGNAL_REF = (char *) (signal_ref_IDLdata + offset);


offset = IDL_StructTagInfoByName(data_signals_t_sdef, "COEFFICIENT", IDL_MSG_LONGJMP, NULL);
double *data_signals_t_sdef_COEFFICIENT;
data_signals_t_sdef_COEFFICIENT = (double *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_COEFFICIENT = signal_ref.data_signal_ref.coefficient;

offset = IDL_StructTagInfoByName(data_signals_t_sdef, "AXIS3_REVISION", IDL_MSG_LONGJMP, NULL);
IDL_INT *data_signals_t_sdef_AXIS3_REVISION;
data_signals_t_sdef_AXIS3_REVISION = (IDL_INT *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_AXIS3_REVISION = signal_ref.data_signal_ref.axis3_revision;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "BOARD_ID", IDL_MSG_LONGJMP, NULL);
IDL_INT *data_signals_t_sdef_BOARD_ID;
data_signals_t_sdef_BOARD_ID = (IDL_INT *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_BOARD_ID = signal_ref.data_signal_ref.board_id;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "REVISION", IDL_MSG_LONGJMP, NULL);
IDL_INT *data_signals_t_sdef_REVISION;
data_signals_t_sdef_REVISION = (IDL_INT *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_REVISION = signal_ref.data_signal_ref.revision;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "COMPUTER_ID", IDL_MSG_LONGJMP, NULL);
IDL_INT *data_signals_t_sdef_COMPUTER_ID;
data_signals_t_sdef_COMPUTER_ID = (IDL_INT *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_COMPUTER_ID = signal_ref.data_signal_ref.computer_id;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "TIMESTAMP", IDL_MSG_LONGJMP, NULL);
double *data_signals_t_sdef_TIMESTAMP;
data_signals_t_sdef_TIMESTAMP = (double *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_TIMESTAMP = signal_ref.data_signal_ref.timestamp;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "AXIS2_REVISION", IDL_MSG_LONGJMP, NULL);
IDL_INT *data_signals_t_sdef_AXIS2_REVISION;
data_signals_t_sdef_AXIS2_REVISION = (IDL_INT *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_AXIS2_REVISION = signal_ref.data_signal_ref.axis2_revision;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "TIME0", IDL_MSG_LONGJMP, NULL);
double *data_signals_t_sdef_TIME0;
data_signals_t_sdef_TIME0 = (double *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_TIME0 = signal_ref.data_signal_ref.time0;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "AXIS1_REVISION", IDL_MSG_LONGJMP, NULL);
IDL_INT *data_signals_t_sdef_AXIS1_REVISION;
data_signals_t_sdef_AXIS1_REVISION = (IDL_INT *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_AXIS1_REVISION = signal_ref.data_signal_ref.axis1_revision;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "NOTE", IDL_MSG_LONGJMP, NULL);
IDL_STRING *data_signals_t_sdef_NOTE;
data_signals_t_sdef_NOTE = (IDL_STRING *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
IDL_StrStore(data_signals_t_sdef_NOTE, signal_ref.data_signal_ref.note);
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "CHANNEL_ID", IDL_MSG_LONGJMP, NULL);
IDL_INT *data_signals_t_sdef_CHANNEL_ID;
data_signals_t_sdef_CHANNEL_ID = (IDL_INT *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_CHANNEL_ID = signal_ref.data_signal_ref.channel_id;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "RECORD_NUMBER", IDL_MSG_LONGJMP, NULL);
IDL_LONG *data_signals_t_sdef_RECORD_NUMBER;
data_signals_t_sdef_RECORD_NUMBER = (IDL_LONG *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_RECORD_NUMBER = signal_ref.data_signal_ref.record_number;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "OFFSET", IDL_MSG_LONGJMP, NULL);
double *data_signals_t_sdef_OFFSET;
data_signals_t_sdef_OFFSET = (double *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_OFFSET = signal_ref.data_signal_ref.offset;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "GENERIC_SIGNAL_ID", IDL_MSG_LONGJMP, NULL);
IDL_LONG *data_signals_t_sdef_GENERIC_SIGNAL_ID;
data_signals_t_sdef_GENERIC_SIGNAL_ID = (IDL_LONG *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_GENERIC_SIGNAL_ID = signal_ref.data_signal_ref.generic_signal_id;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "DATA_FILE_KEY", IDL_MSG_LONGJMP, NULL);
IDL_STRING *data_signals_t_sdef_DATA_FILE_KEY;
data_signals_t_sdef_DATA_FILE_KEY = (IDL_STRING *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
IDL_StrStore(data_signals_t_sdef_DATA_FILE_KEY, signal_ref.data_signal_ref.data_file_key);
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "TIME_AXIS_REVISION", IDL_MSG_LONGJMP, NULL);
IDL_INT *data_signals_t_sdef_TIME_AXIS_REVISION;
data_signals_t_sdef_TIME_AXIS_REVISION = (IDL_INT *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_TIME_AXIS_REVISION = signal_ref.data_signal_ref.time_axis_revision;
offset = IDL_StructTagInfoByName(data_signals_t_sdef, "DATA_FILE_ID", IDL_MSG_LONGJMP, NULL);
IDL_LONG *data_signals_t_sdef_DATA_FILE_ID;
data_signals_t_sdef_DATA_FILE_ID = (IDL_LONG *) (signal_data_ref_t_sdef_DATA_SIGNAL_REF + offset);
*data_signals_t_sdef_DATA_FILE_ID = signal_ref.data_signal_ref.data_file_id;

offset = IDL_StructTagInfoByName(signal_data_ref_t_sdef, "GENERIC_SIGNAL_REF", IDL_MSG_LONGJMP, NULL);
char *signal_data_ref_t_sdef_GENERIC_SIGNAL_REF;
signal_data_ref_t_sdef_GENERIC_SIGNAL_REF = (char *) (signal_ref_IDLdata + offset);

offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "GENERIC_SIGNAL_NAME", IDL_MSG_LONGJMP, NULL);
IDL_STRING *generic_signals_t_sdef_GENERIC_SIGNAL_NAME;
generic_signals_t_sdef_GENERIC_SIGNAL_NAME = (IDL_STRING *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
IDL_StrStore(generic_signals_t_sdef_GENERIC_SIGNAL_NAME, signal_ref.generic_signal_ref.generic_signal_name);
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "DESCRIPTION", IDL_MSG_LONGJMP, NULL);
IDL_STRING *generic_signals_t_sdef_DESCRIPTION;
generic_signals_t_sdef_DESCRIPTION = (IDL_STRING *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
IDL_StrStore(generic_signals_t_sdef_DESCRIPTION, signal_ref.generic_signal_ref.description);
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "LAST_RECORD_NUMBER", IDL_MSG_LONGJMP, NULL);
IDL_LONG *generic_signals_t_sdef_LAST_RECORD_NUMBER;
generic_signals_t_sdef_LAST_RECORD_NUMBER = (IDL_LONG *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
*generic_signals_t_sdef_LAST_RECORD_NUMBER = signal_ref.generic_signal_ref.last_record_number;
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "FIRST_RECORD_NUMBER", IDL_MSG_LONGJMP, NULL);
IDL_LONG *generic_signals_t_sdef_FIRST_RECORD_NUMBER;
generic_signals_t_sdef_FIRST_RECORD_NUMBER = (IDL_LONG *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
*generic_signals_t_sdef_FIRST_RECORD_NUMBER = signal_ref.generic_signal_ref.first_record_number;
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "AXIS3_ID", IDL_MSG_LONGJMP, NULL);
IDL_LONG *generic_signals_t_sdef_AXIS3_ID;
generic_signals_t_sdef_AXIS3_ID = (IDL_LONG *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
*generic_signals_t_sdef_AXIS3_ID = signal_ref.generic_signal_ref.axis3_id;
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "TIME_AXIS_ID", IDL_MSG_LONGJMP, NULL);
IDL_LONG *generic_signals_t_sdef_TIME_AXIS_ID;
generic_signals_t_sdef_TIME_AXIS_ID = (IDL_LONG *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
*generic_signals_t_sdef_TIME_AXIS_ID = signal_ref.generic_signal_ref.time_axis_id;
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "AXIS2_ID", IDL_MSG_LONGJMP, NULL);
IDL_LONG *generic_signals_t_sdef_AXIS2_ID;
generic_signals_t_sdef_AXIS2_ID = (IDL_LONG *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
*generic_signals_t_sdef_AXIS2_ID = signal_ref.generic_signal_ref.axis2_id;
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "AXIS1_ID", IDL_MSG_LONGJMP, NULL);
IDL_LONG *generic_signals_t_sdef_AXIS1_ID;
generic_signals_t_sdef_AXIS1_ID = (IDL_LONG *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
*generic_signals_t_sdef_AXIS1_ID = signal_ref.generic_signal_ref.axis1_id;
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "ALIAS", IDL_MSG_LONGJMP, NULL);
IDL_STRING *generic_signals_t_sdef_ALIAS;
generic_signals_t_sdef_ALIAS = (IDL_STRING *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
IDL_StrStore(generic_signals_t_sdef_ALIAS, signal_ref.generic_signal_ref.alias);
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "UNITS", IDL_MSG_LONGJMP, NULL);
IDL_STRING *generic_signals_t_sdef_UNITS;
generic_signals_t_sdef_UNITS = (IDL_STRING *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
IDL_StrStore(generic_signals_t_sdef_UNITS, signal_ref.generic_signal_ref.units);
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "GENERIC_SIGNAL_ID", IDL_MSG_LONGJMP, NULL);
IDL_LONG *generic_signals_t_sdef_GENERIC_SIGNAL_ID;
generic_signals_t_sdef_GENERIC_SIGNAL_ID = (IDL_LONG *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
*generic_signals_t_sdef_GENERIC_SIGNAL_ID = signal_ref.generic_signal_ref.generic_signal_id;
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "DATA_SOURCE_ID", IDL_MSG_LONGJMP, NULL);
IDL_INT *generic_signals_t_sdef_DATA_SOURCE_ID;
generic_signals_t_sdef_DATA_SOURCE_ID = (IDL_INT *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
*generic_signals_t_sdef_DATA_SOURCE_ID = signal_ref.generic_signal_ref.data_source_id;
offset = IDL_StructTagInfoByName(generic_signals_t_sdef, "SIGNAL_TYPE", IDL_MSG_LONGJMP, NULL);
IDL_STRING *generic_signals_t_sdef_SIGNAL_TYPE;
generic_signals_t_sdef_SIGNAL_TYPE = (IDL_STRING *) (signal_data_ref_t_sdef_GENERIC_SIGNAL_REF + offset);
IDL_StrStore(generic_signals_t_sdef_SIGNAL_TYPE, signal_ref.generic_signal_ref.signal_type);

  free((void*)cdb_signal.data);
  free((void*)cdb_signal.dims);
  free(cdb_signal.time_axis->data);
 // free(cdb_signal.time_axis->dims);  //TENTO POINTER SE NEPOUZIVA A MA NAHODNY OBSAH!!!!!
 // free(cdb_signal.axes->data);
  //free(cdb_signal.axes->dims);
  free(cdb_signal.time_axis->units);
  free(cdb_signal.time_axis->description);
  free(cdb_signal.time_axis);
 // free(cdb_signal.axes);   //CELE POLE POLE AXES NYNI NENI VYPLNENO - NEKONZISTENTNI
  //free(cdb_signal.axes);

  free((void*)cdb_signal.units);
  free((void*)cdb_signal.description);

//free signal_ref structure
  free(signal_ref.full_file_path);
  
  free(signal_ref.data_file_ref.collection_name);
  free(signal_ref.data_file_ref.data_format);
  free(signal_ref.data_file_ref.file_name);
  //free(signal_ref.data_file_ref); //not a pointer, this substructure is "contained"

  free(signal_ref.data_signal_ref.note);
  free(signal_ref.data_signal_ref.data_file_key);
  //free(signal_ref.data_signal_ref); //not a pointer, this substructure is "contained"

  free(signal_ref.generic_signal_ref.generic_signal_name);
  free(signal_ref.generic_signal_ref.description);
  free(signal_ref.generic_signal_ref.alias);
  free(signal_ref.generic_signal_ref.units);
  free(signal_ref.generic_signal_ref.signal_type);
  //free(signal_ref.generic_signal_ref); //not a pointer, this substructure is "contained"


 return signal_ref_IDLptr;

}



int IDL_Load(void)
{
  /*
   * These tables contain information on the functions and procedures
   * that make up the TESTMODULE DLM. The information contained in these
   * tables must be identical to that contained in testmodule.dlm.
   */
  static IDL_SYSFUN_DEF2 function_addr[] = {
    { get_full_signal_ref, "GET_FULL_SIGNAL_REF", 0, IDL_MAXPARAMS, 0, 0},
    { get_signal, "GET_SIGNAL", 0, IDL_MAXPARAMS, IDL_SYSFUN_DEF_F_KEYWORDS, 0},
//    { testfun_struct, "TESTFUN_STRUCT", 0, IDL_MAXPARAMS, 0, 0},
  };

  static IDL_SYSFUN_DEF2 procedure_addr[] = {
   // { (IDL_FUN_RET) testpro, "TESTPRO", 0, IDL_MAX_ARRAY_DIM, 0, 0},
  };

  /*
   * Create a message block to hold our messages. Save its handle where
   * the other routines can access it.
   */
  if (!(msg_block = IDL_MessageDefineBlock("IDL_CDB_module", IDL_CARRAY_ELTS(msg_arr), msg_arr)))
    return IDL_FALSE;

  /*
   * Register our routine. The routines must be specified exactly the same
   * as in testmodule.dlm.
   */
  return IDL_SysRtnAdd(function_addr, TRUE, IDL_CARRAY_ELTS(function_addr))
    && IDL_SysRtnAdd(procedure_addr, FALSE, IDL_CARRAY_ELTS(procedure_addr));
}
