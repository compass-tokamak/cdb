function struct_to_parameterList, structure
  parameterList = OBJ_NEW("IDLjavaObject$CZ_CAS_IPP_COMPASS_JYCDB_UTIL_PARAMETERLIST", "cz.cas.ipp.compass.jycdb.util.ParameterList")
  if size(structure, /type) eq 8 then begin
    tags = tag_names(structure)
    for i = 0, n_tags(structure) - 1 do parameterList->put, strlowcase(tags[i]), structure.(i)
  endif
  return, parameterList
end

function CDB_client::Init, db=db, user=user, log_level=log_level, debug=debug, static=static
  CATCH, error_status & IF (error_status NE 0) THEN begin
    debugJava, debug & catch, /cancel & message, /reissue_last
  endif

  self.glb_axes_names = ['time_axis','axis1','axis2','axis3','axis4','axis5','axis6']
  if keyword_set(static) then begin
    if obj_valid(scope_varfetch('CDB_java_Client_instance', level=1, /enter)) then begin
      self.javaClient = scope_varfetch('CDB_java_Client_instance', level=1)
      if n_elements(db) + n_elements(user) + n_elements(log_level) gt 0 then $
        print, '% WARNING: An existing CDB_client was used, therefore the keywords "db", "user", "log_level", etc. were ignored.'
      return, 1
    endif
  endif
  print, 'Loading JyCDB libraries...'
  oJava = OBJ_NEW("IDLjavaObject$Static$CZ_CAS_IPP_COMPASS_JYCDB_CDBCLIENT", "cz.cas.ipp.compass.jycdb.CDBClient")
  parameterList = struct_to_parameterList(nic)
  ;OBJ_NEW("IDLjavcdaObject$CZ_CAS_IPP_COMPASS_JYCDB_UTIL_PARAMETERLIST", "cz.cas.ipp.compass.jycdb.util.ParameterList")
  if keyword_set(db) then parameterList->put, 'db', db
  if keyword_set(user) then parameterList->put, 'user', user
  if keyword_set(log_level) then parameterList->put, 'log_level', log_level
  
  print, 'retrieving a connection to the CDB database...'
  if parameterList->isEmpty() then self.javaClient = oJava->getInstance() else $
                                   self.javaClient = oJava->getInstance(parameterList)
  if keyword_set(static) then (scope_varfetch('CDB_java_Client_instance', level=1, /enter)) = self.javaClient
  return, 1
end

function javaDictionaryToStruct, obj
  struct = 0 ; structure can only be created as soon as the first field is known 
  keys = obj->getDictKeys()
  for i = 0, n_elements(keys) -1 do begin
    javaValue = obj->getDictValue(keys[i])
    ;print, typename(javaValue)
    if not obj_valid(javaValue) then begin
      if strpos('alias description note units data_file_key', keys[i]) ne -1 then idlValue='' else idlValue=0LL 
    endif else begin
      case obj_class(javaValue) of
        'IDLJAVAOBJECT$JAVA_LANG_LONG': idlValue = javaValue->longValue() 
        'IDLJAVAOBJECT$JAVA_LANG_DOUBLE': idlValue = javaValue->doubleValue()
        'IDLJAVAOBJECT$JAVA_LANG_BOOLEAN': idlValue = javaValue->booleanValue()
        ;'IDLJAVAOBJECT$JAVA_LANG_STRING',   'IDLJAVAOBJECT$JAVA_UTIL_DATE'
        else: idlValue = javaValue->toString()
      endcase
    endelse
    obj_destroy, javaValue
    if size(struct, /TYPE) ne 8 then begin ;i.e., if struct is not yet a STRUCTURE
      struct = create_struct(keys[i], idlValue) ; create the structure with the first field
    endif else struct = create_struct(struct, keys[i], idlValue) ; add another field to existing structure
  endfor
  return, struct
end

function javaDictionaryValue, obj, index, typeOf=typeOf, typeName=type, key=key
  if not keyword_set(key) then begin
    keys = obj->getDictKeys()
    key = keys[index]
  endif
  if not keyword_set(type) then if n_elements(typeOf) gt 0 then type = size(typeOf, /tname) else message, 'Either typeOf or type keyword must be supplied!'
  case type of
    'DOUBLE': idlValue = obj->getDictValueAsDouble(key) ;javaValue->doubleValue()
    'LONG64': idlValue = obj->getDictValueAsLong(key) ;javaValue->longValue() 
    'STRING': idlValue = obj->getDictValueAsString(key) ;javaValue->toString()
    ;dalsi typy jsem zatim nevidel
  endcase
  return, idlValue
end

function num2str, number
  return, strcompress(string(number), /remove_all)
end

function create_strid, generic_signal_name_or_alias_or_id, record_number, revision
  strid = num2str(generic_signal_name_or_alias_or_id)+':'+num2str(record_number)
  if n_params() gt 2 then strid = strid + ':' + num2str(revision)
  return, strid
end

FUNCTION CDB_client::get_signal_ref, str_ID, verbose=verbose, get_shape=get_shape, debug=debug
  CATCH, error_status & IF (error_status NE 0) THEN begin
    debugJava, debug & catch, /cancel & message, /reissue_last
  endif

  if not keyword_set(verbose) then verbose=0
  signal = self.javaClient->getSignal(str_ID)
  if not OBJ_VALID(signal) then begin
       print, 'Signal not found: ' + str_ID 
       return, {isset: 0}
  endif
  sig_ref = javaDictionaryToStruct(signal->getSignalReference())
  if verbose then print, 'Got revision number:',sig_ref.revision 
  gs  = signal->getGenericSignalReference()
  gs_ref = javaDictionaryToStruct(gs)
  if gs_ref.signal_type eq 'FILE' then begin
    datafile = signal->getDataFile()
    file_ref = javaDictionaryToStruct(datafile)
    obj_destroy, datafile
  endif else file_ref=OBJ_NEW()
  ;signal->free  -- this method has been removed in JyCDB
  signalcalibration = self.javaClient->getSignalCalibration(str_ID)
  calibration = javaDictionaryToStruct(signalcalibration)
  reference = CREATE_STRUCT('isSet', 1, calibration, 'ref', sig_ref, 'gs_ref', gs_ref, 'file_ref', file_ref)
  if keyword_set(get_shape) then reference = create_struct(reference, 'shape', self->get_signal_shape(reference=reference))
  obj_destroy, signal & obj_destroy, gs & obj_destroy, signalcalibration
  return, reference
END

function conditional_transpose, data
  if size(data, /N_dim) gt 1 then return, transpose(data) else return, data 
end

FUNCTION CDB_client::get_signal_shape, reference=reference, strID=str_ID
  if not keyword_set(reference) then begin
    reference=self->get_signal_ref(str_ID)
  endif
  signal_type = reference.gs_ref.SIGNAL_TYPE
  if SIGNAL_TYPE ne 'FILE' then message, 'Only FILE type signals have information of their shape!'
  file_name = reference.file_ref.FULL_PATH
  data_file_key = reference.ref.DATA_FILE_KEY
  file_id = H5F_OPEN(file_name)
  dataset_id = H5D_OPEN(file_id, DATA_FILE_KEY)
  dataspace_id = H5D_GET_SPACE(dataset_id)
  if H5S_IS_SIMPLE(Dataspace_id) eq 0 then message, 'This signal is not simple. You can currently only get shape of simple signals.'
  shape = H5S_GET_SIMPLE_EXTENT_DIMS(Dataspace_id)
  H5S_close, dataspace_id
  H5D_CLOSE, dataset_id
  H5F_CLOSE, file_id
  shape = reverse(shape)
  while shape[-1] eq 1 do shape = shape[0:-2]
  return, shape
end

FUNCTION CDB_client::get_signal_data, reference, min_value=min_value, max_value=max_value, N_samples=N_samples, verbose=verbose, stride=stride, debug=debug
  CATCH, error_status & IF (error_status NE 0) THEN begin
    debugJava, debug & catch, /cancel & message, /reissue_last
  endif

  if reference.gs_ref.SIGNAL_TYPE eq 'FILE' then BEGIN
    if not reference.file_ref.FILE_READY then begin
      print, 'File not ready for reading according to CDB'
      reference.isSet = 0
      return, 0
    endif
    file_name = reference.file_ref.FULL_PATH
    if not FILE_TEST(file_name, /READ) then begin
      print, 'ERROR: File ready for reading according to CDB, but not in the file system!!!'
      reference.isSet = 0
      return, 0
    endif
    file_id = H5F_OPEN(file_name)
    dataset_id = H5D_OPEN(file_id, reference.ref.DATA_FILE_KEY)
    coef=reference.COEFFICIENT
    offset = reference.OFFSET
;    if keyword_set(stride) or keyword_set(N_samples) then begin
;      dataspace_id = H5D_GET_SPACE(dataset_id)
;      dims = H5S_get_simple_extent_dims(dataspace_id)
;      if n_elements(dims) ne 2 || dims[0] ne 1 then print, 'downsampling only supported for 1D signals' else begin
;        if not keyword_set(stride) then stride = ceil(dims[1]/N_samples)
;        if not keyword_set(N_samples) then N_samples = floor(dims[1]/stride)
;        H5S_SELECT_HYPERSLAB, Dataspace_id, [0,0], [1,N_samples], /RESET, STRIDE=[1,stride]
;        memory_space_id = H5S_CREATE_SIMPLE([1,N_samples])
;        data = H5D_Read(dataset_id, FILE_SPACE=dataspace_id, MEMORY_SPACE=memory_space_id)
;        H5S_close, memory_space_id
;      endelse
;      H5S_close, dataspace_id
;    endif else begin
;      stride=0
      data = H5D_Read(dataset_id)
;    endelse
    H5D_CLOSE, dataset_id
    H5F_CLOSE, file_id
    if keyword_set(verbose) then help, 'after reading from HDF5 file the data look like:',data
    if size(data, /TYPE) eq 8 then begin  ; i.e., if data is a STRUCTURE
      tagnames = tag_names(data)
      for i = 0, n_tags(data)-1 do begin
        if i eq 0 then  newdata = create_struct(tagnames(i), conditional_transpose(data.(i))) $
                  else  newdata = create_struct(newdata, tagnames(i), conditional_transpose(data.(i)))
        if offset ne 0 or coef ne 1 then newdata.(i) = (newdata.(i) + offset)*coef
      endfor
      data = newdata  ;rewrite the original data structure by the transposed one
    endif else begin
      if size(data, /N_dim) gt 1 then data = transpose(data)
      if offset ne 0 or coef ne 1 then data = (data + offset)*coef
    endelse    
    if keyword_set(verbose) then help, 'the transposed and multiplied data look like:',data
    return, data
  endif else if reference.gs_ref.signal_type eq 'LINEAR' then BEGIN
    stride=0
    if not keyword_set(min_value) then min_value = reference.ref.TIME0
    if not keyword_set(N_samples) then $   ;N_samples keyword overrides the max_value keyword
                                      if keyword_set(max_value) then begin
        N_samples = floor((max_value-min_value) / reference.ref.COEFFICIENT)
    endif else message, 'cannot generate linear signal without either N_samples or max_value'
    data = dindgen(N_samples)*reference.ref.COEFFICIENT + min_value
    return, data
  endif else message, 'unknown signal type'
  return, 0
END

function struct_has_tag, structure, tag
  tagnames = tag_names(structure)
  return, total( strcmp(tagnames, tag, /fold_case) ) gt 0
end

function struct_tag_by_name, structure, tag
  indexes = where(strcmp(tag_names(structure), tag, /fold_case))
  return, structure.(indexes)
end

FUNCTION CDB_client::get_signal, str_ID, min_time=min_time, max_time=max_time, N_samples=N_samples, verbose=verbose, stride=stride, debug=debug
  CATCH, error_status & IF (error_status NE 0) THEN begin
    debugJava, debug & catch, /cancel & message, /reissue_last
  endif

    reference = self->get_signal_ref(str_ID, verbose=verbose, debug=debug)
    if not reference.isSet then return, reference
    data = self->get_signal_data(reference, min_value=min_time, max_value=max_time, N_samples=N_samples, verbose=verbose, stride=stride, debug=debug)
    if not reference.isSet then return, reference
    struct = create_struct(reference, 'data', data)
    for i = 0, n_elements(self.glb_axes_names)-1 do begin
      ax_name = self.glb_axes_names[i]
      if struct_has_tag(reference.gs_ref, ax_name+'_ID') then begin
        if i eq 0 then ax_id = struct_tag_by_name(reference.ref, ax_name+'_ID') else ax_id = 0 
        ; if time_axis_id is not 0 in structure REF (i.e., data signal reference), it must be used instead of GS_REF.time_axis_id
        if ax_id eq 0 then ax_id = struct_tag_by_name(reference.gs_ref, ax_name+'_ID')
        if ax_id ne 0 then begin
          ax_revision = struct_tag_by_name(reference.ref, ax_name+'_REVISION')
          ax_str_ID = create_strid(ax_id, reference.ref.RECORD_NUMBER, ax_revision)
          ax_lenght = (size(data, /dimensions))[i]
          if i eq 0 then begin
;            if stride gt 1 then ax_lenght *= stride
            ax_struct = self->get_signal(ax_str_ID, N_samples=ax_lenght, min_time=reference.ref.TIME0, debug=debug)
;            if stride gt 1 then time = ax_struct.data[0:*:stride] else time = ax_struct.data
            struct = create_struct(struct, ax_name, ax_struct) ;'time', time, 'time_axis', ax_struct)
          endif else begin
            ax_struct = self->get_signal(ax_str_ID, N_samples=ax_lenght, verbose=verbose, debug=debug)
            struct = create_struct(struct, ax_name, ax_struct)
          endelse
        endif
      endif
    endfor  
    RETURN, struct
END

function not_a_number, var
  mask=[0,1,1,1,1,1,0,0, 0,0,0,0,1,1,1,1]
  return, ~ byte(mask(size(var, /type)))
end

FUNCTION CDB_client::update_signal, gs_alias_or_id, record_number, time0=time0, $
                                 data_coefficient=data_coefficient, data_offset=data_offset, data_coef_V2unit=data_coef_V2unit, $
                                 note=note, debug=debug, $
                                 time_axis_id=time_axis_id, time_axis_revision=time_axis_revision, $
                                 data_file_key=data_file_key, data_file_id=data_file_id ;this is useful when you want to assign existing data file to a new data signal
                                 ;TODO: is there anything else to update in a signal?

  CATCH, error_status & IF (error_status NE 0) THEN begin
    debugJava, debug & catch, /cancel & message, /reissue_last
  endif
  gs = self.javaClient->getGenericSignal(gs_alias_or_id)
  if not OBJ_VALID(gs) then begin
       print, 'Generic Signal not found: ' + gs_alias_or_id 
       return, {isSet: 0}
  endif
  if not self.javaClient->recordExists(record_number) then message, 'this record number does NOT EXIST!
  gs_id = gs->getId()
  gs_name = gs->getName()
  if gs->getSignalType() eq 'FILE' then begin ;if signal if file-type, it must already exist, we will only update its references 
    signal = self.javaClient->getSignalReference(record_number, gs_id)
    if OBJ_VALID(signal) then begin
      paramStruct = {generic_signal_id: gs_id, record_number:record_number, $
        data_file_key:signal->getDataFileKey(), data_file_id:signal->getDataFileId()}
    endif else begin
      if n_elements(data_file_key)*n_elements(data_file_id) gt 0 then begin
        paramStruct = {generic_signal_id: gs_id, record_number:record_number, $
          data_file_key:data_file_key, data_file_id:data_file_id}
      endif else message, 'Signal '+gs_name+' in record number'+strcompress(string(record_number))+" hasn't been stored yet! Therefore, it cannot been updated!"
    endelse
  endif else if gs->getSignalType() eq 'LINEAR' then begin
    paramStruct = {generic_signal_id: gs_id, record_number:record_number }
  endif else message, 'only FILE and LINEAR type of signal is supported'
  paramList = struct_to_parameterList(paramStruct)
  if n_elements(time0)+n_elements(data_coefficient)+n_elements(data_coef_V2unit)+n_elements(data_offset) + $
     n_elements(note)+n_elements(time_axis_id) eq 0 then message, "You didn't supply any parameter to update in the selected signal."
  if n_elements(time0) gt 0 then paramList->put, 'time0', time0
  if n_elements(data_coefficient) gt 0 then paramList->put, 'coefficient', data_coefficient
  if n_elements(data_coef_V2unit) gt 0 then paramList->put, 'coefficient_V2unit', data_coefficient
  if n_elements(data_offset) gt 0 then paramList->put, 'offset', data_offset
  if n_elements(note) gt 0 then paramList->put, 'note', note
  if n_elements(time_axis_id) gt 0 then paramList->put, 'time_axis_id', time_axis_id
  if n_elements(time_axis_revision) gt 0 then paramList->put, 'time_axis_revision', time_axis_revision

  success = self.javaClient->storeSignal(paramList)
  if success then begin
    struct = self->get_signal_ref(create_strid(gs_id, record_number), debug=debug)
    print, gs_name+": signal was succesfully stored as a revision no."+strcompress(string(struct.ref.revision))
    return, struct
  endif else begin
    print, "Didn't succeed to store the signal into database."
    return, {isSet: 0}  
  endelse
END

pro nullify, variable
  if n_elements(variable) gt 0 then temp = temporary(variable)  ; function temporary "steals" the value from its argument, which becomes undefined
END

FUNCTION CDB_client::put_file, gs_alias_or_id, record_number, source_filename, file_extension, data_format=data_format, note=note, debug=debug
  CATCH, error_status & IF (error_status NE 0) THEN begin
    debugJava, debug & catch, /cancel & message, /reissue_last
  endif
  gs = self.javaClient->getGenericSignal(gs_alias_or_id)
  if not OBJ_VALID(gs) then begin
    print, 'Generic Signal not found: ' + gs_alias_or_id
    return, {isset: 0}
  endif
  if not self.javaClient->recordExists(record_number) then message, 'this record number does NOT EXIST!'
  if gs->getSignalType() ne 'FILE' then message, 'This generic signal is not file-type!!'
  if not keyword_set(file_extension) then message, 'File_extension (4th parameter) is obligatory! Use e.g. "xml".'
  if not keyword_set(data_format) then data_format = 'GENERIC' ;DATA FORMAT or FILE FORMAT ?????
  if not file_test(source_filename, /read) then message, 'The file which should be stored into CDB does not exist! (or is not readable)'
  ; after all checks were passed, we can start creating a dataset
  collection = gs->getName() ; Set collection name = signal name -- this is an arbitrary choice
  datafile_paramList = struct_to_parameterList({collection_name: collection, record_number:record_number, $
    data_source_id: gs->getDataSourceId(), file_format: data_format, file_extension: file_extension})
  data_file = self.javaClient->newDataFile(datafile_paramList);
  data_file_id = data_file->getId();cdb
  paramStruct = {generic_signal_id: gs->getID(), record_number:record_number, data_file_id:data_file_id}
  paramList = struct_to_parameterList(paramStruct)
  if keyword_set(note) then paramList->put, 'note', note
  ;if keyword_set(collection) then paramList->put, 'data_file_key', collection
  destination_filename = data_file->getFullPath()
  
 ; if not file_test(destination_filename, /write) then message, "Not possible to write to the CDB location."
  FILE_COPY, source_filename, destination_filename, /verbose
  
  self.javaClient->setFileReady, data_file_id ; tell the database that the file is ready to read
  success = self.javaClient->storeSignal(paramList)
  if success then begin
    struct = self->get_signal_ref(create_strid(gs->getId(), record_number), debug=debug)
    print, gs->getName()+": signal was succesfully stored as a revision no."+strcompress(string(struct.ref.revision))
  endif else begin
    print, string(gs_alias_or_id)+": Didn't succeed to store the signal into database."
    struct = {isset: 0}
  endelse
  return, struct
END 

FUNCTION CDB_client::put_signal, gs_alias_or_id, record_number, data, time0=time0, $
                                 data_coefficient=data_coefficient, data_offset=data_offset, data_coef_V2unit=data_coef_V2unit, $
                                 note=note, debug=debug, $
                                 time_axis_data=time_axis_data, time_axis_coef=time_axis_coef, time_axis_offset=time_axis_offset, time_axis_note=time_axis_note, time_axis_id=time_axis_id,$
                                 axis1_data=axis1_data, axis1_coef=axis1_coef, axis1_offset=axis1_offset, axis1_note=axis1_note,$
                                 axis2_data=axis2_data, axis2_coef=axis2_coef, axis2_offset=axis2_offset, axis2_note=axis2_note,$
                                 axis3_data=axis3_data, axis3_coef=axis3_coef, axis3_offset=axis3_offset, axis3_note=axis3_note,$
                                 axis4_data=axis4_data, axis4_coef=axis4_coef, axis4_offset=axis4_offset, axis4_note=axis4_note,$
                                 axis5_data=axis5_data, axis5_coef=axis5_coef, axis5_offset=axis5_offset, axis5_note=axis5_note,$
                                 axis6_data=axis6_data, axis6_coef=axis6_coef, axis6_offset=axis6_offset, axis6_note=axis6_note
  
  CATCH, error_status & IF (error_status NE 0) THEN begin
    debugJava, debug & catch, /cancel & message, /reissue_last
  endif
  gs = self.javaClient->getGenericSignal(gs_alias_or_id)
  if not OBJ_VALID(gs) then begin
       print, 'Generic Signal not found: ' + gs_alias_or_id 
       return, {isset: 0}
  endif
  if not self.javaClient->recordExists(record_number) then message, 'this record number does NOT EXIST!'
  if gs->getSignalType() ne 'FILE' then message, 'This method is designed to store only file-type signals!!'
  ;TODO: ensure that file type is HDF5!! Or, is it necessary to perform some other check on the supplied data??
  if n_elements(data) eq 0 then begin
    message, 'You have to supply data. If you want to update other information than the data itself, please use function "update_signal"
    ;TODO: this could be possibility to renew axis for existing signal. User doesn't supply data, just axis. 
            ;Furthermore, it can be checked here that axis has correct number of elements etc..
  endif
  if not_a_number(data) then message, 'DATA must be of numeric type!'
  if n_elements(data_coefficient) ge 1 then if n_elements(data_coefficient) ge 2 || not_a_number(data_coefficient) || data_coefficient eq 0 then message, 'data_coef is expected to be a scalar number other than zero!'
  if n_elements(data_coef_V2unit) ge 1 then if n_elements(data_coef_V2unit) ge 2 || not_a_number(data_coef_V2unit) || data_coef_V2unit eq 0 then message, 'data_coef_V2unit is expected to be a scalar number other than zero!'
  if n_elements(data_offset) ge 1 then if n_elements(data_offset) ge 2 || not_a_number(data_offset) then message,'data_offset is expected to be a scalar number!'
            
  datadim = size(data, /dim)
  ; at first, check that data for all axes are supplied correctly
  test = 0
  for i = 0, n_elements(self.glb_axes_names)-1 do begin
      ax_name = self.glb_axes_names[i]
      if n_elements(scope_varfetch(ax_name+'_data')) gt 0 then ax_data = scope_varfetch(ax_name+'_data') else nullify, ax_data
      if n_elements(scope_varfetch(ax_name+'_coef')) gt 0 then ax_coef = scope_varfetch(ax_name+'_coef') else nullify, ax_coef
      if n_elements(scope_varfetch(ax_name+'_offset')) gt 0 then ax_offset = scope_varfetch(ax_name+'_offset') else nullify, ax_offset
      if n_elements(ax_data)+n_elements(ax_coef)+n_elements(ax_offsest) gt 0 then begin  ; user wants to store this axis - by either of the ways
        if i eq 0 && n_elements(time_axis_id) > 0 then begin  ; specially for time axis, user can change the axis_ID
          ax_id = time_axis_id
          if size(ax_id, /tname) ne 'INT' || ax_id le 0 then message, "time_axis_id must be a positive integer!"
        endif else begin
          ax_id = gs->getAxisId(i)
          if ax_id eq 0 then message, 'the generic signal '+gs->getName()+' does not have any '+ax_name+' attached!'
        endelse 
        ax_gs = self.javaClient->getGenericSignal(ax_id)
        ax_type = ax_gs->getSignalType()
        test += 1 
        if n_elements(ax_data) gt 0 then begin
            if ax_type ne 'FILE' then message, ax_name+' of the generic signal '+gs->getName()+' is not of type FILE! (its type is '+ax_type+')'
            if n_elements(ax_data) ne datadim[i] then message, ax_name+'_data must have the same number of elements as the'+strcompress(string(i))+'. dimension of data!'
            if n_elements(ax_coef)+n_elements(ax_offset) gt 0 then print, 'WARNING: for FILE-type '+ax_name+', neither coefficient nor offset is usually supplied'
        endif else begin
            if n_elements(ax_coef) ge 1 then if n_elements(ax_coef) ge 2 || not_a_number(ax_coef) || ax_coef eq 0 then message, ax_name+'_coef is expected to be a scalar number other than zero!'
            if n_elements(ax_offset) ge 1 then if n_elements(ax_offset) ge 2 || not_a_number(ax_offset) then message,ax_name+'_offset is expected to be a scalar number!'
            if ax_type ne 'LINEAR' then message, ax_name+' of the generic signal '+gs->getName()+' is not of type LINEAR! (its type is '+ax_type+')'
            if n_elements(ax_coef) eq 0 then print, 'WARNING: '+ax_name+'_coef is not supplied. Using the default value 1.00'
            if i ne 0 && n_elements(ax_offset) eq 0 then print, 'WARNING: '+ax_name+'_offset is not supplied. Using the default value 0.00'  $ ___
              else if i eq 0 && n_elements(ax_offset) ne 0 then print, 'WARNING: time_axis_offset usually does not make any sense. ', 'The "time0" value of the data signal itself is normally used.'            
        endelse
      endif  
  endfor
  ; after all checks were passed, we can start creating a dataset
  collection = gs->getName() ; Set collection name = signal name -- this is an arbitrary choice
  data_file = self.javaClient->newDataFile(collection, record_number, gs->getDataSourceId()); 
  ;TODO: HOW DOES THE DATABASE KNOW THAT WE WILL STORE A HDF5 FILE???
  data_file_id = data_file->getId();cdb
  paramStruct = {generic_signal_id: gs->getID(), record_number:record_number, $
                 data_file_key:collection, data_file_id:data_file_id}
  paramList = struct_to_parameterList(paramStruct)
  if keyword_set(time0) then paramList->put, 'time0', time0
  if keyword_set(data_coefficient) then paramList->put, 'coefficient', data_coefficient
  if keyword_set(data_coef_V2unit) then paramList->put, 'coefficient_V2unit', data_coefficient
  if keyword_set(data_offset) then paramList->put, 'offset', data_offset
  if keyword_set(note) then paramList->put, 'note', note
  ; at first, store all the axis, if there are any (i.e., variable test is > 0 )
  if test gt 0 then for i = 0, n_elements(self.glb_axes_names)-1 do begin
      ax_name = self.glb_axes_names[i]
      if n_elements(scope_varfetch(ax_name+'_data')) gt 0 then ax_data = scope_varfetch(ax_name+'_data') else nullify, ax_data
      if n_elements(scope_varfetch(ax_name+'_coef')) gt 0 then ax_coef = scope_varfetch(ax_name+'_coef') else nullify, ax_coef
      if n_elements(scope_varfetch(ax_name+'_offset')) gt 0 then ax_offset = scope_varfetch(ax_name+'_offset') else nullify, ax_offset
      if n_elements(scope_varfetch(ax_name+'_note')) gt 0 then ax_note = scope_varfetch(ax_name+'_note') else nullify, ax_note
      if n_elements(ax_data)+n_elements(ax_coef)+n_elements(ax_offset) gt 0 then begin  ; user wants to store this axis - by either of the ways
        if i eq 0 && n_elements(time_axis_id) > 0 then begin  ; specially for time axis, user can change the axis_ID
          ax_id = time_axis_id
          paramList->put, 'time_axis_id', time_axis_id
        endif else ax_id = gs->getAxisId(i)
        if n_elements(ax_data) gt 0 then begin
            print, 'Storing data for '+ax_name+'...' 
            ax_struct = self->put_signal(ax_id,record_number,ax_data, data_coefficient=ax_coef, data_offset=ax_offset, debug=debug)
        endif else if n_elements(ax_coef)+n_elements(ax_offset) gt 0 then begin
            print, 'Storing coefficient and/or offset for '+ax_name+'...' 
            ax_struct = self->update_signal(ax_id, record_number, data_coefficient=ax_coef, data_offset=ax_offset, note=ax_note, debug=debug)
        endif
        paramList->put, ax_name+'_revision', ax_struct.ref.revision
        if size(axes_struct, /type) eq 8 then begin  ;if axes_struct already contains a structure, it is necessary to concatenate
          axes_struct = CREATE_STRUCT(axes_struct, ax_name, ax_struct)
        endif else axes_struct = CREATE_STRUCT(ax_name, ax_struct)
      endif  
  endfor
; now, continue with storing the data itself
  file_name = data_file->getFullPath()
  datasetname = collection ; Choose datasetname = collection -- this is again an arbitrary choice
  if size(data, /N_dim) gt 1 then dataT = transpose(data) else dataT = data
;  fid = H5F_CREATE(file_name)
;  datatype_id = H5T_IDL_CREATE(dataT)
;  dataspace_id = H5S_CREATE_SIMPLE(size(dataT,/DIMENSIONS))
;  dataset_id = H5D_CREATE(fid,datasetname,datatype_id,dataspace_id)
;  H5D_WRITE,dataset_id,dataT
;  H5D_CLOSE,dataset_id
;  H5S_CLOSE,dataspace_id
;  H5T_CLOSE,datatype_id
;  H5F_CLOSE,fid
  H5_create, file_name, {_NAME: datasetname, _TYPE: 'dataset', _DATA: dataT}
  self.javaClient->setFileReady, data_file_id ; tell the database that the file is ready to read
 
  ;success = self.javaClient->storeSignal(gs->getId(), record_number, datasetname, data_file_id, time0, 1., 0.);
  success = self.javaClient->storeSignal(paramList)
  if success then begin
    struct = self->get_signal_ref(create_strid(gs->getId(), record_number), debug=debug)
    print, gs->getName()+": signal was succesfully stored as a revision no."+strcompress(string(struct.ref.revision))
  endif else begin
    print, string(gs_alias_or_id)+": Didn't succeed to store the signal into database."
    struct = {isset: 0}  
  endelse
  if test gt 0 then struct = CREATE_STRUCT(struct, axes_struct)
  return, struct
END

FUNCTION CDB_client::last_record_number, recordType
  if n_elements(recordType) eq 0 then recordType = 'EXP'
  rec_number = self.javaClient->lastRecordNumber(recordType)
  if rec_number eq -1 then print, 'The record type '+recordType+' is not present in this database!'
  return, rec_number
END
  
FUNCTION CDB_client::get_signal_references, alias_or_name=alias_or_name, record_number=record_number, revision=revision, generic_signal_id=generic_signal_id, count=count, debug=debug
  CATCH, error_status & IF (error_status NE 0) THEN begin
    debugJava, debug & catch, /cancel & message, /reissue_last
  endif
  
  if keyword_set(alias_or_name)+keyword_set(record_number)+keyword_set(generic_signal_id) lt 1 then begin
      message, "I'm not going to give you references of all signals in all shots!"  
  endif
  parameterList =  struct_to_parameterList()
  if keyword_set(alias_or_name) then parameterList->put, "alias_or_name", alias_or_name
  if keyword_set(record_number) then parameterList->put, "record_number", record_number
  if keyword_set(revision) then parameterList->put, "revision", revision
  if keyword_set(generic_signal_id) then parameterList->put, "generic_signal_id", generic_signal_id
  signals = self.javaClient->getSignalReferences(parameterList)
  if not signals->isEmpty() then begin
    if keyword_set(count) then return, signals->size()
    for i = 0L, signals->size() -1 do begin
       sig_ref = javaDictionaryToStruct(signals->get(i))
       if i eq 0 then sig_array = [sig_ref] else sig_array = [sig_array, sig_ref]
    endfor
    return, sig_array
  endif else return, 0
end

FUNCTION CDB_client::get_generic_signal_references, alias_or_name=alias_or_name, generic_signal_id=generic_signal_id, count=count, debug=debug
  CATCH, error_status & IF (error_status NE 0) THEN begin
    debugJava, debug & catch, /cancel & message, /reissue_last
  endif

  parameterList =  struct_to_parameterList()
  if keyword_set(alias_or_name) then parameterList->put, "alias_or_name", alias_or_name
  if keyword_set(generic_signal_id) then parameterList->put, "generic_signal_id", generic_signal_id
  signals = self.javaClient->getGenericSignals(parameterList)
  if not signals->isEmpty() then begin
    if keyword_set(count) then return, signals->size()
    for i = 0L, signals->size() -1 do begin
       sig_ref = javaDictionaryToStruct(signals->get(i))
       if i eq 0 then sig_array = [sig_ref] else sig_array = [sig_array, sig_ref]
    endfor
    return, sig_array
  endif else begin
    print, 'No generic signal was found.'
    return, 0
  endelse
end

FUNCTION CDB_client::find_generic_signals, alias_or_name, regexp=regexp, debug=debug
  CATCH, error_status & IF (error_status NE 0) THEN begin
    debugJava, debug & catch, /cancel & message, /reissue_last
  endif

  if not keyword_set(alias_or_name) then alias_or_name = ''
  if not keyword_set(regexp) then regexp = 0
  signals = self.javaClient->findGenericSignals(alias_or_name, regexp)
  if not signals->isEmpty() then begin
    print, signals->size(), ' generic signals have been found.'
    if keyword_set(count) then return, signals->size()
    javaSignal = signals->get(0)
    sig_ref = javaDictionaryToStruct(javaSignal)
    sig_array = replicate(sig_ref, signals->size())
    keys = javaSignal->getDictKeys()
    types = strarr(n_elements(keys))
    for t = 0, n_elements(keys)-1 do types[t] = size(sig_ref.(t), /tname) 
    print,  'Transforming to a structure array...'
    for i = 1L, signals->size() -1 do begin
       javaSignal = signals->get(i)
       for t = 0, n_elements(keys) - 1 do sig_array[i].(t) = javaDictionaryValue(javaSignal, typeName=types[t], key=keys[t])
    endfor
    return, sig_array
  endif else begin
    print, 'No generic signal was found.'
    return, 0
  endelse
end

FUNCTION CDB_client::get_signal_names, record_number=record_number, generic_signal_id=generic_signal_id, no_alias=no_alias, debug=debug
  CATCH, error_status & IF (error_status NE 0) THEN begin
    debugJava, debug & catch, /cancel & message, /reissue_last
  endif

  parameterList = struct_to_parameterList()
  ;if keyword_set(alias_or_name) then parameterList->put, "alias_or_name", alias_or_name
  if keyword_set(record_number) then begin
    if keyword_set(generic_signal_id) then print, 'WARNING: generic_signal_id is ignored in this function call'
    parameterList->put, "record_number", record_number
    parameterList->put, "revision", 1L
    ;if keyword_set(generic_signal_id) then parameterList->put, "generic_signal_id", generic_signal_id  
    signals = self.javaClient->getSignalReferences(parameterList)
    if not signals->isEmpty() then begin
      names = strarr(signals->size())
      for i = 0L, signals->size() - 1 do begin
         signal = signals->get(i)
         id = signal->getDictValue('generic_signal_id')
         names[i] = (self->get_signal_names(generic_signal_id=id->longValue(), debug=debug))[0]
      endfor
      return, names
    endif else return, ['']
  endif else begin
    if keyword_set(generic_signal_id) then parameterList->put, "generic_signal_id", generic_signal_id
    Signals = self.javaClient->getGenericSignals(parameterList)
    if not signals->isEmpty() then begin
      names = strarr(signals->size())
      for i = 0L, signals->size() - 1 do begin
        signal = signals->get(i)
        if not keyword_set(no_alias) then name = signal->getDictValue('alias') else name = obj_new()
        if obj_valid(name) eq 0 || name->toString() eq '' then name = signal->getDictValue('generic_signal_name')
        if obj_valid(name) then names[i] = name->toString() else names[i] = ''
      endfor
      return, transpose(names)
    endif else return, ['']
  endelse    
END

PRO debugJava, debug
  if !ERROR_STATE.NAME eq 'IDL_M_USER_ERR' then return
  oJSession = OBJ_NEW('IDLJavaObject$IDLJAVABRIDGESESSION')
  oJExc = oJSession->GetException()
  if OBJ_VALID(oJExc) then begin
    PRINT, '% ERROR in JyCDB: ', oJExc->ToString()
    ;Print, "Stack Trace will be printed out, and then the current routine will repeat once more:"
    oJExc->PrintStackTrace
    OBJ_DESTROY, oJExc
    if not keyword_set(debug) then retall
  endif else print, 'No exception has been thrown in JyCDB client.'
  obj_destroy, oJSession
END

pro CDB_client::Cleanup
  obj_destroy, self.javaClient
end

PRO CDB_client__define
    struct = { CDB_client, javaClient:OBJ_NEW(), glb_axes_names : strarr(7)}
END
