from __future__ import division
import json

try:
    import pyCDB.client as cdb
#    import pyCDB.DAQ as daq
    import numpy as np
    cimport numpy as np
    np.import_array()
except ImportError as ie:
    print ("Error: some of the modules could not be loaded, see below:")
    print ( ie.message )

from libc.stdlib cimport malloc, free
from libc.string cimport memcpy
include "cyCDB_api.pxi"
cdef extern from "Python.h":
    pass

cdef char* cdbstr(strin):
    '''Convert Python string into a C char*.'''
    newPythonStr = ''
    if strin is not None: newPythonStr = str(strin)
    cdef char* oldStr
    oldStr = newPythonStr
    cdef char* newStr
    length = len(newPythonStr)
    newStr = <char*> malloc(length + 1)
    memcpy(<void*>newStr, <void*>oldStr, length)
    newStr[length] = "\0"
    return newStr

# *** Utility functions to deal with dictionaries ***
from cpython.ref cimport Py_CLEAR, Py_XDECREF, PyObject

cdef api make_dict():
    '''Return an empty Python dictionary.

    The dictionary does not get garbage collected.
    You have to call Py_XDECREF(d) or destroy(d) on it.
    '''
    return cdb.OrderedDict()

cdef api dict_set_string(a_dict, const char* key, const char* value):
    '''Add a string value to a dict-like object.'''
    a_dict[key] = value

cdef api dict_set_int(a_dict, const char* key, int value):
    '''Add an integer value to a dict-like object.'''
    a_dict[key] = value

cdef api dict_set_double(a_dict, const char* key, double value):
    '''Add a double value to a dict-like object.'''
    a_dict[key] = value

cdef api destroy(PyObject* obj):
    '''Enable destruction of a Python object.

    This decreases reference count which for objects returned from
    Cython functions is equal to 1. However, in different situations,
    this might not work.
    '''
    Py_XDECREF(obj)

cdef api char* to_json(obj):
    '''Convert an object to its JSON representation.

    Note: You have to free the memory occupied by the result yourself.
    '''
    json_str = json.dumps(obj, indent=4)
    return cdbstr(json_str)

# global client object
cdef object __conn = None

# this is how to convert to void*
cdef extern from *:
    cdef void* tovoid "(void*)"(object)

# *** Structures (apart from those defined in cyCDB_api.pxi) ***

cdef public struct signal_data_ref_t:
    # Structure for full data signal reference, including file reference
    #
    # TODO: Proper decomposition does not work
    int isset
    long record_number
    char* full_file_path
    data_files_t data_file_ref
    data_signals_t data_signal_ref
    generic_signals_t generic_signal_ref

cdef public struct cdb_data_file_t:
    # Structure that is returned by new_data_file
    char* full_path
    data_files_t data_file
    int created
    int file_ready

# *** Exported CDB functions ***

cdef api cdb_connect():
    '''Create a connection to CDB.'''
    global __conn
    if __conn is None:
        try:
            __conn = cdb.CDBClient()
        except BaseException as e:
            print("exception", e)
            __conn = None
    if __conn is None or not __conn.check_connection(True):
        print("error: connection to CDB server could not be established")
    return __conn

cdef api long cdb_last_record_number(char* record_type):
    conn = cdb_connect()
    cdef long res
    res = conn.last_record_number(record_type)
    return res

cdef api long cdb_last_record_number_exp():
    return cdb_last_record_number("EXP")

cdef api char* cdb_get_record_path(long record_number):
    conn = cdb_connect()
    res = conn.get_record_path(record_number = record_number)
    return cdbstr(res)

cdef api int cdb_is_file_ready(long data_file_id):
    cdef int isready = 0
    conn = cdb_connect()
    isready = conn.is_file_ready(data_file_id)
    return isready

cdef api generic_signals_t cdb_get_generic_signal_ref_by_id(long generic_signal_id):
    cdef generic_signals_t res_c
    res_c.isset = 0
    conn = cdb_connect()
    ref =  conn.get_generic_signal_references(generic_signal_id = generic_signal_id)
    if len(ref)==1:
        res_c = assign_generic_signals(ref[0])
    else:
        print('warning: no generic signal found')
    return res_c

cdef api channel_attachments_t cdb_get_channel_attachment_by_FS(char* nodeuniqueid, char* hardwareuniqueid, char* parameteruniqueid):
    '''Return channel attachment.'''
    cdef channel_attachments_t attachment
    attachment.isset = 0
    conn = cdb_connect()
    if conn is None:
        print("Error connecting to CDB.")
    else:
        try:
            channel = conn.FS2CDB_ref(nodeuniqueid, hardwareuniqueid, parameteruniqueid)
            attachments = conn.get_attachment_table(computer_id=channel.computer_id, board_id=channel.board_id, channel_id=channel.channel_id)
            attachment = assign_channel_attachments(attachments[0])
        except:
            print("Error when reading channel attachment.")
    return attachment

cdef api data_signals_t cdb_get_signal_reference(long generic_signal_id, long record_number, int revision):
    cdef data_signals_t signal_ref
    signal_ref.isset = 0
    conn = cdb_connect()
    if revision == -1:
        signal_references = conn.get_signal_references(record_number=record_number, generic_signal_id=generic_signal_id)
    else:
        signal_references = conn.get_signal_references(record_number=record_number, generic_signal_id=generic_signal_id, revision=revision)
    if len(signal_references) < 1:
        # print("Error getting generic signal reference")
        pass
    elif len(signal_references) > 1:
        print("Ambiguous signal reference")
    else:
        signal_ref = assign_data_signals(signal_references[-1])
    return signal_ref

cdef api cdb_data_file_t cdb_new_data_file(long record_number, const char* collection_name,
        const char* file_format, long data_source_id):
    '''Create a new data_file.'''
    cdef cdb_data_file_t value
    value.full_path = ""
    value.created = False
    conn = cdb_connect()
    try:
        res = conn.new_data_file( collection_name = collection_name, file_name_explicit = None, file_format = file_format, data_source_id = data_source_id, record_number = record_number)
        value.data_file = assign_data_files(res)
        value.full_path = res["full_path"]
        value.file_ready = 0
        value.created = 1
    except Exception as exc:
        print exc
        print("error in cdb_new_data_file")
    return value

cdef api int cdb_store_signal(kwargs):
    '''Store signal with whatever arguments.

    :param kwargs: A dict-like object with arguments to "store_signal"
    '''
    conn = cdb_connect()
    res = conn.store_signal(**kwargs)
    if res:
        return 1
    else:
        return 0

cdef api int cdb_store_linear_signal(long generic_signal_id, long record_number,
        double coefficient, double offset, double coefficient_V2unit):
    '''
    Store linear data signal for (record_number, generic axis signal) combination if it does not exist.
    Useful especially for time axes.

    If it exists, nothing will happen.

    Returns: revision_number if created, 0 if it already exists, -2 if the axis is not linear,
        -1 if there was other error in the CDB.
    '''
    conn = cdb_connect()
    if not conn:
        return -1
    refs = conn.get_signal_references(record_number = record_number, generic_signal_id = generic_signal_id)
    if len(refs) > 0:
        return refs[-1].revision
    else:
        try:
            generic_signal = conn.get_generic_signal_references(generic_signal_id=generic_signal_id)[0]
            if generic_signal.signal_type != "LINEAR":
                print("Cannot store non-linear signal as linear.")
                return -2
            new_signal = conn.store_signal( record_number = record_number, generic_signal_id = generic_signal_id,
                coefficient = coefficient, time0 = 0, offset = offset, coefficient_V2unit = coefficient_V2unit)
            return new_signal.revision
        except Exception as e:
            print(e)
            return -1

cdef api void cdb_set_file_ready(long data_file_id):
    conn = cdb_connect()
    conn.set_file_ready(data_file_id)

cdef api DAQ_channels_t cdb_get_channel_from_FS_id(char* node_id, char* hardware_id, char* parameter_id):
    ''' Get channel info based on firesignal id's (node, hardware, parameter).
    '''
    cdef DAQ_channels_t channel
    channel.isset = 0
    conn = cdb_connect()
    res = conn.FS2CDB_ref(node_id, hardware_id, parameter_id)
    if res:
        channel = assign_DAQ_channels( res )
    return channel
