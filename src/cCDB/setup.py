from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
from numpy import get_include

ext_modules = [Extension("cyCDB", ["cyCDB.pyx"],include_dirs=[get_include()], extra_compile_args=["-ggdb","-w"], extra_link_args=["-ggdb"],)]

setup(
  name = 'cyCDB',
  cmdclass = {'build_ext': build_ext},
  ext_modules = ext_modules
)
