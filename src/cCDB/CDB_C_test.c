// #include "cyCDB.h"
#include "cyCDB_api.h"
#include "Python.h"

int main(int argc, char *argv[]) {
  // Initialize Python
  Py_Initialize();
  // Py_SetProgramName(argv[0]);
  printf("argc: %i\n",argc);
  printf("argv: ");
  printf(argv[0]);
  printf("\n");
  PySys_SetArgv(argc, argv);
  // cyCDB initialization
  initcyCDB();
  import_cyCDB();
  
  // cyCDB working now
/*  long record_number = cdb_create_record("VOID", 0, "");
  printf("Created record number: %li\n", record_number);
  printf("Record path: "); printf(cdb_get_record_path(record_number)); printf("\n");
*/

  cdb_get_signal("loop_voltage_Flux_loop_08",3350,-1);
  return 0;

  int n_elements;

  char *gs_name = "electron density";
  int recnum = 1;

  struct generic_signals_t *generic_signals;
  printf("cdb_generic_signals_references\n");
  generic_signals = cdb_generic_signals_references(gs_name,recnum, &n_elements);
  if (n_elements==0) {
    printf("No data sources found\n");
  } else {
    int i;
    for(i=0; i<n_elements; i++) {
      printf("gs name: %s, desc: %s\n",(generic_signals)[i].generic_signal_name,(generic_signals)[i].description);
    }
  }


  //~ struct data_sources_t *data_sources;
  //~ printf("cdb_data_source_references\n");
  //~ data_sources = cdb_data_source_references("Interferometry", &n_elements);
  //~ if (n_elements==0) {
    //~ printf("No data sources found\n");
  //~ } else {
    //~ int i;
    //~ for(i=0; i<n_elements; i++) {
      //~ printf("source name: %s, subdir: %s, id: %i, %s\n",(data_sources)[i].name,(data_sources)[i].subdirectory,
	     //~ (data_sources)[i].data_source_id,(data_sources)[i].description);
    //~ }
  //~ }

 
  struct signal_data_ref_t signal_ref;
  printf("cdb_get_signal_data_ref\n");
  signal_ref = cdb_get_signal_data_ref(gs_name,recnum,-1);
  printf("got signal_ref rev. %i\n",signal_ref.data_signal_ref.revision);
  printf("%s : %s\n", signal_ref.full_file_path, signal_ref.data_signal_ref.data_file_key);
  struct cdb_signal_t cdb_signal;
  printf("cdb_get_signal\n");
  cdb_signal = cdb_get_signal(gs_name,recnum,-1);
  printf("got signal rev.: %i, desc.: %s\n",cdb_signal.revision,cdb_signal.description);
  if (cdb_signal.isset) {
    printf("data: %g %g %g %g %g ...\n",cdb_signal.data[0],cdb_signal.data[1],cdb_signal.data[2],
    		cdb_signal.data[3],cdb_signal.data[4]);
  }
  if (cdb_signal.time_axis!=NULL & (*cdb_signal.time_axis).isset) {
    printf("got time axis rev.: %i, desc.: %s\n",(*cdb_signal.time_axis).revision,(*cdb_signal.time_axis).description);
	  printf("time data: %g %g %g %g %g ...\n",(*cdb_signal.time_axis).data[0],(*cdb_signal.time_axis).data[1],(*cdb_signal.time_axis).data[2],
			  (*cdb_signal.time_axis).data[3],(*cdb_signal.time_axis).data[4]);
  }
  printf("cdb_signal.dims[0]=%i\n",cdb_signal.dims[0]);
  // printf("%s : %s\n", cdb_signal.signal_ref.full_file_path, signal_ref.data_signal_ref.data_file_key);

  // Finish
  int r = 0;
  if (PyErr_Occurred() != NULL) {
    r = 1;
    PyErr_Print(); /* This exits with the right code if SystemExit. */
    if (Py_FlushLine()) PyErr_Clear();
  }
  Py_Finalize();
  return r;
}
