// #include "cyCDB.h"
#include "cyCDB_api.h"
#include "Python.h"
#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[]) {
  // Initialize Python
  Py_Initialize();
  PySys_SetArgv(argc, argv);
  
  // cyCDB initialization
  initcyCDB();
  import_cyCDB();

  PyObject* d = make_dict(); 

  dict_set_string(d, "some", "thing");
  dict_set_int(d, "numba", 1);

  cout << to_json(d) << endl;

  int record = cdb_last_record_number_exp();
  cout << "Last record number: " << record << endl;

  channel_attachments_t attachment = cdb_get_channel_attachment_by_FS("PCIE_ATCA_ADC_01", "BOARD_9", "CHANNEL_015");
  generic_signals_t gs = cdb_get_generic_signal_ref_by_id(attachment.attached_generic_signal_id);

  cout << gs.generic_signal_name << endl;
  destroy_generic_signals(&gs);

  generic_signals_t t;
  cout << t.isset << endl;

  generic_signals_t t2;
  cout << t2.isset << endl;

  generic_signals_t t3;
  cout << t3.isset << endl;

  // Finish
  int r = 0;
  if (PyErr_Occurred() != NULL) {
    r = 1;
    PyErr_Print(); /* This exits with the right code if SystemExit. */
    if (Py_FlushLine()) PyErr_Clear();
  }
  Py_Finalize();
  return r;
}
