from pyCDB import DAQClient
import json
from os import path

class DatabaseCredentials(object):
    """
    Class for containing and handling credentials of cdb databases for users
    """

    def __init__(self, kwargs):

        self._credentials = {}
        for key, item in kwargs.items():
            self._credentials[key] = item

    def __getitem__(self, key):
        try:
            return self._credentials[key]
        except KeyError:
            raise KeyError("DatabaseCredentials has no key {0}".format(key))

    def __setitem__(self, key, value):
        self._credentials[key] = value

    def __getattr__(self, attr):
        try:
            return self._credentials[attr]
        except Exception:
            raise AttributeError("DatabaseCredentials has no attribute {0}".format(attr))

    @classmethod
    def from_json(cls, path):
        """
        Reads credential from a json file.
        :param path: Path to the json file containing connection credentials
        :return: dectionary of DatabaseCredentials
        """

        with open(path, "r") as fl:
            content = json.load(fl)

        credentials = {}
        for key, item in content.items():
            credentials[key] = cls.from_dict(item)

        return credentials

    @classmethod
    def from_dict(cls, data):

        return cls(data)

    def as_dict(self):
        return self._credentials


class CDBClientMeta(object):
    """
    Meta client for cdb clients for multiple databases. It acts as an umbrella. DatabaseCredentials have to be
    added in order for the instance to know how to access the database. User databases might be accessed as
    parameters, e.g. CDBClientMeta.cdb or CDBClientMeta can act as a dictionary: CDBClientMeta["cdb"].
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self._clients = {}
        self._credentials = {}

        self._json_default_databases = path.dirname(DAQClient.__file__) + "/default_credentials.json"
        self._add_default_databases()

    def _add_default_databases(self):
        """
        Loads default database credentials from self._json_default_databases path which has to be json file.
        :return:
        """
        credentials = DatabaseCredentials.from_json(self._json_default_databases)

        for key, item in credentials.items():
            self.add_database(key, item)

    def add_database(self, user, credentials: DatabaseCredentials):
        """
        Adds a user database to the list of known databases.
        :param user: User name under which the database will be known and searched for.
        :param credentials: Credentials with connection details to the database.
        :return:
        """
        self._credentials[user] = credentials

    def __getattr__(self, user):
        """
        Allows access to the specific database to be done as an attribute of the instance. Creates the database
        client if it is non-existent. Raises an error if user database credentials are unknown.
        :param user: User name
        :return:
        """

        try:
            return self._clients[user]
        except KeyError:  # create the client if it does not exist
            try:
                self._clients[user] = DAQClient.CDBDAQClient(**self._credentials[user].as_dict())
            except KeyError:  # if user credentials not known call super class attrs
                raise AttributeError("User not known.")

        return self._clients[user]

    def __getitem__(self, user):
        """
        Allows access to the specific database to be done treating the instance as a dictionary. Creates the database
        client if it is non-existent. Raises an error if user database credentials are unknown.
        :param user: User name
        :return:
        """

        try:
            return self._clients[user]
        except KeyError:  # create the client if it does not exist
            try:
                self._clients[user] = DAQClient.CDBDAQClient(**self._credentials[user].as_dict())
            except KeyError:  # user not known
                raise KeyError("User not known.")

        return self._clients[user]
