class ChannelAlreadyAttachedError(Exception):
    def __init__(self, computer_id, board_id, channel_id):
        self.computer_id = computer_id
        self.board_id = board_id
        self.channel_id = channel_id

    def __str__(self):
        return "Channel (computer %s, board %i, channel %i) already attached."\
            % (self.computer_id, self.board_id, self.channel_id)

