import networkx as nx
from pyCDB.client import CDBClient
import time
from json.encoder import JSONEncoder
from json.decoder import JSONDecoder
try:
    from ipyparallel import Client
except ImportError:
    from IPython.parallel import Client
import sys
import traceback

import pyCDB.libpostproc as lib

json_encoder = JSONEncoder()
json_decoder = JSONDecoder()

# TODO: get rid of this cdb
# cdb = CDBClient()


def parse_log_sig_revs(signals_string):
    '''Parse signals and revisions from a log string

    Format: generic_signal_id:revision, generic_signal_id:revision, ...
    '''
    try:
        res = json_decoder.decode(signals_string)
    except ValueError:
        res = []
    return res


def compose_log_sig_revs_str(input_sig_refs):
    '''Compose input_signal_revisions to log CDB rule run
    '''
    log_dict = [(ref['generic_signal_id'], ref['revision']) for ref in input_sig_refs]
    return json_encoder.encode(log_dict)


def strip_signal_ref(signal_ref):
    '''Leaves only necessary keys in signal reference
    '''
    return {'generic_signal_id': signal_ref['generic_signal_id'],
            'record_number': signal_ref['record_number'],
            'revision': signal_ref['revision']}


class CDBRule(object):
    """Postprocessing rule definition for CDB"""
    def __init__(self, name, rule_id, inputs, outputs, func, func_kwargs={}, max_runs=2, max_time=60):
        '''CDB rule constructor

        :param name: rule name
        :param rule_id: rule numeric id
        :param inputs: list/set of input generic signal id's
        :param outputs: list/set of output generic signal id's
        :param func: function to call (full name incl. modules, e.g. scipy.signal.resample)
        :param max_runs: maximum run attempts
        :param max_time: maximum run time in minutes, None = infinite

        func will be called by __call__ as func(input_sig_refs, outputs, **kwargs).
        func must read and write the signals from/to CDB and return a lits (tuple)
        of output signal references.
        '''
        super(CDBRule, self).__init__()
        self.name = name
        self.rule_id = rule_id
        self.rule = {}
        self.rule['inputs'] = set(inputs)
        self.rule['outputs'] = set(outputs)
        # using celery.task decorator seems not to work :(
        self.rule['func'] = func
        self.rule['func_kwargs'] = func_kwargs.copy()
        self.max_runs = max_runs
        self.max_time = max_time * 60
        self.runs = 0
        self.inputs_avail = []

    def reset(self):
        '''Reset inputs
        '''
        self.inputs_avail = []
        self.runs = 0

    def put_inputs(self, inputs):
        '''Put (register) an input signal reference

        :param inputs: [dict(generic_signal_id:, record_number:, revision: ), ...]
        '''
        for inp in inputs:
            if inp['generic_signal_id'] in self.rule['inputs']:
                self.inputs_avail.append(dict(inp))

    def check_inputs(self, inputs=None):
        '''Check rule inputs, return True if the rule can be executed
        '''
        # simply check whether all input signals are available
        if not inputs:
            inputs = self.inputs_avail
        in_ids = set(i['generic_signal_id'] for i in inputs)
        res = not bool(self.rule['inputs'] - in_ids)
        print('check_inputs: %s / %s --> %s' % (in_ids, self.rule['inputs'], res))
        return res

    def check_last_log(self, last_log):
        '''Check last log, return False if the rule has been run already with current inputs
        '''
        res = False
        for gs_id, rev in parse_log_sig_revs(last_log['input_signal_revisions']):
            rev_in = [inp['revision'] for inp in self.inputs_avail if inp['generic_signal_id'] == gs_id]
            if not rev_in:
                res = True
                break
            else:
                rev_in = rev_in[0]
            if rev_in != rev:
                # check only relevant fields
                ref_in = cdb.get_signal_references(generic_signal_id=gs_id, revision=rev_in,
                                                   record_number=last_log['record_number'])[0]
                ref_last = cdb.get_signal_references(generic_signal_id=gs_id, revision=rev,
                                                     record_number=last_log['record_number'])[0]
                for f in ('data_file_id', 'data_file_key',
                          'time0', 'coefficient', 'offset', 'coefficient_V2unit',
                          'time_axis_id', 'time_axis_revision', 'axis1_revision',
                          'axis2_revision', 'axis3_revision', 'axis4_revision',
                          'axis5_revision', 'axis6_revision'):
                    if ref_in[f] != ref_last:
                        res = True
                        break
                if ref_in['deleted']:
                    # do not run if a signal has been deleted
                    res = False
                    break
                if res:
                    break

        return res

    def get_func_call(self, input_sig_refs=None):
        '''Get the function and its args and kwargs
        '''
        if not input_sig_refs:
            input_sig_refs = self.inputs_avail

        return self.rule['func'], (input_sig_refs, self.rule['outputs']), {}


class DummyResult(object):
    """Dummy async result"""
    def __init__(self, result):
        self.result = result

    def get(self):
        return self.result

    def ready(self):
        return True

    def successful(self):
        return True


# TODO: make it a singleton
class CDBPostProc(object):
    """docstring for CDBPostProc"""
    def __init__(self, cluster_profile='postproc', rules=(), cdb=None):

        self._cdb = cdb
        if self._cdb is None:
            self._cdb = CDBClient()

        # read the rules table
        self.rules = []
        if rules:
            self.add_rules(rules)
        else:
            self.read_rules()
        # init CDBRule instances
        # construct and check the graph
        self.G = None
        self.G_full = None
        self.start_nodes = []
        self.construct_graph()
        self.check_graph()
        # connect to worker nodes
        self._cluster_profile = cluster_profile
        if cluster_profile is not None:
            self.init_cluster(cluster_profile)
        self.run_id = None

        # self._ipy_rc = IPython.parallel.Client(profile='cdb')

    def add_rule(self, name, rule_id, rule):
        '''Add a rule

        :param rule: rule dictionary with inputs, outputs, func fields
        '''
        assert isinstance(rule, dict), 'rule must be a dictionary object'
        assert 'inputs' in rule, 'rule must have inputs'
        assert 'outputs' in rule, 'rule must have outputs'
        assert 'func' in rule, 'rule must have func'
        kwargs = {}
        if 'max_runs' in rule:
            kwargs['max_runs'] = rule['max_runs']
        if 'max_time' in rule:
            kwargs['max_time'] = rule['max_time']
        self.rules.append(CDBRule(name, rule_id, rule['inputs'], rule['outputs'], rule['func'], **kwargs))

    def init_cluster(self, profile):
        '''Get a connection (view) to an IPython cluster
        '''

        self._ipy_rc = Client(profile=profile)
        self._ipy_dv = self._ipy_rc[:]
        self._ipy_lv = self._ipy_rc.load_balanced_view()
        # synchronize imports
        # this does not work like this (why???)
        # -> use profile
        # with self._ipy_dv.sync_imports(local=True):
        #     from pyCDB.pyCDBBase import pyCDBBase
        #     from pyCDB.client import CDBClient as CDBClient
        #     from json.encoder import JSONEncoder
        #     from json.decoder import JSONDecoder
        #     import IPython.parallel

    def add_rules(self, rules):
        '''Add rules from a dictionary

        :param rules: dictionary containing rules dicts, rules names = keys

        This method will probably be removed.
        '''
        assert isinstance(rules, dict), 'rules must be a dictionary'
        for rule_id, (name, rule) in enumerate(rules.items()):
            self.add_rule(name, rule)

    def read_rules(self):
        '''Read rules from db
        '''
        sql_query = 'SELECT * FROM `postproc_rules` WHERE `active` = 1;'
        cursor = self._cdb.db.cursor()
        cursor.execute(sql_query)
        rows = map(lambda row: self._cdb.row_as_dict(row, cursor.description), cursor.fetchall())
        for row in rows:
            if row['func']:
                try:
                    try:
                        func = eval('lib.' + row['func'])
                    except Exception:
                        split_func = row['func'].split('.')
                        if len(split_func) > 1:
                            _temp = __import__('.'.join(split_func[:-1]), globals(), locals(), split_func[-1])
                            func = getattr(_temp, split_func[-1])
                        else:
                            func = eval(row['func'])
                except Exception:
                   raise Exception('Cannot find postprocessing function: {0}'.format(row['func']))
            else:
                func = lib.base.donothing
            rule = {'inputs': [], 'outputs': [], 'func': func}
            sql_query = 'SELECT * FROM `rule_inputs` WHERE `rule_id` = %i;' % row['rule_id']
            cursor.execute(sql_query)
            for inp in cursor.fetchall():
                inp = self._cdb.row_as_dict(inp, cursor.description)
                rule['inputs'].append(inp['generic_signal_id'])
            sql_query = 'SELECT * FROM `rule_outputs` WHERE `rule_id` = %i;' % row['rule_id']
            cursor.execute(sql_query)
            for outp in cursor.fetchall():
                outp = self._cdb.row_as_dict(outp, cursor.description)
                rule['outputs'].append(outp['generic_signal_id'])
            if rule['outputs'] and rule['inputs']:
                self.add_rule(row['rule_name'], row['rule_id'], rule)
            else:
                self._cdb._logger.warning('rule %s ignored: no inputs or outputs found' % row['rule_name'])
        cursor.close()

    def rule_last_log(self, cdb_rule, record_number=None, success=None):
        '''Read last log entry from SQL

        :param record_number: record number, use None for any record
        :param success: success flag, use None for any
        '''
        # look up last log entry and return the corresponding row
        sql_conds = 'rule_id = %i' % cdb_rule.rule_id
        if record_number:
            sql_conds += ' AND record_number=%i' % record_number
        if success is not None:
            sql_conds += ' AND success=%i' % success
        sql_query = 'SELECT * FROM `postproc_log` WHERE %s' % sql_conds
        sql_query += ' AND timestamp=(SELECT MAX(timestamp) FROM `postproc_log` WHERE %s)' % sql_conds
        cursor = self._cdb.db.cursor()
        cursor.execute(sql_query)
        row = cursor.fetchone()
        if not row:
            res = None
        else:
            res = self._cdb.row_as_dict(row, cursor.description)
        cursor.close()
        return res

    def rule_write_log(self, cdb_rule, record_number, success=True, outputs=(), log_text=''):
        '''Write log for a CDB rule run
        '''
        input_signal_revisions = compose_log_sig_revs_str(cdb_rule.inputs_avail)
        output_signal_revisions = compose_log_sig_revs_str(outputs)
        fields = {'rule_id': cdb_rule.rule_id,
                  'record_number': record_number,
                  'run_id': self.run_id,
                  'timestamp': '\x00NOW()',
                  'input_signal_revisions': input_signal_revisions,
                  'success': success,
                  'output_signal_revisions': output_signal_revisions,
                  'log': log_text}
        self._cdb.insert('postproc_log', fields)

    def construct_graph(self):
        G = nx.DiGraph()
        G_full = nx.DiGraph()

        start_sigs = {}
        rules = self.rules

        for r in rules:
            # G.add_node(r.name, r.rule)
            # G_full.add_node(r.name, r.rule)
            G.add_node(r.name, {'rule': r})
            G_full.add_node(r.name, {'rule': r})
            for o in r.rule['outputs']:
                G_full.add_node(o, {'rule': CDBRule(o, None, (o,), (o,), lib.base.check_signal)})
                G_full.add_edge(r.name, o, signals=set((o,)))
            for i in r.rule['inputs']:
                start_sigs[i] = {'rule': CDBRule(i, None, (i,), (i,), lib.base.check_signal)}
                G_full.add_node(i, start_sigs[i])
                G_full.add_edge(i, r.name, signals=set((i,)))
                # start_sigs[i] = {'inputs': (), 'outputs': (i,)}
                for s in rules:
                    # TODO: add all signals as nodes
                    if i in s.rule['outputs'] and i in start_sigs:  # second condition for self-dependent rules
                        # this is not a start signal
                        del start_sigs[i]

        # use starting signals as nodes
        for s in start_sigs:
            start_sigs[s]['rule'].rule['inputs'] = set()
            G.add_node(s, start_sigs[s])

        # edges construction
        for n in G:
            for o in G.node[n]['rule'].rule['outputs']:
                for m in G:
                    if o in G.node[m]['rule'].rule['inputs']:
                        if (n, m) in G.edges():
                            G.edge[n][m]['signals'].add(o)
                        else:
                            G.add_edge(n, m, signals=set((o,)))

        self.G = G
        self.G_full = G_full
        self.start_nodes = start_sigs.keys()

    def check_graph(self):
        '''Check graph for cysles and multiple creation of a signal
        '''

        cycles = tuple(nx.simple_cycles(self.G))
        if cycles or not nx.is_directed_acyclic_graph(self.G_full):
            raise Exception('Cycles exist: %s' % cycles)

        sig_nodes = [n for n in self.G_full.nodes() if not isinstance(n, str)]
        bad_sig_nodes = filter(lambda n: len(self.G_full.predecessors(n)) > 1, sig_nodes)
        if bad_sig_nodes:
            raise Exception('Signal(s) %s has more than 1 predecessor---'
                            'signal would be overwritten during the postprocessing' % bad_sig_nodes)

    def plot(self):
        '''Plot the graph representation
        '''
        from matplotlib.pyplot import figure

        for G in (self.G_full, self.G):
            figure()
            try:
                pos = nx.graphviz_layout(G, prog='dot')
            except ImportError:
                pos = nx.shell_layout(G)
            node_color = ['g' if isinstance(n, str) else 'y' for n in G.nodes()]
            nx.draw(G, pos=pos, node_color=node_color)
        l = dict((((e, h), str(list(G.edge[e][h]['signals']))) for e, h in G.edges()))
        nx.draw_networkx_edge_labels(G, pos, edge_labels=l)

    def _write_run_status(self, record_number, status, return_inserted_id=False):
        """Write a run log entry into db

        :param record_number: record number
        :param status: 'QUEUE', 'START' or 'STOP'
        """
        fields = {'record_number': record_number, 'status': status, 'timestamp': '\0NOW()'}
        if return_inserted_id:
            run_id = self._cdb.insert('postproc_run_log', fields, return_inserted_id=True)
        else:
            if self.run_id is not None:
                fields['run_id'] = self.run_id
                self._cdb.insert('postproc_run_log', fields, return_inserted_id=False)
            run_id = None
        return run_id

    def _get_run_status(self, record_number):
        """Get current postprocessing status and its timestamp

        :param record_number: record number
        """
        res = self._cdb.query('SELECT `status`, `timestamp` FROM `postproc_run_log` WHERE '
                              '`record_number`={0} ORDER BY `timestamp` ASC;'.format(record_number))
        if res:
            status = res[-1]['status']
            timestamp = res[-1]['timestamp']
        else:
            status = None
            timestamp = None
        return status, timestamp

    def __call__(self, record_number, queue=True, force_run=False, reinit=False, timeout=30):
        """Start the postprocessing

        :param record_number: record number
        :param queue: wait if the postprocessing for this record in currently running
        :param force_run: ignore other runs
        :param timeout: maximum run time in minutes
        """

        lib.base.set_timeout(timeout)

        try:
            if reinit:
                print('init_cluster')
                self.init_cluster(self._cluster_profile)
                self._cdb = CDBClient()
            # check if postprocessing is running for the same record
            status, timestamp = self._get_run_status(record_number)
            self.run_id = None
            if not force_run or status is not None:
                if status == 'QUEUE':
                    # another postprocessing is waiting -> nothing to do
                    return
                if status == 'START':
                    # another postprocessing is running -> let's wait
                    if not queue:
                        return
                    self.run_id = self._write_run_status(record_number, 'QUEUE', return_inserted_id=True)
                    self._cdb._logger.info('queueing postprocessing for {0}'.format(record_number))
                    while status == 'START' or status == 'QUEUE':
                        time.sleep(30)
                        status, timestamp = self._get_run_status(record_number)
            # green light to start
            try:
                if self.run_id is not None:
                    # this means queued before
                    self._write_run_status(record_number, 'START', return_inserted_id=False)
                else:
                    self.run_id = self._write_run_status(record_number, 'START', return_inserted_id=True)
            except Exception:
                self._cdb._logger.error('Error writing the run status - stopping')
                try:
                    # try to signal the end of the run anyway
                    self._write_run_status(record_number, 'STOP')
                except Exception:
                    pass
                return
            # start the execution
            for cdb_rule in self.rules:
                cdb_rule.reset()
                # reload all modules before post processing
                func, args, kwargs = cdb_rule.get_func_call()
                reload(sys.modules[func.__module__])
                self._ipy_dv.execute('import {}'.format(func.__module__), block=True)
                with self._ipy_dv.sync_imports():
                   import os
                self._ipy_dv.execute('reload(sys.modules["{0}"])'.format(func.__module__), block=True)
            # traverse the full graph
            nodes_running = {}  # dict node = {task: rule, 'successors': list of successors}
            nodes_to_run = []  # dict node = [list of inputs]
            for node_name in self.start_nodes:
                cdb_rule = self.G_full.node[node_name]['rule']
                # start_nodes are supposed to be only check signals ==> len(cdb_rule.rule['inputs']) = 1
                # TODO this gets complicated with variants !!!
                signal_refs = tuple(({'record_number': record_number,
                                      'revision': -1, 'generic_signal_id': o} for o in cdb_rule.rule['outputs']))
                func, args, kwargs = cdb_rule.get_func_call(signal_refs)
                print('func, args, kwargs: %s' % str((func, args, kwargs)))
                task_ref = self._ipy_lv.apply_async(func, *args, **kwargs)
                task = {'task_ref': task_ref,
                        'successors': self.G_full.successors(node_name)}
                nodes_running[node_name] = task
            print('tasks running: %s' % nodes_running.keys())
            # loop while something's running
            while nodes_running:
                # TODO: signals would be better
                for node_name, task in nodes_running.items():
                    if task['task_ref'].ready():
                        # task has finished
                        print('task %s has finished' % node_name)
                        del nodes_running[node_name]
                        try:
                            task_res, task_log = task['task_ref'].get()
                            task_res = tuple(map(strip_signal_ref, task_res))
                            success = task['task_ref'].successful() and bool(task_res)
                        except Exception as e:
                            exc_type, exc_value, exc_traceback = sys.exc_info()
                            lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
                            msg = ''.join('!! ' + line for line in lines)
                            self._cdb._logger.error('task {0} has failed!:\n{1}'.format(node_name, msg))
                            success = False
                            task_res = ()
                            task_log = str(msg)
                            task['successors'] = ()  # do not run dependent tasks
                        print('res = %s' % str(task_res))
                        # task_log is None for check_signal
                        if task_log is not None and task['cdb_rule'].rule_id is not None:
                            self.rule_write_log(task['cdb_rule'], record_number, success=success,
                                                outputs=task_res, log_text=task_log)
                        for succ_node_name in task['successors']:
                            cdb_rule = self.G_full.node[succ_node_name]['rule']
                            # append input(s) the task
                            cdb_rule.put_inputs(task_res)
                            if succ_node_name not in nodes_to_run:
                                # a new task
                                nodes_to_run.append(succ_node_name)
                                print('task %s considered for execution' % succ_node_name)
                            if cdb_rule.check_inputs():
                                # all inputs available
                                # get the log (for db stored rules)
                                if cdb_rule.rule_id is not None:
                                    last_log = self.rule_last_log(cdb_rule, record_number, success=True)
                                else:
                                    last_log = None
                                if last_log is None or cdb_rule.check_last_log(last_log):
                                    # run only if necessary
                                    func, args, kwargs = cdb_rule.get_func_call()
                                    print('func, args, kwargs: %s' % str((func, args, kwargs)))
                                    task_ref = self._ipy_lv.apply_async(func, *args, **kwargs)
                                else:
                                    # return a dummy result
                                    result = [{'generic_signal_id': l[0], 'revision': l[1],
                                               'record_number': last_log['record_number']}
                                              for l in parse_log_sig_revs(last_log['output_signal_revisions'])]
                                    print('DummyResult = %s' % result)
                                    task_ref = DummyResult((tuple(result), 'dummy run, using previous results'))
                                # we're ready to run
                                new_task = {'task_ref': task_ref,
                                            'successors': self.G_full.successors(succ_node_name),
                                            'cdb_rule': cdb_rule}
                                nodes_running[succ_node_name] = new_task
                                nodes_to_run.remove(succ_node_name)
                                print('executed task %s' % succ_node_name)
            # check if everything has been run
            if nodes_to_run:
                print('\nnodes_to_run left: %s' % nodes_to_run)
        except Exception:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
            msg = ''.join('!! ' + line for line in lines)
            self._cdb._logger.error('error during postprocessing:\n{0}'.format(msg))
            raise
        finally:
            self._write_run_status(record_number, 'STOP')
        # execute starting signal nodes with -1 revision (or get the revisions now if available)
        # execute (schedule) remaining nodes with inputs derived from the graph


def main():
    from multiprocessing import Process
    # from pyCDB.postproc import CDBPostProc

    # TESTING
    # rules = {'r1': {'inputs': (0, 1, 2), 'outputs': (10, 20), 'func': donothing},
    #          'r2': {'inputs': (1, 3), 'outputs': (30,), 'func': donothing},
    #          'r3': {'inputs': (2, 3), 'outputs': (40,), 'func': donothing},
    #          'r4': {'inputs': (20, 40), 'outputs': (52,), 'func': donothing},
    #          # 'r5': {'inputs': (51, ), 'outputs': (52, 53), 'func': donothing},
    #          'r5': {'inputs': (51, ), 'outputs': (53, ), 'func': donothing},
    #          'r6': {'inputs': (52, 53), 'outputs': (54,), 'func': donothing},
    #          'r7': {'inputs': (54, ), 'outputs': (55,), 'func': donothing},
    #          }

    rules = {'r1': {'inputs': (1, 2), 'outputs': (7, ), 'func': lib.base.donothing}}

    # pp = CDBPostProc(rules=rules)
    pp = CDBPostProc()
    pp.init_cluster('postproc')

    # pp = CDBPostProc(cluster_profile='postproc')
    pp = CDBPostProc(cluster_profile=None)
    p = Process(target=pp, args=(10000000,), kwargs={'reinit': True})

    pp._cluster_profile = 'postproc'
    p.start()


    pp.init_cluster('postproc')
    pp(10000000)


    # close('all')
    # pp.plot()
    # show()

    # pp(10000000)
    # pp(10000000)


if __name__ == '__main__':
    main()
