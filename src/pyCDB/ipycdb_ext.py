"""IPython extension for CDB.

Provides the following fuctionality:
* dictionaries are displayed as tables
* CDBSignal displayed as table + links to WebCDB

Variables provided:
cdb - an automatically created CDBDAQClient

Magic functions:
%signal - calls cdb.get_signal with the rest of line as str_id and stores the result to variable signal
%plot - as %signal + displays a plot of the signal

How to use it:
    import pyCDB
    %load_ext pyCDB.ipycdb_ext
"""
from .client import CDBSignal, OrderedDict
from .DAQClient import CDBDAQClient
from IPython.core.display import HTML


def load_ipython_extension(ipython):
    CDBSignal._repr_html_ = signal_html_repr
    # CDBSignal.download = lambda s: download_hdf5(s.ref)

    OrderedDict._repr_html_ = ordered_dict_html_repr
    cdb = CDBDAQClient()
    ipython.push({"cdb": cdb})

    def signal_magic_function(line):
        signal = cdb.get_signal(str_id=line)
        ipython.push("signal", signal)
        return signal

    ipython.register_magic_function(signal_magic_function, magic_name="signal")
    ipython.register_magic_function(
        lambda line: cdb.get_signal(str_id=line).plot(),
        magic_name="plot")
    # ipython.register_magic_function(lambda line: download_hdf5(
    #     cdb.get_signal_references(str_id=line)[0]), magic_name="download")
    print(
        "IPython extension for PyCDB loaded. Magic functions: %signal, %plot")


def unload_ipython_extension(ipython):
    del CDBSignal._repr_html_
    del OrderedDict._repr_html_


def ordered_dict_html_repr(d):
    s = "<table>"
    for key, value in d.items():
        s += "<tr><td><b>%s</b></td><td>%s</td></tr>" % (key, value)
    s += "</table>"
    return s


def download_hdf5(signal_ref):
    download_href = "http://www.tok.ipp.cas.cz/webcdb/data_signals/%d/%d/data?revision=1&format=hdf5" % (
        signal_ref["generic_signal_id"], signal_ref["record_number"])
    return HTML("<a href=\"%s\">Click here</a>" % href)


def signal_html_repr(signal):
    d = signal._repr_dict()
    s = "<table>"
    for key, value in d.items():
        s += "<tr><td><b>%s</b></td><td>%s</td></tr>" % (key, value)
    s += "<tr><td><b>WebCDB link</b></td><td><a href=\"%s\">Click here</a></td></tr>" % signal.url

    download_url = "http://www.tok.ipp.cas.cz/webcdb/data_signals/%d/%d/data?revision=1&format=hdf5" % (
        signal.ref["generic_signal_id"], signal.ref["record_number"])
    s += "<tr><td><b>Download HDF5</b></td><td><a href=\"%s\">Click here</a></td></tr>" % download_url

    # s += "<tr><td colspan=2><img src='http://www/webcdb/data_signals/3272/6364/plot?revision=1'/></td></tr>" % (signal)
    s += "</table>"
    return s
