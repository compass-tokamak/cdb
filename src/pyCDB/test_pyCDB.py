# CDB test using the pyCDB.client module

import sys
import os
import os.path
from pylab import *
import h5py
from client import CDBClient
import copy

if __name__ == "__main__":
    # connectct to the database and test
    cdb = CDBClient(log_level=10)
    if cdb is None or not cdb.check_connection():
        print("error: could not connect to MySQL server")
        sys.exit(-1)
    print("Last record number: %i" % cdb.last_record_number())
    # create a record
    record_number = cdb.create_record(record_type='VOID', record_time=None, description='testing')
    print("Created new record number: %i" % record_number)

    # test the databe
    # use pre-defined source and signal name for testing
    data_source_name = 'Interferometry'
    generic_signal_name = 'electron density'
    generic_lin_signal_name = 'test linear signal'
    # get the full data source reference (all columns)
    data_source_refs = cdb.get_data_source_references(data_source_name=data_source_name)
    print('\nData source "%s" reference(s):' % data_source_name)
    print(str(data_source_refs))
    # get the full generic signal reference (all columns)
    generic_signal_refs = cdb.get_generic_signal_references(generic_signal_name=generic_signal_name)
    lin_sig_gen_ref = cdb.get_generic_signal_references(generic_signal_name=generic_lin_signal_name)[0]
    print('Generic signal "%s" reference(s):' % generic_signal_name)
    print(str(generic_signal_refs))
    # cdb.get_data_source_references(record_number=record_number)
    # get_data_file_reference

    # Now create a file with a data collection
    collection = 'density'
    file_ref = cdb.new_data_file(collection, data_source_id=generic_signal_refs[0]['data_source_id'], record_number=record_number,
                                 file_format="HDF5")
    print('New data file ref:\n%s' % str(file_ref))

    # construct a test signal
    test_t = linspace(-0.2, 1, 200)
    test_s = exp(-((test_t - 0.4) * 3) ** 8) * (rand(*test_t.shape) * 0.2 + 0.9)
    # write to file
    if not os.path.isdir(os.path.dirname(file_ref['full_path'])):
        os.mkdir(os.path.dirname(file_ref['full_path']))
    fh5 = h5py.File(file_ref['full_path'], 'w')
    print('\nPath to HDF5 file: ', file_ref['full_path'])
    grp_name = 'raw data'
    data_file_key = grp_name + '/' + generic_signal_name
    f_grp = fh5.create_group(grp_name)
    f_grp.create_dataset(generic_signal_name, data=test_s)
    fh5.close()
    # tell the database that the file can be used
    cdb.set_file_ready(file_ref['data_file_id'])
    # store signals to the database
    cdb.store_signal(generic_signal_refs[0]['generic_signal_id'],
                     record_number=record_number, data_file_id=file_ref['data_file_id'], data_file_key=data_file_key,
                     computer_id=1, board_id=1, channel_id=2)
    cdb.store_signal(lin_sig_gen_ref['generic_signal_id'],
                     record_number=record_number, offset=0.4, coefficient=0.1, time0=0)
    cdb.store_signal(generic_signal_refs[0]['time_axis_id'],
                     record_number=record_number, offset=test_t[0] - 0.1, coefficient=test_t[1] - test_t[0])
    # create new revision with offset and coeff correction
    cdb.store_signal(generic_signal_refs[0]['generic_signal_id'],
                     record_number=record_number, data_file_id=file_ref['data_file_id'], data_file_key=data_file_key,
                     offset=0.5, coefficient=1.3, time0=-0.2, computer_id=1, board_id=1, channel_id=2,
                     note='changed time0, offset, coefficient')

    # retreive back the data from CDB
    print('\nRetrieving data from CDB')
    # first get references
    signal_refs = cdb.get_signal_references(record_number=record_number, generic_signal_id=generic_signal_refs[0]['generic_signal_id'])
    if len(signal_refs) == 0:
        print("error: signal not found in the database")
    #    return
    signal_ref = signal_refs[-1]
    lin_sig_ref = cdb.get_signal_references(record_number=record_number, generic_signal_id=lin_sig_gen_ref['generic_signal_id'])[0]
    time_ax_refs = cdb.get_signal_references(record_number=record_number, generic_signal_id=generic_signal_refs[0]['time_axis_id'])
    time_ax_ref = time_ax_refs[-1]
    generic_time_ax_ref = cdb.get_generic_signal_references(generic_signal_id=generic_signal_refs[0]['time_axis_id'])[0]
    print("new signal references:\n%s" % str(signal_refs))
    # now get the data using (lower-level) get_signal_data
    # the latest revision
    test_s_cdb = cdb.get_signal_data(signal_ref, generic_signal_refs[0])
    # the 1st revision
    test_s_cdb0 = cdb.get_signal_data(signal_refs[0], generic_signal_refs[0])
    # time axis
    # user must supply offset (x0) in this case
    test_t_cdb = cdb.get_signal_data(time_ax_ref, generic_time_ax_ref, n_samples=test_s_cdb.size, x0=signal_ref['time0'])
    # now get the latest revision using more friendly get_signal
    sig = cdb.get_signal(signal_ref=signal_ref)
    lin_sig = cdb.get_signal(signal_ref=lin_sig_ref, time_limit=0.8)
    print("\nSignal info: %s " % sig.info())
    print("Time axis info: %s " % sig.time_axis.info())

    figure()
    plot(test_t, test_s, 'k', label='original data')
    plot(test_t_cdb, test_s_cdb, 'b--', label='get_signal_data rev 2')
    plot(test_t_cdb, test_s_cdb0, 'r--', label='get_signal_data rev 1')
    plot(sig.time_axis.data, sig.data, 'c:', label='get_signal rev 2')
    plot(lin_sig.time_axis.data, lin_sig.data, 'g', label='linear signal')
    xlabel('time')
    ylabel('signal')
    title('Record %i, %s' % (record_number, generic_signal_name))
    legend(loc='best')

    # test of the CDBSignal.plot method
    sig.plot()
    # manual hack to construct a 2D signal
    x = linspace(0, 1, 100)
    y = linspace(-5, 5, 100)
    X, Y = meshgrid(x, y)
    d = sin(X * Y)
    sig.data = d
    sig.time_axis.data = x
    sig.axes = [copy.deepcopy(sig.time_axis)]
    sig.axes[0].data = y
    sig.axes[0].name = 'y axis'
    sig.axes[0].units = 'a.u.'
    sig.plot()

    show()


    #close()
    print("The end")
    #~ return 0
    #~
