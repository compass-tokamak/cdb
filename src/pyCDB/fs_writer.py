from os import path

class GenericSignalNotFoundException(Exception):
    def __init__(self, message):
        Exception.__init__(self, message)

class RecordNotFoundException(Exception):
    def __init__(self, message):
        Exception.__init__(self, message)

class ChannelAutoCreationException(Exception):
    def __init__(self, message):
        Exception.__init__(self, message)

class FSWriter(object):
    ''' Class used by FireSignal DB controller to write data to CDB.

    Warning: Don't use (or change) this class outside the intended use.
      It is closely fitted to the optimization needs of PGSQLHDF5DatabaseController->writeDataCDB

    Automatically creates channels and generic signals for MARTe signals if
    they are not already stored in the database.

    Basic usage:
    1) Initialization of all input in constructor.
    2) `read_signal_info`: Reading of all necessary information
        (incl. time axes and whether the signal is already written).
    3) `store signal info`: Storing the signal in the DB. Only call if necessary (`signal_exists`)

    Warning:
    The script does not take into account revisions or variants. It is assumed that all
    data coming from FireSignal are completely new (including time axes).

    (C) Jan Pipek 2012-13

    '''
    MARTE_NODE_NAME = "MARTE_NODE"
    MARTE_COMPUTER_NAME = "marte"
    MARTE_NODE_DATA_SOURCE_NAME = "MARTE"

    def __init__(self, cdb_client, **kwargs):
        """ Constructor. For each channel, use new instance.

        :param cdb_client: instance of CDBDAQClient
        """
        self.client = cdb_client

        self.channel_created = False
        self.auto_create_attempted = False
        self.node_id = kwargs["node_id"]
        self.hardware_id = kwargs["hardware_id"]
        self.parameter_id = kwargs["parameter_id"]
        self.record_number = kwargs["record_number"]
        self.time0 = kwargs["time0"]
        self.sample_length = kwargs["sample_length"]
        self.collection_name = "%s.%s.%s" % (self.node_id, self.hardware_id, self.parameter_id)

    def _auto_create_marte_channel(self):
        ''' Attempt to create a MARTE channel.

        The board has to exist otherwise nothing is created.
        Time axis for the channel is then taken from the first channel of the same board
        (that has time axis).
        '''

        self.auto_create_attempted = True
        data_source_id = self.client.get_data_source_id(self.MARTE_NODE_DATA_SOURCE_NAME)

        # TODO: description = node_id is an inapropriate convention -> do something better
        # (change the get_computer_id function itself to accept and treat properly node_id)

        computer_id = self.client.query(
                "SELECT computer_id FROM `DAQ_channels` WHERE nodeuniqueid = '%s' LIMIT 1" % self.node_id,
                error_if_empty=True
            )[0]["computer_id"]
        board_id = self.client.get_board_id(self.node_id, self.hardware_id)

        if board_id == None:
            raise ChannelAutoCreationException("Board not existing.")

        attachments = self.client.get_attachment_table(computer_id=computer_id, board_id=board_id)
        if not attachments:
            raise ChannelAutoCreationException("No attachment information for computer & board %d, %d" % (computer_id, board_id))

        for attachment in attachments:
            generic_signal_id = attachment["attached_generic_signal_id"]
            time_axis_id = self.client.get_generic_signal_references(generic_signal_id = generic_signal_id)[0]["time_axis_id"]
            if time_axis_id:
                break
        if not time_axis_id:
            raise ChannelAutoCreationException("Time axis not found.")

        self.client.create_DAQ_channels(self.node_id, self.hardware_id, self.parameter_id,
            data_source_id=data_source_id, time_axis_id=time_axis_id)
        self.channel_created = True

    def _on_read_error(self):
        if not self.client.record_exists(record_number = self.record_number):
            raise RecordNotFoundException("Record not found: %d" % self.record_number)
        if not self.client.get_FS_signal_reference(self.node_id, self.hardware_id, self.parameter_id):            
            if (self.node_id == self.MARTE_NODE_NAME) and not self.auto_create_attempted:
                self._auto_create_marte_channel()
                return
            else:
                raise GenericSignalNotFoundException("Generic signal not found for %s/%s/%s." % (self.node_id, self.hardware_id, self.parameter_id))
        raise Exception("Unknown error")

    def _read_row(self, sql):
        cursor = self.client.db.cursor()
        row = None

        try:
            cursor.execute(sql)
            res = cursor.fetchone()
            if not res:
                self._on_read_error()
                cursor.execute(sql)
                res = cursor.fetchone()
            row = self.client.row_as_dict( res, cursor.description )
            if not row["data_directory"]:
                raise RecordNotFoundException("Record not found: %d" % self.record_number)
            return row
        finally:
            cursor.close()

    def read_signal_info(self):
        ''' To know where and what to write.
        '''
        if not self.client.check_connection(self.client.autoreconnect):
            raise Exception('Database connection failed')

        # SQL that returns all we want to know about
        sql = """SELECT
            gs.generic_signal_id, gs.signal_type, gs.data_source_id, daq.computer_id, daq.board_id, daq.channel_id, ca.coefficient_lev2V, ca.coefficient_V2unit, ca.offset,
            gs.time_axis_id, ca.time_axis_id AS setup_time_axis_id, ds.data_file_id, rd.data_directory, das.subdirectory, df.file_name, ds.data_file_key, tax.coefficient AS time_axis_coefficient,
            tax_gs.signal_type as time_axis_signal_type, sp.parameters as gs_parameters, ca.parameters as daq_parameters
        FROM DAQ_channels AS daq
        JOIN channel_attachments AS ca
            ON ca.computer_id = daq.computer_id AND ca.board_id = daq.board_id AND ca.channel_id = daq.channel_id
        JOIN generic_signals AS gs
            ON ca.attached_generic_signal_id = gs.generic_signal_id
        LEFT OUTER JOIN generic_signals as tax_gs
            ON tax_gs.generic_signal_id = gs.time_axis_id
        LEFT OUTER JOIN record_directories AS rd
            ON rd.record_number = %d
        LEFT OUTER JOIN data_signals AS ds
            ON ds.generic_signal_id = gs.generic_signal_id AND ds.record_number = rd.record_number
        LEFT OUTER JOIN data_signals AS tax
            ON tax.generic_signal_id = gs.time_axis_id AND tax.record_number = ds.record_number
        LEFT OUTER JOIN data_files AS df
            ON df.data_file_id = ds.data_file_id
        LEFT OUTER JOIN data_sources AS das
            ON das.data_source_id = gs.data_source_id
        LEFT OUTER JOIN signal_setup AS sp
            ON sp.generic_signal_id = gs.generic_signal_id
        WHERE daq.nodeuniqueid = '%s' AND daq.hardwareuniqueid = '%s' AND daq.parameteruniqueid = '%s'
        """ % (self.record_number, self.node_id, self.hardware_id, self.parameter_id)
        self.client._logger.debug('FSWriter SQL query:\n%s' % sql)

        row = self._read_row(sql)

        self.generic_signal_id = row["generic_signal_id"]
        self.data_source_id = row["data_source_id"]
        
        if row["setup_time_axis_id"]:
            self.time_axis_id = row["setup_time_axis_id"]
            self.time_axis_signal_type = self.client.query("SELECT signal_type FROM generic_signals WHERE generic_signal_id = %d" % self.time_axis_id)[0]["signal_type"]
            self.default_time_axis = False
        else:
            self.time_axis_id = row["time_axis_id"]
            self.time_axis_signal_type = row["time_axis_signal_type"]
            self.default_time_axis = True
        self.data_file_id = row["data_file_id"]

        self.computer_id = row["computer_id"]
        self.board_id = row["board_id"]
        self.channel_id = row["channel_id"]

        self.coefficient_lev2V = row["coefficient_lev2V"]
        self.coefficient_V2unit = row["coefficient_V2unit"]
        self.offset = row["offset"]

        self.signal_type = row["signal_type"]

        self.gs_parameters = row["gs_parameters"]
        self.daq_parameters = row["daq_parameters"]

        if row["file_name"]:
            self.file_name = row["file_name"]
        else:
            self.file_name = self.client.format_file_name( collection_name = self.collection_name, revision = 1, file_format = 'HDF5' )

        # for writing, use the 1st data directory (cache)
        self.file_path = path.join(self.client.get_data_root_path()[0], row["data_directory"], row["subdirectory"], self.file_name)

        self.time_axis_exists = ( row["time_axis_coefficient"] != None )
        self.signal_exists = ( row["data_file_id"] != None )

        if row["data_file_key"]:
            self.data_file_key = row["data_file_key"]
        else:
            self.data_file_key = self.collection_name

    def store_signal_and_file(self):
        '''

        Warning #1: There is no check for revisions. FireSignal is the canonical source of data, so there is no need.
        Warning #2: We automatically say that the file is ready, which in general case is not necessarily true.
        '''

        if not self.client.check_connection(self.client.autoreconnect):
            raise Exception('Database connection failed')
        try:
            cursor = self.client.db.cursor()
            self._store_signal_single(cursor)
        except:
            cursor.close()
            raise
        cursor.close()

    def _store_signal_single(self, cursor):
        ''' Store using several SQL statements (one per query)
        '''
        # Automatically write (only linear) time axes if not present.
        if self.time_axis_signal_type == "LINEAR":
            if not self.time_axis_exists:
                sql = """INSERT IGNORE INTO data_signals (record_number, generic_signal_id, revision, `timestamp`, coefficient) %s""" % \
                    self.client.mysql_values((self.record_number, self.time_axis_id, 1, "\x00NOW()", self.sample_length))
                self.client._logger.debug('FSWriter SQL query:\n%s' % sql)
                cursor.execute(sql)
        else:
            # For non-linear time axes, time0 is not taken into account
            # (better to set it to zero to avoid confusion)
            self.time0 = 0.

        # Create data file record
        sql = """INSERT INTO data_files (data_source_id, record_number, data_format, collection_name, revision, file_name) %s""" % \
            self.client.mysql_values((self.data_source_id, self.record_number, 'HDF5', self.collection_name, 1, self.file_name))
        self.client._logger.debug('FSWriter SQL query:\n%s' % sql)
        cursor.execute(sql)

        data_file_id = cursor.lastrowid

        # Create data signal record
        data_signal_dict = {
            "record_number" : self.record_number,
            "generic_signal_id" : self.generic_signal_id,
            "revision" : 1,
            "timestamp" : "\x00NOW()",
            "data_file_id" : data_file_id,
            "data_file_key" : self.data_file_key,
            "time0" : self.time0,
            "coefficient" : self.coefficient_lev2V,
            "coefficient_V2unit" : self.coefficient_V2unit,
            "offset" : self.offset,
            "computer_id" : self.computer_id,
            "board_id" : self.board_id,
            "channel_id" : self.channel_id
        }
        if not self.default_time_axis:
            data_signal_dict["time_axis_id"] = self.time_axis_id
        self.client.insert("data_signals", data_signal_dict)

        # Parameters
        # (written only if non-trivial)
        if self.daq_parameters or self.gs_parameters:
            sql = "INSERT INTO data_signal_parameters (record_number, generic_signal_id, revision, gs_parameters, daq_parameters) %s" % \
                self.client.mysql_values((self.record_number, self.generic_signal_id, 1, self.gs_parameters, self.daq_parameters))
            self.client._logger.debug('FSWriter SQL query:\n%s' % sql)
            cursor.execute(sql)

        # Mark file as ready
        sql = """INSERT INTO file_status (data_file_id, file_ready) VALUES ( %d, 1 )""" % data_file_id
        self.client._logger.debug('FSWriter SQL query:\n%s' % sql)
        cursor.execute(sql)
        self.client.db.commit()