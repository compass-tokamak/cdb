import inspect
from .client import CDBClient
from .pyCDBBase import CDBException


class FastClient(CDBClient):
    """Client for C++ web-api use in the nodes that should make work of nodes easier and faster."""


    def _handle_exception(self, exc, log_msg=None):
        if not isinstance(exc, CDBException):
            if log_msg is None:
                log_msg = "Exception occurred in {}".format(
                    inspect.currentframe().f_back.f_code.co_name
                )
            self._logger.exception(log_msg)
        return {"msg": str(exc)}

    def get_daq_info(
        self,
        name=None,
        computer_name=None,
        computer_id=None,
        board_id=None,
        channel_id=None,
    ):
        """computer_id (or computer_name), board_id, channel_id or signal name as arguments"""
        if name is not None:
            # [0] first element or last, handle more results
            gsRef = self.get_generic_signal_references(name)
            if len(gsRef) == 0:
                raise CDBException(f"Sig. {name} not found.")
            gsRef = gsRef[0]
            sigAtt = self.get_attachment_table(
                generic_signal_id=gsRef.generic_signal_id
            )
            if len(sigAtt) == 0:
                raise CDBException(f"Sig. attach. {gsRef.generic_signal_id} not found.")
            sigAtt = sigAtt[0]
            computer_id = sigAtt["computer_id"]
            board_id = sigAtt["board_id"]
            channel_id = sigAtt["channel_id"]
        else:
            if computer_name is not None:
                computer_id = self.get_computer_id(computer_name=computer_name)

            sigAtt = self.get_attachment_table(
                computer_id=computer_id, board_id=board_id, channel_id=channel_id
            )[0]
            gsRef = self.get_generic_signal_references(
                generic_signal_id=sigAtt["attached_generic_signal_id"]
            )[0]

        # how it works for axis1_id and other axes, exists sigAtt['axis1_id']?
        if gsRef["time_axis_id"] is not None:
            time_axis_id = gsRef["time_axis_id"]
        else:
            time_axis_id = sigAtt["time_axis_id"]
        return {
            "data_source_id": gsRef["data_source_id"],
            "data_id": sigAtt["attached_generic_signal_id"],
            "time_axis_id": time_axis_id,
            "axis1_id": gsRef["axis1_id"],
            "axis2_id": gsRef["axis2_id"],
            "computer_id": computer_id,
            "board_id": board_id,
            "channel_id": channel_id,
        }

    def new_data_fileF(
        self,
        collection_name: str,
        record_number: int,
        computer_id: int = None,
        computer_name: str = None,
        board_id: int = None,
        channel_id: int = None,
        name: str = None,
        file_format: str = "HDF5",
        file_extension: str = "h5",
        create_subdir: bool = None,
        data_root: str = None,
    ):
        """
        returns data_file_id and full_path

        arguments:
        - record_number
        - computer_id
        - computer_name (alternative to computer_id)
        - board_id
        - channel_id
        - name (instead computer_id, board_id, channel_id)
        - collection_name
        - file_format
        - file_extension (optional)
        - create_subdir (optional), leave it on the node, he write file
        - data_root
        """
        kwargs = {
            "record_number": record_number,
            "computer_id": computer_id,
            "computer_name": computer_name,
            "board_id": board_id,
            "channel_id": channel_id,
            "name": name,
        }
        kwargs = {k: v for k, v in kwargs.items() if v is not None}
        try:
            info = self.get_daq_info(
                name=name,
                computer_name=computer_name,
                computer_id=computer_id,
                board_id=board_id,
                channel_id=channel_id,
            )
            kwargs.update({"data_source_id": info["data_source_id"]})
            data_file = self.new_data_file(
                collection_name,
                file_format,
                file_extension,
                create_subdir,
                data_root,
                **kwargs
            )
            if data_root is None:
                # data files are created in the 1st data directory (cache), cut data root from full path
                data_root = self.get_data_root_path()[0]
                data_file["full_path"] = data_file["full_path"][len(data_root) :]
        except Exception as e:
            return self._handle_exception(e)

        return {
            "data_file_id": data_file["data_file_id"],
            "full_path": data_file["full_path"],
        }

    def set_file_readyF(self, data_file_id: int, chmod_ro: bool = True):
        try:
            self.set_file_ready(data_file_id, chmod_ro)
        except Exception as e:
            return self._handle_exception(e)
        return 1

    def store_signalF(
        self,
        signal_type: str,
        record_number: int,
        computer_id: int = None,
        computer_name: str = None,
        board_id: int = None,
        channel_id: int = None,
        name: str = None,
        time0: float = None,
        data_file_id: int = None,
        data_file_key: str = None,
        coefficient: float = None,
        offset: float = None,
        time_axis_id: int = None,
        daq_parameters: str = None,
        coefficient_v2unit: float = None,
        ignore_if_exists: bool = None,
    ):
        """can save data signal and axis signals (typically linear), returns signal_id

        signal_type is 'time_axis_id', 'axis1_id', 'axis2_id' or 'data_id' for data signal

        agruments for data signals are:
        - signal_type='data_id'
        - record_number
        - computer_id
        - computer_name (alternative to computer_id)
        - board_id
        - channel_id
        - name (instead computer_id, board_id, channel_id)
        - time0=930
        - data_file_id
        - data_file_key
        - coefficient     (optional), default is in CDB, possibly with callibration
        - offset          (optional), default is in CDB, possibly with callibration
        - time_axis_id    (optional), default is taken from CDB, but node should provide axis that was really written
        - daq_parameters  (optional), additional settings or parameters can be saved here

        arguments for linear axis signals are:
        - signal_type='time_axis_id'
        - record_number
        - computer_id
        - computer_name (alternative to computer_id)
        - board_id
        - channel_id
        - name (instead computer_id, board_id, channel_id)
        - coefficient
        - offset
        - coefficient_v2unit (optional)
        - ignore_if_exists (optional)
        """
        kwargs = {
            "record_number": record_number,
            "computer_id": computer_id,
            "computer_name": computer_name,
            "board_id": board_id,
            "channel_id": channel_id,
            "name": name,
            "time0": time0,
            "data_file_id": data_file_id,
            "data_file_key": data_file_key,
            "coefficient": coefficient,
            "offset": offset,
            "time_axis_id": time_axis_id,
            "daq_parameters": daq_parameters,
            "coefficient_v2unit": coefficient_v2unit,
            "ignore_if_exists": ignore_if_exists,
        }
        kwargs = {k: v for k, v in kwargs.items() if v is not None}

        try:
            info = self.get_daq_info(
                name=name,
                computer_name=computer_name,
                computer_id=computer_id,
                board_id=board_id,
                channel_id=channel_id,
            )
            signal_id = info[signal_type]

            if "name" in kwargs:
                kwargs.update(
                    {
                        "computer_id": info["computer_id"],
                        "board_id": info["board_id"],
                        "channel_id": info["channel_id"],
                    }
                )

            # for axes remove kwargs['computer_id'], kwargs['board_id'], kwargs['channel_id'], otherwise it create mess
            if signal_type != "data_id":
                if "computer_id" in kwargs:
                    del kwargs["computer_id"]
                if "computer_name" in kwargs:
                    del kwargs["computer_name"]
                if "board_id" in kwargs:
                    del kwargs["board_id"]
                if "channel_id" in kwargs:
                    del kwargs["channel_id"]

            data_file = self.store_signal(signal_id, **kwargs)
        except Exception as e:
            return self._handle_exception(e)
        return signal_id  # signal id is useful for writing data file as time_axis_id for example

    def create_recordF(self, record_type: str, description: str, ControlString: str):
        """create new record, returns created record number
        - record_type 'EXP', 'VOID', 'MODEL'
        - description of the record
        - ControlString - small input barrier - temporary solution
        """
        ret_val = 0
        if ControlString == "Jeden nový záznam pro TOK Control, prosím.":
            ret_val = self.create_record(
                record_type=record_type, description=description
            )
        else:
            ret_val = -17
        return ret_val

    def last_record_numberF(self, record_type: str = "EXP"):
        """just last_record_number, returns record number or -1
        - record_type 'EXP', 'VOID', 'MODEL'
        """
        return self.last_record_number(record_type)
