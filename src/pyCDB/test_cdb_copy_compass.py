# CDB test using the pyCDB.client module

import sys
import os
import os.path
from pylab import *
import h5py
from pyCDB.client import CDBClient as cdb_client
import copy

if __name__ == "__main__":
	# connectct to the database and test
	cdb = cdb_client(log_level=20, db='CDB_copy')
	if cdb is None or not cdb.check_connection():
	    print("error: could not connect to MySQL server")
	    sys.exit(-1)
	print("Last VOID record number: %i" % cdb.last_record_number(record_type='VOID'))
	# create a record
	record_number = cdb.create_record(record_type='VOID', record_time=None, description='testing')
	print("Created new record number: %i" % record_number)

	# test the databe
	# use pre-defined source and signal name for testing
	data_source_name = 'pycdb_test'
	generic_signal_name = 'test_01D'

	# get the full data source reference (all columns)
	refs = cdb.get_data_source_references(data_source_name=data_source_name)
	assert len(refs) == 1
	print('\nData source "%s" reference:' % data_source_name)
	print(refs[0])

	# get the full generic signal reference (all columns)
	refs = cdb.get_generic_signal_references(generic_signal_name)
	assert len(refs) == 1
	gs_ref = refs[0]
	# and for the time axis
	refs = cdb.get_generic_signal_references(generic_signal_id=gs_ref['time_axis_id'])
	assert len(refs) == 1
	t_ax_gs_ref = refs[0]

	print('Generic signal "%s" reference(s):' % generic_signal_name)
	print(gs_ref)

	# Get current attachment
	# attach - do it just once
	# import pyCDB.DAQClient
	# daq = pyCDB.DAQClient.CDBDAQClient(db='CDB_copy')
	refs = cdb.get_attachment_table(generic_signal_id=gs_ref['generic_signal_id'])
	assert len(refs) == 1
	daq_ref = refs[0]

	# Now create a file with a data collection
	collection = 'pycdb_test'
	file_ref = cdb.new_data_file(collection, data_source_id=gs_ref['data_source_id'], record_number=record_number,
	                             file_format="HDF5")
	print('Target file: %s' % file_ref['full_path'])

	# construct a test signal
	test_t = linspace(-0.2, 1, 200)
	test_s = exp(-((test_t - 0.4) * 3) ** 8) * (rand(*test_t.shape) * 0.2 + 0.9)
	figure()
	plot(test_t, test_s)
	title('test signal')

	# write to the file
	if not os.path.isdir(os.path.dirname(file_ref['full_path'])):
	    os.mkdir(os.path.dirname(file_ref['full_path']))
	fh5 = h5py.File(file_ref['full_path'], 'w')
	print('\nPath to HDF5 file: ', file_ref['full_path'])
	grp_name = 'test'
	data_file_key = grp_name + '/' + generic_signal_name
	f_grp = fh5.create_group(grp_name)
	f_grp.create_dataset(generic_signal_name, data=test_s)
	fh5.close()
	# tell the database that the file can be used
	cdb.set_file_ready(file_ref['data_file_id'])

	# store signals to the database
	# axis first
	cdb.store_signal(gs_ref['time_axis_id'], record_number=record_number, offset=test_t[0], coefficient=test_t[1] - test_t[0])
	# now get the data signal reference to know the revision
	refs = cdb.get_signal_references(generic_signal_id=gs_ref['time_axis_id'], record_number=record_number)
	assert len(refs) > 0
	# use the latest revision
	t_ax_rev = refs[-1]['revision']
	# now store the signal itself
	cdb.store_signal(gs_ref['generic_signal_id'], record_number=record_number, data_file_id=file_ref['data_file_id'],
	    data_file_key=data_file_key, time_axis_revision=t_ax_rev, time0=test_t[0],
	    computer_id=daq_ref['computer_id'], board_id=daq_ref['board_id'], channel_id=daq_ref['board_id'])


	# retreive back the data from CDB
	print('\nRetrieving data from CDB')
	# use string id
	str_id = '%s:%i' % (gs_ref['alias'], record_number)
	sig = cdb.get_signal(str_id)
	# compare plots
	sig.plot()
	hold(True)
	plot(test_t, test_s, 'r--', label='original')
	legend(loc='lower center')

	# plot(sig.time_axis.data, sig.data, 'r--')
