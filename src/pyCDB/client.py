'''COMPASS database main module

Contains functions to connect, retrieve and store data using the COMPASS database.
'''

from typing import Literal
import sys
import os.path
import datetime
from pyCDB.pyCDBBase import pyCDBBase, cdb_base_check, _mysqlErrorsModule
from pyCDB.pyCDBBase import OrderedDict, isstringlike, isintlike, json
import logging
import itertools
from time import sleep
import re
from pyCDB.slicing import str_to_slice

try:  # hack to fix import error for python>= 3.10
    from collections import Iterable
except ImportError:
    from collections.abc import Iterable

axes_names = ('time_axis', 'axis1', 'axis2', 'axis3', 'axis4', 'axis5', 'axis6')

_wildcard_shot_number_ = 0

RecordType = Literal['EXP', 'VOID', 'MODEL', '']


class CDBClient(pyCDBBase):
    '''Fundamental connector class for CDB
    '''

    # TODO: Use global instead
    __axes_names = axes_names

    @cdb_base_check
    def last_record_number(self, record_type: RecordType ='EXP'):
        '''Returns the "record number" of the last available record

        :param record_type: record type: EXP, VOID, MODEL or empty string (any type)
        :rtype: record number (integer)
        '''
        sql = "SELECT MAX(`record_number`) AS last_record_number FROM `shot_database`"
        if record_type:
            sql += " WHERE `record_type`='%s'" % record_type
        result = self.query(sql)
        if len(result) == 0 or result[0]["last_record_number"] is None:
            return -1
        return result[0]["last_record_number"]

    def last_shot_number(self):
        '''The same as last_record_number for record_type = 'EXP'

        :rtype: record number (integer)
        '''
        return self.last_record_number(record_type='EXP')

    @cdb_base_check
    def get_FS_signal_reference(self, nodeuniqueid, hardwareuniqueid, parameteruniqueid):
        '''Get the currently attached generic signal reference for a FireSignal channel.

        Returns None if not found.
        '''
        daq_sql = "SELECT * FROM `DAQ_channels` WHERE " \
                  "`nodeuniqueid`='%s' AND `hardwareuniqueid`='%s' AND `parameteruniqueid`='%s'" \
                  % (nodeuniqueid, hardwareuniqueid, parameteruniqueid)
        result = self.query(daq_sql)
        if len(result) == 0:
            return None
        cdb_ids = result[0]

        ca_sql = "SELECT * FROM `channel_attachments` WHERE " \
                 "`computer_id`=%i AND `board_id`=%i AND `channel_id`=%i" \
                 % (cdb_ids['computer_id'], cdb_ids['board_id'], cdb_ids['channel_id'])
        attachment = self.query(ca_sql, error_if_empty=True)[0]
        return self.get_generic_signal_references(generic_signal_id=attachment['attached_generic_signal_id'])[0]

    @cdb_base_check
    def create_record(self, record_type='EXP', record_time=None, description='', data_root=None,
                      FS_event_number=0, FS_event_id='', record_number=None, autonumber=False):
        '''Creates a new record in the database

        :param record_type: 'EXP', 'VOID' or 'MODEL'
        :param record_time: default current, can be integer time stamp or a datetime.datetime obejct
        :param description: record description
        :param data_root: CDB data root path, see :func:`get_data_root_path`
        :param FS_event_number: FireSignal event number (optional) # TODO: Remove?
        :param FS_event_id: FireSignal event id (optional) # TODO: Remove?
        :param record_number: new record number (None for first available)
        :param autonumber: automatic numbering (effective only if record_number is not None)
        :rtype: new record number
        '''
        cursor = self.db.cursor()
        if record_time is None:
            record_time = '\x00NOW()'
        elif isintlike(record_time):
            record_time = datetime.datetime.fromtimestamp(record_time)
        if record_number is None:
            record_number_new = self.last_record_number(record_type=record_type) + 1
        else:
            record_number_new = record_number
        min_rec_nums = {'EXP': 0, 'MODEL': 1000000, 'VOID': 10000000}

        record_number_new = max(record_number_new, min_rec_nums[record_type])

        # Use proxy _pub table for non-EXP records
        if record_type == 'EXP':
            rec_db = 'shot_database'
        else:
            # TODO: remove this checking in the future
            cursor.execute("SHOW TABLES LIKE 'shot_database_pub'")
            if cursor.fetchall():
                rec_db = 'shot_database_pub'
            else:
                self._logger.debug('using all databese design - shot_database_pub table does not exist')
                rec_db = 'shot_database'

        # Try the new record number (takes first available if autonumber==True)
        while True:
            new_record = {
                "record_number": record_number_new,
                "record_time": record_time,
                "record_type": record_type,
                "description": description
            }
            try:
                self.insert(rec_db, new_record)
                record_number = record_number_new
                break
            except _mysqlErrorsModule.IntegrityError:
                # TODO: maybe OR should be here instead? (at least according to __doc__)
                if record_number is not None and not autonumber:
                    self.error("record number %i already exists" % record_number)
                else:
                    record_number_new += 1
            except _mysqlErrorsModule.OperationalError:
                self.error("Error creating record, probably not authorized")
                raise
            except:
                self.error("SQL error: " + str(sys.exc_info()[1]))
                raise Exception('SQL error')

        data_directory = format_record_path(record_number, record_type)
        data_root = self.get_data_root_path(data_root)[0]  # use the 1st (cache) directory
        new_row = {"record_number": record_number, "data_directory": data_directory}
        self.insert("record_directories", new_row)

        # create directory structure
        # this will be done outside to manage permissions
        full_data_directory = os.path.join(data_root, data_directory)
        if not os.path.isdir(full_data_directory):
            try:
                self.makedirs(full_data_directory, mode=2047)  # 2047 = 0o3777, Jython does not support 0o
                # os.chmod(full_data_directory, 2047)  # 2047 = 0o3777, Jython does not support 0o
            except Exception:
                cursor.close()
                self.error("record directory could not be created")
                raise
                # raise Exception('record directory could not be created')
        else:
            try:
                os.chmod(full_data_directory, 511)  # 511 = 0o777, Jython does not support 0o
            except Exception:
                self._logger.warning('could not chmod the data directory to 777')
        try:
            for data_dir in (os.path.join(full_data_directory, ds['subdirectory']) for ds in self.get_data_source_references()):
                self.makedirs(data_dir, mode=2047)  # 2047 = 0o3777, Jython does not support 0o
                # os.chmod(data_dir, 2047)  # 2047 = 0o3777, Jython does not support 0o
        except Exception:
            self._logger.error("data directory(ies) could not be created")
            raise
        return record_number

    @cdb_base_check
    def create_generic_signal(self, generic_signal_name=None, alias=None, first_record_number=0, last_record_number=-1,
                              data_source_id=None, time_axis_id=None, axis1_id=None, axis2_id=None, axis3_id=None,
                              axis4_id=None, axis5_id=None, axis6_id=None,
                              units=None, description=None, signal_type='FILE', wildcard=False, **kwargs):
        '''Creates a generic signal in database.

        Returns the id of the new generic signal.
        '''

        if 'data_source' in kwargs and data_source_id is None:
            # backward compatibility
            data_source_id = kwargs['data_source']
            self._logger.warning('use data_source_id argument instead of data_source')

        if generic_signal_name is None:
            self.error('You must provide generic_signal_name')

        if wildcard == True and not (first_record_number == 0 and last_record_number == 0):
            self.error('wildcard=True requires first_record_number=0 and last_record_number=0')
        # allow '.' for FS generic signal names
        generic_signal_name, repl_ch = self.filter_sql_str(generic_signal_name, extra_chars='_.')
        alias, repl_ch = self.filter_sql_str(alias)

        new_row = {
            "generic_signal_id": None,
            "generic_signal_name": generic_signal_name,
            "alias": alias,
            "first_record_number": first_record_number,
            "last_record_number": last_record_number,
            "data_source_id": data_source_id,
            "time_axis_id": time_axis_id,
            "axis1_id": axis1_id,
            "axis2_id": axis2_id,
            "axis3_id": axis3_id,
            "axis4_id": axis4_id,
            "axis5_id": axis5_id,
            "axis6_id": axis6_id,
            "units": units,
            "description": description,
            "signal_type": signal_type,
            "wildcard": int(wildcard)
        }
        return self.insert("generic_signals", new_row, return_inserted_id=True)

    @cdb_base_check
    def resolve_record_number(self, argv):
        '''Resolves record number from input parameters dictionary argv

        :param argv: dictionary with parameters
        :rtype: record number

        Recognized argv keys:

        * record_number (-1 returns last record number)
        * record_type
        '''

        if 'record_number' in argv:
            record_number = argv['record_number']
            if record_number == -1:
                record_type = '' if 'record_type' not in argv else argv['record_type']
                record_number = self.last_record_number(record_type)
        else:
            # self._logger.warning("warning: resolve_record_number needs record_number")
            record_number = None
        return record_number

    @cdb_base_check
    def get_coefficient_lev2V(self, generic_signal_id):
        '''Get coefficient_lev2V (needed for FireSignal operation)
        '''

        cursor = self.db.cursor()
        cursor.execute("SELECT `coefficient_lev2V` FROM `channel_attachments` WHERE `attached_generic_signal_id` = %i" % generic_signal_id)
        row = cursor.fetchone()
        cursor.close()
        if row:
            return row[0]
        else:
            self._logger.debug('using default lev2V coefficient 1 for generic_signal_id = %i' % generic_signal_id)
            return 1

    @cdb_base_check
    def get_coefficient_V2unit(self, generic_signal_id):
        '''Get coefficient_V2unit (needed for FireSignal operation)
        '''

        cursor = self.db.cursor()
        cursor.execute("SELECT `coefficient_V2unit` FROM `channel_attachments` WHERE `attached_generic_signal_id` = %i" % generic_signal_id)
        row = cursor.fetchone()
        cursor.close()
        if row:
            return row[0]
        else:
            self._logger.debug('using default V2unit coefficient 1 for generic_signal_id = %i' % generic_signal_id)
            return 1

    @cdb_base_check
    def find_generic_signals(self, alias_or_name='', regexp=False):
        '''Find generic signals by a name or alias

        :param alias_or_name: search string
        :param regexp: use regular expressions (MySQL REGEXP)
        :rtype: tuple of signal references (like get_generic_signal_references)

        The search criterion in MySQL is "SELECT ... WHERE generic_signal_name LIKE %%alias_or_name%%
        OR alias LIKE %%alias_or_name%%"

        '''
        if regexp:
            sql_query = ("SELECT * FROM `generic_signals` WHERE `generic_signal_name` REGEXP '%s' "
                         "OR `alias` REGEXP '%s';" % (alias_or_name, alias_or_name))
        else:
            sql_query = ("SELECT * FROM `generic_signals` WHERE `generic_signal_name` LIKE '%%%s%%' "
                         "OR `alias` LIKE '%%%s%%';" % (alias_or_name, alias_or_name))
        return self.query(sql_query)

    @cdb_base_check
    def get_generic_signal_references(self, str_id='', **kwargs):
        '''Returns a tuple of references to generic signal, based on specified criteria

        :param str_id: generic signal string id - see see :func:`decode_generic_signal_strid`

        Keyword parameters:
        generic_signal_id (or signal_id): id of the generic signal
        generic_signal_ref or generic_signal_reference
        generic_signal_name + (optionally) data_source_id or data_source_name + record_number (for validity)
        alias_or_name: first find by alias; if no match find by name
        generic_signal_strid: deprecated - use str_id
        '''

        if 'generic_signal_reference' in kwargs:
            kwargs['generic_signal_ref'] = kwargs['generic_signal_reference']
        if 'generic_signal_ref' in kwargs:
            return (kwargs['generic_signal_ref'],)
        if 'generic_signal_strid' in kwargs:  # backward compatibility
            str_id = kwargs.pop('generic_signal_strid')
        if str_id:
            kwargs.update(decode_generic_signal_strid(str_id))
        if 'alias_or_name' in kwargs:
            kwargs2 = kwargs.copy()
            kwargs2['alias'] = kwargs2.pop('alias_or_name')
            self._logger.disabled = True
            res = self.get_generic_signal_references(**kwargs2)
            self._logger.disabled = False
            if len(res) > 0:
                return res
            else:
                kwargs['generic_signal_name'] = kwargs['alias_or_name']
        sql_query = "SELECT * FROM `generic_signals` WHERE 1"
        if 'signal_name' in kwargs:
            kwargs['generic_signal_name'] = kwargs['signal_name']
        if 'signal_id' in kwargs:
            kwargs['generic_signal_id'] = kwargs['signal_id']
        if 'generic_signal_id' in kwargs:
            sql_query += " AND `generic_signal_id`=%i" % kwargs['generic_signal_id']
    #    else:
    #        print("generic_signal_name or generic_signal_id must be specified")
    #        return None
        else:
            if 'alias' in kwargs:
                sql_query += " AND `alias`='%s'" % kwargs['alias']
            if 'generic_signal_name' in kwargs:
                sql_query += " AND `generic_signal_name`='%s'" % kwargs['generic_signal_name']
            if 'data_source_id' in kwargs:
                sql_query += " AND `data_source_id`=%i" % kwargs['data_source_id']
            elif 'data_source_name' in kwargs:
                data_source_id = self.get_data_source_id(kwargs['data_source_name'])
                if data_source_id is not None:
                    sql_query += " AND `data_source_id`=%i" % data_source_id
                else:
                    self._logger.warning("data_source not found, searching unspecified data source")
            # TODO: change to time stamps because of VOID and MODEL numbering
            # record_number = self.resolve_record_number(kwargs)
            # if record_number is not None:
            #     sql_query += " AND ((`last_record_number`=-1 OR `last_record_number`>=%i) AND `first_record_number`<=%i)" % \
            #     (record_number, record_number)
        return self.query(sql_query)

    @cdb_base_check
    def get_data_file_reference(self, **kwargs):
        '''Returns a tuple of dictionaries containing complete file references containing the signal.
        Returns one reference for each file revision.

        Typical parameters:
        data_file_id: unique id
        generic_signal_name, data_source_id (name), data_source_id, generic_signal_id
        record_number

        If data_source_id is not specified the signal is first sought in generic signal names.
        '''

        try:
            cursor = self.db.cursor()
            if 'data_file_id' not in kwargs:
                sql_query = "SELECT `data_file_id`,`data_file_key` FROM `data_signals` WHERE"
                data_source_id = None
                if 'data_source_id' in kwargs:
                    data_source_id = kwargs['data_source_id']
                elif 'data_source' in kwargs:
                    data_source_id = self.get_data_source_id(kwargs['data_source'])
                if 'generic_signal_id' in kwargs:
                    generic_signal_id = kwargs['generic_signal_id']
                elif 'generic_signal_name' in kwargs:
                    generic_signal_id = self.get_generic_signal_id(kwargs['generic_signal_name'], data_source_id=data_source_id)
                else:
                    self.error("data_file_id, generic_signal_id or generic_signal_name must be specified")
                if generic_signal_id is None:
                    self.error("signal not found")
                sql_query += " `generic_signal_id`=%i" % generic_signal_id
                record_number = self.resolve_record_number(kwargs)
                if record_number is None:
                    self.error("record number not found")
                sql_query += " AND `record_number`=%i" % record_number
                cursor.execute(sql_query)
                if cursor.rowcount > 1:
                    self.error("Ambiguous signal reference")
                row = cursor.fetchone()
                if row is None:
                    self.error("Signal not found.")
                data_file_id = row[0]
            else:
                data_file_id = kwargs['data_file_id']
            cursor.execute("SELECT * FROM `data_files` " +
                           "WHERE `data_file_id`='%i'" % data_file_id +
                           "ORDER BY `revision`")
            # ,`file_ready`
            if cursor.rowcount == 0:
                self.error("File not found in the database: ID={0}".format(data_file_id))
            elif cursor.rowcount != 1:
                self.error("Ambiguous file reference: {0} rows found.".format(cursor.rowcount))
            res = self.row_as_dict(cursor.fetchone(), cursor.description)
            subdir = self.get_data_source_subdir(res['data_source_id'])
            record_path = self.get_record_path(record_number=res['record_number'])
            cursor.execute('SELECT `file_ready` from `file_status` WHERE `data_file_id`=%i' % data_file_id)
            file_ready = False
            if cursor.rowcount == 1:
                file_ready = bool(cursor.fetchone()[0])
            res['file_ready'] = file_ready
            res['full_path'] = None
            for data_root in self.get_data_root_path():
                # search data directories and return the first one containing the file
                full_path = os.path.join(data_root, record_path, subdir, res['file_name'])
                if os.path.isfile(full_path):
                    res['full_path'] = full_path
                    break
            if not res['full_path']:
                self._logger.error('CDB data file {} not found'.format(os.path.join(
                    record_path, subdir, res['file_name'])))
            cursor.close()
            return res
        finally:
            if cursor:
                try:
                    cursor.fetchall()
                    cursor.close()
                except:
                    pass

    @cdb_base_check
    def get_data_source_id(self, data_source_name):
        '''Returns data source id of a given data source name'''

        sql_query = ("SELECT `data_source_id` FROM `data_sources` "
                     "WHERE `name` =  '%s' " % data_source_name)
        row = self.query(sql_query, error_if_empty=True)[0]
        return row["data_source_id"]

    @cdb_base_check
    def get_data_source_subdir(self, data_source_id):
        '''Returns the data storage subdirectory for a given data source id'''

        sql_query = ("SELECT `subdirectory` FROM `data_sources` "
                     "WHERE `data_source_id` =  '%i' " % data_source_id)
        row = self.query(sql_query, error_if_empty=True)[0]
        return row["subdirectory"]

    @cdb_base_check
    def get_data_source_references(self, **kwargs):
        '''Returns a tuple with references to data sources

        Typical parameters:
        data_source_name, data_source_id, description, record_number
        '''

        sql_query = "SELECT * FROM `data_sources` WHERE 1"
        if 'data_source_name' in kwargs:
            sql_query += " AND `name`='%s'" % kwargs['data_source_name']
        if 'data_source_id' in kwargs:
            sql_query += " AND `data_source_id`=%i" % kwargs['data_source_id']
        if 'description' in kwargs:
            sql_query += " AND `description` LIKE '%%%s%%'" % kwargs['description']
        record_number = self.resolve_record_number(kwargs)
        if record_number is not None:
            generic_signal_refs = self.get_generic_signal_references(record_number=record_number)
            sql_ids = '('
            for r in generic_signal_refs:
                sql_ids += '%i,' % r['data_source_id']
            sql_ids = sql_ids[:-1] + ')'
            sql_query += " AND `data_source_id` IN %s" % sql_ids
        return self.query(sql_query)

    def get_data_root_path(self, data_root=None):
        '''Return the CDB data root path

        :param data_root: user specified root directory, if None given by CDB_DATA_ROOT
        :rtype:  the full path with expanded variables
        '''
        if data_root:
            res = [os.path.expandvars(os.path.expanduser(d)) for d in data_root.split(os.path.pathsep)]
        else:
            res = self.data_root
        return res

    @cdb_base_check
    def get_record_path(self, **kwargs):
        '''Returns data storage path (from the database) for a given record number'''

        record_number = self.resolve_record_number(kwargs)
        if record_number is None:
            self.error("cannot resolve record number")
        cursor = self.db.cursor()
        cursor.execute("SELECT `data_directory` FROM record_directories WHERE `record_number`=%i" % record_number)
        row = cursor.fetchone()
        cursor.close()
        if not row:
            self.error('record path not found')
        return row[0].replace("/", os.sep)

    @cdb_base_check
    def new_data_file(self, collection_name, file_format="HDF5", file_extension=None, create_subdir=None, data_root=None, **kwargs):
        '''Creates a record in the COMPASS database for a new data file. Returns the created data file reference.

        :param collection_name: signal collection name (used for file name)
        :param create_subdir: automatically create the file subdirectory (default if data_root is not set)
        :param data_root: use user-supplied data_root instead of the default one

        :param kwargs: keyword parameters
            * record_number: resolve_record_number
            * data_source_id, data_source_ref, or data_source (name)

        First looks for a record with the input record number, data source and collection.
        If found, a new revision is created. Otherwise a new id with revision 1 is created.
        '''

        if 'data_source_id' in kwargs:
            data_source_refs = self.get_data_source_references(data_source_id=kwargs['data_source_id'])
        elif 'data_source_ref' in kwargs:
            data_source_refs = (kwargs['data_source_ref'],)
        elif 'data_source' in kwargs:
            data_source_refs = self.get_data_source_references(data_source=kwargs['data_source'])
        else:
            self.error("data_source_id, data_source_ref or data_source must be specified")
        if len(data_source_refs) != 1:
            self.error("data source not found or ambiguous")
        data_source_id = data_source_refs[0]['data_source_id']
        record_number = self.resolve_record_number(kwargs)
        if record_number is None:
            self._logger.warning("record_number not specified, using the last record")
            record_number = self.last_record_number()
        if record_number == -1 or record_number is None or data_source_id is None or not self.record_exists(record_number=record_number):
            self.error("invalid record number or data source")
        cursor = self.db.cursor()
    #    cursor.execute(("SELECT MAX(`revision`) from `data_files` WHERE " + \
    #                   "`data_files`.`record_number` =  %i AND `data_files`.`data_source_id` =  %i " + \
    #                   "AND `data_files`.`collection_name` = '%s'") \
    #                   % (record_number,data_source_id,collection_name) )
        cursor.execute(("SELECT `data_file_id`,MAX(`revision`) from `data_files` WHERE " +
                       "`record_number` =  %i AND `data_source_id` =  %i AND `collection_name` = '%s'")
                       % (record_number, data_source_id, collection_name))
        row = cursor.fetchone()
        cursor.close()
        if row[0] is None:
            revision = 1
        else:
            revision = row[1] + 1

        file_name = self.format_file_name(collection_name, revision, file_format, file_extension)

        # now insert
        new_data_file = {
            "data_file_id": None,
            "data_source_id": data_source_id,
            "record_number": record_number,
            "data_format": file_format,
            "collection_name": collection_name,
            "revision": revision,
            "file_name": file_name
        }
        data_file_id = self.insert("data_files", new_data_file, return_inserted_id=True)
        res = self.query("SELECT * FROM `data_files` WHERE `data_file_id`=%i" % data_file_id, error_if_empty=True)[0]
        res['file_ready'] = False
        if create_subdir is None:
            create_subdir = not data_root
        if data_root is None:
            data_root = self.get_data_root_path()[0]  # data files are created in the 1st data directory (cache)

        file_path = self.get_record_path(record_number=record_number)
        file_path = os.path.join(file_path, data_source_refs[0]['subdirectory'])
        res['full_path'] = os.path.join(data_root, file_path, file_name)
        if create_subdir and not os.path.isdir(os.path.dirname(res['full_path'])):
            # Create the destination directory(ies) if requested
            # with cache, even the root record derectory might not exist
            # ALTERNATIVE: don't delete directories on the cache
            self.makedirs(os.path.dirname(res['full_path']), mode=2047)  # 2047 = 0o3777, Jython does not support 0o
        return res

    @cdb_base_check
    def store_signal(self, generic_signal_id, **kwargs):
        '''Insert new signal into SQL database. Inserts a single database row in the data_signals table. The generic signal
        has to exist in the database. The recommended procedure is to call this method after the data file is created or the
        data are inserted into the file. It not create or write in to the data file!!! In general data signal is specified by
        generic signal id, By default all axes are defined in th generic
        signal table and are identified by id, variant and revision.

        :param generic_signal_id: generic signal id to assign the data to.
        :param record_number: Record numbber the data belongs to.
        :param data_file_id: ID of the row in data_files table. The id is returned by create_data_file method.
        :param data_file_key: Path of the group storing the data within the HDF5 file.
        :param time0: Offset of a LINEAR time axis
        :param coefficient: ADC level to voltage conversion factor (given by channel ADC range). Corresponds to coefficient_lev2V in channel_setup table.
        :param offset: Integer offset of the ADC level (gives 0 voltage). To be applied before mupltiplication by coefficient.
        :param coefficient_V2unit: LINEAR conversion coefficient of the ADC voltage to physical units.
        :param variant: Variant of the stored data.
        :param time_axis_id: Generic signal id of the time axis for this signal. This might change for every
               record_number and variant. If None the time
               axis from the generic_signal table is used.
        :param time_axis_revision: Revision of the time axis for this data signal. If omitted it defaults to latest
               revision of the time axis signal.
        :param time_axis_variant: Variant of the time axis for this signal. Typically the same as variant.
        :param axis#_revision: Generic signal id of the spatial axis for this signal. This might change for every
               record_number and variant. If None the time axis from the generic_signal table is used.
        :param axis#_variant:Variant of the an axis for this signal. Typically the same as variant.
        :param note: text note (new revision reason etc.)
        :param computer_id: specify originating ADC
        :param computer_name: alternative to computer_id
        :param board_id: Id of the board in the computer. Viz. hardware channels page in webcdb
        :param channel_id: Channel id on the board
        :param data_quality: enum('UNKNOWN', 'POOR', 'GOOD', 'VALIDATED')
        :param deleted:
        :param daq_parameters: DAQ parameters (setup)
        :param gs_parameters: generic signal parameters (setup)
        :param ignore_if_exists: Will not write if such signal exists (useful when multiple sources want to write it
           at the same time)
        :kwargs fields from data signal reference:

        Attachment information (computer, board & channel) is automatically
        filled in if not specified (using `get_attachment_table` method).
        '''
        # Remove None's from kwargs
        kwargs = dict(((k, v) for k, v in kwargs.items() if v is not None))

        # Object that will be passed to insert() method in the end
        new_signal = {}

        # Get generic signal
        gsref = self.get_generic_signal_references(generic_signal_id=generic_signal_id)
        if len(gsref) != 1:
            self.error("generic signal id %i not found" % generic_signal_id)
        gsref = gsref[0]
        new_signal["generic_signal_id"] = generic_signal_id

        # Record number
        record_number = self.resolve_record_number(kwargs)
        if record_number is None:
            self.error("invalid record number")
        new_signal["record_number"] = record_number

        # Get channel attachment info
        attachments = self.get_attachment_table(generic_signal_id=generic_signal_id, record_number=record_number)
        if attachments:
            attachment = attachments[-1]
        else:
            attachment = None

        signal_parameters = {} # TODO: Move somewhere to the end

        # Time axis (if not default)
        if 'time_axis_id' in kwargs:
            new_signal['time_axis_id'] = kwargs.get('time_axis_id')
        elif attachment and attachment['time_axis_id']:
            new_signal['time_axis_id'] = attachment['time_axis_id']
        else:
            new_signal['time_axis_id'] = gsref['time_axis_id']

        new_signal['data_quality'] = kwargs.get('data_quality', 'UNKNOWN')
        new_signal['deleted'] = bool(kwargs.get('deleted'))
        new_signal['time0'] = kwargs.get('time0', 0.0)
        new_signal['note'] = kwargs.get('note', '')

        # Variant (default = '')
        new_signal['variant'] = kwargs.get('variant', '')
        try:
            validate_variant(new_signal['variant'])
        except Exception as e:
            self.error(str(e))

        # Add all axes parameters (id, revision, variant)
        for ax in self.__axes_names:
            # Try to get axis id (only time_axis can override the default one.)
            ax_id = ax + '_id'
            if ax == "time_axis" and ax_id in kwargs:
                axis_id = kwargs[ax_id]
                new_signal[ax_id] = axis_id
            elif ax == "time_axis" and attachment and attachment.get(ax_id):
                axis_id = attachment.get(ax_id)
                new_signal[ax_id] = axis_id
            else:
                axis_id = gsref[ax_id] # Default (None in data_signals table)

            if not axis_id:
                continue

            ax_refs = self.get_signal_references(generic_signal_id=axis_id,
                                                 record_number=record_number)

            # Get axis variant
            ax_var = ax + '_variant'
            if ax_var in kwargs:
                try:
                    validate_variant(kwargs[ax_var])
                except Exception as e:
                    self.error(str(e))
                new_signal[ax_var] = kwargs[ax_var]
            else:
                # use the signal variant for the ax
                new_signal[ax_var] = new_signal['variant']

            # Get axis revision
            ax_rev = ax + '_revision'
            if ax_rev in kwargs:
                new_signal[ax_rev] = kwargs[ax_rev]
            else:
                # use already stored axes
                ax_refs = [ref for ref in ax_refs if ref['variant'] == new_signal[ax_var]]
                if ax_refs:
                    # the same variant
                    new_signal[ax_rev] = max(ax['revision'] for ax in ax_refs)
                    self._logger.info('Using latest revision %i for %s' % (new_signal[ax_rev], ax))
                else:
                    # ax not stored yet
                    # TODO should throw an error here?
                    new_signal[ax_rev] = 1
                    self._logger.warning('Axis signal has not been stored yet.')

        # Data file & key
        if gsref['signal_type'] == 'FILE':
            if 'data_file_id' not in kwargs:
                self.error("data_file_id must be specified")
            new_signal["data_file_id"] = int(kwargs['data_file_id'])
            data_file = self.get_data_file_reference(data_file_id=new_signal["data_file_id"])
            if data_file['data_format'] in ('HDF5', 'NETCDF4') and 'data_file_key' not in kwargs:
                self.error("data_file_key must be specified")
            new_signal["data_file_key"] = kwargs.get('data_file_key', '')
            if len(new_signal["data_file_key"]) > 127:
                self.error("data_file_key too long (%d > 127 characters): %s" %
                    (len(new_signal["data_file_key"]), new_signal["data_file_key"]))

        elif gsref['signal_type'] == 'LINEAR':
            new_signal["data_file_id"] = None
            new_signal["data_file_key"] = ''

        # Channel, boards & coefficients (default)
        if attachment:
            new_signal["computer_id"] = attachment['computer_id']
            new_signal["board_id"] = attachment['board_id']
            new_signal["channel_id"] = attachment['channel_id']
            new_signal["coefficient"] = attachment['coefficient_lev2V']
            new_signal["offset"] = attachment['offset']
            new_signal["coefficient_V2unit"] = attachment['coefficient_V2unit']
            signal_parameters["daq_parameters"] = attachment['parameters']
        else:
            new_signal["coefficient"] = 1.0
            new_signal["offset"] = 0.0
            new_signal["coefficient_V2unit"] = 1.0
            new_signal["computer_id"] = None
            new_signal["board_id"] = None
            new_signal["channel_id"] = None
            signal_parameters["daq_parameters"] = ''

        # Channel, boards & coefficients (from kwargs)
        if 'computer_id' in kwargs:
            new_signal["computer_id"] = kwargs["computer_id"]
        elif 'computer_name' in kwargs:
            new_signal["computer_id"] = self.get_computer_id(computer_name=kwargs['computer_name'])
        if 'board_id' in kwargs:
            new_signal["board_id"] = '%i' % kwargs['board_id']
        if 'channel_id' in kwargs:
            new_signal["channel_id"] = '%i' % kwargs['channel_id']
        if 'coefficient_lev2V' in kwargs:
            new_signal["coefficient"] = kwargs['coefficient_lev2V']  # TODO: Remove and don't use
        if 'coefficient' in kwargs:
            new_signal["coefficient"] = kwargs['coefficient']
        if 'offset' in kwargs:
            new_signal["offset"] = kwargs['offset']
        if 'coefficient_V2unit' in kwargs:
            new_signal["coefficient_V2unit"] = kwargs['coefficient_V2unit']
        if 'daq_parameters' in kwargs:
            signal_parameters["daq_parameters"] = kwargs['daq_parameters']

        if 'timestamp' in kwargs:
            if isintlike(kwargs['timestamp']):
                new_signal["timestamp"] = datetime.datetime.fromtimestamp(kwargs['timestamp']).strftime('%Y-%m-%d %H:%M:%S')
            else:
                new_signal["timestamp"] = kwargs['timestamp']
        else:
            new_signal["timestamp"] = '\x00NOW()'

        # Get revision for the signal
        cursor = self.db.cursor()
        cursor.execute(("SELECT MAX(`revision`) from `data_signals` WHERE "
                        "`record_number` =  %i AND `generic_signal_id` =  %i "
                        "AND `variant` = '%s' ")
                       % (record_number, generic_signal_id, new_signal['variant']))
        row = cursor.fetchone()
        cursor.close()
        if row[0] is None:
            revision = 1
        else:
            if kwargs.get("ignore_if_exists"):
                return None
            revision = row[0] + 1
        new_signal["revision"] = revision

        signal_parameters['gs_parameters'] = ''
        if 'gs_parameters' in kwargs:
            signal_parameters['gs_parameters'] = kwargs['gs_parameters']
        else:
            gs_setup = self.get_signal_setup(generic_signal_id, record_number=record_number)
            if gs_setup:
                signal_parameters['gs_parameters'] = gs_setup['parameters']
        # enforce JSON paramaters
        if signal_parameters['gs_parameters'] or signal_parameters['daq_parameters']:
            for pkey in ('gs_parameters', 'daq_parameters'):
                if signal_parameters[pkey]:
                    if isinstance(signal_parameters[pkey], dict):
                        signal_parameters[pkey] = json.dumps(signal_parameters[pkey])
                    elif not self.is_json(signal_parameters[pkey]):
                        self.error('{0} is not a valid JSON string'.format(pkey))
                else:
                    # empty parameters will not be stored
                    signal_parameters[pkey] = ''

        # Insert and return signal reference
        for i in range(10):
            try:
                self.insert("data_signals", new_signal)
            except Exception as e:
                last_error = e
                # increase the revision number and wait 50 ms
                new_signal["revision"] += 1
                if kwargs.get("ignore_if_exists"):
                    return None
                sleep(0.05)
            else:
                sig_ref = self.get_signal_references(generic_signal_id=generic_signal_id, record_number=record_number,
                                                     revision=new_signal["revision"], variant=new_signal['variant'])
                break
        else:
            self.error('Cannot store the signal, {0}'.format(last_error))

        if len(sig_ref) != 1:
            self.error('Error retrieving the stored signal reference.')

        # Write signal parameters
        if signal_parameters['gs_parameters'] or signal_parameters['daq_parameters']:
            for pkey in ('gs_parameters', 'daq_parameters'):
                if not signal_parameters[pkey]:
                    # enforce valid empty JSON
                    signal_parameters[pkey] = '{}'
            signal_parameters['generic_signal_id'] = sig_ref[0]['generic_signal_id']
            signal_parameters['record_number'] = sig_ref[0]['record_number']
            signal_parameters['revision'] = sig_ref[0]['revision']
            signal_parameters['variant'] = sig_ref[0]['variant']
            self.insert('data_signal_parameters', signal_parameters)

        return sig_ref[0]

    @cdb_base_check
    def store_time_axis(self, record_number, time_axis_id=None, generic_signal_id=None, coefficient=1.0, offset=0.0):
        # TODO: I think it is not used any more
        if generic_signal_id:
            if time_axis_id:
                self.error("Parameters overspecified: provide either only time_axis_id or generic_signal_id, not both")
            gs_ref = self.get_generic_signal_references(generic_signal_id=generic_signal_id)[0]
            time_axis_id = gs_ref["time_axis_id"]
        generic_time_axis = self.get_generic_signal_references(generic_signal_id=time_axis_id)[0]
        # Try to find the signal for current record
        signals = self.get_signal_references(generic_signal_id=generic_time_axis["generic_signal_id"], record_number=record_number)
        # print "Time axis coefficient: ", coefficient
        if len(signals) == 0:
            # Store time axis
            self.store_signal(record_number=record_number, generic_signal_id=time_axis_id,
                              coefficient=coefficient, offset=offset, time0=0.0)
            return True
        else:
            return False

    @cdb_base_check
    def delete_signal(self, str_id='', **kwargs):
        '''Delete a signal

        :param str_id: string id of the signal
        :param kwargs: alternative signal id (signal_reference)

        kwargs - fields from data signal reference:

        :param timestamp:
        :param note:

        '''
        signal_refs = self.get_signal_references(str_id, **kwargs)

        if len(signal_refs) == 0:
            self.error("Signal not found.")
        elif len(signal_refs) > 1:
            self.error('Multiple signals found, delete_signal can delete only one.')
        else:
            signal_ref = signal_refs[0]

        if 'timestamp' in kwargs:
            if isintlike(kwargs['timestamp']):
                timestamp = datetime.datetime.fromtimestamp(kwargs['timestamp']).strftime('%Y-%m-%d %H:%M:%S')
            else:
                timestamp = kwargs['timestamp']
        else:
            timestamp = '\x00NOW()'
        record_number = signal_ref['record_number']
        variant = signal_ref['variant']
        generic_signal_id = signal_ref['generic_signal_id']
        cursor = self.db.cursor()
        cursor.execute(("SELECT MAX(`revision`) from `data_signals` WHERE " +
                       "`record_number` =  %i AND `generic_signal_id` =  %i AND `variant` = %s")
                       % (record_number, generic_signal_id, self.mysql_str(variant)))
        row = cursor.fetchone()
        cursor.close()
        if row[0] is None:
            # nothing to delete
            pass
        else:
            revision = row[0] + 1
            new_row = dict(signal_ref)
            if 'data_signal_id' in new_row:  # TODO remove "if" when schema is changed everywhere
                del new_row['data_signal_id']
            new_row.update({
                'generic_signal_id': generic_signal_id,
                'record_number': record_number,
                'revision': revision,
                'timestamp': timestamp,
                'deleted': 1,
                'note': kwargs.get("note", "")
            })
            self.insert("data_signals", new_row)

    @cdb_base_check
    def update_signal(self, str_id='', sig_ref=None, **kwargs):
        '''Create a new revision of a signal with some properties changed.

        Use ether str_id or sig_ref as signal identifier.

        :param str_id: signal string id
        :param sig_ref: signal reference (dict)
        :param kwargs: new values of the fields
        '''
        if str_id:
            sig_refs = self.get_signal_references(str_id=str_id)
        elif sig_ref:
            sig_refs = self.get_signal_references(**sig_ref)
        else:
            self.error('str_id or sig_ref must be provided')
        if not sig_refs:
            self._logger.warning('no signals found')
            return
        # alllow multiple record numbers
        last_sig_refs = {}  # last revision refs for each record number
        for sig_ref in sig_refs:
            if sig_ref['record_number'] in last_sig_refs:
                if last_sig_refs[sig_ref['record_number']]['revision'] < sig_ref['revision']:
                    last_sig_refs[sig_ref['record_number']] = sig_ref
                    last_sig_refs[sig_ref['record_number']].update(
                        self.get_signal_parameters(**sig_ref))
                    update_keys = last_sig_refs[sig_ref['record_number']].keys()
            else:
                last_sig_refs[sig_ref['record_number']] = sig_ref
                last_sig_refs[sig_ref['record_number']].update(
                    self.get_signal_parameters(**sig_ref))
                update_keys = last_sig_refs[sig_ref['record_number']].keys()
        # use kwargs that are part of the data signal reference
        change_attrs = set(kwargs.keys()) & set(update_keys)
        res = []
        for record_number, sig_ref in last_sig_refs.items():
            store_args = sig_ref
            # replace input parameters
            for attr in change_attrs:
                store_args[attr] = kwargs[attr]
            if 'data_signal_id' in store_args:  # TODO remove "if" when schema is changed everywhere
                del store_args['data_signal_id']
            del store_args['revision']
            del store_args['timestamp']
            generic_signal_id = sig_ref['generic_signal_id']
            del store_args['generic_signal_id']
            ref = self.store_signal(generic_signal_id=generic_signal_id, **store_args)
            res.append(ref)
        return tuple(res)

    # @cdb_base_check
    # def restore_default_generic_signal(self, computer_id, board_id, channel_id, record_number, delete=True, note=None):
    #     '''Reinterpret channel data as the default generic signal.
    #
    #     :param delete: If there is a data signal with non-default generic signal, mark it as deleted.
    #
    #     Note: Default is the original generic signal stored in DAQ_channels table,
    #         not the one defined in channel_setup.
    #     '''
    #     if isstringlike(computer_id):
    #         computer_id = self.get_computer_id(computer_name=computer_id)
    #
    #     channel = self.get_channel_references(computer_id=computer_id, board_id=board_id, channel_id=channel_id)
    #     default_gsid = channel["default_generic_signal_id"]
    #
    #     self.store_channel_data_as_signal(computer_id, board_id, channel_id, default_gsid,
    #                                       record_numbers=[record_number], delete=delete, note=note)

    @cdb_base_check
    def store_channel_data_as_signal(self, computer_id, board_id, channel_id,
                                     generic_signal_id, record_numbers,
                                     time_axis_id=None, time0=None,
                                     offset=None, coefficient=None, coefficient_V2unit=None,
                                     delete=True, note=None, enable_deleted=False, restore_default=True):
        '''Store data signal(s) from a particular DAQ channel as a given generic signal

        :param computer_id: numeric id or computer name
        :param board_id:
        :param channel_id:
        :param generic_signal_id:
        :param record_numbers: list (or any iterable) of record numbers (or a single record number)
        :param time_axis_id: time axis (generic signal) id, latest revision will be used
        :param time0: time0, None keeps the last value
        :param offset: set offset (in raw levels), None keeps the last value
        :param coefficient: DAQ levels to Volts coefficient, None keeps the last value
        :param coefficient_V2unit: DAQ Volts to units coefficient, None keeps the last value
        :param delete: delete the previous data signal
        :param enable_deleted: do it even if there is no generic signal that is not deleted.
        :param restore_default: if another data signal belongs to the g.s., it is restored to default g.s.
        '''
        assert channel_id is not None
        assert board_id is not None

        if isstringlike(computer_id):
            computer_id = self.get_computer_id(computer_name=computer_id)
        res = []
        try:
            iterator = iter(record_numbers)
        except TypeError:
            iterator = (record_numbers, )
        for record_number in iterator:
            refs = self.get_signal_references(computer_id=computer_id, board_id=board_id, channel_id=channel_id,
                                              record_number=record_number, revision=-1, deleted=enable_deleted)
            if refs:
                samedata = True
                if len(refs) > 1:
                    # TODO: some decision can be taken, e.g. if data file references are identical
                    samedata = len(set(ref['data_file_id'] for ref in refs)) == 1       # Note: set comprehension selects unique keys
                    samedata = len(set(ref['data_file_key'] for ref in refs)) == 1
                    if samedata:
                        self._logger.info('multiple data signals found for record_number=%i, computer_id=%i, '
                                          'board_id=%i, channel_id=%i,' % (record_number, computer_id, board_id, channel_id))
                        self._logger.info('the data files and data sets are identical, using to store the new signal')
                    else:
                        self._logger.error('multiple data signals found for record_number=%i, computer_id=%i, '
                                           'board_id=%i, channel_id=%i,' % (record_number, computer_id, board_id, channel_id))
                        self._logger.error("don't know what to do")
                if samedata:
                    # Delete all stored signals (but only if other gs is used)
                    if delete:
                        to_delete = [ ref for ref in refs if not ref["deleted"] and ref["generic_signal_id"] != generic_signal_id ]
                        for ref in to_delete:
                            self.delete_signal(signal_ref=ref, record_number=record_number, note="Reinterpreted as generic signal ID={0}".format(generic_signal_id))

                    # Remove generic signal from where it was originally and restore the default one there.
                    if restore_default:
                        signals = self.get_signal_references(record_number=record_number, generic_signal_id=generic_signal_id, revision=-1)
                        if signals:
                            signal = signals[0]
                            if not (signal["computer_id"] == computer_id and signal["board_id"] == board_id
                                    and signal["channel_id"] == channel_id):
                                self.delete_signal(record_number=record_number, generic_signal_id=generic_signal_id,
                                                   revision = signal["revision"])
                                channel = self.get_channel_references(computer_id=signal["computer_id"],
                                                                      board_id=signal["board_id"],
                                                                      channel_id=signal["channel_id"])[0]
                                default_gsid = channel["default_generic_signal_id"]
                                self.store_channel_data_as_signal(signal["computer_id"], signal["board_id"],
                                                                  signal["channel_id"], default_gsid,
                                                                  record_numbers=[record_number], enable_deleted=True)

                    kwargs = refs[0]
                    if 'data_signal_id' in kwargs:   # TODO remove "if" when schema is changed everywhere
                        del kwargs['data_signal_id']
                    del kwargs['note']
                    del kwargs['deleted']
                    del kwargs['revision']
                    del kwargs['timestamp']
                    del kwargs['generic_signal_id']
                    for k in ['time_axis_id', 'time0', 'offset', 'coefficient', 'coefficient_V2unit', 'note']:
                        if eval(k) is not None:
                            kwargs[k] = eval(k)
                    newref = self.store_signal(generic_signal_id=generic_signal_id, **kwargs)
                    res.append(newref)
            else:
                self._logger.info('No data signal found for record_number=%i, computer_id=%i, '
                                  'board_id=%i, channel_id=%i,' % (record_number, computer_id, board_id, channel_id))

        return tuple(res)

    @cdb_base_check
    def set_file_ready(self, data_file_id, chmod_ro=True):
        '''Set file_ready to True

        :param data_file_id: data file id
        :param chmod_ro: make the file read-only (strongly recommended)
        '''
        fref = self.get_data_file_reference(data_file_id=data_file_id)
        fname = fref.get('full_path',None)
        if not fname:
            self.error("set_file_ready(): empty filename in data_file_id=%d. You are probably trying to set ready non existing file - not doing anything." % data_file_id)
            return
        new_row = {'data_file_id': data_file_id, 'file_ready': 1}
        self.insert("file_status", new_row)
        if chmod_ro:
            if fname and os.path.exists(fname):
                os.chmod(fref['full_path'], 292)  # 292 = 0o444, Jython does not support 0o

    @cdb_base_check
    def is_file_ready(self, data_file_id):
        '''Set file_ready to True
        '''

        cursor = self.db.cursor()
        cursor.execute('SELECT `file_ready` from `file_status` WHERE `data_file_id`=%i' % data_file_id)
        file_ready = False
        if cursor.rowcount == 1:
            file_ready = bool(cursor.fetchone()[0])
        cursor.close()
        return file_ready

    @cdb_base_check
    def get_generic_signal_id(self, generic_signal_name, data_source_id=None):
        '''Returns generic signal id given the signal name'''

        cursor = self.db.cursor()
        sql_query = "SELECT `generic_signal_id` FROM `generic_signals` WHERE `generic_signal_name`='%s'" % generic_signal_name
        if data_source_id is not None:
            sql_query += " AND `data_source_id`=%i" % data_source_id
        cursor.execute(sql_query)
        if cursor.rowcount > 1:
            self.error("ambiguous signal name")
        elif cursor.rowcount == 0:
            self.error("signal not found")
        else:
            row = cursor.fetchone()
            res = row[0]
        cursor.close()
        return res

    @cdb_base_check
    def record_exists(self, record_number=None, **kwargs):
        '''Returns True if a record exists, False otherwise.

        :param kwargs: named parameters interpretable by resolve_record_number (see)
        :rtype: bool
        '''

        if record_number is None:
            record_number = self.resolve_record_number(kwargs)
            if record_number is None:
                self.error("cannot resolve record number")

        rows = self.query("SELECT `record_number` FROM shot_database WHERE `record_number`=%i" % record_number)
        return len(rows) > 0

    @cdb_base_check
    def get_record_datetime(self, **kwargs):
        '''Return record datetime (timestamp)

        kwargs:
        :param record_number: record number
        '''

        record_number = self.resolve_record_number(kwargs)
        if record_number is None:
            self.error("cannot resolve record number")
        sql = "SELECT * FROM shot_database WHERE `record_number`=%i" % record_number
        result = self.query(sql, error_if_empty=True)
        return result[0]['record_time']

    @cdb_base_check
    def get_record_info(self, record_number: int = -1, record_type: RecordType = 'EXP') -> dict:
        '''Return record information.

        :param record_number: Record number of -1 for the last.
        :param record_type: Record type in case record_number is -1.
        :return Record information dictionary or exception if not found:

            * record_number: Record number (int).

            * record_type: Record type (RecordType).

            * record_time: Record time (datetime).

            * description: Record description (str).
        '''
        record_number = self.resolve_record_number({
            "record_number": record_number,
            "record_type": record_type
        })
        if record_number is None:
            self.error("cannot resolve record number (record_number={}, record_type={})".format(
                record_number,
                record_type
            ))
        sql = "SELECT * FROM shot_database WHERE `record_number`=%i" % record_number
        result = self.query(sql, error_if_empty=True)
        return result[0]

    @cdb_base_check
    def get_channel_references(self, computer_name=None, computer_id=None, board_id=None, channel_id=None):
        '''Get DAQ channel reference(s)'''

        sql_query = "SELECT * FROM `DAQ_channels` WHERE 1"
        if computer_name is not None:
            computer_id = self.get_computer_id(computer_name=computer_name)
            sql_query += " AND computer_id=%i" % computer_id
        if computer_id is not None:
            sql_query += " AND computer_id=%i" % computer_id
        if board_id is not None:
            sql_query += " AND board_id=%i" % board_id
        if channel_id is not None:
            sql_query += " AND channel_id=%i" % channel_id
        return self.query(sql_query)

    @cdb_base_check
    def FS2CDB_ref(self, nodeuniqueid, hardwareuniqueid, parameteruniqueid):
        '''Return the CDB DAQ_channel reference of a FireSignal channel'''

        sql_query = ("SELECT * FROM `DAQ_channels` WHERE "
                     "`nodeuniqueid`='%s' AND `hardwareuniqueid`='%s' AND `parameteruniqueid`='%s'"
                     % (nodeuniqueid, hardwareuniqueid, parameteruniqueid))
        return self.query(sql_query, error_if_empty=True)[0]

    @cdb_base_check
    def CDB2FS_ref(self, computer_id, board_id, channel_id):
        '''Return the CDB DAQ_channel reference of a CDB channel'''

        sql_query = ("SELECT * FROM `DAQ_channels` WHERE "
                     "`computer_id`='%s' AND `board_id`='%s' AND `channel_id`='%s'"
                     % (computer_id, board_id, channel_id))
        return self.query(sql_query, error_if_empty=True)[0]

    @cdb_base_check
    def get_computer_id(self, computer_name=None, description=None):
        '''Returns computer ID by computer name or description
        '''
        if not (computer_name or description):
            self.error("You have to specify computer_name and/or description")
        sql_query = 'SELECT * FROM `da_computers` WHERE 1'
        if computer_name is not None:
            sql_query += " AND `computer_name`='%s'" % computer_name
        if description is not None:
            sql_query += " AND `description`='%s'" % description
        return self.query(sql_query, error_if_empty=True)[0]['computer_id']

    @cdb_base_check
    def get_attachment_table(self, timestamp=None, computer_id=None, board_id=None, channel_id=None,
                             generic_signal_id=None, record_number=None):
        '''Returns tuple of rows from a "view" corresponding to channel attachments
        based on specified criteria.

        :param timestamp: reference time (default current)
        :param computer_id: DAQ computer id
        :param board_id: DAQ board id
        :param channel_id: DAQ channel id
        :param generic_signal_id: attached generic signal id
        :param record_number: timestamp of this record number will be used
        '''

        if isstringlike(computer_id):
            computer_id = self.get_computer_id(computer_name=computer_id)
        if record_number is not None:
            timestamp = self.get_record_datetime(record_number=record_number)
        if timestamp is None:
            timestamp = '\x00NOW()'
        elif isintlike(timestamp):
            timestamp = datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(timestamp, str):
            pass
        elif not isinstance(timestamp, (datetime.datetime,)):
            self.error('wrong timestamp format')
        timestamp = str(timestamp)
        if timestamp[0] == '\x00':
            # MySQL function --> no quotes
            timestamp = timestamp[1:]
        else:
            timestamp = "'" + timestamp + "'"
        sqlquery = """
        SELECT * FROM (
        SELECT DAQ_channels.computer_id,DAQ_channels.board_id,DAQ_channels.channel_id,
        DAQ_channels.default_generic_signal_id,
        t3.attached_generic_signal_id,t3.attach_time,
        generic_signals.generic_signal_name AS attached_signal_name,
        DAQ_channels.default_generic_signal_id<>t3.attached_generic_signal_id AS is_attached,
        t3.offset, t3.coefficient_lev2V, t3.coefficient_V2unit, t3.uid, t3.note, t3.time_axis_id, t3.parameters
        FROM (
        SELECT    t1.*
        FROM      channel_setup AS t1
                  INNER JOIN (
                      SELECT computer_id,board_id,channel_id, MAX(attach_time) AS attach_time
                      FROM  (
                            SELECT computer_id,board_id,channel_id,attach_time FROM channel_setup
                            WHERE attach_time <= %s
                            ) AS t11
                      GROUP BY  computer_id,board_id,channel_id
                  ) AS t2 ON t2.computer_id = t1.computer_id
                    AND t2.board_id = t1.board_id
                    AND t2.channel_id = t1.channel_id
                    AND t2.attach_time = t1.attach_time
        ) AS t3, DAQ_channels, generic_signals
        WHERE DAQ_channels.computer_id=t3.computer_id
        AND DAQ_channels.board_id=t3.board_id
        AND DAQ_channels.channel_id=t3.channel_id
        AND generic_signals.generic_signal_id=t3.attached_generic_signal_id
        ) AS t4 WHERE 1""" % timestamp
        if channel_id is not None:
            sqlquery += " AND channel_id=%i" % channel_id
        if board_id is not None:
            sqlquery += " AND board_id=%i" % board_id
        if computer_id is not None:
            sqlquery += " AND computer_id=%i" % computer_id
        if generic_signal_id is not None:
            sqlquery += " AND attached_generic_signal_id=%i" % generic_signal_id
        sqlquery += ";"
        return self.query(sqlquery)

    @cdb_base_check
    def get_signal_setup(self, gs_str_id, record_number=None, timestamp=None):
        '''Get signal setup for a generic signal

        :param gs_str_id: generic signal alias, name or numeric id
        '''
        gs_refs = self.get_generic_signal_references(gs_str_id)
        if len(gs_refs) == 0:
            self.error('generic signal not found')
        if len(gs_refs) > 1:
            self.error('generic signal id is not unique')
        gs_id = gs_refs[0]['generic_signal_id']
        if record_number is not None:
            timestamp = self.get_record_datetime(record_number=record_number)
        if timestamp is None:
            timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        elif isintlike(timestamp):
            timestamp = datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(timestamp, str):
            pass
        elif not isinstance(timestamp, (datetime.datetime,)):
            self.error('wrong timestamp format')
        timestamp = str(timestamp)
        if timestamp[0] == '\x00':
            # MySQL function --> no quotes
            timestamp = timestamp[1:]
        else:
            timestamp = "'" + timestamp + "'"
        sqlquery = ("SELECT * FROM `signal_setup` WHERE `generic_signal_id`=%i "
                    "AND `timestamp`<=%s ORDER BY `timestamp` DESC LIMIT 1" % (gs_id, timestamp))
        rows = self.query(sqlquery)
        if rows:
            res = rows[0]
        else:
            res = None
        return res

    @cdb_base_check
    def get_signal_references(self, str_id='', deleted=False, limit=10000, dereference=False, **kwargs):
        '''Get signal references by specified criteria

        :param str_id: signal string id (see :func:`decode_signal_strid`)
        :param deleted: return or not deleted references for revision=-1
        :param limit: limit the number of results
        :param dereference: return explicit gs_ref, data_file_ref and data_source_ref

        keyword arguments offer an alternative to str_id:
        :param record_number: record number
        :param revision: revision number indexed from 1, use negative number to index from the last revision
                         (e.g. -1 for the last revision, -2 for the second last revision, etc.)
        :param signal_ref: signal reference
        :param signal_reference: the same as signal_ref
        '''
        # Temporary storage for original record number in case of wildcard generic signal
        original_record_number = None
        if 'signal_reference' in kwargs:
            kwargs['signal_ref'] = kwargs['signal_reference']
        if 'signal' in kwargs:
            kwargs['signal_ref'] = kwargs['signal'].ref
        if 'signal_ref' in kwargs:
            # Return copy as the object is often manipulated with
            return (dict(kwargs['signal_ref']), )

        sql_query = "SELECT * FROM data_signals WHERE ("
        if str_id:
            iddict = decode_signal_strid(str_id)
            # all str id types contain record numbers
            # record_number = iddict['record_number'] if 'record_number' in iddict else self.resolve_record_number(kwargs)
        elif ('generic_signal_id' in kwargs):
            # give generic signal id a preference
            iddict = kwargs
            iddict['record_number'] = self.resolve_record_number(kwargs)
            iddict['id_type'] = 'CDB'
        elif ('nodeuniqueid' in kwargs) and ('hardwareuniqueid' in kwargs) and ('parameteruniqueid' in kwargs):
            # backward compatibility, use str_id
            # channel = self.FS2CDB_ref(kwargs['nodeuniqueid'], kwargs['hardwareuniqueid'], kwargs['parameteruniqueid'])
            iddict = {}
            iddict['id_type'] = 'FS'
            iddict['nodeuniqueid'] = kwargs['nodeuniqueid']
            iddict['hardwareuniqueid'] = kwargs['hardwareuniqueid']
            iddict['parameteruniqueid'] = kwargs['parameteruniqueid']
            iddict['record_number'] = self.resolve_record_number(kwargs)
            if 'revision' in kwargs:
                iddict['revision'] = kwargs['revision']
            if 'variant' in kwargs:
                iddict['variant'] = kwargs['variant']
        elif (('computer_id' in kwargs) or ('computer_name' in kwargs)):
            iddict = {}
            iddict['id_type'] = 'DAQ'
            if 'computer_name' in kwargs:
                kwargs['computer_id'] = kwargs['computer_name']
            iddict['computer_id'] = kwargs['computer_id']
            if 'board_id' in kwargs:
                iddict['board_id'] = kwargs['board_id']
                if 'channel_id' in kwargs:
                    iddict['channel_id'] = kwargs['channel_id']
            iddict['record_number'] = self.resolve_record_number(kwargs)
            if 'revision' in kwargs:
                iddict['revision'] = kwargs['revision']
            if 'variant' in kwargs:
                iddict['variant'] = kwargs['variant']
        else:
            iddict = kwargs
            iddict['record_number'] = self.resolve_record_number(kwargs)
            iddict['id_type'] = 'CDB'
        if iddict['record_number'] == -1:
            iddict['record_number'] = self.last_record_number()

        if iddict['id_type'] == 'CDB':
            # get generic signal references by whatever is on the input (str_id or kwargs)
            generic_signal_refs = self.get_generic_signal_references(**iddict)
            # no references if no generic signal found
            if len(generic_signal_refs) == 0:
                self._logger.warning('no generic signal references found')
                return ()
                # this would get all signal references
                # sql_query += '1'
            for iref, generic_signal_ref in enumerate(generic_signal_refs):
                # mangle the record number when generic signal is wildcard:
                if generic_signal_ref.get("wildcard",0) == 1 and self.record_exists(iddict['record_number']) and iddict['record_number'] != original_record_number:
                    original_record_number = iddict['record_number']
                    iddict['record_number'] = _wildcard_shot_number_
                # test for first reference
                if iref > 0:
                    sql_query += ' OR'
                sql_query += ' (1'
                sql_query += " AND `generic_signal_id`=%i" % generic_signal_ref['generic_signal_id']
                if 'revision' in iddict and iddict['revision'] > 0:
                    sql_query += " AND `revision`=%i" % iddict['revision']
                if iddict.get('variant') is not None:
                    sql_query += " AND `variant`='%s'" % iddict['variant']
                sql_query += ')'
            sql_query += ')'
            if iddict['record_number'] is not None:
                sql_query += " AND `record_number`=%i" % iddict['record_number']
            sql_query += " ORDER BY `record_number`, `generic_signal_id`, `variant`, `revision`"
        elif iddict['id_type'] == 'FS' or iddict['id_type'] == 'DAQ':
            # Revision number is valid for generic signals, here it's arbitrary (and wrong)
            if 'revision' in iddict and iddict["revision"] != -1:
                self.error("Cannot search for a numbered revision of a channel-specified signal.")
            sql_query += ' (1'
            if iddict['id_type'] == 'FS':
                # translate FS to CDB DAQ
                iddict.update(self.FS2CDB_ref(iddict['nodeuniqueid'],
                                              iddict['hardwareuniqueid'],
                                              iddict['parameteruniqueid']))
            # string computer_id = computer_name
            if isstringlike(iddict['computer_id']):
                iddict['computer_id'] = self.get_computer_id(computer_name=iddict['computer_id'])
            # this is the preferred option
            sql_query += " AND `computer_id`=%i " % iddict['computer_id']
            if 'board_id' in iddict:
                sql_query += " AND `board_id`=%i " % iddict['board_id']
                if 'channel_id' in iddict:
                    sql_query += " AND `channel_id`=%i " % iddict['channel_id']
            if iddict.get('variant') is not None:
                sql_query += " AND `variant`='%s'" % iddict['variant']
            sql_query += ')'
            sql_query += ')'
            if iddict['record_number']:
                sql_query += " AND `record_number`=%i" % iddict['record_number']
            sql_query += " ORDER BY `record_number`, `generic_signal_id`, `revision`"
        else:
            self.error('error retrieving CDB signal id')
        sql_query += ' LIMIT %i' % (limit+1)
        rows = self.query(sql_query)
        if len(rows) > limit:
            self.error('Number of references is over the limit: increase the limit or change the search')
        self._logger.debug('rows fetched = %i' % len(rows))
        if rows and len(rows) > 1 and 'revision' in iddict and iddict['revision'] <= -1:
            res = self.get_last_revisions(rows, int(iddict["revision"]), deleted=deleted)
        else:
            res = rows
        if 'revision' in iddict and iddict['revision'] == -1 and not deleted:
            res = filter(lambda x: not bool(x['deleted']), res)
        if dereference:
            # dereference generic signal and data file
            dereferenced = []
            for ref in res:
                _ref = ref
                _ref['gs_ref'] = self.get_generic_signal_references(generic_signal_id=ref['generic_signal_id'])[0]
                _ref['file_ref'] = self.get_data_file_reference(data_file_id=ref['data_file_id'])
                _ref['data_source_ref'] = self.get_data_source_references(data_source_id=ref['gs_ref']['data_source_id'])[0]
                dereferenced.append(_ref)
            res = tuple(dereferenced)
        else:
            res = tuple(res)
        # optionally unmangle the record number
        for ref in res:
            if original_record_number is not None and 'record_number' in ref:
                ref['record_number'] = original_record_number
        return res

    @classmethod
    def get_last_revisions(cls, signals, idx=-1, deleted=False):
        '''From a set of signals, take only those that have ith last revision number for each combination of (record_number, generic_signal_id, variant).

        :param signals: List of signals with ['record_number', 'generic_signal_id', 'variant', 'revision'] fields.
        :param idx: Negative index of the revision to take - similar to Python list indexing. Throws ValueError if idx >= 0.
                    Default -1 points to the last revision.

        :param deleted: If true, keep deleted, otherwise filter them out.
        '''
        if idx >= 0:
            raise ValueError("idx must be negative")

        max_len = -idx

        res = []
        sortfunc = lambda signal: signal["revision"]
        keyfunc = lambda signal: (signal["record_number"], signal["generic_signal_id"], signal["variant"])
        iterator = sorted(signals, key=keyfunc)

        for key, grouper in itertools.groupby(iterator, keyfunc):
            signals = list(sorted(grouper, key=sortfunc))

            if len(signals) < max_len:
                logger = logging.getLogger('pyCDB')
                logger.warning("Cannot get %d-th last revision for %s, skipping" % (-idx, key))
                continue

            signal = signals[idx]
            if deleted or not signal["deleted"]:
                res.append(signal)
        return res

    @cdb_base_check
    def get_signal_calibration(self, str_id=None, sig_ref=None, units=None, gs_ref=None):
        '''Calculate signal offset and coefficient based on signal reference

        :param str_id: signal string id
        :param sig_ref: signal reference
        :param units: signal units, None uses default

        The calibration transformations is: (data + offset) * coefficient

        If str_id is input, all the references are fetched from the database.
        Note: Applies only to FILE signals, LINEAR cannot be transformed this way
        '''

        if str_id:
            # override all other inputs
            str_id_dict = decode_signal_strid(str_id)
            units = str_id_dict['units']
            sig_ref = self.get_signal_references(str_id)
            if len(sig_ref) == 1:
                sig_ref = sig_ref[-1]
            else:
                self.error("ambiguous signal str_id in get_signal_calibration")
        if not sig_ref:
            # we must have sig_ref now
            self.error("signal not found")
        if units is None:
            units = "default"

        res = {"unit_system": None}
        # check units
        if units.lower() == 'raw':
            res['offset'] = 0
            res['coefficient'] = 1
            res['units'] = 'RAW'
        elif units.lower() == 'dav':
            res['offset'] = sig_ref['offset']
            res['coefficient'] = sig_ref['coefficient']
            res['units'] = 'DAV'
        else:
            res['offset'] = sig_ref['offset']
            res['coefficient'] = sig_ref['coefficient'] * sig_ref['coefficient_V2unit']
            if not gs_ref:
                gs_ref = self.get_generic_signal_references(generic_signal_id=sig_ref['generic_signal_id'])[-1]
            res['units'] = str(gs_ref['units'])
            if units is not None and units.lower() not in ('default', res['units'].lower()):
                self.error('units are not implemented yet, using DEFAULT units')
            if not units or units in ('default', gs_ref['units']):
                res['units'] = str(gs_ref['units'])
            else:
                from . import units as u
                unit_system = self.get_unit_system(unit_system_name=units)
                if unit_system:
                    res["unit_system"] = unit_system
                    dims = u.get_dimension(gs_ref['units'])
                    unit = self.get_unit(unit_system=unit_system, **dims)
                    if unit:
                        units = unit["unit_name"]
                    else:
                        units = gs_ref["units"]
                res['units'] = units
                res['coefficient'] = u.convert_units(gs_ref['units'], units) * res['coefficient']
        return res

    def get_signal_base_tree(self, signal_ref):
        '''Create a reference tree of dependent signals.

        :param signal_ref: A valid signal reference

        It reads parameters from the database and sets some signal properties as well.
        '''
        signal = CDBSignal()
        signal.ref = signal_ref
        signal.gs_ref = self.get_generic_signal_references(generic_signal_id=signal_ref['generic_signal_id'])[0]
        signal.name = signal.gs_ref['generic_signal_name']
        signal.description = signal.gs_ref['description']
        if signal_ref["data_file_id"]:
            signal.file_ref = self.get_data_file_reference(data_file_id=signal_ref["data_file_id"])

        for axis_name in self.__axes_names:
            axis_field = axis_name + '_id'
            if (axis_field in signal.gs_ref and signal.gs_ref[axis_field]) or \
                    (axis_field in signal.ref and signal.ref[axis_field]):
                if axis_name == 'time_axis' and signal.ref['time_axis_id'] is not None:
                    axid = signal.ref['time_axis_id']
                else:
                    axid = signal.gs_ref[axis_field]
                ax_gen_ref = self.get_generic_signal_references(generic_signal_id=axid)[0]
                ax_sig_ref = self.get_signal_references(generic_signal_id=axid,
                                                        record_number=signal_ref['record_number'],
                                                        revision=signal_ref[axis_name + '_revision'],
                                                        variant=signal_ref[axis_name + '_variant'])
                if len(ax_sig_ref) != 1:
                    self.error('Cannot get signal axis %s.' % axis_name)
                axis = self.get_signal_base_tree(ax_sig_ref[0])
                # if axis_name != "time_axis":
                signal.axes.append(axis)
                signal.__setattr__(axis_name, axis)

        parameters = self.get_signal_parameters(signal_ref=signal_ref)
        signal.gs_parameters = parameters.gs_parameters
        signal.daq_parameters = parameters.daq_parameters

        return signal

    def get_unit_factor_tree(self, signal, units=None, unit_system=None):
        '''Get tree of factors and unit names for a signal object.

        :param signal: CDBSignal + its axes signals in a tree
        :param units: name of the unit or unit system to convert to
        :param unit_system: unit system to convert to (takes precedence)

        Works recursively but only unit system is passed along the recursion.
        Example return value: { 'units': 'mm', 'factor' : 0.001}
        '''
        from . import units as u
        tree = OrderedDict()
        original_units = signal.gs_ref['units']
        dim = u.get_dimension(original_units)
        if not unit_system:
            if units:
                unit_system = self.get_unit_system(units)
        if unit_system:
            units = self.get_unit(unit_system=unit_system, **dim)
            if units:
                units = units['unit_name']
            elif [si_u for si_u in dim if dim[si_u] != 0 ]:
                # Non-dimensional data don't need to have dimension
                raise Exception("Unit not defined in the system.")
        if not units or units == 'default':
            units = original_units
        tree['factor'] = u.convert_units(original_units, units)
        tree['units'] = units
        for axis_name in self.__axes_names:
            if hasattr(signal, axis_name) and getattr(signal, axis_name):
                axis = getattr(signal, axis_name)
                # Recursion
                tree[axis_name] = self.get_unit_factor_tree(axis, unit_system=unit_system)
        return tree

    def apply_unit_factor_tree(self, signal, unit_tree):
        '''Apply unit conversion of signal data tree.

        :param signal: a CDBSignal object with dependent axes and fully read data.
        :param unit_tree: a tree of units to convert to as returned by get_unit_factor_tree.
        '''
        signal.data = signal.data * unit_tree['factor']
        signal.units = unit_tree['units']
        for axis_name in self.__axes_names:
            if hasattr(signal, axis_name) and getattr(signal, axis_name):
                axis = getattr(signal, axis_name)
                self.apply_unit_factor_tree(axis, unit_tree[axis_name])

    @cdb_base_check
    def get_signal(self, str_id=None, squeeze=True, deleted=False, dtype=None, **kwargs):
        '''Get signal information and data.

        :param str_id: signal string id, see decode_signal_strid
        :param squeeze: remove singleton dimensions in the received data
        :param deleted: return or not deleted signals (this will not work anyway ...)
        :param dtype: force a certain numpy data type

        Keyword parameters:
        :param units: raw / dav / default (case insensitive, conflict with str_id)
        :param signal_ref: signal reference
        :param generic_signal_ref: generic signal reference
        :param n_samples: number of samples
        :param time_limit: time limit (instead of n_samples)
        :param slice_ standard Python slice
        :rtype: CDBSignal instance

        If str_id argument is not present, keyword arguments are used in the search.
        '''

        if 'n_samples' in kwargs and 'time_limit' in kwargs:
            self.error("Cannot specify n_samples and time_limit simultaneously.")
        if 'n_samples' in kwargs and 'slice_' in kwargs:
            self.error("Cannot specify n_samples and slice_ simultaneously.")
        if str_id and 'units' in kwargs:
            self.error("Cannot specify units and str_id simultaneously.")

        if str_id:
            decoded_str_id = decode_signal_strid(str_id)
            units = decoded_str_id['units']
            decoded_slice = decoded_str_id['slice']
        else:
            if 'units' in kwargs:
                units = kwargs['units']
            else:
                units = 'default'
            # default revision = -1 like with str_id
            if 'revision' not in kwargs:
                kwargs['revision'] = -1
            kwargs['variant'] = kwargs.get('variant', '')
            decoded_slice = Ellipsis

        if str_id:
            signal_refs = self.get_signal_references(str_id, deleted=True)
        else:
            signal_refs = self.get_signal_references(**kwargs)

        if len(signal_refs) == 0:
            self.error("signal not found")
        elif len(signal_refs) > 1:
            self.error('multiple signals found, get_signal can read only one')

        signal_ref = signal_refs[0]
        if not deleted and signal_ref['deleted']:
            self.error('data signal has been deleted')

        signal = self.get_signal_base_tree(signal_ref)

        if decoded_slice != Ellipsis and "slice_" in kwargs:
            raise self.error("Slices can not be specified in string_id and slice_ parameter simultaneously.")
        if decoded_slice != Ellipsis:
            slice_ = decoded_slice
        else:
            slice_ = kwargs.get("slice_", Ellipsis)

        if 'time_limit' in kwargs:
            if slice_ != Ellipsis:
                self.error("Cannot specify slice and time_limit simultaneously.")
            # This should be safe, factor (1.+1.e-15) is present in order to maintain compatibility
            slice_ = (slice(None,float(kwargs['time_limit'])*(1.+1.e-15)*1j),Ellipsis)

        # Read all data
        n_samples = kwargs.get('n_samples')
        self.get_signal_data_tree(signal, units=units,
                                  n_samples=n_samples, squeeze=squeeze, slice_=slice_, dtype=dtype)

        if units.lower() not in ('raw', 'dav', 'default'): # Only for units / unit systems
            unit_factor_tree = self.get_unit_factor_tree(signal, units)
            self.apply_unit_factor_tree(signal, unit_factor_tree)
        return signal

    def get_signal_data_tree(self, signal, units='default', n_samples=None,
                             squeeze=True, x0=None, slice_=Ellipsis, is_this_axis=False, dtype=None):
        '''Recursively read data for signal and all its axes.

        :param signal: the signal object to set data to.
        :param x0: additive constant
        '''
        from .slicing import fill_ellipsis, is_slice_complex, translate_complex_slice, is_slice_reducing
        import numpy as np

        signal_type = signal.gs_ref["signal_type"]
        axis_names = [ax for ax in self.__axes_names if hasattr(
            signal, ax) and getattr(signal, ax)]

        # Set units - only RAW/DAV/DEFAULT
        if units and not units.lower() in ('raw', 'dav'):
            units = 'default'
            signal.units = signal.gs_ref["units"]
        else:
            signal.units = units.lower()
        # Set slices
        slices = fill_ellipsis(slice_, signal.ndim)
        if is_this_axis:
            if isinstance(slices, Iterable):
                slices = len(slices)*(slice(None),)
            else:
                slices = slice(None)
        if signal_type == 'FILE':
            file_ref = signal.file_ref
            signal_ref = signal.ref
            if not file_ref["file_ready"]:
                self.error("file not ready for reading")
            self._logger.info(
                "Full path %s; Data file key %s" % (file_ref['full_path'], signal_ref['data_file_key']))
            if not file_ref['full_path']:
                # file not found
                self.error('Data file not found in current CDB_DATA_ROOT directories')
            try:
                unsliced_shape = get_h5_dataset_shape(
                    file_ref['full_path'], signal_ref['data_file_key'])
            except Exception as ex:
                self.error(ex)
            translated_slices = []
            for i, axis_name in enumerate(axis_names):
                if isinstance(slices, Iterable):
                    if i < len(slices) :
                        sl = slices[i]
                    else:
                        sl = slice(None)
                else:
                    sl = slices
                # if the slice is complex, use slice(None) instead
                sl_old = None
                if is_slice_complex(sl):
                    sl_old, sl = sl, Ellipsis
                axis = getattr(signal, axis_name)
                axis_samples = unsliced_shape[i]
                if axis_name == 'time_axis':
                    self.get_signal_data_tree(
                        signal=axis, n_samples=axis_samples,
                        x0=signal.ref["time0"], squeeze=squeeze, slice_=sl, is_this_axis=True)
                else:
                    if hasattr(axis, "time_axis"):
                        sl = (translated_slices[0], sl)
                    self.get_signal_data_tree(signal=axis,
                                              n_samples=axis_samples,
                                              squeeze=squeeze,
                                              slice_=sl,
                                              is_this_axis=True)
                if sl_old:
                    ts = translated_slices[0] if translated_slices else slice(None)
                    if hasattr(axis, "time_axis"):
                        sl = translate_complex_slice(sl_old, axis.data[ts, ...].squeeze())
                    else:
                        sl = translate_complex_slice(sl_old, axis.data)
                    if hasattr(axis, "time_axis") and axis.data.ndim > 1:
                        sl = (translated_slices[0], sl)
                if axis.data.shape:
                    axis.data = axis.data[sl]
                if isinstance(sl, Iterable):
                    sl = sl[-1]
                translated_slices.append(sl)
                if hasattr(axis, "time_axis"):
                    axis.time_axis.data = axis.time_axis.data[translated_slices[0]]
            if not isinstance(slices, Iterable):
                slices = (slices,)
            translated_slices = tuple(translated_slices)
            signal_data = self.get_signal_data(signal=signal,
                                               n_samples=n_samples,
                                               squeeze=squeeze,
                                               slice_=tuple(translated_slices),
                                               is_this_axis=is_this_axis)
            calibration = self.get_signal_calibration(sig_ref=signal.ref,
                                                      units=units, gs_ref=signal.gs_ref)
            data = apply_calibration(signal_data, calibration)
        elif signal_type == 'LINEAR':
            if axis_names:  # Dependent => recursion
                axis_name = axis_names[0]
                axis = getattr(signal, axis_name)

                if axis_name == "time_axis":
                    self.get_signal_data_tree(signal=axis, n_samples=n_samples,
                                              x0=signal.ref['time0'], squeeze=squeeze)
                else:
                    self.get_signal_data_tree(
                        signal=axis, n_samples=n_samples, squeeze=squeeze)
                if not hasattr(axis.data, 'shape'):
                    self.error(
                        "Cannot get number of samples for dependent linear axis.")

                data_f = self.get_signal_data(signal=signal)  # A lambda
                data = data_f(axis.data)[slices]
            else:  # End of recursion
                data = self.get_signal_data(
                    signal=signal, n_samples=n_samples, x0=x0, squeeze=squeeze)[slices]
        if squeeze:
            data = np.squeeze(data)
        if dtype is not None:
            data = np.asanyarray(data)
        signal.data = data

    @cdb_base_check
    def get_signal_data(self, signal_ref=None, generic_signal_ref=None, signal=None, squeeze=True, raw=False, **kwargs):
        '''Get signal data.

        :param squeze: Reduce N... x 1 x...M data_sets into N... x ...M (numpy squeeze function).

        For FILE data signals, it returns the data without any modifications (apart from squeeze or slicing).
        For LINEAR data signals, it returns:
        - if n_samples is set: an array of linear values
        - else: a linear function

        kwargs:

        :param x0: if set, it is used instead of signal_ref["offset"]
        Parameters:
        time_limit, n_samples
        x0,x1 - axis limits
        raw - if True, data won't be scaled
        '''

        import numpy as np

        if signal is not None:
            generic_signal_ref = signal.gs_ref
            signal_ref = signal.ref

        if generic_signal_ref is None:
            generic_signal_ref = self.get_generic_signal_references(generic_signal_id=signal_ref['generic_signal_id'])[0]

        slice_ = kwargs.get("slice_", Ellipsis)
        if generic_signal_ref['signal_type'] == 'FILE':
            if signal:
                file_ref = signal.file_ref
            else:
                file_ref = self.get_data_file_reference(data_file_id=signal_ref['data_file_id'])
            if not file_ref["file_ready"]:
                self.error("file not ready for reading")
            self._logger.info("Full path " + file_ref['full_path'] + "; Data file key " + signal_ref['data_file_key'])
            try:
                signal_data = read_file_data(file_ref['full_path'], signal_ref['data_file_key'], slice_=slice_)
            except Exception as ex:
                self.error(ex)
            if squeeze:
                signal_data = np.squeeze(signal_data)
        elif generic_signal_ref['signal_type'] == 'LINEAR':
            if kwargs.get('x0') is not None:
                x0 = kwargs['x0']
            else:
                x0 = signal_ref['offset']
            if 'n_samples' in kwargs:
                n_samples = kwargs['n_samples']
                if n_samples is None:
                    if signal_ref['coefficient'] == 0.:
                        # "SCALAR" support: effectively get the value of x0
                        n_samples = 1
                    else:
                        self.error("Cannot generate LINEAR signal with n_samples == None. "
                                   "Probably loading LINEAR axis data directly.")
                return x0 + np.arange(n_samples) * signal_ref['coefficient']
            else:
                return lambda i: x0 + signal_ref['coefficient'] * i
        else:
            self.error('unsupported signal type "%s"' % generic_signal_ref['signal_type'])
        return signal_data

    @cdb_base_check
    def get_signal_parameters(self, str_id='', parse_json=True, **kwargs):
        '''Get data signal parameters from data_signal_parameters table

        :param str_id: string id
        :param kwargs: keyword arguments for signal identification
        :param parse_json: if True, the method tries to parse fields as JSON.
        '''
        refs = self.get_signal_references(str_id, **kwargs)
        if not refs:
            self.error('signal not found')
        if len(refs) > 1:
            self.error('multiple signals found')
        ref = refs[0]

        sql_query = ('SELECT gs_parameters, daq_parameters FROM data_signal_parameters '
                     'WHERE generic_signal_id=%(generic_signal_id)i AND '
                     'record_number=%(record_number)i AND '
                     'revision=%(revision)i AND variant="%(variant)s"'
                     % ref)
        rows = self.query(sql_query)
        if len(rows) > 1:
            self.error('multiple records in data_signal_parameters - check the database')
        elif not rows:
            row = OrderedDict()
        else:
            row = rows[0]

        for pkey in ('gs_parameters', 'daq_parameters'):
            if not row.get(pkey):  # no row, NULL, empty string
                if parse_json:
                    row[pkey] = OrderedDict()
                else:
                    row[pkey] = ''
            elif parse_json:
                if row[pkey].strip() == '{}':
                    # TODO avoids a bug in Jython JSON lib
                    row[pkey] = OrderedDict()
                else:
                    row[pkey] = json.loads(row[pkey], object_pairs_hook=OrderedDict)

        return row

    @cdb_base_check
    def get_unit_system(self, unit_system_name=None, unit_system_id=None, load_parent=True, **ignored_args):
        ''' Get a unit system.

        :param unit_system_name: Name of the system.
        :param unit_system_id: The numerical ID of the system.
        :param load_parent: if true, automatically load parent unit systems.

        You have to specify either the system name (takes precedence)
        or unit_system_id.
        '''
        query = 'SELECT * FROM unit_systems WHERE '
        if unit_system_name is not None:
            query += 'unit_system_name LIKE %s' % self.mysql_str(unit_system_name)
        elif unit_system_id is not None:
            query += 'unit_system_id = %d' % int(unit_system_id)
        else:
            self.error('Unit system not specified')
        res = self.query(query)
        if not res:
            return None

        unit_system = res[0]
        if load_parent:
            if unit_system['parent_id']:
                unit_system['parent'] = self.get_unit_system(unit_system_id=unit_system['parent_id'], load_parent=True)
            else:
                unit_system['parent'] = None
        return unit_system

    @cdb_base_check
    def get_unit(self, **kwargs):
        ''' Get a unit.

        kwargs:

        :param unit_id: Direct ID of the unit.
        :param unit_system (or unit_system_id or unit_system_name): The unit system to use.
        :param construct: whether to construct unit from basic units (default: True)

        For unit system, specify any of the following keyword arguments:
            m, kg, s, A, K, mol, cd (as dimensionality)

        You can specify either unit_id OR
        unit_system (unit_system_id/unit_system_name) and dimensionality.
        '''
        if 'unit_id' in kwargs:
            query = 'SELECT * FROM units WHERE unit_id = %i' % (int(kwargs['unit_id']))
            return self.query(query)[0]
        else:
            # Get dimensions
            from . import units
            dim_args = dict((unit, int(kwargs.pop(unit, 0))) for unit in units.SI_UNITS.values())
            dim_where = ' AND '.join( '`%s` = %i' % (key, value) for key, value in dim_args.items())

            # Get system
            if 'unit_system' in kwargs:
                unit_system = kwargs['unit_system']
            else:
                kwargs['load_parent'] = True
                unit_system = self.get_unit_system(**kwargs)

            query = 'SELECT * FROM units WHERE unit_system_id=%i AND ' % (int(unit_system['unit_system_id']))
            query += dim_where
            res = self.query(query)
            if res:
                return res[0]
            else:
                if unit_system['parent']:
                    parent_unit = self.get_unit(unit_system=unit_system['parent'], construct=False, **dim_args)
                    if parent_unit:
                        return parent_unit
                if kwargs.get('construct', True):
                    unit = OrderedDict()
                    frags = []
                    for dim in units.SI_UNITS.values():
                        rank = dim_args[dim]
                        unit[dim] = rank
                        if rank:
                            gu_kwargs = { dim: 1, "unit_system" : unit_system}
                            frag = self.get_unit(**gu_kwargs)["unit_name"]
                            if rank != 1:
                                frag += "^%d" % rank
                            frags.append(frag)
                    unit["unit_name"] = "*".join(frags)
                    return unit
                return None

    @cdb_base_check
    def add_unit_system(self, unit_system_name, parent_id=None):
        insert_args = { 'unit_system_name' : unit_system_name}
        if parent_id:
            if not self.get_unit_system(unit_system_id=parent_id):
                raise Exception('Cannot make unit system descendant of non-existing parent.')
            insert_args['parent_id'] = int(parent_id)
        return self.insert('unit_systems', insert_args, True)

    @cdb_base_check
    def delete_unit_system(self, unit_system_id, force_unit_deletion=False):
        query = 'SELECT * FROM units WHERE unit_system_id = %i' % (int(unit_system_id))
        units = self.query(query)
        if units:
            if force_unit_deletion:
                for unit in units:
                    self.delete_unit(unit)
            else:
                raise Exception('Cannot delete unit system. It has existing units.')
        self.delete('unit_systems', {'unit_system_id': unit_system_id})

    @cdb_base_check
    def add_unit(self, unit_system_id, unit_name, auto_dim = False, **dimensions):
        """Add unit to unit system.

        :param auto_dim: if true, dimensions are inferred automatically.
        :returns: unit_id of the unit
        :rtype: int
        """
        from . import units
        if auto_dim:
            dimensions = units.get_dimension(unit_name)
        existing = self.get_unit(unit_system_id=unit_system_id, construct=False, **dimensions)
        if existing:
            raise Exception('Unit with the dimensions already exists in the unit system.')
        if not self.get_unit_system(unit_system_id=unit_system_id):
            raise Exception('Non-existing unit system.')

        # Sanitize **dimensions
        insert_args = dict((unit, int(dimensions.pop(unit, 0))) for unit in units.SI_UNITS.values())
        if dimensions:
            raise Exception('Invalid arguments in add_unit: %s' % dimensions)

        insert_args['unit_name'] = unit_name
        insert_args['unit_system_id'] = int(unit_system_id)
        return self.insert('units', insert_args, True)

    @cdb_base_check
    def delete_unit(self, unit_id=None, unit=None):
        if not unit:
            unit = self.get_unit(unit_id=unit_id)
            if not unit:
                raise Exception('Cannot delete non-existent unit.')
        self.delete('units', {'unit_id': unit['unit_id']})

    @cdb_base_check
    def put_signal(self, gs_alias_or_id, record_number, data,
                   variant='', time0=None,
                   data_coef=None, data_offset=None, data_coef_V2unit=None,
                   note=None, no_debug=None, data_quality='UNKNOWN',
                   time_axis_data=None, time_axis_coef=None, time_axis_offset=None, time_axis_note=None, time_axis_id=None,
                   axis1_data=None, axis1_coef=None, axis1_offset=None, axis1_note=None,
                   axis2_data=None, axis2_coef=None, axis2_offset=None, axis2_note=None,
                   axis3_data=None, axis3_coef=None, axis3_offset=None, axis3_note=None,
                   axis4_data=None, axis4_coef=None, axis4_offset=None, axis4_note=None,
                   axis5_data=None, axis5_coef=None, axis5_offset=None, axis5_note=None,
                   axis6_data=None, axis6_coef=None, axis6_offset=None, axis6_note=None,
                   gs_parameters=None, daq_parameters=None):
        """
        High-level function. Creates HDF5 file and also creates corresponding metadata in the database. Time axis can be
        expressed as a LINEAR function described by the time0 parameter (offset or the initial time) and the step (slope
        of the LINEAR function).


        :param gs_alias_or_id: Generic signal alias or generic signal id to assign the data to.
        :param record_number: Record number the data belongs to
        :param variant: Variant of the stored data.
        :param time0: Offset of a LINEAR time axis
        :param data_coef:
        :param data_offset:
        :param data_coef_V2unit:
        :param note:
        :param no_debug:
        :param data_quality:
        :param time_axis_data:
        :param time_axis_coef:
        :param time_axis_offset:
        :param time_axis_note:
        :param time_axis_id:
        :param axis#_data:
        :param axis#_coef:
        :param axis#_offset:
        :param axis#_note:
        :param gs_parameters:
        :param daq_parameters:
        :return:
        """

        from numpy import asarray
        from h5py import File

        gs_refs = self.get_generic_signal_references(gs_alias_or_id)
        if not gs_refs:
            self.error('generic signal not found')
        if len(gs_refs) > 1:
            self.error('multiple generic signals found')
        gs = gs_refs[0]
        if not self.record_exists(record_number=record_number):
            self.error('record number does not exist')
        if gs['signal_type'] != 'FILE':
            self.error('This method is designed to store only file-type signals!')
        data = asarray(data)
        # check data dimensions according to axes info only if not scalar
        if data.ndim != 0 and self.signal_dim(gs) != data.ndim:
            self.error('Wrong data dimensions')

        # TODO: check data consistency

        # after all checks were passed, we can start creating a dataset
        collection = gs.generic_signal_name
        data_file = self.new_data_file(collection, record_number=record_number,
                                       data_source_id=gs['data_source_id'], file_format="HDF5")
        data_file_id = data_file['data_file_id']
        param_list = {}
        # loop over possible axes
        for ax in self.__axes_names:
            ax_gs_id = gs[ax + '_id']
            # can change id of time axis
            if time_axis_id and ax == 'time_axis':
                ax_gs_id = time_axis_id
                param_list['time_axis_id'] = ax_gs_id
            if ax_gs_id:
                # this is a valid axis
                ax_gs = self.get_generic_signal_references(generic_signal_id=ax_gs_id)[0]
                stored_ax_refs = self.get_signal_references(generic_signal_id=ax_gs_id,
                                                            record_number=record_number)
                stored_ax_refs_variant = [ref for ref in stored_ax_refs if ref['variant'] == variant]
                #TODO: novariant is useless -> variant can be filtered already by get_signal_references
                stored_ax_refs_novariant = [ref for ref in stored_ax_refs if not ref['variant']]
                if ax_gs['signal_type'] == 'FILE':
                    # store FILE axis using cdb_put_signal
                    ax_data = eval(ax + '_data')
                    if ax_data is None:
                        if stored_ax_refs_variant:
                            # use already stored ax with the same variant
                            #TODO: does this give the last revision? Better to obtain by revision=-1 in get_signal_references()
                            ax_ref = stored_ax_refs_variant[-1]
                        else:
                            self.error(
                                '{ax} signal with variant={variant} does not exist, {ax}_data must be supplied'.format(
                                    ax=ax,
                                    variant=variant))
                    # use time axis data for time0
                        if time0 is None and ax == 'time_axis':
                            time0 = self.get_signal(**ax_ref).data[0]
                    else:
                        if time0 is None and ax == 'time_axis':
                            time0 = ax_data[0]
                        ax_param_list = dict(variant=variant,
                                             data_coef=eval(ax + '_coef'),
                                             data_offset=eval(ax + '_offset'),
                                             note=eval(ax + '_note'))
                        ax_ref = self.put_signal(ax_gs['generic_signal_id'], record_number, ax_data,
                                                 **ax_param_list)
                else:
                    # store LINEAR axis
                    # do not store if the axis can be reused
                    ax_param_list = {}
                    ax_param_list['record_number'] = record_number
                    ax_param_list['variant'] = variant
                    ax_param_list['offset'] = eval(ax + '_offset')
                    ax_param_list['coefficient'] = eval(ax + '_coef')
                    ax_param_list['note'] = eval(ax + '_note')
                    ax_param_list['data_quality'] = data_quality

                    if (ax_param_list['offset'] is None and
                            ax_param_list['coefficient'] is None and
                            ax_param_list['note'] is None):
                        if stored_ax_refs_variant:
                            # use already stores ax with the same variant
                            ax_ref = stored_ax_refs_variant[-1]
                        else:
                            self.error(
                                '{ax} signal with variant={variant} does not exist, {ax}_offset and {ax}_coef must be supplied'.format(
                                    ax=ax,
                                    variant=variant))
                    else:
                        # check if linear axis with the same coefficient and parameter exists already
                        # the axis variant can be different in this case
                        avail_ax_refs = [r for r in stored_ax_refs if
                                         ((ax_param_list['coefficient'] is None and r['coefficient'] == 1)
                                          or ax_param_list['coefficient'] == r['coefficient']) and
                                         ((ax_param_list['offset'] is None and r['offset'] == 0)
                                          or ax_param_list['offset'] == r['offset'])]
                        if avail_ax_refs:
                            # if so, use the existing axis
                            ax_ref = avail_ax_refs[-1]
                        else:
                            # otherwise store a new one
                            ax_ref = self.store_signal(ax_gs_id, **ax_param_list)
                        if not ax_ref:
                            self.error('could not store ' + ax)

                param_list[ax + '_revision'] = ax_ref['revision']
                param_list[ax + '_variant'] = ax_ref['variant']

        param_list['record_number'] = record_number
        param_list['variant'] = variant
        param_list['data_file_key'] = collection
        param_list['data_file_id'] = data_file_id
        param_list['time0'] = time0
        param_list['coefficient'] = data_coef
        param_list['offset'] = data_offset
        param_list['coefficient_V2unit'] = data_coef_V2unit
        param_list['note'] = note
        param_list['data_quality'] = data_quality
        param_list['gs_parameters'] = gs_parameters
        param_list['daq_parameters'] = daq_parameters

        # now, continue with storing the data itself
        # choose datasetname = collection -- this is again an arbitrary choice
        datasetname = collection

        with File(data_file['full_path'], 'w') as fh5:
            fh5.create_dataset(datasetname, data=data)
        self.set_file_ready(data_file_id)
        sig_ref = self.store_signal(gs['generic_signal_id'], **param_list)
        if not sig_ref:
            self.error('could not store signal')

        return sig_ref

    @classmethod
    def signal_dim(cls, signal_ref):
        '''Signal dimensionality from signal dict reference

        If the signal has no axes, it is assumed to be an axis and
        expected to have dimension 1. Otherwise the expected dimension
        is equal to the number of signal axes.
        '''
        return max(1, sum([0 if signal_ref[ax + '_id'] is None else 1
                           for ax in cls.__axes_names if (ax + '_id') in signal_ref]))


class CDBSignal(object):
    '''CDB signal class, contains description and data
    '''
    _logger = logging.getLogger('pyCDB')

    def get_log_level(self):
        '''Get logging level
        '''
        return self._log_level

    def set_log_level(self, value):
        '''Set logging level
        '''
        self._logger.handlers[0].setLevel(value)
        self._log_level = value

    log_level = property(get_log_level, set_log_level)

    def get_ndim(self):
        if self.gs_ref:
            ndim = 0
            for ax in CDBClient._CDBClient__axes_names:
                if self.gs_ref.get(ax + '_id'):
                    ndim += 1
        else:
            self._logger.warning('signal is empty')
            ndim = None
        return ndim

    def set_ndim(self):
        raise AttributeError('ndim cannot be modified')

    ndim = property(get_ndim, set_ndim)

    def __init__(self, data=None, name='', units='', axes=None,
                 description='', ref=None, gs_ref=None, file_ref=None,
                 daq_attachment=None):
        '''Create a CDBSignal instance with specified attributes

        The axes list should be a list of CDBSignal instances.
        Each axis in the list is also assigned as an attribute of the instance
        according to the axes_names list.
        '''
        self.data = data
        # TODO setdefault type attrs should be properties in the future
        self.gs_ref = {} if gs_ref is None else gs_ref
        self.gs_ref.setdefault('generic_signal_name', name)
        self.name = self.gs_ref.get('generic_signal_name')
        self.gs_ref.setdefault('units', units)
        self.units = self.gs_ref.get('units')
        self.axes = [] if axes is None else axes
        for axis_name, axis in zip(axes_names, self.axes):
            setattr(self, axis_name, axis)
            self.gs_ref[axis_name + '_id'] = axis.gs_ref
        self.gs_ref.setdefault('description', description)
        self.description = self.gs_ref.get('description')
        self.ref = {} if ref is None else ref
        self.file_ref = {} if file_ref is None else file_ref
        self.daq_attachment = {} if daq_attachment is None else daq_attachment
        # timestamp to be added to signals
        # self._timestamp = None

    def _repr_dict(self):
        flds = OrderedDict()
        flds['Generic signal name'] = self.gs_ref.get('generic_signal_name')
        flds['Generic signal alias'] = self.gs_ref.get('alias')
        flds['Generic signal id'] = self.gs_ref.get('generic_signal_id')
        flds['Record number'] = self.ref.get('record_number')
        flds['Revision'] = self.ref.get('revision')
        flds['Units'] = self.units
        # flds['Data'] = str(self.data)
        if self.data is not None:
            flds['Data'] = 'shape = %s, dtype = %s' % (str(self.data.shape), str(self.data.dtype))
        else:
            flds['Data'] = None
        flds['More details in'] = 'ref, gs_ref, daq_attachment, file_ref'
        return flds

    def __repr__(self):
        return self._repr_dict().__repr__()

    @property
    def url(self):
        '''WebCDB URL.'''
        return 'https://www.tok.ipp.cas.cz/webcdb/data_signals/%d/%d?revision=%d' % (self.gs_ref['generic_signal_id'], self.ref['record_number'], self.ref['revision'])

    def info(self):
        '''Information string about the signal'''
        res = 'reference: %s\n' % str(self.ref)
        if self.data is not None:
            res += 'data shape: %s\n' % str(self.data.shape)
        return res

    def get_axes_info(self, remove_empty_dim=True):
        '''Return list of dicts describing each viable axis'''
        from numpy import arange
        axes = self.axes[:]
        if self.time_axis and self.time_axis not in axes:  # may be already there
            axes.insert(0, self.time_axis)
        axes = [{'data': a.data, 'label': '%s [%s]' % (a.name,
                                                       a.units)} for a in axes]
        if len(axes) == 0:
            axes = [{'data': arange(n), 'label': 'index'} for n in
                    self.data.shape if n > 1 or not remove_empty_dim]
        return axes

    def plot(self, fig=None, subplot=111, mplbackend=None, down_sample=1, start_time=-1, stop_time=-1,
             y_start=-1, y_stop=-1, xmin=None, xmax=None, color_set=None, plot_kwargs={}, contour=True,
             colormap=True, **kwargs):
        '''Plots the signal

        :param fig: existing figure to add plot to
        :param subplot: subplot to plot to
        :param mplbackend: matplotlib backend to use (None - use default), use 'Agg' for no screen output
        :param color_set: Color set of the figure, currently supported None (default) and logbook
        :param plot_kwargs: dictionary with keyword arguments for the matplotlib plotting command
        :param contour (only 2D): show contour map
        :param colormap (only 2D): show color map
        '''

        if 'DownSmpl' in kwargs:
            self.warning('use down_sample instead of DownSmpl')
            down_sample = kwargs['DownSmpl']

        import matplotlib
        if mplbackend is not None:
            matplotlib.use(mplbackend)
        import matplotlib.pyplot as plt
        from numpy import arange
        plot_args = plot_kwargs
        subplot_args = {}
        figure_args = {}
        if color_set == 'logbook':
            plt.rcParams['figure.facecolor'] = '#333333'
            plt.rcParams['axes.labelcolor'] = 'white'
            plt.rc('axes', edgecolor='#999999')
            plot_args.update({'color': 'orange', 'lw': 2, 'marker': 'x'})
            subplot_args['axisbg'] = '#333333'
            figure_args.update({'figsize': (6, 4), 'dpi': 100})

        if self.data is None:
            self._logger.warning('no data to plot')
            return

        if fig is None:
            fig = plt.figure(**figure_args)
        ax = fig.add_subplot(subplot, **subplot_args)

        axes = self.get_axes_info()
        if len(axes) > 2 or self.data.ndim == 0 or self.data.ndim > 2:
            self._logger.warning("sorry, don't know how to plot %i-dimensional data" % self.data.ndim)
            return

        if len(axes) == 1:
            # 1D data
            ax.plot(axes[0]['data'][xmin:xmax:down_sample], self.data[xmin:xmax:down_sample],
                    label=self.name, **plot_args)
            plt.xlabel(axes[0]['label'])
            plt.ylabel(self.units)

            if start_time != -1:
                if stop_time != -1:
                    ax.set_xlim(start_time, stop_time)
            if y_stop != -1:
                if y_start != -1:
                    ax.set_ylim(y_start, y_stop)
            # for child in ax.get_children():
                # child.set_color('#999999')
            if color_set == 'logbook':
                ax.xaxis.label.set_color('white')
                ax.yaxis.label.set_color('white')
                ax.tick_params(axis='x', colors='white')
                ax.tick_params(axis='y', colors='white')

                for line in ax.yaxis.get_ticklines():
                    line.set_color('#999999')
                    line.set_markeredgewidth(1)
                    line.set_markersize(6)
                for line in ax.xaxis.get_ticklines():
                    line.set_color('#999999')
                    line.set_markeredgewidth(1)
                    line.set_markersize(6)

        elif len(axes) == 2:
            # 2D data
            x = axes[0]['data']
            y = axes[1]['data']
            if x.ndim == 1 and y.ndim == 1:
                extent = (y.min(), y.max(), x.min(), x.max())
                if colormap:
                    ax.imshow(self.data, interpolation=kwargs.get('interpolation', 'bilinear'), origin='lower', extent=extent)
                if contour:
                    cs = ax.contour(y, x, self.data, origin='lower', extent=extent)
                    plt.clabel(cs)
                # plt.colorbar(cs)
                ax.axis('tight')
                plt.ylabel(axes[0]['label'])
                plt.xlabel(axes[1]['label'])
            # alternative plot for time dependent 1D data
            elif self.time_axis:
                # plt.figure(**figure_args)
                # ax = fig.add_subplot(111)
                ax.plot(y.T, self.data.T)
                plt.xlabel(axes[1]['label'])
                plt.ylabel(self.units)
                plt.title('Time evolution of %s' % self.gs_ref['generic_signal_name'])

        return fig

    def __array__(self, dtype=None):
        '''Expose the underlying data array through the __array__ interface

        This enables application of NumPy ufuncs and other functions
        that require a NumPy-like array (they usually do
        input = numpy.asarray(input)) on the signal.
        '''
        return self.data.view(dtype=dtype)


def get_h5_dataset(file_path='', file_key='', file_format='HDF5', slice_=Ellipsis):
    ''' Get the HDF5 dataset.
    '''
    # TODO: ? Add force copy parameter
    import h5py
    import numpy as np
    if not os.path.isfile(file_path):
        raise Exception('File "%s" not found' % file_path)
    with h5py.File(file_path, 'r') as file_handle:
        dataset = file_handle.get(file_key)
        if dataset:
            if dataset.compression:
                all_data = np.copy(dataset[...])
                return all_data[slice_]
            else:
                return dataset[slice_]
        else:
            raise Exception('Cannot read "%s" from file "%s"' % (file_key, file_path))

def get_h5_dataset_shape(file_path='', file_key='', file_format='HDF5'):
    ''' Get the HDF5 dataset shape.
    '''
    import h5py
    import numpy as np
    if not os.path.isfile(file_path):
        raise Exception('File "%s" not found' % file_path)
    with h5py.File(file_path, 'r') as file_handle:
        dataset = file_handle.get(file_key)
        if dataset:
            return dataset.shape
        else:
            raise Exception('Cannot read "%s" from file "%s"' % (file_key, file_path))

def read_file_data(file_path='', file_key='', file_format='HDF5', slice_=Ellipsis):
    '''Read data from a file

    :param file_path: file path of the file
    :param file_key: data location in the file (groupA/groupB/.../dataset for HDF5)
    :param file_format: data format of the file
    :param file_handle: file handle if file is already open
    :param slice_: slice specification
    '''
    if file_format == 'HDF5':
        return get_h5_dataset(file_path, file_key, file_format, slice_)
    else:
        raise Exception('Data format {0} not supported'.format(file_format))

def format_record_path(record_number, record_type):
    '''Return data path for a given record number

    :rtype: string with the subdirectory name
    '''

    data_directory = '%i' % record_number
    if record_type != 'EXP':
        data_directory = os.path.join(record_type, data_directory)
    return data_directory


def decode_generic_signal_strid(generic_signal_strid):
    '''Decode generic signal string reference and return a dictionary

    :param generic_signal_strid: is defined as 'alias or generic_signal_name' or
                                 'generic_signal_name/data_source_name' or
                                 'generic_signal_name/data_source_id'
    :rtype: dictionary containing 'alias_or_name' or ('generic_signal_name' and ('data_source_id' or 'data_source_name'))
    '''

    # convert to string (e.g. for numeric id's as input)
    generic_signal_strid = str(generic_signal_strid)
    str_id_parts = [s.strip() for s in generic_signal_strid.split('/')]
    if len(str_id_parts) == 1:
        if str_id_parts[0].isdigit():
            res = {'generic_signal_id': int(str_id_parts[0])}
        else:
            res = {'alias_or_name': str_id_parts[0]}
    elif len(str_id_parts) == 2:
        res = {'alias_or_name': str_id_parts[0]}
        if str_id_parts[1].isdigit():
            res['data_source_id'] = int(str_id_parts[1])
        else:
            res['data_source_name'] = str_id_parts[1]
    else:
        raise Exception('syntax error in CDB string id: wrong number of fields')
    return res


def decode_signal_strid(str_id):
    '''Decode signal string id

    :param str_id: string identifier, defined as id_type:identification[units_or_slice][units_or_slice]
    units_or_slice can be either units or slice specification. If two brackets are supplied,
    the second section in brackets has to differ from the first one.
    id_type := CDB | FS | DAQ

    identification := (based on id_type)
     * CDB -> generic_signal_strid:record_number:variant:revision
     * FS -> nodeuniqueid/hardwareuniqueid/parameteruniqueid:record_number:variant:revision
     * DAQ -> computer_id/board_id/channel_id:record_number:variant:revision
       * if computer_id is a string (contains non-numeric characters), it's understood as computer_name
       * alternatively, computer_id and channel_id can be prefixed by "board\\_" and "channel\\_"
       * in DAQ and FS, you can use "." instead of "/"

    special units:
     * [RAW] or [raw] - get raw signal
     * [DAV] - signal in Data Acquistion Volts (applied get_coefficient_lev2V)
     * [] or [DEFAULT] or [default] - default units
     * [unit_name] or [unit_system_name] - conversion to unit stored in the database

    certain parts can be omitted, the defaults are:
     * id_type = CDB
     * revision = -1
     * variant = ''
     * record_number = -1
     * default units

    i.e., "ne/thomson:100" == "CDB:ne/thomson:100:-1"
    '''

    __signal_id_types = ('CDB', 'FS', 'DAQ')

    res = {}
    # convert to string (e.g. for numeric id's as input)
    str_id = str(str_id)
    # resove brackets - units or indices can be within them
    # match stuff between brackets
    bb = re.compile(r"\[(.*?)\]")
    bblist = bb.findall(str_id)
    if len(bblist) > 2:
        raise Exception("More than two brackets sections detected.")
    for b in bblist:
        try:
            res['slice'] = str_to_slice(b)
            # print "got slice: %s" %b
        except SyntaxError:
            res['units'] = b
            # print "got units: %s" %b
    if len(bblist) == 2 and ('units' not in res or 'slice' not in res):
        raise Exception(
            "Detected two in-brackets-sections of the same kind: {} {}".format(*bblist))
    if 'units' not in res:
        res['units'] = 'default'
    if 'slice' not in res:
        res['slice'] = Ellipsis
    # Remove bracket sections from string, assume that brackets sections can be placed only at the end of a string
    str_id = str_id.split("[")[0]

    # start parsing
    str_id_parts = [s.strip() for s in str_id.split(':')]
    if str_id_parts[0].upper() not in __signal_id_types:
        str_id_parts.insert(0, 'CDB')
    res['id_type'] = str_id_parts[0].upper()
    if res['id_type'] == 'CDB':
        if not (2 <= len(str_id_parts) <= 5):
            raise Exception('syntax error in CDB string id: wrong number of fields')
        res['generic_signal_strid'] = str_id_parts[1]
        res['revision'] = -1
        res['variant'] = ''
        res['record_number'] = -1
        if len(str_id_parts) == 5:
            res['record_number'] = int(str_id_parts[2])
            res['variant'] = str_id_parts[3]
            res['revision'] = int(str_id_parts[4])
        elif len(str_id_parts) == 4:
            res['record_number'] = int(str_id_parts[2])
            # this can be either revision (int) or variant (str)
            try:
                res['revision'] = int(str_id_parts[3])
            except ValueError:
                res['variant'] = str_id_parts[3]
        elif len(str_id_parts) == 3:
            res['record_number'] = int(str_id_parts[2])
        else:
            pass
    elif res['id_type'] == 'FS':
        if not (2 <= len(str_id_parts) <= 4):
            raise Exception('syntax error in CDB string id: wrong number of fields')
        if '/' in str_id_parts[1]:
            split_char = '/'
        else:
            split_char = '.'
        FS_channel_id = str_id_parts[1].split(split_char)
        if len(FS_channel_id) != 3:
            raise Exception('syntax error in CDB string id: wrong number of fields')
        res['nodeuniqueid'] = FS_channel_id[0]
        res['hardwareuniqueid'] = FS_channel_id[1]
        res['parameteruniqueid'] = FS_channel_id[2]
        if len(str_id_parts) == 4:
            res['record_number'] = int(str_id_parts[2])
            res['revision'] = int(str_id_parts[3])
        elif len(str_id_parts) == 3:
            res['record_number'] = int(str_id_parts[2])
            res['revision'] = -1
        else:
            res['record_number'] = -1
            res['revision'] = -1
    elif res['id_type'] == 'DAQ':
        if not (2 <= len(str_id_parts) <= 4):
            raise Exception('syntax error in CDB string id: wrong number of fields')
        if '/' in str_id_parts[1]:
            split_char = '/'
        else:
            split_char = '.'
        channel_id = str_id_parts[1].split(split_char)
        if len(channel_id) != 3:
            raise Exception('syntax error in CDB string id: wrong number of fields')
        try:
            res['computer_id'] = int(channel_id[0])
        except ValueError:
            # string -> computer_name
            res['computer_name'] = channel_id[0]
            res['computer_id'] = res['computer_name']  # compatibility reasons
        res['board_id'] = channel_id[1]
        if res['board_id'].lower().startswith('board_'):
            res['board_id'] = res['board_id'][6:]
        res['board_id'] = int(res['board_id'])
        res['channel_id'] = channel_id[2]
        if res['channel_id'].lower().startswith('channel_'):
            res['channel_id'] = res['channel_id'][8:]
        res['channel_id'] = int(res['channel_id'])
        if len(str_id_parts) == 4:
            res['record_number'] = int(str_id_parts[2])
            res['revision'] = int(str_id_parts[3])
        elif len(str_id_parts) == 3:
            res['record_number'] = int(str_id_parts[2])
            res['revision'] = -1
        else:
            res['record_number'] = -1
            res['revision'] = -1
    else:
        raise Exception('unknow string id type "%s"' % res['id_type'])
    return res

def apply_calibration(data, calibration):
    '''Apply calibration to RAW data.

    :param data: numpy array (or anything that can be multiplied and added)
    :param calibration: dictionary having keys [offset, coefficient]
        (signal_ref can be used)

    Equation:
        ( data + offset ) * coefficient
    '''
    if data.dtype.names:
        data = data.copy()
        for n in data.dtype.names:
            data[n] = apply_calibration(data[n], calibration)
    else:
        # Optimized calculation:
        #   addition & multiplication applied only if needed
        if calibration["offset"] != 0.0 or calibration["coefficient"] != 1.0:
            data = (data + calibration["offset"]) * calibration["coefficient"]
        else:
            data = data.copy()
    return data

def validate_variant(variant):
    '''Check that the supplied variant follows the rules.'''
    if not isstringlike(variant):
        raise Exception('Variant must be a string')
    if not len(variant):
        return # '' is acceptable :-)
    if not variant[0].isalpha():
        raise Exception('Variant must start with a letter')
    if len(variant) > 20:
        raise Exception('variant cannot be longer than 20 characters')
    try:
        pyCDBBase.filter_sql_str(variant, extra_chars='_')
    except Exception:
        raise Exception('variant must contain only letters, numbers and underscore')
