'''Functions for manipulation with database schemas.

!!! USE VERY CAREFULLY !!!
'''
import getpass
import subprocess
import os
import sys
import re

from pyCDB.pyCDBBase import _mysqlClass

def dump_schema(path=None, **kwargs):
    """Dump database schema.

    :param triggers: if True, include also SQL for triggers (conflict for unprivileged users)
    :param definers: if True, include also SQL for definers (conflict for unprivileged users)
    :param autoincrement: if True, include also AUTOINCREMENT values

    Keyword arguments:
        You have to specify either client (valid CDBClient object) or (host, db, user, [port]).
    """
    args = ["mysqldump"] + _connection_arguments(**_parse_arguments(**kwargs)) + ["--no-data", "--skip-comments", "--skip-lock-tables"]
    if not kwargs.get("triggers"):
        args.append("--skip-triggers")
    if sys.version_info >= (2, 7):
        try:
            output = subprocess.check_output(args)
        except subprocess.CalledProcessError as err:
            print(err.output)
            raise
    else:
        output = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()[0]
    if path:
        with open(path, "w") as f:
            for line in output.splitlines(True):
                if "DEFINER" in line and not kwargs.get("definers") :
                    continue
                if not kwargs.get("autoincrement"):
                    line = re.sub(" AUTO_INCREMENT=\\d+", "", line)
                f.write(line)
        return True
    else:
        return output

def load_schema(path, **kwargs):
    """Load schema into database."""
    if not os.path.exists(path):
        raise ValueError("%s is not a valid path" % path)

    args = ["mysql"] + _connection_arguments(**_parse_arguments(**kwargs))
    with open(path, "r") as f:
        if sys.version_info >= (2, 7):
            output = subprocess.check_output(args, stdin=f)
        else:
            output = subprocess.Popen(args, stdin=f, stdout=subprocess.PIPE).communicate()[0]

def create_database(db, ignore_existing=False, **kwargs):
    """Create empty database.

    Requires root privileges."""
    args = _parse_arguments(**kwargs)
    with _mysqlClass(
        host=args["host"],
        port=args["port"],
        user=args["user"],
        password=args["password"]
    ) as connection:
        with connection.cursor() as cursor:
            if ignore_existing:
                sql = "CREATE DATABASE IF NOT EXISTS `{}`".format(db)
            else:
                sql = "CREATE DATABASE `{}`".format(db)
            cursor.execute(sql)
        connection.commit()


def delete_all_tables(db, **kwargs):
    """Delete all tables and views in the database.

    It should leave an empty database.
    """
    args = _parse_arguments(**kwargs)
    with _mysqlClass(
        host=args["host"],
        port=args["port"],
        user=args["user"],
        password=args["password"],
        db=db
    ) as connection:
        with connection.cursor() as cursor:
            cursor.execute("SET FOREIGN_KEY_CHECKS=0")
            cursor.execute("SHOW FULL TABLES")
            tables = cursor.fetchall()
            for table in tables:
                if table[1] == "BASE TABLE":
                    cursor.execute("DROP TABLE `%s`" % (table[0]))
                else:
                    cursor.execute("DROP VIEW `%s`" % (table[0]))
        connection.commit()


def drop_database(db, **kwargs):
    """Drop database.

    Safety reasons disables deleting database named `CDB`."""
    if db == 'CDB':
        raise RuntimeError("Dropping the CDB database not allowed.")
    sql = "DROP DATABASE %s;" % db
    args = ["mysql"] + _connection_arguments(**_parse_arguments(**kwargs))
    process = subprocess.Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    result = process.communicate(sql)
    if result[0]:
        print(result[0])

def reload_schema(path, **kwargs):
    """Empty database and recreate all tables using saved schema.

    :param path: the file with SQL schema.
    """
    if not os.path.exists(path):
        raise ValueError("%s is not a valid path" % path)
    create_database(ignore_existing=True, **kwargs)
    delete_all_tables(**kwargs)
    load_schema(path, **kwargs)

def _parse_arguments(**kwargs):
    result = {}
    if 'client' in kwargs:
        client = kwargs['client']
        result['host'] = client._host
        result['db'] = client._db_name
        result['user'] = client._user
        result['password'] = client._passwd
        result['port'] = client._port
    else:
        result['host'] = kwargs['host']
        result['db'] = kwargs.get('db', None)  # needn't be specified
        result['user'] = kwargs['user']
        result['port'] = kwargs.get('port', 3306)
        if 'password' in kwargs:
            result['password'] = kwargs['password']
        else:
            result['password'] = getpass.getpass()
    return result

def _connection_arguments(host, user, password, db, port):
    if not host:
        raise ValueError("'host' must be specified.")
    if not user:
        raise ValueError("'user' must be specified.")
    if not password:
        raise ValueError("'password' must be specified.")
    result = [
        '--host=%s' % host,
        '--user=%s' % user,
        '--password=%s' % password,
        '--port=%s' % port
    ]
    if db:
        result.append(db)
    return result