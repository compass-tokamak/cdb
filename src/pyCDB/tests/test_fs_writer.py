import pytest
from .cdb_test_case import CDBTestCase, run
import time
from pyCDB.fs_writer import *

class TestFSWriter(CDBTestCase):
    def check_writer_filled(self, writer):
        self.assertTrue( writer.generic_signal_id )
        self.assertTrue( writer.data_source_id )
        self.assertTrue( writer.time_axis_id )
        self.assertTrue( writer.computer_id )
        self.assertNotEqual(None, writer.board_id )
        self.assertNotEqual(None, writer.channel_id )
        self.assertNotEqual(None, writer.coefficient_lev2V)
        self.assertNotEqual(None, writer.coefficient_V2unit)
        self.assertNotEqual(None, writer.offset)

    def ensure_marte_boards_existence(self):
        node_id = "MARTE_NODE"
        hardware_id = "ChannelDataCollection"
        parameter_id = "Channel_001"
        try:
            self.client.FS2CDB_ref(node_id, hardware_id, parameter_id) # Raises if non-existent
        except:
            data_source_name = "MARTE"
            try:
                data_source_id = self.client.get_data_source_id(data_source_name)
            except:
                data_source_id = self.client.insert("data_sources", {"name": data_source_name, "subdirectory": data_source_name}, return_inserted_id=True)
            computer_name = "marte"
            try:
                computer_id = self.client.get_computer_id(computer_name)
            except:
                computer_id = self.client.create_computer(computer_name)
            time_axis_id = self.create_random_generic_signal(generic_signal_name=self.random_name("time_axis_marte"), data_source_id=data_source_id, signal_type="LINEAR")["generic_signal_id"]
            self.client.create_DAQ_channels(node_id, hardware_id, parameter_id, data_source_id, time_axis_id=time_axis_id, computer_id=computer_id, board_id=-1)

    def test_read_gs_non_existent(self):
        node_id = "unknown"
        hardware_id = "unknown"
        parameter_id = "unknown"
        time0 = 0
        sample_length = 1.0
        record_number = self.client.create_record()

        writer = FSWriter(self.client, node_id = node_id, hardware_id = hardware_id, parameter_id = parameter_id, record_number = record_number, time0 = time0, sample_length = sample_length)
        self.assertRaises(GenericSignalNotFoundException, writer.read_signal_info)
        self.assertFalse(writer.channel_created)

    def test_read_record_non_existent(self):
        channel = self.create_random_channel()
        (node_id, hardware_id, parameter_id) = channel.nodeuniqueid, channel.hardwareuniqueid, channel.parameteruniqueid
        time0 = 0
        sample_length = 1.0
        record_number = 75448951

        writer = FSWriter(self.client, node_id = node_id, hardware_id = hardware_id, parameter_id = parameter_id, record_number = record_number, time0 = time0, sample_length = sample_length)
        self.assertRaises(RecordNotFoundException, writer.read_signal_info)
        self.assertFalse(writer.channel_created)

    def test_read_auto_create_non_existent_board(self):
        self.ensure_marte_boards_existence()
        node_id = "MARTE_NODE"
        hardware_id = "unknown"
        parameter_id = "unknown"
        time0 = 0
        sample_length = 1.0
        record_number = self.client.create_record()

        writer = FSWriter(self.client, node_id = node_id, hardware_id = hardware_id, parameter_id = parameter_id, record_number = record_number, time0 = time0, sample_length = sample_length)
        self.assertRaises(ChannelAutoCreationException, writer.read_signal_info)
        self.assertFalse(writer.channel_created)

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_read_auto_create_marte_existing_board(self):
        self.ensure_marte_boards_existence()
        node_id = "MARTE_NODE"
        hardware_id = "ChannelDataCollection"
        parameter_id = self.random_name("channel_")

        time0 = 0
        sample_length = 1.0
        record_number = self.client.create_record()

        writer = FSWriter(self.client, node_id = node_id, hardware_id = hardware_id, parameter_id = parameter_id, record_number = record_number, time0 = time0, sample_length = sample_length)
        writer.read_signal_info()
        self.assertTrue(writer.channel_created)
        self.check_writer_filled(writer)

        generic_signal = self.client.get_FS_signal_reference(node_id, hardware_id, parameter_id)
        self.assertTrue(generic_signal)

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_write_signal(self):
        channel = self.create_random_channel(create_time_axis=True)
        (node_id, hardware_id, parameter_id) = channel.nodeuniqueid, channel.hardwareuniqueid, channel.parameteruniqueid
        record_number = self.client.create_record()

        time0 = 0.1
        sample_length = 1.01
        writer = FSWriter(self.client, node_id = node_id, hardware_id = hardware_id, parameter_id = parameter_id,
            record_number=record_number, time0=time0, sample_length=sample_length)
        writer.read_signal_info()
        writer.store_signal_and_file()

        str_id = "FS:{0}/{1}/{2}:{3}".format(node_id, hardware_id, parameter_id, record_number)
        signal = self.client.get_signal_references(str_id)[0]
        self.assertEqual(time0, signal["time0"])

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_write_signal_with_time_axis_in_setup(self):
        generic_signal = self.create_random_generic_signal(create_time_axis=True)
        channel = self.create_random_channel(create_time_axis=True)
        (node_id, hardware_id, parameter_id) = channel.nodeuniqueid, channel.hardwareuniqueid, channel.parameteruniqueid
        computer_id = channel["computer_id"]
        board_id = channel["board_id"]
        channel_id = channel["channel_id"]
        new_time_axis_id = self.create_random_generic_signal(signal_type="LINEAR")["generic_signal_id"]

        time.sleep(1.2)
        self.client.attach_channel(computer_id, board_id, channel_id, generic_signal["generic_signal_id"], time_axis_id=new_time_axis_id)
        record_number = self.client.create_record()

        time0 = 0
        sample_length = 1.01
        writer = FSWriter(self.client, node_id=node_id, hardware_id=hardware_id, parameter_id=parameter_id, record_number=record_number,
            time0=time0, sample_length=sample_length)
        writer.read_signal_info()
        writer.store_signal_and_file()

        data_signal = self.client.get_signal_references(generic_signal_id=generic_signal["generic_signal_id"], record_number=record_number)[0]
        self.assertEqual(new_time_axis_id, data_signal["time_axis_id"])

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_parameters(self):
        channel = self.create_random_channel(create_time_axis=True)
        (node_id, hardware_id, parameter_id) = channel.nodeuniqueid, channel.hardwareuniqueid, channel.parameteruniqueid
        gs_parameters = '{"a":"b:"}'
        generic_signal_id = self.client.get_generic_signal_id(generic_signal_name = "{0}.{1}.{2}".format(node_id, hardware_id, parameter_id))

        self.client.signal_setup(generic_signal_id=generic_signal_id,
            parameters = gs_parameters)

        record_number = self.client.create_record()

        time0 = 0
        sample_length = 1.
        writer = FSWriter(self.client, node_id = node_id, hardware_id = hardware_id, parameter_id = parameter_id,
            record_number=record_number, time0=time0, sample_length=sample_length)
        writer.read_signal_info()
        writer.store_signal_and_file()

        str_id = "FS:{0}/{1}/{2}:{3}".format(node_id, hardware_id, parameter_id, record_number)
        signal = self.client.get_signal_references(str_id)[0]
        parameters = self.client.get_signal_parameters(str_id, parse_json=False)

        self.assertEqual(gs_parameters, parameters.gs_parameters)

if __name__ == '__main__':
    run()
