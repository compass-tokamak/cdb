from . import cdb_test_case  # Just for paths etc.

import unittest
import math

from pyCDB import epics_archiver_appliance_api


class TestEpicsArchiverApplianceApi(unittest.TestCase):
    def test_parse_csv_row(self):
        api = epics_archiver_appliance_api.EpicsArchiverApplianceApi(url="http://localhost")
        sample = api.parse_csv_row(["1689580806", "1.5E-6", "0", "0", "932371337"])
        self.assertEqual(1.5e-6, sample.value)
        self.assertEqual(1689580806932.371, math.floor(sample.js_timestamp * 1000) / 1000)


if __name__ == "__main__":
    unittest.main()
