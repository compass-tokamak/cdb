-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: codac2
-- Generation Time: Jun 23, 2022 at 02:11 PM
-- Server version: 5.5.68-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `CDB`
--
-- CREATE DATABASE IF NOT EXISTS `CDB` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
-- USE `CDB`;

-- --------------------------------------------------------

--
-- Stand-in structure for view `channel_attachments`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `channel_attachments`;
CREATE TABLE IF NOT EXISTS `channel_attachments` (
`computer_id` smallint(6)
,`board_id` smallint(6)
,`channel_id` smallint(6)
,`default_generic_signal_id` int(11)
,`attached_generic_signal_id` int(11)
,`attach_time` datetime
,`offset` double
,`coefficient_lev2V` double
,`coefficient_V2unit` double
,`time_axis_id` int(11)
,`parameters` text
,`uid` int(11)
,`note` varchar(140)
,`attached_signal_name` varchar(127)
,`is_attached` int(1)
);

-- --------------------------------------------------------

--
-- Table structure for table `channel_setup`
--

DROP TABLE IF EXISTS `channel_setup`;
CREATE TABLE IF NOT EXISTS `channel_setup` (
  `attached_generic_signal_id` int(11) NOT NULL,
  `attach_time` datetime NOT NULL,
  `computer_id` smallint(6) NOT NULL,
  `board_id` smallint(6) NOT NULL,
  `channel_id` smallint(6) NOT NULL,
  `uid` int(11) DEFAULT NULL COMMENT 'user id of the event creator',
  `note` varchar(140) DEFAULT NULL,
  `coefficient_lev2V` double DEFAULT '1' COMMENT 'conversion from channel level (raw) to Volts',
  `offset` double DEFAULT '0' COMMENT 'offset in channel level (raw) units',
  `coefficient_V2unit` double DEFAULT '1' COMMENT 'conversion factor from Volts to physical units (of the attached generic signal)',
  `time_axis_id` int(11) DEFAULT NULL,
  `parameters` text,
  PRIMARY KEY (`attached_generic_signal_id`,`attach_time`),
  UNIQUE KEY `unique_attach_time_idx` (`computer_id`,`board_id`,`channel_id`,`attach_time`),
  KEY `channel_id_index` (`computer_id`,`board_id`,`channel_id`),
  KEY `fk_physical_setup_generic_signals1_idx` (`attached_generic_signal_id`),
  KEY `fk_channel_setup_1_idx` (`computer_id`,`board_id`,`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Record and shot numbers. Includes data directories (relative';

-- --------------------------------------------------------

--
-- Table structure for table `DAQ_channels`
--

DROP TABLE IF EXISTS `DAQ_channels`;
CREATE TABLE IF NOT EXISTS `DAQ_channels` (
  `computer_id` smallint(6) NOT NULL,
  `board_id` smallint(6) NOT NULL,
  `channel_id` smallint(6) NOT NULL,
  `default_generic_signal_id` int(11) NOT NULL,
  `note` varchar(500) DEFAULT NULL,
  `nodeuniqueid` varchar(90) DEFAULT NULL COMMENT 'FireSignal node id',
  `hardwareuniqueid` varchar(90) DEFAULT NULL COMMENT 'FireSignal hardware id',
  `parameteruniqueid` varchar(90) DEFAULT NULL COMMENT 'FireSignal parameter id ',
  PRIMARY KEY (`computer_id`,`board_id`,`channel_id`),
  UNIQUE KEY `generic_signal_id_UNIQUE` (`default_generic_signal_id`),
  UNIQUE KEY `FS_id_index` (`nodeuniqueid`,`hardwareuniqueid`,`parameteruniqueid`),
  KEY `fk_detach_ids_generic_signals1_idx` (`default_generic_signal_id`),
  KEY `fk_physical_channels_1_idx` (`computer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of A/D channels, identified by computer, board and chan';

-- --------------------------------------------------------

--
-- Table structure for table `data_files`
--

DROP TABLE IF EXISTS `data_files`;
CREATE TABLE IF NOT EXISTS `data_files` (
  `data_file_id` int(11) NOT NULL AUTO_INCREMENT,
  `data_source_id` smallint(6) NOT NULL,
  `record_number` int(11) NOT NULL,
  `data_format` enum('HDF5','NETCDF4','GENERIC','VIDEO') NOT NULL,
  `collection_name` varchar(127) NOT NULL COMMENT 'filename = collection_name.revision.ext',
  `revision` smallint(6) NOT NULL DEFAULT '1',
  `file_name` varchar(135) NOT NULL,
  PRIMARY KEY (`data_file_id`),
  UNIQUE KEY `file_UNIQUE` (`record_number`,`data_source_id`,`collection_name`,`revision`),
  KEY `data_source_id` (`data_source_id`),
  KEY `fk_data_files_1_idx` (`data_source_id`),
  KEY `shot_number_idx` (`record_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of data files, i.e. files that are physically on disks.';

-- --------------------------------------------------------

--
-- Table structure for table `data_signals`
--

DROP TABLE IF EXISTS `data_signals`;
CREATE TABLE IF NOT EXISTS `data_signals` (
  `record_number` int(11) NOT NULL,
  `generic_signal_id` int(11) NOT NULL,
  `revision` smallint(6) NOT NULL DEFAULT '1',
  `variant` varchar(20) NOT NULL DEFAULT '',
  `timestamp` datetime NOT NULL,
  `data_file_id` int(11) DEFAULT NULL,
  `data_file_key` varchar(127) DEFAULT NULL COMMENT 'placement in the data file',
  `time0` double DEFAULT '0',
  `coefficient` double DEFAULT '1' COMMENT 'corresponds\r\nto coefficient_lev2V in channel_setup',
  `offset` double DEFAULT '0',
  `coefficient_V2unit` double DEFAULT '1',
  `time_axis_id` int(11) DEFAULT NULL COMMENT 'time_axis_id is set when different to the generic signal,\r\ne.g. when connected to a different DAQ\n',
  `time_axis_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis1_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis2_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis3_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis4_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis5_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis6_revision` smallint(6) NOT NULL DEFAULT '1',
  `time_axis_variant` varchar(20) NOT NULL DEFAULT '',
  `axis1_variant` varchar(20) NOT NULL DEFAULT '',
  `axis2_variant` varchar(20) NOT NULL DEFAULT '',
  `axis3_variant` varchar(20) NOT NULL DEFAULT '',
  `axis4_variant` varchar(20) NOT NULL DEFAULT '',
  `axis5_variant` varchar(20) NOT NULL DEFAULT '',
  `axis6_variant` varchar(20) NOT NULL DEFAULT '',
  `note` varchar(140) DEFAULT NULL,
  `computer_id` smallint(6) DEFAULT NULL,
  `board_id` smallint(6) DEFAULT NULL,
  `channel_id` smallint(6) DEFAULT NULL,
  `data_quality` enum('UNKNOWN','POOR','GOOD','VALIDATED') NOT NULL DEFAULT 'UNKNOWN',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`record_number`,`generic_signal_id`,`revision`,`variant`),
  KEY `fk_data_file_id_idx` (`data_file_id`),
  KEY `fk_record_number_idx` (`record_number`),
  KEY `fk_data_signals_1_idx` (`computer_id`,`board_id`,`channel_id`),
  KEY `fk_generic_channel_id_idx` (`generic_signal_id`),
  KEY `timestamp_idx` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Information of signals for particular records (instances of ';

-- --------------------------------------------------------

--
-- Table structure for table `data_signals_myisam`
--

DROP TABLE IF EXISTS `data_signals_myisam`;
CREATE TABLE IF NOT EXISTS `data_signals_myisam` (
  `record_number` int(11) NOT NULL,
  `generic_signal_id` int(11) NOT NULL,
  `revision` smallint(6) NOT NULL AUTO_INCREMENT,
  `variant` varchar(20) NOT NULL DEFAULT '',
  `timestamp` datetime NOT NULL,
  `data_file_id` int(11) DEFAULT NULL,
  `data_file_key` text,
  `time0` double DEFAULT '0',
  `coefficient` double DEFAULT '1' COMMENT 'corresponds\r\nto coefficient_lev2V in channel_setup',
  `offset` double DEFAULT '0',
  `coefficient_V2unit` double DEFAULT '1',
  `time_axis_id` int(11) DEFAULT NULL COMMENT 'time_axis_id is set when different to the generic signal,\r\ne.g. when connected to a different DAQ\n',
  `time_axis_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis1_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis2_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis3_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis4_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis5_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis6_revision` smallint(6) NOT NULL DEFAULT '1',
  `time_axis_variant` varchar(20) NOT NULL,
  `axis1_variant` varchar(20) NOT NULL,
  `axis2_variant` varchar(20) NOT NULL,
  `axis3_variant` varchar(20) NOT NULL,
  `axis4_variant` varchar(20) NOT NULL,
  `axis5_variant` varchar(20) NOT NULL,
  `axis6_variant` varchar(20) NOT NULL,
  `note` varchar(140) DEFAULT NULL,
  `computer_id` smallint(6) DEFAULT NULL,
  `board_id` smallint(6) DEFAULT NULL,
  `channel_id` smallint(6) DEFAULT NULL,
  `data_quality` enum('UNKNOWN','POOR','GOOD','VALIDATED') NOT NULL DEFAULT 'UNKNOWN',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`record_number`,`generic_signal_id`,`variant`,`revision`),
  KEY `fk_data_file_id_idx` (`data_file_id`),
  KEY `fk_record_number_idx` (`record_number`),
  KEY `fk_data_signals_1_idx` (`computer_id`,`board_id`,`channel_id`),
  KEY `fk_generic_channel_id_idx` (`generic_signal_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Information of signals for particular records (instances of ';

-- --------------------------------------------------------

--
-- Table structure for table `data_signal_parameters`
--

DROP TABLE IF EXISTS `data_signal_parameters`;
CREATE TABLE IF NOT EXISTS `data_signal_parameters` (
  `generic_signal_id` int(11) NOT NULL,
  `record_number` int(11) NOT NULL,
  `revision` smallint(6) NOT NULL,
  `variant` varchar(20) NOT NULL DEFAULT '',
  `gs_parameters` text COMMENT 'generic signal parameters (from signal_setup)',
  `daq_parameters` text COMMENT 'daq channel parameters (from channel_setup)',
  PRIMARY KEY (`generic_signal_id`,`record_number`,`revision`,`variant`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='data signal parameters (references data_signals)';

-- --------------------------------------------------------

--
-- Table structure for table `data_sources`
--

DROP TABLE IF EXISTS `data_sources`;
CREATE TABLE IF NOT EXISTS `data_sources` (
  `data_source_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text,
  `subdirectory` varchar(45) NOT NULL,
  PRIMARY KEY (`data_source_id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of data sources (providers) and their descriptions.';

-- --------------------------------------------------------

--
-- Table structure for table `da_computers`
--

DROP TABLE IF EXISTS `da_computers`;
CREATE TABLE IF NOT EXISTS `da_computers` (
  `computer_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `computer_name` varchar(45) NOT NULL,
  `location` varchar(45) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`computer_id`),
  KEY `computer_name_idx` (`computer_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of data acquisition computers.';

-- --------------------------------------------------------

--
-- Table structure for table `file_status`
--

DROP TABLE IF EXISTS `file_status`;
CREATE TABLE IF NOT EXISTS `file_status` (
  `data_file_id` int(11) NOT NULL,
  `file_ready` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`data_file_id`),
  KEY `fk_file_status_1_idx` (`data_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Indicates whether file can be read (is ready).';

-- --------------------------------------------------------

--
-- Table structure for table `FireSignal_Event_IDs`
--

DROP TABLE IF EXISTS `FireSignal_Event_IDs`;
CREATE TABLE IF NOT EXISTS `FireSignal_Event_IDs` (
  `event_number` int(11) NOT NULL,
  `event_id` varchar(200) NOT NULL,
  `record_number` int(11) NOT NULL,
  PRIMARY KEY (`event_number`,`event_id`),
  UNIQUE KEY `record_number_UNIQUE` (`record_number`),
  KEY `fk_FireSignal_Event_IDs_1_idx` (`record_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `generic_signals`
--

DROP TABLE IF EXISTS `generic_signals`;
CREATE TABLE IF NOT EXISTS `generic_signals` (
  `generic_signal_id` int(11) NOT NULL AUTO_INCREMENT,
  `generic_signal_name` varchar(127) NOT NULL,
  `alias` varchar(30) DEFAULT NULL,
  `first_record_number` int(11) NOT NULL DEFAULT '1' COMMENT 'channel validity from first to last shot no.',
  `last_record_number` int(11) NOT NULL DEFAULT '-1' COMMENT '-1 for infinite validity',
  `data_source_id` smallint(6) NOT NULL,
  `time_axis_id` int(11) DEFAULT NULL,
  `axis1_id` int(11) DEFAULT NULL,
  `axis2_id` int(11) DEFAULT NULL,
  `axis3_id` int(11) DEFAULT NULL,
  `axis4_id` int(11) DEFAULT NULL,
  `axis5_id` int(11) DEFAULT NULL,
  `axis6_id` int(11) DEFAULT NULL,
  `units` varchar(30) DEFAULT NULL,
  `description` text,
  `signal_type` enum('FILE','LINEAR') DEFAULT 'FILE',
  `wildcard` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`generic_signal_id`),
  UNIQUE KEY `idgeneric_channels_UNIQUE` (`generic_signal_id`),
  UNIQUE KEY `id_name_source_id` (`generic_signal_name`,`data_source_id`),
  UNIQUE KEY `alias_UNQ` (`alias`),
  KEY `fk_generic_signals_1_idx` (`data_source_id`),
  KEY `fk_generic_signals_2_idx` (`time_axis_id`),
  KEY `fk_generic_signals_3_idx` (`axis1_id`),
  KEY `fk_generic_signals_4_idx` (`axis2_id`),
  KEY `fk_generic_signals_5_idx` (`axis3_id`),
  KEY `fk_generic_signals_6_idx` (`axis4_id`),
  KEY `fk_generic_signals_7_idx` (`axis5_id`),
  KEY `fk_generic_signals_8_idx` (`axis6_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of signal names, i.e. physical quantities, with units a';

-- --------------------------------------------------------

--
-- Stand-in structure for view `last_attach_time`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `last_attach_time`;
CREATE TABLE IF NOT EXISTS `last_attach_time` (
`computer_id` smallint(6)
,`board_id` smallint(6)
,`channel_id` smallint(6)
,`attach_time` datetime
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `last_channel_attach`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `last_channel_attach`;
CREATE TABLE IF NOT EXISTS `last_channel_attach` (
`computer_id` smallint(6)
,`board_id` smallint(6)
,`channel_id` smallint(6)
,`attach_time` datetime
,`attached_generic_signal_id` int(11)
,`offset` double
,`coefficient_lev2V` double
,`coefficient_V2unit` double
,`time_axis_id` int(11)
,`parameters` text
,`uid` int(11)
,`note` varchar(140)
);

-- --------------------------------------------------------

--
-- Table structure for table `postproc_log`
--

DROP TABLE IF EXISTS `postproc_log`;
CREATE TABLE IF NOT EXISTS `postproc_log` (
  `rule_id` int(11) NOT NULL,
  `record_number` int(11) NOT NULL,
  `run_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `input_signal_revisions` varchar(250) DEFAULT NULL COMMENT 'gs_id:revision, ...',
  `success` tinyint(1) NOT NULL,
  `output_signal_revisions` varchar(250) DEFAULT NULL COMMENT 'gs_id:revision, ...',
  `log` text,
  PRIMARY KEY (`rule_id`,`record_number`,`run_id`),
  KEY `fk_postproc_log_1_idx` (`rule_id`),
  KEY `fk_postproc_log_3_idx` (`record_number`,`run_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `postproc_rules`
--

DROP TABLE IF EXISTS `postproc_rules`;
CREATE TABLE IF NOT EXISTS `postproc_rules` (
  `rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_name` varchar(45) NOT NULL,
  `func` varchar(100) NOT NULL,
  `kwargs` text COMMENT 'JSON serialized keyword arguments of func',
  `description` text,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rule_id`),
  UNIQUE KEY `rule_name_UNIQUE` (`rule_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `postproc_run_log`
--

DROP TABLE IF EXISTS `postproc_run_log`;
CREATE TABLE IF NOT EXISTS `postproc_run_log` (
  `run_id` int(11) NOT NULL AUTO_INCREMENT,
  `record_number` int(11) NOT NULL,
  `status` enum('QUEUE','START','STOP') NOT NULL,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`run_id`,`status`),
  KEY `fk_postproc_run_log_1_idx` (`record_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `record_directories`
--

DROP TABLE IF EXISTS `record_directories`;
CREATE TABLE IF NOT EXISTS `record_directories` (
  `record_number` int(11) NOT NULL,
  `data_directory` varchar(100) NOT NULL,
  PRIMARY KEY (`record_number`),
  UNIQUE KEY `record_number_UNIQUE` (`record_number`),
  UNIQUE KEY `data_directory_UNIQUE` (`data_directory`),
  KEY `fk_record_directories_1_idx` (`record_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rule_inputs`
--

DROP TABLE IF EXISTS `rule_inputs`;
CREATE TABLE IF NOT EXISTS `rule_inputs` (
  `rule_id` int(11) NOT NULL,
  `generic_signal_id` int(11) NOT NULL,
  `note` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`rule_id`,`generic_signal_id`),
  KEY `fk_rule_inputs_1_idx` (`rule_id`),
  KEY `fk_rule_inputs_2_idx` (`generic_signal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rule_outputs`
--

DROP TABLE IF EXISTS `rule_outputs`;
CREATE TABLE IF NOT EXISTS `rule_outputs` (
  `rule_id` int(11) NOT NULL,
  `generic_signal_id` int(11) NOT NULL,
  `note` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`rule_id`,`generic_signal_id`),
  KEY `fk_rule_outputs_1_idx` (`rule_id`),
  KEY `fk_rule_outputs_2_idx` (`generic_signal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `shot_database`
--

DROP TABLE IF EXISTS `shot_database`;
CREATE TABLE IF NOT EXISTS `shot_database` (
  `record_number` int(11) NOT NULL,
  `record_time` datetime NOT NULL,
  `record_type` enum('EXP','VOID','MODEL') NOT NULL,
  `description` text,
  PRIMARY KEY (`record_number`),
  UNIQUE KEY `record_number_UNIQUE` (`record_number`),
  KEY `shot_time_INDEX` (`record_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `shot_database_pub`
--

DROP TABLE IF EXISTS `shot_database_pub`;
CREATE TABLE IF NOT EXISTS `shot_database_pub` (
  `record_number` int(11) NOT NULL,
  `record_time` datetime NOT NULL,
  `record_type` enum('VOID','MODEL') NOT NULL,
  `description` text,
  PRIMARY KEY (`record_number`),
  UNIQUE KEY `record_number_UNIQUE` (`record_number`),
  KEY `shot_time_INDEX` (`record_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `shot_database_pub`
--
DROP TRIGGER IF EXISTS `copy_to_shot_database`;
DELIMITER $$
CREATE TRIGGER `copy_to_shot_database` BEFORE INSERT ON `shot_database_pub` FOR EACH ROW INSERT INTO  `shot_database` (`record_number`,`record_time`, `record_type`, `description`) VALUES (NEW.record_number,NEW.record_time, NEW.record_type, NEW.description)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `signal_setup`
--

DROP TABLE IF EXISTS `signal_setup`;
CREATE TABLE IF NOT EXISTS `signal_setup` (
  `generic_signal_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `parameters` text COMMENT 'custom parameters in JSON',
  PRIMARY KEY (`generic_signal_id`,`timestamp`),
  KEY `fk_signal_setup_1_idx` (`generic_signal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='similat to channel setup, store persistent signal parameters';

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

DROP TABLE IF EXISTS `units`;
CREATE TABLE IF NOT EXISTS `units` (
  `unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_system_id` int(11) NOT NULL,
  `unit_name` varchar(20) NOT NULL,
  `m` smallint(6) NOT NULL DEFAULT '0',
  `kg` smallint(6) NOT NULL DEFAULT '0',
  `s` smallint(6) NOT NULL DEFAULT '0',
  `A` smallint(6) NOT NULL DEFAULT '0',
  `K` smallint(6) NOT NULL DEFAULT '0',
  `mol` smallint(6) NOT NULL DEFAULT '0',
  `cd` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`unit_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `unit_systems`
--

DROP TABLE IF EXISTS `unit_systems`;
CREATE TABLE IF NOT EXISTS `unit_systems` (
  `unit_system_id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_system_name` varchar(40) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `editable` tinyint(4) NOT NULL DEFAULT '1',
  `description` text,
  PRIMARY KEY (`unit_system_id`),
  UNIQUE KEY `unit_system_name` (`unit_system_name`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure for view `channel_attachments`
--
DROP TABLE IF EXISTS `channel_attachments`;

DROP VIEW IF EXISTS `channel_attachments`;
CREATE ALGORITHM=UNDEFINED DEFINER=CURRENT_USER  SQL SECURITY INVOKER VIEW `channel_attachments`  AS SELECT `DAQ_channels`.`computer_id` AS `computer_id`, `DAQ_channels`.`board_id` AS `board_id`, `DAQ_channels`.`channel_id` AS `channel_id`, `DAQ_channels`.`default_generic_signal_id` AS `default_generic_signal_id`, `last_channel_attach`.`attached_generic_signal_id` AS `attached_generic_signal_id`, `last_channel_attach`.`attach_time` AS `attach_time`, `last_channel_attach`.`offset` AS `offset`, `last_channel_attach`.`coefficient_lev2V` AS `coefficient_lev2V`, `last_channel_attach`.`coefficient_V2unit` AS `coefficient_V2unit`, `last_channel_attach`.`time_axis_id` AS `time_axis_id`, `last_channel_attach`.`parameters` AS `parameters`, `last_channel_attach`.`uid` AS `uid`, `last_channel_attach`.`note` AS `note`, `generic_signals`.`generic_signal_name` AS `attached_signal_name`, (`DAQ_channels`.`default_generic_signal_id` <> `last_channel_attach`.`attached_generic_signal_id`) AS `is_attached` FROM ((`DAQ_channels` join `last_channel_attach`) join `generic_signals`) WHERE ((`DAQ_channels`.`computer_id` = `last_channel_attach`.`computer_id`) AND (`DAQ_channels`.`board_id` = `last_channel_attach`.`board_id`) AND (`DAQ_channels`.`channel_id` = `last_channel_attach`.`channel_id`) AND (`generic_signals`.`generic_signal_id` = `last_channel_attach`.`attached_generic_signal_id`))  ;

-- --------------------------------------------------------

--
-- Structure for view `last_attach_time`
--
DROP TABLE IF EXISTS `last_attach_time`;

DROP VIEW IF EXISTS `last_attach_time`;
CREATE ALGORITHM=UNDEFINED DEFINER=CURRENT_USER  SQL SECURITY INVOKER VIEW `last_attach_time`  AS SELECT `channel_setup`.`computer_id` AS `computer_id`, `channel_setup`.`board_id` AS `board_id`, `channel_setup`.`channel_id` AS `channel_id`, max(`channel_setup`.`attach_time`) AS `attach_time` FROM `channel_setup` GROUP BY `channel_setup`.`computer_id`, `channel_setup`.`board_id`, `channel_setup`.`channel_id`  ;

-- --------------------------------------------------------

--
-- Structure for view `last_channel_attach`
--
DROP TABLE IF EXISTS `last_channel_attach`;

DROP VIEW IF EXISTS `last_channel_attach`;
CREATE ALGORITHM=UNDEFINED DEFINER=CURRENT_USER  SQL SECURITY INVOKER VIEW `last_channel_attach`  AS SELECT `channel_setup`.`computer_id` AS `computer_id`, `channel_setup`.`board_id` AS `board_id`, `channel_setup`.`channel_id` AS `channel_id`, `channel_setup`.`attach_time` AS `attach_time`, `channel_setup`.`attached_generic_signal_id` AS `attached_generic_signal_id`, `channel_setup`.`offset` AS `offset`, `channel_setup`.`coefficient_lev2V` AS `coefficient_lev2V`, `channel_setup`.`coefficient_V2unit` AS `coefficient_V2unit`, `channel_setup`.`time_axis_id` AS `time_axis_id`, `channel_setup`.`parameters` AS `parameters`, `channel_setup`.`uid` AS `uid`, `channel_setup`.`note` AS `note` FROM (`channel_setup` join `last_attach_time` on(((`channel_setup`.`computer_id` = `last_attach_time`.`computer_id`) and (`channel_setup`.`board_id` = `last_attach_time`.`board_id`) and (`channel_setup`.`channel_id` = `last_attach_time`.`channel_id`) and (`channel_setup`.`attach_time` = `last_attach_time`.`attach_time`))))  ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `channel_setup`
--
ALTER TABLE `channel_setup`
  ADD CONSTRAINT `fk_channel_setup_1` FOREIGN KEY (`computer_id`,`board_id`,`channel_id`) REFERENCES `DAQ_channels` (`computer_id`, `board_id`, `channel_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_physical_setup_generic_signals1` FOREIGN KEY (`attached_generic_signal_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `DAQ_channels`
--
ALTER TABLE `DAQ_channels`
  ADD CONSTRAINT `fk_detach_ids_generic_signals1` FOREIGN KEY (`default_generic_signal_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_physical_channels_1` FOREIGN KEY (`computer_id`) REFERENCES `da_computers` (`computer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `data_files`
--
ALTER TABLE `data_files`
  ADD CONSTRAINT `fk_data_files_1` FOREIGN KEY (`data_source_id`) REFERENCES `data_sources` (`data_source_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `shot_number` FOREIGN KEY (`record_number`) REFERENCES `shot_database` (`record_number`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `data_signals`
--
ALTER TABLE `data_signals`
  ADD CONSTRAINT `fk_data_file_id` FOREIGN KEY (`data_file_id`) REFERENCES `data_files` (`data_file_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_data_signals_1` FOREIGN KEY (`computer_id`,`board_id`,`channel_id`) REFERENCES `DAQ_channels` (`computer_id`, `board_id`, `channel_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_generic_channel_id` FOREIGN KEY (`generic_signal_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_record_number` FOREIGN KEY (`record_number`) REFERENCES `shot_database` (`record_number`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `data_signal_parameters`
--
ALTER TABLE `data_signal_parameters`
  ADD CONSTRAINT `fk_signal_parameters_1` FOREIGN KEY (`generic_signal_id`,`record_number`,`revision`,`variant`) REFERENCES `data_signals` (`generic_signal_id`, `record_number`, `revision`, `variant`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `file_status`
--
ALTER TABLE `file_status`
  ADD CONSTRAINT `fk_file_status_1` FOREIGN KEY (`data_file_id`) REFERENCES `data_files` (`data_file_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `FireSignal_Event_IDs`
--
ALTER TABLE `FireSignal_Event_IDs`
  ADD CONSTRAINT `fk_FireSignal_Event_IDs_1` FOREIGN KEY (`record_number`) REFERENCES `shot_database` (`record_number`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `generic_signals`
--
ALTER TABLE `generic_signals`
  ADD CONSTRAINT `fk_generic_signals_1` FOREIGN KEY (`data_source_id`) REFERENCES `data_sources` (`data_source_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_generic_signals_2` FOREIGN KEY (`time_axis_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_generic_signals_3` FOREIGN KEY (`axis1_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_generic_signals_4` FOREIGN KEY (`axis2_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_generic_signals_5` FOREIGN KEY (`axis3_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_generic_signals_6` FOREIGN KEY (`axis4_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_generic_signals_7` FOREIGN KEY (`axis5_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_generic_signals_8` FOREIGN KEY (`axis6_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `postproc_log`
--
ALTER TABLE `postproc_log`
  ADD CONSTRAINT `fk_postproc_log_1` FOREIGN KEY (`rule_id`) REFERENCES `postproc_rules` (`rule_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_postproc_log_3` FOREIGN KEY (`record_number`,`run_id`) REFERENCES `postproc_run_log` (`record_number`, `run_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `postproc_run_log`
--
ALTER TABLE `postproc_run_log`
  ADD CONSTRAINT `fk_postproc_run_log_1` FOREIGN KEY (`record_number`) REFERENCES `shot_database` (`record_number`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `record_directories`
--
ALTER TABLE `record_directories`
  ADD CONSTRAINT `fk_record_directories_1` FOREIGN KEY (`record_number`) REFERENCES `shot_database` (`record_number`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `rule_inputs`
--
ALTER TABLE `rule_inputs`
  ADD CONSTRAINT `fk_rule_inputs_1` FOREIGN KEY (`rule_id`) REFERENCES `postproc_rules` (`rule_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rule_inputs_2` FOREIGN KEY (`generic_signal_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `rule_outputs`
--
ALTER TABLE `rule_outputs`
  ADD CONSTRAINT `fk_rule_outputs_1` FOREIGN KEY (`rule_id`) REFERENCES `postproc_rules` (`rule_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rule_outputs_2` FOREIGN KEY (`generic_signal_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `signal_setup`
--
ALTER TABLE `signal_setup`
  ADD CONSTRAINT `fk_signal_setup_1` FOREIGN KEY (`generic_signal_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
