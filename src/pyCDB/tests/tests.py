#!/usr/bin/env python
from pyCDB.tests.test_fs_writer import *
from pyCDB.tests.test_client import *
from pyCDB.tests.test_daq_client import *
from pyCDB.tests.test_syntax_correctness import *
from pyCDB.tests.test_matlab import *
from pyCDB.tests.test_slicing import *
from pyCDB.tests.test_mysql import *
from pyCDB.tests.test_epics_archiver_appliance_api import *
from pyCDB.tests.test_epics_archiver_appliance_utils import *
from pyCDB.tests.test_pyCDBBase import *
from pyCDB.tests.cdb_test_case import run


if __name__ == '__main__':
    run()
