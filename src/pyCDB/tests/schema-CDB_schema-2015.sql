
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `DAQ_channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DAQ_channels` (
  `computer_id` smallint(6) NOT NULL,
  `board_id` smallint(6) NOT NULL,
  `channel_id` smallint(6) NOT NULL,
  `default_generic_signal_id` int(11) NOT NULL,
  `note` varchar(500) DEFAULT NULL,
  `nodeuniqueid` varchar(90) DEFAULT NULL COMMENT 'FireSignal node id',
  `hardwareuniqueid` varchar(90) DEFAULT NULL COMMENT 'FireSignal hardware id',
  `parameteruniqueid` varchar(90) DEFAULT NULL COMMENT 'FireSignal parameter id ',
  PRIMARY KEY (`computer_id`,`board_id`,`channel_id`),
  UNIQUE KEY `generic_signal_id_UNIQUE` (`default_generic_signal_id`),
  UNIQUE KEY `FS_id_index` (`nodeuniqueid`,`hardwareuniqueid`,`parameteruniqueid`),
  KEY `fk_detach_ids_generic_signals1_idx` (`default_generic_signal_id`),
  KEY `fk_physical_channels_1_idx` (`computer_id`),
  CONSTRAINT `fk_detach_ids_generic_signals1` FOREIGN KEY (`default_generic_signal_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_physical_channels_1` FOREIGN KEY (`computer_id`) REFERENCES `da_computers` (`computer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of A/D channels, identified by computer, board and chan';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `FireSignal_Event_IDs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FireSignal_Event_IDs` (
  `event_number` int(11) NOT NULL,
  `event_id` varchar(200) NOT NULL,
  `record_number` int(11) NOT NULL,
  PRIMARY KEY (`event_number`,`event_id`),
  UNIQUE KEY `record_number_UNIQUE` (`record_number`),
  KEY `fk_FireSignal_Event_IDs_1_idx` (`record_number`),
  CONSTRAINT `fk_FireSignal_Event_IDs_1` FOREIGN KEY (`record_number`) REFERENCES `shot_database` (`record_number`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP VIEW IF EXISTS `channel_attachments`;
DROP TABLE IF EXISTS `channel_attachments`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE IF NOT EXISTS `channel_attachments` (
`computer_id` smallint(6)
,`board_id` smallint(6)
,`channel_id` smallint(6)
,`default_generic_signal_id` int(11)
,`attached_generic_signal_id` int(11)
,`attach_time` datetime
,`offset` double
,`coefficient_lev2V` double
,`coefficient_V2unit` double
,`time_axis_id` int(11)
,`parameters` text
,`uid` int(11)
,`note` varchar(140)
,`attached_signal_name` varchar(127)
,`is_attached` int(1)
);
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `channel_setup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_setup` (
  `attached_generic_signal_id` int(11) NOT NULL,
  `attach_time` datetime NOT NULL,
  `computer_id` smallint(6) NOT NULL,
  `board_id` smallint(6) NOT NULL,
  `channel_id` smallint(6) NOT NULL,
  `uid` int(11) DEFAULT NULL COMMENT 'user id of the event creator',
  `note` varchar(140) DEFAULT NULL,
  `coefficient_lev2V` double DEFAULT '1' COMMENT 'conversion from channel level (raw) to Volts',
  `offset` double DEFAULT '0' COMMENT 'offset in channel level (raw) units',
  `coefficient_V2unit` double DEFAULT '1' COMMENT 'conversion factor from Volts to physical units (of the attached generic signal)',
  `time_axis_id` int(11) DEFAULT NULL,
  `parameters` text,
  PRIMARY KEY (`attached_generic_signal_id`,`attach_time`),
  UNIQUE KEY `unique_attach_time_idx` (`computer_id`,`board_id`,`channel_id`,`attach_time`),
  KEY `channel_id_index` (`computer_id`,`board_id`,`channel_id`),
  KEY `fk_physical_setup_generic_signals1_idx` (`attached_generic_signal_id`),
  KEY `fk_channel_setup_1_idx` (`computer_id`,`board_id`,`channel_id`),
  CONSTRAINT `fk_channel_setup_1` FOREIGN KEY (`computer_id`, `board_id`, `channel_id`) REFERENCES `DAQ_channels` (`computer_id`, `board_id`, `channel_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_physical_setup_generic_signals1` FOREIGN KEY (`attached_generic_signal_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Record and shot numbers. Includes data directories (relative';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `da_computers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_computers` (
  `computer_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `computer_name` varchar(45) NOT NULL,
  `location` varchar(45) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`computer_id`),
  KEY `computer_name_idx` (`computer_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of data acquisition computers.';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `data_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_files` (
  `data_file_id` int(11) NOT NULL AUTO_INCREMENT,
  `data_source_id` smallint(6) NOT NULL,
  `record_number` int(11) NOT NULL,
  `data_format` enum('HDF5','NETCDF4','GENERIC') NOT NULL,
  `collection_name` varchar(127) NOT NULL COMMENT 'filename = collection_name.revision.ext',
  `revision` smallint(6) NOT NULL DEFAULT '1',
  `file_name` varchar(135) NOT NULL,
  PRIMARY KEY (`data_file_id`),
  UNIQUE KEY `file_UNIQUE` (`record_number`,`data_source_id`,`collection_name`,`revision`),
  KEY `data_source_id` (`data_source_id`),
  KEY `fk_data_files_1_idx` (`data_source_id`),
  KEY `shot_number_idx` (`record_number`),
  CONSTRAINT `fk_data_files_1` FOREIGN KEY (`data_source_id`) REFERENCES `data_sources` (`data_source_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `shot_number` FOREIGN KEY (`record_number`) REFERENCES `shot_database` (`record_number`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of data files, i.e. files that are physically on disks.';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `data_signal_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_signal_parameters` (
  `generic_signal_id` int(11) NOT NULL,
  `record_number` int(11) NOT NULL,
  `revision` smallint(6) NOT NULL,
  `variant` varchar(20) NOT NULL DEFAULT '',
  `gs_parameters` text COMMENT 'generic signal parameters (from signal_setup)',
  `daq_parameters` text COMMENT 'daq channel parameters (from channel_setup)',
  PRIMARY KEY (`generic_signal_id`,`record_number`,`revision`,`variant`),
  CONSTRAINT `fk_signal_parameters_1` FOREIGN KEY (`generic_signal_id`, `record_number`, `revision`, `variant`) REFERENCES `data_signals` (`generic_signal_id`, `record_number`, `revision`, `variant`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='data signal parameters (references data_signals)';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `data_signals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_signals` (
  `data_signal_id` int(11) NOT NULL AUTO_INCREMENT,
  `record_number` int(11) NOT NULL,
  `generic_signal_id` int(11) NOT NULL,
  `revision` smallint(6) NOT NULL DEFAULT '1',
  `variant` varchar(20) NOT NULL DEFAULT '',
  `timestamp` datetime NOT NULL,
  `data_file_id` int(11) DEFAULT NULL,
  `data_file_key` varchar(127) DEFAULT NULL COMMENT 'placement in the data file',
  `time0` double DEFAULT '0',
  `coefficient` double DEFAULT '1' COMMENT 'corresponds\r\nto coefficient_lev2V in channel_setup',
  `offset` double DEFAULT '0',
  `coefficient_V2unit` double DEFAULT '1',
  `time_axis_id` int(11) DEFAULT NULL COMMENT 'time_axis_id is set when different to the generic signal,\r\ne.g. when connected to a different DAQ\n',
  `time_axis_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis1_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis2_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis3_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis4_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis5_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis6_revision` smallint(6) NOT NULL DEFAULT '1',
  `time_axis_variant` varchar(20) NOT NULL DEFAULT '',
  `axis1_variant` varchar(20) NOT NULL DEFAULT '',
  `axis2_variant` varchar(20) NOT NULL DEFAULT '',
  `axis3_variant` varchar(20) NOT NULL DEFAULT '',
  `axis4_variant` varchar(20) NOT NULL DEFAULT '',
  `axis5_variant` varchar(20) NOT NULL DEFAULT '',
  `axis6_variant` varchar(20) NOT NULL DEFAULT '',
  `error_signal_id` int(11) DEFAULT NULL,
  `note` varchar(140) DEFAULT NULL,
  `computer_id` smallint(6) DEFAULT NULL,
  `board_id` smallint(6) DEFAULT NULL,
  `channel_id` smallint(6) DEFAULT NULL,
  `data_quality` enum('UNKNOWN','POOR','GOOD','VALIDATED') NOT NULL DEFAULT 'UNKNOWN',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`data_signal_id`),
  UNIQUE KEY (`generic_signal_id`, `record_number`,`revision`,`variant`),
  KEY `fk_data_file_id_idx` (`data_file_id`),
  KEY `fk_record_number_idx` (`record_number`),
  KEY `fk_data_signals_1_idx` (`computer_id`,`board_id`,`channel_id`),
  KEY `fk_generic_channel_id_idx` (`generic_signal_id`),
  CONSTRAINT `fk_data_file_id` FOREIGN KEY (`data_file_id`) REFERENCES `data_files` (`data_file_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_data_signals_1` FOREIGN KEY (`computer_id`, `board_id`, `channel_id`) REFERENCES `DAQ_channels` (`computer_id`, `board_id`, `channel_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_generic_channel_id` FOREIGN KEY (`generic_signal_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_record_number` FOREIGN KEY (`record_number`) REFERENCES `shot_database` (`record_number`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_error` FOREIGN KEY (`error_signal_id`) REFERENCES `data_signals` (`data_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Information of signals for particular records (instances of ';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `data_signals_myisam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_signals_myisam` (
  `record_number` int(11) NOT NULL,
  `generic_signal_id` int(11) NOT NULL,
  `revision` smallint(6) NOT NULL AUTO_INCREMENT,
  `variant` varchar(20) NOT NULL DEFAULT '',
  `timestamp` datetime NOT NULL,
  `data_file_id` int(11) DEFAULT NULL,
  `data_file_key` text,
  `time0` double DEFAULT '0',
  `coefficient` double DEFAULT '1' COMMENT 'corresponds\r\nto coefficient_lev2V in channel_setup',
  `offset` double DEFAULT '0',
  `coefficient_V2unit` double DEFAULT '1',
  `time_axis_id` int(11) DEFAULT NULL COMMENT 'time_axis_id is set when different to the generic signal,\r\ne.g. when connected to a different DAQ\n',
  `time_axis_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis1_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis2_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis3_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis4_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis5_revision` smallint(6) NOT NULL DEFAULT '1',
  `axis6_revision` smallint(6) NOT NULL DEFAULT '1',
  `time_axis_variant` varchar(20) NOT NULL,
  `axis1_variant` varchar(20) NOT NULL,
  `axis2_variant` varchar(20) NOT NULL,
  `axis3_variant` varchar(20) NOT NULL,
  `axis4_variant` varchar(20) NOT NULL,
  `axis5_variant` varchar(20) NOT NULL,
  `axis6_variant` varchar(20) NOT NULL,
  `note` varchar(140) DEFAULT NULL,
  `computer_id` smallint(6) DEFAULT NULL,
  `board_id` smallint(6) DEFAULT NULL,
  `channel_id` smallint(6) DEFAULT NULL,
  `data_quality` enum('UNKNOWN','POOR','GOOD','VALIDATED') NOT NULL DEFAULT 'UNKNOWN',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`record_number`,`generic_signal_id`,`variant`,`revision`),
  KEY `fk_data_file_id_idx` (`data_file_id`),
  KEY `fk_record_number_idx` (`record_number`),
  KEY `fk_data_signals_1_idx` (`computer_id`,`board_id`,`channel_id`),
  KEY `fk_generic_channel_id_idx` (`generic_signal_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Information of signals for particular records (instances of ';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `data_sources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_sources` (
  `data_source_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text,
  `subdirectory` varchar(45) NOT NULL,
  PRIMARY KEY (`data_source_id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of data sources (providers) and their descriptions.';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `file_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_status` (
  `data_file_id` int(11) NOT NULL,
  `file_ready` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`data_file_id`),
  KEY `fk_file_status_1_idx` (`data_file_id`),
  CONSTRAINT `fk_file_status_1` FOREIGN KEY (`data_file_id`) REFERENCES `data_files` (`data_file_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Indicates whether file can be read (is ready).';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `generic_signals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `generic_signals` (
  `generic_signal_id` int(11) NOT NULL AUTO_INCREMENT,
  `generic_signal_name` varchar(127) NOT NULL,
  `alias` varchar(30) DEFAULT NULL,
  `first_record_number` int(11) NOT NULL DEFAULT '1' COMMENT 'channel validity from first to last shot no.',
  `last_record_number` int(11) NOT NULL DEFAULT '-1' COMMENT '-1 for infinite validity',
  `data_source_id` smallint(6) NOT NULL,
  `time_axis_id` int(11) DEFAULT NULL,
  `axis1_id` int(11) DEFAULT NULL,
  `axis2_id` int(11) DEFAULT NULL,
  `axis3_id` int(11) DEFAULT NULL,
  `axis4_id` int(11) DEFAULT NULL,
  `axis5_id` int(11) DEFAULT NULL,
  `axis6_id` int(11) DEFAULT NULL,
  `units` varchar(30) DEFAULT NULL,
  `description` text,
  `signal_type` enum('FILE','LINEAR') DEFAULT 'FILE',
  `wildcard` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`generic_signal_id`),
  UNIQUE KEY `idgeneric_channels_UNIQUE` (`generic_signal_id`),
  UNIQUE KEY `id_name_source_id` (`generic_signal_name`,`data_source_id`),
  UNIQUE KEY `alias_UNQ` (`alias`),
  KEY `fk_generic_signals_1_idx` (`data_source_id`),
  KEY `fk_generic_signals_2_idx` (`time_axis_id`),
  KEY `fk_generic_signals_3_idx` (`axis1_id`),
  KEY `fk_generic_signals_4_idx` (`axis2_id`),
  KEY `fk_generic_signals_5_idx` (`axis3_id`),
  KEY `fk_generic_signals_6_idx` (`axis4_id`),
  KEY `fk_generic_signals_7_idx` (`axis5_id`),
  KEY `fk_generic_signals_8_idx` (`axis6_id`),
  CONSTRAINT `fk_generic_signals_1` FOREIGN KEY (`data_source_id`) REFERENCES `data_sources` (`data_source_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_generic_signals_2` FOREIGN KEY (`time_axis_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_generic_signals_3` FOREIGN KEY (`axis1_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_generic_signals_4` FOREIGN KEY (`axis2_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_generic_signals_5` FOREIGN KEY (`axis3_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_generic_signals_6` FOREIGN KEY (`axis4_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_generic_signals_7` FOREIGN KEY (`axis5_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_generic_signals_8` FOREIGN KEY (`axis6_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of signal names, i.e. physical quantities, with units a';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `last_attach_time`;
/*!50001 DROP VIEW IF EXISTS `last_attach_time`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `last_attach_time` (
  `computer_id` tinyint NOT NULL,
  `board_id` tinyint NOT NULL,
  `channel_id` tinyint NOT NULL,
  `attach_time` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `last_channel_attach`;
/*!50001 DROP VIEW IF EXISTS `last_channel_attach`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `last_channel_attach` (
  `computer_id` tinyint NOT NULL,
  `board_id` tinyint NOT NULL,
  `channel_id` tinyint NOT NULL,
  `attach_time` tinyint NOT NULL,
  `attached_generic_signal_id` tinyint NOT NULL,
  `offset` tinyint NOT NULL,
  `coefficient_lev2V` tinyint NOT NULL,
  `coefficient_V2unit` tinyint NOT NULL,
  `time_axis_id` tinyint NOT NULL,
  `parameters` tinyint NOT NULL,
  `uid` tinyint NOT NULL,
  `note` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `postproc_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `postproc_log` (
  `rule_id` int(11) NOT NULL,
  `record_number` int(11) NOT NULL,
  `run_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `input_signal_revisions` varchar(250) DEFAULT NULL COMMENT 'gs_id:revision, ...',
  `success` tinyint(1) NOT NULL,
  `output_signal_revisions` varchar(250) DEFAULT NULL COMMENT 'gs_id:revision, ...',
  `log` text,
  PRIMARY KEY (`rule_id`,`record_number`,`run_id`),
  KEY `fk_postproc_log_1_idx` (`rule_id`),
  KEY `fk_postproc_log_3_idx` (`record_number`,`run_id`),
  CONSTRAINT `fk_postproc_log_1` FOREIGN KEY (`rule_id`) REFERENCES `postproc_rules` (`rule_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_postproc_log_3` FOREIGN KEY (`record_number`, `run_id`) REFERENCES `postproc_run_log` (`record_number`, `run_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `postproc_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `postproc_rules` (
  `rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_name` varchar(45) NOT NULL,
  `func` varchar(100) NOT NULL,
  `kwargs` text COMMENT 'JSON serialized keyword arguments of func',
  `description` text,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rule_id`),
  UNIQUE KEY `rule_name_UNIQUE` (`rule_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `postproc_run_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `postproc_run_log` (
  `run_id` int(11) NOT NULL AUTO_INCREMENT,
  `record_number` int(11) NOT NULL,
  `status` enum('QUEUE','START','STOP') NOT NULL,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`run_id`,`status`),
  KEY `fk_postproc_run_log_1_idx` (`record_number`),
  CONSTRAINT `fk_postproc_run_log_1` FOREIGN KEY (`record_number`) REFERENCES `shot_database` (`record_number`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `record_directories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `record_directories` (
  `record_number` int(11) NOT NULL,
  `data_directory` varchar(100) NOT NULL,
  PRIMARY KEY (`record_number`),
  UNIQUE KEY `record_number_UNIQUE` (`record_number`),
  UNIQUE KEY `data_directory_UNIQUE` (`data_directory`),
  KEY `fk_record_directories_1_idx` (`record_number`),
  CONSTRAINT `fk_record_directories_1` FOREIGN KEY (`record_number`) REFERENCES `shot_database` (`record_number`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `rule_inputs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rule_inputs` (
  `rule_id` int(11) NOT NULL,
  `generic_signal_id` int(11) NOT NULL,
  `note` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`rule_id`,`generic_signal_id`),
  KEY `fk_rule_inputs_1_idx` (`rule_id`),
  KEY `fk_rule_inputs_2_idx` (`generic_signal_id`),
  CONSTRAINT `fk_rule_inputs_1` FOREIGN KEY (`rule_id`) REFERENCES `postproc_rules` (`rule_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_rule_inputs_2` FOREIGN KEY (`generic_signal_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `rule_outputs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rule_outputs` (
  `rule_id` int(11) NOT NULL,
  `generic_signal_id` int(11) NOT NULL,
  `note` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`rule_id`,`generic_signal_id`),
  KEY `fk_rule_outputs_1_idx` (`rule_id`),
  KEY `fk_rule_outputs_2_idx` (`generic_signal_id`),
  CONSTRAINT `fk_rule_outputs_1` FOREIGN KEY (`rule_id`) REFERENCES `postproc_rules` (`rule_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_rule_outputs_2` FOREIGN KEY (`generic_signal_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `shot_database`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shot_database` (
  `record_number` int(11) NOT NULL,
  `record_time` datetime NOT NULL,
  `record_type` enum('EXP','VOID','MODEL') NOT NULL,
  `description` text,
  PRIMARY KEY (`record_number`),
  UNIQUE KEY `record_number_UNIQUE` (`record_number`),
  KEY `shot_time_INDEX` (`record_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `shot_database_pub`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shot_database_pub` (
  `record_number` int(11) NOT NULL,
  `record_time` datetime NOT NULL,
  `record_type` enum('VOID','MODEL') NOT NULL,
  `description` text,
  PRIMARY KEY (`record_number`),
  UNIQUE KEY `record_number_UNIQUE` (`record_number`),
  KEY `shot_time_INDEX` (`record_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `signal_setup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `signal_setup` (
  `generic_signal_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `parameters` text COMMENT 'custom parameters in JSON',
  PRIMARY KEY (`generic_signal_id`,`timestamp`),
  KEY `fk_signal_setup_1_idx` (`generic_signal_id`),
  CONSTRAINT `fk_signal_setup_1` FOREIGN KEY (`generic_signal_id`) REFERENCES `generic_signals` (`generic_signal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='similat to channel setup, store persistent signal parameters';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `unit_systems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unit_systems` (
  `unit_system_id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_system_name` varchar(40) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `editable` tinyint(4) NOT NULL DEFAULT '1',
  `description` text,
  PRIMARY KEY (`unit_system_id`),
  UNIQUE KEY `unit_system_name` (`unit_system_name`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `units` (
  `unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_system_id` int(11) NOT NULL,
  `unit_name` varchar(20) NOT NULL,
  `m` smallint(6) NOT NULL DEFAULT '0',
  `kg` smallint(6) NOT NULL DEFAULT '0',
  `s` smallint(6) NOT NULL DEFAULT '0',
  `A` smallint(6) NOT NULL DEFAULT '0',
  `K` smallint(6) NOT NULL DEFAULT '0',
  `mol` smallint(6) NOT NULL DEFAULT '0',
  `cd` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`unit_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50001 DROP TABLE IF EXISTS `channel_attachments`*/;
/*!50001 DROP VIEW IF EXISTS `channel_attachments`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `channel_attachments` AS select `DAQ_channels`.`computer_id` AS `computer_id`,`DAQ_channels`.`board_id` AS `board_id`,`DAQ_channels`.`channel_id` AS `channel_id`,`DAQ_channels`.`default_generic_signal_id` AS `default_generic_signal_id`,`last_channel_attach`.`attached_generic_signal_id` AS `attached_generic_signal_id`,`last_channel_attach`.`attach_time` AS `attach_time`,`last_channel_attach`.`offset` AS `offset`,`last_channel_attach`.`coefficient_lev2V` AS `coefficient_lev2V`,`last_channel_attach`.`coefficient_V2unit` AS `coefficient_V2unit`,`last_channel_attach`.`time_axis_id` AS `time_axis_id`,`last_channel_attach`.`parameters` AS `parameters`,`last_channel_attach`.`uid` AS `uid`,`last_channel_attach`.`note` AS `note`,`generic_signals`.`generic_signal_name` AS `attached_signal_name`,(`DAQ_channels`.`default_generic_signal_id` <> `last_channel_attach`.`attached_generic_signal_id`) AS `is_attached` from ((`DAQ_channels` join `last_channel_attach`) join `generic_signals`) where ((`DAQ_channels`.`computer_id` = `last_channel_attach`.`computer_id`) and (`DAQ_channels`.`board_id` = `last_channel_attach`.`board_id`) and (`DAQ_channels`.`channel_id` = `last_channel_attach`.`channel_id`) and (`generic_signals`.`generic_signal_id` = `last_channel_attach`.`attached_generic_signal_id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `last_attach_time`*/;
/*!50001 DROP VIEW IF EXISTS `last_attach_time`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `last_attach_time` AS select `channel_setup`.`computer_id` AS `computer_id`,`channel_setup`.`board_id` AS `board_id`,`channel_setup`.`channel_id` AS `channel_id`,max(`channel_setup`.`attach_time`) AS `attach_time` from `channel_setup` group by `channel_setup`.`computer_id`,`channel_setup`.`board_id`,`channel_setup`.`channel_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `last_channel_attach`*/;
/*!50001 DROP VIEW IF EXISTS `last_channel_attach`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `last_channel_attach` AS select `channel_setup`.`computer_id` AS `computer_id`,`channel_setup`.`board_id` AS `board_id`,`channel_setup`.`channel_id` AS `channel_id`,`channel_setup`.`attach_time` AS `attach_time`,`channel_setup`.`attached_generic_signal_id` AS `attached_generic_signal_id`,`channel_setup`.`offset` AS `offset`,`channel_setup`.`coefficient_lev2V` AS `coefficient_lev2V`,`channel_setup`.`coefficient_V2unit` AS `coefficient_V2unit`,`channel_setup`.`time_axis_id` AS `time_axis_id`,`channel_setup`.`parameters` AS `parameters`,`channel_setup`.`uid` AS `uid`,`channel_setup`.`note` AS `note` from (`channel_setup` join `last_attach_time` on(((`channel_setup`.`computer_id` = `last_attach_time`.`computer_id`) and (`channel_setup`.`board_id` = `last_attach_time`.`board_id`) and (`channel_setup`.`channel_id` = `last_attach_time`.`channel_id`) and (`channel_setup`.`attach_time` = `last_attach_time`.`attach_time`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

