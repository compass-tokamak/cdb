import unittest
import pytest
from .cdb_test_case import run

import numpy as np
import pyCDB.signal_ndarray as sn


class TestSignalNdArray(unittest.TestCase):
    arr_shape = (23, 37)
    arr_ax1 = np.arange(arr_shape[0])
    arr_ax2 = np.arange(arr_shape[1])
    arr = sn.SignalNdArray(np.random.random(arr_shape),
                                axes=(arr_ax1, arr_ax2))

    @classmethod
    def setUpClass(cls):
        '''Set up an array for whole class to make sure no clashes occur'''
        pass

    def _check_axes_dims(self, arr):
        self.assertEqual(len(arr.axes), len(arr.shape))
        for i in range(len(arr.axes)):
            self.assertEqual(arr.shape[i], arr.axes[i].shape[0])


    def test_signal_ndrarray_initialization(self):
        self.assertEqual(len(self.arr.axes), len(self.arr_shape))
        self._check_axes_dims(self.arr)
        for i in range(2):
            orig_ax = getattr(self, ('arr_ax%i' % (i+1)))
            self.assertTrue(self.arr.axes[i] is orig_ax)

    def test_dim_reduction(self):
        arr = np.mean(self.arr, axis=-1)
        self._check_axes_dims(arr)
        self.assertEqual(arr.shape, self.arr_shape[:-1])

    def test_dim_addition(self):
        arr = self.arr[:,:,np.newaxis]
        self.assertEqual(self.arr_shape + (1,), arr.shape)
        self._check_axes_dims(arr)


    def test_slicing(self):
        arr = self.arr[2::2,3:-2]
        self.assertEqual(arr.shape, (11, 32))
        self._check_axes_dims(arr)
        self.assertTrue(np.all(self.arr_ax2[3:-2] == arr.axes[1]))
        self.assertTrue(np.all(self.arr_ax1[2::2] == arr.axes[0]))

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_bool_mask(self):
        mask = self.arr[:,5] > 0
        arr = self.arr[mask,:]
        self._check_axes_dims(arr)
        self.assertTrue(np.all(self.arr_ax1[mask] == arr.axes[0]))

    def test_1D_slice(self):
        arr = self.arr[:,4]
        arr = arr[2:-4]
        self._check_axes_dims(arr)
        self.assertTrue(np.all(self.arr_ax1[2:-4] == arr.axes[0]))

    def test_get_item(self):
        arr = self.arr.copy()
        arr[2,4] = 4
        self.assertEqual(arr[2,4], 4)

    def test_implicit_slice(self):
        arr = self.arr[2]
        self._check_axes_dims(arr)
        self.assertEqual(arr.shape, (37,))
        arr = self.arr[2:]
        self._check_axes_dims(arr)
        self.assertEqual(arr.shape, (21, 37))

    def test_2D_short_slice(self):
        arr = self.arr[:,[2]]
        self._check_axes_dims(arr)
        self.assertEqual(arr.shape, (23, 1))
        self.assertTrue(np.all(arr.axes[0] == self.arr_ax1))
        self.assertTrue(np.all(self.arr_ax2[2] == arr.axes[1]))

    def test_complex_slicing(self):
        arr = self.arr[:5.12j, 3.6j:]
        self._check_axes_dims(arr)
        self.assertTrue(np.all(arr == self.arr[:6,4:]))

    def test_1D_complex_slicing(self):
        arr = self.arr[8,:]     # make 1D
        arr = arr[3.2j:]
        self._check_axes_dims(arr)
        self.assertTrue(np.all(arr == self.arr[8,4:]))

if __name__ == '__main__':
    run()
