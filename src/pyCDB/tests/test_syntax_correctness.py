from . import cdb_test_case  # Just for paths etc.
import unittest
import os
import glob
import importlib


class TestSyntaxCorrectness(unittest.TestCase):
    def test_syntax_correctness(self):
        '''Just tries to load used modules.

        If there is any syntax error, the test fails.
        '''
        directory = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
        files = glob.glob(os.path.join(directory, "*.py"))
        files = [os.path.basename(f).split(".")[0] for f in files]
        files = [f for f in files if not f.startswith("_") and not f == "mex_help"]
        for file in files:
            try:
                # print('--- test import {} ---'.format(file))
                importlib.import_module('pyCDB.' + file)
            except SyntaxError:
                print ("SyntaxError while importing \"{0}\":".format(file))
                raise
            except Exception as err:
                print ("Error importing module \"{0}\":".format(file))
                print ("    {0}".format(err))


if __name__ == '__main__':
    unittest.main()
