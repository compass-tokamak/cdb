import time

from .cdb_test_case import CDBTestCase, run

class TestDAQClient(CDBTestCase):
    def test_create_computer(self):
        self.assertRaises(TypeError, self.client.create_computer)
        name = self.random_name("comp")
        self.client.create_computer(name)
        self.assertTrue(self.client.get_computer_id(name) > 0)

    def test_update_calibration(self):
        pass

    def test_detach(self):
        channel = self.create_random_channel()
        ta = self.create_random_generic_signal(signal_type="LINEAR")
        gs = self.create_random_generic_signal()

        time.sleep(1.1) # Otherwise, foreign key error

        self.client.attach_channel(channel.computer_id, channel.board_id, channel.channel_id,
            generic_signal_id=gs.generic_signal_id, time_axis_id=ta.generic_signal_id)
        attachment = self.client.get_attachment_table(computer_id = channel.computer_id,
            board_id=channel.board_id, channel_id=channel.channel_id)[0]
        self.assertEqual(gs.generic_signal_id, attachment.attached_generic_signal_id)
        self.assertEqual(ta.generic_signal_id, attachment.time_axis_id)
        self.assertEqual(True, attachment.is_attached)

        time.sleep(1.1) # Otherwise, foreign key error

        self.client.detach_channel(channel.computer_id, channel.board_id, channel.channel_id)
        attachment = self.client.get_attachment_table(computer_id = channel.computer_id,
            board_id=channel.board_id, channel_id=channel.channel_id)[0]

        self.assertEqual(None, attachment.time_axis_id)
        self.assertEqual(channel.default_generic_signal_id, attachment.attached_generic_signal_id)
        self.assertEqual(False, attachment.is_attached)


if __name__ == '__main__':
    run()