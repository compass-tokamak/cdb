from .cdb_test_case import CDBTestCase, run

import pytest
import random
import string
from time import sleep
from pyCDB.client import OrderedDict, CDBClient
import pyCDB.client
import numpy as np
import numpy.testing
from pyCDB.pyCDBBase import CDBException

import pytest
import datetime


class TestCDBClientUtils:

    def test_get_ith_last_revisions_normal_case(self):
        signals = [
            {"record_number": 1, "generic_signal_id": 101, "variant": "A", "revision": 1, "deleted": False},
            {"record_number": 1, "generic_signal_id": 101, "variant": "A", "revision": 2, "deleted": False},
            {"record_number": 1, "generic_signal_id": 101, "variant": "A", "revision": 3, "deleted": False},
            {"record_number": 2, "generic_signal_id": 102, "variant": "B", "revision": 1, "deleted": False},
            {"record_number": 2, "generic_signal_id": 102, "variant": "B", "revision": 2, "deleted": True},
        ]

        expected = [
            {"record_number": 1, "generic_signal_id": 101, "variant": "A", "revision": 2, "deleted": False},
            {"record_number": 2, "generic_signal_id": 102, "variant": "B", "revision": 1, "deleted": False},
        ]

        result = CDBClient.get_last_revisions(signals, idx=-2, deleted=False)
        assert result == expected

    def test_get_ith_last_revisions_invalid_idx(self):
        signals = [
            {"record_number": 1, "generic_signal_id": 101, "variant": "A", "revision": 1, "deleted": False},
        ]

        with pytest.raises(Exception) as excinfo:
            CDBClient.get_last_revisions(signals, idx=1)
        assert str(excinfo.value) == "idx must be negative"

    def test_get_ith_last_revisions_deleted_signals(self):
        signals = [
            {"record_number": 1, "generic_signal_id": 101, "variant": "A", "revision": 1, "deleted": True},
            {"record_number": 1, "generic_signal_id": 101, "variant": "A", "revision": 2, "deleted": True},
            {"record_number": 2, "generic_signal_id": 102, "variant": "B", "revision": 1, "deleted": True},
        ]

        expected = [
            {"record_number": 1, "generic_signal_id": 101, "variant": "A", "revision": 2, "deleted": True},
            {"record_number": 2, "generic_signal_id": 102, "variant": "B", "revision": 1, "deleted": True},
        ]

        result = CDBClient.get_last_revisions(signals, idx=-1, deleted=True)
        assert result == expected

    def test_get_ith_last_revisions_not_enough_revisions(self, caplog):
        signals = [
            {"record_number": 1, "generic_signal_id": 101, "variant": "A", "revision": 1, "deleted": False},
            {"record_number": 2, "generic_signal_id": 102, "variant": "B", "revision": 1, "deleted": False},
        ]

        result = CDBClient.get_last_revisions(signals, idx=-2, deleted=False)

        assert result == []


class TestClient(CDBTestCase):
    def test_thread_safe(self):
        self.create_random_channel()

        def _do():
            channel = self.client.get_attachment_table(computer_id=1,
                                                       board_id=1,
                                                       channel_id=1)
            self.assertEqual(1, channel[0]["computer_id"])

        import threading
        threads = []
        for i in range(30):
            thread = threading.Thread(target=_do)
            threads.append(thread)
            thread.start()

        for thread in threads:
            thread.join()

    def test_last_record_number_no_record(self):
        self.assertEqual(-1, self.client.last_record_number())

    def test_last_record_number_one_record(self):
        record_number_exp = self.client.create_record(record_type='EXP')
        self.assertEqual(record_number_exp, self.client.last_record_number())

    def test_last_record_number_multiple_types(self):
        self.client.create_record(record_type='EXP')
        self.client.create_record(record_type='EXP')
        record_number_exp = self.client.create_record(record_type='EXP')
        record_number_void = self.client.create_record(record_type='VOID')
        self.client.create_record(record_type='MODEL')
        record_number_model = self.client.create_record(record_type='MODEL')

        self.assertEqual(record_number_exp, self.client.last_record_number())
        self.assertEqual(record_number_exp, self.client.last_record_number('EXP'))
        self.assertEqual(record_number_void, self.client.last_record_number('VOID'))
        self.assertEqual(record_number_model, self.client.last_record_number('MODEL'))

    def test_get_record_info(self):
        fromisoformat = datetime.datetime.fromisoformat

        self.client.create_record(
            record_type='EXP',
            record_time=fromisoformat('2025-02-12T17:37:00')
        )
        record_number_exp = self.client.create_record(
            record_type='EXP',
            record_time=fromisoformat('2025-02-12T17:38:00'),
            description='2nd EXP'
        )
        last_record_number_exp = self.client.create_record(
            record_type='EXP',
            record_time=fromisoformat('2025-02-12T17:39:00')
        )
        record_number_void = self.client.create_record(
            record_type='VOID',
            record_time=fromisoformat('2025-02-12 17:40:00')
        )
        last_record_number_void = self.client.create_record(
            record_type='VOID',
            record_time=fromisoformat('2025-02-12 17:41:00')
        )

        # Retrieve specific record number
        record = self.client.get_record_info(record_number_exp)
        self.assertIsInstance(record["record_number"], int)
        self.assertEqual(record_number_exp, record["record_number"])
        self.assertIsInstance(record["record_type"], str)
        self.assertEqual('EXP', record["record_type"])
        self.assertIsInstance(record["record_time"], datetime.datetime)
        self.assertEqual(fromisoformat('2025-02-12 17:38:00'), record["record_time"])
        self.assertIsInstance(record["description"], str)
        self.assertEqual('2nd EXP', record["description"])

        record = self.client.get_record_info(record_number_void)
        self.assertEqual(record_number_void, record["record_number"])
        self.assertEqual('VOID', record["record_type"])
        self.assertEqual(fromisoformat('2025-02-12 17:40:00'), record["record_time"])
        self.assertEqual('', record["description"])

        # Record type has not impact on specific record number
        record = self.client.get_record_info(record_number_exp, record_type='VOID')
        self.assertEqual(record_number_exp, record["record_number"])

        # Retrieve last record number
        record = self.client.get_record_info(-1)
        self.assertEqual(last_record_number_exp, record["record_number"])
        record = self.client.get_record_info(-1, record_type='EXP')
        self.assertEqual(last_record_number_exp, record["record_number"])
        record = self.client.get_record_info(-1, record_type='VOID')
        self.assertEqual(last_record_number_void, record["record_number"])

        # Record not found
        self.assertRaises(CDBException, self.client.get_record_info, last_record_number_exp + last_record_number_void)


class TestStoreSignals(CDBTestCase):
    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_data_file_keys_of_various_lengths(self):
        gs = self.create_random_generic_signal(create_channel=False)
        data_file_id = self.create_random_data_file()["data_file_id"]
        one_hundred_chars = 100 * "D"

        self.client.store_signal(generic_signal_id=gs["generic_signal_id"],
                                 record_number=self.test_record,
                                 data_file_key=one_hundred_chars,
                                 data_file_id=data_file_id)

    def test_put_signal_time_data(self):
        time_ax_ref = self.create_random_generic_signal(signal_type="FILE")
        gs_ref = self.create_random_generic_signal(
            signal_type="FILE",
            time_axis_id=time_ax_ref["generic_signal_id"])
        t_data = np.linspace(0, 1)
        data = np.random.rand(t_data.size)
        sig_ref = self.client.put_signal(gs_ref['generic_signal_id'],
                                         self.test_record,
                                         data,
                                         time_axis_data=t_data)
        sig = self.client.get_signal(**sig_ref)
        assert np.allclose(data, sig.data)
        assert np.allclose(t_data, sig.time_axis.data)

    def test_put_signal_linear_time(self):
        time_ax_ref = self.create_random_generic_signal(signal_type="LINEAR")
        gs_ref = self.create_random_generic_signal(
            signal_type="FILE",
            time_axis_id=time_ax_ref["generic_signal_id"])
        time_axis_coef = np.random.random()
        time_axis_offset = np.random.random()
        time0 = np.random.random()
        data = np.random.rand(np.random.randint(2, 100))
        sig_ref = self.client.put_signal(gs_ref['generic_signal_id'],
                                         self.test_record,
                                         data,
                                         time0=time0,
                                         time_axis_coef=time_axis_coef,
                                         time_axis_offset=time_axis_offset)
        sig = self.client.get_signal(**sig_ref)
        assert np.allclose(data, sig.data)
        t_data = time0 + np.arange(data.size) * time_axis_coef
        assert np.allclose(t_data, sig.time_axis.data)

    def test_put_signal_axis1_data(self):
        ax1_ref = self.create_random_generic_signal(signal_type="FILE")
        gs_ref = self.create_random_generic_signal(
            signal_type="FILE",
            axis1_id=ax1_ref["generic_signal_id"])
        ax_data = np.linspace(0, 1)
        data = np.random.rand(ax_data.size)
        sig_ref = self.client.put_signal(gs_ref['generic_signal_id'],
                                         self.test_record,
                                         data,
                                         axis1_data=ax_data)
        sig = self.client.get_signal(**sig_ref)
        assert np.allclose(data, sig.data)
        assert np.allclose(ax_data, sig.axis1.data)

    def test_put_signal_axes_arguments(self):
        axes_ids = dict((ax + '_id', self.create_random_generic_signal(
            signal_type="FILE")["generic_signal_id"])
                        for ax in pyCDB.client.axes_names)
        gs_ref = self.create_random_generic_signal(signal_type="FILE",
                                                   **axes_ids)

        axes_arguments = {}
        for ax in pyCDB.client.axes_names:
            axes_arguments.update({ax + '_data': np.linspace(
                0, 1, np.random.randint(2, 5)),
                                   ax + '_coef': np.random.rand(),
                                   ax + '_offset': np.random.rand(), })
        data = np.random.random([axes_arguments[ax + '_data'].size
                                 for ax in pyCDB.client.axes_names])
        sig_ref = self.client.put_signal(gs_ref['generic_signal_id'],
                                         self.test_record, data,
                                         **axes_arguments)
        sig = self.client.get_signal(**sig_ref)
        assert np.allclose(data, sig.data)
        for ax in pyCDB.client.axes_names:
            ax_data = (axes_arguments[ax + '_coef'] * (
                axes_arguments[ax + '_data'] + axes_arguments[ax + '_offset']))
            assert np.allclose(getattr(sig, ax).data, ax_data)

    def test_put_signal_gs_parameters(self):
        gs_ref = self.create_random_generic_signal(signal_type="FILE",
                                                   create_time_axis=True)
        data = np.random.rand(np.random.randint(1, 100))
        gs_parameters = {'param1': 1, 'param2': 2.0}
        sig_ref = self.client.put_signal(gs_ref['generic_signal_id'],
                                         self.test_record,
                                         data,
                                         time_axis_coef=1.0,
                                         gs_parameters=gs_parameters)
        sig = self.client.get_signal(**sig_ref)
        assert np.allclose(data, sig.data)
        assert sig.gs_parameters == gs_parameters

    def test_put_signal_data_quality(self):
        gs_ref = self.create_random_generic_signal(signal_type="FILE",
                                                   create_time_axis=True)
        data = np.random.rand(np.random.randint(2, 20))
        for data_quality in ('UNKNOWN', 'POOR', 'GOOD', 'VALIDATED'):
            sig_ref = self.client.put_signal(gs_ref['generic_signal_id'],
                                             self.test_record,
                                             data,
                                             data_quality=data_quality,
                                             time_axis_coef=1,
                                             time_axis_offset=0)
            sig = self.client.get_signal(**sig_ref)
            assert (sig.ref.data_quality == data_quality)

    def test_signal_array_interface(self):
        gs_ref = self.create_random_generic_signal(signal_type="FILE",
                                                   create_time_axis=True, )
        data = np.random.random(60)
        sig_ref = self.client.put_signal(gs_ref['generic_signal_id'],
                                         self.test_record,
                                         data,
                                         time_axis_coef=1,
                                         time_axis_offset=0, )
        sig = self.client.get_signal(**sig_ref)
        data_p1 = np.add(sig, 1)
        self.assertIsInstance(data_p1, np.ndarray)
        numpy.testing.assert_allclose(data + 1, data_p1)

    def test_put_signal_scalar(self):
        gs_ref = self.create_random_generic_signal(signal_type="FILE")
        data = 15.0
        sig_ref = self.client.put_signal(gs_ref['generic_signal_id'],
                                         self.test_record,
                                         data, )
        sig = self.client.get_signal(**sig_ref)
        assert np.allclose(data, sig.data)

    def test_ignore_if_exists(self):
        gs_ref = self.create_random_generic_signal(signal_type="LINEAR")
        attempt1 = self.client.store_signal(
            generic_signal_id=gs_ref["generic_signal_id"],
            record_number=self.test_record)
        self.assertEqual(1, attempt1["revision"])
        attempt2 = self.client.store_signal(
            generic_signal_id=gs_ref["generic_signal_id"],
            record_number=self.test_record,
            ignore_if_exists=True)
        self.assertIsNone(attempt2)


class TestStoreChannelDateAsSignal(CDBTestCase):
    """Test store_channel_data_as_signal method.

    Various test
    """

    def _store_channel_data_as_signal(self, signal=None, cbc=None, **kwargs):
        if signal is not None:
            kwargs["computer_id"] = signal["computer_id"]
            kwargs["board_id"] = signal["board_id"]
            kwargs["channel_id"] = signal["channel_id"]
        elif cbc is not None:
            kwargs["computer_id"] = cbc[0]
            kwargs["board_id"] = cbc[1]
            kwargs["channel_id"] = cbc[2]
        kwargs["record_numbers"] = signal["record_number"]
        self.client.store_channel_data_as_signal(**kwargs)

    def _get_signal_count(self,
                          signal=None,
                          generic_signal=None,
                          generic_signal_id=None,
                          record_number=None):
        '''

        :param signal:
        :param generic_signal:
        :param generic_signal_id:
        :param record_number:
        :return:(total_count, deleted_count, is_deleted)
        '''
        if generic_signal_id is None:
            if signal is not None:
                generic_signal_id = signal["generic_signal_id"]
            if generic_signal is not None:
                generic_signal_id = generic_signal["generic_signal_id"]
        if record_number is None:
            if signal:
                record_number = signal["record_number"]
            else:
                record_number = self.test_record
        signals = self.client.get_signal_references(
            generic_signal_id=generic_signal_id,
            record_number=record_number,
            deleted=True)
        total_count = len(signals)
        if not total_count:
            return (0, 0, False)
        else:
            deleted_count = len([s for s in signals if s["deleted"] > 0])
            max_revision = max([s["revision"] for s in signals])
            is_deleted = [s["deleted"] > 0
                          for s in signals if s["revision"] == max_revision][0]
            return (total_count, deleted_count, is_deleted)

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_simple(self):
        gs1 = self.create_random_generic_signal(create_channel=False)
        signal1 = self.create_random_data_signal()
        self._store_channel_data_as_signal(
            signal=signal1,
            generic_signal_id=gs1["generic_signal_id"])
        self.assertEqual((2, 1, True), self._get_signal_count(signal1))
        self.assertEqual(
            (1, 0, False),
            self._get_signal_count(generic_signal=gs1,
                                   record_number=signal1["record_number"]))

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_twice(self):
        gs1 = self.create_random_generic_signal(create_channel=False)
        gs2 = self.create_random_generic_signal(create_channel=False)
        signal1 = self.create_random_data_signal()  # ->gs 3
        self._store_channel_data_as_signal(
            signal=signal1,
            generic_signal_id=gs1["generic_signal_id"])
        self._store_channel_data_as_signal(
            signal=signal1,
            generic_signal_id=gs2["generic_signal_id"])
        self.assertEqual((2, 1, True), self._get_signal_count(signal1))
        self.assertEqual(
            (2, 1, True),
            self._get_signal_count(generic_signal=gs1,
                                   record_number=signal1["record_number"]))
        self.assertEqual(
            (1, 0, False),
            self._get_signal_count(generic_signal=gs2,
                                   record_number=signal1["record_number"]))

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_twice_with_update(self):
        gs1 = self.create_random_generic_signal(create_channel=False)
        gs2 = self.create_random_generic_signal(create_channel=False)
        signal1 = self.create_random_data_signal()  # ->gs3

        self.client.update_signal(sig_ref=signal1, coefficient=1.1)
        self._store_channel_data_as_signal(
            signal=signal1,
            generic_signal_id=gs1["generic_signal_id"])
        self._store_channel_data_as_signal(
            signal=signal1,
            generic_signal_id=gs2["generic_signal_id"])
        self.assertEqual((3, 1, True), self._get_signal_count(signal1))
        self.assertEqual(
            (2, 1, True),
            self._get_signal_count(generic_signal=gs1,
                                   record_number=signal1["record_number"]))
        self.assertEqual(
            (1, 0, False),
            self._get_signal_count(generic_signal=gs2,
                                   record_number=signal1["record_number"]))

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_twice_with_delete(self):
        gs1 = self.create_random_generic_signal(create_channel=False)
        gs2 = self.create_random_generic_signal(create_channel=False)
        signal1 = self.create_random_data_signal()  # ->gs3

        self._store_channel_data_as_signal(
            signal=signal1,
            generic_signal_id=gs1["generic_signal_id"])
        self.client.delete_signal(generic_signal_id=gs1["generic_signal_id"],
                                  record_number=signal1["record_number"])
        self._store_channel_data_as_signal(
            signal=signal1,
            generic_signal_id=gs2["generic_signal_id"])
        self.assertEqual((2, 1, True), self._get_signal_count(signal1))
        self.assertEqual(
            (2, 1, True),
            self._get_signal_count(generic_signal=gs1,
                                   record_number=signal1["record_number"]))
        self.assertEqual(
            (0, 0, False),
            self._get_signal_count(generic_signal=gs2,
                                   record_number=signal1["record_number"]))

        self._store_channel_data_as_signal(
            signal=signal1,
            generic_signal_id=gs2["generic_signal_id"],
            enable_deleted=True)
        self.assertEqual(
            (1, 0, False),
            self._get_signal_count(generic_signal=gs2,
                                   record_number=signal1["record_number"]))

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_same_gs(self):
        signal1 = self.create_random_data_signal()
        self._store_channel_data_as_signal(
            signal=signal1,
            generic_signal_id=signal1["generic_signal_id"])
        self.assertEqual((2, 0, False), self._get_signal_count(signal1))

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_swap_two_generic_signals_multiple_times(self):
        chan1 = self.create_random_channel()
        chan2 = self.create_random_channel()

        sleep(1)

        default_gsid1 = chan1["default_generic_signal_id"]
        default_gsid2 = chan2["default_generic_signal_id"]

        gsid1 = self.create_random_generic_signal()["generic_signal_id"]
        gsid2 = self.create_random_generic_signal()["generic_signal_id"]

        cpc1 = chan1["computer_id"], chan1["board_id"], chan1["channel_id"]
        cpc2 = chan2["computer_id"], chan2["board_id"], chan2["channel_id"]

        self.client.attach_channel(*cpc1, generic_signal_id=gsid1)
        self.client.attach_channel(*cpc2, generic_signal_id=gsid2)

        sleep(1)

        data_sig1 = self.create_random_data_signal(generic_signal_id=gsid1)
        data_sig2 = self.create_random_data_signal(generic_signal_id=gsid2)

        # 1st step: move gs2 from chan2 to chan1
        self._store_channel_data_as_signal(signal=data_sig1,
                                           generic_signal_id=gsid2)

        self.assertEqual(
            (2, 1, True),
            self._get_signal_count(generic_signal_id=gsid1))
        self.assertEqual(
            (3, 1, False),
            self._get_signal_count(generic_signal_id=gsid2))

        signal1 = self.client.get_signal_references(
            generic_signal_id=gsid1,
            record_number=self.test_record,
            deleted=True,
            revision=-1)[0]
        signal2 = self.client.get_signal_references(
            generic_signal_id=gsid2,
            record_number=self.test_record,
            deleted=True,
            revision=-1)[0]

        self.assertEqual(2, signal1["revision"])
        self.assertTrue(signal1["deleted"])

        self.assertEqual(3, signal2["revision"])
        self.assertFalse(signal2["deleted"])

        chansig2 = self.client.get_signal_references(
            computer_id=cpc2[0],
            board_id=cpc2[1],
            channel_id=cpc2[2],
            record_number=self.test_record,
            revision=-1)[0]
        chansig1 = self.client.get_signal_references(
            computer_id=cpc1[0],
            board_id=cpc1[1],
            channel_id=cpc1[2],
            record_number=self.test_record,
            revision=-1)[0]

        self.assertEqual(default_gsid2, chansig2["generic_signal_id"])
        self.assertEqual(gsid2, chansig1["generic_signal_id"])

        # 2nd step: move gs1 to chan2
        sleep(2)  # Have another timestamp

        self.client.store_channel_data_as_signal(
            *cpc2,
            generic_signal_id=gsid1,
            record_numbers=self.test_record)

        signal1 = self.client.get_signal_references(
            generic_signal_id=gsid1,
            record_number=self.test_record,
            deleted=True,
            revision=-1)[0]
        signal2 = self.client.get_signal_references(
            generic_signal_id=gsid2,
            record_number=self.test_record,
            deleted=True,
            revision=-1)[0]

        self.assertEqual(0, signal1["deleted"])
        self.assertEqual(0, signal2["deleted"])

        chansig2 = self.client.get_signal_references(
            computer_id=cpc2[0],
            board_id=cpc2[1],
            channel_id=cpc2[2],
            record_number=self.test_record,
            revision=-1)[0]
        chansig1 = self.client.get_signal_references(
            computer_id=cpc1[0],
            board_id=cpc1[1],
            channel_id=cpc1[2],
            record_number=self.test_record,
            revision=-1)[0]

        self.assertEqual(gsid1, chansig2["generic_signal_id"])
        self.assertEqual(gsid2, chansig1["generic_signal_id"])

        # 3nd step: move also gs2 to chan2
        self._store_channel_data_as_signal(signal=data_sig2,
                                           generic_signal_id=gsid2)

        chansig2 = self.client.get_signal_references(
            computer_id=cpc2[0],
            board_id=cpc2[1],
            channel_id=cpc2[2],
            record_number=self.test_record,
            revision=-1)[0]
        chansig1 = self.client.get_signal_references(
            computer_id=cpc1[0],
            board_id=cpc1[1],
            channel_id=cpc1[2],
            record_number=self.test_record,
            revision=-1)[0]

        self.assertEqual(gsid2, chansig2["generic_signal_id"])
        self.assertEqual(default_gsid1, chansig1["generic_signal_id"])

        self.assertEqual(
            (4, 2, True),
            self._get_signal_count(generic_signal_id=gsid1))
        self.assertEqual(
            (5, 2, False),
            self._get_signal_count(generic_signal_id=gsid2))


class TestVariants(CDBTestCase):
    """Signal variants unit tests"""

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_store(self):
        signal1 = self.create_random_data_signal()
        variant = 'var_a1'
        signal2 = self.create_random_data_signal(
            generic_signal_id=signal1['generic_signal_id'],
            variant=variant)
        signal1a = self.create_random_data_signal(
            generic_signal_id=signal1['generic_signal_id'],
            variant='')
        self.assertEqual(signal1a['revision'], 2)
        sig_refs = self.client.get_signal_references(**signal2)
        self.assertEqual(len(sig_refs), 1)
        self.assertEqual(sig_refs[0], signal2)
        self.assertEqual(signal1['variant'], '')
        self.assertEqual(signal2['variant'], variant)
        self.assertEqual(signal1['revision'], 1)
        self.assertEqual(signal2['revision'], 1)

    def test_invalid(self):
        self.assertRaises(Exception,
                          self.create_random_data_signal,
                          variant=100 * 'a')
        self.assertRaises(Exception,
                          self.create_random_data_signal,
                          variant=''.join(random.choice(string.digits)))

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_axes_variants(self):
        for ax in self.client._CDBClient__axes_names:
            variant = self.random_alpha(10)
            ax_ref = self.create_random_data_signal(variant=variant)
            kwargs = {ax + '_id': ax_ref['generic_signal_id']}
            gs = self.create_random_generic_signal(**kwargs)
            kwargs = {'generic_signal_id': gs['generic_signal_id'],
                      ax + '_variant': variant}
            sig_ref = self.create_random_data_signal(**kwargs)
            self.assertEqual(sig_ref[ax + '_variant'], variant)

    def test_put_signal_variant(self):
        time_ax_ref = self.create_random_generic_signal(signal_type="FILE")
        gs_ref = self.create_random_generic_signal(
            signal_type="FILE",
            time_axis_id=time_ax_ref["generic_signal_id"])
        t_data = np.linspace(0, 1)
        data = np.random.rand(t_data.size)
        variant = 'my_variant'
        # axis data do not exist ==> exception
        with self.assertRaises(CDBException):
            self.client.put_signal(gs_ref['generic_signal_id'],
                                   self.test_record,
                                   data,
                                   variant=variant)
        sig_ref = self.client.put_signal(gs_ref['generic_signal_id'],
                                         self.test_record,
                                         data,
                                         time_axis_data=t_data,
                                         variant=variant)
        sig = self.client.get_signal(**sig_ref)
        assert sig_ref.variant == variant
        assert sig_ref == sig.ref
        assert sig.time_axis.ref.variant == variant
        assert np.allclose(data, sig.data)
        assert np.allclose(t_data, sig.time_axis.data)
        # this should reuse axis data
        sig_ref = self.client.put_signal(gs_ref['generic_signal_id'],
                                         self.test_record,
                                         data,
                                         variant=variant)
        assert sig_ref.revision == 2
        sig = self.client.get_signal(**sig_ref)
        assert sig_ref.variant == variant
        assert sig_ref == sig.ref
        assert np.allclose(data, sig.data)
        assert np.allclose(t_data, sig.time_axis.data)
        # axis data with variant2 do not exist ==> exception
        variant2 = variant + '2'
        with self.assertRaises(CDBException):
            self.client.put_signal(gs_ref['generic_signal_id'],
                                   self.test_record,
                                   data,
                                   variant=variant2)

    def test_put_signal_variant_linear_time_axis(self):
        time_ax_ref = self.create_random_generic_signal(signal_type="LINEAR")
        gs_ref = self.create_random_generic_signal(
            signal_type="FILE",
            time_axis_id=time_ax_ref["generic_signal_id"])
        time_axis_coef = np.random.random()
        time_axis_offset = np.random.random()
        time0 = np.random.random()
        data = np.random.rand(np.random.randint(2, 100))
        variant = self.random_alpha(7)
        sig_ref = self.client.put_signal(gs_ref['generic_signal_id'],
                                         self.test_record,
                                         data,
                                         variant=variant,
                                         time0=time0,
                                         time_axis_coef=time_axis_coef,
                                         time_axis_offset=time_axis_offset)
        sig = self.client.get_signal(**sig_ref)
        assert np.allclose(data, sig.data)
        t_data = time0 + np.arange(data.size) * time_axis_coef
        assert np.allclose(t_data, sig.time_axis.data)
        assert sig_ref.variant == variant
        assert sig_ref == sig.ref
        assert sig.time_axis.ref.variant == variant


class TestParameters(CDBTestCase):
    """Test data signal parameters"""

    def _random_json(self):
        import json
        test_dict = OrderedDict()
        subkey = self.random_alpha(10)
        test_dict[subkey] = OrderedDict()
        for i in range(random.randint(3, 6)):
            k = self.random_alpha(random.randint(3, 6))
            test_dict[k] = self.random_alphanum(random.randint(0, 6))
            test_dict[subkey][k] = self.random_alphanum(random.randint(0, 6))
        return json.dumps(test_dict), test_dict

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_store(self):
        # empty parameters
        signal = self.create_random_data_signal(gs_parameters='',
                                                daq_parameters='')
        sig_param = self.client.get_signal_parameters(**signal)
        for kparam in ('gs_parameters', 'daq_parameters'):
            self.assertEqual(OrderedDict(), sig_param[kparam])
        test_text, test_dict = self._random_json()
        for kparam in ('gs_parameters', 'daq_parameters'):
            signal = self.create_random_data_signal(**{kparam: test_text})
            sig_param = self.client.get_signal_parameters(**signal)
            self.assertEqual(test_dict, sig_param[kparam])

    def test_invalid(self):
        for kparam in ('gs_parameters', 'daq_parameters'):
            self.assertRaises(CDBException, self.create_random_data_signal, **
                              {kparam: 'a: 1'})
            self.assertRaises(CDBException, self.create_random_data_signal, **
                              {kparam: 'bla bla bla'})

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_attach(self):
        ch1 = self.create_random_channel()
        gs_parameters, gs_param_dict = self._random_json()
        daq_parameters, daq_param_dict = self._random_json()
        gs1 = self.create_random_generic_signal(create_channel=False)
        # TODO: this is a problem in create_DAQ_channels, which automatically attaches
        # but does not input parameters
        sleep(1)
        self.client.attach_channel(ch1['computer_id'],
                                   ch1['board_id'],
                                   ch1['channel_id'],
                                   gs1['generic_signal_id'],
                                   parameters=daq_parameters)
        self.client.signal_setup(gs1['generic_signal_id'],
                                 parameters=gs_parameters)
        sig = self.create_random_data_signal(
            generic_signal_id=gs1['generic_signal_id'])

        self.assertEqual(daq_param_dict, self.client.get_signal_parameters(
            **sig)['daq_parameters'])
        self.assertEqual(gs_param_dict, self.client.get_signal_parameters(
            **sig)['gs_parameters'])

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_attach_invalid(self):
        ch1 = self.create_random_channel()
        parameters = self.random_alphanum(random.randint(1, 10))
        gs1 = self.create_random_generic_signal(create_channel=False)
        # TODO: this is a problem in create_DAQ_channels, which automatically attaches
        # but does not input parameters
        sleep(1)
        self.assertRaises(
            Exception,
            self.client.attach_channel,
            ch1['computer_id'],
            ch1['board_id'],
            ch1['channel_id'],
            gs1['generic_signal_id'],
            parameters=parameters
        )  # TODO: check this - AssertionError: Exception not raised
        self.assertRaises(Exception,
                          self.client.signal_setup,
                          gs1['generic_signal_id'],
                          parameters=parameters)


class TestGetSignal(CDBTestCase):
    '''Various tests for get_signal and its sub-methods.'''

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_get_signal_base_tree(self):
        time_axis = self.create_random_generic_signal(signal_type="LINEAR")
        gs = self.create_random_generic_signal(
            time_axis_id=time_axis["generic_signal_id"],
            signal_type="FILE")
        signal_ref = self.store_signal(
            generic_signal_id=gs["generic_signal_id"],
            random_data_file=True)

        # Time axis does not exist yet
        with self.assertRaises(Exception):
            signal = self.client.get_signal_base_tree(signal_ref)

        # Axis as specified in generic_signals
        self.store_signal(generic_signal_id=time_axis["generic_signal_id"])
        signal = self.client.get_signal_base_tree(signal_ref)
        self.assertEqual(time_axis.generic_signal_id,
                         signal.time_axis.ref["generic_signal_id"])

        # Axis as specified in data_signals
        time_axis2 = self.create_random_generic_signal(signal_type="LINEAR")
        self.store_signal(generic_signal_id=time_axis2["generic_signal_id"])
        signal_ref = self.store_signal(
            generic_signal_id=gs["generic_signal_id"],
            random_data_file=True,
            time_axis_id=time_axis2["generic_signal_id"])
        signal = self.client.get_signal_base_tree(signal_ref)
        self.assertEqual(time_axis2.generic_signal_id,
                         signal.time_axis.ref["generic_signal_id"])

        # Axis dependent on another axis
        time_axis3 = self.create_random_generic_signal(signal_type="LINEAR")
        time_axis4 = self.create_random_generic_signal(
            signal_type="LINEAR",
            time_axis_id=time_axis3["generic_signal_id"])
        self.store_signal(generic_signal_id=time_axis3["generic_signal_id"])
        self.store_signal(generic_signal_id=time_axis4["generic_signal_id"])
        signal_ref = self.store_signal(
            generic_signal_id=gs["generic_signal_id"],
            random_data_file=True,
            time_axis_id=time_axis4["generic_signal_id"])
        signal = self.client.get_signal_base_tree(signal_ref)
        self.assertEqual(time_axis4.generic_signal_id,
                         signal.time_axis.ref["generic_signal_id"])
        self.assertEqual(time_axis3.generic_signal_id,
                         signal.time_axis.time_axis.ref["generic_signal_id"])

    def test_get_signal_data_tree(self):
        # Impossible to read linear without n_samples
        linear = self.create_random_generic_signal(signal_type="LINEAR")
        signal_ref = self.store_signal(
            generic_signal_id=linear["generic_signal_id"])
        signal = self.client.get_signal_base_tree(signal_ref)
        with self.assertRaises(Exception):
            self.client.get_signal_data_tree(signal)
        # Possible to read it with n_samples
        self.client.get_signal_data_tree(signal, n_samples=10)

        # With some data & time_axis (incl. time_axis_coef and time0)
        gs = self.create_random_generic_signal(create_time_axis=True)
        data = np.arange(0, 10)
        signal_ref = self.client.put_signal(gs.generic_signal_id,
                                            self.test_record,
                                            data=data,
                                            time_axis_coef=2.2,
                                            time0=1.3)
        signal = self.client.get_signal_base_tree(signal_ref)
        self.client.get_signal_data_tree(signal)
        self.assertEqual(list(data), list(signal.data))
        self.assertTrue(np.array_equal(data * 2.2 + 1.3,
                                       signal.time_axis.data))

        # Linear signal dependent on file
        file_gs = self.create_random_generic_signal()
        linear = self.create_random_generic_signal(
            time_axis_id=file_gs.generic_signal_id,
            signal_type="LINEAR")
        file_data = np.arange(40, 60, 0.7)
        data_file_key = self.random_name()

        data_file = self.create_random_data_file(data_file_key=data_file_key,
                                                 data=file_data)
        self.store_signal(generic_signal_id=file_gs.generic_signal_id,
                          data_file_key=data_file_key,
                          data_file_id=data_file.data_file_id)
        signal_ref = self.store_signal(
            generic_signal_id=linear["generic_signal_id"],
            coefficient=2.4,
            offset=1.7)
        signal = self.client.get_signal_base_tree(signal_ref)
        self.client.get_signal_data_tree(signal)
        self.assertTrue(np.array_equal(signal.time_axis.data, file_data))
        self.assertTrue(np.array_equal(signal.data, file_data * 2.4 + 1.7)
                        )  # Trivial linear transformation

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_get_unit_factor_tree(self):
        # TODO: Change signals to mock objects to test the functionality only

        # One signal, one unit
        gs = self.create_random_generic_signal(units='m', signal_type='LINEAR')
        signal_ref = self.store_signal(
            generic_signal_id=gs["generic_signal_id"])
        signal = self.client.get_signal_base_tree(signal_ref)
        factor_tree = self.client.get_unit_factor_tree(signal, units='mm')
        self.assertEqual({'units': 'mm', 'factor': 1000.0}, factor_tree)

        # One signal, one system (units in system)
        factor_tree = self.client.get_unit_factor_tree(signal, units='SI')
        self.assertEqual({'units': 'm', 'factor': 1.0}, factor_tree)
        factor_tree = self.client.get_unit_factor_tree(signal, units='COMPASS')
        self.assertEqual({'units': 'cm', 'factor': 100.0}, factor_tree)

        # Convert to invalid unit system
        with self.assertRaises(Exception):
            factor_tree = self.client.get_unit_factor_tree(signal, units='A')

        # One signal, unit not in system (=> keep)
        gs = self.create_random_generic_signal(units='A', signal_type='LINEAR')
        signal_ref = self.store_signal(
            generic_signal_id=gs["generic_signal_id"])
        signal = self.client.get_signal_base_tree(signal_ref)
        factor_tree = self.client.get_unit_factor_tree(signal, units='SI')
        self.assertEqual({'units': 'A', 'factor': 1.0}, factor_tree)

        # Two signal, unit (applied only to the first)
        gs = self.create_random_generic_signal(units='ms',
                                               signal_type='LINEAR')
        gs2 = self.create_random_generic_signal(
            units='ns',
            signal_type='LINEAR',
            time_axis_id=gs.generic_signal_id)
        signal_ref = self.store_signal(
            generic_signal_id=gs["generic_signal_id"])
        signal2_ref = self.store_signal(
            generic_signal_id=gs2["generic_signal_id"])
        signal2 = self.client.get_signal_base_tree(signal2_ref)
        factor_tree = self.client.get_unit_factor_tree(signal2, units='s')
        self.assertEqual(
            {'units': 's',
             'factor': 1e-9,
             'time_axis': {'units': 'ms',
                           'factor': 1.0}}, factor_tree)

        # Two signals with unit systems
        factor_tree = self.client.get_unit_factor_tree(signal2, units='SI')
        self.assertEqual(
            {'units': 's',
             'factor': 1e-9,
             'time_axis': {'units': 's',
                           'factor': 1e-3}}, factor_tree)
        factor_tree = self.client.get_unit_factor_tree(signal2,
                                                       units='COMPASS')
        expected = OrderedDict((('factor', 1e-6), ('units', 'ms'), (
            'time_axis', OrderedDict((('factor', 1.0), ('units', 'ms'))))))
        self.assertEqual(expected, factor_tree)

        # Wild units (eV + 10^-19 J) - from unit and unit_system
        gs = self.create_random_generic_signal(units='eV',
                                               signal_type='LINEAR')
        signal_ref = self.store_signal(
            generic_signal_id=gs["generic_signal_id"])
        signal = self.client.get_signal_base_tree(signal_ref)
        factor_tree = self.client.get_unit_factor_tree(signal,
                                                       units='10^-19 J')
        expected = OrderedDict((('factor', 1.60217653), ('units', '10^-19 J')))
        self.assertEqual(expected, factor_tree)
        factor_tree = self.client.get_unit_factor_tree(signal, units='COMPASS')
        self.assertEqual(expected, factor_tree)

    def test_apply_unit_factor_tree(self):
        # Simple
        data = np.arange(11, 56, 0.23)
        factor_tree = {"units": "V", "factor": 2.39}
        signal = OrderedDict({"units": "W", "data": data})
        self.client.apply_unit_factor_tree(signal, factor_tree)
        self.assertEqual("V", signal.units)
        self.assertTrue(np.allclose(data * factor_tree["factor"], signal.data))

        # Two signals
        factor_tree["time_axis"] = {"units": "ms", "factor": 1.7}
        signal.data = data  # Return
        signal.time_axis = OrderedDict({"units": "s", "data": data})
        self.client.apply_unit_factor_tree(signal, factor_tree)
        self.assertEqual("ms", signal.time_axis.units)
        self.assertTrue(np.allclose(data * factor_tree["time_axis"]["factor"],
                                    signal.time_axis.data))

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_get_signal_typical(self):

        # A typical signal
        time_axis = self.create_random_generic_signal(units='s',
                                                      signal_type='LINEAR')
        gs = self.create_random_generic_signal(
            time_axis_id=time_axis.generic_signal_id,
            units='m')
        data = np.arange(11, 5600, 0.23)
        ta_data = np.arange(0, data.shape[0])
        data_file = self.create_random_data_file(data=data,
                                                 data_file_key="abcd")
        self.store_signal(generic_signal_id=time_axis["generic_signal_id"])

        offset = -1.0
        coefficient_V2unit = 3.0
        coefficient = 2.0

        self.store_signal(generic_signal_id=gs.generic_signal_id,
                          data_file_id=data_file.data_file_id,
                          data_file_key="abcd",
                          coefficient=coefficient,
                          coefficient_V2unit=coefficient_V2unit,
                          offset=offset)
        str_id = "%s:%d" % (gs.generic_signal_id, self.test_record)

        # Default
        signal = self.client.get_signal(str_id)
        self.assertEqual("m", signal.units)
        self.assertTrue(np.allclose(
            (data + offset) * coefficient * coefficient_V2unit, signal.data))
        self.assertEqual("s", signal.time_axis.units)
        self.assertTrue(np.allclose(ta_data, signal.time_axis.data))

        # RAW
        signal = self.client.get_signal(str_id + "[raw]")
        self.assertEqual("raw", signal.units)
        self.assertTrue(np.allclose(data, signal.data))
        self.assertEqual("s", signal.time_axis.units)
        self.assertTrue(np.allclose(ta_data, signal.time_axis.data))
        signal = self.client.get_signal(str_id + "[RAW]")
        self.assertEqual("raw", signal.units)

        # DAV
        signal = self.client.get_signal(str_id + "[dav]")
        self.assertEqual("dav", signal.units)
        self.assertTrue(np.allclose(
            (data + offset) * coefficient, signal.data))
        self.assertEqual("s", signal.time_axis.units)
        self.assertTrue(np.allclose(ta_data, signal.time_axis.data))

        # Unit
        signal = self.client.get_signal(str_id + "[cm]")
        self.assertEqual("cm", signal.units)
        self.assertTrue(np.allclose(
            (data + offset
             ) * coefficient * coefficient_V2unit * 100, signal.data))
        self.assertEqual("s", signal.time_axis.units)
        self.assertTrue(np.allclose(ta_data, signal.time_axis.data))

        # Unit system
        signal = self.client.get_signal(str_id + "[COMPASS]")
        self.assertEqual("cm", signal.units)
        self.assertTrue(np.allclose(
            (data + offset
             ) * coefficient * coefficient_V2unit * 100, signal.data))
        self.assertEqual("ms", signal.time_axis.units)
        self.assertTrue(np.allclose(ta_data * 1000, signal.time_axis.data))

        # Nonsense unit
        with self.assertRaises(Exception):
            self.client.get_signal(str_id + "[nonsense]")

        # test slices only
        signal = self.client.get_signal(str_id + "[1::3]")
        self.assertEqual("m", signal.units)
        self.assertEqual(signal.data.shape, data[1::3].shape)
        self.assertEqual(signal.time_axis.data.shape, ta_data[1::3].shape)

        # slice assertions
        with self.assertRaises(ValueError):
            signal = self.client.get_signal(str_id + "[1.1]")
        with self.assertRaises(ValueError):
            signal = self.client.get_signal(str_id + "[1.1][cm]")
        with self.assertRaises(ValueError):
            signal = self.client.get_signal(str_id + "[cm][1.1]")
        with self.assertRaises(Exception):
            signal = self.client.get_signal(str_id + "[nonsense][::2]")

        sliced_data = data[1::3]
        sliced_ta_data = ta_data[1::3]

        # Unit + slice
        signal = self.client.get_signal(str_id + "[cm][1::3]")
        self.assertEqual("cm", signal.units)
        self.assertTrue(np.allclose(
            (sliced_data + offset
             ) * coefficient * coefficient_V2unit * 100, signal.data))
        self.assertEqual("s", signal.time_axis.units)
        self.assertTrue(np.allclose(sliced_ta_data, signal.time_axis.data))

        # Slice + Unit
        signal = self.client.get_signal(str_id + "[1::3][cm]")
        self.assertEqual("cm", signal.units)
        self.assertTrue(np.allclose(
            (sliced_data + offset
             ) * coefficient * coefficient_V2unit * 100, signal.data))
        self.assertEqual("s", signal.time_axis.units)
        self.assertTrue(np.allclose(sliced_ta_data, signal.time_axis.data))

        # Test proper shape propagation
        threed_data = np.random.rand(10, 20, 30)
        nt, nx, ny = threed_data.shape
        ta_data = np.arange(0, nt, dtype=np.float)
        ax1_data = np.arange(0, nx, dtype=np.float)
        ax2_data = np.arange(0, ny, dtype=np.float)
        sliced_threed_data = threed_data[1:6, 1:11:2, 1:28:3]
        sliced_ta_data = ta_data[1:6]
        sliced_ax1_data = ax1_data[1:11:2]
        sliced_ax2_data = ax2_data[1:28:3]

        ta_ref = self.create_random_generic_signal(signal_type="FILE")
        ax1_ref = self.create_random_generic_signal(signal_type="FILE")
        ax2_ref = self.create_random_generic_signal(signal_type="FILE")
        gs_ref = self.create_random_generic_signal(
            signal_type="FILE",
            time_axis_id=ta_ref["generic_signal_id"],
            axis1_id=ax1_ref["generic_signal_id"],
            axis2_id=ax2_ref["generic_signal_id"])
        sig_ref = self.client.put_signal(gs_ref["generic_signal_id"],
                                         self.test_record,
                                         threed_data,
                                         time_axis_data=ta_data,
                                         axis1_data=ax1_data,
                                         axis2_data=ax2_data)
        sig = self.client.get_signal(
            slice_=(slice(1, 6), slice(1, 11, 2), slice(1, 28, 3)),
            **sig_ref)

        # Check the arrays shapes
        self.assertEqual(sliced_threed_data.shape, sig.data.shape)
        self.assertEqual(sliced_ta_data.shape, sig.time_axis.data.shape)
        self.assertEqual(sliced_ax1_data.shape, sig.axis1.data.shape)
        self.assertEqual(sliced_ax2_data.shape, sig.axis2.data.shape)

        # Check the data
        self.assertTrue(np.allclose(sliced_threed_data, sig.data))
        self.assertTrue(np.allclose(sliced_ta_data, sig.time_axis.data))
        self.assertTrue(np.allclose(sliced_ax1_data, sig.axis1.data))
        self.assertTrue(np.allclose(sliced_ax2_data, sig.axis2.data))

        # Check time_limit
        time_lim = 5
        limited_ta_data = ta_data[:6]
        limited_threed_data = threed_data[:6,...]
        sig = self.client.get_signal(time_limit=time_lim,**sig_ref)
        # Check the arrays shapes
        self.assertEqual(limited_threed_data.shape, sig.data.shape)
        self.assertEqual(limited_ta_data.shape, sig.time_axis.data.shape)
        self.assertEqual(ax1_data.shape, sig.axis1.data.shape)
        self.assertEqual(ax2_data.shape, sig.axis2.data.shape)

        # Check the data
        self.assertTrue(np.allclose(limited_threed_data, sig.data))
        self.assertTrue(np.allclose(limited_ta_data, sig.time_axis.data))
        self.assertTrue(np.allclose(ax1_data, sig.axis1.data))
        self.assertTrue(np.allclose(ax2_data, sig.axis2.data))

    def test_get_signal_variant(self):

        # A typical signal
        time_axis = self.create_random_generic_signal(units='s',
                                                      signal_type='LINEAR')
        gs = self.create_random_generic_signal(
            time_axis_id=time_axis.generic_signal_id,
            units='m')
        data = np.arange(11, 5600, 0.23)
        ta_data = np.arange(0, data.shape[0])
        data_file = self.create_random_data_file(data=data,
                                                 data_file_key="abcd")
        self.store_signal(generic_signal_id=time_axis["generic_signal_id"])

        offset = -1.0
        coefficient_V2unit = 3.0
        coefficient = 2.0

        variant = "v"

        ref = self.store_signal(generic_signal_id=gs.generic_signal_id,
                                data_file_id=data_file.data_file_id,
                                data_file_key="abcd",
                                coefficient=coefficient,
                                coefficient_V2unit=coefficient_V2unit,
                                offset=offset,
                                variant=variant,
                                time_axis_variant='')
        str_id = "%s:%d:%s" % (gs.generic_signal_id, self.test_record, variant)

        # Default
        signal = self.client.get_signal(str_id)
        self.assertEqual("m", signal.units)
        self.assertTrue(np.allclose(
            (data + offset) * coefficient * coefficient_V2unit, signal.data))
        self.assertEqual("s", signal.time_axis.units)
        self.assertTrue(np.allclose(ta_data, signal.time_axis.data))
        self.assertEqual(variant, signal.ref.variant)

        # should not get signal with a different (even default) variant
        str_id = "%s:%d" % (gs.generic_signal_id, self.test_record)
        with self.assertRaises(Exception):
            signal = self.client.get_signal(str_id)


class TestUnitsInClient(CDBTestCase):
    @property
    def si(self):
        return self.client.get_unit_system("SI")

    @property
    def compass(self):
        return self.client.get_unit_system("COMPASS")

    def test_get_unit(self):
        self.assertEqual("A",
                         self.client.get_unit(unit_system=self.si,
                                              A=1)["unit_name"])
        self.assertEqual("ms",
                         self.client.get_unit(unit_system=self.compass,
                                              s=1)["unit_name"])
        self.assertEqual("s",
                         self.client.get_unit(unit_system=self.si,
                                              s=1)["unit_name"])

    def test_correct_si_system(self):
        for u in ["A", "kg", "s", "m", "cd", "mol", "K"]:
            kwargs = {u: 1}
            self.assertEqual(u,
                             self.client.get_unit(unit_system=self.si,
                                                  **kwargs)["unit_name"])

    def test_get_unit_from_parent(self):
        self.assertEqual("A",
                         self.client.get_unit(unit_system=self.compass,
                                              A=1)["unit_name"])

    def test_get_derived_unit(self):
        self.assertEqual("C",
                         self.client.get_unit(unit_system=self.si,
                                              A=1,
                                              s=1)["unit_name"])
        self.assertEqual("C",
                         self.client.get_unit(unit_system=self.compass,
                                              A=1,
                                              s=1)["unit_name"])
        self.assertEqual("ms^2*A",
                         self.client.get_unit(unit_system=self.compass,
                                              A=1,
                                              s=2)["unit_name"])
        self.assertEqual("ms^-2*A",
                         self.client.get_unit(unit_system=self.compass,
                                              A=1,
                                              s=-2)["unit_name"])

    def test_dimensionless(self):
        self.assertEqual("",
                         self.client.get_unit(
                             unit_system=self.si)["unit_name"])
        self.assertEqual("",
                         self.client.get_unit(
                             unit_system=self.compass)["unit_name"])
        self.assertEqual("",
                         self.client.get_unit(unit_system=self.compass,
                                              A=0,
                                              K=0)["unit_name"])

    def test_unit_order(self):
        kwargs = dict((u, 1) for u in ["A", "kg", "s", "m", "cd", "mol", "K"])
        self.assertEqual("m*kg*s*A*K*mol*cd",
                         self.client.get_unit(unit_system=self.si,
                                              **kwargs)["unit_name"])

    def test_add_unit_auto_dim(self):
        self.client.add_unit(self.compass.unit_system_id, "kg * A")
        self.assertEqual("kg*A",
                         self.client.get_unit(unit_system=self.compass,
                                              kg=1,
                                              A=1)["unit_name"])


class TestCDBSignal(CDBTestCase):
    '''Tests specific to the CDBSignal class'''

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_init(self):
        '''Try creating a CDBSignal instance and test basic functionality and consistence'''
        sig = pyCDB.client.CDBSignal(
            np.random.random((60, 10)),
            'linsig',
            'a.u.',
            [pyCDB.client.CDBSignal(
                np.linspace(1, 2, 60), 'time', 'ms'),
             pyCDB.client.CDBSignal(
                 np.arange(10,
                           dtype=np.float),
                 'y axis',
                 'V'), ],
            'some test signal', )
        self.assertEqual(sig.ndim, sig.data.ndim)
        self.assertIn('linsig', repr(sig))
        self.assertEqual(
            tuple(axis.data.shape[0] for axis in sig.axes), sig.data.shape)
        self.assertEqual(sig.time_axis.units, 'ms')
        self.assertEqual(sig.axis1.name, 'y axis')
        numpy.testing.assert_allclose(
            np.add(sig, sig.axis1), sig.data + sig.axes[1].data)


if __name__ == "__main__":
    run()
