from . import cdb_test_case  # Just for paths etc.
import unittest
from pyCDB import slicing


class TestSlicing(unittest.TestCase):
    """Class for testing slicing module"""

    def test_slicing(self):
        slices_known_working = {
            "...": (Ellipsis,),
            ":": (slice(None),),
            "1:": (slice(1, None),),
            ":10": (slice(None, 10),),
            "::2": (slice(None, None, 2),),
            "1::2": (slice(1, None, 2),),
            "-5:": (slice(-5, None),),
            "1:10:2,:3": (slice(1, 10, 2), slice(None, 3)),
            "1j:2j:3": (slice(1j, 2j, 3),),
            "1.3j:2j:3": (slice(1.3j, 2j, 3),),
            "1": (1,),
            "1,2": (1, 2),
            "     1,    2 ": (1, 2),
            "::2,1": (slice(None, None, 2), 1),
        }
        slices_known_not_working_ve = [  # ValueError
            "1j",  # interval needed for complex slicing
            "1.1",  # float as index - should die
            "1.1:",  # float, should give an error?
            "...,2,..."  # only one ellipsis is allowed
        ]
        slices_known_not_working_se = [
            "s"  # not allowed character
        ]

        strings_with_ellipsis = [
            (":,...,:", 2, (slice(None), slice(None))),
            ("1:10:2,...,:10", 3, (slice(1, 10, 2),
                                   slice(None), slice(None, 10))),
            ("1:10:2,...,:10", 5, (slice(1, 10, 2), slice(
                None), slice(None), slice(None), slice(None, 10)))
        ]
        complex_slices = [
            (slice(1j), True),
            (slice(None, 1j), True),
            (slice(1j, 2.0j), True),
            (slice(None), False),
            (slice(1, 1), False),
            (Ellipsis, False),
            (1, False)
        ]
        slices_to_translate = [
            (slice(None), slice(None)),
            (slice(1, 10, 2), slice(1, 10, 2)),
            (slice(2j, 10j), slice(8, 64)),
            (slice(2j, 10j, 2), slice(8, 64, 2)),
            (slice(1.5j, 10.5j), slice(4, 68)),
            (slice(None, 10.5j), slice(0, 68)),
            (slice(2j, None), slice(8, 100))
        ]
        # test parsing
        for key, value in slices_known_working.items():
            self.assertEqual(slicing.str_to_slice(key), value)
        # test parsing with a given dimension
        for slice_str, ndim, result_slice in strings_with_ellipsis:
            self.assertEqual(slicing.str_to_slice(
                slice_str, ndim=ndim), result_slice)
        # test wrong slices exceptions - ValueError
        for sl in slices_known_not_working_ve:
            with self.assertRaises(ValueError):
                a = slicing.str_to_slice(sl)
        # test wrong slices exceptions - SyntaxError = could not be parsed
        for sl in slices_known_not_working_se:
            with self.assertRaises(SyntaxError):
                a = slicing.str_to_slice(sl)
        # test ellipsis filling
        for slice_str, ndim, result_slice in strings_with_ellipsis:
            slice_to_fill = slicing.str_to_slice(slice_str)
            self.assertEqual(
                result_slice, slicing.fill_ellipsis(slice_to_fill, ndim))
        # test behaviour on too small ndim
        with self.assertRaises(ValueError):
            slicing.str_to_slice(":,...,:", ndim=1)
        with self.assertRaises(ValueError):
            slicing.fill_ellipsis((slice(None), Ellipsis, slice(None)), ndim=1)
        # test is_slice_complex function
        for sl, logical in complex_slices:
            self.assertEqual(slicing.is_slice_complex(sl), logical)
        # test complex slices translation
        from numpy import linspace
        data = linspace(1.0, 15.0, 100)
        for complex_slice, int_slice in slices_to_translate:
            self.assertEqual(slicing.translate_complex_slice(
                complex_slice, data), int_slice)
        # test exception with no data:
        with self.assertRaises(ValueError):
            slicing.translate_complex_slice(slice(1j, 2j), None)
        # test exception with non satistifying slice
        with self.assertRaises(ValueError):
            slicing.translate_complex_slice(slice(100j, 200j), data)
if __name__ == "__main__":
    unittest.main()
