import unittest
import sys
import os
from .cdb_test_case import run

from pyCDB.pyCDBBase import pyCDBBase, escape_string


class TestpyCDBBase(unittest.TestCase):
    def setUp(self):
        self._cdb_path = os.environ.get('CDB_PATH')
        return super().setUp()

    def tearDown(self):
        if self._cdb_path is not None:
            os.environ['CDB_PATH'] = self._cdb_path
        elif os.environ.get('CDB_PATH') is not None:
            del os.environ['CDB_PATH']
        return super().tearDown()

    def test_get_conf_file_search_paths__linux(self):
        paths = pyCDBBase.get_conf_file_search_paths(platform='linux')
        self.assertRegex(paths[0], r'^.*[/\\].CDBrc$')
        self.assertEqual(paths[1], '.CDBrc')
        self.assertEqual(2, len(paths))

    def test_get_conf_file_search_paths__linux_cdb_path(self):
        os.environ['CDB_PATH'] = '/mycdbpath/subpath'
        paths = pyCDBBase.get_conf_file_search_paths(platform='linux')
        self.assertRegex(paths[0], r'^[/\\]mycdbpath[/\\]subpath[/\\]\.\.[/\\].CDBrc$')
        self.assertRegex(paths[1], r'^.*[/\\].CDBrc$')
        self.assertEqual(paths[2], '.CDBrc')
        self.assertEqual(3, len(paths))

    def test_get_conf_file_search_paths__windows(self):
        paths = pyCDBBase.get_conf_file_search_paths(platform='win32')
        self.assertRegex(paths[0], r'^.*[/\\].CDBrc$')
        self.assertEqual(paths[1], '.CDBrc')
        self.assertRegex(paths[2], r"^[A-Z]:[/\\]ProgramData[/\\]CDB[/\\].CDBrc$")
        self.assertEqual(3, len(paths))

    def test_get_conf_file_search_paths__native(self):
        paths = pyCDBBase.get_conf_file_search_paths()
        if sys.platform.startswith('win'):
            self.assertRegex(paths[0], r'^C:\\Users\\[^\\]+[/\\].CDBrc$')
        else:
            self.assertEqual(paths[0], os.path.join(os.path.expanduser('~/.CDBrc')))

    def test_escape_string(self):
        # escape_string always returns str
        self.assertEqual(type(escape_string("abc")), str)

        # escape_string escapes SQL sensitive characters
        self.assertEqual(escape_string("a\\c"), "a\\\\c")
        self.assertEqual(escape_string("a\nc"), "a\\nc")
        self.assertEqual(escape_string("a\rc"), "a\\rc")
        self.assertEqual(escape_string("a'c"), "a\\'c")


if __name__ == '__main__':
    run()
