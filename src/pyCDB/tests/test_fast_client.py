from .cdb_test_case import CDBTestCase, run
import pytest
from pyCDB.fast_client import FastClient

class TestFastClient(CDBTestCase):
    fast_client: FastClient = None

    def _create_client(self):
        self.fast_client = FastClient()
        return super()._create_client()

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_correctly_stored_signal(self):
        gs = self.create_random_generic_signal()
        res = self.client.fast_store_signal(generic_signal_id = gs["generic_signal_id"], record_number=self.test_record)
        signals = self.client.get_signal_references(generic_signal_id=gs["generic_signal_id"], record_number=self.test_record)
        self.assertEqual(1, len(signals))

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_store_signal_duplicated(self):
        gs = self.create_random_generic_signal()
        res = self.client.fast_store_signal(generic_signal_id = gs["generic_signal_id"], record_number=self.test_record)
        self.assertEqual(1, res)
        res = self.client.fast_store_signal(generic_signal_id = gs["generic_signal_id"], record_number=self.test_record)
        self.assertEqual(0, res)
        signals = self.client.get_signal_references(generic_signal_id=gs["generic_signal_id"], record_number=self.test_record)
        self.assertEqual(1, len(signals))

    def test_new_data_fileF_missing_signal_ref(self):
        result = self.fast_client.new_data_fileF("test_handle_exception_cdb_exception",
                                                 record_number=self.test_record)
        self.assertEqual(type(result), dict)
        self.assertTrue("msg" in result)

    def test_new_data_fileF_invalid_record_number(self):
        gs = self.create_random_generic_signal(create_channel=True, create_time_axis=True)
        attch = self.client.get_attachment_table(generic_signal_id=gs["generic_signal_id"])[0]
        ref_args = {
            "record_number": self.test_record + 1,
            "computer_id": attch["computer_id"],
            "board_id": attch["board_id"],
            "channel_id": attch["channel_id"]
        }
        result = self.fast_client.new_data_fileF("test_handle_exception_cdb_exception",
                                                 **ref_args)
        self.assertEqual(type(result), dict)
        self.assertTrue("msg" in result)

    def test_store_signalF_text_axis_id(self):
        gs = self.create_random_generic_signal(create_channel=True, create_time_axis=True)
        attch = self.client.get_attachment_table(generic_signal_id=gs["generic_signal_id"])[0]
        ref_args = {
            "record_number": self.test_record,
            "computer_id": attch["computer_id"],
            "board_id": attch["board_id"],
            "channel_id": attch["channel_id"]
        }
        signal_id = self.fast_client.store_signalF(signal_type="time_axis_id",
                                                   **ref_args,
                                                   coefficient=0.5,
                                                   offset=960)
        str_id = f'CDB:{gs["generic_signal_name"]}_time_axis:{self.test_record}'
        signal = self.fast_client.get_signal(str_id, n_samples=10)

        self.assertEqual(signal.ref["record_number"], self.test_record)
        self.assertEqual(signal.ref["generic_signal_id"], gs["time_axis_id"])
        self.assertEqual(signal.ref["coefficient"], 0.5)
        self.assertEqual(signal.ref["offset"], 960)

        daq_info = self.fast_client.get_daq_info(
            computer_id=ref_args["computer_id"],
            board_id=ref_args["board_id"],
            channel_id=ref_args["channel_id"]
        )
        self.assertEqual(daq_info["time_axis_id"], signal_id)


    def test_store_signalF_data_id(self):
        gs = self.create_random_generic_signal(create_channel=True, create_time_axis=True)
        attch = self.client.get_attachment_table(generic_signal_id=gs["generic_signal_id"])[0]
        ref_args = {
            "record_number": self.test_record,
            "computer_id": attch["computer_id"],
            "board_id": attch["board_id"],
            "channel_id": attch["channel_id"]
        }
        time_axis_id = self.fast_client.store_signalF(signal_type="time_axis_id",
                                                      **ref_args,
                                                      coefficient=1,
                                                      offset=0)
        data_file = self.fast_client.new_data_fileF(collection_name="test_store_signalF_data_id",
                                                    **ref_args)

        with self.data_root_file(data_file["full_path"]) as f:
            data_file_key = f'CHANNEL_{attch["channel_id"]}'
            f.create_hdf5(data_file_key)
            self.fast_client.set_file_readyF(data_file_id=data_file["data_file_id"])
            signal_id = self.fast_client.store_signalF(signal_type="data_id",
                                                       **ref_args,
                                                       data_file_id=data_file["data_file_id"],
                                                       data_file_key="CHANNEL_1",
                                                       time_axis_id=time_axis_id,
                                                       daq_parameters="{}")

            self.assertEqual(type(signal_id), int, f"Received signal_id={signal_id}")

            str_id = f'CDB:{gs["generic_signal_name"]}:{self.test_record}'
            signal = self.fast_client.get_signal(str_id)

            self.assertEqual(signal.ref["record_number"], self.test_record)
            self.assertEqual(signal.ref["generic_signal_id"], gs["generic_signal_id"])
            self.assertEqual(signal.ref["data_file_id"], data_file["data_file_id"])

            daq_info = self.fast_client.get_daq_info(
                computer_id=ref_args["computer_id"],
                board_id=ref_args["board_id"],
                channel_id=ref_args["channel_id"]
            )
            self.assertEqual(daq_info["data_id"], signal_id)



if __name__ == '__main__':
    run()

