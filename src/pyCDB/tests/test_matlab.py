from .cdb_test_case import CDBTestCase, run

import numpy as np
# requires Matlab >= 2014b Python module
_matlab_engine = None


class TestMatlabUnits(CDBTestCase):
    def matlab_engine(self):
        global _matlab_engine
        try:
            import matlab.engine
            if _matlab_engine is None:
                _matlab_engine = matlab.engine.start_matlab()
        except Exception:
            print('WARNING: Matlab tests skipped, requires Matlab >= 2014b '
                  'and Matlab Python engine available!')
            self.skipTest('Matlab engine init failed')
        return _matlab_engine

    def test_get_signal_typical(self):
        # matlab engine
        mat = self.matlab_engine()

        # A typical signal
        time_axis = self.create_random_generic_signal(units='s',
                                                      signal_type='LINEAR')
        gs = self.create_random_generic_signal(
            time_axis_id=time_axis.generic_signal_id,
            units='m')
        data = np.arange(11, 5600, 0.23)
        data_file = self.create_random_data_file(data=data,
                                                 data_file_key="abcd")
        self.store_signal(generic_signal_id=time_axis["generic_signal_id"])

        offset = -1.0
        coefficient_V2unit = 3.0
        coefficient = 2.0

        self.store_signal(generic_signal_id=gs.generic_signal_id,
                          data_file_id=data_file.data_file_id,
                          data_file_key="abcd",
                          coefficient=coefficient,
                          coefficient_V2unit=coefficient_V2unit,
                          offset=offset)

        # our string id
        str_id = "%s:%d" % (gs.generic_signal_id, self.test_record)

        # Default
        py_sig = self.client.get_signal(str_id)
        m_sig = mat.cdb_get_signal2(str_id)
        m_data = np.asarray(m_sig['data']).squeeze()
        m_tax = np.asarray(m_sig['time_axis']['data']).squeeze()

        assert "m" == m_sig['units']
        assert np.allclose(m_data, py_sig.data)
        assert "s" == m_sig['time_axis']['units']
        assert np.allclose(m_tax, py_sig.time_axis.data)

        # RAW
        py_sig = self.client.get_signal(str_id + "[RAW]")
        m_sig = mat.cdb_get_signal2(str_id + "[RAW]")
        m_data = np.asarray(m_sig['data']).squeeze()
        m_tax = np.asarray(m_sig['time_axis']['data']).squeeze()
        self.assertEqual("RAW", m_sig['units'])
        assert np.allclose(m_data, py_sig.data)
        self.assertEqual("s", py_sig.time_axis.units)
        assert np.allclose(m_tax, py_sig.time_axis.data)

        # DAV
        py_sig = self.client.get_signal(str_id + "[DAV]")
        m_sig = mat.cdb_get_signal2(str_id + "[DAV]")
        m_data = np.asarray(m_sig['data']).squeeze()
        m_tax = np.asarray(m_sig['time_axis']['data']).squeeze()
        self.assertEqual("DAV", m_sig['units'])
        assert np.allclose(m_data, py_sig.data)
        self.assertEqual("s", py_sig.time_axis.units)
        assert np.allclose(m_tax, py_sig.time_axis.data)

        # Unit
        py_sig = self.client.get_signal(str_id + "[cm]")
        m_sig = mat.cdb_get_signal2(str_id + "[cm]")
        m_data = np.asarray(m_sig['data']).squeeze()
        m_tax = np.asarray(m_sig['time_axis']['data']).squeeze()
        self.assertEqual("cm", m_sig['units'])
        assert np.allclose(m_data, py_sig.data)
        self.assertEqual("s", py_sig.time_axis.units)
        assert np.allclose(m_tax, py_sig.time_axis.data)

        # Unit system
        py_sig = self.client.get_signal(str_id + "[COMPASS]")
        m_sig = mat.cdb_get_signal2(str_id + "[COMPASS]")
        m_data = np.asarray(m_sig['data']).squeeze()
        m_tax = np.asarray(m_sig['time_axis']['data']).squeeze()
        self.assertEqual("cm", m_sig['units'])
        assert np.allclose(m_data, py_sig.data)
        self.assertEqual("ms", py_sig.time_axis.units)
        assert np.allclose(m_tax, py_sig.time_axis.data)

    def test_signal_variants(self):
        # matlab engine
        mat = self.matlab_engine()

        # A typical signal
        time_axis = self.create_random_generic_signal(units='s',
                                                      signal_type='LINEAR')
        gs = self.create_random_generic_signal(
            time_axis_id=time_axis.generic_signal_id,
            units='m')
        data = np.arange(11, 5600, 0.23)
        data_file = self.create_random_data_file(data=data,
                                                 data_file_key="abcd")
        self.store_signal(generic_signal_id=time_axis["generic_signal_id"])

        offset = -1.0
        coefficient_V2unit = 3.0
        coefficient = 2.0

        variant = "var"

        self.store_signal(generic_signal_id=gs.generic_signal_id,
                          data_file_id=data_file.data_file_id,
                          data_file_key="abcd",
                          coefficient=coefficient,
                          coefficient_V2unit=coefficient_V2unit,
                          offset=offset,
                          variant=variant,
                          time_axis_variant='')
        # our string id
        str_id = "%s:%d:%s" % (gs.generic_signal_id, self.test_record, variant)

        # Default
        py_sig = self.client.get_signal(str_id)
        m_sig = mat.cdb_get_signal2(str_id)
        m_data = np.asarray(m_sig['data']).squeeze()
        assert np.allclose(m_data, py_sig.data)
        self.assertEqual(m_sig['ref']['variant'], variant)

    def test_store_signal(self):
        mat = self.matlab_engine()

        time_axis = self.create_random_generic_signal(units='s', signal_type='LINEAR')
        axis1 = self.create_random_generic_signal(units='m', signal_type='FILE')
        gs = self.create_random_generic_signal(
            time_axis_id=time_axis.generic_signal_id,
            axis1_id=axis1.generic_signal_id,
            units='A')

        test_data = mat.rand(3, 4)
        axis1_data = mat.rand(4, 1)

        mat.cdb_put_signal(gs.generic_signal_id, self.test_record, test_data, 'time0', 1.0, 'time_axis_coef', 0.1, 'axis1_data', axis1_data)

        sig = self.client.get_signal(generic_signal_id=gs.generic_signal_id, record_number=self.test_record, variant='')

        self.assertTrue(np.allclose(sig.data, np.asanyarray(test_data)))

    def test_store_signal_variant(self):
        mat = self.matlab_engine()

        time_axis = self.create_random_generic_signal(units='s', signal_type='LINEAR')
        axis1 = self.create_random_generic_signal(units='m', signal_type='FILE')
        gs = self.create_random_generic_signal(
            time_axis_id=time_axis.generic_signal_id,
            axis1_id=axis1.generic_signal_id,
            units='A')

        test_data = mat.rand(3, 4)
        axis1_data = mat.rand(4, 1)
        variant = 'test_var'

        mat.cdb_put_signal(gs.generic_signal_id, self.test_record, test_data, 'time0', 1.0, 'time_axis_coef', 0.1, 'axis1_data', axis1_data, 'variant', variant)

        sig = self.client.get_signal(generic_signal_id=gs.generic_signal_id, record_number=self.test_record, variant=variant)

        # import pdb; pdb.set_trace()

        self.assertTrue(np.allclose(sig.data, np.asanyarray(test_data)))
        self.assertTrue(np.allclose(sig.axis1.data, np.asanyarray(axis1_data).squeeze()))
        self.assertTrue(sig.ref.variant == variant)
        self.assertTrue(sig.time_axis.ref.variant == variant)
        self.assertTrue(sig.axis1.ref.variant == variant)

        msig = mat.cdb_get_signal('{id}:{shot}:{variant}'.format(id=gs.generic_signal_name, shot=self.test_record, variant=variant))
        self.assertTrue(np.allclose(sig.data, np.asarray(msig['data'])))
        self.assertTrue(np.allclose(sig.axis1.data, np.asarray(msig['axis1']['data']).squeeze()))
        self.assertTrue(np.allclose(sig.time_axis.data, np.asarray(msig['time_axis']['data']).squeeze()))
        self.assertTrue(msig['ref']['variant'] == variant)
        self.assertTrue(msig['time_axis']['ref']['variant'] == variant)
        self.assertTrue(msig['axis1']['ref']['variant'] == variant)

        # test with reusing the previously stored axes
        mat.cdb_put_signal(gs.generic_signal_id, self.test_record, test_data, 'time0', 1.0, 'variant', variant)

        sig2 = self.client.get_signal(generic_signal_id=gs.generic_signal_id, record_number=self.test_record, variant=variant)
        self.assertTrue(sig2.ref.revision == 2)
        self.assertTrue(sig2.time_axis.ref.revision == 1)
        self.assertTrue(sig2.axis1.ref.revision == 1)
        self.assertTrue(np.allclose(sig2.data, np.asanyarray(test_data)))
        self.assertTrue(np.allclose(sig2.axis1.data, np.asanyarray(axis1_data).squeeze()))
        self.assertTrue(sig2.ref.variant == variant)
        self.assertTrue(sig2.time_axis.ref.variant == variant)
        self.assertTrue(sig2.axis1.ref.variant == variant)

        # import pdb; pdb.set_trace()


if __name__ == "__main__":
    run()
