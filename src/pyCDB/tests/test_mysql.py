from .cdb_test_case import CDBTestCase

import numpy as np
from pyCDB.pyCDBBase import isstringlike, pyCDBBase, CDBException


class TestSQLAux(CDBTestCase):
    def test_mysql_str(self):
        mysql_str = pyCDBBase.mysql_str

        val = 1
        for typ in (int, np.int_, np.array):
            res = mysql_str(typ(val))
            assert isstringlike(res)
            assert int(res) == val

        val = 2.34
        for typ in (float, np.float_, np.array):
            res = mysql_str(typ(val))
            assert isstringlike(res)
            assert float(res) == val

        val = self.random_alphanum(10)
        for typ in (str, np.str_, np.array):
            res = mysql_str(typ(val), quote=False)
            assert isstringlike(res)
            assert res == val
            res = mysql_str(typ(val), quote=True)
            assert isstringlike(res)
            assert res[1:-1] == val

        val = 2.34
        res = mysql_str(np.array([val]))
        assert isstringlike(res)
        assert float(res) == val

        val = 12
        res = mysql_str(np.array([val]))
        assert isstringlike(res)
        assert int(res) == val

        self.assertRaises(CDBException,
                          mysql_str,
                          np.array([1, 2]))


