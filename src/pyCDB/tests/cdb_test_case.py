import sys

# In Python 2.6, use external unittest2 library
if sys.version_info < (2, 7):
    import unittest2 as unittest
else:
    import unittest

import os
import random
from random import randint
import string
from h5py import File
import numpy

from pyCDB import DAQClient
from pyCDB.client import OrderedDict
from pyCDB import schema


DEFAULT_TEST_SCHEMA = os.path.join(os.path.dirname(__file__), 'schema-CDB_schema-2022.sql')


def run():
    """Correctly set unittest.main and run the tests."""

    import argparse
    import tempfile

    parser = argparse.ArgumentParser(description='CDB unit tests')
    # Always explicit sql server
    parser.add_argument("CDB_HOST", help='CDB SQL server (codac3)', nargs=1)
    parser.add_argument("test", help='test to run', nargs='*')
    parser.add_argument('-l', '--log-level', default='CRITICAL',
                        help='logging level', dest='CDB_LOG_LEVEL')
    parser.add_argument('-s', '--schema', default=DEFAULT_TEST_SCHEMA, dest='schema',
                        help='Path to the schema definition relative to the tests directory.')
    args = parser.parse_args()

    os.environ["CDB_HOST"] = args.CDB_HOST[0]
    os.environ["CDB_LOG_LEVEL"] = args.CDB_LOG_LEVEL
    # Ensure that we don't use test against production database.
    # Host can be set as first argument
    os.environ["CDB_DB"] = "CDB_test"
    os.environ["CDB_USER"] = "CDB_test"
    os.environ["CDB_DATA_ROOT"] = tempfile.gettempdir()
    os.environ["CDB_LOG_FILE"] = os.devnull

    os.environ["CDB_TEST_SCHEMA"] = args.schema

    print("**************************************************************************")
    print("Important: Please ignore ERROR messages of CDB if the test results are OK.")
    print("**************************************************************************")
    print("Using CDB: %s/%s" % (os.environ["CDB_HOST"], os.environ["CDB_DB"]))
    print("**************************************************************************")
    print(sys.version)
    print("**************************************************************************")

    unittest.main(argv=(sys.argv[:1] + args.test))


class DataRootFile:
    """Temporary data file with a clean-up.

    Use this class to create a temporary file (both HDF5 or non-HDF5).
    Use the keyword "with" through the class CDBTestCase in order to
    allow clean-up:
    ```
    data_file = cdb_client.new_data_fileF
    with self.data_root_file(data_file["full_path"]) as file:
        file.create_hdf5("MY_DATASET")
        cdb_client.set_file_readyF(data_file_id=data_file["data_file_id"])
    ```
    """
    def __init__(self, file_path):
        self._file_path = file_path
        self._managed = False

    def __enter__(self):
        self._managed = True
        self.remove()
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.remove()
        self._managed = False
        return False

    @property
    def file_path(self):
        return self._file_path

    def __str__(self):
        return self._file_path

    def remove(self):
        assert self._managed, "Use the keyword \"with\" first."
        if os.path.exists(self._file_path):
            os.remove(self._file_path)

    def create_empty(self):
        assert self._managed, "Use the keyword \"with\" first."
        with open(self._file_path, "wb"):
            pass

    def create_hdf5(self, dataset_name, data=None):
        assert self._managed, "Use the keyword \"with\" first."
        with File(self._file_path, "w") as f:
            f.create_dataset(dataset_name,
                             data=numpy.array(data) if isinstance(data, list) else [123.0, 456.0, 789.0])


class CDBTestCase(unittest.TestCase):
    log_level = None
    """Set log level to all tests within the test case."""

    '''Common behaviour for CDB testing classes.'''
    def setUp(self):
        schema_path = os.getenv("CDB_TEST_SCHEMA", DEFAULT_TEST_SCHEMA)
        schema.reload_schema(
            schema_path,
            db=os.getenv("CDB_DB", "CDB_test"),
            user=os.getenv("CDB_USER", "CDB_test"),
            host=os.getenv("CDB_HOST", ":memory:"),
            password=DAQClient.CDBDAQClient.get_conf_value("CDB_PASSWORD"))
        self.create_si_units()
        self.create_compass_units()

    def __init__(self, *args, **kwargs):
        super(CDBTestCase, self).__init__(*args, **kwargs)
        self.addTypeEqualityFunc(OrderedDict, 'assertOrderedDictEqual')

    def assertOrderedDictEqual(self, d1, d2, msg=None):
        '''Comparison for ordered dictionaries.

        Floats are compared for similarity, not equality.
        '''
        self.assertIsInstance(d1, OrderedDict, 'First argument is not an OrderedDict.')
        self.assertIsInstance(d2, OrderedDict, 'Second argument is not an OrderedDict.')

        if tuple(d1.keys()) != tuple(d2.keys()):
            self.fail('Keys are not the same')

        for key in d1.keys():
            if type(d1[key]) == float:
                self.assertAlmostEqual(d1[key], d2[key])
            else:
                self.assertEqual(d1[key], d2[key])

    def _create_client(self):
        return DAQClient.CDBDAQClient()

    @property
    def client(self):
        '''Client with rights similar to those of the real client.

        :rtype: client.CDBClient
        '''
        if not hasattr(self, "_client"):
            os.environ["CDB_DB"] = os.environ.get("CDB_DB", "CDB_test")
            if os.environ["CDB_DB"].lower() != "cdb_test":
                raise RuntimeError(
                    "Tests requires DB CDB_test."
                    " Any other DB is not permitted for safety reasons."
                )
            os.environ["CDB_USER"] = os.environ.get("CDB_USER", "CDB_test")
            data_root = os.environ.get("CDB_DATA_ROOT")
            if data_root is None:
                data_root = os.path.join(os.path.dirname(__file__), "tmp")
                if not os.path.isdir(data_root):
                    os.mkdir(data_root)
                os.environ["CDB_DATA_ROOT"] = data_root
            self._client = self._create_client()

            if self.log_level is not None:
                self._client.set_log_level(self.log_level)
        return self._client

    def create_si_units(self):
        from pyCDB import units
        cdb = self.client

        si = cdb.get_unit_system("SI")
        if si:
            si_id = si["unit_system_id"]
        else:
            si_id = cdb.insert('unit_systems', {'unit_system_name': 'SI', 'editable': False},
                               return_inserted_id=True)

        def add_si_unit(name):
            dims = units.get_dimension(name)
            try:
                cdb.add_unit(si_id, name, **dims)
            except:
                raise Exception("Unit `%s` cannnot be added" % name)

        # Basic units
        add_si_unit("m")
        add_si_unit("kg")
        add_si_unit("s")
        add_si_unit("A")
        add_si_unit("K")
        add_si_unit("mol")
        add_si_unit("cd")

        # Derived
        add_si_unit("Hz")
        add_si_unit("N")
        add_si_unit("Pa")
        add_si_unit("J")
        add_si_unit("W")
        add_si_unit("C")
        add_si_unit("V")
        add_si_unit("F")
        add_si_unit("S")
        add_si_unit("Wb")
        add_si_unit("T")
        add_si_unit("H")
        add_si_unit("ohm")

        # Combined
        add_si_unit("V/m")
        add_si_unit("J/kg")
        add_si_unit("J/mol")

    def create_compass_units(self):
        client = self.client
        # si_id = client.insert('unit_systems', {'unit_system_name': 'SI'}, return_inserted_id=True)
        si_id = client.get_unit_system('SI')['unit_system_id']
        compass_id = client.insert('unit_systems',
                                   {'unit_system_name': 'COMPASS', 'parent_id': si_id},
                                   return_inserted_id=True)
        client.insert('units', {'unit_system_id': compass_id, 's': 1, 'unit_name': 'ms'})
        client.insert('units', {'unit_system_id': compass_id, 'm': 1, 'unit_name': 'cm'})
        client.insert('units', {'unit_system_id': compass_id, 'm': 2, 'kg': 1,
                                's': -2, 'unit_name': '10^-19 J'})

    @property
    def test_data_source_id(self):
        data_source_name = "TEST"
        try:
            return self.client.get_data_source_id(data_source_name)
        except:
            return self.client.insert("data_sources", {"name": "test", "subdirectory": "test"},
                                      return_inserted_id=True)

    @property
    def test_record(self):
        if "_test_record" not in dir(self):
            self._test_record = self.client.create_record()
        if not self.client.record_exists(self._test_record):
            self._test_record = self.client.create_record()
        return self._test_record

    def create_random_computer(self):
        name = self.random_name("computer")
        return self.client.create_computer(name)

    def create_random_channel(self, nodeuniqueid=None, hardwareuniqueid=None,
                              parameteruniqueid=None, create_time_axis=False, computer_id=None,
                              **kwargs):
        if not nodeuniqueid:
            nodeuniqueid = self.random_name("node")
        if not hardwareuniqueid:
            hardwareuniqueid = self.random_name("hw")
        if not parameteruniqueid:
            parameteruniqueid = self.random_name("parameter")
        if not computer_id:
            computer_id = self.create_random_computer()
        if create_time_axis:
            time_axis_id = self.create_random_generic_signal(
                               signal_type="LINEAR")["generic_signal_id"]
        else:
            time_axis_id = None
        channel = self.client.create_DAQ_channels(nodeuniqueid, hardwareuniqueid,
                                                  parameteruniqueid, self.test_data_source_id,
                                                  computer_id=computer_id, board_id=-1,
                                                  time_axis_id=time_axis_id, **kwargs)
        return channel

    def create_random_generic_signal(self, create_time_axis=False, create_channel=False, **kwargs):
        if "generic_signal_name" not in kwargs:
            kwargs["generic_signal_name"] = self.random_name("gs")
        if create_time_axis:
            time_axis_name = kwargs["generic_signal_name"] + "_time_axis"
            kwargs["time_axis_id"] = self.client.create_generic_signal(
                                         generic_signal_name=time_axis_name,
                                         data_source_id=self.test_data_source_id,
                                         signal_type="LINEAR")
        if "data_source_id" not in kwargs:
            kwargs["data_source_id"] = self.test_data_source_id
        gs_id = self.client.create_generic_signal(**kwargs)
        if create_channel:
            self.create_random_channel(generic_signal_id=gs_id)
        return self.client.get_generic_signal_references(generic_signal_id=gs_id)[0]

    def create_random_data_file(self, ready=True, chmod_ro=False, data=None,
                                data_file_key=None, **kwargs):
        if "collection_name" not in kwargs:
            kwargs["collection_name"] = self.random_name("collection")
        if "data_source_id" not in kwargs:
            kwargs["data_source_id"] = self.test_data_source_id
        if "create_subdir" not in kwargs:
            kwargs["create_subdir"] = False
        if "record_number" not in kwargs:
            kwargs["record_number"] = self.test_record
        data_file = self.client.new_data_file(**kwargs)
        data_file_id = data_file["data_file_id"]
        if data is not None and data_file_key:
            with File(data_file['full_path'], 'w') as fh5:
                fh5.create_dataset(data_file_key, data=data)
        if ready:
            self.client.set_file_ready(data_file_id, chmod_ro=chmod_ro)
        return data_file

    def create_random_data_signal(self, **kwargs):
        if "generic_signal_id" not in kwargs:
            kwargs["generic_signal_id"] = self.create_random_generic_signal(
                                              True, True)["generic_signal_id"]
        if "record_number" not in kwargs:
            kwargs["record_number"] = self.test_record
        if "data_file_id" not in kwargs:
            kwargs["data_file_id"] = self.create_random_data_file(
                                         record_number=kwargs["record_number"])['data_file_id']
        if "data_file_key" not in kwargs:
            kwargs["data_file_key"] = self.random_name("key")
        return self.client.store_signal(**kwargs)

    def store_signal(self, **kwargs):
        '''Convenience method for store_signal supplying useful default values.

        Kwargs:
        :param random_data_file: if true, create a random file and supply it to store_signal
        '''
        if kwargs.pop('random_data_file', False):
            kwargs['data_file_id'] = self.create_random_data_file()['data_file_id']
            kwargs['data_file_key'] = self.random_name("key")
        if 'record_number' not in kwargs:
            kwargs['record_number'] = self.test_record
        return self.client.store_signal(**kwargs)

    def data_root_file(self, file_name):
        if file_name.startswith("/"):
            file_name = "." + file_name
        return DataRootFile(os.path.join(self.client.data_root[0], file_name))

    def random_name(self, base='test'):
        '''Create a random name that probably won't conflict with existing ones.'''
        return "%s%d" % (base, randint(1, int(1.e12)))

    def random_alpha(self, length=1):
        return ''.join(random.choice(string.ascii_letters) for i in range(length))

    def random_alphanum(self, length=1):
        return ''.join(random.choice(string.ascii_letters + string.digits) for i in range(length))
