from . import cdb_test_case  # Just for paths etc.

import unittest

from pyCDB import epics_archiver_appliance_utils


class TestEpicsArchiverAppliancetUtils(unittest.TestCase):
    def test_decode_signal_strid(self):
        # Decode PV format correctly
        self.assertDictEqual(
            {
                "pv_name": "vacuum:1g1:pressure",
                "id_type": "EAA",
                "record_number": -1,
                "revision": -1,
                "slice": Ellipsis,
                "units": "default"
            },
            epics_archiver_appliance_utils.decode_signal_strid("EAA:vacuum:1g1:pressure")
        )
        self.assertDictEqual(
            {
                "pv_name": "vacuum:1g1:pressure",
                "id_type": "EAA",
                "record_number": -1,
                "revision": -1,
                "slice": Ellipsis,
                "units": "default"
            },
            epics_archiver_appliance_utils.decode_signal_strid("EAA:vacuum:1g1:pressure[default]")
        )

        # If not a PV format, decode standard CDB format.
        self.assertDictEqual(
            {
                "generic_signal_strid": "NIMBUS.SLOT_14.CHANNEL_001",
                "id_type": "CDB",
                "record_number": 3,
                "revision": -1,
                "slice": Ellipsis,
                "units": "default",
                "variant": ""
            },
            epics_archiver_appliance_utils.decode_signal_strid("CDB:NIMBUS.SLOT_14.CHANNEL_001:3")
        )


if __name__ == "__main__":
    unittest.main()
