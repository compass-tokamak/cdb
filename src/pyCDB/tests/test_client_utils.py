from . import cdb_test_case  # Just for paths etc.

import unittest

from pyCDB import client


class TestClientUtils(unittest.TestCase):
    def test_decode_signal_strid(self):
        # CDB
        self.assertDictEqual(
            {
                "generic_signal_strid": "NIMBUS.SLOT_14.CHANNEL_001",
                "id_type": "CDB",
                "record_number": 3,
                "revision": -1,
                "slice": Ellipsis,
                "units": "default",
                "variant": ""
            },
            client.decode_signal_strid("CDB:NIMBUS.SLOT_14.CHANNEL_001:3")
        )


if __name__ == "__main__":
    unittest.main()
