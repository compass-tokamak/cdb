from . import cdb_test_case  # Just for paths etc.

import unittest
import pytest
from pyCDB import units


class TestUnits(unittest.TestCase):
    def test_simple_units(self):
        self.assertEqual(1.0, units.convert_units("m", "m"))
        self.assertEqual(1000.0, units.convert_units("m", "mm"))

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_exponent_units(self):
        self.assertEqual(10.0, units.convert_units("m", "10^-1m"))
        self.assertEqual(100.0, units.convert_units("10^1m", "10^-1m"))
        self.assertEqual(100.0, units.convert_units("10^1m", "10^-1m^1"))
        self.assertAlmostEqual(1.60217653, units.convert_units("eV", "10^-19J"))

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_incompatible_units(self):
        with self.assertRaises(units.ConversionError):
            units.convert_units("m", "kg")

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_invalid_units(self):
        strings = (
            "*",
            "__",
            "*##$#$"
        )
        for s in strings:
            with self.assertRaises(units.UnitError):
                units.convert_units(s, "s")

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_temperature(self):
        with self.assertRaises(units.ConversionError):
            units.convert_quantity("10", "degC", "K")

    @pytest.mark.skip(reason="Broken: not executed for long time.")
    def test_html(self):
        self.assertEqual("m", units.dimension_html({'m' : 1}))
        self.assertEqual("m<sup>3</sup>", units.dimension_html({'m' : 3}))
        self.assertEqual("kg.m<sup>-3</sup>", units.dimension_html({'m' : -3, 'kg' : 1}))

if __name__ == "__main__":
    unittest.main()
