def_files = (open("cyCDB.pyx",'r'),open("cyCDB_api.pxi",'r'))
def_text = [f.readlines() for f in def_files]
for f in def_files: f.close()


mx_funcs = {
    'int':'mxCreateDoubleScalar',
    'long':'mxCreateDoubleScalar',
    'double':'mxCreateDoubleScalar',
    'char*':'mxCreateString',
    'double*':'mxCreateDoubleMatrix(mwSize m, mwSize n, mxComplexity ComplexFlag)'}

def mxStruct(data_fields_t,array_name,struct_name):
    data_signals_fields = data_fields_t.split()
    field_fill = ''
    field_init = '  const char* %s_field_names[] = {\n' % array_name
    for i in range(len(data_signals_fields)/2):
        #    print data_signals_fields[2*i]
        #    print data_signals_fields[2*i+1]
        field_init += '"'+data_signals_fields[2*i+1]+'",'
        if data_signals_fields[2*i] in mx_funcs:
            mx_func = mx_funcs[data_signals_fields[2*i]]
            field_fill += ('mxSetField(%s,0,"%s",%s(%s.%s));\n' %
             (array_name,data_signals_fields[2*i+1],mx_func,struct_name,data_signals_fields[2*i+1]))
        else:
            field_fill += ('mxSetField(%s,0,"%s",%s));\n' %
             (array_name,data_signals_fields[2*i][:-2],data_signals_fields[2*i][:-2]))
            mx_func = '!CUSTOM!'

    field_init=field_init[:-1]+'};\n'
    field_init += '  mxArray* %s = mxCreateStructArray(1, dims, %i, %s_field_names);\n' % (array_name,len(data_signals_fields)/2,array_name)
    return field_init,field_fill


data_signals_t = """double coefficient
    int axis3_revision
    int board_id
    int revision
    int computer_id
    double timestamp
    int axis2_revision
    double time0
    int axis1_revision
    char* note
    int channel_id
    long record_number
    double offset
    long generic_signal_id
    char* data_file_key
    int time_axis_revision
    long data_file_id
"""
array_name = 'data_signal_ref'
struct_name = 'signal_ref.data_signal_ref'
field_init1,field_fill1 = mxStruct(data_signals_t,array_name,struct_name)

generic_signals_t="""    char* generic_signal_name
    char* description
    long last_record_number
    long axis3_id
    long time_axis_id
    long axis2_id
    long axis1_id
    long first_record_number
    char* units
    long generic_signal_id
    int data_source_id
    char* signal_type
"""
array_name = 'generic_signal_ref'
struct_name = 'signal_ref.generic_signal_ref'
field_init2, field_fill2 = mxStruct(generic_signals_t,array_name,struct_name)

data_files_t = """
    char* collection_name
    char* data_format
    long record_number
    char* file_name
    long data_file_id
    int data_source_id
    int revision
    """
array_name = 'data_file_ref'
struct_name = 'signal_ref.data_file_ref'
field_init3, field_fill3 = mxStruct(data_files_t,array_name,struct_name)

cdb_signal_t = """
    int isset
    double* data
    long record_number
    long generic_signal_id
    int revision
    cdb_signal_t* time_axis
    cdb_signal_t* axes
    char* units
    char* description
    """
array_name = 'cdb_signal'
struct_name = 'cdb_signal'
field_init4, field_fill4 = mxStruct(cdb_signal_t,array_name,struct_name)

generic_signals_t = """
    char* generic_signal_name
    char* description
    long last_record_number
    long axis3_id
    long time_axis_id
    long axis2_id
    long axis1_id
    long first_record_number
    char* units
    long generic_signal_id
    int data_source_id
    char* signal_type
    """
array_name = 'plhs[0]'
struct_name = 'generic_signal_ref'
field_init5, field_fill5 = mxStruct(generic_signals_t,array_name,struct_name)
