"""Various functions for working with FireSignal-originated signals."""
import h5py
import numpy as np
from xml.dom.minidom import parseString

def get_xml_config(signal=None, parse=True):
    """Return data saved as XML in Firesignal.

    :param signal: A CDBSignal object
    :param parse: Whether to turn parse the data as dictionary of parameters.

    The data are saved in the same XML file in "XMLConfig" data set.
    If not applicable for the signal, None is returned.
    """
    data_file = signal.file_ref
    if not data_file:
        return None
    if data_file["data_format"] != "HDF5":
        return None
    with h5py.File(data_file["full_path"], "r") as h5f:
        xml_config = h5f.get("XMLconfig")
        if not xml_config:
            return None
        else:
            xml_config = np.array(xml_config).tostring()
            if parse:
                dom = parseString(xml_config)
                xml_config = {}
                for tag in dom.getElementsByTagName('field'):
                    key = tag.attributes["uniqueID"].value
                    value = tag.attributes["value"].value
                    xml_config[key] = value
            return xml_config

