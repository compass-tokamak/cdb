import pint
from .pyCDBBase import OrderedDict

reg = pint.UnitRegistry()


class UnitError(Exception):
    '''Base class for all unit exceptions.'''
    pass


class ConversionError(UnitError):
    '''Invalid conversion between units.'''
    pass


class ParseError(UnitError):
    '''Unit string could not be parsed.'''
    pass


class UnknownUnitError(UnitError):
    '''Unit unknown though syntactically correct.'''
    pass


def convert_units(original, new):
    '''Get original unit in terms of a new one.

    :param original: Original unit (str)
    :param new: New unit (str)

    Units with offset (e.g. temperature) not yet supported.

    >>> convert_units("m", "mm")
    1000.0
    '''
    if original == new:
        return 1.0
    try:
        original = reg.parse_expression(original)
        new = reg.parse_expression(new)
    except SyntaxError as e:
        raise ParseError(e)
    except ValueError as e:
        raise UnknownUnitError(e)

    try:
        return (original / new.to(original)).magnitude
    except ValueError as e:
        raise ConversionError(e)


def convert_quantity(quantity, original, new):
    '''Convert a physical quantity from one unit to another.

    :param quantity: Magnitude of quantity
    :param original: Original unit (str)
    :param new: Unit to convert to (str)

    >>> convert_quantity(1.0, "m", "mm")
    1000.0
    >>> convert_quantity(3.5, "atm", "10^2Pa")
    3546.375

    '''
    if original == new:
        return quantity
    return convert_units(original, new) * quantity

def convert_to_si(unit):
    '''Returns unit in terms of SI units.

    :returns Numerical value in SI units

    >>> convert_to_si('km')
    1000.0
    '''
    si_dim = get_dimension(unit)
    si_unit = dimension_txt(si_dim)
    return convert_units(unit, si_unit)

SI_UNITS = OrderedDict((
    ('length', 'm'),
    ('mass', 'kg'),
    ('time', 's'),
    ('current', 'A'),
    ('temperature', 'K'),
    ('substance', 'mol'),
    ('luminosity', 'cd')))

def get_dimension(unit):
    '''Returns dimensionality in terms of SI.

    :param unit: name of the unit
    :type unit: str

    Returns an OrderedDict with all 7 basic units
    as keys. Values are integers.
    '''
    try:
        unit = reg.parse_expression(unit)
    except Exception as e:
        raise UnknownUnitError(e)

    dimension = OrderedDict()
    for si_quantity, si_unit in SI_UNITS.items():
        dim_key = "[%s]" % si_quantity
        dim_value = unit.dimensionality.get(dim_key)
        if dim_value:
            dimension[si_unit] = int(dim_value)
        else:
            dimension[si_unit] = 0
    return dimension

def dimension_txt(dimension):
    '''Text representation of dimensions.

    :param dimension: A dictionary of {"unit_name" : "unit_dimensionality"}

    Format as understood by pint.

    >>> dimension_txt({"kg" : 1, "m" : -3})
    'kg m^-3'
    '''
    dims = []
    for si_unit, dim in dimension.items():
        if dim == 1:
            dims.append(si_unit)
        elif dim:    
            dims.append(si_unit + "^%i" % dim)
    return " ".join(dims)

def dimension_html(dimension, multiple_sign="."):
    '''HTML representation of dimensions.

    :param dimension: A dictionary of {"unit_name" : "unit_dimensionality"}
    :param multiple_sign: Sign that separates units.

    Same as dimension_txt but output is nice HTML format
    >>> dimension_html({"m" : -3})
    'm<sup>-3</sup>'

    >>> dimension_html({"kg" : 1, "m" : -3}, multiple_sign=" x ")
    'kg x m<sup>-3</sup>'
    '''
    dims = []
    for unit, dim in dimension.items():
        if dim == 1:
            dims.append(unit)
        elif dim:
            dims.append("%s<sup>%i</sup>" % (unit, dim))
    return multiple_sign.join(dims)

def exists(unit):
    try:
        reg.parse_expression(unit)
        return True
    except:
        return False
