'''@package DAQClient
COMPASS Data AcQuisition module

Contains class for DAQ management.
'''

import sys
import datetime
import logging

from .client import CDBClient
from .pyCDBBase import cdb_base_check
from . import errors

_logger = logging.getLogger('pyCDB')


class CDBDAQClient(CDBClient):
    '''CDB data acquisition class - extends CDBClient
    '''

    @cdb_base_check
    def get_record_number_from_FS_event(self, event_number, event_id='0x0000'):
        '''Return record number for a specific FireSignal event.

        :event_number: Number of the FS event.
        :event_id: string with the ID of the event. Default is "0x0000", shot event.
        '''

        cursor = self.db.cursor()
        sql_query = "SELECT `record_number` FROM `FireSignal_Event_IDs` WHERE `event_number`=%i AND event_id='%s'" % (
            event_number, event_id)
        cursor.execute(sql_query)
        if cursor.rowcount == 0:
            res = -1
        else:
            res = cursor.fetchone()[0]
        return res

    @cdb_base_check
    def create_computer(self, computer_name, description=None, location=None):
        '''Create new computer in the database.
        '''
        if computer_name == '':
            self.error('You must provide name of computer!')
        elif len(computer_name) > 45:
            self.error("Computer name too long (max. allowed 45): %d" % len(computer_name))
        computer = {
            "computer_name": computer_name,
            "location": location,
            "description": description,
        }
        return self.insert("da_computers", computer, return_inserted_id=True)

    @cdb_base_check
    def create_data_source(self, name, subdirectory, description):
        return self.insert("data_sources", {"name": name, "subdirectory": subdirectory, "description": description},
                           return_inserted_id=True)

    @cdb_base_check
    def change_calibration(self, generic_signal_id=None, computer_id=None, board_id=None,
                           channel_id=None, **kwargs):
        '''Shortcut method for changing the calibration of channel / generic signal)

        Specify channel / generic signal by either choice (overspecification will be rejected):
        1) generic_signal_id
        2) computer_id, board_id, channel_id

        Arguments that are specified, will be changed, other won't.
        '''
        if generic_signal_id != None:
            if computer_id or board_id or channel_id:
                self.error("You should specify only generic signal or channel, not both.")
            attachment = self.get_attachment_table(generic_signal_id=generic_signal_id)[0]
            computer_id = attachment["computer_id"]
            board_id = attachment["board_id"]
            channel_id = attachment["channel_id"]
        elif computer_id != None and board_id != None and channel_id != None:
            attachment = self.get_attachment_table(computer_id=computer_id, board_id=board_id, channel_id=channel_id)[0]
            generic_signal_id = attachment["attached_generic_signal_id"]
        else:
            self.error("You have to specify either generic signal or channel")
        self.attach_channel(generic_signal_id=generic_signal_id, pc_id=computer_id, board_id=board_id,
                            channel_id=channel_id, force_detach=True, **kwargs)

    @cdb_base_check
    def create_DAQ_channels(self, nodeuniqueid, hardwareuniqueid, parameteruniqueid, data_source_id=None, note='',
                            time_axis_id=None, axis1_id=None, axis2_id=None, axis3_id=None,
                            axis4_id=None, axis5_id=None, axis6_id=None, **kwargs):
        '''Creates a new DAQ channel.

        It enables automatic creation of new channels and boards (if requested). New computers
        cannot be created automatically. Use `create_computer` method in such case.

        :param data_source_id: Not required only if you specify generic_signal_id

        Keyword arguments:

        :param default_generic_signal_id: If set, it is used as default generic signal
            instead of creating a new one.
        :param computer_id: Valid computer id that will be used if there is no channel for the computer yet.
        :param board_id: Board id for a non-existent board (if found, stored board_id is always used). If set to -1,
            the number will be evaluated automatically. If not set, exception is thrown. Please note that two
            hardwareuniqueid's can be used as one board (although not recommended).
        :param channel_id: If set, it will be used. Otherwise (or value of -1), a new one will be created.
        :param coefficient_lev2V: coefficient_lev2V for the default channel attachment.
        :param offset: offset for the default channel attachment.
        '''

        sql_query = ("SELECT * FROM `DAQ_channels` WHERE `nodeuniqueid` = '%s' "
                     "AND `hardwareuniqueid` = '%s' AND `parameteruniqueid` = '%s' LIMIT 1"
                     % (nodeuniqueid, hardwareuniqueid, parameteruniqueid))  # TODO: use mysql_values
        if len(self.query(sql_query)):
            self.error(
                "DAQ channel already exists: {0}/{1}/{2}".format(nodeuniqueid, hardwareuniqueid, parameteruniqueid))

        # Computer must be already in CDB. Either find its in DAQ_channels or require explicit value.
        computer_sql = "SELECT computer_id FROM `DAQ_channels` WHERE `nodeuniqueid` = '%s' LIMIT 1" % nodeuniqueid
        try:
            computer_id = self.query(computer_sql)[0]["computer_id"]
        except:
            computer_id = kwargs.get("computer_id")
            if not computer_id:
                self.error("The computer already has to have channels or you have to specify the computer_id.")

        # Board can be already in CDB, but can be created automatically.
        board_id = self.get_board_id(nodeuniqueid, hardwareuniqueid)
        if not board_id:
            board_id = kwargs.get("board_id")
            if board_id is None:
                self.error("Automatic board creation not explicitly requested: {0}/{1}/{2}".format(nodeuniqueid,
                                                                                                   hardwareuniqueid,
                                                                                                   parameteruniqueid))
            elif board_id == -1:
                board_query = "SELECT (MAX(`board_id`) + 1) AS board_id FROM `DAQ_channels` WHERE `nodeuniqueid` = '%s' LIMIT 1" % nodeuniqueid
                board_id = self.query(board_query, error_if_empty=True)[0]["board_id"] or 1

        # Channel id can be set manually or automatically.
        channel_id = kwargs.get("channel_id")
        if channel_id == None or channel_id == -1:
            channel_query = """SELECT (MAX(`channel_id`) + 1) AS channel_id FROM `DAQ_channels`
                               WHERE `nodeuniqueid` = '%s' AND `hardwareuniqueid` = '%s'
                               LIMIT 1""" % (nodeuniqueid, hardwareuniqueid)
            channel_id = self.query(channel_query, error_if_empty=True)[0]["channel_id"] or 1

        # Generic signal id can be set manually or automatically (then a new gs is created).
        generic_signal_id = kwargs.get("generic_signal_id")
        if generic_signal_id:
            # Test existence
            generic_signal = self.get_generic_signal_references(generic_signal_id=generic_signal_id)
            if not len(generic_signal):
                self.error("Non-existent generic_signal {0} requested for new channel.".format(generic_signal_id))
        else:
            if not data_source_id:
                self.error("You have to specify data_source_id when creating new channels.")
            generic_signal_name = "{0}.{1}.{2}".format(nodeuniqueid, hardwareuniqueid, parameteruniqueid)
            generic_signal_id = self.create_generic_signal(generic_signal_name=generic_signal_name,
                                                           first_record_number=1, last_record_number=-1,
                                                           data_source_id=data_source_id, time_axis_id=time_axis_id,
                                                           axis1_id=axis1_id, axis2_id=axis2_id, axis3_id=axis3_id,
                                                           axis4_id=axis4_id, axis5_id=axis5_id, axis6_id=axis6_id
                                                           )

        # Create the DAQ channel
        insert_args = {
            "computer_id": computer_id, "board_id": board_id, "channel_id": channel_id,
            "nodeuniqueid": nodeuniqueid, "hardwareuniqueid": hardwareuniqueid, "parameteruniqueid": parameteruniqueid,
            "default_generic_signal_id": generic_signal_id, "note": note
        }
        self.insert("DAQ_channels", insert_args)

        # Attach default channel (with possible calibration)
        coefficient_lev2V = kwargs.get("coefficient_lev2V", 1.0)
        offset = kwargs.get("offset", 0.0)
        self.attach_channel(computer_id, board_id, channel_id, generic_signal_id, coefficient_lev2V=coefficient_lev2V,
                            offset=offset)
        return self.get_channel_references(computer_id=computer_id, board_id=board_id, channel_id=channel_id)[0]

    @cdb_base_check
    def get_board_id(self, description='', description2=''):
        '''Returns board ID by computer and board name
        '''

        cursor = self.db.cursor()
        cursor.execute("SELECT * FROM `DAQ_channels` WHERE `nodeuniqueid`='%s' AND `hardwareuniqueid`='%s' " %
                       (description, description2))
        if cursor.rowcount == 0:
            self._logger.error("Node unique ID \"%s\", HW unique ID \"%s\" NOT in CDB database." % (description, description2))
            return None
        res = self.row_as_dict(cursor.fetchone(), cursor.description)
        cursor.close()
        return res['board_id']

    @cdb_base_check
    def detach_channel(self, pc_id, board_id, channel_id, timestamp=None):
        '''Detach channel in the channel setup'''

        cursor = self.db.cursor()
        sqlquery = ("SELECT `default_generic_signal_id` FROM `DAQ_channels` WHERE "
                    "`computer_id`=%i AND `board_id`=%i AND `channel_id`=%i"
                    % (pc_id, board_id, channel_id))
        cursor.execute(sqlquery)
        row = cursor.fetchone()
        if row is None:
            self.error("Cannot find channel in DAQ_channels.")
        generic_signal_id = row[0]
        cursor.close()
        self.attach_channel(pc_id, board_id, channel_id, generic_signal_id, timestamp=timestamp, force_detach=True,
                            coefficient_V2unit=1.0, time_axis_id=None)

    @cdb_base_check
    def is_channel_attached(self, pc_id, board_id, channel_id):
        '''Checks whether a physical channel is attached'''

        sqlquery = ("SELECT `is_attached` FROM `channel_attachments` WHERE "
                    "`computer_id`=%i AND `board_id`=%i AND `channel_id`=%i"
                    % (pc_id, board_id, channel_id))
        cursor = self.db.cursor()
        cursor.execute(sqlquery)
        row = cursor.fetchone()
        cursor.close()
        if row is None:
            return False
        else:
            return bool(row[0])

    @cdb_base_check
    def attached_channel_reference(self, pc_id, board_id, channel_id):
        '''Checks whether a physical channel is attached'''

        sqlquery = ("SELECT * FROM `channel_attachments` WHERE "
                    "`computer_id`=%i AND `board_id`=%i AND `channel_id`=%i"
                    % (pc_id, board_id, channel_id))
        cursor = self.db.cursor()
        cursor.execute(sqlquery)
        row = cursor.fetchone()
        if row is not None:
            row = self.row_as_dict(row, cursor.description)
        cursor.close()
        return row

    @cdb_base_check
    def get_FS_attachement(self, nodeuniqueid, hardwareuniqueid, parameteruniqueid):
        '''Return the currently attached generic signal reference and the corresponding line from the channel_attachments view for a FireSignal channel'''

        cdb_ids = self.FS2CDB_ref(nodeuniqueid, hardwareuniqueid, parameteruniqueid)
        sqlquery = ("SELECT * FROM `channel_attachments` WHERE "
                    "`computer_id`=%i AND `board_id`=%i AND `channel_id`=%i"
                    % (cdb_ids['computer_id'], cdb_ids['board_id'], cdb_ids['channel_id']))
        cursor = self.db.cursor()
        cursor.execute(sqlquery)
        row = cursor.fetchone()
        if row is None:
            self._logger.error('could not find the FireSignal channel in CDB')
            return None
        row = self.row_as_dict(row, cursor.description)
        signal_ref = self.get_generic_signal_references(generic_signal_id=row['attached_generic_signal_id'])[0]
        cursor.close()
        return signal_ref, row

    @cdb_base_check
    def is_generic_signal_attached(self, generic_signal_id):
        '''Checks whether a generic signal is attached to a physical channel
        '''
        sql_query = ("SELECT `is_attached` FROM `channel_attachments` WHERE "
                     "`attached_generic_signal_id`=%i AND `is_attached`=1"
                     % generic_signal_id)
        rows = self.query(sql_query)
        if len(rows) == 0:
            return False
        elif len(rows) == 1:
            return True
        else:
            self.error("Error in database: generic channel id %i attached to more than 1 channel!" % generic_signal_id)

    @cdb_base_check
    def attach_channel(self, pc_id, board_id, channel_id, generic_signal_id,
                       offset=None, coefficient_lev2V=None, coefficient_V2unit=None,
                       uid=None, note='', parameters=None,
                       timestamp=None, force_detach=False, **kwargs):
        '''Attach a channel to a generic signal (specified by id)

        Parameters that are not specified (default values None) will be inherited
        from previous channel attachment record.

        parameters is a JSON text that describes channel parameters
        '''

        last = self.get_attachment_table(timestamp=timestamp, computer_id=pc_id, board_id=board_id,
                                         channel_id=channel_id)
        if len(last) > 1:
            self.error('get_attachment_table returned more than 1 row.')
        elif len(last) == 0:
            self._logger.warning('DAQ channel %i/%i/%i has never been configured' % (pc_id, board_id, channel_id))
            last = {}
            last['offset'] = 0
            last['coefficient_lev2V'] = 1
            last['coefficient_V2unit'] = 1
            last['time_axis_id'] = None
            last['parameters'] = ''
            last['uid'] = None
        else:
            last = last[0]

        channel_ref = self.get_channel_references(computer_id=pc_id, board_id=board_id,
                                                  channel_id=channel_id)
        if channel_ref:
            channel_ref = channel_ref[0]
        else:
            self.error('channel %i/%i/%i not found' % (pc_id, board_id, channel_id))
        if timestamp is None:
            timestamp = '\x00NOW()'
        else:
            self._logger.warning('Using non-default timestamp can be very dangerous!')
            if isinstance(timestamp, (int, long)):
                timestamp = datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S')
        if last.get('is_attached'):
            if force_detach:
                if generic_signal_id != channel_ref['default_generic_signal_id']:
                    # warn only if this is not a detach
                    self._logger.info("forcing detachment of currently attached channel")
                    # self.detach_channel(pc_id, board_id, channel_id, timestamp=timestamp)
            else:
                # self._logger.error("channel (%i,%i,%i) currently attached, operation failed" % (pc_id, board_id, channel_id))
                self.error(errors.ChannelAlreadyAttachedError(pc_id, board_id, channel_id))
        # detach generic signal from all other channels
        daq_attachments = self.get_attachment_table(generic_signal_id=generic_signal_id, timestamp=timestamp)
        for da in daq_attachments:
            if (da['computer_id'] != pc_id and da['board_id'] != board_id and
                    da['channel_id'] != channel_id):
                self._logger.warning('detaching computer_id: %i, board_id: %i, channel_id: %i'
                                     ' because it is attached to gen.sig %i' %
                                     (da['computer_id'], da['board_id'], da['channel_id']), generic_signal_id)
                self.detach_channel(da['computer_id'], da['board_id'], da['channel_id'], timestamp=timestamp)

        if offset is None:
            offset = last['offset']
        if coefficient_lev2V is None:
            coefficient_lev2V = last['coefficient_lev2V']
        if coefficient_V2unit is None:
            coefficient_V2unit = last['coefficient_V2unit']
        time_axis_id = kwargs.get('time_axis_id', last['time_axis_id'])
        if parameters is None:
            parameters = last['parameters']
        if not parameters:
            # empty parameters are not stored
            parameters = ''
        elif not self.is_json(parameters):
            self.error('parameters must be a valid JSON string')
        if uid is None:
            uid = last['uid']
        sqlquery = ("INSERT INTO `channel_setup` "
                    "(`attached_generic_signal_id`,`attach_time`,`computer_id`,`board_id`,`channel_id`, "
                    "`offset`,`coefficient_lev2V`,`coefficient_V2unit`,`uid`,`note`, "
                    "`time_axis_id`,`parameters`) %s;"
                    % self.mysql_values((generic_signal_id, timestamp, pc_id, board_id, channel_id,
                                         offset, coefficient_lev2V, coefficient_V2unit, uid, note,
                                         time_axis_id, parameters)))
        cursor = self.db.cursor()
        try:
            cursor.execute(sqlquery)
            self.db.commit()
        except:
            cursor.close()
            self.error('SQL query: {0}\nSQL error: {1}'.format(sqlquery, str(sys.exc_info()[1])))
        cursor.close()

    @cdb_base_check
    def switch_channels(self, pc_id_1, board_id_1, channel_id_1, pc_id_2, board_id_2, channel_id_2,
                        uid=None, note='', timestamp=None):
        '''Switch 2 channels attached generic signals

        Keeps the offset and coefficient from the original channel, i.e.,
        the offset and the coefficient are switched as well.
        '''

        if timestamp is None:
            timestamp = '\x00NOW()'
        elif isinstance(timestamp, (int, long)):
            timestamp = datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S')
        ref1 = self.attached_channel_reference(pc_id_1, board_id_1, channel_id_1)
        ref2 = self.attached_channel_reference(pc_id_2, board_id_2, channel_id_2)
        if ref1['is_attached']:
            self.attach_channel(pc_id_2, board_id_2, channel_id_2, ref1['attached_generic_signal_id'],
                                offset=ref1['offset'], coefficient=ref1['coefficient'], uid=uid,
                                note=note, timestamp=timestamp, force_detach=True)
        else:
            print('detaching channel %i,%i,%i' % (pc_id_2, board_id_2, channel_id_2))
            self.detach_channel(pc_id_2, board_id_2, channel_id_2, timestamp)
        if ref2['is_attached']:
            self.attach_channel(pc_id_1, board_id_1, channel_id_1, ref2['attached_generic_signal_id'],
                                offset=ref2['offset'], coefficient=ref2['coefficient'], uid=uid,
                                note=note, timestamp=timestamp, force_detach=True)
        else:
            print('detaching channel %i,%i,%i' % (pc_id_1, board_id_1, channel_id_1))
            self.detach_channel(pc_id_1, board_id_1, channel_id_1, timestamp)

    def signal_setup(self, generic_signal_id, parameters='', timestamp=None, uid=None):
        '''Write generic signal parameters (setup) into signal_setup

        :param generic_signal_id: numerical generic_signal_id
        :param parameters: signal parameters (typically JSON/YAML formatted text)
        :param timestamp: time, default is now
        :param uid: user id
        '''

        fields = {}
        if timestamp is None:
            timestamp = '\x00NOW()'
        fields['generic_signal_id'] = generic_signal_id
        if not parameters:
            # empty parameters are not stored
            parameters = ''
        elif not self.is_json(parameters):
            self.error('parameters must be a valid JSON string')
        fields['parameters'] = parameters
        fields['timestamp'] = timestamp
        self.insert('signal_setup', fields)

    def nodiff_signal_data(self, signal1, signal2):
        '''Compare data of two signals. Returns True for identical signals.

        :param signal1: first signal str_id or reference
        :param signal2: second signal str_id or reference
        '''
        import h5py
        from pyCDB.client import get_h5_dataset
        file_refs = []
        sig_refs = []
        for signal in (signal1, signal2):
            if isinstance(signal1, (str, unicode)):
                refs = self.get_signal_references(signal)
                assert len(refs) == 1
                sig_refs.append(refs[0])
            else:
                sig_refs.append(signal)
            file_refs.append(self.get_data_file_reference(**sig_refs[-1]))
        # with , h5py.File(file_refs[1]['full_path'], 'r') as f1, f2
        # data1 = get_h5_dataset(file_refs[0]['full_path'], sig_refs[0]['data_file_key'])
        # data2 = get_h5_dataset(file_refs[1]['full_path'], sig_refs[1]['data_file_key'])
        try:
            f1 = h5py.File(file_refs[0]['full_path'], 'r')
        except:
            return False
        try:
            f2 = h5py.File(file_refs[0]['full_path'], 'r')
        except:
            f1.close()
            return False
        try:
            data1 = get_h5_dataset(file_key=sig_refs[0]['data_file_key'], file_handle=f1)
            data2 = get_h5_dataset(file_key=sig_refs[1]['data_file_key'], file_handle=f1)
            # check the size first
            if data1.shape != data2.shape:
                return False
            # check first couple numbers
            n = min(16, data1.len())
            if (data1[:n] != data2[:n]).any():
                return False
        except:
            res = False
        finally:
            f1.close()
            f2.close()
        # check more here?
        #
        # the signals are identical
        return True

    def find_daq_signals(self, record_number=-1):
        '''Find all data signals that originate in any DAQ channel.
        '''
        record_number = self.resolve_record_number({'record_number': record_number})
        sql_query = ('SELECT * FROM data_signals WHERE '
                     'record_number = {0} AND computer_id IS NOT NULL'
                     ).format(record_number)
        rows = self.query(sql_query)
        # get only the last revision
        if rows and len(rows) > 1:
            res = self.get_max_revisions(rows, deleted=True)
        else:
            res = rows
        # filter out deleted signals
        res = filter(lambda x: not bool(x['deleted']), res)
        return res

    def find_duplicate_signals(self, *record_numbers):
        '''Find duplicate signals from DAQ channels in a set of records.

        Example: cdb.find_duplicate_signals(6228, 6229)

        :param *record_numbers: two or more record numbers
        '''
        from itertools import combinations
        from collections import defaultdict
        res = defaultdict(list)
        for rec1, rec2 in combinations(record_numbers, 2):
            sigs1 = self.find_daq_signals(rec1)
            gs_ids1 = set(s['generic_signal_id'] for s in sigs1)
            sigs2 = self.find_daq_signals(rec2)
            gs_ids2 = set(s['generic_signal_id'] for s in sigs2)
            gs_ids12 = gs_ids1 & gs_ids2
            sigs_dict1 = dict(((s['generic_signal_id'], s) for s in sigs1 if s['generic_signal_id'] in gs_ids12))
            sigs_dict2 = dict(((s['generic_signal_id'], s) for s in sigs2 if s['generic_signal_id'] in gs_ids12))
            for gs_id in gs_ids12:
                if self.nodiff_signal_data(sigs_dict1[gs_id], sigs_dict2[gs_id]):
                    res[gs_id].append((rec1, rec2))
        return res
