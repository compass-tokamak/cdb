__all__ = ['client', 'DAQClient', 'logbook']

# Python versions
# ---------------
import sys
if sys.version_info >= (3, 0) and sys.version_info < (3, 4) or sys.version_info < (2, 6):
    print("pyCDB supported in Python 2.6, 2.7 and 3.4")

# Jython versions
# ---------------
# Jython library jython.jar does not add site-packages directory to its sitedir.
# However, setuptools (required by pint) have to be installed. And they do it in sitedir.
# => We have to manually add the jython site-packages path to sites. With this
# hack, it is possible to install all packages in the site-packages path.
# See: http://bugs.jython.org/issue2143
import platform
if platform.python_implementation() == "Jython":
    from org.python.util import jython
    jar_location = jython().getClass().getProtectionDomain().getCodeSource().getLocation().getPath()
    import site
    import os.path
    site.addsitedir(os.path.join(jar_location, 'Lib/site-packages'))
