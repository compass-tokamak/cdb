#!/usr/bin/env python
import sys
if sys.version_info < (3,0):
    from Tkinter import *
else:
    from tkinter import *
from PIL import Image, ImageTk, ImageFont
from PIL import ImageDraw
import os, sys
import pyCDB.client
import matplotlib.pyplot as plt
import numpy as np
import h5py


class Viewer:
    def __init__(self, master):
        self.top = master
          
        self.index = 0
        self.cdb = pyCDB.client.CDBClient()
        self.file_read = True
        self.sfim = IntVar()
        self.sfim.set(1)
#        self.file_read = False

        self.sn_var = IntVar()
        self.camera_num = IntVar()
        self.camera_num.set(1)
        self.sn_var.set(self.cdb.last_shot_number() -3 )
###        self.sn_var.set(4327)
###        self.sn_var.set(5228)
###        self.sn_var.set(5310)
###        self.sn_var.set(4607)
        self.frame_nr = IntVar()
        self.frame_nr.set(1)

#        self.font = ImageFont.truetype('/usr/share/fonts/truetype/msttcorefonts/Times_New_Roman.ttf', 30)
        self.font = ImageFont.truetype('/usr/share/fonts/truetype/msttcorefonts/Arial_Black.ttf', 30)

        self.loadData(self.sn_var.get())
        self.actual_sn = self.sn_var.get()

##        self.title = Label(text='Fast camera viewer')
##        self.title.pack()
        self.im = self.getImage(0)
        print ("Image size: ",self.im.size )
        self.size = self.im.size
 
        self.tkimage = ImageTk.PhotoImage(self.im, palette=256)
        
        self.lbl = Label(master, image=self.tkimage)
#        self.lbl = Label(master, text="asd")
        self.lbl.pack(side='top')

        # the button frame
        fr = Frame(master)

        fr.pack(side='top', expand=1, fill='x')
        ilabel = Label(fr, text="Image number:")
        ilabel.grid(row=0, column=0, sticky="w", pady=4)


        entry = Entry(fr, textvariable= self.frame_nr)
        entry.grid(row=0, column=1, sticky="w", pady=4)
        entry.bind('<Return>', self.getimgnum)

        back = Button(fr, text="back", command=lambda : self.nextframe(-1))
        back.grid(row=0, column=2, sticky="e", padx=4, pady=4)
        
        next = Button(fr, text="next", command=lambda : self.nextframe(1))
        next.grid(row=0, column=3, sticky="w", padx=4, pady=4)

#        reloadBtn = Button(fr, text="Reload", command=self.reloadAvailImages)
#        reloadBtn.grid(row=0, column=4, sticky="w", padx=4, pady=4)


        ilabel2 = Label(fr, text="Shot number:")
        ilabel2.grid(row=1, column=0, sticky="w", pady=4)
        entry2 = Entry(fr, textvariable= self.sn_var )
        entry2.grid(row=1, column=1, sticky="w", pady=4)
        entry2.bind('<Return>', self.change_rn)
#        print "asdfasdf  " , str(entry2.get())
        next2 = Button(fr, text="Change", command=self.change_rn)
        next2.grid(row=1, column=2, sticky="e", padx=4, pady=4)

        c = Checkbutton(fr, text="Read from CDB (i.e. not from HDF5 file)", variable=self.file_read, command=self.change_source)
        c.grid(row=0, column=4, sticky="w", padx=4, pady=4)

        c2 = Checkbutton(fr, text="Substract the first image", variable=self.sfim, command=self.change_sfim )
        c2.grid(row=1, column=4, sticky="w", padx=4, pady=4)
#        self.c2.select()

        r1 = Radiobutton(fr, text="Edicam 1", variable=self.camera_num, value=1, command=self.change_camera)
        r2 = Radiobutton(fr, text="Edicam 2", variable=self.camera_num, value=2, command=self.change_camera)

        r1.grid(row=0, column=5, sticky="e", padx=4, pady=4)
        r2.grid(row=1, column=5, sticky="e", padx=4, pady=4)


    def change_source(self) :
        self.file_read = not self.file_read
        if self.file_read :
              print("Reading from HDF directly.")
        else :
              print("Reading via CDB functions")

        self.loadData(self.actual_sn)
        self.nextframe(1)
        self.nextframe(-1)

    def change_camera(self) :
        self.loadData(self.actual_sn)
        self.nextframe(1)
        self.nextframe(-1)

    def change_rn(self) :
        self.loadData(self.sn_var.get())
        #self.loadData(self.actual_sn)
        self.nextframe(1)
        self.nextframe(-1)

    def change_sfim(self) :
        self.loadData(self.sn_var.get())
        self.nextframe(1)
        self.nextframe(-1)


    def reloadAvailImages(self) :
        self.max_frame = 5
        self.image_list = range(1,self.max_frame)
        print("Implement??")

    # image doesn't appear unless put Image.open in separate function?
    # and need to use tkimage.paste, not ImageTk.PhotoImage
    def getImage(self, index):
        x0 = 0
        x1 = 900
        y0 = 20 
        y1 = 1020

        if self.sfim.get() :
             onezero = 1.0
             if self.camera_num.get() == 2 :
                   onezero = 0.0
        else :
             onezero = 0.0

        if self.file_read :



             print(self.camera_num.get())
             if self.camera_num.get() == 1 :
                  self.sd0 = self.h5f['Edicam1'][0,:,:]
                  self.sd = self.h5f['Edicam1'][index-1,:,:]
#                  self.im = Image.fromarray((self.sd[y0:y1:1,x1:x0:-1].transpose() + onezero * (- self.sd0[y0:y1:1,x1:x0:-1].transpose() + self.dd)   )/16)
                  self.im = Image.fromarray((self.sd[:,::-1].transpose() + onezero * (- self.sd0[::1,::-1].transpose() + self.dd)   )/16)

             if self.camera_num.get() == 2 :
                  self.sd0 = self.h5f['Edicam2'][0,:,:]
                  self.sd = self.h5f['Edicam2'][index-4,:,:]
                  self.im = Image.fromarray((self.sd[y0:y1:1,x1:x0:-1].transpose() + onezero * (- self.sd0[y0:y1:1,x1:x0:-1].transpose()  + self.dd)   )/16)

        else :
             self.im = Image.fromarray((self.sig.data[index-1,y0:y1:1,x1:x0:-1].transpose() + onezero * (- self.sig.data[0,y0:y1:1,x1:x0:-1].transpose() + self.dd  )  )/16)
#             self.im = Image.fromarray((self.sig.data[index-1,y0:y1:1,x1:x0:-1].transpose() )/16)
   
        print("Showing image for shot number: ", self.actual_sn, " , frame:  ", index + 1)

        self.draw = ImageDraw.Draw(self.im)
        text = "Shot number: " + str(self.actual_sn)
     
        self.draw.text((10, 5), text, font=self.font, fill=  "white" if not self.sfim.get()  else "black")
        text = "Frame:  " + str(self.index + 1) + "/" + str(self.max_frame)
        self.draw.text((10, 40), text, font=self.font, fill=  "white" if not self.sfim.get()  else "black")
        if not self.file_read : 
             text = "Time:  %.2f ms" %self.sig.time_axis.data[self.index]
             self.draw.text((10, 75), text, font=self.font, fill=  "white" if not self.sfim.get()  else "black")

        return self.im

    def loadData(self, sn) :
        self.actual_sn = sn
        if self.file_read :
#             sig_ref = self.cdb.get_signal_references("edicam2_image_data:" + str(sn))
#             file_ref = self.cdb.get_data_file_reference(data_file_id = sig_ref[-1]['data_file_id'])
#             full_path = file_ref['full_path']
             if self.camera_num.get() == 1 :
                  full_path = os.path.join(self.cdb.get_data_root_path()[0],  str(sn),"EDICAM/Edicam1.1.h5")
                  print(full_path)
                  self.h5f = h5py.File(full_path,'r')
                  self.dd = self.h5f['Edicam1'][0,:,:].max()
                  print("Edicam 1 HDF5 file open for shot number:  ", str(sn))
                  self.max_frame = self.h5f['Edicam1'].shape[0]
             if self.camera_num.get() == 2 :
                  full_path = os.path.join(self.cdb.get_data_root_path()[0],  str(sn),"EDICAM/Edicam2.1.h5")
                  print(full_path)
                  self.h5f = h5py.File(full_path,'r')
                  self.dd = self.h5f['Edicam2'][0,:,:].max()
                  print("Edicam 2 HDF5 file open for shot number:  ", str(sn))
                  self.max_frame = self.h5f['Edicam2'].shape[0]
        else :
             print("Loding data for shot number:  ", str(sn))
             if self.camera_num.get() == 1 :             
                  self.sig = self.cdb.get_signal("edicam_image_data:" + str(sn))
             if self.camera_num.get() == 2 :             
                  self.sig = self.cdb.get_signal("edicam2_image_data:" + str(sn))

             self.dd = self.sig.data[0,:,:].max()

             print("Data loaded..  ", self.sig.data.shape)
             self.max_frame = self.sig.data.shape[0]

#        self.getImage(2)
        self.image_list = range(1,self.max_frame)
#        self.getImage(1)

         

    def nextframe(self,i=1, imgnum=-1):
        if imgnum == -1:
            self.index += i
        else:
            self.index = imgnum - 1

        if self.index > len(self.image_list ):
            self.index = 0
        elif self.index < 0:
            self.index = len(self.image_list)

        self.frame_nr.set(self.index+1)
        
        im = self.getImage(self.index)
        self.tkimage.paste(im)

    def getimgnum(self, event=None):
        self.nextframe(imgnum=self.frame_nr.get())

if __name__ == "__main__":
    root = Tk()
    root.title("Python edicam camera viewer")
    app = Viewer(root)
    root.mainloop()


