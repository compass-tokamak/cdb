from typing import Any, Dict
import re
from . import client


_pv_strid_re = re.compile(r"^EAA:([^[]+)(\[[^\]]+\])*$")


def decode_signal_strid(str_id) -> Dict[str, Any]:
    '''Decode signal string id

    :param str_id: string identifier, defined as `id_type:identification[units_or_slice][units_or_slice]`.

    This function mimic behaviour of `client.decode_signal_strid()` function.
    In the current implementation, there is no support for units or slicing
    for the id_type `EAA`. Therefore, these components of str_id are ignored
    and defaults will be used.

    For more details, see `client.decode_signal_strid()`.
    '''
    str_id = str(str_id)
    if str_id.startswith("EAA:"):
        match = _pv_strid_re.match(str_id)
        if match is None:
            raise Exception('syntax error in EAA string id: invalid format')
        return {
            "id_type": "EAA",
            "pv_name": match.group(1),
            "record_number": -1,
            "revision": -1,
            "slice": Ellipsis,
            "units": "default"
        }
    else:
        return client.decode_signal_strid(str_id)
