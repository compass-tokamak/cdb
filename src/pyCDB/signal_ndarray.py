'''Module providing a :class:`numpy.ndarray` subclass SignalNdArray.
'''


import numpy as np
from numbers import Number


class SignalNdArray(np.ndarray):
    ''':class:`nupy.ndarray` subclass which tries to behave like a signal
    of a dependent variable by maintaining a connection to its axes as
    independent variables.

    :Attributes:
    axes: None or list of SignalNdArrays
        None if axes manipulation was ambiguous at some point or not provided
        each element in the list corresponds to an axis for the corresponding
        dimension, so len(arr.axes) == len(arr.shape)
    '''

    def __new__(cls, input_array, axes=None, dtype=None):
        '''Subclass initializer

        based on the subclassing tutorial
        http://docs.scipy.org/doc/numpy/user/basics.subclassing.html
        '''
        obj = np.asarray(input_array, dtype).view(cls)
        obj.axes = axes if isinstance(axes, list) else list(axes)
        obj._get_args = (None, None)
        return obj

    def __getitem__(self, idx):
        '''Method used for getting items or complicated slices

        stores the arguments to be able to slice axes accordingly

        If any slices contain complex numbers (at least one),
        they are translated to axis-based slices using
        :func:`translate_complex_slice`
        '''
        if isinstance(idx, (slice, tuple)):
            idx = (idx,) if isinstance(idx, slice) else idx
            idx = tuple((translate_complex_slice(maybe_slice, self.axes[i])
                         if isinstance(maybe_slice, slice) else maybe_slice)
                         for i, maybe_slice in enumerate(idx))
        self._get_args = ('item', idx)
        arr = super(SignalNdArray, self).__getitem__(idx)
        self._get_args = (None, None)
        return arr

    def __getslice__(self, start=None, end=None):
        '''Method used for simple slices

        stores the arguments to be able to slice axes accordingly
        Gets called only for 1D arrays

        If any slices contain complex numbers (at least one),
        they are translated to axis-based slices using
        :func:`translate_complex_slice`
        '''
        # axes slice
        temp_slice = slice(start, end)
        new_slice = translate_complex_slice(temp_slice, self.axes[0])
        if new_slice is not temp_slice:
            return self[new_slice]
        self._get_args = ('slice', (start, end))
        arr = super(SignalNdArray, self).__getslice__(start, end)
        self._get_args = (None, None)
        return arr

    def _finalize_axes(self, obj, axes):
        '''Finalize axes according to any shape changes'''
        if axes is None:
            return axes
        axes_shape = [len(axis) for axis in axes]
        if axes_shape != self.shape: # adjust axes shape
            axes = list(axes)   # copy
            get_name, args = getattr(obj, '_get_args', (None, None))
            if get_name == 'item':
                idx = args
                if not isinstance(idx, tuple):
                    idx = (idx,)
                idx_shape_diff = len(axes_shape) - len(idx)
                if idx_shape_diff != 0: # implicit slices
                    idx = idx + ((slice(None),) * idx_shape_diff)
                offset = 0  # index offset due to removal or insertion
                for i, axis_idx in enumerate(idx):
                    i += offset
                    if axis_idx == np.newaxis:
                        axes.insert(i, np.arange(1))
                        offset += 1
                    elif isinstance(axis_idx, Number):
                        axes.pop(i)
                        offset -= 1
                    else:
                        axes[i] = axes[i][axis_idx]
            elif get_name == 'slice': # gets called only for 1D arrays
                start, end = args
                axes[0] = axes[0][start:end]
            elif get_name == 'done':
                pass            # prevent finalizing again
            else:               # most likely shape reduction
                i = 0
                shape_len = len(self.shape)
                for axis in axes[:]:
                    if i >= shape_len or len(axis) != self.shape[i]:
                        axes.remove(axis)
                    else:
                        i += 1
        return axes

    def __array_finalize__(self, obj):
        '''Finalize array modification by modifying axes accordingly if needed

        Some heuristics are used, so it is possible that not all cases
        are taken into account (e.g. adding or taking more than 1 dimension)
        '''
        if obj is None:       # in __new__, axes will be assigned later
            return

        axes = getattr(obj, 'axes', None)
        self.axes = self._finalize_axes(obj, axes)
        self._get_args = ('done', None) # show that it is finalized


def translate_complex_slice(slice_, axis):
    '''Create a slice over the given axis corresponding to a bool mask
    if one of start or stop attributes of the slice object is complex

    The complex values are translated to real values through abs().
    The bool mask is then constructed as
    mask = (start <= axis) & (axis < stop)
    '''
    values = (slice_.start, slice_.stop)
    for value in values:        # check if one of them is complex
        if isinstance(value, complex):
            break
    else:
        return slice_
    start, stop = [(limit if value is None else abs(value)) for (value, limit)
                   in zip(values, (-np.inf, np.inf))]
    # <= used in accordance with Python slicing conventions
    axis_mask = (start <= axis) & (axis < stop)
    indices = np.nonzero(axis_mask)[0]
    if len(indices) == 0:
        raise ValueError("No data satisfy %s <= data < %s" % (start, stop))
    return slice(indices[0], indices[-1] + 1)
