import client as cc

if __name__ == "__main__":
	ccc = cc.CDBClient()

	record_number = 3373
	ll = []
	ll.append(dict(record_number = record_number, generic_signal_id = 294, DownSmpl=50))
	ll.append(dict(record_number = record_number, generic_signal_id = 296, DownSmpl=50))
	ll.append(dict(record_number = record_number, generic_signal_id = 292, DownSmpl=50))
	ll.append(dict(record_number = record_number, generic_signal_id = 290, DownSmpl=50))

	for sig in ll:
	    ww = ccc.get_signal(record_number=sig['record_number'], generic_signal_id=sig['generic_signal_id'])
	    print("Data read..")
	    fig = ww.plot(DownSmpl=sig['DownSmpl'])
	    # fig.title('this is the figure title', fontsize=12)
	    fig.canvas.set_window_title(ww.name)
	    
	    fig.show()
	    print("Figure plotted..")
