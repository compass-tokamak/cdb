__all__ = ['base']

# import all modules automatically
for m in __all__:
    __import__("pyCDB.libpostproc."+m, globals=globals())
