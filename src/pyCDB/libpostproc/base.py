# def auto_cdb_refs(func, in_refs, out_refs, *args, **kwargs):
#     '''Automatically creates cdb client
#     '''
#     pass


def donothing(in_refs, out_refs, **kwargs):

    from pyCDB.client import CDBClient as cdb_client

    # raise ZeroDivisionError('test Exception')

    print('donothing called for in_refs=%s, out_refs=%s, kwargs=%s' %
         (in_refs, out_refs, kwargs))
    cdb = cdb_client()
    # cdb_refs = tuple((cdb.get_signal_references(**ref) for ref in in_refs))
    # check signal should guarantee that references exist
    cdb_refs = [cdb.get_signal_references(**ref)[-1] for ref in in_refs]
    # cdb_refs = map(lambda ref: ref[0] if ref else (), cdb_refs)
    print('cdb_refs: %s' % cdb_refs)
    res = []
    for gs_id in out_refs:
        for ref in cdb_refs:
            if ref['data_file_id'] is not None:
                cdb.store_signal(generic_signal_id=gs_id, record_number=ref['record_number'],
                                 data_file_id=ref['data_file_id'], data_file_key=ref['data_file_key'],
                                 coefficient=10, time0=0)
                res.append(dict(cdb.get_signal_references(generic_signal_id=gs_id, record_number=ref['record_number'],
                                                          revision=-1)[0]))
                break
    print('res: %s' % str(res))
    return tuple(res), 'run ok'


def func_exec(in_refs, out_gs_ids, func=None, imports=(), sys_paths=()):
    '''Execute a function after getting CDB signals.

    The function
    '''

    # add sys paths
    import sys
    for p in sys_paths:
        sys.path.append(p)
    # import imports
    for m in imports:
        __import__(m)

    # get cdb signals
    from pyCDB.client import CDBClient as cdb_client
    cdb = cdb_client()
    cdb_sigs = [cdb.get_signal(**ref)[-1] for ref in in_refs]

    # call the function
    res = eval(func)(cdb_sigs)
    # TODO: what about the output???
    # store signals here (?)
    return res


# @interactive
def check_signal(in_refs, out_refs, **kwargs):
    '''Check if signal is available
    '''

    # from pyCDB.client import CDBClient as CDBClient
    from time import sleep
    from pyCDB.client import CDBClient

    set_timeout(5)

    print('check_signal called for in_refs=%s, out_refs=%s, kwargs=%s' %
         (in_refs, out_refs, kwargs))
    res = False
    cdb = CDBClient()
    while not res:
        # TODO: optimize - no need to check already checked signals
        # however, only signle signal should be input
        cdb_refs = [cdb.get_signal_references(**ref) for ref in in_refs]
        cdb_refs = map(lambda ref: dict(ref[0]) if ref else (), cdb_refs)
        res = all(map(bool, cdb_refs))
        if not res:
            sleep(1)
    # print('cdb_refs: %s' % cdb_refs)
    cancel_timeout()
    return tuple(cdb_refs), None


def resample_100us(in_refs, out_refs, **kwargs):
    from scipy.signal import resample
    from pyCDB.client import CDBClient
    import h5py
    import sys
    import traceback
    cdb = CDBClient()


    # works for a single signal only
    assert len(in_refs) == len(out_refs) == 1
    sig = cdb.get_signal(**in_refs[0])

    time_axis, data = sig.time_axis.data, sig.data
    target_dt = 0.1
    ndt = int(target_dt // sig.time_axis.ref.coefficient)
    resdata = resample(data, data.size // ndt)
    # restime = linspace(time_axis[ndt//2], time_axis[-ndt//2], resdata.size)
    restime = time_axis[ndt//2:-ndt//2:ndt]
    assert restime.size == resdata.size

    # use put_signal
    # TODO can make it smarter by reusing already stored time_axis
    gs_id = out_refs.pop()  # ['generic_signal_id']
    record_number = sig.ref.record_number
    kwargs = {}
    kwargs['time0'] = restime[0]
    kwargs['time_axis_coef'] = restime[1] - restime[0]
    kwargs['time_axis_offset'] = kwargs['time0']
    try:
        new_refs = (dict(cdb.put_signal(gs_id, record_number, resdata, **kwargs)), )
        msg = 'successfully resampled and stored'
    except Exception:
        new_refs = ()
        exc_type, exc_value, exc_traceback = sys.exc_info()
        lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
        msg = ''.join('!! ' + line for line in lines)

    return new_refs, msg


class TimeoutException(Exception):
    pass


def timeout_handler(signum, frame):
    raise TimeoutException()


def set_timeout(minutes=10):
    """Initialize a timeout counter

    :param minutes: number of minutes before timeout
    """
    import time
    import signal

    signal.signal(signal.SIGALRM, timeout_handler)
    signal.alarm(int(minutes*60))


def cancel_timeout():
    """Cancel the timeout"""
    set_timeout(0)

