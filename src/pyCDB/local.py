from pyCDB.client import CDBClient, json
import os
import datetime
import string
import h5py

AXIS_NAMES = ["time_axis", "axis1", "axis2", "axis3", "axis4", "axis5", "axis6"]

class MyJsonEncoder(json.JSONEncoder):
    """Tweeked JSON encoder that can deal with all our types.

    E.g. datetime
    """
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return str(obj)
        if isinstance(obj, type):
            import re
            m = re.match("<type '(.*)'>", str(obj))
            return m.group(1)
        else:
            return json.JSONEncoder.default(self, obj)

class Local(CDBClient):
    """CDB client variant that reads from local files.
    """
    # TODO: Implement
    def connect(self, *args, **kwargs):
        print("Local CDB client created. It doesn't connect do CDB.")

    def get_signal_references(self, json_file):
        # TODO: Implement
        pass

    def get_signal(self, json_file):
        # TODO: Implement
        pass

class Exporter(object):
    def __init__(self, *args, **kwargs):
        self.cdb = CDBClient(*args, **kwargs)

    def get_signal_object(self, str_id=None, **kwargs):
        """Nested directory with all relevant meta-data for a signal."""
        signals = self.cdb.get_signal_references(str_id, **kwargs)
        if not len(signals):
            raise Exception("No signal.")
        elif len(signals) > 1:
            raise Exception("Ambiguous signal.")
        
        signal = signals[0]
        generic_signal = self.cdb.get_generic_signal_references(generic_signal_id=signal["generic_signal_id"])[0]
        signal["generic_signal"] = generic_signal

        if signal["data_file_id"]:
            data_file = self.cdb.get_data_file_reference(data_file_id = signal["data_file_id"])
            signal["data_file"] = data_file

        signal["axes"] = {}
        for axis in AXIS_NAMES:
            gs_field = axis + "_id"
            rev_field = axis + "_revision"

            if generic_signal[gs_field] or signal.get(gs_field):
                axis_id = generic_signal[gs_field]
                if axis == "time_axis" and signal["time_axis_id"]:
                    axis_id = signal["time_axis_id"]

                # Recursively for all this axis
                signal["axes"][axis] = self.get_signal_object(
                    generic_signal_id=axis_id,
                    record_number=signal["record_number"],
                    revision=signal[rev_field]
                )
        signal["parameters"] = self.cdb.get_signal_parameters(signal_ref=signal)
        return signal

    def get_signal_json(self, str_id=None, **kwargs):
        """All relevant signal meta-data as JSON."""
        signal = self.get_signal_object(str_id, **kwargs)
        return json.dumps(signal, cls=MyJsonEncoder, indent=4)

    def export_data(self, str_id=None, filename=None, include_meta_data=False, **kwargs):
        '''Create HDF5 with data values without any meta-data.

        :param filename: if not present, it is built from 
        :param include_meta_data: if True, a dataset 'meta-data' containing JSON string is created.

        Keyword arguments:

        :param h5_file: HDF5 file object that will be used instead of a new file
            (must be open and won't be closed at the end.)
        :param signal: The signal itself (no need to read twice)

        Resulting HDF5 file has the following datasets:
        - values
        [- time]
        [- axis1]
        ...
        [- axis6]
        [- meta-data] if specified
        '''
        if 'signal' in kwargs:
            signal = kwargs['signal']
        else:
            signal = self.cdb.get_signal(str_id, **kwargs)

        if signal.data is not None:
            if 'h5_file' in kwargs:
                f = kwargs['h5_file']
            else:
                if not filename:
                    tr_table = string.maketrans(':/', "@_")
                    filename = str_id.translate(tr_table) + ".h5"
                f = h5py.File(filename, "w")

            f.create_dataset("values", data=signal.data, compression="gzip")
            if hasattr(signal,"time_axis"):
                if signal.time_axis.data is not None:
                    f.create_dataset("time", data=signal.time_axis.data, compression="gzip")
            for i in range(1, 7):
                if hasattr(signal, "axis%d" % i):
                    axis = getattr(signal, "axis%d" % i)
                    f.create_dataset("axis%d" % i, data=axis.data, compression="gzip")
            if include_meta_data:
                str_type = h5py.new_vlen(str)
                ds = f.create_dataset('meta-data', shape=(1,), dtype=str_type, compression="gzip")
                ds[:] = self.get_signal_json(str_id, **kwargs)
            if 'h5_file' not in kwargs:
                f.close()
        else:
            raise Exception("No signal!")

    def export_data_range(self, signal_names, shots, include_meta_data=False):
        '''Exports all signals of interest in their last revision.

        :param signal_names: iterable of signal_names (either "I_plasma" or "I_plasma_Rogowski_coil_RAW:MAGNETICS_RAW")
        :param shots: iterable of shots (list, range, ...)

        It sorts the data into directories according to shot numbers.
        Ignores non-existent signals but for every signal writes info about success.

        Example:

            from pyCDB import local
            exporter = local.Exporter()
            exporter.export_data_range(["I_plasma", "AXUV_A_1"], range(5999, 6000))
            
        '''
        for signal_name in signal_names:
            if len(signal_name.split("/")) == 2:
                signal_name, data_source_name = signal_name.split("/")
                data_source_id = self.cdb.get_data_source_id(data_source_name)
                gs_id = self.cdb.get_generic_signal_id(signal_name, data_source_id)
            else:
                gs = self.cdb.get_generic_signal_references(signal_name)[0]
                gs_id = gs["generic_signal_id"]
                data_source_id = gs["data_source_id"]
                data_source_name = self.cdb.get_data_source_references(data_source_id=data_source_id)[0]["name"]           

            for shot in shots:
                directory = str(shot)
                if not os.path.isdir(directory):
                    os.mkdir(directory)
                filename = os.path.join(directory, "%s_@_%s_@_%d.h5" %(signal_name, data_source_name, shot))
                try:
                    self.export_data(generic_signal_id=gs_id, record_number=shot, revision=-1, filename=filename, include_meta_data=include_meta_data)
                except:
                    print("Signal %s/%s:%d could not be written." % (signal_name, data_source_name, shot))





