from typing import List, Optional, Union
import logging
import time
import datetime
import json
import urllib.request
import urllib.parse
import urllib.error
import codecs
import csv
import numpy as np
from .pyCDBBase import pyCDBBase


class Sample:
    def __init__(self, value: float = 0.0, js_timestamp: float = 0.0):
        self.value = value
        self.js_timestamp = js_timestamp


class ContinuousSignalSamples:
    def __init__(self, values, time_axis, url_get_data: str, timed_out: bool):
        self.values = values
        self.time_axis = time_axis
        self.url_get_data = url_get_data
        self.timed_out = timed_out


class EpicsArchiverApplianceApi:
    def __init__(self, logger: logging.Logger = None, url = None):
        self._logger = logger
        if logger is None:
            self._logger = logging.getLogger(self.__class__.__name__)
        else:
            self._logger = logger

        if url is None:
            url = pyCDBBase.get_conf_value(
                "CDB_EPICS_ARCHIVER_APPLIANCE_URL", section="continuous_signal"
            )
        if url is None:
            raise RuntimeError(
                "EPICS Appliance Archiver URL not configured."
                " Either use env. CDB_EPICS_ARCHIVER_APPLIANCE_URL"
                " or supply it as an argument url."
            )
        self._url = url

    def get_data_url(
        self,
        format: str,
        pv_name: str,
        from_dt: Optional[datetime.datetime],
        to_dt: Optional[datetime.datetime],
    ) -> Optional[str]:
        if self._url is None:
            return None

        def encode(s):
            if isinstance(s, datetime.datetime):
                if s.tzinfo is None:
                    tzinfo = datetime.datetime.now(datetime.timezone.utc).astimezone().tzinfo
                    s = s.replace(tzinfo=tzinfo)
                s = s.isoformat()
            return urllib.parse.quote_plus(s)

        url = "{}/retrieval/data/getData.{}?pv={}".format(
            self._url,
            encode(format),
            encode(pv_name)
        )
        if from_dt is not None:
            url = url + "&from=" + encode(from_dt)
        if to_dt is not None:
            url = url + "&to=" + encode(to_dt)
        return url

    @staticmethod
    def parse_number(value: bytes) -> Union[float, str]:
        try:
            return float(value)
        except ValueError:
            return value.decode("utf-8")

    @classmethod
    def parse_csv_row(cls, row: List[str]) -> Union[Sample, None]:
        if len(row) == 0:
            return None

        fragments = (cls.parse_number(f) for f in row)
        sample = Sample()
        js_timestamp = next(fragments, 0)
        sample.value = next(fragments, 0)
        next(fragments, 0)
        next(fragments, 0)
        sample.js_timestamp = js_timestamp * 1000 + next(fragments, 0) / 1000000
        return sample

    def get_signal(
        self,
        pv_name,
        from_dt: Optional[datetime.datetime],
        to_dt: Optional[datetime.datetime],
        timeout: float = 5,
    ) -> Optional[ContinuousSignalSamples]:
        url = self.get_data_url(
            format="csv",
            pv_name=pv_name,
            from_dt=from_dt,
            to_dt=to_dt,
        )
        if url is None:
            self._logger.error("EPICS Archiver Appliance URL is not defined (PV %s)", pv_name)
            return None

        samples_timed_out = False
        samples_value = []
        samples_js_timestamp = []
        try:
            start_perf_counter = time.perf_counter()
            with urllib.request.urlopen(url, timeout=timeout) as response:
                csv_reader = csv.reader(codecs.iterdecode(response, "utf8"))
                for row in csv_reader:
                    sample = self.parse_csv_row(row)
                    if sample is not None:
                        samples_value.append(sample.value)
                        samples_js_timestamp.append(sample.js_timestamp)

                    if csv_reader.line_num % 20 == 0:
                        duration = time.perf_counter() - start_perf_counter
                        if duration >= timeout:
                            self._logger.warning(
                                "PV %s: Timed out after retrieving %d samples (URL %s).",
                                pv_name,
                                len(samples_value),
                                url,
                            )
                            samples_timed_out = True
                            break

            duration = time.perf_counter() - start_perf_counter
            self._logger.info(
                "PV %s: Retrieved %d samples (URL %s) in %.03f s",
                pv_name,
                len(samples_value),
                url,
                duration,
            )
            return ContinuousSignalSamples(
                values=np.asanyarray(samples_value),
                time_axis=np.asanyarray(samples_js_timestamp),
                url_get_data=url,
                timed_out=samples_timed_out,
            )

        except urllib.error.HTTPError as e:
            code = e.getcode()
            if code == 404:
                self._logger.error("PV %s not found", pv_name)
            else:
                self._logger.error(
                    "Retrieving PV %s failed with an HTTP error %d (URL %s)",
                    pv_name,
                    code,
                    url,
                )
            return None

        except Exception as e:
            self._logger.error("Retrieving PV %s failed (URL %s): %s", pv_name, url, str(e))

    def get_signal_meta(
        self,
        pv_name,
        timeout: float = 5
    ) -> Optional[dict]:
        now = datetime.datetime.now()
        url = self.get_data_url(
            format="json",
            pv_name=pv_name,
            from_dt=now,
            to_dt=now,
        )
        if url is None:
            self._logger.error("EPICS Archiver Appliance URL is not defined (PV %s)", pv_name)
            return None

        try:
            with urllib.request.urlopen(url, timeout=timeout) as response:
                data = json.loads(response.read().decode("utf8"))
                return data[0].get("meta", None)

        except urllib.error.HTTPError as e:
            code = e.getcode()
            if code == 404:
                self._logger.error("PV %s not found", pv_name)
            else:
                self._logger.error(
                    "Retrieving PV %s failed with an HTTP error %d (URL %s)",
                    pv_name,
                    code,
                    url,
                )
            return None

        except Exception as e:
            self._logger.error("Retrieving PV %s failed (URL %s): %s", pv_name, url, str(e))
