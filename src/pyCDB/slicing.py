'''Module for python slice manipulation and parsing'''
import re

try:  # hack to fix import error for python>= 3.10
    from collections import Iterable
except ImportError:
    from collections.abc import Iterable

# This functions are kept as they could be usefull
# def _string_to_slice(str_slice):
#     if str_slice.strip() == "...":
#         return Ellipsis
#     else:
#         return slice(*map(lambda x: long(x.strip())
#                if x.strip() else None, str_slice.split(':')))
#
#
# def _string_to_slices(str_slice):
#     return tuple(map(_string_to_slice_single, str_slice.split(",")))


def fill_ellipsis(_slices, ndim):
    """Replaces an Ellipsis object by slices to fit ndim"""

    if not isinstance(_slices, Iterable):
        slices = [_slices]
    else:
        slices = list(_slices)
    slices_len = len(slices)
    ell_count = slices.count(Ellipsis)
    if ell_count > 1:
        raise ValueError(
            "Only one Ellipsis object (...) can be used for array indexing.")
    elif ell_count == 1:
        if (slices_len - 1 > ndim):
            raise ValueError(
                "The lenght of non-ellipsis slices > target dimension.")
        ell_pos = slices.index(Ellipsis)
        for i in range(ndim - slices_len + 1):
            slices.insert(ell_pos, slice(None))
        slices.remove(Ellipsis)
    return tuple(slices)


def to_long(value, check=False):
    import sys
    value = float(value)
    if not value.is_integer() and check:
        raise ValueError(
            "Detected float, but had to be either complex or integer: {}".format(value))
    return int(value) if sys.version_info >= (3, 0) else long(value)


def str_to_slice(str_slice, ndim=None):
    """
    Converts slice string to slice tuple which can be used for indexing
    :param str_slice: String representing a slice
    :param ndim: dimension of the tuple of slices to be returned (basically
    Ellipsis is filled by a correct number of slice(None) if supplied)
    """
    if not re.match(r"^[0-9(.,:)j\-\ ]", str_slice):
        raise SyntaxError("invalid character in slice expression")
    slices = []
    for s in str_slice.split(","):
        if s == "...":
            slices.append(Ellipsis)
        elif ":" not in s:
            try:
                import sys
                value = to_long(s, check=True)
                slices.append(value)
            except ValueError:
                raise ValueError("Slice can be only integer, ellipsis (...), complex, or slice " +
                                "(something containing one or two semicolons), but " +
                                "something another was found. Please check the slice " +
                                "string: {}\n.".format(s))
        else:
            values = map(lambda x: complex(x.strip())
                         if x.strip() else None, s.split(':'))
            values = map(lambda x: to_long(x.real, check=True) if isinstance(
                x, complex) and not x.imag else x, values)
            slices.append(slice(*values))
    if slices.count(Ellipsis) > 1:
        raise ValueError(
            "Only one Ellipsis object (...) can be used for array indexing.")
    if ndim is not None and Ellipsis in slices:
        slices = fill_ellipsis(slices, ndim)
    return tuple(slices)


def translate_complex_slice(slice_, data):
    """Translates the complex slice into slice for indexing"""
    if not isinstance(slice_, slice):
        return slice_
    if isinstance(slice_.step, complex):
        raise ValueError(
            "Complex step is not supported by pyCDB. If needed,",
            " please use cdb_extras instead.")
    if not isinstance(slice_.start, complex) and not isinstance(slice_.stop, complex):
        return slice_
    if data is None:
        raise ValueError("Cannot perform complex slicing with invalid axes")
    if data.ndim > 1:
        raise ValueError("Cannot slice 2D axis data by complex slice.")
    from numpy import array, nonzero
    axis_data = data[...] if data.ndim <= 1 else data[time_slice, :]
    inf = float('inf')
    start = abs(slice_.start) if slice_.start is not None else -inf
    stop = abs(slice_.stop) if slice_.stop is not None else inf
    # <= used in accordance with Python slicing conventions
    axis_mask = (start <= axis_data) & (axis_data < stop)
    indices = axis_mask.nonzero()[0]
    if len(indices) == 0:
        raise ValueError("No data satisfy %s <= data < %s" % (start, stop))
    sl = slice(indices[0], indices[-1] + 1, slice_.step)
    return slice(indices[0], indices[-1] + 1, slice_.step)


def is_slice_complex(sl):
    """Determines whether the slice is complex"""
    if isinstance(sl, slice):
        values = (sl.start, sl.stop, sl.step)
        for v in values:
            if isinstance(v, complex):
                return True
    return False


def is_slice_reducing(s, ubound):
    """Determines whether the slice is reducing dimension of data"""
    if s is Ellipsis or s is None:
        return False
    if isinstance(s, (int, long)):
        return True
    import sys
    if sys.version_info >= (3, 0):
        return len(range(*s.indices(ubound))) <= 1
    else:
        return len(xrange(*s.indices(ubound))) <= 1
