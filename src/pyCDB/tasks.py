from celery import Celery, chord
import time

# running worker: celery -A tasks worker --loglevel=info

# celery = Celery('tasks', broker='amqp://', backend='amqp')
celery = Celery('tasks', backend='redis://localhost', broker='amqp://')

# os.putenv('BROKER_URL', 'amqp:/celery:celery@localhost:5672/celery')


@celery.task
def add5(x, y):
    time.sleep(0.1)
    return x + y


@celery.task
def add(x, y):
    return x + y


@celery.task
def mult10(x):
    return x * 10


def main():
    pass

if __name__ == '__main__':
    main()

    res = add5.delay(2, 3)
    print('ready : %s' % res.ready())
    time.sleep(0.1)
    print('state : %s' % res.state)
    print('result: %s' % res.get(timeout=11))

    # chain
    ch = (add.s(4, 5) | add.s(6))()
    print('4 + 5 + 6 = %i' % ch.get())

    # chord
    res = chord((add.s(5, 5), ), mult10.s())()
    print('chord = %s' % res.get())

    # callbacks
    res = add.apply_async((5, 5), link=mult10.s())
    print('res = %s' % res.get())

    r3 = mult10.subtask(())
    s2 = add.subtask((5, ), link=r3)
    r2 = mult10.subtask((), link=s2)
    s1 = add.subtask((1, 2), link=r2)
    res = s1.delay()
    print('((1 + 2) * 10 + 5) * 10 =? %i' % res.children[0].children[0].children[0].get())

    # r3 = mult10.subtask(())
    # s2 = add.subtask((), link=r3)
    # r1a = mult10.subtask((1, ), link=s2)
    # r1b = mult10.subtask((2, ), link=s2)
    # resa = r1a.delay()
    # resb = r1b.delay()


    # j1 = add.s(4, 5)
    # res = mult10.delay(j1)
    # print('res = %s' % res().get())

