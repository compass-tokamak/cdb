'''
Created on Jun 13, 2012

@author: jurban
'''

import sys
import os
import inspect
import os.path
import logging
import logging.handlers
import threading
import errno
if sys.version_info >= (2, 7):
    import json
else:
    import simplejson as json
import numbers


# Version-specific options
if sys.version_info >= (3, 2):
    from configparser import ConfigParser
    unicode = str
    long = int
    from string import ascii_letters as letters, digits
else:
    from ConfigParser import SafeConfigParser as ConfigParser
    from string import letters, digits


def isstringlike(obj):
    """Is object string-like?

    In Python 2, this means string or unicode.
    In Python 3, this means just string.
    """
    return isinstance(obj, (str, unicode))


def isintlike(obj):
    """Is object int-like?

    In Python 2, this means int or long.
    In Python 3, this means just int.
    """
    return isinstance(obj, (int, long))


for _collections in ('collections', 'ordereddict'):  # In Python 2.6 or Jython 2.5, ordereddict must be installed
    try:
        _collectionsModule = __import__(_collections)
        _OrderedDict = _collectionsModule.OrderedDict
    except:
        pass
try:
    _OrderedDict
except NameError:
    raise ImportError('No OrderedDict module found')


def addprop(inst, name, *methods):
    cls = type(inst)
    if not hasattr(cls, '__perinstance'):
        cls = type(cls.__name__, (cls,), {})
        cls.__perinstance = True
        inst.__class__ = cls
    setattr(cls, name, property(*methods))


def delprop(inst, name):
    cls = type(inst)
    if not hasattr(cls, '__perinstance'):
        cls = type(cls.__name__, (cls,), {})
        cls.__perinstance = True
        inst.__class__ = cls
    delattr(cls, name)


class OrderedDict(_OrderedDict):
    '''Customized ordered dictionary
    '''

    def __init__(self, *args, **kwargs):
        super(OrderedDict, self).__init__(*args, **kwargs)

    def __getattr__(self, item):
        if item in self:
            return self[item]
        else:
            raise(AttributeError("no attribute '{0}'".format(item)))
            # return getattr(super(OrderedDict, self), item)

    def __setattr__(self, item, value):
        if item in self:
            self.__setitem__(item, value)
        else:
            # the default behavior must be implemented
            self.__dict__[item] = value

    def __delattr__(self, item):
        if item in self:
            del self[item]
        else:
            raise(AttributeError("no attribute '{0}'".format(item)))

    def __dir__(self):
        d = []
        d += self.keys()
        d += dir(self.__class__)
        d += self.__dict__.keys()
        d.sort()
        return d

    def __repr__(self):
        '''Custom OrderedDict __str__ for IPython
        '''
        if self:
            res = []
            fmt = '%%%ds: %%s' % (max(map(len, self.keys())))
            for k, v in self.items():
                res.append(fmt % (k, v))
            res = '{' + '\n '.join(res) + '}'
        else:
            res = '{}'
        return res

    def diff(self, other, mode='norm'):
        '''Find differences to another OrderedDict, ignoring the keys order

        :param other: OrderedDict object to compare to
        '''
        from numpy.linalg import norm
        from numpy import ndarray

        self_keys = set(self.keys())
        other_keys = set(other.keys())
        common_keys = self_keys & other_keys
        res = OrderedDict()
        nnorm = 0
        if mode == 'norm':
            if 'diff_norm' in self or 'diff_max' in self:
                raise Exception('diff_norm and diff_max cannot be in the compared dict')
            res['diff_norm'] = 0 + 0j
            res['diff_max'] = 0
        for key in self.keys():
            # common keys
            if key in common_keys:
                if isinstance(self[key], dict):
                    if isinstance(other[key], dict):
                        # this allows for standard dict
                        res[key] = self.__class__.diff(self[key], other[key])
                        res['diff_norm'] += res[key]['diff_norm']
                        res['diff_max'] = max(res['diff_max'], res[key]['diff_norm'].real)
                        nnorm += 1
                    else:
                        if mode=='norm':
                            res[key] = 1j
                            res['diff_norm'] += res[key]
                        else:
                            res[key] = 'other["{0}"] is {1}, not {2}'.format(key,
                                                                          type(other[key]),
                                                                          type(self[key]))
                elif isinstance(self[key], (numbers.Number, ndarray)):
                    try:
                        diff = norm(self[key] - other[key])
                    except Exception as e:
                        if mode=='norm':
                            res[key] = 1j
                            res['diff_norm'] += res[key]
                        else:
                            res[key] = '{0}'.format(e)
                    else:
                        self_norm = norm(self[key])
                        if self_norm < 1e-12:
                            self_norm = 1
                        else:
                            res[key] = diff / self_norm
                        res['diff_max'] = max(res['diff_max'], res[key].real)
                        res['diff_norm'] += res[key]
                        nnorm += 1
                elif isinstance(self[key], (str, unicode)):
                    try:
                        if self[key] == other[key]:
                            res[key] = 0
                            nnorm += 1
                        else:
                            res[key] = 1j
                        res['diff_norm'] += res[key]
                    except Exception as e:
                        if mode=='norm':
                            res[key] = 1j
                            res['diff_norm'] += res[key]
                        else:
                            res[key] = '{0}'.format(e)
                else:
                    raise NotImplementedError('Not implemented for {0}'.format(type(self[key])))
            else:
                if mode=='norm':
                    res[key] = 1j
                    res['diff_norm'] += res[key]
                else:
                    res[key] = '{}'.format(e)

        if mode=='norm':
            if isinstance(res['diff_norm'], numbers.Complex):
                res['diff_norm'] = res['diff_norm'].real / nnorm + 1j * res['diff_norm'].imag
            else:
                res['diff_norm'] /= nnorm
        return res

    @classmethod
    def __save_dict_to_h5(cls, data, group):
        if not isinstance(data, (cls, dict)):
            raise ValueError('data must be a dict type')
        for k, v in data.items():
            if isinstance(v, (cls, dict)):
                cls.__save_dict_to_h5(v, group.create_group(str(k)))
            elif isinstance(v, (list, tuple)):
                raise ValueError('list and tuples are not supported (yet)')
            else:
                group.create_dataset(str(k), data=v)

    @classmethod
    def __get_dict_from_h5(cls, d, group):
        if not isinstance(d, (cls, dict)):
            raise ValueError('data must be a dict type')
        for k, v in group.items():
            if hasattr(v, 'items'):
                # this is a group
                d[k] = cls()
                cls.__get_dict_from_h5(d[k], v)
            else:
                d[k] = v[()]

    def save_h5(self, filename, mode='w'):
        """Serialize to an HDF5 file

        :param filename: output file name
        :param mode: file open mode, typically 'w' or 'a'
        """
        from h5py import File
        with File(filename, mode) as fh5:
            self.__save_dict_to_h5(self, fh5)

    @classmethod
    def load_h5(cls, filename):
        """Deserialize from an HDF5 file (created by save_h5)

        :param filename: input file name
        """
        from h5py import File
        self = cls()
        with File(filename, 'r') as fh5:
            self.__get_dict_from_h5(self, fh5)
        return self


class CDBException(Exception):
    '''A generic CDB exception class
    '''
    pass


# Import MySQL module (one of the options)
for _mysql in ('MySQLdb', 'pymysql', 'mysql.connector'):
    try:
        if _mysql == 'mysql.connector':
            _mysqlModule = __import__('mysql.connector.connection', fromlist=("MySQLConnection"))
            _mysqlErrorsModule = __import__('mysql.connector.errors', fromlist=("IntegrityError", "OperationalError"))
            _mysqlClass = _mysqlModule.MySQLConnection

            def _escape_string(value):
                """
                Escapes special characters as they are expected to by when MySQL
                receives them.
                As found in MySQL source mysys/charset.c

                Returns the value if not a string, or the escaped string.
                """
                res = value
                res = res.replace('\\', '\\\\')
                res = res.replace('\n', '\\n')
                res = res.replace('\r', '\\r')
                res = res.replace('\047', '\134\047')  # single quotes
                res = res.replace('\042', '\134\042')  # double quotes
                res = res.replace('\032', '\134\032')  # for Win32
                return res

        else:
            _mysqlModule = __import__('%s.connections' % _mysql, fromlist=("Connection"))
            _mysqlErrorsModule = __import__(_mysql)
            _mysqlClass = _mysqlModule.Connection
            try:
                _escape_string = __import__('%s' % _mysql, globals(), locals(),
                                           ['escape_string']).escape_string
            except AttributeError:
                try:
                    _escape_string = __import__("%s" % _mysql, globals(), locals(),
                                               ["converters"]).converters.escape_string
                except AttributeError:
                    if _mysql == "MySQLdb":
                        _escape_string = __import__("%s" % _mysql, globals(), locals(),
                                                    ["_mysql"])._mysql.escape_string
                    else:
                        raise
        break
    except ImportError:
        pass
try:
    _mysqlClass
except NameError:
    raise ImportError('No MySQL module found')

# Ensure that escape_string() returns the current Python string.
# E.g. escape_string() of MySQLdb returns bytes instead of str.
if isinstance(_escape_string("abc"), bytes):
    def escape_string(value):
        return _escape_string(value).decode("utf-8")
else:
    escape_string = _escape_string


def _init_logger(log_level=logging.ERROR, log_file=None, file_log_level=logging.ERROR, propagate=False):
    '''Initialize global pyCDB logger
    '''
    # create logger
    logger = logging.getLogger('pyCDB')
    logger.propagate = False
    while logger.handlers:
        l = logger.handlers[-1]
        l.close()
        logger.removeHandler(l)
    logger.setLevel(log_level)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(log_level)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)-12s: %(levelname)-8s %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    # create file handler which logs even debug messages
    if log_file:
        fh = logging.FileHandler(log_file)
        fh.setLevel(file_log_level)
        formatter = logging.Formatter('%(asctime)s - %(name)s:%(funcName)s:%(lineno)d: %(levelname)-8s %(message)s')
        fh.setFormatter(formatter)
        # add the handlers to the logger
        logger.addHandler(fh)


# initialize pyCDB logger
_init_logger()
_logger = logging.getLogger('pyCDB')


def _asscalar(val):
    if "numpy" in str(type(val)):
        import numpy
        try:
            return val.item()
        except AttributeError:
            return numpy.asscalar(val)
    else:
        return val


def cdb_base_check(func):
    '''Basic checking (database connection in particular) before calling CDB functions.

    To be used as decorator for client methods. Raises an exception if problem occurs.
    '''
    def wrapper(*args, **kwargs):
        if len(args) == 0 \
            or not isinstance(args[0], pyCDBBase):
            raise CDBException('Use with pyCDBBase only')
        if not args[0].check_connection(args[0].autoreconnect):
            raise CDBException('Database connection failed')
        return func(*args, **kwargs)
    return wrapper


class pyCDBBase(object):
    '''
    PyCDB base class for other pyCDB classes
    '''

    def get_log_level(self):
        '''Get logging level
        '''
        return self._log_level

    def set_log_level(self, value):
        '''Set logging level
        '''
        self._logger.handlers[0].setLevel(value)
        self._log_level = value

    log_level = property(get_log_level, set_log_level)

    def get_file_log_level(self):
        '''Get logging level
        '''
        return self._file_log_level

    def set_file_log_level(self, value):
        '''Set logging level
        '''
        try:
            self._logger.handlers[1].setLevel(value)
        except IndexError:
            self._logger.warning('file logging disabled (empty CDB_LOG_FILE)')
        self._file_log_level = value

    file_log_level = property(get_file_log_level, set_file_log_level)

    def __init__(self, host=None, user=None, passwd=None, db=None, port=3306, log_level=None, data_root=None):
        '''Contructor

        :param host: host address (IP or domain) with optional :port (e.g. 'localhost:3306')
        :param user: database user name
        :param passwd: database user password
        :param db: database name
        :param data_root: top-level directory(ies) of the data, :-separated
        '''

        # logger setup
        self._log_level = log_level or self.get_conf_value('CDB_LOG_LEVEL', 'logging') or 'ERROR'
        log_file = self.get_conf_value('CDB_LOG_FILE', 'logging')
        if log_file:
            log_file = os.path.expandvars(os.path.expanduser(log_file))
        self._file_log_level = self.get_conf_value('CDB_FILE_LOG_LEVEL', 'logging') or 'ERROR'
        # get pyCDB logger and reset handlers
        _init_logger(self._log_level, log_file, self._file_log_level)
        self._logger = logging.getLogger('pyCDB')
        # MySQL connection
        host = host or self.get_conf_value('CDB_HOST', required=True)
        host = host.split(':')
        if len(host) > 1:
            port = int(host[1])
        self._port = port
        self._host = host[0]
        self._user = user or self.get_conf_value('CDB_USER', required=True)
        self._passwd = passwd or self.get_conf_value('CDB_PASSWORD', required=True)
        self._db_name = db or self.get_conf_value('CDB_DB', required=True)
        data_root = data_root or self.get_conf_value('CDB_DATA_ROOT', required=True)

        self.data_root = [os.path.expandvars(os.path.expanduser(d)) for d in
                          data_root.split(os.path.pathsep)]
        # self.connect(host=self._host, user=self._user, passwd=self._passwd, db=self._db_name, port=self._port)
        # automatically reconnect in check_connection
        self.autoreconnect = True   # Not used!!!

        self._thread_local = threading.local()

    def __repr__(self):
        '''Representation that enables creation of a similar object.'''
        return "%s.%s(host=%s, db=%s, user=%s, passwd=%s, data_root=%s, log_level=%s)" % (
            self.__module__, self.__class__.__name__, repr(self._host),
            repr(self._db_name), repr(self._user), repr(self._passwd), repr(os.pathsep.join(self.data_root)),
            repr(self.log_level)
        )

    def error(self, exception):
        '''Log an error and throw the exception

        :param exception: can be an exception or string with the message.
        '''
        if isstringlike(exception):
            self._logger.error(exception)
            exception = CDBException(exception)
        else:
            self._logger.error(exception.__class__.__name__ + ": " + str(exception))
            self._logger.error(exception.__class__.__name__ + ": " + repr(exception))
        raise exception

    def _connect(self):
        try:
            self._logger.info("Connecting to database [%s]" % repr(self))
            db = _mysqlClass(host=self._host, user=self._user, passwd=self._passwd,
                                               db=self._db_name, port=self._port)
            if hasattr(db.autocommit, '__call__'):  # Autocommit (transactions)
                db.autocommit(True)
            else:
                db.autocommit = True
            return db
        except Exception:
            self.error("database connection error: %s" % sys.exc_info()[1])
    @property
    def db(self):
        '''Connection to the database.

        * each thread has its local connection
        * connection is automatically opened
        '''
        if not 'db' in dir(self._thread_local):
            self._thread_local.db = self._connect()
        return self._thread_local.db

    def check_connection(self, reconnect=True):
        '''
        Checks if connection is open, optionally (re)connects.
        '''
        try:
            self.db.ping(reconnect)
            res = True
        except Exception:  # (_mysqlModule.OperationalError,AttributeError):
            self._logger.error("Database connection error:\n%s", sys.exc_info()[1])
            res = False
        return res

    def query(self, sql, error_if_empty=False, warn_if_empty=False, error_if_multiple=False):
        '''Execute SQL query and returns a list of rows as dictionaries.

        :param sql: valid SQL query
        :param error_if_empty: raises exception if the result set is empty
        :param warn_if_empty: issues warning if the result set is empty but successfully returns it
        :param error_if_multiple: the result has to include at maximum 1 row
        '''
        self._logger.debug('SQL_query: %s' % sql)
        try:
            cursor = self.db.cursor()
        except:
            self.error("Could not obtain cursor")
        try:
            cursor.execute(sql)
            result = [self.row_as_dict(row, cursor.description) for row in cursor]
            cursor.close()
        except Exception as e:
            cursor.close()
            self.error('SQL query: {0}\nSQL error ({2}): {1}'.format(sql, str(e), type(e).__name__))
        if len(result) == 0:
            if error_if_empty:
                self.error("Empty result in %s()." % inspect.stack()[1][3])
            if warn_if_empty:
                self._logger.warning("Empty result in %s()." % inspect.stack()[1][3])
        if error_if_multiple and len(result) > 1:
                self.error("Too many results for single row in %s()" % inspect.stack()[1][3])
        return result

    def insert(self, table_name, fields, return_inserted_id=False, check_structure=True):
        '''Insert a new row to a table.

        :param table_name: name of table to insert to
        :param fields: dictionary of ( field name, value ) to insert
        :param return_inserted_id: if true, returns the id of the inserted row
        :param check_structure: if true, checks, whether fields agree with table layout,

        Values are processed to be DB-friendly by mysql_values (see).
        '''
        if check_structure:
            table_struct = self.get_table_struct(table_name)
            # TODO incomplete
            for f in fields:
                type_def = table_struct.get(f)
                if not type_def:
                    self.error('error inserting {0} into {1} - not such column'.format(f, table_name))
                if type_def['type'] == 'varchar':
                    if fields[f] and len(fields[f]) > type_def['length']:
                        self.error('error inserting {0} into {1} - value too long'.format(f, table_name))
        field_str = ",".join(("`{0}`".format(key) for key in fields.keys()))
        values_str = self.mysql_values(fields.values())
        sql = "INSERT INTO `{0}` ({1}) {2};".format(table_name, field_str, values_str)
        self._logger.debug('SQL_query: {0}'.format(sql))
        cursor = self.db.cursor()
        try:
            cursor.execute(sql)
            self.db.commit()
        except Exception:
            cursor.close()
            self.error('SQL query: {0}\nSQL error: {1}'.format(sql, str(sys.exc_info()[1])))
        if return_inserted_id:
            try:
                cursor.execute('SELECT LAST_INSERT_ID()')
                inserted_id = cursor.fetchone()[0]
                cursor.close()
                return inserted_id
            except Exception:
                cursor.close()
                self.error('Cannot get last inserted id\n'
                           'SQL error: ' + str(sys.exc_info()[1]))
        cursor.close()

    def update(self, table_name, id_field, id_value, fields):
        '''Update a row in a table.

        :param table_name: name of table to insert to
        :param id_field: name of the unique key field
        :param id_value: value of the unique key
        :param fields: dictionary of ( field name, value ) to insert

        '''
        if id_field in fields:
            raise CDBException("Cannot change unique key value.")
        if len(fields) == 0:
            self._logger.warning("Trying to update empty set of fields")
            return

        sql = "UPDATE `%s` SET " % table_name
        field_parts = [ " `%s` = %s" % (key, self.mysql_str(fields[key])) for key in fields ]
        sql += ", ".join(field_parts)
        sql += " WHERE `%s` = %s" % (id_field, self.mysql_str(id_value))
        self._logger.debug('SQL_query: %s' % sql)
        cursor = self.db.cursor()
        try:
            cursor.execute(sql)
            self.db.commit()
        except Exception:
            cursor.close()
            self.error('SQL query: {0}\nSQL error: {1}'.format(sql, str(sys.exc_info()[1])))
        cursor.close()

    def delete(self, table_name, fields):
        '''Delete a row from the table.

        :param table_name: name of table to insert to
        :param fields: dictionary of fields identifying the row

        Deletes at most one row: First, it checks for its existence in the table.
        If more than one row is found, raises exception.
        Returns True is something was deleted, False if not.
        '''
        if len(fields) == 0:
            raise CDBException("Cannot delete unspecified rows.")

        query_end = "FROM `%s` WHERE 1 " % table_name
        for key in fields:
            query_end += " AND `%s` = %s" % (key, self.mysql_str(fields[key]))
        select_sql = "SELECT 1 AS one " + query_end

        rows = self.query(select_sql)
        if len(rows) == 0:
            return False
        elif len(rows) == 1:
            delete_sql = "DELETE " + query_end
            self._logger.debug('SQL_query: %s' % delete_sql)
            cursor = self.db.cursor()
            try:
                cursor.execute(delete_sql)
                self.db.commit()
            except Exception:
                cursor.close()
                self.error('SQL query: {0}\nSQL error: {1}'.format(delete_sql, str(sys.exc_info()[1])))
            cursor.close()
            return True
        else:
            raise CDBException("Cannot delete more than 1 row at once.")

    def get_table_struct(self, table_name):
        """Get a table structure (as a dict)
        """
        struct = self.query('DESCRIBE %s' % table_name)
        res = OrderedDict()
        for col in struct:
            sql_type, sql_len = self.sql_to_python_type(col['Type'])
            res[col['Field']] = {'type': sql_type, 'length': sql_len}
        return res

    def close(self):
        '''Close all connections (SQL)
        '''

        self._logger.info("closing pyCDB client")
        try:
            if self.db.ping(False):
                self.db.close()
        except Exception:  # (_mysqlModule.OperationalError,AttributeError):
            self._logger.error("Database connection error:\n%s", sys.exc_info()[1])

    @classmethod
    def get_conf_file_search_paths(cls, conf_file_name='.CDBrc', platform=None):
        platform = platform or sys.platform
        cfg_files = [os.path.join(os.path.expanduser('~'), conf_file_name), conf_file_name]

        if os.getenv('CDB_PATH'):
            cfg_files.insert(0, os.path.expanduser(os.path.expandvars(os.path.join(
                os.getenv('CDB_PATH'), '..', conf_file_name))))
        else:
            _logger.info('CDB_PATH environment variable not set, cannot find default config file')

        if platform.startswith('win'):
            cfg_files.append(os.path.join(os.getenv('ProgramData') or os.path.join('C:', 'ProgramData'), 'CDB', conf_file_name))

        return cfg_files

    @classmethod
    def get_conf_value(cls, var_name, section='database', required=False):
        '''Get configuration value for a given variable name

        First looks for environment variables, then to ./.CDBrc, then to ~/.CDBrc

        :param var_name: one of CDB_HOST, CDB_USER, CDB_PASSWD, CDB_DB
        :param required: if True, exception is raised telling the user
            that he/she has to set the option.
        :rtype: variable value
        '''
        res = os.getenv(var_name)
        if not res:
            cfg = ConfigParser()  # See imports
            cfg_files = cls.get_conf_file_search_paths()
            if not cfg.read(cfg_files):
                _logger.warning('no .CDBrc found in ' + ", ".join(cfg_files))
            if cfg.has_option(section, var_name):
                res = cfg.get(section, var_name)
                os.environ[var_name] = res
            else:
                if required:
                    message = var_name + " must be set for CDB to work correctly."
                    _logger.error(message)
                    raise CDBException(message)
                else:
                    _logger.warning('config variable %s not found, leaving empty' % var_name)
        return res

    @classmethod
    def row_as_dict(cls, row, description):
        '''Convert SQL row tuple to python dict'''
        res = OrderedDict()
        for i, d in enumerate(description):
            res[d[0]] = row[i]
        return res

    @classmethod
    def sql_to_python_type(self, sql_type_str):
        """Transforms MySQL type definition to Python type and its length
        """
        type_parts = sql_type_str.strip().rsplit('(', 1)
        sql_type = type_parts[0].lower()
        if len(type_parts) == 1:
            sql_len = None
        else:
            type_parts[1] = type_parts[1].rstrip(')')
            if sql_type == 'enum':
                sql_len = max((len(x) - 2 for x in type_parts[1].split(',')))
            else:
                sql_len = int(type_parts[1])
        return sql_type, sql_len

    @classmethod
    def format_file_name(cls, collection_name, revision, file_format='HDF5', file_extension=None):
        '''Returns generic file name'''
        file_exts = {'HDF5': 'h5', 'NETCDF4': 'nc'}
        if file_extension is None:
            if file_format in file_exts:
                file_extension = file_exts[file_format]
            else:
                # self._logger.error('error: file_extension must be specified for file_format="%s"' % file_format)
                return None
        collection_name = str(collection_name).strip()
        file_format = str(file_format).strip()
        if not isintlike(revision) or revision < 1:
            # self._logger.error("revision must be integer > 0")
            return None
        res = "%s.%i.%s" % (collection_name, revision, file_extension)
        return res

    @classmethod
    def mysql_str(cls, val, float_format='%.17g', datetime_format='%Y-%m-%d %H:%M:%S', quote=True):
        '''Convert Python value into MySQL string and quote if necessary

        Use \x00 as the first character of MySQL builtin functions to avoid quoting

        Lists and tuples are converted into parentheses-wrapped lists (using recursion)
        to work with IN operator.
        '''
        typ = type(val)
        if 'numpy' in str(typ):
            import numpy
            val = val.squeeze()
            if val.size != 1 or val.ndim > 1:
                raise CDBException('Only 1D numpy objects are accepted')
            dtype = val.dtype
            if val.size > 1:
                # lists are simply iterated later
                typ = list
            elif numpy.issubdtype(dtype, numpy.core.numerictypes.integer):
                typ = int
                val = _asscalar(val)
            elif numpy.issubdtype(dtype, numpy.core.numerictypes.float):
                typ = float
                val = _asscalar(val)
            elif numpy.issubdtype(dtype, numpy.core.numerictypes.str_):
                typ = str
                val = _asscalar(val)
            else:
                raise CDBException('numpy dtype {} not supported'.format(dtype))
        if val is None:
            res = 'NULL'
        elif typ in (int, long, bool):
            res = '%i' % val
        elif typ is float:
            res = float_format % val
        elif hasattr(val, 'strftime') and callable(val.strftime):
            res = val.strftime(datetime_format)
            if quote:
                res = "'" + res + "'"
        elif typ in (list, tuple):
            values = [cls.mysql_str(v, float_format, datetime_format, quote) for v in val]
            res = "(" + ",".join(values) + ")"

        else:
            # string, unicode and the rest
            # if typ is unicode:
            #     res = val.encode("utf-8")
            # else:
            #     res = str(val)
            res = val

            if len(res) > 0 and res[0] == '\x00':
                res = res[1:]
            else:
                res = escape_string(res)
                if (quote and (res and res[0] != "'")) or (quote and not res):  # pymysql escape adds quotes itself
                    res = "'" + res + "'"
        return res

    @classmethod
    def mysql_values(cls, val_list, float_format='%.17g', datetime_format='%Y-%m-%d %H:%M:%S', quote=True):
        '''Construct whole VALUES MySQL INSERT construct for a list of Python values
        '''
        fn = lambda val: cls.mysql_str(val, float_format=float_format,
                                       datetime_format=datetime_format,
                                       quote=quote)
        xxx = ", ".join(map(fn, val_list))
        return 'VALUES (%s)' % ", ".join(map(fn, val_list))

    @classmethod
    def filter_sql_str(cls, sql_str, extra_chars='_', quiet=False):
        '''Filter a string intended to be stored to SQL

        :param sql_str: input string

        Keep only white listed characters: letters, digits, _.
        '''

        if not sql_str:
            # do nothing for emty string or None
            return sql_str, ''

        allowed_chars = letters + digits + extra_chars
        new_s = []
        repls_s = []
        for ch in sql_str:
            if ch in allowed_chars:
                new_s.append(ch)
            else:
                new_s.append('_')
                repls_s.append(ch)
                if not quiet:
                    raise CDBException('Invalid characters present in "%s", '
                                    'allowed characters are letters, digits and "%s"' % (sql_str, extra_chars))
        return ''.join(new_s), ''.join(repls_s)

    @classmethod
    def is_json(self, text):
        """Checks whether text has a valid JSON syntax

        :param text: text (string) to check
        """
        try:
            json.loads(text)
        except ValueError:
            return False
        else:
            return True

    @classmethod
    def makedirs(cls, name, mode=2047):
        """makedirs(path [, mode=2047])

        Super-mkdir; create a leaf directory and all intermediate ones.
        Works like mkdir, except that any intermediate path segment (not
        just the rightmost) will be created if it does not exist.  This is
        recursive.

        """
        head, tail = os.path.split(name)
        if not tail:
            head, tail = os.path.split(head)
        if head and tail and not os.path.exists(head):
            try:
                cls.makedirs(head, mode)
            except OSError as e:
                # be happy if someone already created the path
                if e.errno != errno.EEXIST:
                    raise CDBException(str(e))
            if tail == os.curdir:           # xxx/newdir/. exists if xxx/newdir exists
                return
        try:
            if not os.path.isdir(name):
                os.makedirs(name)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise CDBException(str(e))
        try:
            os.chmod(name, mode)  # mkdir mode seems buggy for 0o3777
        except OSError as e:
            _logger.debug('could not chmod %s' % name)
