'''EPICS Archiver Appliance client

EPICS Archiver Appliance is an application which monitors EPICS PVs and captures
their changes. Then, the user can select time range and the application
will return all value changes captures within the range. This enables viewing
history of continuous data exposed through EPICS.
'''

from typing import Optional, Union
import datetime
from pyCDB.pyCDBBase import pyCDBBase, json
from .epics_archiver_appliance_utils import decode_signal_strid
from .epics_archiver_appliance_api import EpicsArchiverApplianceApi
from . import client


class CDBContinuousSignal(client.CDBSignal):
    continuous_data_url = ""
    '''URL which provided the data.'''
    continuous_data_timed_out = False
    '''True if retrieving stopped prematurely due to timeout.'''

    @property
    def ndim(self):
        return 1

    @ndim.setter
    def ndim(self, _):
        raise AttributeError('ndim cannot be modified')

    @property
    def url(self):
        return ""


class EpicsArchiverApplianceClient(pyCDBBase):

    @property
    def api(self) -> EpicsArchiverApplianceApi:
        if not hasattr(self, "_api"):
            self._api = EpicsArchiverApplianceApi(self._logger)
        return self._api

    def get_continuous_signal(
        self,
        str_id: str,
        from_dt: Optional[datetime.datetime] = None,
        to_dt: Optional[datetime.datetime] = None,
        timeout: float = 5,
    ) -> CDBContinuousSignal:
        '''Retrieve continuous signal from the EPICS Archiver Appliance.

        :param str_id: Signal STR_ID. Allowed ID type is EAA (e.g. `EAA:vacuum:1g1:pressure`).
        :param from_dt: Time of the first retrieved sample. Use `None` for the first recorded sample.
        :param to_dt: Time of the last retrieved sample. Use `None` for the last recoded sample.
        :param timeout: Request timeout in seconds.
        :return: Retrieved signal.
        '''

        # Validate parameters
        if from_dt is not None and to_dt is not None and from_dt > to_dt:
            self.error("From datetime is after to date time.")

        try:
            decoded_str_id = decode_signal_strid(str_id)
        except Exception as e:
            self.error("Invalid str_id: {}".format(e))

        if decoded_str_id["id_type"] != "EAA":
            self.error(
                "ID type {} is not a continuous signal.".format(
                    decoded_str_id["id_type"]
                )
            )
        pv_name = decoded_str_id["pv_name"]

        # Retrieve content
        continuous_signal = self.api.get_signal(
            pv_name=pv_name,
            from_dt=from_dt,
            to_dt=to_dt,
            timeout=timeout,
        )
        if continuous_signal is None:
            self.error("signal not found")

        signal = CDBContinuousSignal(data=continuous_signal.values, name=str_id)
        signal.time_axis = CDBContinuousSignal(data=continuous_signal.time_axis)
        signal.continuous_data_url = continuous_signal.url_get_data
        signal.continuous_data_timed_out = continuous_signal.timed_out
        return signal

    def get_continuous_signal_parameters(
        self,
        str_id: str,
        parse_json=True,
        timeout: float = 5,
    ) -> Union[dict, str]:
        '''Retrieve continuous signal parameters from the EPICS Archiver Appliance.

        :param str_id: Signal STR_ID. Allowed ID type is EAA (e.g. `EAA:vacuum:1g1:pressure`).
        :param parse_json: `True` to retrieved dict, `False` to retrieve raw string.
        :param timeout: Request timeout in seconds.
        :return: Retrieved parameters.
            If `parse_json` is `True`, the dicitionary contains following items
            * `name`: Signal name.
            * `EGU`: Eng. units of the signal (if available).
            * `PREC`: Display precision of values.
        '''
        try:
            decoded_str_id = decode_signal_strid(str_id)
        except Exception as e:
            self.error("Invalid str_id: {}".format(e))

        if decoded_str_id["id_type"] != "EAA":
            self.error(
                "ID type {} is not a continuous signal.".format(
                    decoded_str_id["id_type"]
                )
            )
        pv_name = decoded_str_id["pv_name"]

        # Retrieve content
        parameters = self.api.get_signal_meta(
            pv_name=pv_name,
            timeout=timeout,
        )
        if parameters is None:
            self.error("signal not found")
        if parse_json:
            return parameters
        else:
            return json.dumps(parameters)
