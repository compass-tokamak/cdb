'''
Created on Jun 13, 2012

@author: jurban
'''

#1. TAGy pridelene danemu vystrelu
#- fce get_tags(shot_no), ktera vrati seznam tagu pridelenych k
#vystrelu. Ty se ziskaji v tabulce logbook.logbook (stejny mysql server
#na kterem je cdb) sloupecek taglist
#- fce has_tag(shot_no,TAG), ktera vrati bool toho, jestli je dany tag
#zadany nebo ne
#
#2. Parametry daneho vystrelu
#- fce get_shot_params(shot_no), ktera vrati asociativni pole parametru
#a jejich hodnot.Hodnoty parametru jsou k vystrelum asociovane v
#tabulce logbook.shot_param_values, popisy parametru i s jednotkami
#apod jsou v tabulce logbook.shot_params
#- fce get_shot_param_value(shot_no,param_name) ktera vrati hodnotu
#daneho parametru
#
#3. Linearni profily daneho vystrelu
#fce get_shot_linear_profiles(shot_no), hodnoty linearnich profil (1D
#poli) jsou v tabulce logbook.linear_profile_values zapsane jako
#jednotlive body s udajem o casu a hodnote. Udaje o linearnich
#profilech jsou opet v logbook.shot_params, od normalnich parametru
#jsou odlisene priznakem type.
#- fce get_shot_linear_profile_value(shot_no,param_name) ktera vrati 1D
#pole hodnot daneho linearniho profilu
#
#4. Komentare
#fce get_shot_comments(shot_no). Komentare jsou v tabulce logbook.comments
#
#5. fce get_shot_info(shot_no),ktera vrati datovou strukturu obsahujici
#informace o vystrelu, ktere jsou v tabulce logbook.logbook, jako je
#nazev vystrelu (sname), k jake patri kampani (cid), kdy probehl
#(cr_time) apod.


from . import pyCDBBase
import numpy as np
from .pyCDBBase import cdb_base_check


class logbook(pyCDBBase.pyCDBBase):
    '''
    logbook client
    '''

    def __init__(self, host=None, user=None, passwd=None, db=None, port=3306, log_level=None):
        '''
        Constructor
        '''
        host = host or self.get_conf_value('CDB_LOGBOOK_HOST', 'logbook')
        host = host.split(':')
        if len(host) > 1:
            port = int(host[1])
        host = host[0]
        user = user or self.get_conf_value('CDB_LOGBOOK_USER', 'logbook')
        passwd = passwd or self.get_conf_value('CDB_LOGBOOK_PASSWORD', 'logbook')
        db = db or self.get_conf_value('CDB_LOGBOOK_DB', 'logbook')
        super(logbook, self).__init__(host=host, user=user, passwd=passwd, db=db, port=port)

    @cdb_base_check
    def get_tags(self, shot_number):
        '''
        Get shot tags
        '''

        cursor = self.db.cursor()
        cursor.execute("SELECT taglist FROM logbook WHERE shot_no=%i;" % shot_number)
        row = cursor.fetchone()
        cursor.close()
        return tuple(row[0].split()) if row else ()

    def has_tag(self, shot_number, tag):
        '''
        Check for a tag
        '''
        return tag in self.get_tags(shot_number)

    @cdb_base_check
    def shot_param_name(self, param_id):
        '''
        Translate param_id to param name
        '''

        cursor = self.db.cursor()
        cursor.execute('SELECT name FROM shot_params WHERE pid=%i' % param_id)
        row = cursor.fetchone()
        cursor.close()
        return row[0] if row else None

    @cdb_base_check
    def get_shot_params(self, shot_number):
        '''
        Get all shot parameters (in a dictionary)
        '''

        cursor = self.db.cursor()
        sqlquery = "SELECT spid, value FROM shot_params_values WHERE shot_no=%i" % shot_number
        cursor.execute(sqlquery)
        shot_params = dict(((self.shot_param_name(pid), val) for pid, val in cursor))
        cursor.close()
        return shot_params

    def get_shot_param_value(self, shot_number, param_name):
        '''
        Get the parameter's value
        '''
        shot_params = self.get_shot_params(shot_number)
        return shot_params[param_name] if param_name in shot_params else None

    @cdb_base_check
    def get_shot_linear_profiles(self, shot_number):
        '''
        Get all shot parameters (in a dictionary)
        '''

        cursor = self.db.cursor()
        sqlquery = "SELECT pid, start_time, value FROM linear_profile_values WHERE shot_no=%i " % shot_number
        sqlquery += "ORDER BY pid,start_time;"
        cursor.execute(sqlquery)
        res = []
        for row in cursor:
            if res == [] or res[-1][0] != row[0]:
                res.append([row[0], [[row[1], row[2]]]])
            else:
                res[-1][1].append([row[1], row[2]])
        profs = dict(((self.shot_param_name(pid), np.array(val)) for pid, val in res))
        cursor.close()
        return profs

    def get_shot_linear_profile_value(self, shot_number, prof_name):
        '''
        Get a profile as an array
        '''
        profs = self.get_shot_linear_profiles(shot_number)
        return profs[prof_name] if prof_name in profs else None

    @cdb_base_check
    def get_shot_comments(self, shot_number):
        '''
        Get shot comments
        '''

        cursor = self.db.cursor()
        sqlquery = "SELECT comment FROM comments WHERE shot_no=%i " % shot_number
        cursor.execute(sqlquery)
        row = cursor.fetchone()
        cursor.close()
        return row[0] if row else None

    @cdb_base_check
    def get_shot_info(self, shot_number):
        '''
        Get shot info
        '''

        cursor = self.db.cursor()
        sqlquery = "SELECT sname, cr_time, resp_uid, vice_uid, cid, wiki_fact FROM logbook WHERE shot_no=%i " % shot_number
        cursor.execute(sqlquery)
        if cursor:
            res = self.row_as_dict(cursor.fetchone(), cursor.description) if cursor else None
            res['resp_user'] = self.uid_name(res['resp_uid'])
            res['vice_user'] = self.uid_name(res['vice_uid'])
            res['campaign'] = self.campaign_name(res['cid'])
        else:
            res = None
        cursor.close()
        return res

    @cdb_base_check
    def uid_name(self, uid):
        '''
        Translates uid to (LDAP) name
        '''

        cursor = self.db.cursor()
        sqlquery = "SELECT ldap_name FROM users WHERE uid=%i " % uid
        cursor.execute(sqlquery)
        row = cursor.fetchone()
        cursor.close()
        return row[0] if row else None

    @cdb_base_check
    def campaign_name(self, cid):
        '''
        Translates cid to the campaign name
        '''

        cursor = self.db.cursor()
        sqlquery = "SELECT name FROM campains WHERE cid=%i " % cid
        cursor.execute(sqlquery)
        row = cursor.fetchone()
        cursor.close()
        return row[0] if row else None

if __name__ == '__main__':
    from pprint import pprint
    log = logbook()
    tags = log.get_tags(2)
    print('tags: %s' % str(tags))
    print('yes: %i, no: %i' % (log.has_tag(2, tags[0]), log.has_tag(2, 'XYZ')))
    shot_params = log.get_shot_params(3128)
    print('shot params: %s' % str(log.get_shot_params(3128)))
    print('B_T(2) = %g' % log.get_shot_param_value(2, 'B_T'))
    lin_profs = log.get_shot_linear_profiles(3128)
    pprint(lin_profs)
    pprint(log.get_shot_linear_profile_value(3128, 'MFPS_profile'))
    print(log.get_shot_comments(3128))
