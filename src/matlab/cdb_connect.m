function [ client ] = cdb_connect()
%cdb_connect Connect to CDB via JyCDB

try
    % Try to find already existing instance
    client = cz.cas.ipp.compass.jycdb.CDBClient.getInstance();

catch
    disp('Loading JyCDB libraries...')


    % Set up necessary paths
    if isunix
        % Ubuntu
        [s, r] = unix('dirname `which jython`');
        % avoid constructing path from the shell warnings:
        sr = splitlines(r);
        r = sr{end-1};
        if s == 0 && ~isempty(r)
            jypath = strtrim(r);
        else
            jypath = '/sw/jython/2.5.2';
        end
        cdbpath = getenv('CDB_PATH');
        if isempty(cdbpath)
            if exist('../JyCDB/dist/JyCDB.jar', 'file')
                % local development
                cdbpath = '..';
            else
                cdbpath = '/sw/CDB/latest/src';
            end
        end
        javaaddpath([jypath, '/jython.jar']);
        javaaddpath([cdbpath, '/JyCDB/dist/JyCDB.jar']);

    else
        % Windows
        javaaddpath('C:\jython2.5.2\jython.jar');
        javaaddpath('C:\jython2.5.2\compass_libs\pyCDB\src\JyCDB\dist\JyCDB.jar');
    end

    %
    try
        client = cz.cas.ipp.compass.jycdb.CDBClient.getInstance();
    catch
        error('CDB:JyCDBError', 'JyCDB library could not be initialized.')
    end
end

