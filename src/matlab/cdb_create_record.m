function [ record_number ] = cdb_create_record(record_type, client)
%cdb_create_record Create a new record and specify the record type
%   

if nargin < 2
    client = cdb_connect();
end

record_number = client.createRecord(record_type);

end

