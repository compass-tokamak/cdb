function data = cdb_read_file_data(file_name, file_type, data_key)

if data_key(1) ~= '/'
    % initial / required
    data_key = ['/', data_key];
end

if strcmpi(file_type, 'HDF5')
    try
        % new function, supports compound types
        data = h5read(file_name, data_key);
    catch
	    try
			% when the hdf5 data structure is not recognized
			data = load(file_name,data_key(2:end));
			try
			    % attempt to simplify output structure according to key
				data = data.(data_key(2:end));				
			end
			% disp(['Loaded directly hdf5 structure from database : ' data_key(2:end)]);
		catch		    
			% older Matlab
			try
				% hdf5info will fail for unsupported types
				cmd = sprintf('env -u LD_LIBRARY_PATH h5dump -H -d %s %s | grep DATATYPE | head -1 | sed s/DATATYPE// | sed s/[\\"\\ \\t]*//g', data_key, file_name);
				[st, dtype] = unix(cmd);
				% deblank, find end of lines
				dtype = deblank(dtype);
				i = find(dtype < 30);
				if ~isempty(i)
					dtype = dtype(i(end)+1:end);
				end
				% old Matlab cannot read H5T_COMPOUND
				if strcmp(dtype, 'H5T_COMPOUND') || isempty(strfind(dtype, 'H5T_')) % || strfind(dtype, 'H5T_') ~= 1
					error('CDB:SignalError', 'HDF5 features not supported, use newer Matlab (2012)')
				end
				data = hdf5read(file_name, data_key);
			catch
				error('CDB:SignalError', 'HDF5 features not supported, use newer Matlab (2012)')
			end
		end
    end
else
    error('CDB:SignalError', ['unknown fyle type: ', file_type]);
    data = [];
end

end
