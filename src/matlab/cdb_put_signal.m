function [sig_ref] = cdb_put_signal(gs_alias_or_id, record_number, data, varargin)
%cdb_put_signal Write CDB signal to HDF5 and database
%   cdb_signal = cdb_put_signal(gs_alias_or_id, record_number, data, ...)
%   writes signal data into CDB, including HDF5 file.
%
%   gs_alias_or_id: generic signal string id
%   record_number: record number
%   data: signal data (numeric)
%   optional arguments: 'argument_name', default value, validation_function
%   'variant', '', @ischar
%   'time0', 0, @isnumeric
%   'data_coef', [], @isnumeric
%   'data_offset', [], @isnumeric
%   'data_coef_V2unit', 1, @isnumeric
%   'note', '', @ischar
%   'no_debug', false, @islogical
%   'time_axis_id', [], @isnumeric)
%   'time_axis_data', [], @isnumeric
%   'time_axis_coef', [], @isnumeric
%   'time_axis_offset', [], @isnumeric
%   'time_axis_note', '', @ischar
%   'axis1_data', [], @isnumeric
%   'axis1_coef', [], @isnumeric
%   'axis1_offset', [], @isnumeric
%   'axis1_note', '', @ischar
%   'axis2_data', [], @isnumeric
%   'axis2_coef', [], @isnumeric
%   'axis2_offset', [], @isnumeric
%   'axis2_note', '', @ischar
%   'axis3_data', [], @isnumeric
%   'axis3_coef', [], @isnumeric
%   'axis3_offset', [], @isnumeric
%   'axis3_note', '', @ischar
%   'axis4_data', [], @isnumeric
%   'axis4_coef', [], @isnumeric
%   'axis4_offset', [], @isnumeric
%   'axis4_note', '', @ischar
%   'axis5_data', [], @isnumeric
%   'axis5_coef', [], @isnumeric
%   'axis5_offset', [], @isnumeric
%   'axis5_note', '', @ischar
%   'axis6_data', [], @isnumeric
%   'axis6_coef', [], @isnumeric
%   'axis6_offset', [], @isnumeric
%   'axis6_note', '', @ischar


client = cdb_connect();
glb_axes_names = {'time_axis','axis1','axis2','axis3','axis4','axis5','axis6'};

p = inputParser();

p.addParameter('time0', 0, @isnumeric);
p.addParameter('variant', '', @ischar);
p.addParameter('data_coef', [], @isnumeric);
p.addParameter('data_offset', [], @isnumeric);
p.addParameter('data_coef_V2unit', 1, @isnumeric);
p.addParameter('note', '', @ischar);
p.addParameter('no_debug', false, @islogical);
p.addParameter('time_axis_id', [], @isnumeric);
p.addParameter('time_axis_data', [], @isnumeric);
p.addParameter('time_axis_coef', [], @isnumeric);
p.addParameter('time_axis_offset', [], @isnumeric);
p.addParameter('time_axis_note', '', @ischar);
p.addParameter('axis1_data', [], @isnumeric);
p.addParameter('axis1_coef', [], @isnumeric);
p.addParameter('axis1_offset', [], @isnumeric);
p.addParameter('axis1_note', '', @ischar);
p.addParameter('axis2_data', [], @isnumeric);
p.addParameter('axis2_coef', [], @isnumeric);
p.addParameter('axis2_offset', [], @isnumeric);
p.addParameter('axis2_note', '', @ischar);
p.addParameter('axis3_data', [], @isnumeric);
p.addParameter('axis3_coef', [], @isnumeric);
p.addParameter('axis3_offset', [], @isnumeric);
p.addParameter('axis3_note', '', @ischar);
p.addParameter('axis4_data', [], @isnumeric);
p.addParameter('axis4_coef', [], @isnumeric);
p.addParameter('axis4_offset', [], @isnumeric);
p.addParameter('axis4_note', '', @ischar);
p.addParameter('axis5_data', [], @isnumeric);
p.addParameter('axis5_coef', [], @isnumeric);
p.addParameter('axis5_offset', [], @isnumeric);
p.addParameter('axis5_note', '', @ischar);
p.addParameter('axis6_data', [], @isnumeric);
p.addParameter('axis6_coef', [], @isnumeric);
p.addParameter('axis6_offset', [], @isnumeric);
p.addParameter('axis6_note', '', @ischar);
p.addParameter('gs_parameters','', @ischar);
p.addParameter('daq_parameters','', @ischar);

p.parse(varargin{:});

gs = client.getGenericSignal(gs_alias_or_id);
if isempty(gs)
    error('CDB:InputError', 'generic signal not found')
end
if ~client.recordExists(record_number)
    error('CDB:InputError', 'record number does not exist')
end
if ~strcmp(gs.getSignalType(), 'FILE')
    error('CDB:InputError', 'This method is designed to store only file-type signals!')
end
if ~isnumeric(data) || isempty(data)
    error('CDB:InputError', 'You have to supply data. If you want to update other information than the data itself, please use function "update_signal"')
end

% TODO: check data consistency

% after all checks were passed, we can start creating a dataset
collection = gs.getName(); % Set collection name = signal name -- this is an arbitrary choice
data_file = client.newDataFile(collection, record_number, gs.getDataSourceId());
data_file_id = data_file.getId();
paramList = cz.cas.ipp.compass.jycdb.util.ParameterList();

for i = 1:length(glb_axes_names)
    ax_name = glb_axes_names{i};

    ax_gs_id = gs.getAxisId(i-1);

    % can change id of time axis
    if i == 1 && ~isempty(p.Results.time_axis_id) && p.Results.time_axis_id > 0
        ax_gs_id = p.Results.time_axis_id;
        paramList.put('time_axis_id', int64(ax_gs_id))
    end

    if ax_gs_id > 0

        ax_gs = client.getGenericSignal(ax_gs_id);

        if strcmp(ax_gs.getSignalType(), 'FILE') && ~isempty(p.Results.([ax_name, '_data']))
            % store FILE axis using cdb_put_signal
            ax_ref = cdb_put_signal(ax_gs.getId(), record_number, p.Results.([ax_name, '_data']), ...
                'data_coef', p.Results.([ax_name, '_coef']), ...
                'data_offset', p.Results.([ax_name, '_offset']), ...
                'note', p.Results.([ax_name, '_note']), ...
                'variant', p.Results.variant);

        elseif strcmp(ax_gs.getSignalType(), 'LINEAR') && ...
            (~isempty(p.Results.([ax_name, '_offset'])) || ~isempty(p.Results.([ax_name, '_coef'])))
            % store LINEAR axis
            ax_paramList = cz.cas.ipp.compass.jycdb.util.ParameterList();
            ax_paramList.put('generic_signal_id', int64(ax_gs.getId()));
            ax_paramList.put('record_number', int64(record_number));
            ax_paramList.put('offset', double(p.Results.([ax_name, '_offset'])));
            ax_paramList.put('coefficient', double(p.Results.([ax_name, '_coef'])));
            ax_paramList.put('note', p.Results.([ax_name, '_note']));
            % store axes always using the signal variant
            ax_paramList.put('variant', p.Results.variant);
            signal = client.storeSignal(ax_paramList);
            if isempty(signal)
                error('CDB:StoreError', ['could not store ', ax_name])
            end
            ax_ref = javaMap2Struct(signal.asMap());
        else
            % get the last stored ax
            % use the same variant
            ax_str_id = sprintf('%d:%d:%s', ax_gs.getId(), record_number, p.Results.variant);
            signal = client.getSignal(ax_str_id);
            if isempty(signal)
                error('CDB:InputError', [ax_name, ' signal (', ax_str_id, ') does not exist, you must supply the data']);
            end
            java_sig_ref = signal.getSignalReference();
            ax_ref = javaMap2Struct(java_sig_ref.asMap());

        end
        paramList.put([ax_name, '_revision'], int32(ax_ref.revision));
        paramList.put([ax_name, '_variant'], p.Results.variant);
    end
end

paramList.put('generic_signal_id', int64(gs.getId()));
paramList.put('record_number', int64(record_number));
paramList.put('data_file_key', collection);
paramList.put('data_file_id', int64(data_file_id));
paramList.put('time0', double(p.Results.time0));
paramList.put('coefficient', double(p.Results.data_coef));
paramList.put('offset', double(p.Results.data_offset));
paramList.put('coefficient_V2unit', double(p.Results.data_coef_V2unit));
paramList.put('note', p.Results.note);
paramList.put('variant', p.Results.variant);
paramList.put('gs_parameters',p.Results.gs_parameters);
paramList.put('daq_parameters',p.Results.daq_parameters);

% now, continue with storing the data itself
datasetname = collection; % Choose datasetname = collection -- this is again an arbitrary choice

cdb_write_file_data(char(data_file.getFullPath()), 'HDF5', datasetname, data, true);
client.setFileReady(data_file.getId());
signal = client.storeSignal(paramList);
if isempty(signal)
    error('CDB:StoreError', 'could not store signal')
end
sig_ref = javaMap2Struct(signal.asMap());
% fprintf(1, sprintf('%s successfully stored as revision %i', ));

