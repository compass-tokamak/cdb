function [cdb_signal] = cdb_get_signal(str_id, raw_data)
%cdb_get_signal Get CDB signal including the data

if nargin < 2
    raw_data = false;
end
client = cdb_connect();

glb_axes_names = {'time_axis','axis1','axis2','axis3','axis4','axis5','axis6'};
% glb_axes_names = {'time_axis','axis1','axis2','axis3'};

if ~ischar(str_id)
    error('CDB:InputError', 'str_id must be a string')
end
cdb_signal = cdb_get_signal_fullref(str_id, client);
sig_calib = client.getSignalCalibration(str_id);
sig_calib = javaMap2Struct(sig_calib.asMap());
cdb_signal.units = sig_calib.units;
% get the data
cdb_signal.data = cdb_get_signal_data(cdb_signal, nan, nan, nan, sig_calib, raw_data);
% read axes
iax = 0;
for ax_name = glb_axes_names
    ax_id_str = char(strcat(ax_name,'_id'));
    if isfield(cdb_signal.gs_ref, ax_id_str) && ~isempty(cdb_signal.gs_ref.(ax_id_str)) && ...
            isnumeric(cdb_signal.gs_ref.(ax_id_str)) && cdb_signal.gs_ref.(ax_id_str)
        iax = iax + 1;
        if strcmp(ax_id_str, 'time_axis_id') && ~isempty(cdb_signal.ref.(ax_id_str))
            axid = cdb_signal.ref.(ax_id_str);
        else
            axid = cdb_signal.gs_ref.(ax_id_str);
        end
        ax_str_id = sprintf('CDB:%i:%i:%s:%i', axid, cdb_signal.ref.record_number, ...
            cdb_signal.ref.(char(strcat(ax_name,'_variant'))), cdb_signal.ref.(char(strcat(ax_name,'_revision'))));
        ax_signal = cdb_get_signal_fullref(ax_str_id, client);
        ax_sig_calib = client.getSignalCalibration(ax_str_id);
        ax_sig_calib = javaMap2Struct(ax_sig_calib.asMap());
        ax_signal.units = ax_sig_calib.units;
        if strcmpi(ax_name,'time_axis')
            ax_data = cdb_get_signal_data(ax_signal,size(cdb_signal.data, iax),cdb_signal.ref.time0,nan,ax_sig_calib, raw_data);
        else
            ax_data = cdb_get_signal_data(ax_signal,size(cdb_signal.data, iax),nan,nan,ax_sig_calib, raw_data);
        end
        ax_signal.data = ax_data;
        cdb_signal.(char(ax_name)) = ax_signal;
    end
end

end
