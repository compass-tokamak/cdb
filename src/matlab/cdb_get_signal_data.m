function [signal_data] = cdb_get_signal_data(cdb_signal, n_samples, x0, x1, sig_calib, raw_data)
%cdb_get_signal_data Get CDB signal data by the signal reference

glb_axes_names = {'time_axis','axis1','axis2','axis3','axis4','axis5','axis6'};

if nargin<6
    raw_data = false;
end
if nargin<5
    sig_calib.coefficient = 1;
    sig_calib.offset = 0;
end
if nargin<4
    x1 = nan;
end
if nargin <3
    x0 = nan;
end
if nargin <2
    n_samples = nan;
end

if ~isempty(cdb_signal.file_ref)
    % FILE signal
    if ~cdb_signal.file_ref.file_ready
        error('CDB:SignalError', 'file not ready for reading')
    end
    if isempty(cdb_signal.file_ref.full_path)
        error('Data file not found in current CDB_DATA_ROOT directories')
    end
    signal_data = cdb_read_file_data(cdb_signal.file_ref.full_path, cdb_signal.file_ref.data_format, cdb_signal.ref.data_file_key);
    if ~raw_data
        signal_data = cdb_permute_data(signal_data);
    end
    if sig_calib.coefficient ~= 1 || sig_calib.offset ~= 0
        if isstruct(signal_data)
            for s = fieldnames(signal_data)'
                signal_data.(char(s)) = (signal_data.(char(s)) + sig_calib.offset) * sig_calib.coefficient;
            end
        else
            signal_data = (signal_data + sig_calib.offset) * sig_calib.coefficient;
        end
    end
elseif strcmpi(cdb_signal.gs_ref.signal_type,'LINEAR')
    % LINEAR signal
    ax_ref = struct([]);
    is_time_axis = false;
    for ax_name = glb_axes_names
        if isfield(cdb_signal,char(strcat(ax_name,'_ref')))
            if isempty(ax_ref)
                ax_ref = cdb_signal.(char(strcat(ax_name,'_ref')));
                if strcmpi(ax_name,'time_axis')
                    is_time_axis = true;
                end
            else
                error('CDB:SignalError', 'cannot generate LINEAR signal with >1 axes');
            end
        end
    end

    if ~isempty(ax_ref)
        % linear signal with axes
        ax_sig_ref = cdb_get_full_signal_ref(ax_ref.gs_ref.generic_signal_id,cdb_signal.record_number,ax_ref.ref.revision);
        axis_data = cdb_get_signal_data(ax_sig_ref,n_samples,x0,x1);
        if isempty(axis_data)
            error('CDB:SignalError', 'no axis data could be retrived')
        end

        if is_time_axis
            signal_data = ax_ref.ref.offset + ax_ref.ref.coefficient * (axis_data - ax_ref.ref.time0);
        else
            signal_data = ax_ref.ref.offset + ax_ref.ref.coefficient * axis_data;
        end

    else
        % linear signal without axis
        % produce data
        if ~isnan(n_samples)
            if isnan(x0)
                x0 = cdb_signal.ref.offset;
            end
            signal_data = reshape(x0 + (0:n_samples-1)*cdb_signal.ref.coefficient,[],1);
        elseif ~isnan(x1)
            if isnan(x0)
                x0 = cdb_signal.ref.offset;
            end
            signal_data = reshape(x0:cdb_signal.ref.coefficient:x1,[],1);
        else
            error('CDB:SignalError', 'cannot create linear signal')
        end
    end
else
    error('CDB:SignalError', 'unknown signal type')
end

end
