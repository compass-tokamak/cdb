function sig_ref = cdb_get_full_signal_ref(name_or_id,record_number,revision)

if nargin<2 
    record_number = -1; 
end
if nargin<3 
    revision = -1; 
end

glb_axes_names = {'time_axis','axis1','axis2','axis3','axis4','axis5','axis6'};

sig_ref = cdb_get_signal_ref_mex(name_or_id,record_number,revision);
if sig_ref.isset
    for ax_name=glb_axes_names
        ax_id = sig_ref.generic_signal_ref.(char(strcat(ax_name,'_id')));
        if ax_id>0
            sig_ref.(char(strcat(ax_name,'_ref'))) = cdb_get_signal_ref_mex(ax_id,sig_ref.record_number,...
                                                              sig_ref.data_signal_ref.(char(strcat(ax_name,'_revision'))));
        end		                                   
    end
end
