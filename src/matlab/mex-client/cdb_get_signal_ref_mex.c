#include "mex.h"
#include "cyCDB_api.h"
/* #include "Python.h" */
#include <string.h>

void mexFunction( int nlhs, mxArray *plhs[], 
		  int nrhs, const mxArray*prhs[] )
     
{ 

  /* Check for proper number of arguments */
  if (nrhs < 1) { 
    mexErrMsgTxt("At least one input arguments required."); 
  } else if (nlhs > 1) {
    mexErrMsgTxt("Too many output arguments."); 
  } 

  /* Initialize Python */
  /* mexPrintf("Py_IsInitialized: %i\n", Py_IsInitialized()); */
  if (~Py_IsInitialized()) {
    /* mexPrintf("Py_Initialize()\n"); */
    Py_Initialize();
    int argc = 0;
    char* argv="";
  /* Py_SetProgramName(argv[0]); */
  /* Segmentation fault if PySys_SetArgv not called! */
    PySys_SetArgv(argc,&argv); 
  /* cyCDB initialization */
    /* mexPrintf("initcyCDB\n"); */
    initcyCDB();
    /* mexPrintf("importcyCDB\n"); */
    import_cyCDB();
  /* cyCDB working now */
    /* mexPrintf("cyCDB initialized :)\n"); */
    /* mexPrintf("Py_IsInitialized: %i\n",Py_IsInitialized()); */
  }

  /* parse input parameters */
  int revision = -1;
  if (nrhs >= 3) revision = (int) mxGetScalar(prhs[2]);
  long record_number = -1;
  if (nrhs >= 2) record_number = (long) mxGetScalar(prhs[1]);
  char* signal_name;
  long signal_id=0;
  mwSize buflen;
  int status;
  if (mxIsChar(prhs[0])) {
    buflen = mxGetN(prhs[0])*sizeof(mxChar)+1;
    signal_name = mxMalloc(buflen);
    status = mxGetString(prhs[0], signal_name, buflen); 
  } else {
    signal_id = (long)  mxGetScalar(prhs[0]);
  }

  struct signal_data_ref_t signal_ref;
  if (signal_id) {
    signal_ref = cdb_get_signal_data_ref_by_id(signal_id,record_number,revision);
  } else {
    signal_ref = cdb_get_signal_data_ref(signal_name,record_number,revision);
    mxFree(signal_name);
  }
  
  mwSize dims[1] = {1};
  int nfields;
  if (signal_ref.isset == 0) {
    plhs[0] = mxCreateStructArray(0, NULL, 0, NULL);
  } else {
    const char* data_file_ref_field_names[] = {"collection_name","data_format","record_number",
                                               "file_name","data_file_id","data_source_id","revision"};
    mxArray* data_file_ref = mxCreateStructArray(1, dims, 7, data_file_ref_field_names);
    if (strcmp(signal_ref.generic_signal_ref.signal_type,"FILE") == 0) {
      mxSetField(data_file_ref,0,"collection_name",mxCreateString(signal_ref.data_file_ref.collection_name));
      mxSetField(data_file_ref,0,"data_format",mxCreateString(signal_ref.data_file_ref.data_format));
      mxSetField(data_file_ref,0,"record_number",mxCreateDoubleScalar(signal_ref.data_file_ref.record_number));
      mxSetField(data_file_ref,0,"file_name",mxCreateString(signal_ref.data_file_ref.file_name));
      mxSetField(data_file_ref,0,"data_file_id",mxCreateDoubleScalar(signal_ref.data_file_ref.data_file_id));
      mxSetField(data_file_ref,0,"data_source_id",mxCreateDoubleScalar(signal_ref.data_file_ref.data_source_id));
      mxSetField(data_file_ref,0,"revision",mxCreateDoubleScalar(signal_ref.data_file_ref.revision));
    }
   
    const char* generic_signal_ref_field_names[] = {
      "generic_signal_name","alias","description","last_record_number","axis3_id","time_axis_id","axis2_id","axis1_id","first_record_number","units","generic_signal_id","data_source_id","signal_type","axis4_id","axis5_id","axis6_id"};
    mxArray* generic_signal_ref = mxCreateStructArray(1, dims, 16, generic_signal_ref_field_names);
    mxSetField(generic_signal_ref,0,"generic_signal_name",mxCreateString(signal_ref.generic_signal_ref.generic_signal_name));
    mxSetField(generic_signal_ref,0,"alias",mxCreateString(signal_ref.generic_signal_ref.alias));
    mxSetField(generic_signal_ref,0,"description",mxCreateString(signal_ref.generic_signal_ref.description));
    mxSetField(generic_signal_ref,0,"last_record_number",mxCreateDoubleScalar(signal_ref.generic_signal_ref.last_record_number));
    mxSetField(generic_signal_ref,0,"axis3_id",mxCreateDoubleScalar(signal_ref.generic_signal_ref.axis3_id));
    mxSetField(generic_signal_ref,0,"time_axis_id",mxCreateDoubleScalar(signal_ref.generic_signal_ref.time_axis_id));
    mxSetField(generic_signal_ref,0,"axis2_id",mxCreateDoubleScalar(signal_ref.generic_signal_ref.axis2_id));
    mxSetField(generic_signal_ref,0,"axis1_id",mxCreateDoubleScalar(signal_ref.generic_signal_ref.axis1_id));
    mxSetField(generic_signal_ref,0,"first_record_number",mxCreateDoubleScalar(signal_ref.generic_signal_ref.first_record_number));
    mxSetField(generic_signal_ref,0,"units",mxCreateString(signal_ref.generic_signal_ref.units));
    mxSetField(generic_signal_ref,0,"generic_signal_id",mxCreateDoubleScalar(signal_ref.generic_signal_ref.generic_signal_id));
    mxSetField(generic_signal_ref,0,"data_source_id",mxCreateDoubleScalar(signal_ref.generic_signal_ref.data_source_id));
    mxSetField(generic_signal_ref,0,"signal_type",mxCreateString(signal_ref.generic_signal_ref.signal_type));
    mxSetField(generic_signal_ref,0,"axis4_id",mxCreateDoubleScalar(signal_ref.generic_signal_ref.axis4_id));
    mxSetField(generic_signal_ref,0,"axis5_id",mxCreateDoubleScalar(signal_ref.generic_signal_ref.axis5_id));
    mxSetField(generic_signal_ref,0,"axis6_id",mxCreateDoubleScalar(signal_ref.generic_signal_ref.axis6_id));

    const char* data_signal_ref_field_names[] = {
  "coefficient","axis3_revision","board_id","revision","computer_id","timestamp","axis2_revision","time0","axis1_revision","note","channel_id","record_number","offset","generic_signal_id","data_file_key","time_axis_revision","data_file_id"};
    mxArray* data_signal_ref = mxCreateStructArray(1, dims, 17, data_signal_ref_field_names);
    mxSetField(data_signal_ref,0,"coefficient",mxCreateDoubleScalar(signal_ref.data_signal_ref.coefficient));
    mxSetField(data_signal_ref,0,"axis3_revision",mxCreateDoubleScalar(signal_ref.data_signal_ref.axis3_revision));
    mxSetField(data_signal_ref,0,"board_id",mxCreateDoubleScalar(signal_ref.data_signal_ref.board_id));
    mxSetField(data_signal_ref,0,"revision",mxCreateDoubleScalar(signal_ref.data_signal_ref.revision));
    mxSetField(data_signal_ref,0,"computer_id",mxCreateDoubleScalar(signal_ref.data_signal_ref.computer_id));
    mxSetField(data_signal_ref,0,"timestamp",mxCreateDoubleScalar(signal_ref.data_signal_ref.timestamp));
    mxSetField(data_signal_ref,0,"axis2_revision",mxCreateDoubleScalar(signal_ref.data_signal_ref.axis2_revision));
    mxSetField(data_signal_ref,0,"time0",mxCreateDoubleScalar(signal_ref.data_signal_ref.time0));
    mxSetField(data_signal_ref,0,"axis1_revision",mxCreateDoubleScalar(signal_ref.data_signal_ref.axis1_revision));
    mxSetField(data_signal_ref,0,"note",mxCreateString(signal_ref.data_signal_ref.note));
    mxSetField(data_signal_ref,0,"channel_id",mxCreateDoubleScalar(signal_ref.data_signal_ref.channel_id));
    mxSetField(data_signal_ref,0,"record_number",mxCreateDoubleScalar(signal_ref.data_signal_ref.record_number));
    mxSetField(data_signal_ref,0,"offset",mxCreateDoubleScalar(signal_ref.data_signal_ref.offset));
    mxSetField(data_signal_ref,0,"generic_signal_id",mxCreateDoubleScalar(signal_ref.data_signal_ref.generic_signal_id));
    mxSetField(data_signal_ref,0,"data_file_key",mxCreateString(signal_ref.data_signal_ref.data_file_key));
    mxSetField(data_signal_ref,0,"time_axis_revision",mxCreateDoubleScalar(signal_ref.data_signal_ref.time_axis_revision));
    mxSetField(data_signal_ref,0,"data_file_id",mxCreateDoubleScalar(signal_ref.data_signal_ref.data_file_id));


    nfields = 6;
    const char *field_names[] = {"isset", "record_number", "full_file_path",
               "data_file_ref", "data_signal_ref","generic_signal_ref"};
    plhs[0] = mxCreateStructArray(1, dims, nfields, field_names);
    mxSetField(plhs[0],0,"isset",mxCreateLogicalScalar(signal_ref.isset));
    mxArray *field_value;
    field_value = mxCreateDoubleMatrix(1,1,mxREAL);
    *mxGetPr(field_value) = signal_ref.record_number;
    mxSetField(plhs[0],0,"record_number",field_value); 
    mxSetField(plhs[0],0,"full_file_path",mxCreateString(signal_ref.full_file_path));
    mxSetField(plhs[0],0,"data_file_ref",data_file_ref);
    mxSetField(plhs[0],0,"data_signal_ref",data_signal_ref);
    mxSetField(plhs[0],0,"generic_signal_ref",generic_signal_ref);
  }
}
