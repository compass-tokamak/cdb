#include "mex.h"
#include "cyCDB_api.h"

void mexFunction( int nlhs, mxArray *plhs[], 
		  int nrhs, const mxArray*prhs[] )
     
{ 

  /* Check for proper number of arguments */
  if (nrhs < 1) { 
    mexErrMsgTxt("At least one input arguments required."); 
  } else if (nlhs > 1) {
    mexErrMsgTxt("Too many output arguments."); 
  } 

  /* Initialize Python */
  /* mexPrintf("Py_IsInitialized: %i\n", Py_IsInitialized()); */
  if (~Py_IsInitialized()) {
    /* mexPrintf("Py_Initialize()\n"); */
    Py_Initialize();
    int argc = 0;
    char* argv="";
  /* Py_SetProgramName(argv[0]); */
  /* Segmentation fault if PySys_SetArgv not called! */
    PySys_SetArgv(argc,&argv); 
  /* cyCDB initialization */
    /* mexPrintf("initcyCDB\n"); */
    initcyCDB();
    /* mexPrintf("importcyCDB\n"); */
    import_cyCDB();
  /* cyCDB working now */
    /* mexPrintf("cyCDB initialized :)\n"); */
    /* mexPrintf("Py_IsInitialized: %i\n",Py_IsInitialized()); */
  }

  /* parse input parameters */
  char* data_source_name;
  long data_source_id=0;
  mwSize buflen;
  int status;
  
  if (mxIsNumeric(prhs[0])) {
    data_source_id = (long) mxGetScalar(prhs[0]);
    /* SEG FAULT !!!
     * mexPrintf('generic_signal_id: %ld\n',generic_signal_id); */
  } else {
    buflen = mxGetN(prhs[0])*sizeof(mxChar)+1;
    data_source_name = mxMalloc(buflen);
    status = mxGetString(prhs[0], data_source_name, buflen); 
    /*mexPrintf("The input string is:  %s\n", generic_signal_name);*/
  }

  struct data_sources_t data_source;
  struct data_sources_t* data_sources;
  int n_elements = 1;
  if (data_source_id) {
    data_source = cdb_get_data_source_ref_by_id(data_source_id);
    if (data_source.data_source_id == 0) n_elements = 0;
  } else {
    data_sources = cdb_data_source_references(data_source_name,&n_elements);
    if (n_elements>0) data_source = data_sources[0];
    mxFree(data_source_name);
  }
  
  if (n_elements==0) {  
    plhs[0] = mxCreateStructArray(0, NULL, 0, NULL);
  } else {
    mwSize dims[1] = {1};
    const char *field_names[] = {"subdirectory","name","data_source_id","description"};
    plhs[0] = mxCreateStructArray(1, dims, 4, field_names);
    mxSetField(plhs[0],0,"subdirectory",mxCreateString(data_source.subdirectory));
    mxSetField(plhs[0],0,"name",mxCreateString(data_source.name));
    mxSetField(plhs[0],0,"data_source_id",mxCreateDoubleScalar(data_source.data_source_id));
    mxSetField(plhs[0],0,"description",mxCreateString(data_source.description));
  }
}
