#include "mex.h"
#include "cyCDB_api.h"
/* #include "Python.h" */

void mexFunction( int nlhs, mxArray *plhs[], 
		  int nrhs, const mxArray*prhs[] )
     
{ 
  double *yp; 
  double *t,*y; 
  mwSize m,n; 

  /* Check for proper number of arguments */
  if (nrhs < 2) { 
    mexErrMsgTxt("At least two input arguments required."); 
  } else if (nlhs > 1) {
    mexErrMsgTxt("Too many output arguments."); 
  } 

  /* Initialize Python */
  if (~Py_IsInitialized()) {
    Py_Initialize();
    int argc = 0;
    char* argv="";
    /* Segmentation fault if PySys_SetArgv not called! */
    PySys_SetArgv(argc,&argv); 
    /* cyCDB initialization */
    initcyCDB();
    import_cyCDB();
  }

  /* parse input parameters */
  int revision = -1;
  if (nrhs >= 3) revision = (int) mxGetScalar(prhs[2]);
  long record_number;
  record_number = (long) mxGetScalar(prhs[1]);
  char* signal_name;
  signal_name = (char*) mxArrayToString(prhs[0]);

  mexPrintf("Input signal name: %s\n",signal_name);
  mexPrintf("Record number: %i, revision: %i\n",record_number,revision);

/*
  struct signal_data_ref_t cdb_signal_ref;
  cdb_signal_ref = cdb_get_signal_data_ref(signal_name,record_number,revision);
*/

  struct cdb_signal_t cdb_signal;
  int raw = 0;
  cdb_signal = cdb_get_signal(signal_name,record_number,revision,raw);

  mwSize dims[1] = {1};

/*
  const char* data_file_ref_field_names[] = {
"collection_name","data_format","record_number","file_name","data_file_id","data_source_id","revision"};
  mxArray* data_file_ref = mxCreateStructArray(1, dims, 7, data_file_ref_field_names);
  mxSetField(data_file_ref,0,"collection_name",mxCreateString(cdb_signal_ref.data_file_ref.collection_name));
  mxSetField(data_file_ref,0,"data_format",mxCreateString(cdb_signal_ref.data_file_ref.data_format));
  mxSetField(data_file_ref,0,"record_number",mxCreateDoubleScalar(cdb_signal_ref.data_file_ref.record_number));
  mxSetField(data_file_ref,0,"file_name",mxCreateString(cdb_signal_ref.data_file_ref.file_name));
  mxSetField(data_file_ref,0,"data_file_id",mxCreateDoubleScalar(cdb_signal_ref.data_file_ref.data_file_id));
  mxSetField(data_file_ref,0,"data_source_id",mxCreateDoubleScalar(cdb_signal_ref.data_file_ref.data_source_id));
  mxSetField(data_file_ref,0,"revision",mxCreateDoubleScalar(cdb_signal_ref.data_file_ref.revision));
 
  const char* generic_signal_ref_field_names[] = {
"generic_signal_name","description","last_record_number","axis3_id","time_axis_id","axis2_id","axis1_id","first_record_number","units","generic_signal_id","data_source_id","signal_type"};
  mxArray* generic_signal_ref = mxCreateStructArray(1, dims, 12, generic_signal_ref_field_names);
  mxSetField(generic_signal_ref,0,"generic_signal_name",mxCreateString(cdb_signal_ref.generic_signal_ref.generic_signal_name));
  mxSetField(generic_signal_ref,0,"description",mxCreateString(cdb_signal_ref.generic_signal_ref.description));
  mxSetField(generic_signal_ref,0,"last_record_number",mxCreateDoubleScalar(cdb_signal_ref.generic_signal_ref.last_record_number));
  mxSetField(generic_signal_ref,0,"axis3_id",mxCreateDoubleScalar(cdb_signal_ref.generic_signal_ref.axis3_id));
  mxSetField(generic_signal_ref,0,"time_axis_id",mxCreateDoubleScalar(cdb_signal_ref.generic_signal_ref.time_axis_id));
  mxSetField(generic_signal_ref,0,"axis2_id",mxCreateDoubleScalar(cdb_signal_ref.generic_signal_ref.axis2_id));
  mxSetField(generic_signal_ref,0,"axis1_id",mxCreateDoubleScalar(cdb_signal_ref.generic_signal_ref.axis1_id));
  mxSetField(generic_signal_ref,0,"first_record_number",mxCreateDoubleScalar(cdb_signal_ref.generic_signal_ref.first_record_number));
  mxSetField(generic_signal_ref,0,"units",mxCreateString(cdb_signal_ref.generic_signal_ref.units));
  mxSetField(generic_signal_ref,0,"generic_signal_id",mxCreateDoubleScalar(cdb_signal_ref.generic_signal_ref.generic_signal_id));
  mxSetField(generic_signal_ref,0,"data_source_id",mxCreateDoubleScalar(cdb_signal_ref.generic_signal_ref.data_source_id));
  mxSetField(generic_signal_ref,0,"signal_type",mxCreateString(cdb_signal_ref.generic_signal_ref.signal_type));

  const char* data_signal_ref_field_names[] = {
"coefficient","axis3_revision","board_id","revision","computer_id","timestamp","axis2_revision","time0","axis1_revision","note","channel_id","record_number","offset","generic_signal_id","data_file_key","time_axis_revision","data_file_id"};
  mxArray* data_signal_ref = mxCreateStructArray(1, dims, 17, data_signal_ref_field_names);
  mxSetField(data_signal_ref,0,"coefficient",mxCreateDoubleScalar(cdb_signal_ref.data_signal_ref.coefficient));
  mxSetField(data_signal_ref,0,"axis3_revision",mxCreateDoubleScalar(cdb_signal_ref.data_signal_ref.axis3_revision));
  mxSetField(data_signal_ref,0,"board_id",mxCreateDoubleScalar(cdb_signal_ref.data_signal_ref.board_id));
  mxSetField(data_signal_ref,0,"revision",mxCreateDoubleScalar(cdb_signal_ref.data_signal_ref.revision));
  mxSetField(data_signal_ref,0,"computer_id",mxCreateDoubleScalar(cdb_signal_ref.data_signal_ref.computer_id));
  mxSetField(data_signal_ref,0,"timestamp",mxCreateDoubleScalar(cdb_signal_ref.data_signal_ref.timestamp));
  mxSetField(data_signal_ref,0,"axis2_revision",mxCreateDoubleScalar(cdb_signal_ref.data_signal_ref.axis2_revision));
  mxSetField(data_signal_ref,0,"time0",mxCreateDoubleScalar(cdb_signal_ref.data_signal_ref.time0));
  mxSetField(data_signal_ref,0,"axis1_revision",mxCreateDoubleScalar(cdb_signal_ref.data_signal_ref.axis1_revision));
  mxSetField(data_signal_ref,0,"note",mxCreateString(cdb_signal_ref.data_signal_ref.note));
  mxSetField(data_signal_ref,0,"channel_id",mxCreateDoubleScalar(cdb_signal_ref.data_signal_ref.channel_id));
  mxSetField(data_signal_ref,0,"record_number",mxCreateDoubleScalar(cdb_signal_ref.data_signal_ref.record_number));
  mxSetField(data_signal_ref,0,"offset",mxCreateDoubleScalar(cdb_signal_ref.data_signal_ref.offset));
  mxSetField(data_signal_ref,0,"generic_signal_id",mxCreateDoubleScalar(cdb_signal_ref.data_signal_ref.generic_signal_id));
  mxSetField(data_signal_ref,0,"data_file_key",mxCreateString(cdb_signal_ref.data_signal_ref.data_file_key));
  mxSetField(data_signal_ref,0,"time_axis_revision",mxCreateDoubleScalar(cdb_signal_ref.data_signal_ref.time_axis_revision));
  mxSetField(data_signal_ref,0,"data_file_id",mxCreateDoubleScalar(cdb_signal_ref.data_signal_ref.data_file_id));

*/

  const char* cdb_signal_field_names[] = {
"isset","data","record_number","generic_signal_id","revision","time_axis","axes","units","description"};
  mxArray* mx_cdb_signal = mxCreateStructArray(1, dims, 9, cdb_signal_field_names);
  mxSetField(mx_cdb_signal,0,"isset",mxCreateLogicalScalar(cdb_signal.isset));
  mwSize ndim = (mwSize) cdb_signal.ndim;
  mwSize *data_dims = (mwSize*) cdb_signal.dims;
  mxArray *data = mxCreateNumericArray(ndim, data_dims, mxDOUBLE_CLASS, mxREAL);
  /* http://www.mathworks.fr/help/techdoc/matlab_external/f24626.html */
  int i, ndata=data_dims[0];
  for (i=1;i<ndim; i++) {
    ndata = ndata * data_dims[i];
  }
  /* if sizeof(cdb_signal.data[0])==sizeof(mxReal) */
  memcpy(mxGetPr(data), cdb_signal.data, ndata*sizeof(mxREAL));
  mxSetField(mx_cdb_signal,0,"data",data);

  mxSetField(mx_cdb_signal,0,"record_number",mxCreateDoubleScalar(cdb_signal.record_number));
  mxSetField(mx_cdb_signal,0,"generic_signal_id",mxCreateDoubleScalar(cdb_signal.generic_signal_id));
  mxSetField(mx_cdb_signal,0,"revision",mxCreateDoubleScalar(cdb_signal.revision));
  mxSetField(mx_cdb_signal,0,"time_axis",mxCreateString("zatim nic"));
  mxSetField(mx_cdb_signal,0,"axes",mxCreateString("zatim nic"));
/*  mxSetField(mx_cdb_signal,0,"cdb_signal_",cdb_signal_);
  mxSetField(mx_cdb_signal,0,"cdb_signal_",cdb_signal_); */
  mxSetField(mx_cdb_signal,0,"units",mxCreateString(cdb_signal.units));
  mxSetField(mx_cdb_signal,0,"description",mxCreateString(cdb_signal.description));

}
