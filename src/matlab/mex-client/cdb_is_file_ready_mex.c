#include "mex.h"
#include "cyCDB_api.h"
/* #include "Python.h" */

void mexFunction( int nlhs, mxArray *plhs[], 
		  int nrhs, const mxArray*prhs[] )
     
{ 
  double *yp; 
  double *t,*y; 
  mwSize m,n; 

  /* Check for proper number of arguments */
  if (nrhs < 1) { 
    mexErrMsgTxt("At least one input argument required."); 
  } else if (nlhs > 1) {
    mexErrMsgTxt("Too many output arguments."); 
  } 

  /* Initialize Python */
  /* mexPrintf("Py_IsInitialized: %i\n", Py_IsInitialized()); */
  if (~Py_IsInitialized()) {
    /* mexPrintf("Py_Initialize()\n"); */
    Py_Initialize();
    int argc = 0;
    char* argv="";
  /* Py_SetProgramName(argv[0]); */
  /* Segmentation fault if PySys_SetArgv not called! */
    PySys_SetArgv(argc,&argv); 
  /* cyCDB initialization */
    /* mexPrintf("initcyCDB\n"); */
    initcyCDB();
    /* mexPrintf("importcyCDB\n"); */
    import_cyCDB();
  /* cyCDB working now */
    /* mexPrintf("cyCDB initialized :)\n"); */
    /* mexPrintf("Py_IsInitialized: %i\n",Py_IsInitialized()); */
  }

  /* parse input parameters */
  long data_file_id = (long) mxGetScalar(prhs[0]);

  int isready = cdb_is_file_ready(data_file_id);
  mxArray *field_value;
  field_value = mxCreateLogicalScalar(isready);
  plhs[0] = field_value;

}
