function data = cdb_read_file_data(file_name,file_type,data_key)

if strcmpi(file_type,'HDF5')
    data=hdf5read(file_name,data_key);
end
