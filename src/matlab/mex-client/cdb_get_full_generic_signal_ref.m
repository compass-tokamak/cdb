function ref = cdb_get_full_generic_signal_ref(name_or_id, record_number)

if nargin<2
    record_number = -1;
end	

glb_axes_names = {'time_axis','axis1','axis2','axis3','axis4','axis5','axis6'};

ref = cdb_get_generic_signal_ref_mex(name_or_id, record_number);
if isempty(ref)
  warning('generic signal not found');
else
  for v=glb_axes_names
      ax_name_id = char(strcat(v,'_id'));
      ax_name = char(v);
      if ref.(ax_name_id)
	  ref.(ax_name) = cdb_get_generic_signal_ref_mex(ref.(ax_name_id), record_number);
      end
  end
  ref.data_source = cdb_get_data_source_ref_mex(ref.data_source_id);
end
