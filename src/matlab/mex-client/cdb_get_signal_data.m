function signal_data = cdb_get_signal_data(signal_ref,n_samples,x0,x1)
% signal_ref is a  full signal reference obtained by cdb_get_full_signal_ref

glb_axes_names = {'time_axis','axis1','axis2','axis3','axis4','axis5','axis6'};

if nargin<4
    x1 = nan;
end
if nargin <3
    x0 = nan;
end
if nargin <2
    n_samples = nan;
end

% default value
signal_data = [];

if ~signal_ref.isset
    error('signal reference is not set');
end

if strcmpi(signal_ref.generic_signal_ref.signal_type,'FILE')
    if ~cdb_is_file_ready_mex(signal_ref.data_file_ref.data_file_id)
        error('file not ready for reading')
    end
    signal_data = cdb_read_file_data(signal_ref.full_file_path,signal_ref.data_file_ref.data_format,signal_ref.data_signal_ref.data_file_key);
	signal_data = double(reshape(signal_data, numel(signal_data), 1));
    if signal_ref.data_signal_ref.coefficient~=1 || signal_ref.data_signal_ref.offset~=0
        signal_data = (signal_data + signal_ref.data_signal_ref.offset) * signal_ref.data_signal_ref.coefficient;
    end
    % pridat i nacitani os
elseif strcmpi(signal_ref.generic_signal_ref.signal_type,'LINEAR')
    % predelat protoze signal_ref obsahuje i reference os
    ax_ref = struct([]);
    is_time_axis = false;
    for ax_name=glb_axes_names
        if isfield(signal_ref,char(strcat(ax_name,'_ref')))
            if isempty(ax_ref)
		ax_ref = signal_ref.(char(strcat(ax_name,'_ref')));
                if strcmpi(ax_name,'time_axis')
                    is_time_axis = true;
                end
            else
                error('cannot generate LINEAR signal with >1 axes');
            end
        end
    end

    if ~isempty(ax_ref)
        % linear signal with axes
        ax_sig_ref = cdb_get_full_signal_ref(ax_ref.generic_signal_ref.generic_signal_id,signal_ref.record_number,ax_ref.data_signal_ref.revision);
        axis_data = cdb_get_signal_data(ax_sig_ref,n_samples,x0,x1);
        if isempty(axis_data)
            error('no axis data coul be retrived')
        end

        if is_time_axis
            signal_data = ax_ref.data_signal_ref.offset + ax_ref.data_signal_ref.coefficient*(axis_data - ax_ref.data_signal_ref.time0);
        else
            signal_data = ax_ref.data_signal_ref.offset + ax_ref.data_signal_ref.coefficient*axis_data;
        end

    else
        % linear signal without axis
        % produce data
        if ~isnan(n_samples)
            if isnan(x0) x0 = 0; end
            signal_data = reshape(x0 + (0:n_samples-1)*signal_ref.data_signal_ref.coefficient,[],1);
        elseif ~isnan(x0) && ~isnan(x1)
            signal_data = reshape(x0:signal_ref.data_signal_ref.coefficient:x1,[],1);
        else
            error('cannot create linear signal')
        end
    end
else
    error('unknown signal type')
end
