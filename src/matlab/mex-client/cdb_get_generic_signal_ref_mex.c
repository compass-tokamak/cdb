#include "mex.h"
#include "cyCDB_api.h"

void mexFunction( int nlhs, mxArray *plhs[], 
		  int nrhs, const mxArray*prhs[] )
     
{ 

  /* Check for proper number of arguments */
  if (nrhs < 1) { 
    mexErrMsgTxt("At least one input arguments required."); 
  } else if (nlhs > 1) {
    mexErrMsgTxt("Too many output arguments."); 
  } 

  /* Initialize Python */
  /* mexPrintf("Py_IsInitialized: %i\n", Py_IsInitialized()); */
  if (~Py_IsInitialized()) {
    /* mexPrintf("Py_Initialize()\n"); */
    Py_Initialize();
    int argc = 0;
    char* argv="";
  /* Py_SetProgramName(argv[0]); */
  /* Segmentation fault if PySys_SetArgv not called! */
    PySys_SetArgv(argc,&argv); 
  /* cyCDB initialization */
    /* mexPrintf("initcyCDB\n"); */
    initcyCDB();
    /* mexPrintf("importcyCDB\n"); */
    import_cyCDB();
  /* cyCDB working now */
    /* mexPrintf("cyCDB initialized :)\n"); */
    /* mexPrintf("Py_IsInitialized: %i\n",Py_IsInitialized()); */
  }

  /* parse input parameters */
  long record_number = -1;
  if (nrhs >= 2) record_number = (long) mxGetScalar(prhs[1]);
  char* generic_signal_name;
  long generic_signal_id=0;
  mwSize buflen;
  int status;
  
  if (mxIsNumeric(prhs[0])) {
    generic_signal_id = (long) mxGetScalar(prhs[0]);
    /* SEG FAULT !!!
     * mexPrintf('generic_signal_id: %ld\n',generic_signal_id); */
  } else {
    buflen = mxGetN(prhs[0])*sizeof(mxChar)+1;
    generic_signal_name = mxMalloc(buflen);
    status = mxGetString(prhs[0], generic_signal_name, buflen); 
    /*mexPrintf("The input string is:  %s\n", generic_signal_name);*/
  }

  struct generic_signals_t generic_signal_ref;
  struct generic_signals_t* generic_signal_refs;
  int n_elements = 1;
  if (generic_signal_id) {
    generic_signal_ref = cdb_get_generic_signal_ref_by_id(generic_signal_id,record_number);
    if (generic_signal_ref.generic_signal_id == 0) n_elements = 0;
  } else {
    generic_signal_refs = cdb_generic_signals_references(generic_signal_name,record_number,&n_elements);
    if (n_elements>0) generic_signal_ref = generic_signal_refs[0];
    mxFree(generic_signal_name);
  }
  
  if (n_elements==0) {  
    plhs[0] = mxCreateStructArray(0, NULL, 0, NULL);
  } else {
    mwSize dims[1] = {1};
    const char *field_names[] = {
  "generic_signal_name","alias","description","last_record_number","axis3_id","time_axis_id","axis2_id","axis1_id","first_record_number","units","generic_signal_id","data_source_id","signal_type","axis4_id","axis5_id","axis6_id"};
    plhs[0] = mxCreateStructArray(1, dims, 16, field_names);
    mxSetField(plhs[0],0,"generic_signal_name",mxCreateString(generic_signal_ref.generic_signal_name));
    mxSetField(plhs[0],0,"alias",mxCreateString(generic_signal_ref.alias));
    mxSetField(plhs[0],0,"description",mxCreateString(generic_signal_ref.description));
    mxSetField(plhs[0],0,"last_record_number",mxCreateDoubleScalar(generic_signal_ref.last_record_number));
    mxSetField(plhs[0],0,"axis3_id",mxCreateDoubleScalar(generic_signal_ref.axis3_id));
    mxSetField(plhs[0],0,"time_axis_id",mxCreateDoubleScalar(generic_signal_ref.time_axis_id));
    mxSetField(plhs[0],0,"axis2_id",mxCreateDoubleScalar(generic_signal_ref.axis2_id));
    mxSetField(plhs[0],0,"axis1_id",mxCreateDoubleScalar(generic_signal_ref.axis1_id));
    mxSetField(plhs[0],0,"first_record_number",mxCreateDoubleScalar(generic_signal_ref.first_record_number));
    mxSetField(plhs[0],0,"units",mxCreateString(generic_signal_ref.units));
    mxSetField(plhs[0],0,"generic_signal_id",mxCreateDoubleScalar(generic_signal_ref.generic_signal_id));
    mxSetField(plhs[0],0,"data_source_id",mxCreateDoubleScalar(generic_signal_ref.data_source_id));
    mxSetField(plhs[0],0,"signal_type",mxCreateString(generic_signal_ref.signal_type));
    mxSetField(plhs[0],0,"axis4_id",mxCreateDoubleScalar(generic_signal_ref.axis4_id));
    mxSetField(plhs[0],0,"axis5_id",mxCreateDoubleScalar(generic_signal_ref.axis5_id));
    mxSetField(plhs[0],0,"axis6_id",mxCreateDoubleScalar(generic_signal_ref.axis6_id));
  }
}
