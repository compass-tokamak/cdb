% get signal reference
recordn = 1;
sig_ref = cdb_get_signal_ref_mex('electron density',recordn,-1)
% check is file is ready
isready = cdb_is_file_ready_mex(sig_ref.data_file_ref.data_file_id);
if isready
    disp('It''s possible to read from the file')
end

% cdb_get_generic_signal_ref_mex()
% using the name
gen_ref = cdb_get_generic_signal_ref_mex('electron density')
% using generic signal name + source name
gen_ref_full = cdb_get_full_generic_signal_ref('electron density/Interferometry')
%ref = cdb_get_generic_signal_ref_mex(1)

% prototype for cdb_get_signal function
% using alias
sig_ref_full = cdb_get_full_signal_ref('ne',recordn,-1)
%signal.data = cdb_get_signal_data(sig_ref)
%signal.time_axis_data = cdb_get_signal_data(signal.time_axis_ref,length(signal.data),signal.data_signal_ref.time0)

signal = cdb_get_signal(sig_ref_full)
