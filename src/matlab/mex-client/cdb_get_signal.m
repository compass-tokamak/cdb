function signal = cdb_get_signal(signal_ref)

glb_axes_names = {'time_axis','axis1','axis2','axis3','axis4','axis5','axis6'};

if isempty(signal_ref) || ~isfield(signal_ref,'isset') || ~signal_ref.isset
    error('signal reference undefined')
end

signal = signal_ref;
signal.data = cdb_get_signal_data(signal_ref);

for ax_name=glb_axes_names
    if isfield(signal_ref,char(strcat(ax_name,'_ref')))
        if strcmpi(ax_name,'time_axis')
            signal.(char(strcat(ax_name,'_data'))) = cdb_get_signal_data(signal.(char(strcat(ax_name,'_ref'))),length(signal.data),signal.data_signal_ref.time0);
        else
            signal.(char(strcat(ax_name,'_data'))) = cdb_get_signal_data(signal.(char(strcat(ax_name,'_ref'))),length(signal.data));
        end
    end		                                   
end
