function [cdb_signal] = cdb_convert_java_signal(java_signal, n_samples, x0)
%cdb_convert_java_signal Convert signal object in Java to Matlab structure (recursive)

if nargin < 3
    x0 = 0;
end
if nargin < 2
    n_samples = nan;
end

cdb_signal=[];
cdb_signal.java_ref = java_signal; % Can be removed

if isempty(java_signal)
    error('CDB:SignalError', 'Signal not found.')
end

% Data signal
java_sig_ref = java_signal.getSignalReference();
cdb_signal.ref = javaMap2Struct(java_sig_ref.asMap());

% Generic signal
java_gs_ref = java_signal.getGenericSignalReference();
cdb_signal.gs_ref = javaMap2Struct(java_gs_ref.asMap());

% Data file reference
if ~isempty(cdb_signal.ref.data_file_id)
  java_data_file_ref = java_signal.getDataFile();
  cdb_signal.file_ref = javaMap2Struct(java_data_file_ref.asMap());
else
  cdb_signal.file_ref = [];
end

% Axes (just reference tree)
axes = javaMap2Struct(java_signal.getAxes());
if isstruct(axes)
    axes_count = length(fieldnames(axes));
else
    axes_count = 0;
end

% Get data
is_file = java_signal.isFile();
if is_file
  calibration = javaMap2Struct(java_signal.getSignalCalibration(0).asMap()); % no unit factor
  cdb_signal.data = cdb_get_signal_data(cdb_signal, n_samples, nan, nan, calibration);
  signal_dim = size(cdb_signal.data);
else
  if n_samples
    cdb_signal.data = cdb_get_signal_data(cdb_signal, n_samples, x0);
  elseif axes_count > 1
    error('CDB:SignalError', 'Linear axis cannot depend on more than 1 axis.')
  else
    % Will read data later, bottom-up
  end
end

% Recursive call on axes
if axes_count
  axindex = 1;
  axis_names = fieldnames(axes);
  for axis_name = axis_names.'
    if exist('signal_dim', 'var')
      axis_samples = signal_dim(axindex);
      x0 = 0;
      if strcmp(axis_name, 'time_axis')
          x0 = cdb_signal.ref.time0;
      end
      axis = cdb_convert_java_signal(axes.(char(axis_name)), axis_samples, x0);
    else
      axis = cdb_convert_java_signal(axes.(char(axis_name)));
    end
    cdb_signal.(char(axis_name)) = axis;
    axindex = axindex + 1;
  end
end

% Read data bottom-up (linear signals dependent on other signals)
if ~is_file && axes_count == 1
  % TODO: Find a correct solution!
  cdb_signal.data = cdb_signal.ref.offset + cdb_signal.ref.coefficient * axis.data;
end

% Apply units
cdb_signal.units = char(java_signal.getUnit());
if java_signal.hasUnitFactor()
    unit_factor = java_signal.getUnitFactor();
    if ~(unit_factor == 1)
        cdb_signal.data = cdb_signal.data * unit_factor;
    end
end
    
end
