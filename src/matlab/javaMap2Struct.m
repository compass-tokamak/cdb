function [m_struct] = javaMap2Struct(jmap)
%javaMap2Struct Converts a Java Map object to a Matlab structure

m_struct = [];
it = jmap.entrySet().iterator();
while it.hasNext()
    elem = it.next();
    key = elem.getKey();
    val = elem.getValue();
    try
        % test for nested Java Maps
        void = val.entrySet();
        val = javaMap2Struct(val);
    catch
        % nothing
    end
    if strcmp(class(val), 'java.util.Date')
        val = char(val.toLocaleString());
    end
    m_struct.(key) = val;
end

end

