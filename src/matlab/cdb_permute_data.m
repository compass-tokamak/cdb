function [data] = cdb_permute_data(data, first_dim)

if nargin < 2
    first_dim = false;
end

if isstruct(data)
    for s = fieldnames(data)'
        data.(char(s)) = cdb_permute_data(data.(char(s)));
    end
else
    data = double(data);
    ndim = length(find(size(data) > 1));
    if ndim <= 1
        if ~first_dim
            % use 1st dimension for 1D data
            data = reshape(data, numel(data), 1);
        else
            % swap first two dimensions
            data = data.';
        end
    else
        % permute >= 2-D data
        data = permute(data, ndim:-1:1);
    end
end

end
