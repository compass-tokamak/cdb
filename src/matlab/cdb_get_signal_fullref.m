function [ cdb_signal_fullref ] = cdb_get_signal_fullref(str_id, client)
%cdb_get_signal_fullref Get full (composite) CDB signal reference

if nargin < 2
    client = cdb_connect();
end

% Get the signal reference
signal = client.getSignal(str_id);
if isempty(signal)
    error('CDB:SignalError', ['Signal not found: ' str_id ])
end
% convert to Matlab
sig_ref = javaMap2Struct(signal.getSignalReference().asMap());
gs_ref  = javaMap2Struct(signal.getGenericSignalReference().asMap());

cdb_signal_fullref.ref = sig_ref;
cdb_signal_fullref.gs_ref = gs_ref;

sig_params = javaMap2Struct(signal.getParameters().asMap());
for fld = fieldnames(sig_params).'
    cdb_signal_fullref.(fld{1}) = sig_params.(fld{1});
end

if strcmp(gs_ref.signal_type,'FILE')
    % read file signal
    file_ref = javaMap2Struct(signal.getDataFile().asMap());
else
    % linear signal
    file_ref = [];
end
cdb_signal_fullref.file_ref = file_ref;

end

