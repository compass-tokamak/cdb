function cdb_write_file_data(file_name, file_type, data_key, data, permute_data)

if nargin < 5
    permute_data = true;
end
if permute_data
    data = cdb_permute_data(data, true);
end

if data_key(1) ~= '/'
    % initial / required
    data_key = ['/', char(data_key)];
end

if strcmpi(file_type, 'HDF5')
    try
        h5create(file_name, data_key, size(data));
        h5write(file_name, data_key, data);
    catch
        hdf5write(file_name, data_key, data);
    end
else
    error('CDB:InputError', ['unknown fyle type: ', file_type]);
end

end
