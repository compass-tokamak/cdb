function [cdb_signal] = cdb_get_signal2(str_id, client)
%cdb_get_signal Get CDB signal including the data

if nargin < 2
    client = cdb_connect();
end

java_signal = client.getSignal(str_id);
cdb_signal = cdb_convert_java_signal(java_signal);

end
