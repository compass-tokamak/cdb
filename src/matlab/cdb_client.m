classdef cdb_client
    %cdb_client CDB client using JyCDB
    %

    properties
        % jClient
    end

    methods
        % contructor
        function obj = cdb_client()
            %cdb_client cdb_client contructor.
            % obj.jClient = cdb_connect();
            % obj.jClient = [];
        end

        function cdb_signal = get_signal(obj, str_id, raw_data)
            %get_signal Get CDB signal by string id.
            %   cdb_signal = get_signal(str_id) returns a structure withthe CDB signal,
            %   including the data and the description (references).

            if nargin < 3
                raw_data = false;
            end

            cdb_signal = cdb_get_signal(str_id, raw_data);
        end

        function cdb_signal = put_signal(obj, gs_alias_or_id, record_number, data, varargin)
            %put_signal Write CDB signal to HDF5 and database
            %   cdb_signal = put_signal(gs_alias_or_id, record_number, data, ...)
            %   writes signal data into CDB, including HDF5 file.
            %
            %   gs_alias_or_id: generic signal string id
            %   record_number: record number
            %   data: signal data (numeric)
            %   optional arguments: 'argument_name', default value, validation_function
            %   'variant', '', @ischar
            %   'time0', 0, @isnumeric
            %   'data_coef', 1, @isnumeric
            %   'data_offset', 0, @isnumeric
            %   'data_coef_V2unit', 1, @isnumeric
            %   'note', '', @ischar
            %   'no_debug', false, @islogical
            %   'time_axis_id', [], @isnumeric)
            %   'time_axis_data', [], @isnumeric
            %   'time_axis_coef', 1, @isnumeric
            %   'time_axis_offset', 0, @isnumeric
            %   'time_axis_note', '', @ischar
            %   'axis1_data', [], @isnumeric
            %   'axis1_coef', 1, @isnumeric
            %   'axis1_offset', 0, @isnumeric
            %   'axis1_note', '', @ischar
            %   'axis2_data', [], @isnumeric
            %   'axis2_coef', 1, @isnumeric
            %   'axis2_offset', 0, @isnumeric
            %   'axis2_note', '', @ischar
            %   'axis3_data', [], @isnumeric
            %   'axis3_coef', 1, @isnumeric
            %   'axis3_offset', 0, @isnumeric
            %   'axis3_note', '', @ischar
            %   'axis4_data', [], @isnumeric
            %   'axis4_coef', 1, @isnumeric
            %   'axis4_offset', 0, @isnumeric
            %   'axis4_note', '', @ischar
            %   'axis5_data', [], @isnumeric
            %   'axis5_coef', 1, @isnumeric
            %   'axis5_offset', 0, @isnumeric
            %   'axis5_note', '', @ischar
            %   'axis6_data', [], @isnumeric
            %   'axis6_coef', 1, @isnumeric
            %   'axis6_offset', 0, @isnumeric
            %   'axis6_note', '', @ischar

            cdb_signal = cdb_put_signal(gs_alias_or_id, record_number, data, varargin{:});
        end

        function record_number = create_record(obj, record_type)
            %create_record Create a new record and specify the record type

            if nargin < 2
                record_type = 'VOID'
            end
            record_number = cdb_create_record(record_type);  % , obj.jClient);
        end

    end

end

