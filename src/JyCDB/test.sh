#!/bin/bash
# ant # -- Build first (was too slow)

export CDB_DB="CDB_test"
export CDB_USER="CDB_test"
export CDB_DATA_ROOT="/tmp"
export CDB_HOST="$1"
export LD_LIBRARY_PATH="lib:$LD_LIBRARY_PATH"
export CLASSPATH="dist/JyCDB.jar:lib/junit-4.11.jar:lib/hamcrest-core-1.3.jar:build/test/classes:$CLASSPATH"
export JYTHONPATH="..:$JYTHONPATH"

java -Xmx1500m -cp $CLASSPATH org.junit.runner.JUnitCore cz.cas.ipp.compass.jycdb.JyCDBTestSuite | ./color-junit.py
