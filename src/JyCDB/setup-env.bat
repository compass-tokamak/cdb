@echo off
if defined CDB_PATH (
    echo Configuring CDB paths...
) else (
    echo ERROR: You have to set CDB_PATH environmental variable!
    exit /b -1;
)
set JYCDB_LIB_PATH=%CDB_PATH%\JyCDB\lib
set CLASSPATH=%CLASSPATH%;%CDB_PATH%\JyCDB\dist\JyCDB.jar
set CLASSPATH=%CLASSPATH%;%JYCDB_LIB_PATH%\jcommon-1.0.17.jar
set CLASSPATH=%CLASSPATH%;%JYCDB_LIB_PATH%\jfreechart-1.0.14.jar
set CLASSPATH=%CLASSPATH%;%JYCDB_LIB_PATH%\jhdf5.jar
set CLASSPATH=%CLASSPATH%;%JYCDB_LIB_PATH%\jhdfobj.jar
set CLASSPATH=%CLASSPATH%;%JYCDB_LIB_PATH%\jhdf5obj.jar
