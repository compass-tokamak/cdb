package cz.cas.ipp.compass.jycdb.util;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TestArrayUtils {
    @Test
    public void testRank() throws WrongDimensionsException {
        int notAnArray = 0;
        
        Assert.assertEquals(0, ArrayUtils.rank(notAnArray));
        
        int[] array1D = { 0 };
        Assert.assertEquals(1, ArrayUtils.rank(array1D));
        
        int[][] array2D = { { 0 } };
        Assert.assertEquals(2, ArrayUtils.rank(array2D));
        
        int[][][][][][][] array7D = new int[1][1][1][1][1][1][1];
        Assert.assertEquals(7, ArrayUtils.rank(array7D));
    }
    
    @Test
    public void testDimensions() throws WrongDimensionsException {
        int notAnArray = 0;
        Assert.assertArrayEquals(new long[0], ArrayUtils.dimensions(notAnArray));
        
        int[] array1D = { 0 };
        Assert.assertArrayEquals(new long[] {1}, ArrayUtils.dimensions(array1D));
        
        int[][] array2D = {{1, 2, 3}, {4, 5, 6}};
        Assert.assertArrayEquals(new long[] {2, 3}, ArrayUtils.dimensions(array2D));
    }
    
    @Test
    public void testReshape() throws WrongDimensionsException {
        int[][] sourceArray = {{1, 2, 3}, {4, 5, 6}};
        int[][] expectedArray = {{1, 2}, {3, 4}, {5, 6}};
        int[][] reshapedArray = (int[][]) ArrayUtils.reshape(sourceArray, new int[]{3, 2});
        Assert.assertArrayEquals(expectedArray, reshapedArray);
        
        try {
            ArrayUtils.reshape(sourceArray, new int[]{4, 2});
            Assert.fail("WrongDimensionsException expected but not thrown.");
        } catch (WrongDimensionsException ex) {
            // OK
        }
        
        int[][][] reshapedArray3 = (int[][][]) ArrayUtils.reshape(sourceArray, new int[]{1, 6, 1});
        int[][][] expectedArray3 = {{{1}, {2}, {3}, {4}, {5}, {6}}};
        Assert.assertArrayEquals(expectedArray3, reshapedArray3);
    }
}
