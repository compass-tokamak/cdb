package cz.cas.ipp.compass.jycdb;

import cz.cas.ipp.compass.jycdb.util.ParameterList;
import java.lang.reflect.InvocationTargetException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TestCDBClient {
    private CDBClient client;
    
    @Before
    public void setUp() throws CDBException {
        client = CDBClient.getInstance();
    }
    
    @Test
    public void testClientInitialization() {
        Assert.assertNotNull(client);
    }
    
    @Test
    public void testSameThreadGetsSameClient() throws CDBException {
        CDBClient client1 = CDBClient.getInstance();
        Assert.assertEquals(client, client1);
    }
    
    @Test
    public void testDifferentThreadsGetDifferentClient() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InterruptedException {
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    client = CDBClient.getInstance();
                } catch (CDBException ex) {
                    Logger.getLogger(TestCDBClient.class.getName()).log(Level.SEVERE, null, ex);
                    client = null;
                }
            }
            
            public CDBClient getClient() {
                return client;
            }
            
            public CDBClient client; 
        };
        t.start();
        t.join();
        CDBClient threaded = (CDBClient) t.getClass().getMethod("getClient").invoke(t);
        
        Assert.assertNotNull(threaded);
        Assert.assertNotEquals(client, threaded);
    }
    
    @Test
    public void testGetInstanceWithParameters() throws CDBException {
        // Two clients connected to different databases should have different last shot numbers.
        ParameterList parameters = new ParameterList();
        parameters.put("user", "CDB");
        parameters.put("db", "CDB");
        parameters.put("host", "codac2");
        CDBClient client2 = CDBClient.getInstance(parameters);
        
        Assert.assertNotEquals(client2.lastShotNumber(), client.lastShotNumber());
    }
    
    @Test
    public void testLastShotNumber() {
        Assert.assertTrue(client.lastShotNumber() > 0);
    }
    
    @Test
    public void testRecordExists() {
        long last = client.lastShotNumber();
        Assert.assertTrue(client.recordExists(last));
        Assert.assertFalse(client.recordExists(last + 1));
    }
    
    @Test
    public void testCheckConnection() {
        Assert.assertTrue(client.checkConnection());
    }
    
    @Test
    public void testGetFSSignal() {
        GenericSignal nonexistent = client.getFSSignal("Asasadqww", "Awqeqwq", "Aasdeerw");
        Assert.assertNull(nonexistent);
    }
    
    @Test
    public void testGetGenericSignal() throws CDBException {
        GenericSignal nonexistent = client.getGenericSignal("Nonexistent signal name gfdszxssd");
        Assert.assertNull(nonexistent);
    }

    @Test
    public void testCreateDAQChannel() {
        Random random = new Random();
        final long dataSourceId = 1;
        Integer id = (Integer)Math.abs(random.nextInt());
        final String nodeId = "TEST_" + id.toString();
        final String hardwareId = "TEST_BOARD";
        final String parameterId = "TEST_PARAMETER";
        
        final String expectedName = nodeId + "." + hardwareId + "." + parameterId;
        
        System.out.println(expectedName);
        
        //client.createDAQChannel(nodeId, hardwareId, parameterId, dataSourceId,  new Long[0]); // This is from 0.4 version, whis is hopefully not used anymore...
        final String computerName = "TEST" + id.toString();
        Long computerId;
        try {
            computerId = client.getComputerId(computerName);
        } catch(Exception e) {
            computerId = client.createComputer(computerName);
        }
        ParameterList parameters = new ParameterList();
        parameters.put("nodeuniqueid", nodeId);
        parameters.put("hardwareuniqueid", hardwareId);
        parameters.put("parameteruniqueid", parameterId);
        parameters.put("data_source_id", dataSourceId);
        parameters.put("computer_id", computerId);
        parameters.put("board_id",-1);
        client.createDAQChannel(parameters);

        GenericSignal gs = client.getFSSignal(nodeId, hardwareId, parameterId);
        
        Assert.assertNotNull(gs);
        Assert.assertEquals(expectedName, gs.getName());
    }

    @Test
    public void testCreateGenericSignal() throws WrongSignalTypeException, CDBException {
        Random random = new Random();
        final long dataSourceId = 1;
        Integer id = (Integer)Math.abs(random.nextInt());
        
        // Generic signal without axis and no alias
        String genericSignalName = "test_generic_signal_" + id.toString();
        String units = "uuu";
        
        client.createGenericSignal(genericSignalName, null, dataSourceId, null, units, GenericSignal.FILE);
        GenericSignal gs = client.getGenericSignal(genericSignalName);
        
        Assert.assertNotNull(gs);
        Assert.assertEquals(genericSignalName, gs.getName());
        Assert.assertEquals(null, gs.getAlias());
        Assert.assertEquals(units, gs.getUnits());
        Assert.assertEquals(null, gs.getTimeAxis());
        Assert.assertEquals(null, gs.getAxis(1));
        Assert.assertEquals(null, gs.getAxis(2));
        Assert.assertEquals(null, gs.getAxis(3));
        
        // Generic signal with time- and two spatial axes, with alias
        id = (Integer)Math.abs(random.nextInt()); 
        genericSignalName = "test_generic_signal_" + id.toString();
        String signalAlias = "aliaaas" + id.toString();
        
        String gsTimeAxisName = genericSignalName + "_t";
        String gsXAxisName = genericSignalName + "_x";
        String gsYAxisName = genericSignalName + "_y";
        
        client.createGenericSignal(gsTimeAxisName, null, dataSourceId, null, units, GenericSignal.LINEAR);
        client.createGenericSignal(gsXAxisName, null, dataSourceId, null, units, GenericSignal.LINEAR);
        client.createGenericSignal(gsYAxisName, null, dataSourceId, null, units, GenericSignal.LINEAR);
        
        GenericSignal gsT = client.getGenericSignal(gsTimeAxisName);
        GenericSignal gsX = client.getGenericSignal(gsXAxisName);
        GenericSignal gsY = client.getGenericSignal(gsYAxisName);
        
        Assert.assertEquals(GenericSignal.LINEAR, gsT.getSignalType());
        Assert.assertEquals(GenericSignal.LINEAR, gsX.getSignalType());
        Assert.assertEquals(GenericSignal.LINEAR, gsY.getSignalType());
        
        client.createGenericSignal(genericSignalName, signalAlias, dataSourceId, new Long[] {gsT.getId(), gsX.getId(), gsY.getId()}, units, GenericSignal.FILE);
        gs = client.getGenericSignal(genericSignalName);
        
        Assert.assertEquals(gsTimeAxisName, gs.getTimeAxis().getName());
        Assert.assertEquals(gsXAxisName, gs.getAxis(1).getName());
        Assert.assertEquals(gsYAxisName, gs.getAxis(2).getName());
        Assert.assertEquals(null, gs.getAxis(3));
    }
    
    @Test
    public void storeSignal() {
        long dataSourceId = 1;
        long recordNumber = client.lastShotNumber();
        double time0 = 0;
        double coefficient = 1.0;
        double offset = 0.0;
        
        TestHelper.ensureRecordDirectoryExistence(recordNumber);
        
        DataFile dataFile = client.newDataFile("test", recordNumber, dataSourceId);
        GenericSignal genericSignal = client.findGenericSignals("TEST", false).get(0);
        
        SignalReference signal = client.storeSignal(genericSignal.getId(), recordNumber, "some_data_file_key", dataFile.getId(), time0, coefficient, offset, 1.0);
        Assert.assertEquals(genericSignal.getId(), signal.getGenericSignalId());
    }
}
