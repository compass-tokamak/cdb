package cz.cas.ipp.compass.jycdb;

import cz.cas.ipp.compass.jycdb.util.UtilsTestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Test suite (JUnit) which aggregates all tests related to JyCDB.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    UtilsTestSuite.class,
    
    TestCDBClient.class,
    TestFSWriter.class,
    TestGenericSignal.class
})
public class JyCDBTestSuite {
    // Empty - just aggregate tests
}
