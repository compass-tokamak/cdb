package cz.cas.ipp.compass.jycdb.util;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TestHdf5Utils {   
    @Test
    public void testWriteData_short1D() {
        final String fileName = "testWriteData_short1D.h5";
        final String dataSetName = "data";
        
        File file = new File(fileName);
        if (file.exists()) file.delete();
        
        short[] originalArray = { 1, 4, 9, 16, 25, 35, 49, 64 };
        Data originalData = new Data(originalArray);
                
        try {
            Hdf5Utils.writeData(fileName, dataSetName, originalData);
            Data readData = Hdf5Utils.readFileData(fileName, dataSetName);
            
            short[] readArray = (short[])(readData.getRawData());
            Assert.assertArrayEquals(originalArray, readArray);
            
        } catch (Exception ex) {
            Logger.getLogger(TestHdf5Utils.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail(ex.getMessage());
        }
    }
    
    @Test
    public void testAppendData() throws UnknownDataTypeException, Hdf5ReadException, WrongDimensionsException, Hdf5WriteException {
        final String fileName = "testAppendData.h5";
        final String dataSetName = "data";
        
        File file = new File(fileName);
        if (file.exists()) file.delete();        
        
        int[][] firstArray = {{1, 2, 3}};
        int[][] secondArray = {{4, 5}}; // Last item will 
        int[][] expectedArray = {{1, 2, 3}, {4, 5, 6}};
        
        Hdf5Utils.writeData(fileName, dataSetName,  new Data(firstArray));
        Hdf5Utils.writeData(fileName, dataSetName, new Data(secondArray), new long[]{1, 0});
        Hdf5Utils.writeData(fileName, dataSetName, new Data(new int[][] {{6}}), new long[]{1, 2});
        
        Data readData = Hdf5Utils.readFileData(fileName, dataSetName);
        int[][] readArray = (int[][])(readData.getRawData());
        
        Assert.assertArrayEquals(expectedArray, readArray);
    }
    
    @Test
    public void testWriteData_double1D() {
        final String fileName = "testWriteData_double1D.h5";
        final String dataSetName = "data";
        
        File file = new File(fileName);
        if (file.exists()) file.delete();
        
        double[] originalArray = { 1.146949412521, 4.4464684674, -9.448647e4 };
        Data originalData = new Data(originalArray);
        
        double delta = 1e-3;
                
        try {
            Hdf5Utils.writeData(fileName, dataSetName, originalData);
            Data readData = Hdf5Utils.readFileData(fileName, dataSetName);
            
            double[] readArray = (double[])(readData.getRawData());
            
            Assert.assertArrayEquals(originalArray, readArray, delta);
            
        } catch (Exception ex) {
            Logger.getLogger(TestHdf5Utils.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail(ex.getMessage());
        }
    }    
    
    @Test
    public void testWriteData_int1D() {
        final String fileName = "testWriteData_int1D.h5";
        final String dataSetName = "data";
        
        File file = new File(fileName);
        if (file.exists()) file.delete();
        
        int[] originalArray = { -1949412521, 44684674, 4486474 };
        Data originalData = new Data(originalArray);
        
                
        try {
            Hdf5Utils.writeData(fileName, dataSetName, originalData);
            Data readData = Hdf5Utils.readFileData(fileName, dataSetName);
            
            int[] readArray = (int[])(readData.getRawData());
            
            Assert.assertArrayEquals(originalArray, readArray);
            
        } catch (Exception ex) {
            Logger.getLogger(TestHdf5Utils.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail(ex.getMessage());
        }
    }  
    
    @Test
    public void testWriteData_long1D() {
        final String fileName = "testWriteData_long1D.h5";
        final String dataSetName = "data";
        
        File file = new File(fileName);
        if (file.exists()) file.delete();
        
        long[] originalArray = { -19494454612521L, 446846464684786674L, 4486474, 0, -1 };
        Data originalData = new Data(originalArray);
        
                
        try {
            Hdf5Utils.writeData(fileName, dataSetName, originalData);
            Data readData = Hdf5Utils.readFileData(fileName, dataSetName);
            
            long[] readArray = (long[])(readData.getRawData());
            
            Assert.assertArrayEquals(originalArray, readArray);
            
        } catch (Exception ex) {
            Logger.getLogger(TestHdf5Utils.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail(ex.getMessage());
        }
    }     
    
    @Test
    public void testWriteData_short2D() throws Hdf5WriteException, UnknownDataTypeException, Hdf5ReadException, WrongDimensionsException {
        final String fileName = "testWriteData_short2D.h5";
        final String dataSetName = "data";
        
        File file = new File(fileName);
        if (file.exists()) file.delete();
        
        short[][] originalArray = { { 1, 2, 3, 4, 5, 6, 7, 8 }, { 1, 4, 9, 16, 25, 35, 49, 64 } };
        Data originalData = new Data(originalArray);

        Hdf5Utils.writeData(fileName, dataSetName, originalData);
        Data readData = Hdf5Utils.readFileData(fileName, dataSetName);

        short[][] readArray = (short[][])(readData.getRawData());
        Assert.assertArrayEquals(originalArray, readArray);
    }    
    
    @Test
    public void testWriteData_short3D() throws Hdf5WriteException, UnknownDataTypeException, Hdf5ReadException, WrongDimensionsException {
        final String fileName = "testWriteData_short3D.h5";
        final String dataSetName = "data";
        
        File file = new File(fileName);
        if (file.exists()) file.delete();
        
        short[][][] originalArray = { { {1, 2, 4} , { 5, 6, 7 } }, { {0, 0, 0}, { 5, 6, 8 } } };
        Data originalData = new Data(originalArray);

        Hdf5Utils.writeData(fileName, dataSetName, originalData);
        Data readData = Hdf5Utils.readFileData(fileName, dataSetName);

        short[][][] readArray = (short[][][])(readData.getRawData());
        Assert.assertArrayEquals(originalArray, readArray);
    }  
    
    @Test
    public void testMediumDataSet() throws Hdf5WriteException, UnknownDataTypeException, Hdf5ReadException, WrongDimensionsException {        
        // Inspired by 
        int array[][][] = new int[15][1216][900];
        array[14][1215][899] = 14;
       
        final String fileName = "testWriteData_medium.h5";
        final String dataSetName = "data";
        
        Data data = new Data(array);      
        
        File file = new File(fileName);
        if (file.exists()) file.delete();
        Hdf5Utils.writeData(fileName, dataSetName, data);
        Data readData = Hdf5Utils.readFileData(fileName, dataSetName);
        Assert.assertArrayEquals(data.getDimensions(), readData.getDimensions());
        int[][][] readArray = (int[][][])data.getRawData();
        Assert.assertArrayEquals(array[14][1215], readArray[14][1215]);
        
        file = new File(fileName);
        if (file.exists()) file.delete();
    }
    
    @Ignore
    @Test
    public void testHugeDataSet() throws Hdf5WriteException, UnknownDataTypeException, Hdf5ReadException, WrongDimensionsException {
        System.out.println();
        System.out.println();
        System.out.println("Writing and reading 800 MB of data.");
        System.out.println("This test takes a relatively long time.");
        System.out.println("You can disable it by setting @Ignore attribute to method TestHdf5Utils.testHugeDataSet().");
        System.out.println();
        
        int size = 200000000; // => 800 MB of data
        
        final String fileName = "testWriteData_huge.h5";
        final String dataSetName = "data";
        
        File file = new File(fileName);
        if (file.exists()) file.delete();

        int array[] = new int[size];
        for (int i = 0; i < size; i++) {
            array[i] = i;
        }
        Data data = new Data(array);            
        Hdf5Utils.writeData(fileName, dataSetName, data);
        Data readData = Hdf5Utils.readFileData(fileName, dataSetName);
        Assert.assertEquals(size, readData.getDimensions()[0]);
        int readArray[] = (int[])readData.getRawData();
        Assert.assertEquals(size - 2, readArray[size - 2]);
            
        file = new File(fileName);
        if (file.exists()) file.delete();
    }
}
