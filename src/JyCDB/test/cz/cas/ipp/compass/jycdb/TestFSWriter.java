package cz.cas.ipp.compass.jycdb;

import cz.cas.ipp.compass.jycdb.util.ParameterList;
import java.util.Random;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TestFSWriter {
    private CDBClient client;
    
    
    @Before
    public void setUp() throws CDBException {
        client = CDBClient.getInstance();
    }    

    public void ensureMarteBoardsExistence() {
        String node_id = "MARTE_NODE";
        String hardware_id = "ChannelDataCollection";
        String parameter_id = "Channel_001";
        Long data_source_id;
        try {
            client.FS2CDB_ref(node_id, hardware_id, parameter_id);
        } catch (Exception e) {
            System.out.print(e);
            String data_source_name = "MARTE";
            try {
                data_source_id = client.getDataSourceId(data_source_name);
            }
            catch (Exception e2) {
                ParameterList fields = new ParameterList();
                fields.put("name", data_source_name);
                fields.put("subdirectory",data_source_name);
                data_source_id = client.insert("data_sources", fields, true);
            }
            String computer_name = "marte";
            Long computer_id;
            try {
                computer_id = client.getComputerId(computer_name);
            }
            catch (Exception e3) {
                computer_id = client.createComputer(computer_name);
            }
            Random random = new Random();
            Integer id = (Integer)Math.abs(random.nextInt());
            String parameter_id_axis = "time_axis_marte" + id.toString();

            ParameterList axisParameters = new ParameterList();
            axisParameters.put("generic_signal_name",parameter_id_axis);
            axisParameters.put("data_source_id", data_source_id);
            axisParameters.put("signal_type", "LINEAR");
            Long time_axis_id = client.createGenericSignalWhichReturnsLong(axisParameters);
            Long[] axes = {time_axis_id};
            ParameterList parameters = new ParameterList();            
            parameters.put("nodeuniqueid",node_id);
            parameters.put("hardwareuniqueid",hardware_id);
            parameters.put("parameteruniqueid",parameter_id);
            parameters.put("data_source_id",data_source_id);
            parameters.put("time_axis_id",time_axis_id);
            parameters.put("computer_id",computer_id);
            parameters.put("board_id",-1);
            client.createDAQChannel(parameters);
        }
    }

    @Test
    public void testCreateMarteChannelExistingBoard() throws CDBException {
        String nodeId = "MARTE_NODE";
        String hardwareId = "ChannelDataCollection";
        
        Random random = new Random();
        Integer id = (Integer)Math.abs(random.nextInt());
        final String parameterId = "Channel_"+ id.toString();
        
        long recordNumber = client.lastShotNumber();
        ensureMarteBoardsExistence();

        FSWriter writer = new FSWriter(client, nodeId, hardwareId, parameterId, recordNumber, 0.0, 1.0);
        writer.readSignalInfo();
        
        GenericSignal gs = client.getFSSignal(nodeId, hardwareId, parameterId);
        Assert.assertNotNull(gs);
    }
    
    @Test(expected=Exception.class)
    public void testCreateMarteChannelNonExistingBoard() throws CDBException {
        String nodeId = "MARTE_NODE";
        String hardwareId = "unknown";
        
        Random random = new Random();
        Integer id = (Integer)Math.abs(random.nextInt());
        final String parameterId = "Channel_"+ id.toString();
        
        long recordNumber = client.lastShotNumber();
        ensureMarteBoardsExistence();

        FSWriter writer = new FSWriter(client, nodeId, hardwareId, parameterId, recordNumber, 0.0, 1.0);
        writer.readSignalInfo();
        
        GenericSignal gs = client.getFSSignal(nodeId, hardwareId, parameterId);
    }
}
