package cz.cas.ipp.compass.jycdb.util;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 *
 * @author pipek
 */
@RunWith(JUnit4.class)
public class TestData {
    @Test
    public void testAsDoubleArray1D() {
        try {
            double delta = 1e-3;
            double dArray[] = new double[] { 0.1, 0.2, 0.3 };
            Data dData = new Data(dArray);
            double dResult[] = dData.asDoubleArray1D();
            Assert.assertArrayEquals(dArray, dResult, delta);
            
            float fArray[] = new float[] { 0.1f, 0.2f, 0.3f };
            double fExpected[] = new double[] { 0.1, 0.2, 0.3 };
            Data fData = new Data(fArray);
            double fResult[] = fData.asDoubleArray1D();
            Assert.assertArrayEquals(fExpected, fResult, delta);
            
            int iArray[] = new int[] { 1, 2, -127 };
            double iExpected[] = new double[] { 1.0, 2.0, -127.0 };
            Data iData = new Data(iArray);
            double iResult[] = iData.asDoubleArray1D();
            Assert.assertArrayEquals(iExpected, iResult, delta);
            
            short sArray[] = new short[] { 1, 2, -127 };
            double sExpected[] = new double[] { 1.0, 2.0, -127.0 };
            Data sData = new Data(sArray);
            double sResult[] = sData.asDoubleArray1D();
            Assert.assertArrayEquals(sExpected, sResult, delta);            
            
        } catch (WrongDimensionsException ex) {
            Logger.getLogger(TestData.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail(ex.getMessage());
        } catch (UnknownDataTypeException ex) {
            Logger.getLogger(TestData.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail(ex.getMessage());
        }
    }
    
    @Test
    public void testFlatten1D() {
        try {
            int[] originalArray = new int[] { 1, 2 };
            Data data = new Data(originalArray);
            Assert.assertFalse(data.flatten());
            
            int[] readArray = (int[])data.getRawData();
            Assert.assertArrayEquals(originalArray, readArray);
            
            int[] array2 = new int[] { 1 };
            Data data2 = new Data(array2);
            Assert.assertFalse(data2.flatten());
            
        } catch (WrongDimensionsException ex) {
            Logger.getLogger(TestData.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail(ex.getMessage());
        }
    }
    
    @Test
    public void testFlatten2D() {
         try {
            // Nx1
            int[][] originalX = new int[][] { { 1 }, { 2 } };
            int[] expectedX = new int[] { 1, 2 };
            Data dataX = new Data(originalX);
            Assert.assertTrue(dataX.flatten());
            
            int[] readX = (int[])dataX.getRawData();
            Assert.assertArrayEquals(expectedX, readX);
            
            // 1xN
            int[][] originalY = new int[][] { { 12, -2 } };
            int[] expectedY = new int[] { 12, -2 };
            Data dataY = new Data(originalY);
            Assert.assertTrue(dataY.flatten());
            
            int[] readY = (int[])dataY.getRawData();
            Assert.assertArrayEquals(expectedY, readY);            
            
            // MxN
            int[][] original = new int[][] { { 1, 2 }, {3, 4 } };
            Data data = new Data(original);
            Assert.assertFalse(data.flatten());
            
            int[][] read = (int[][])data.getRawData();
            Assert.assertArrayEquals(original, read);
            
        } catch (WrongDimensionsException ex) {
            Logger.getLogger(TestData.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail(ex.getMessage());
        }       
    }
}
