package cz.cas.ipp.compass.jycdb;

import java.util.Random;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TestGenericSignal {
    @Test
    public void NonExistentAxes() throws CDBException {
        CDBClient client = CDBClient.getInstance();
        Random random = new Random();
        Integer id = (Integer)Math.abs(random.nextInt());
        final String gsName = "TEST_" + id.toString();
        
        client.createGenericSignal(gsName, null, 1, new Long[] {}, "u", GenericSignal.FILE);
        GenericSignal gs = client.getGenericSignal(gsName);
        
        Assert.assertEquals(0, gs.getTimeAxisId());
        Assert.assertNull(gs.getTimeAxis());
        
        for (int i = 0; i <= GenericSignal.MAX_AXES; i++) {
            Assert.assertEquals(0, gs.getAxisId(i));
            Assert.assertNull(gs.getAxis(i));
        }
    } 
}
