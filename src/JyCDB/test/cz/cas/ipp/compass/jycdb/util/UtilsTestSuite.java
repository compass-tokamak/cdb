package cz.cas.ipp.compass.jycdb.util;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Test suite (JUnit) which aggregates all tests related to utils used by JyCDB (but not directly CDB functionality).
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    TestHdf5Utils.class,
    TestData.class,
    TestArrayUtils.class
})
public class UtilsTestSuite {
    // Empty - just aggregate tests
}
