package cz.cas.ipp.compass.jycdb;

import org.junit.Before;

public class TestBase {
    protected CDBClient client;
    
    @Before
    public void setUp() throws CDBException {
        client = CDBClient.getInstance();
    }       
}
