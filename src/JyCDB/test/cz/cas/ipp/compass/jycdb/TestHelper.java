package cz.cas.ipp.compass.jycdb;

import java.io.File;

public class TestHelper {
    public static void ensureRecordDirectoryExistence(long recordNumber) {
        String base = System.getenv("CDB_DATA_ROOT");
        File recordDirectory = new File(base, (new Long(recordNumber)).toString());
        
        if (!recordDirectory.exists()) {
            recordDirectory.mkdir();
        }
    }
}
