package cz.cas.ipp.compass.jycdb.util;

import cz.cas.ipp.compass.jycdb.CDBException;

/**
 *
 * @author pipek
 */
public class Hdf5ReadException extends CDBException {
   public Hdf5ReadException(String message) {
       super(message);
   }       
}
