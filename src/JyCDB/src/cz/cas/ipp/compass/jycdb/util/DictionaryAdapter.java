package cz.cas.ipp.compass.jycdb.util;

import java.util.SortedMap;
import org.python.core.Py;
import org.python.core.PyObject;

/**
 * Object that is represented by a python dictionary (or OrderedDict)
 * and usually obtained as a row from database.
 * 
 * Each of the classes should implement static method create(PyObject)
 * which returns null if underlying Python object is None.
 * 
 * There should be no public constructor in child classes.
 */
public class DictionaryAdapter extends PythonAdapter {
    public DictionaryAdapter(PyObject object) {
        super(object);
        // TODO: check for dict-ness of this.
    }
    
    /**
     * Get dictionary value indexed by key as python object.
     */
    public PyObject get(String key) {
        return pyObject.__getitem__(Py.newString(key));
    }
    
    /**
     * Map python dictionary to java map.
     * 
     * Motivation: Binding for MATLAB.
     */
    public SortedMap asMap() {
        return (SortedMap)PythonUtils.asNative(this.pyObject);
    }    
        
    /**
     * Get list of dictionary keys.
     * 
     * Motivation: Binding for IDL.
     * 
     * @return Array of keys in the order they are stored in by Python.
     */
    public String[] getDictKeys() {
        PyObject pyKeyArray = this.pyObject.invoke("keys");
        int keyLength = pyKeyArray.__len__();
        
        String[] keys = new String[keyLength];
        
        for (int i = 0; i < keyLength; i++) {
            keys[i] = pyKeyArray.__getitem__(i).asString();
        }
        return keys;
    }
    
    /**
     * Get dictionary value indexed by key as "Java native" object.
     * 
     * Motivation: Binding for IDL.
     * 
     * @see PythonUtils#asNative(org.python.core.PyObject)
     */
    public Object getDictValue(String key) {
        return PythonUtils.asNative(get(key));
    }
    
    /**
     * Get dictionary value indexed by key as long.
     * 
     * Motivation: Binding for IDL.
     */
    public long getDictValueAsLong(String key) {
        Object obj = getDictValue(key);
        if (obj == null) {
            return 0;
        } else {
            return ((Long)obj).longValue();
        }
    }
    
    /**
     * Get dictionary value indexed by key as string.
     * 
     * Motivation: Binding for IDL.
     */
    public String getDictValueAsString(String key) {
        Object obj = getDictValue(key);
        if (obj == null) {
            return "";
        } else {
            return (String)obj;
        }
    }
    
    /**
     * Get dictionary value indexed by key as double.
     * 
     * Motivation: Binding for IDL.
     */
    public double getDictValueAsDouble(String key) {
        Object obj = getDictValue(key);
        if (obj == null) {
            return 0.0;
        } else {
            return ((Double)obj).doubleValue();
        }
    }
}
