package cz.cas.ipp.compass.jycdb;

/**
 * The connection to database does not work.
 * 
 * This means that the pyCDB was correctly loaded but there is a problem
 * inside it or between it and MySQL.
 * 
 * @author pipek
 */
public class CDBConnectionException extends CDBException {
   public CDBConnectionException(String message) {
       super(message);
   }
}
