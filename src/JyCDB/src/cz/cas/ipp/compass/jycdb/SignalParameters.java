package cz.cas.ipp.compass.jycdb;

import cz.cas.ipp.compass.jycdb.util.DictionaryAdapter;
import cz.cas.ipp.compass.jycdb.util.JsonUtils;
import org.python.core.PyObject;

/**
 * Signal parameters (both GS &amp; DAQ-based).
 * 
 * These are strings interpreted as JSON.
 * 
 * Strings are accessible using getDAQParametersString() &amp; getGenericSignalParametersString(),
 * Parsed JSON objects (as trees) are accessible using getDAQParameters() &amp; getGenericSignalParameters().
 * 
 * @author pipek
 */
public class SignalParameters extends DictionaryAdapter {   
    public static SignalParameters create(PyObject object) {
        if (object.__nonzero__()) {
            return new SignalParameters(object);
        } else {
            return null;
        }
    }     
    
    private SignalParameters(PyObject object) {
        super(object);
    } 
    
    public String getDAQParametersString() {
        return get("daq_parameters").asString();
    }
    
    public String getGenericSignalParametersString() {
        return get("gs_parameters").asString();
    }
    
    public Object getDAQParameters() {
        return JsonUtils.parseNative(getDAQParametersString());
    }
    
    public Object getGenericSignalParameters() {
        return JsonUtils.parseNative(getGenericSignalParametersString());
    }
}
