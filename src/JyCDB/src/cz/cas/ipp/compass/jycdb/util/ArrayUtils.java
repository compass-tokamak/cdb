package cz.cas.ipp.compass.jycdb.util;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Utils for quick manipulation with (numeric) arrays.
 * 
 * Warning: most methods work only on rectangular arrays.
 * 
 * @author pipek
 */
public class ArrayUtils {
    /**
     * Multiply all elements of an array by a constant.
     * 
     * @return a new array
     */
    public static double[] multiply(double[] array, double coefficient) {
        return linearTransform(array, coefficient, 0.0);
    }
    
    /**
     * Add a constant to all elements of an array.
     * 
     * @return a new array
     */
    public static double[] add(double[] array, double offset) {
        return linearTransform(array, 1.0, offset); 
    }
    
    /**
     * Apply a linear transformation y = ax + b on an array.
     * 
     * @param multiplyBy Multiplication constant
     * @param add Addition constant
     * 
     * @return a new array
     */
    public static double[] linearTransform(double[] array, double multiplyBy, double add) {
        double[] result = new double[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i] * multiplyBy + add;
        }
        return result;          
    }
    
    /**
     * A general linear transformation of multidimensional array.
     * 
     * @param array - N-dimensional rectangular array of double/long/int/short/float
     * @param multiplyBy - multiplicative factor
     * @param add - additive factor
     * @return - N-dimensional array of the same shape as input
     */
    public static Object linearTransform(Object array, double multiplyBy, double add) throws UnknownDataTypeException {
        int length = Array.getLength(array);
        if (length == 0) {
            return new double[0];
        } else {
            Object firstElement = Array.get(array, 0);
            if (firstElement.getClass().isArray()) {
                Object[] newArray = new Object[length];
                for (int i = 0; i < length; i++) {
                    ((Object[])newArray)[i] = linearTransform(Array.get(array, i), multiplyBy, add);
                }
                return newArray;
            } else {
                double[] doubleArray = asDoubleArray(array);
                double[] newArray = new double[length];
                if (multiplyBy == 1.0) {
                    if (add == 0.0) {
                        // Must copy
                        for (int i = 0; i < length; i++) {
                            newArray[i] = Array.getDouble(doubleArray, i);
                        }
                    } else {
                        for (int i = 0; i < length; i++) {
                            newArray[i] = Array.getDouble(doubleArray, i) + add;
                        }
                    }
                } else if (add == 0.0) {
                    for (int i = 0; i < length; i++) {
                        newArray[i] = Array.getDouble(doubleArray, i) * multiplyBy;
                    }
                } else {
                    for (int i = 0; i < length; i++) {
                        newArray[i] = Array.getDouble(doubleArray, i) * multiplyBy + add;
                    }                   
                }

                return newArray;
            }
        }
    }
    
    /**
     * Cast all values of an array to double (runtime version).
     * 
     * @param array Array of allowed type.
     * @return
     * @throws UnknownDataTypeException 
     * 
     * 
     */
    public static double[] asDoubleArray(Object array) throws UnknownDataTypeException {
        if (array instanceof double[]) {
            return (double [])array;
        } else if (array instanceof long[]) {
            return asDoubleArray((long[]) array);
        } else if (array instanceof short[]) {
            return asDoubleArray((short[]) array);
        } else if (array instanceof int[]) {
            return asDoubleArray((int[]) array);
        } else if (array instanceof float[]) {
            return asDoubleArray((float[]) array);
        } else {
            throw new UnknownDataTypeException("Cannot convert array to doubles.");
        }
    }
    
    /**
     * Cast all values of an array to double.
     */    
    public static double[] asDoubleArray(long[] array) {
        double[] result = new double[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }
    
    /**
     * Cast all values of an array to double.
     */    
    public static double[] asDoubleArray(int[] array) {
        double[] result = new double[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }    
    
    /**
     * Cast all values of an array to double.
     */    
    public static double[] asDoubleArray(short[] array) {
        double[] result = new double[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }    
    
    /**
     * Cast all values of an array to double.
     */
    public static double[] asDoubleArray(float[] array) {
        double[] result = new double[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }
    
    /**
     * Cast all values of an array to int.
     */    
    public static int[] asIntArray(long[] array) {
        int[] result = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = (int)array[i];
        }
        return result;
    }
    
    /**
     * Cast all values of an array to long.
     */    
    public static long[] asLongArray(int[] array) {
        long[] result = new long[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }    
    
    /**
     * Minimum value found in the array.
     */
    public static double min(double[] array) {
        double min = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
            }
        }
        return min;
    }
    
    /**
     * Maximum value found in the array.
     */
    public static double max(double[] array) {
        double max = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }
        return max;
    }
    
    public static long product(int[] array) {
        long result = 1;
        for (int i = 0; i < array.length; i++) {
            result *= array[i];
        }
        return result;
    }
    
    public static long product(long[] array) {
        long result = 1;
        for (int i = 0; i < array.length; i++) {
            result *= array[i];
        }
        return result;
    }    

    /**
     * Get the number of dimensions of an array.
     * 
     * Works only on rectangular arrays.
     */
    public static int rank(Object array) throws WrongDimensionsException {
        int rank = 0;
        Object o = array;
        
        while (o.getClass().isArray()) {
            if (Array.getLength(o) == 0) {
                throw new WrongDimensionsException("Cannot get rank for degenerate multidimensional array.");
            }
            o = Array.get(o, 0);
            rank++;
        }
        return rank;
    }
    
    /**
     * Get the dimensions along all axes of an array.
     * 
     * Works only on rectangular arrays.
     */
    public static long[] dimensions(Object array) throws WrongDimensionsException {
        long[] dimensions = new long[rank(array)];
        
        Object o = array;
        int rank = 0;
        
        while (o.getClass().isArray()) {
            dimensions[rank] = Array.getLength(o);
            if (dimensions[rank] == 0) {
                throw new WrongDimensionsException("Cannot get rank for degenerate multidimensional array.");
            }
            o = Array.get(o, 0);
            rank++;
        }
        return dimensions;
    }
    
    /**
     * Total number of items in array.
     * 
     * Works only on rectangular arrays.
     */
    public static long size(Object array) throws WrongDimensionsException {
        return product(dimensions(array));
    }
    
    /**
     * Get an element from a multidimensional array.
     * 
     * Works on all arrays.
     */
    public static Object get(Object array, int[] index) {
        Object o = array;
        for (int i = 0; i < index.length; i++) {
            o = Array.get(o, index[i]);
        }
        return o;
    }
    
    /**
     * Set an element in a multidimensional array.
     * 
     * Works on all arrays.
     */    
    public static void set(Object array, int[] index, Object value) {
        Object o = array;
        for (int i = 0; i < (index.length - 1); i++) {
            o = Array.get(o, index[i]);
        }
        Array.set(o, index[index.length - 1], value);
    }
    
    /**
     * Reshape array.
     * 
     * Create a new array with the same total size but with different dimensions.
     * Last index is moving fastest, while the elements are copied one after another.
     * 
     * Works only on rectangular arrays.
     * 
     * @param array
     * @param newDimensions
     * @return new array
     */
    public static Object reshape(Object array, int[] newDimensions) throws WrongDimensionsException {
        Class type = array.getClass();
        while (type.isArray()) {
            type = type.getComponentType();
        }
        
        Object newArray = Array.newInstance(type, newDimensions);
        
        int[] oldDimensions = asIntArray(dimensions(array));
        
        int[] oldIndex = new int[oldDimensions.length];
        int[] newIndex = new int[newDimensions.length];
        
        int max = (int)product(oldDimensions);
        if (product(newDimensions) != max) {
            throw new WrongDimensionsException("Arrays of different size.");
        }
        
        for (int i = 0; i < max; i++) {
            Object value = get(array, oldIndex);
            set(newArray, newIndex, value);
            
            oldIndex[oldIndex.length - 1] += 1;
            for (int dim = oldIndex.length - 1; dim >= 0; dim--) {
                if (oldIndex[dim] == oldDimensions[dim]) {
                    if (dim > 0) {
                        oldIndex[dim-1] += 1;
                    }
                    oldIndex[dim] = 0;
                }
            }
                
            newIndex[newIndex.length - 1] += 1;
            for (int dim = newIndex.length - 1; dim >= 0; dim--) {
                if (newIndex[dim] == newDimensions[dim]) {
                    if (dim > 0) {
                        newIndex[dim-1] += 1;
                    }
                    newIndex[dim] = 0;
                }
            }            
        }
        return newArray;
    }
    
    public static int[] range(int xmin, int xmax) {
        int length = xmax - xmin;
        int[] result = new int[length];
        for (int i = xmin; i < xmax; i++) {
            result[i] = i;
        }
        return result;
    }
}
