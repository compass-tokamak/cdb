package cz.cas.ipp.compass.jycdb.util;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utilities helping with debugging.
 * 
 * @author pipek
 */
public class DebugUtils {
    private static long nanoTimeLast = System.nanoTime();
    
    private static Logger mainLogger = Logger.getLogger("DebugUtils");
    
    /**
     * Print a message alongside with the number of nanoseconds since last call of this method.
     * 
     * @param message Message to log
     */
    public static void logWithDuration(String message) {
        logWithDuration(message, mainLogger);
    }
    
    /**
     * Print a message alongside with the number of nanoseconds since last call of this method.
     * 
     * @param message Message to log
     * @param logger Logger to use
     */    
    public static void logWithDuration(String message, Logger logger) {
        long diff = System.nanoTime() - nanoTimeLast;
        String wholeMessage = message + "(" + (new Long(diff / 1000000)).toString() + "msec )";
        logger.log(Level.FINE, wholeMessage);
        nanoTimeLast = System.nanoTime();
    }
}