package cz.cas.ipp.compass.jycdb.plotting;

import cz.cas.ipp.compass.jycdb.CDBClient;
import cz.cas.ipp.compass.jycdb.CDBException;
import cz.cas.ipp.compass.jycdb.GenericSignal;
import cz.cas.ipp.compass.jycdb.Signal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.SamplingXYLineRenderer;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.data.xy.DefaultXYDataset;

public class DataPlotter extends JFrame {
    public DataPlotter() {
        super("JyCDB Plot");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
    private static double[][] getXYData(Signal signal) throws CDBException {
        double[] xdata = signal.getAxis(0).getData().asDoubleArray1D();
        double[] ydata = signal.getData().asDoubleArray1D();
        return new double[][] { xdata, ydata };
    }
    
    public static JFreeChart createChart(Signal signal) throws CDBException {
        long recordNumber = signal.getSignalReference().getRecordNumber();
        GenericSignal genericSignal = signal.getGenericSignalReference();
        DefaultXYDataset ds = new DefaultXYDataset();
        double[][] data = getXYData(signal);
        ds.addSeries(genericSignal.getName()+" - " + recordNumber, data);
        
        SamplingXYLineRenderer renderer = new SamplingXYLineRenderer();
        
        XYPlot plot = new XYPlot(ds, new NumberAxis("time [ms]"), new NumberAxis("value"), renderer);
        
        JFreeChart chart = new JFreeChart(plot);
        chart.setTitle(genericSignal.getName() + "(" + (new Long(recordNumber)).toString()  +")");
        chart.removeLegend();
        return chart;
    }
    
    public static JFreeChart appendChart(JFreeChart oldChart, Signal signal) 
           throws CDBException{
        DefaultXYDataset ds = (DefaultXYDataset) oldChart.getXYPlot().getDataset();
        double[][] data = getXYData(signal);
        long recordNumber = signal.getSignalReference().getRecordNumber();
        
        //comperession: 
        final int comressionStep = 100;
        double[][] newData = new double[2][(int) data[0].length/comressionStep];
        for(int i=0, j=0; i < data[1].length && j < newData[1].length; i+=comressionStep, j++){
            newData[0][j] = data[0][i];
            newData[1][j] = data[1][i];
        }
        ds.addSeries(signal.getName() + " - " + recordNumber, newData);

        XYPlot plot = new XYPlot(ds, new NumberAxis("time [ms]"), new NumberAxis("value"),
                new StandardXYItemRenderer());
        System.err.println("4");
        JFreeChart chart = new JFreeChart(plot);
        chart.setTitle("Multiple plot");
        return chart;
    }
    
    public static void main(String[] args) {
        try {
            Signal signal;
            CDBClient cdb = CDBClient.getInstance();
            if (args.length == 1) {
                signal = cdb.getSignal(args[0]);
            } else {
                System.out.println("Usage: jycdb-plot <strid>");
                return;
            }
            JFreeChart chart = createChart(signal);
            
            DataPlotter dp = new DataPlotter();

            ChartPanel chartPanel = new ChartPanel(chart);
            
            chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
            dp.setContentPane(chartPanel);
            dp.pack();
            dp.setVisible(true);
        } catch (CDBException ex) {
            Logger.getLogger(DataPlotter.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
}
