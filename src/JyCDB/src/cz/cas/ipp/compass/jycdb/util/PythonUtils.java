package cz.cas.ipp.compass.jycdb.util;

import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import org.python.core.Py;
import org.python.core.PyObject;
import org.python.util.PythonInterpreter;

/**
 * Utilities for easier manipulation with Python through Jython.
 * 
 * @author pipek
 */
public class PythonUtils {
    private static PythonInterpreter interpreter = null;
    
    /**
     * Get a single copy of python interpreter.
     * 
     * More of them should not be needed.
     */
    public synchronized static PythonInterpreter getInterpreter()
    {
        if (interpreter == null) {
            interpreter = new PythonInterpreter();
        }
        return interpreter;
    }
    
    /**
     * Invoke a method on a Python object with named parameters.
     * 
     * It calls `invoke` method present in Jython, it only allows easier
     * manipulation with parameters (enabling this to be called with ParameterList).
     */
    public static PyObject invoke(PyObject object, String method, Map<String,PyObject> parameters) {
        Set<Map.Entry<String, PyObject>> entries = parameters.entrySet();
        PyObject[] values = new PyObject[parameters.size()];
        String[] keywords = new String[parameters.size()];
        
        int i = 0;
        for (Map.Entry<String, PyObject> entry : entries) {
            keywords[i] = entry.getKey();
            values[i] = entry.getValue();
            i++;
        }
        return object.invoke(method, values, keywords);
    }
    
    /**
     * Create a new instance of a named python class.
     */
    public static PyObject newObject(String className, Map<String,PyObject> parameters) {
        PyObject klass = getInterpreter().get(className);
        return invoke(klass, "__call__", parameters);
    }
    
    /**
     * Interpret the python object as a native Java one.
     * 
     * @return Object of the correct type. It has to be cast to be useful.
     * 
     * Numbers converts to numbers (long or double).
     * Dicts converts to TreeMap.
     * Tuples and lists converts to Object[].
     * Dates converts to Date.
     * NoneType converts to null.
     * 
     * In case it does not know a type, it returns its string representation along with ! and type name.
     * (e.g. "!representation of my weird type{WeirdType}")
     */
    public static Object asNative(PyObject object) {
        String type = object.getType().getName();
        if (type.equals("NoneType")) {
            return null;
        } else if (type.equals("str") || type.equals("unicode")) {
            return object.asString();
        } else if (type.equals("bool")) {
            return (object.asInt() > 0);
        } else if (type.equals("int")) {
            return (object.asLong());
        } else if (type.equals("long")) {
            return (object.asLong());
        } else if (type.equals("float")) {
            return (object.asDouble());
        } else if (type.equals("datetime.datetime") || type.equals("datetime")) {
            PythonInterpreter python = getInterpreter();
            String format = "%d %m %Y %H:%M:%S";
            python.exec("import time");
            String timeStr = object.invoke("strftime", Py.newString(format)).asString();
            double seconds = python.eval("time.mktime(time.strptime(\"" + timeStr + "\", \"" + format + "\"))").asDouble();
            return new java.util.Date((long)(seconds * 1000));
        } else if (type.equals("tuple") || type.equals("list")) {
            Object[] array = new Object[object.__len__()];
            Iterable<PyObject> iterable = object.asIterable();
            int index = 0;
            for (PyObject item : iterable) {
                array[index] = asNative(item);
                index++;
            }
            return array;
        } else if (type.equals("dict") || type.equals("ordereddict.OrderedDict") || type.equals("OrderedDict")) {
            SortedMap map = new TreeMap();
            Iterable<PyObject> keys = object.invoke("keys").asIterable();
            for (PyObject keyObject : keys) {
                String key = keyObject.asString();
                PyObject value = object.__getitem__(Py.newString(key));
                map.put(key, PythonUtils.asNative(value));
            }   
            return map;
        } else {
            return "!" + object.__str__().asString() + "{" + type + "}";
        }
    }
}
