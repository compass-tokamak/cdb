package cz.cas.ipp.compass.jycdb;

import cz.cas.ipp.compass.jycdb.util.DictionaryAdapter;
import org.python.core.PyObject;

/**
 * Wrapper around rows of `data_files` table.
 * 
 * @author honza
 */
public class DataFile extends DictionaryAdapter {
    public static DataFile create(PyObject object) {
        if (object.__nonzero__()) {
            return new DataFile(object);
        } else {
            return null;
        }
    }
    
    private DataFile(PyObject object) {
        super(object);
    }
    
    /**
     * Full path to the file containing data.
     */
    public String getFullPath() {
        return get("full_path").asString();
    }
    
    public String getFileName() {
        return get("file_name").asString();
    }
    
    public String getCollectionName() {
        return get("collection_name").asString();
    }
    
    public long getId() {
        return get("data_file_id").asLong();
    }
}
