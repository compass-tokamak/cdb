package cz.cas.ipp.compass.jycdb;

import cz.cas.ipp.compass.jycdb.util.ArrayUtils;
import cz.cas.ipp.compass.jycdb.util.DictionaryAdapter;
import org.python.core.PyObject;

/**
 * Signal calibration.
 * 
 * Meaning: value = (data + offset) * coefficient
 * 
 * @author pipek
 */
public class SignalCalibration extends DictionaryAdapter {
    public static SignalCalibration create(PyObject object) {
        if (object.__nonzero__()) {
            return new SignalCalibration(object);
        } else {
            return null;
        }
    }    
    
    public static final String RAW = "RAW";    
    public static final String DAQ_VOLTS = "DAV";
    
    private SignalCalibration(PyObject object) {
        super(object);
    }  
        
    public double getOffset() {
        return get("offset").asDouble();
    }
    
    public double getCoefficient() {
        return get("coefficient").asDouble();
    }
    
    public String getUnits() {
        return get("units").asString();
    }
    
    /**
     * If you have an array of data, apply calibration to it.
     * 
     * Returns new array.
     */
    public double[] apply (double[] array) {
        double add = getOffset() / getCoefficient();
        return ArrayUtils.linearTransform(array, getCoefficient(), add);
    }
}
