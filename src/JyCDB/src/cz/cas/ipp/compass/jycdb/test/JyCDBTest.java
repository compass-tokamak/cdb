package cz.cas.ipp.compass.jycdb.test;

import cz.cas.ipp.compass.jycdb.*;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @deprecated
 * @author pipek
 */
public class JyCDBTest {
    public static void main(String[] args) {
        // instanceTest();
        fsWriterTest();
    }
    
    public static void fsWriterTest() {
        try {
            CDBClient client = CDBClient.getInstance();
            for (int i= 0; i < 100; i++) {
                long shotNo = client.createRecord("EXP");
                FSWriter fsw = new FSWriter(client, "HUNGRY_NODE", "BOARD_1", "CHANNEL_001", shotNo, 912.0, 0.11011);
                fsw.readSignalInfo();
                if (fsw.signalExists()) {
                    System.out.println("Signal exists.");
                } else {
                    // fsw.storeSignalAndFile();
                }
            }
        } catch (CDBException ex) {
            Logger.getLogger(JyCDBTest.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
        
    }
        
    public static void instanceTest() {
        final Random randomGenerator = new Random();
        
        try {
            CDBClient client0 = CDBClient.getInstance();
            final long shot = client0.lastShotNumber();
            for (int i = 1; i <= 10000; i++) {
                try { Thread.sleep(randomGenerator.nextInt(10)); } catch(Exception e) { }
                Thread t = new Thread() {
                    @Override
                    public void run() {
                        // System.out.print(">");
                        long max = 10; //Thread.currentThread().getId();
                        try { CDBClient client = CDBClient.getInstance();
                            for (int j = 1; j <= max; j++) {
                                try { Thread.sleep(10); } catch(Exception e) { }
                                long shotNumber = client.lastShotNumber();
                                if (shotNumber != shot) {
                                    System.out.println();
                                    System.out.println("!!!!!!!!!!!!!!!!");
                                } else {
                                    System.out.print(".");
                                }
                            }
                        } catch( CDBException ex) {
                            System.out.println("X");
                        }
                    }
                };
                t.start();
            }
        } catch (CDBException ex) {
            System.out.println("Failed - no connection");
        }
    }
}
