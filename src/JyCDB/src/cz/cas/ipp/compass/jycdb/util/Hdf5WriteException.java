package cz.cas.ipp.compass.jycdb.util;

import cz.cas.ipp.compass.jycdb.CDBException;

/**
 * An error when writing to HDF5.
 * 
 * @author pipek
 */
public class Hdf5WriteException extends CDBException {
    public Hdf5WriteException(String message) {
        super(message);
    }
}
