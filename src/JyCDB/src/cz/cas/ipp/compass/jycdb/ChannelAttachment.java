package cz.cas.ipp.compass.jycdb;

import cz.cas.ipp.compass.jycdb.util.DictionaryAdapter;
import cz.cas.ipp.compass.jycdb.util.JsonUtils;
import org.python.core.PyObject;

public class ChannelAttachment extends DictionaryAdapter {
    private ChannelAttachment(PyObject object) {
        super(object);
    }
    
    public static ChannelAttachment create(PyObject object) {
        if (object.__nonzero__()) {
            return new ChannelAttachment(object);
        } else {
            return null;
        }
    }
    
    public double getCoefficientLev2V() {
        return get("coefficient_lev2V").asDouble();
    }
    
    public double getCoefficientV2Unit() {
        return get("coefficient_V2unit").asDouble();
    }
    
    public String getParametersString() {
        return get("parameters").asString();
    }
    
    public long getAttachedGenericSignalId() {
        return get("attached_generic_signal_id").asLong();
    }
    
    public Object getParameters() {
        return JsonUtils.parseNative(getParametersString());
    }
}
