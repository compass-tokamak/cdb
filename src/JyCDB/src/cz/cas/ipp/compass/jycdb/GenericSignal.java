package cz.cas.ipp.compass.jycdb;

import cz.cas.ipp.compass.jycdb.util.DictionaryAdapter;
import cz.cas.ipp.compass.jycdb.util.PythonUtils;
import org.python.core.PyObject;

/**
 * Wrapper around rows of `generic_signals` table.
 * 
 * @author honza
 */
public class GenericSignal extends DictionaryAdapter {    
    public static int MAX_AXES = 6;
    
    public static final String FILE = "FILE";
    public static final String LINEAR = "LINEAR";
    
    public static final String[] VALID_TYPES = { FILE, LINEAR };
    
    /**
     * Check if a signal type is valid.
     * 
     * Only "LINEAR" and "FILE" are accepted now.
     */
    public static boolean isValidType(String signalType) {
        for (int i = 0; i < VALID_TYPES.length; i++) {
            if (VALID_TYPES[i].equals(signalType)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isFile()
    {
        return getSignalType().equals(FILE);
    }
    
    public boolean isLinear()
    {
        return getSignalType().equals(LINEAR);
    }
    
    public static GenericSignal create(PyObject object) {
        if (object.__nonzero__()) {
            return new GenericSignal(object);
        } else {
            return null;
        }
    }
    
    private GenericSignal(PyObject object) {
        super(object);
    }
    
    public long getId() {
        return get("generic_signal_id").asLong();
    }
    
    public long getLastRecordNumber() {
        return get("last_record_number").asLong();
    }
    
    public long getDataSourceId() {
        return get("data_source_id").asLong();
    }
    
    public long getTimeAxisId() throws CDBException {
        return getAxisId(0);
    }
    
    /**
     * Get id of one of the axes (time or spatial).
     * 
     * @param index Number of the axis (starting with 1). If 0, time axis is returned.
     * @return 0 if there is no axis.
     * @throws CDBException 
     */
    public long getAxisId(int index) throws CDBException {
        if (index < 0 || index > MAX_AXES) {
            throw new CDBException("Invalid axis");
        }
        String axisName;
        if (index == 0) {
            axisName = "time_axis_id";
        } else {
            axisName = String.format("axis%d_id", index);
        }
        Long axisId = (Long)(PythonUtils.asNative(get(axisName)));
        if (axisId == null) return 0;
        return axisId;
    }
    
    public GenericSignal getAxis(int index) throws CDBException {
        long axisId = getAxisId(index);
        if (axisId > 0) {
            return CDBClient.getInstance().getGenericSignal(axisId);
        }
        return null;
    }
    
    public GenericSignal getTimeAxis() throws CDBException {
        long timeAxisId = getTimeAxisId();
        if (timeAxisId > 0) {
            return CDBClient.getInstance().getGenericSignal(timeAxisId);
        }
        return null;
    }    
    
    public String getUnits() {
        return get("units").asString();
    }
    
    public String getSignalType() {
        return get("signal_type").asString();
    }    
    
    public String getName() {
        return (String)PythonUtils.asNative(get("generic_signal_name"));
    }
    
    public String getAlias() {
        return (String)PythonUtils.asNative(get("alias"));
    }

    String getDescription() {
        return (String)PythonUtils.asNative(get("description"));
    }
}
