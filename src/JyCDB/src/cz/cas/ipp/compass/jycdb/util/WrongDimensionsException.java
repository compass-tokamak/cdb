package cz.cas.ipp.compass.jycdb.util;

import cz.cas.ipp.compass.jycdb.CDBException;

/**
 *
 * @author pipek
 */
public class WrongDimensionsException extends CDBException {
   public WrongDimensionsException(String message) {
       super(message);
   }      
}
