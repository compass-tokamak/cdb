package cz.cas.ipp.compass.jycdb;

/**
 * Signal type is not valid for its intended use.
 *
 * It can be either LINEAR or FILE with different operations available.
 * 
 * @author pipek
 */
public class WrongSignalTypeException extends CDBException {
   public WrongSignalTypeException(String message) {
       super(message);
   }    
}
