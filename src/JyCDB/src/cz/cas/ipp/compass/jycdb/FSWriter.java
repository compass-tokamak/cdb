package cz.cas.ipp.compass.jycdb;

import cz.cas.ipp.compass.jycdb.util.ParameterList;
import cz.cas.ipp.compass.jycdb.util.PythonAdapter;
import cz.cas.ipp.compass.jycdb.util.PythonUtils;
import org.python.util.PythonInterpreter;

/**
 * Java wrapper for quick storing of signal data to CDB in database controller.
 * 
 * See: fs_writer.py
 * 
 * Warning: This class is meant to be used only in FireSignal database controller!
 * Not elsewhere, even the nodes. Unexpected behaviour may result.
 * 
 * @author pipek
 */
public class FSWriter extends PythonAdapter {
    private static boolean moduleLoaded = false;
    
    public FSWriter(CDBClient client, String nodeId, String hardwareId, String parameterId, long recordNumber, double time0, double sampleLength) throws CDBConnectionException {
        super(null);
        // CDBClient client = CDBClient.getInstance();
        
        if (!moduleLoaded) {
            PythonInterpreter python = PythonUtils.getInterpreter();
            python.exec("from pyCDB.fs_writer import FSWriter");
            moduleLoaded = true;
        }
        ParameterList parameters = new ParameterList();
        parameters.put("cdb_client", client.getPythonObject());
        parameters.put("node_id", nodeId);
        parameters.put("hardware_id", hardwareId);
        parameters.put("parameter_id", parameterId);
        parameters.put("record_number", recordNumber);
        parameters.put("time0", time0);
        parameters.put("sample_length", sampleLength);
        
        pyObject = PythonUtils.newObject("FSWriter", parameters);
    }
    
    /**
     * 1st step - read all information we need for file storage.
     */
    public void readSignalInfo() {
        invoke("read_signal_info");
    }
    
    /**
     * 2nd step - write all that is to be written to database.
     * 
     * This step should be undertaken only if signal does not exist.
     */
    public void storeSignalAndFile() {
        invoke("store_signal_and_file");
    }
    
    /**
     * Get the full path where to write the file.
     */
    public String getFilePath() {
        return getAttribute("file_path").asString();
    }
    
    /**
     * Get the name of the dataset to write to.
     */
    public String getFileKey() {
        return getAttribute("data_file_key").asString();
    }
    
    /**
     * Is the signal already stored in the database?
     */
    public boolean signalExists() {
        return (getAttribute("signal_exists").asInt() > 0);
    }
}
