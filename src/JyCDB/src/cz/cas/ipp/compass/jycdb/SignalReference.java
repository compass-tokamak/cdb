package cz.cas.ipp.compass.jycdb;

import cz.cas.ipp.compass.jycdb.util.*;
import org.python.core.PyObject;

/**
 * Wrapper around rows of `data_signals` table.
 * 
 * More sophisticated implementation of CDBSignal is in Signal.
 */
public class SignalReference extends DictionaryAdapter {    
    private GenericSignal genericSignal = null;
        
    public static SignalReference create(PyObject object) {
        if (object.__nonzero__()) {
            return new SignalReference(object);
        } else {
            return null;
        }
    } 
        
    private SignalReference(PyObject object) {
        super(object);
    }    
    
    public long getComputerId() {
        return get("computer_id").asLong();
    }
    
    public long getBoardId() {
        return get("board_id").asLong();
    }
    
    public long getChannelId() {
        return get("channel_id").asLong();
    }
    
    public long getDataFileId() {
        return get("data_file_id").asLong();
    }
    
    public long getRecordNumber() {
        return get("record_number").asLong();
    }
    
    public long getGenericSignalId() {
        return get("generic_signal_id").asLong();
    }
    
    public String getDataFileKey() {
        return get("data_file_key").asString();
    }
    
    public Boolean isFile() throws CDBException {
        return getSignalType().equals(GenericSignal.FILE);
    }
    
    public Boolean isLinear() throws CDBException {
        return getSignalType().equals(GenericSignal.LINEAR);
    }
    
    public String getSignalType() throws CDBException {
        return getGenericSignal().getSignalType();
    }
    
    public long getTimeAxisRevision() {
        return getAxisRevision(0);
    }
    
    public long getAxisRevision(Integer axis) {
        String key;
        if (axis == 0) {
            key = "time_axis_revision";
        } else {
            key = "axis" + axis.toString() + "_revision";
        }
        return get(key).asLong();
    }
    
    public synchronized long getAxisId(int i) throws CDBException {
        if (i == 0) {
            return getTimeAxisId();
        } else {
            return getGenericSignal().getAxisId(i);
        }
    }

    public long getTimeAxisId() throws CDBException {
        long in_data_signal = get("time_axis_id").asLong();
        if (in_data_signal > 0) {
            return in_data_signal;
        } else {
            return getGenericSignal().getTimeAxisId();
        }
    }        
    
    public double getTime0() {
        return get("time0").asDouble();
    }
    
    public SignalCalibration getCalibration() throws CDBException {
        ParameterList parameters = new ParameterList();
        parameters.put("sig_ref", pyObject);
        return CDBClient.getInstance().getSignalCalibration(parameters);
    }
    
    public double getCoefficient() {
        return get("coefficient").asDouble();     
    }
    
    public double getCoefficientV2unit() {
        return get("coefficient_V2unit").asDouble();
    }
    
    public double getOffset() {
        return get("offset").asDouble();
    }
    
    public String getNote() {
        return get("note").asString();
    }
    
    public long getRevision() {
        return get("revision").asLong();
    }
    
    public String getVariant() {
        return get("variant").asString();
    }
    
    public GenericSignal getGenericSignal() throws CDBException {
        if (genericSignal == null) {
            genericSignal = CDBClient.getInstance().getGenericSignal(getGenericSignalId());
        }
        return genericSignal;
    }
        
    public static String createCDBStrid(String genericSignalNameOrAlias) {
        return genericSignalNameOrAlias;
    }
    
    public static String createCDBStrid(String genericSignalNameOrAlias, long recordNumber) {
        StringBuilder sb = new StringBuilder();
        sb.append(genericSignalNameOrAlias).append(":").append(recordNumber);
        return sb.toString();
    }
    
    public static String createCDBStrid(String genericSignalNameOrAlias, long recordNumber, long revision) {
        StringBuilder sb = new StringBuilder();
        sb.append(genericSignalNameOrAlias).append(":").append(recordNumber).append(":").append(revision);
        return sb.toString();
    }    
    
    public static String createCDBStrid(String genericSignalNameOrAlias, long recordNumber, long revision, String variant) {
        StringBuilder sb = new StringBuilder();
        sb.append(genericSignalNameOrAlias).append(":").append(recordNumber).append(":").append(revision).append(":").append(variant);
        return sb.toString();
    }  
    
    public static String createCDBStrid(String genericSignalNameOrAlias, long recordNumber, String variant) {
        StringBuilder sb = new StringBuilder();
        sb.append(genericSignalNameOrAlias).append(":").append(recordNumber).append(":").append(variant);
        return sb.toString();
    }      
    
    public static String createFSStrid(String nodeId, String hardwareId, String parameterId) {
        StringBuilder sb = new StringBuilder();
        sb.append("FS:").append(nodeId).append(":").append(hardwareId).append(":").append(parameterId);
        return sb.toString();
    }
    
    public static String createFSStrid(String nodeId, String hardwareId, String parameterId, long recordNumber) {
        StringBuilder sb = new StringBuilder();
        sb.append("FS:").append(nodeId).append(":").append(hardwareId).append(":").append(parameterId).append(":").append(recordNumber);
        return sb.toString();
    }
    
    public static String createFSStrid(String nodeId, String hardwareId, String parameterId, long recordNumber, long revision) {
        StringBuilder sb = new StringBuilder();
        sb.append("FS:").append(nodeId).append(":").append(hardwareId).append(":").append(parameterId).append(":").append(recordNumber).append(":").append(revision);
        return sb.toString();
    }  
}
