package cz.cas.ipp.compass.jycdb.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Utility to read and write data from/to JSON format.
 * 
 * @author pipek
 */
public class JsonUtils {   
    /**
     * Take JSON string and turn it into native objects.
     */
    public static Object parseNative(String source) {
        if (source == null || source.isEmpty()) {
            return null;
        }
        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(source);
        return asNative(element);
    }
    
    /**
     * Transform JSON element into native object.
     * 
     * Works recursively:
     * - double, boolean, string - native
     * - arrays =&gt; Object[]
     * - objects =&gt; TreeMap
     */
    public static Object asNative(JsonElement element)
    {
        if (element.isJsonNull()) {
            return null;
        } else if (element.isJsonPrimitive()) {
            JsonPrimitive primitive = (JsonPrimitive)element;
            if (primitive.isNumber()) {
                return primitive.getAsDouble();
            } else if (primitive.isBoolean()) {
                return primitive.getAsBoolean();
            } else { // (primitive.isString()) {
                return primitive.getAsString();
            }
        } else if (element.isJsonArray()) {
            JsonArray jsonArray = (JsonArray)element;
            Object[] array = new Object[jsonArray.size()];
            for (int i = 0; i < jsonArray.size(); i++) {
                array[i] = asNative(jsonArray.get(i));
            }
            return array;
        } else { // if (element.isJsonObject()) {
            JsonObject object = (JsonObject)element;
            SortedMap map = new TreeMap();
            for (Map.Entry<String, JsonElement> pair : object.entrySet()) {
                String key = pair.getKey();
                Object value = asNative(pair.getValue());
                map.put(key, value);
            }
            return map;
        }
    }
}
