package cz.cas.ipp.compass.jycdb.util;

import java.util.Map;
import org.python.core.Py;
import org.python.core.PyException;
import org.python.core.PyObject;

/**
 * Adapter of a PyObject that offers some shortcut methods for invoking etc.
 * 
 * Motivation: The Jython API is not as elegant as would be desirable.
 * However, with proper understanding of PyObject, this class would not be necessary.
 * 
 * @author pipek
 */
public class PythonAdapter {
    protected PyObject pyObject;
    
    public PythonAdapter(PyObject object) {
        pyObject = object;
    }
    
    /**
     * Call a method on PyObject with parameters.
     */
    public PyObject invoke(String method, Map<String,PyObject> parameters) {
        return PythonUtils.invoke(getPythonObject(), method, parameters);
    }    
    
    /**
     * Call a method on PyObject without parameters.
     * 
     * @param method Name of the method (callable attribute) to call on the object.
     * @return Result of the call as PyObject
     */
    public PyObject invoke(String method) {
        return getPythonObject().invoke(method);
    }
    
    /**
     * Get the unwrapped PyObject.
     */
    public PyObject getPythonObject() {
        return pyObject;
    }
    
    /**
     * Get the attribute of PyObject.
     * 
     * @param name Name of the attribute.
     * @return The attribute as PyObject or null (if attribute not present).
     */
    public PyObject getAttribute(String name) {
        try {
            return getPythonObject().__getattr__(name);
        } catch (PyException exc) {
            if (exc.match(Py.AttributeError)) {
                return null;
            } else {
                throw exc;
            }
        }
    } 
    
    public boolean hasAttribute(String name) {
        return getPythonObject().__findattr__(name) != null;
    }
}
