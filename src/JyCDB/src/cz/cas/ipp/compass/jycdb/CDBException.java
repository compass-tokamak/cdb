package cz.cas.ipp.compass.jycdb;

/**
 * Some problem with CDB.
 * 
 * Common parent for more specific exception classes.
 * 
 * @author pipek
 */
public class CDBException extends Exception {
   public CDBException(String message) {
       super(message);
   }    
}
