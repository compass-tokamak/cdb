package cz.cas.ipp.compass.jycdb;

import cz.cas.ipp.compass.jycdb.util.*;
import java.util.SortedMap;
import java.util.TreeMap;
import org.python.core.PyObject;

/**
 * Class mimicking the CDBSignal Python class.
 *
 * In a few aspects, it is more object-oriented and lazy-evaluated
 * to simplify manipulation in MATLAB/IDL.
 * 
 * @author pipek
 */
public class Signal extends PythonAdapter
{
    private Signal parent = null;
    private int nthChild = 0;

    private SignalReference signalReference;

    private PythonAdapter pySignalTree = null;

    private Data rawData = null;
    private Data davData = null;
    private Data defaultData = null;
    private Data data = null;

    private DictionaryAdapter unitFactorTree = null;

    private SortedMap<String, Signal> axes = null;

    private String units;

    private static final String[] axisNames = { "time_axis", "axis1", "axis2", "axis3", "axis4", "axis5", "axis6" };

    public Signal(SignalReference signalRef, String units) {
        super(null);
        this.signalReference = signalRef;
        this.units = units;
    }

    /**
     * Constructor for dependent objects from signal tree with unit factor.
     *
     * @param parent  Signal that uses this as an axis.
     * @param nthChild The order of this signal among axes in the parent signal.
     * @param signalTree Signal subtree.
     * @param unitFactorTree Unit factor subtree.
     */
    protected Signal(Signal parent, int nthChild, PythonAdapter signalTree, DictionaryAdapter unitFactorTree) {
        super(null);
        this.parent = parent;
        this.nthChild = nthChild;

        this.pySignalTree = signalTree;
        this.unitFactorTree = unitFactorTree;
        this.units = unitFactorTree.getDictValueAsString("units"); // May as well be null
        this.signalReference = SignalReference.create(this.pySignalTree.getAttribute("ref"));
    }
    
    /**
     * Constructor for dependent objects from signal tree without unit factor.
     * 
     * @param parent  Signal that uses this as an axis.
     * @param nthChild The order of this signal among axes in the parent signal.
     * @param signalTree Signal subtree.
     * @param units Most times, this will be "default", but any other value can used.
     */
    protected Signal(Signal parent, int nthChild, PythonAdapter signalTree, String units) {
        super(null);
        this.parent = parent;
        this.nthChild = nthChild;
        
        this.pySignalTree = signalTree;
        this.units = units;
        this.signalReference = SignalReference.create(this.pySignalTree.getAttribute("ref"));
    }

    public SignalReference getSignalReference() {
        return signalReference;
    }

    public GenericSignal getGenericSignalReference() throws CDBException {
        return signalReference.getGenericSignal();
    }

    public DataFile getDataFile() throws CDBException {
        return CDBClient.getInstance().getDataFile(getSignalReference().getDataFileId());
    }

    public SignalParameters getParameters() throws CDBException {
        ParameterList args = new ParameterList();
        SignalReference ref = getSignalReference();
        args.put("generic_signal_id", ref.getGenericSignalId());
        args.put("record_number", ref.getRecordNumber());
        args.put("revision", ref.getRevision());
        args.put("variant", ref.getVariant());
        return CDBClient.getInstance().getSignalParameters(args);
    }

    public long getGenericSignalId() throws CDBException {
        return getSignalReference().getGenericSignalId();
    }
    
    public long getRecordNumber() throws CDBException {
        return getSignalReference().getRecordNumber();
    }
    
    public GenericSignal getGenericSignal() throws CDBException {
        return getSignalReference().getGenericSignal();
    }
    
    public long getDataFileId() {
        return getSignalReference().getDataFileId();
    }
    
    /**
     * Generic signal name.
     */
    public String getName() throws CDBException {
        return getGenericSignalReference().getName();
    }

    /**
     * Generic signal description.
     */
    public String getDescription() throws CDBException {
        return getGenericSignalReference().getDescription();
    }

    public boolean isFile() throws CDBException {
        return getGenericSignalReference().isFile();
    }

    public boolean isLinear() throws CDBException {
        return getGenericSignalReference().isLinear();
    }
    
    public boolean isRaw() {
        return units.toLowerCase().equals("raw");
    }
    
    public boolean isDAV() {
        return units.toLowerCase().equals("dav");
    }   

    /**
     * Whether the signal has associated unit factor tree.
     * 
     * This is true only if unit or unit system were requested.
     */
    public boolean hasUnitFactor() {
        String unit = units.toLowerCase();
        return !("raw".equals(unit) || "dav".equals(unit) || "default".equals(unit));
    }
    
    private PythonAdapter getPySignalTree() throws CDBException {
        if (pySignalTree == null) {
            CDBClient client = CDBClient.getInstance();
            ParameterList parameters = new ParameterList();
            parameters.put("signal_ref", signalReference.getPythonObject());
            pySignalTree = new PythonAdapter(client.invoke("get_signal_base_tree", parameters));
        }
        return pySignalTree;
    }

    /**
     * Return the Python object of CDBSignal class.
     *
     * @return null if CDBSignal not found.
     *
     * Note: This method catches exceptions thus hiding problems.
     */
    @Override
    public PyObject getPythonObject() {
        try {
            return getPySignalTree().getPythonObject();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * A map of all existing axes.
     *
     * @return A dictionary of [ axis_name, Signal object ] values.
     * @throws CDBException
     *
     * Non-existent axes are omitted.
     */
    public SortedMap<String, Signal> getAxes() throws CDBException {
        PythonAdapter tree = getPySignalTree();
        int order = 0;
        if (axes == null) {
            axes = new TreeMap<String, Signal>();
            for (String axisName : axisNames) { 
                PyObject axisValue = tree.getAttribute(axisName);
                if (axisValue != null && axisValue.__nonzero__()) {
                    PythonAdapter axis = new PythonAdapter(axisValue);
                    Signal axisSignal;
                    if (hasUnitFactor()) {
                        DictionaryAdapter axisFactorTree = new DictionaryAdapter(getUnitFactorTree().get(axisName));
                        axisSignal = new Signal(this, order, axis, axisFactorTree);
                    } else {
                        axisSignal = new Signal(this, order, axis, "default");
                    }
                    axes.put(axisName, axisSignal);
                    order++;
                }
            }
        }
        return axes;
    }

    /**
     * An axis as Signal instance.
     *
     * @param i 0-time axis, N-axisN
     *
     * @return null if not found
     * @throws CDBException
     */
    public Signal getAxis(int i) throws CDBException {
        if (i < 0 || i >= axisNames.length) {
            throw new CDBException("Invalid axis number.");
        }
        return getAxes().get(axisNames[i]);
    }
    
    /**
     * Time axis as Signal instance.
     * 
     * Just shortcut to getAxis method.
     */
    public Signal getTimeAxis() throws CDBException {
        return getAxis(0);
    }

    private boolean hasTimeAxis() throws CDBException {
        return (signalReference.getTimeAxisId() != 0);
    }

    /**
     * Unit factor, by which the default signal has to be multiplied.
     *
     * @throws CDBException
     *
     * If DAV/RAW/default are selected, this factor is 1.0.
     *
     * If another unit/unit system is selected, this is a conversion
     * from the default unit to the selected one.
     *
     * Internally, this reflects unit_factor_tree returned from Python.
     */
    public double getUnitFactor() throws CDBException {
        if (hasUnitFactor()) {
            return getUnitFactorTree().getDictValueAsDouble("factor");
        } else {
            return 1.0;
        }
    }

    /**
     * Unit name.
     *
     * @throws CDBException
     *
     * RAW/DAQ/default GS units or units from factor tree (if requested).
     */
    public String getUnit() throws CDBException {
        String unit = units.toLowerCase();
        if ("raw".equals(unit)) {
            return SignalCalibration.RAW;
        } else if ("dav".equals(unit)) {
            return SignalCalibration.DAQ_VOLTS;
        } else if ("default".equals(unit)) {
            return getGenericSignalReference().getUnits();
        } else {
            return getUnitFactorTree().getDictValueAsString("units");
        }
    }

    /**
     * Signal calibration for default/DAV/RAW.
     *
     * @return
     * @throws CDBException
     */
    public SignalCalibration getSignalCalibration(boolean includeUnitFactor) throws CDBException {
        if (!getGenericSignalReference().getSignalType().equals(GenericSignal.FILE)) {
            throw new CDBException("Only FILE signals have meaningful calibration.");
        }
        ParameterList parameters = new ParameterList();
        parameters.put("sig_ref", signalReference.getPythonObject());
        if (hasUnitFactor() && !includeUnitFactor) {
            parameters.put("units", "default");
        } else {
            parameters.put("units", units);
        }
        parameters.put("gs_ref", getGenericSignalReference().getPythonObject());
        return CDBClient.getInstance().getSignalCalibration(parameters);
    }

    private DictionaryAdapter getUnitFactorTree() throws CDBException {
        if (unitFactorTree == null) {
            CDBClient client = CDBClient.getInstance();
            ParameterList parameters = new ParameterList();
            parameters.put("signal", getPySignalTree().getPythonObject());
            parameters.put("units", units);
            unitFactorTree = new DictionaryAdapter(client.invoke("get_unit_factor_tree", parameters));
        }
        return unitFactorTree;
    }

    /**
      * Get raw data as they are saved in the database.
      */
    private synchronized Data getRawData() throws CDBException {
        if (isLinear()) {
            throw new CDBException("Cannot get raw data for a linear signal.");
        }
        if (rawData == null) {
            if (getGenericSignalReference().isFile()) {
                DataFile dataFile = getDataFile();
                rawData = Hdf5Utils.readFileData(dataFile.getFullPath(), signalReference.getDataFileKey());
                rawData.flatten(); // TODO: Can be removed.
            } else {
                throw new WrongSignalTypeException("Cannot get raw data for linear signal.");
            }
        }
        return rawData;
    }

    private synchronized Data getDavData() throws CDBException {
        if (isLinear()) {
            throw new CDBException("Cannot get DAV data for a linear signal.");
        }
        if (davData == null) {
            double mul = signalReference.getCoefficient();
            double add = signalReference.getOffset() / mul;
            davData = getRawData().linearTransform(mul, add);
        }
        return davData;
    }

    private synchronized Data getDefaultData() throws CDBException {
        if (defaultData == null) {
            if (isFile()) {
                double mul = signalReference.getCoefficient() * signalReference.getCoefficientV2unit();
                double add = signalReference.getOffset() / mul;
                defaultData = getRawData().linearTransform(mul, add);
            } else if (isLinear()) {
                if (parent != null && parent.isFile()) {
                    double[] range = ArrayUtils.asDoubleArray(
                        ArrayUtils.range(0, (int)parent.getRawData().getDimensions()[0])
                    );
                    double offset = getSignalReference().getOffset();
                    if (nthChild == 0 && parent.hasTimeAxis()) {
                        offset = parent.getSignalReference().getTime0();
                    }
                    double[] array = ArrayUtils.linearTransform(
                        range,
                        offset,
                        getSignalReference().getCoefficient()
                    );
                    defaultData = new Data(array);
                } else {
                    // TODO: Implement
                    throw new CDBException("Not yet implemented: multi-dependent linear signals.");
                }
            }
        }
        return defaultData;
    }

    /**
     * Numerical data of the signal.
     * 
     * Note: this method is not used in bindings using JyCDB (IDL/MATLAB) due to efficiency.
     *   Try using native implementations of file reading.
     */
    public synchronized Data getData() throws CDBException  {
        if (data == null) {
            String unit = units.toLowerCase();
            if ("raw".equals(units)) {
                data = rawData;
            } else if ("dav".equals(units)) {
                data = getDavData();
            } else if ("default".equals(units)) {
                data = getDefaultData();
            } else {
                double factor = getUnitFactor();
                if (factor == 1.0) {
                    data = getDefaultData();
                } else {
                    data = getDefaultData().linearTransform(factor, 0.0);
                }
            }
        }
        return data;
    }
}
