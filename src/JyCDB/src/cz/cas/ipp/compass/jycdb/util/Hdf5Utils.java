package cz.cas.ipp.compass.jycdb.util;

import java.io.File;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import ncsa.hdf.hdf5lib.H5;
import ncsa.hdf.hdf5lib.HDF5Constants;
import ncsa.hdf.hdf5lib.exceptions.HDF5Exception;
import ncsa.hdf.hdf5lib.exceptions.HDF5LibraryException;
import ncsa.hdf.object.Dataset;
import ncsa.hdf.object.Datatype;
import ncsa.hdf.object.h5.H5File;

/**
 * Utilities for reading/writing HDF5 files.
 * 
 * Methods readData, writeData and writeData
 * 
 * @author pipek
 */
public class Hdf5Utils {
    /**
     * Convert HDF5 data type to dataType used in Data class.
     */
    private static int hdf5toInternalDataType(Datatype datatype) throws UnknownDataTypeException {
        if (datatype.getDatatypeClass() == Datatype.CLASS_ARRAY) {
            datatype = datatype.getBasetype();
        }

        switch(datatype.getDatatypeClass()) {
            case Datatype.CLASS_INTEGER:
                switch (datatype.getDatatypeSize()) {
                    case 1:
                        return Data.INT8;
                    case 2:
                        return Data.INT16;
                    case 4:
                        return Data.INT32;
                    case 8:
                        return Data.INT64;
                }
            case Datatype.CLASS_FLOAT:
                switch (datatype.getDatatypeSize()) {
                    case 4:
                        return Data.FLOAT;
                    case 8:
                        return Data.DOUBLE;
                }

        }
        throw new UnknownDataTypeException("Unknown HDF5 data type: " + datatype.getDatatypeDescription());
    }
    
    /**
     * Convert dataType used in Data class to HDF5 data type.
     */    
    private static int internalToHdf5DataType(int type) throws UnknownDataTypeException {
        try {
            switch (type) {
                case Data.INT8:
                    return H5.H5Tcopy(HDF5Constants.H5T_NATIVE_INT8);
                case Data.INT16:
                    return H5.H5Tcopy(HDF5Constants.H5T_NATIVE_INT16);
                case Data.INT32:
                    return H5.H5Tcopy(HDF5Constants.H5T_NATIVE_INT32);
                case Data.INT64:
                    return H5.H5Tcopy(HDF5Constants.H5T_NATIVE_INT64);
                case Data.FLOAT:
                    return H5.H5Tcopy(HDF5Constants.H5T_NATIVE_FLOAT);
                case Data.DOUBLE:
                    return H5.H5Tcopy(HDF5Constants.H5T_NATIVE_DOUBLE);
            }
            throw new UnknownDataTypeException("Unknown data type.");
        } catch (HDF5LibraryException exc) {
            throw new UnknownDataTypeException("Error translating HDF5 data type.");
        }
    }
    
    /**
     * Read data from file.
     * 
     * Up to 3-D arrays are supported.
     * (Higher rank would require reimplementation of Data class)
     * 
     * @param filePath Path to the file.
     * @param fileKey Name of the dataset in the file.
     */
    public static Data readFileData(String filePath, String fileKey) throws UnknownDataTypeException, Hdf5ReadException, WrongDimensionsException {
        H5File file = new H5File(filePath, HDF5Constants.H5F_ACC_RDONLY);
        
        Dataset dataset = null;
        Object readData = null;
        
        try {
            dataset = (Dataset)file.get(fileKey);
            dataset.read();
        }
        catch (Exception ex) {
            // Alas, Hdf5 declares Exception
            throw new Hdf5ReadException(ex.getMessage());
        }
        int internalDataType = hdf5toInternalDataType(dataset.getDatatype());
        long[] dimensions = dataset.getDims();    
        
        if (dimensions.length > 3) {
            throw new WrongDimensionsException("Doesn't understand data with rank > 3.");
        }
        
        // Select that we want everything (this must be done for rank = 3)
        long[] selectedDims = dataset.getSelectedDims();
        System.arraycopy(dimensions, 0, selectedDims, 0, selectedDims.length);
                
        try {
            readData = dataset.getData();
        } catch (Exception ex) {
            throw new Hdf5ReadException(ex.getMessage());
        }
        return Data.from1DArray(readData, internalDataType, dimensions);
    }
    
    /**
     * Write (from beginning) new HDF5 dataset.
     * 
     * @param filePath Path to the file (may or may not exist).
     * @param fileKey Name of the dataset (error if it exists).
     * @param data Wrapped data (see class Data).
     */
    public static void writeData(String filePath, String fileKey, Data data) throws Hdf5WriteException, UnknownDataTypeException {
        writeData(filePath, fileKey, data, null);
    }
    
    /**
     * Write (or append) data to an existing HDF5 dataset.
     * 
     * @param filePath Path to the file.
     * @param fileKey Name of the (existing) dataset.
     * @param data Wrapped data (see class Data).
     * @param offset Offset (in dimensions) from which start with writing data.
     *    If offset == null, new data set is created.
     * 
     * You have to be sure that your offset is correct. The dataset is enlarged if possible
     * but you can overwrite existing data if you are not careful.
     */
    public static void writeData(String filePath, String fileKey, Data data, long[] offset) throws UnknownDataTypeException, Hdf5WriteException {
        long[] dimensions = data.getDimensions();
        int dataType = internalToHdf5DataType(data.getType());
        writeData(filePath, fileKey, dataType, dimensions, data.getRawData(), offset);
    }   
    
    /**
     * Write data to a HDF5 dataset (generic version).
     * 
     * This generic version is available as a public method, still it is more convenient
     * (and practical) to use writeData(String, String, Data) or writeData.
     * 
     * @param filePath Path to the file (may or may not exist).
     * @param fileKey Name of the dataset.
     * @param dataType HDF5 data type.
     * @param dimensions The dimensions of the data array.
     * @param rawData The data array of any format.
     * @param offset Offset of data (null =&gt; create new dataset)
     */
    public static void writeData(String filePath, String fileKey, int dataType, long[] dimensions, Object rawData, long[] offset) throws Hdf5WriteException {
        File file = new File(filePath);
        
        int fileId;
        int dataSetId;
        int dataSpaceId;
        
        // Set unlimited maximum dimensions for possible appending
        long[] maxDimensions = new long[dimensions.length];
        Arrays.fill(maxDimensions, HDF5Constants.H5S_UNLIMITED);
        
        try {
            if (offset == null)
            {
                // Creating new file (if necessary) and dataset
                File directory = file.getParentFile();
                if (directory != null && !directory.exists()) {
                    directory.mkdirs(); // Ensure the existence of directory
                }
                if (file.exists()) {
                    fileId = H5.H5Fopen(filePath, HDF5Constants.H5F_ACC_RDWR, HDF5Constants.H5P_DEFAULT);
                    dataSetId = H5.H5Dopen(fileId, fileKey, HDF5Constants.H5P_DEFAULT);
                    if (dataSetId >= 0) {
                        throw new Hdf5WriteException("Data set '" + fileKey + "' already exists in '" + filePath + "'.");
                    }
                } else {
                    fileId = H5.H5Fcreate(filePath, HDF5Constants.H5F_ACC_TRUNC, HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT);
                }
                dataSpaceId = H5.H5Screate_simple(dimensions.length, dimensions, maxDimensions);

                // Set chunking
                int dcplId = H5.H5Pcreate(HDF5Constants.H5P_DATASET_CREATE);
                H5.H5Pset_chunk(dcplId, dimensions.length, dimensions);          // ERROR ???

                dataSetId = H5.H5Dcreate(fileId, fileKey, dataType, dataSpaceId, HDF5Constants.H5P_DEFAULT, dcplId, HDF5Constants.H5P_DEFAULT);        

                H5.H5Dwrite(dataSetId, dataType, HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL, HDF5Constants.H5P_DEFAULT, rawData);
                H5.H5Tclose(dataType);
                
            } else {
                // Appending to an existing dataset.
                if (!file.exists()) {
                    throw new Hdf5WriteException("Cannot append to non-existent file '" + filePath + "'");
                }
                fileId = H5.H5Fopen(filePath, HDF5Constants.H5F_ACC_RDWR, HDF5Constants.H5P_DEFAULT);
                dataSetId = H5.H5Dopen(fileId, fileKey, HDF5Constants.H5P_DEFAULT);
                if (dataSetId < 0) {
                    throw new Hdf5WriteException("Data set '" + fileKey + "' does not exist in '" + filePath + "' (cannot append data).");
                }
                
                int originalFileSpaceId = H5.H5Dget_space(dataSetId);
                long[] originalDimensions = new long[dimensions.length];
                H5.H5Sget_simple_extent_dims(originalFileSpaceId, originalDimensions, null);
                H5.H5Sclose(originalFileSpaceId);
                
                long[] newDimensions = new long[dimensions.length];
                for (int i = 0; i < dimensions.length; i++) {
                    // Don't allow to shrink data set.
                    newDimensions[i] = Math.max(originalDimensions[i], offset[i] + dimensions[i]);
                }
                H5.H5Dset_extent(dataSetId, newDimensions);
                
                int fileSpaceId = H5.H5Dget_space(dataSetId);
                
                dataSpaceId = H5.H5Screate_simple(dimensions.length, dimensions, null);
                H5.H5Sselect_hyperslab(fileSpaceId, HDF5Constants.H5S_SELECT_SET, offset, null, dimensions, null);
                H5.H5Dwrite(dataSetId, dataType, dataSpaceId, fileSpaceId, HDF5Constants.H5P_DEFAULT, rawData);
                H5.H5Sclose(fileSpaceId);
            }
            H5.H5Dclose(dataSetId);
            H5.H5Sclose(dataSpaceId);           
            H5.H5Fclose(fileId);
        } catch (HDF5Exception exception) {
            throw new Hdf5WriteException(exception.getMessage());
        }
    }
}
