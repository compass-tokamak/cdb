package cz.cas.ipp.compass.jycdb.util;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import org.python.core.Py;
import org.python.core.PyDictionary;
import org.python.core.PyObject;

/**
 * List of parameters for python methods that accepts (via put method) some of the basic
 * types in addition to default PyObject.
 * 
 * Various put methods just simplify adding of native objects.
 */
public class ParameterList extends HashMap<String,PyObject> {  
    public String put(String key, String value) {
        if (value == null) {
            put(key, Py.None);
        } else {
            put(key, Py.newString(value));
        }
        return value;
    }
    
    public long put(String key, long value) {
        put(key, Py.newLong(value));
        return value;
    }
    
    public int put(String key, int value) {
        put(key, Py.newInteger(value));
        return value;
    }
    
    public Date put(String key, java.sql.Date value) {
        put(key, Py.newDate(value));
        return value;
    }
    
    public boolean put(String key, boolean value) {
        put(key, Py.newBoolean(value));
        return value;
    }
    
    public double put(String key, double value) {
        put(key, Py.newFloat(value));
        return value;
    }
    
    public Timestamp put(String key, Timestamp value) {
        put(key, Py.newDatetime(value));
        return value;
    }
    
    public short put(String key, short value) {
        put(key, Py.newInteger(value));
        return value;
    }
    
    public ParameterList put(String key, ParameterList value) {
        PyDictionary nested = new PyDictionary();
        for (Map.Entry pair : value.entrySet()) {
            nested.put(pair.getKey(),pair.getValue());
        }
        put(key, nested);
        return value;
    }

}
