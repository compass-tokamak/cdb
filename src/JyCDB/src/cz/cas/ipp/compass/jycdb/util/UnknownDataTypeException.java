package cz.cas.ipp.compass.jycdb.util;

import cz.cas.ipp.compass.jycdb.CDBException;

/** 
 * @author pipek
 */
public class UnknownDataTypeException extends CDBException {
   public UnknownDataTypeException(String message) {
       super(message);
   }    
}
