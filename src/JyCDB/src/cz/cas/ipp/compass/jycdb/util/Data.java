package cz.cas.ipp.compass.jycdb.util;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Wrapper around data.
 * 
 * It enables us to live with just one copy of most methods and pass data around
 * independently of its dimension and numerical type.
 * 
 * It can be constructed using:
 *  1) one of the 6x3 constructors with explicit array types
 *  2) a general constructor using Object and dimensionality data specified by you.
 * 
 * @author pipek
 * 
 */
public class Data {
    // TODO: Implementation works only for int16
    public static final int MAX_RANK = 3;
    
    // Definitions of types
    public static final int INT8 = 1;
    public static final int INT16 = 2;
    public static final int INT32 = 3;
    public static final int INT64 = 4;
    
    public static final int FLOAT = 101;
    public static final int DOUBLE = 102;
    
    
    private int type;
    private long[] dimensions;
    private Object storage;
 
    public Data(byte[] data) {
        type = INT8;
        storage = data;
        dimensions = new long[] { data.length };
    }
    
    public Data(byte[][] data) {
        type = INT8;
        storage = data;
        dimensions = new long[] { data.length, data[0].length };
    }
    
    public Data(byte[][][] data) {
        type = INT8;
        storage = data;
        dimensions = new long[] { data.length, data[0].length, data[0][0].length };        
    }     
    
    public Data(short[] data) {
        type = INT16;
        storage = data;
        dimensions = new long[] { data.length };
    }
    
    public Data(short[][] data) {
        type = INT16;
        storage = data;
        dimensions = new long[] { data.length, data[0].length };
    }
    
    public Data(short[][][] data) {
        type = INT16;
        storage = data;
        dimensions = new long[] { data.length, data[0].length, data[0][0].length };        
    } 
    
    public Data(int[] data) {
        type = INT32;
        storage = data;
        dimensions = new long[] { data.length };
    }
    
    public Data(int[][] data) {
        type = INT32;
        storage = data;
        dimensions = new long[] { data.length, data[0].length };
    }
    
    public Data(int[][][] data) {
        type = INT32;
        storage = data;
        dimensions = new long[] { data.length, data[0].length, data[0][0].length };        
    }
    
    public Data(long[] data) {
        type = INT64;
        storage = data;
        dimensions = new long[] { data.length };
    }
    
    public Data(long[][] data) {
        type = INT64;
        storage = data;
        dimensions = new long[] { data.length, data[0].length };
    }
    
    public Data(long[][][] data) {
        type = INT64;
        storage = data;
        dimensions = new long[] { data.length, data[0].length, data[0][0].length };        
    }      
    
    public Data(double[] data) {
        type = DOUBLE;
        storage = data;
        dimensions = new long[] { data.length };
    }
    
    public Data(double[][] data) {
        type = DOUBLE;
        storage = data;
        dimensions = new long[] { data.length, data[0].length };
    }
    
    public Data(double[][][] data) {
        type = DOUBLE;
        storage = data;
        dimensions = new long[] { data.length, data[0].length, data[0][0].length };       
    }
    
    public Data(float[] data) {
        type = FLOAT;
        storage = data;
        dimensions = new long[] { data.length };
    }
    
    public Data(float[][] data) {
        type = FLOAT;
        storage = data;    
        dimensions = new long[] { data.length, data[0].length };
    }
    
    public Data(float[][][] data) {
        type = FLOAT;
        storage = data;    
        dimensions = new long[] { data.length, data[0].length, data[0][0].length };      
    }
    
    /**
     * Generic constructor.
     * 
     * @param data The correct array object (or null)
     * @param type Type of data in terms of this class constants.
     * @param dimensions Length along all dimensions. Gets copied.
     * 
     * This is useful if we obtain data from external source, we know its properties
     * but we don't want to cast them unnecessarily (like read from HDF5).
     * 
     * Data needn't be specified (in such case a new array is created.)
     */
    public Data(Object data, int type, long[] dimensions) throws WrongDimensionsException
    {        
        // TODO: Reflection enables guessing of the two remaining parameters.
        this.type = type;
        this.dimensions = Arrays.copyOf(dimensions, dimensions.length);
        if (data != null) {
            storage = data;
        } else {
            storage = createRawObject(type, dimensions);
        }
    }
    
    /**
     * Create a multidimensional array object of a selected type and dimensions.
     */
    public static Object createRawObject(int type, long[] dimensions) throws WrongDimensionsException
    {
        // TODO: Could be moved to ArrayUtils?
        if (dimensions.length == 0 || dimensions.length > MAX_RANK) {
            throw new WrongDimensionsException("Invalid rank in createRawObject()");
        }
        
        Method newInstanceMethod;
        try {
            newInstanceMethod = Array.class.getMethod("newInstance", Class.class, int[].class );
            int[] intDimensions = ArrayUtils.asIntArray(dimensions);
            Object object = newInstanceMethod.invoke(null, getJavaType(type), intDimensions);
            return object;
        } catch (Exception ex) {
        // } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(Data.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }            
        return null;
    }
    
    public Data linearTransform(double mul, double add) throws WrongDimensionsException, UnknownDataTypeException {
        Object array = ArrayUtils.linearTransform(storage, mul, add);
        return new Data(array, DOUBLE, getDimensions());
    }
    
    /**
     * Reshapes 1-D array to a multidimensional array of correct dimensions.
     * 
     * Motivation: HDF5 library returns only 1-D arrays.
     * 
     * In C order.
     */
    public static Data from1DArray(Object array, int type, long[] dimensions) throws WrongDimensionsException {
        if (dimensions.length == 1) {
            return new Data(array, type, dimensions);
        } else if (dimensions.length == 2) {
            Object rawData = createRawObject(type, dimensions);
            int offset = 0;
            
            for (int i = 0; i < dimensions[0]; i++) {
                for (int j = 0; j < dimensions[1]; j++) {
                    
                    Object iArray = Array.get(rawData, i);
                    Object value = Array.get(array, offset);
                    
                    Array.set(iArray, j, value);
                    offset++;
                }
            }
            return new Data(rawData, type, dimensions);
        } else if (dimensions.length == 3) {
            Object rawData = createRawObject(type, dimensions);
            
            int offset = 0;
            
            for (int i = 0; i < dimensions[0]; i++) {
                for (int j = 0; j < dimensions[1]; j++) { 
                    Object iArray = Array.get(rawData, i);
                    
                    for (int k = 0; k < dimensions[2]; k++) {
                        
                        Object ijArray = Array.get(iArray, j);
                        Object value = Array.get(array, offset);
                        Array.set(ijArray, k, value);
                        offset++;
                    }
                }
            }
            return new Data(rawData, type, dimensions);
        } else {
            throw new WrongDimensionsException("Invalid rank in from1DArray()");
        }
    }
    
    /**
     * Get the type of stored array (in terms of constants defined in this class).
     * 
     * Our data types differ from HDF5 so that this class is not dependent
     * on HDF5 library. Hdf5Utils class contains conversion routines.
     */
    public int getType() {
        return type;
    }
    
    private static Class getJavaType(int type) {
        switch (type) {
            case INT8:
                return byte.class;
            case INT16:
                return short.class;
            case INT32:
                return int.class;
            case INT64:
                return long.class;
            case FLOAT: 
                return float.class;
            case DOUBLE:
                return double.class;
        }
        return null;
    }
    
    /**
     * Get length of the data along all dimensions.
     */
    public long[] getDimensions() {
        return Arrays.copyOf(dimensions, dimensions.length);
    }
    
    /**
     * Get the number of dimensions.
     * 
     * i.e. double[] =&gt; 1, short[][] =&gt; 2 etc.
     */
    public int getRank() {
        return dimensions.length;
    }
    
    /**
     * Get the original array used for the initialization of this object.
     * 
     * No type information, you have to cast it yourself.
     */
    public Object getRawData() {
        return storage;
    }
    
    /**
     * Get a 1-D double array representation of data.
     * 
     * Works for 1-D data. For doubles, it simply returns, for others, it converts them using ArrayUtils.asDoubleArray() (see).
     * 
     * @throws WrongDimensionsException if data are not 1-D.
     * @throws UnknownDataTypeException if conversion to double is not supported by underlying procedure.
     */
    public double[] asDoubleArray1D() throws WrongDimensionsException, UnknownDataTypeException {
        if (getRank() > 1) {
            throw new WrongDimensionsException("Only 1-D arrays are supported in asDoubleArray1D()");
        }
        if (type == DOUBLE) {
            return (double[])storage;
        }
        try {
            Method asDoubleArrayMethod = ArrayUtils.class.getMethod("asDoubleArray", storage.getClass());
            return (double[]) asDoubleArrayMethod.invoke(null, storage);
        } catch (Exception ex) {
            throw new UnknownDataTypeException("asDoubleArray not implemented for " + storage.getClass().getName() + " in ArrayUtils");
        }
    }
    
    /**
     * Remove dimensions that have length 1.
     * 
     * Preserves 1D arrays. Makes changes only if there is a trivial dimension.
     * 
     * @return true if there was a change, false otherwise.
     * 
     * Motivation: Our HDF5 file sometimes have Nx1 arrays instead of N arrays.
     */
    public boolean flatten() throws WrongDimensionsException
    {
        if (getRank() == 1) {
            // Do nothing. We already are linear;
            return false;
        }
        if (getRank() == 2) {
            if (dimensions[1] == 1) {
                long newDimensions[] = new long[] { dimensions[0] };
                Object newStorage = Data.createRawObject(type, newDimensions);
                for (int i = 0; i < dimensions[0]; i++) {
                    Object value = Array.get(Array.get(storage, i), 0);
                    Array.set(newStorage, i, value);
                }
                storage = newStorage;
                dimensions = newDimensions;
                return true;
            } else if (dimensions[0] == 1) {
                long newDimensions[] = new long[] { dimensions[1] };
                Object newStorage = Data.createRawObject(type, newDimensions);
                for (int i = 0; i < dimensions[1]; i++) {
                    Object value = Array.get(Array.get(storage, 0), i);
                    Array.set(newStorage, i, value);
                }
                storage = newStorage;
                dimensions = newDimensions;
                return true;
            }
        }
        return false;
    }
}
