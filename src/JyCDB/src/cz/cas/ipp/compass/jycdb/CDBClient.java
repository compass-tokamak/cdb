package cz.cas.ipp.compass.jycdb;

import cz.cas.ipp.compass.jycdb.util.DictionaryAdapter;
import cz.cas.ipp.compass.jycdb.util.ParameterList;
import cz.cas.ipp.compass.jycdb.util.PythonAdapter;
import cz.cas.ipp.compass.jycdb.util.PythonUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import org.python.core.Py;
import org.python.core.PyException;
import org.python.core.PyObject;
import org.python.util.PythonInterpreter;

/**
 * Jython-based adapter of python CDBClient.
 * 
 * Threading: Each thread should acquire an instance of this class using getInstance().
 * Multiple calls in one thread result in returning the same object.
 * There is no need of closing the connections, it happens automatically
 * in garbage collector (this is forced when we run out of connections).
 * However, you should not pass reference to one instance among different threads
 * (if you cannot assure the threads will not use it concurrently).
 * 
 * The methods map to Client/DAQClient python methods as closely as possible.
 * Several methods have overloaded versions:
 *   1) a totally generic one that accepts ParameterList (a "dictionary" of values)
 *   2,...) more specific versions with frequently used sets of parameters.
 * 
 * See pyCDB documentation.
 * 
 * @author pipek
 */
public class CDBClient extends PythonAdapter {    
    private static PythonInterpreter python = null;
    
    private synchronized static void loadPython() throws CDBException {
        if (python == null) {
            try {
                python = PythonUtils.getInterpreter();
                python.exec("from pyCDB import DAQClient");
            } catch (PyException exc) {
                System.err.println("---------------------");
                System.err.println("Could not load pyCDB!");
                System.err.println("Error: " + exc.toString());
                System.err.println("Please check the JYTHONPATH environment variable.");
                System.err.println("Current value: " + System.getenv("JYTHONPATH"));
                System.err.println("---------------------");
                throw new CDBException("Cannot load PyCDB");
            }
        }
    }
    
    private CDBClient() throws CDBException {
        super(null); // Will be substituted by the end of the constructor
        loadPython(); // Load python just before first CDBclient
        python.exec("cl = DAQClient.CDBDAQClient()");
        pyObject = python.get("cl");
    }
    
    private CDBClient(ParameterList parameters) throws CDBException {
        super(null);
        loadPython();
        PythonAdapter pythonClass = new PythonAdapter(python.eval("DAQClient.CDBDAQClient"));
        pyObject = pythonClass.invoke("__call__", parameters);
    }
    
    
    /**
     * Even if finalize is not always called, try to close the connection.
     */
    @Override
    public void finalize() throws Throwable {
        try {
            close();
        } catch (Exception e) { }
        super.finalize();
    }
    
    // Weak hash map is used so that threads can be finalized and removed even when serving as keys.
    private static WeakHashMap<Thread, CDBClient> instances = new WeakHashMap<Thread, CDBClient>();
      
    /**
     * Get CDB client specific for current thread.
     * 
     * It can serve a few hundred connections before problem arises.
     * However, at that moment, there is a slight pause during which we
     * try to close all unused connections (by hinting garbage connection)
     * and obtain the connection for the second time. Only after this
     * it eventually fails.
     * 
     * Note: you should not pass CDBClient instances among threads or
     * undefined behaviour results. But it is possible if you know
     * what you are doing (e.g. in FS database controller).
     * 
     * @return null if not connected.
     */
    public synchronized static CDBClient getInstance() throws CDBException {        
        Thread thread = Thread.currentThread();
        if (!instances.containsKey(thread)) {
            CDBClient client = null;
            try {
                client = new CDBClient();        
            } catch (CDBException cdbEx) {
                throw cdbEx;
            } catch (Exception e) {
                // Call garbage collector and give it some time to get rid of unused connections.
                System.gc();
                try { Thread.sleep(1000); } catch (Exception ex) { }
                try { client = new CDBClient(); } catch (Exception exc) {
                    exc.printStackTrace();
                    throw new CDBConnectionException("Cannot connect to CDB.");
                }
            }
            instances.put(thread, client);
        }
        return instances.get(thread);
    }
    
    /**
     * Get CDB Client with specific construction parameters.
     * 
     * @param parameters (host, user, passwd, db, port, log_level, data_root). If any 
     *    of the parameters is not specified, default value (from env. variables, etc.)
     *    is taken.
     *     
     * Warning: This version of constructor is not recommended for general use.
     * It doesn't use any clever method of preserving connections, memory, thread safety etc.
     */
    public synchronized static CDBClient getInstance(ParameterList parameters) throws CDBException {
        return new CDBClient(parameters);
    }
    
    public long lastRecordNumber(String recordType) {
        return pyObject.invoke("last_record_number", Py.newString(recordType)).asLong();
    }
    
    /**
     * Last shot (or experimental record) number.
     */
    public long lastShotNumber() {
        return pyObject.invoke("last_shot_number").asLong();
    }  
    
    /**
     * Check whether a record exists.
     */
    public boolean recordExists(long recordNumber) {
        ParameterList parameters = new ParameterList();
        parameters.put("record_number", recordNumber);
        return invoke("record_exists", parameters).asInt() > 0;
    }
    
    /**
     * Get current generic signal attached to node, board &amp; parameter.
     * 
     * @return null if not found.
     * 
     * Use Firesignal names.
     */
    public GenericSignal getFSSignal(String nodeId, String hardwareId, String parameterId) {
        PyObject[] parameters = new PyObject[3];
        parameters[0] = Py.newString(nodeId);
        parameters[1] = Py.newString(hardwareId);
        parameters[2] = Py.newString(parameterId);
        PyObject signalObject = pyObject.invoke("get_FS_signal_reference", parameters);
        return GenericSignal.create(signalObject);
    }
    
    public List<GenericSignal> getGenericSignals(ParameterList parameters) {
        Iterable<PyObject> objects = invoke("get_generic_signal_references", parameters).asIterable();
        ArrayList<GenericSignal> signals = new ArrayList<GenericSignal>();
        for (PyObject obj : objects) {
            signals.add(GenericSignal.create(obj));
        }
        return signals;
    }
    
    public List<GenericSignal> findGenericSignals(String aliasOrName, boolean useRegexp) {
        ParameterList parameters = new ParameterList();
        parameters.put("alias_or_name", aliasOrName);
        parameters.put("regexp", useRegexp);
        Iterable<PyObject> objects = invoke("find_generic_signals", parameters).asIterable();
        ArrayList<GenericSignal> signals = new ArrayList<GenericSignal>();
        for (PyObject obj : objects) {
            signals.add(GenericSignal.create(obj));
        }
        return signals;        
    }
    
    public GenericSignal getGenericSignal(long genericSignalId) {
        ParameterList parameters = new ParameterList();
        parameters.put("generic_signal_id", genericSignalId);
        List<GenericSignal> signals = getGenericSignals(parameters);
        if (signals.isEmpty()) {
            return null;
        } else {
            return signals.get(0);
        }
    }
    
    public GenericSignal getGenericSignal(String strid) throws CDBException {
        List<GenericSignal> signals = getGenericSignals(strid);
        if (signals.isEmpty()) {
            return null;
        } else if (signals.size() > 1 ) {
            throw new CDBException("Generic signal str_id is ambiguous. More than one generic signals returned.");
        } else {
            return signals.get(0);
        }        
    }
    
    public List<GenericSignal> getGenericSignals(String strid) {
        ParameterList parameters = new ParameterList();
        parameters.put("str_id", strid);
        return getGenericSignals(parameters);
    }
    
    public SignalCalibration getSignalCalibration(String strId) {
        ParameterList parameters = new ParameterList();
        parameters.put("str_id", strId);
        return getSignalCalibration(parameters);
    }
    
    public SignalCalibration getSignalCalibration(ParameterList parameters) {
        PyObject obj = invoke("get_signal_calibration", parameters);
        return SignalCalibration.create(obj);
    }
    
    public SignalParameters getSignalParameters(ParameterList parameters) {
        PyObject obj = invoke("get_signal_parameters", parameters);
        return SignalParameters.create(obj);
    }
    
    public ChannelAttachment getAttachment(long genericSignalId) {
        ParameterList parameters = new ParameterList();
        parameters.put("generic_signal_id", genericSignalId);
        List<ChannelAttachment> attachments = getAttachmentTable(parameters);
        if (attachments.isEmpty()) {
            return null;
        } else {
            return attachments.get(0);
        }
    }
    
    public List<ChannelAttachment> getAttachmentTable(ParameterList parameters) {
        Iterable<PyObject> objects = invoke("get_attachment_table", parameters).asIterable();
        ArrayList<ChannelAttachment> attachments = new ArrayList<ChannelAttachment>();
        for (PyObject obj : objects) {
            attachments.add(ChannelAttachment.create(obj));
        }
        return attachments;
    }
    
    public ChannelAttachment getAttachmentTable(long computerId, long boardId, long channelId) {
        ParameterList parameters = new ParameterList();
        parameters.put("computer_id", computerId);
        parameters.put("board_id", boardId);
        parameters.put("channel_id", channelId);
        
        List<ChannelAttachment> attachments = getAttachmentTable(parameters);
        if (attachments.isEmpty()) {
            return null;
        } else {
            return attachments.get(0);
        }
    }
    
    public DataFile newDataFile(ParameterList parameters) {
        PyObject object = invoke("new_data_file", parameters);
        return DataFile.create(object); 
    }
    
    public DataFile newDataFile(String collectionName, long recordNumber, long dataSourceId) {
        ParameterList parameters = new ParameterList();
        parameters.put("collection_name", collectionName);
        parameters.put("file_format", "HDF5");
        parameters.put("data_source_id", dataSourceId);
        parameters.put("record_number", recordNumber);
        return newDataFile(parameters);
    }
    
    public SignalReference storeSignal(ParameterList parameters) {
        PyObject object = invoke("store_signal", parameters);
        return SignalReference.create(object);
    }
    
    /**
     * Update signal.
     *
     * @param parameters As few parameters as needed.
     */
    public void updateSignal(ParameterList parameters) {
        invoke("update_signal", parameters);
    }
    
    public void deleteSignal(ParameterList parameters) {
        invoke("delete_signal", parameters);
    }
    
    /**
     * Store signal of type FILE.
     * 
     * Underlying Python method ensures that computer, board and channel ids are correctly set.
     */
    public SignalReference storeSignal(long genericSignalId, long recordNumber, String dataFileKey, long dataFileId, double time0, double coefficient, double offset, double coefficient_V2unit) {   
        // Check if the data file is valid
        ParameterList parameters = new ParameterList();
        parameters.put("generic_signal_id", genericSignalId);
        parameters.put("record_number", recordNumber);
        parameters.put("offset", offset);
        parameters.put("time0", time0);
        parameters.put("coefficient", coefficient);
        parameters.put("coefficient_V2unit", coefficient_V2unit);
        parameters.put("data_file_id", dataFileId);
        parameters.put("data_file_key", dataFileKey);
        return storeSignal(parameters);
    }     
    
    /**
     * Store signal of type FILE.
     * 
     * Underlying Python method ensures that computer, board and channel ids are correctly set.
     */
    public SignalReference storeSignal(long genericSignalId, long recordNumber, String dataFileKey, long dataFileId, double time0) {   
        // Check if the data file is valid
        ParameterList parameters = new ParameterList();
        parameters.put("generic_signal_id", genericSignalId);
        parameters.put("collection_name", dataFileKey);
        parameters.put("record_number", recordNumber);
        parameters.put("time0", time0);
        parameters.put("data_file_id", dataFileId);
        parameters.put("data_file_key", dataFileKey);
        return storeSignal(parameters);
    }      
    
    /**
     * Store signal of type LINEAR.
     */
    public SignalReference storeLinearSignal(long genericSignalId, long recordNumber, double time0, double coefficient, double offset) {        
        // Check if the signal is linear
        ParameterList parameters = new ParameterList();
        parameters.put("generic_signal_id", genericSignalId);
        parameters.put("record_number", recordNumber);
        parameters.put("time0", time0);
        parameters.put("coefficient", coefficient); 
        parameters.put("offset", offset); 
        return storeSignal(parameters);
    }
   
    public long createRecord(Map<String,PyObject> parameters) {
        return invoke("create_record", parameters).asLong();
    }
    
    public long createRecord(String recordType) {
        ParameterList parameters = new ParameterList();
        parameters.put("record_type", recordType);
        return createRecord(parameters);
    }
    
    /**
     * Creates new FireSignal record with forced record number equivalent to fsEventNumber.
     * 
     * @param fsEventID event identification "0x0000" -
     * @param fsEventNumber firesignal shot number
     */
    public long createRecord(long fsEventNumber, String fsEventID) {
        return createRecord(fsEventNumber, fsEventID, "EXP");
    }
    
    public long createRecord(long fsEventNumber, String fsEventID, String recordType)
    {
        ParameterList parameters = new ParameterList();
        parameters.put("record_type", recordType);
        parameters.put("FS_event_number", fsEventNumber);
        parameters.put("record_number", fsEventNumber); // FS takes precedence!
        parameters.put("FS_event_id", fsEventID);
        return createRecord(parameters);       
    }
   
    // DEPRECATED?
    public long getRecordNumberFromFSEvent(long eventNumber, String eventId) {
        ParameterList parameters = new ParameterList();
        parameters.put("event_number", eventNumber);
        parameters.put("event_id", eventId);
        return invoke("get_record_number_from_FS_event", parameters).asLong();
    }
    
    public void setFileReady(long fileId) {
        pyObject.invoke("set_file_ready", Py.newLong(fileId));
    }
    
    public List<SignalReference> getSignalReferences(ParameterList parameters) {
        Iterable<PyObject> objects = invoke("get_signal_references", parameters).asIterable();
        ArrayList<SignalReference> signals = new ArrayList<SignalReference>();
        for (PyObject obj : objects) {
            signals.add(SignalReference.create(obj));
        }
        return signals;    
    }    
    
    public SignalReference getSignalReference(long recordNumber, long genericSignalId, long revision) {
        ParameterList parameters = new ParameterList();
        parameters.put("record_number", recordNumber);
        parameters.put("generic_signal_id", genericSignalId);
        parameters.put("revision", revision);
        List<SignalReference> signals = getSignalReferences(parameters);
        if (signals.isEmpty()) {
            return null;
        } else {
            return signals.get(0);
        }
    }    
    
    public SignalReference getSignalReference(long recordNumber, long genericSignalId) {
        ParameterList parameters = new ParameterList();
        parameters.put("record_number", recordNumber);
        parameters.put("generic_signal_id", genericSignalId);
        List<SignalReference> signals = getSignalReferences(parameters);
        if (signals.isEmpty()) {
            return null;
        } else {
            return signals.get(0);
        }
    }
    
    public SignalReference getSignalReference(String strid) throws CDBException {
        ParameterList parameters = new ParameterList();
        parameters.put("str_id", strid);
        List<SignalReference> signals = getSignalReferences(parameters);
        if (signals.isEmpty()) {
            return null;
        } else if (signals.size() > 1) {
            throw new CDBException("Generic signal str_id is ambiguous. More than one generic signals returned.");
        } else {
            return signals.get(0);
        }       
    }
    
    public Signal getSignal(String strid, String variant) throws CDBException {
        ParameterList parameters = new ParameterList();
        parameters.put("str_id", strid);
        parameters.put("variant", variant);
        return getSignal(parameters);
    }
    
    public Signal getSignal(String strid) throws CDBException {
        return getSignal(strid,"");
    }
    
    public Signal getSignal(long recordNumber, long genericSignalId) throws CDBException {
        ParameterList parameters = new ParameterList();
        parameters.put("record_number", recordNumber);
        parameters.put("generic_signal_id", genericSignalId);
        return getSignal(parameters);
    }
    
    public Signal getSignal(ParameterList parameters) throws CDBException {
        String units = "default";
        String variant = "";
        if (parameters.containsKey("units")) {
            units = parameters.get("units").asString();
        } else if (parameters.containsKey("str_id")) {
            python.exec("from pyCDB import client");
            PyObject module = python.get("client");
            ParameterList unitParameters = new ParameterList();
            unitParameters.put("str_id", parameters.get("str_id"));
            PyObject response = PythonUtils.invoke(module, "decode_signal_strid", unitParameters);
            DictionaryAdapter da = new DictionaryAdapter(response);
            units = da.getDictValueAsString("units");
        }
        
        List<SignalReference> refs = getSignalReferences(parameters);
        if (refs.size() > 1) {
            throw new CDBException("Ambiguous signal references.");
        } else if (refs.isEmpty()) {
            return null;
        }
         
        return new Signal(refs.get(0), units);
    }
    
    public DataFile getDataFile(ParameterList parameters) {
        PyObject obj = invoke("get_data_file_reference", parameters);
        return DataFile.create(obj);
    }
    
    public DataFile getDataFile(long dataFileId) {
        ParameterList parameters = new ParameterList();
        parameters.put("data_file_id", dataFileId);
        return getDataFile(parameters);
    }
    
    /**
     * Return the CDB DAQ_channel reference of a FireSignal channel
     * @param nodeUniqueId
     * @param hardwareUniqueId
     * @param parameterUniqueId
     * @return 
     */
    public void FS2CDB_ref(String nodeUniqueId, String hardwareUniqueId, String parameterUniqueId) {
        ParameterList parameters = new ParameterList();
        parameters.put("nodeuniqueid", nodeUniqueId);
        parameters.put("hardwareuniqueid", hardwareUniqueId);
        parameters.put("parameteruniqueid", parameterUniqueId);
        invoke("FS2CDB_ref", parameters);
    }
    
    public Long getComputerId(String computerName, String description) {
        ParameterList parameters = new ParameterList();
        parameters.put("computer_name", computerName);
        parameters.put("description", description);
        return invoke("get_computer_id", parameters).asLong();
    }
    
    public Long getComputerId(String computerName) {
        ParameterList parameters = new ParameterList();
        parameters.put("computer_name", computerName);
        return invoke("get_computer_id", parameters).asLong();
    }
    
    public Long getDataSourceId(String dataSourceName) {
        ParameterList parameters = new ParameterList();
        parameters.put("data_source_name", dataSourceName);
        return invoke("get_data_source_id", parameters).asLong();
    }
    
    public Long insert(String tableName, ParameterList fields, Boolean returnInsertedId, Boolean checkStructure) {
        ParameterList parameters = new ParameterList();
        parameters.put("table_name", tableName);
        parameters.put("fields", fields);
        parameters.put("return_inserted_id", returnInsertedId);
        parameters.put("check_structure", checkStructure);
        return invoke("insert",parameters).asLong();
    }
    
    public Long insert(String tableName, ParameterList fields, Boolean returnInsertedId) {
        ParameterList parameters = new ParameterList();
        parameters.put("table_name", tableName);
        parameters.put("fields", fields);
        parameters.put("return_inserted_id", returnInsertedId);
        return invoke("insert",parameters).asLong();
    }
    
    public Long insert(String tableName, ParameterList fields) {
        ParameterList parameters = new ParameterList();
        parameters.put("table_name", tableName);
        parameters.put("fields", fields);
        return invoke("insert",parameters).asLong();
    }

    public Long createComputer(String computerName, String description, String location) {
        ParameterList parameters = new ParameterList();
        parameters.put("computer_name", computerName);
        parameters.put("description", description);
        parameters.put("location", location);
        return invoke("create_computer",parameters).asLong();
    }
    
    public Long createComputer(String computerName, String description) {
        ParameterList parameters = new ParameterList();
        parameters.put("computer_name", computerName);
        parameters.put("description", description);
        return invoke("create_computer",parameters).asLong();
    }
    
    public Long createComputer(String computerName) {
        ParameterList parameters = new ParameterList();
        parameters.put("computer_name", computerName);
        return invoke("create_computer",parameters).asLong();
    }
    
    
    
    
    /**
     * Convert axis IDs to parameters and append to parameter parameters.
     * 
     * If time axis should be omitted, enter null;
     * If no axes at all, supply either null or new Long[0];
     * 
     * @param parameters
     * @param axisIds 
     */
    private void addAxesToParameterList(ParameterList parameters, Long[] axisIds) {
        if (axisIds == null) {
            return;
        }
        if (axisIds.length > 0) {
            if (axisIds[0] != null) {
                parameters.put("time_axis_id", axisIds[0]);
            }
            if (axisIds.length > 1) {
                parameters.put("axis1_id", axisIds[1]);
            }
            if (axisIds.length > 2) {
                parameters.put("axis2_id", axisIds[2]);
            }
            if (axisIds.length > 3) {
                parameters.put("axis3_id", axisIds[3]);
            }
        }
    }

    public void createDAQChannel(String nodeId, String hardwareId, String parameterId, long dataSourceId, Long[] axisIds) {
        ParameterList parameters = new ParameterList();
        parameters.put("nodeuniqueid", nodeId);
        parameters.put("hardwareuniqueid", hardwareId);
        parameters.put("parameteruniqueid", parameterId);
        parameters.put("data_source_id", dataSourceId);
        addAxesToParameterList(parameters, axisIds);
        createDAQChannel(parameters);
    }
    
    public void createDAQChannel(ParameterList parameters) {
        invoke("create_DAQ_channels", parameters);
    }

    public void createGenericSignal(String name, String alias, long dataSourceId, Long[] axisIds, String units, String signalType) throws WrongSignalTypeException {
        if (!GenericSignal.isValidType(signalType)) {
            throw new WrongSignalTypeException("Not valid signal type: " + signalType);
        }
        ParameterList parameters = new ParameterList();
        parameters.put("generic_signal_name", name);
        parameters.put("alias", alias);
        parameters.put("data_source_id", dataSourceId);
        addAxesToParameterList(parameters, axisIds);
        parameters.put("units", units);
        parameters.put("signal_type", signalType);
        createGenericSignal(parameters);
    }        
    
    public void createGenericSignal(ParameterList parameters) {
        invoke("create_generic_signal", parameters);
    }

    public Long createGenericSignalWhichReturnsLong(String name, String alias, long dataSourceId, Long[] axisIds, String units, String signalType) throws WrongSignalTypeException {
        if (!GenericSignal.isValidType(signalType)) {
            throw new WrongSignalTypeException("Not valid signal type: " + signalType);
        }
        ParameterList parameters = new ParameterList();
        parameters.put("generic_signal_name", name);
        parameters.put("alias", alias);
        parameters.put("data_source_id", dataSourceId);
        addAxesToParameterList(parameters, axisIds);
        parameters.put("units", units);
        parameters.put("signal_type", signalType);
        return createGenericSignalWhichReturnsLong(parameters);
    } 
    
    public Long createGenericSignalWhichReturnsLong(ParameterList parameters) {
        return invoke("create_generic_signal", parameters).asLong();
    }

    public boolean checkConnection() {
        return invoke("check_connection").asInt() > 0;
    }
    
    /**
     * Close the connection.
     */
    private void close() {
        pyObject.invoke("close");
    }
}
