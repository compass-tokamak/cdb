package cz.cas.ipp.compass.jycdb;

import com.google.gson.Gson;
import cz.cas.ipp.compass.jycdb.util.Data;
import cz.cas.ipp.compass.jycdb.util.Hdf5Utils;
import cz.cas.ipp.compass.jycdb.util.Hdf5WriteException;
import cz.cas.ipp.compass.jycdb.util.ParameterList;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.LinkedHashMap;

/**
 * Methods for FireSignal nodes.
 * 
 * It is mostly a facade over CDBClient methods to simplify development
 * of nodes that communicate directly with CDB.
 * 
 * How to write a signal?
 * 
 * 1) For each record, create an instance of FSNodeWriter (you have to know generic signal id as well).
 * 2) Create a file using createDataFile(). (see variants of this method)
 * 3) Write data (as many times as you want).
 *    a] Using writeData().
 *       Please, notice that you have to be aware of the data offset in HDF5 (or write them in one step).
 *    b] If you have an existing HDF5 file, use copyHdf5().
 * 4) Tell DB that the file is ready using setFileReady(). (if you changed it)
 * 5) Write all axes using writeAxis(index). index=0 for time axis, index=1,2,3,... for other axes.
 *    (can be called multiple times for the same axis).
 * 6) Write signal using writeSignal().
 * 
 * @author pipek
 */
public class FSNodeWriter {  
    private boolean signalExists(long genericSignalId, long recordNumber) throws CDBException {
        SignalReference signal = client.getSignalReference(recordNumber, genericSignalId);
        return (signal != null);
    }
    
    private CDBClient client = null;
    
    private GenericSignal genericSignal = null;
    
    private long recordNumber = 0;
    
    private String collectionName = null;
    
    private String fileKey = null;
    
    private DataFile dataFile = null;
    
    private boolean fileReady = false;
    
    private boolean requireChannelAttachment = true;
    
    private final LinkedHashMap<String, Object> gsParameters = new LinkedHashMap<String, Object>();
    
    private final LinkedHashMap<String, Object> daqParameters = new LinkedHashMap<String, Object>();
    
    Gson gson = new Gson();
    
    /**
     * @param collectionName Name of the file without prefixes.
     * @param fileKey Name of the dataset in HDF5 file.
     * 
     * @throws cz.cas.ipp.compass.jycdb.CDBException
     */
    public FSNodeWriter(long genericSignalId, long recordNumber, String collectionName, String fileKey) throws CDBException {
        client = CDBClient.getInstance();
        genericSignal = client.getGenericSignal(genericSignalId);
        this.collectionName = collectionName;
        this.fileKey = fileKey;
        this.recordNumber = recordNumber;
    }
    
    public FSNodeWriter(long computerId, long boardId, long channelId, long recordNumber, String collectionName, String fileKey) throws CDBException {
        this(
            CDBClient.getInstance().getAttachmentTable(computerId, boardId, channelId).getAttachedGenericSignalId(),
            recordNumber, collectionName, fileKey
        );
    }
    
    /**
     * Set whether writing should succeed (assuming value 1.0 for coefficients) when channel attachment not found.
     * 
     * It has to be specified explicitely because it is quite probable that non-existence marks a bug.
     */
    public void setRequireChannelAttachment(boolean require)
    {
        requireChannelAttachment = require;
    }
    
    /**
     * Create new data file (row in `data_files`).
     * 
     * @throws CDBException 
     * 
     * In this default version, an already existing file with requested 
     * properties is taken as error.
     */
    public void createDataFile() throws CDBException {
        createDataFile(false, false);
    }

    /**
     * Create new data file (row in `data_files`).
     * 
     * @return true if a file was created, false if nothing happens.
     * @throws CDBException 
     * 
     * @param okIfExists If false, an exception is thrown if a file
     *    with the same collection name, data source and record number exists
     * @param createIfExists If true and a file already exists, create a new one
     *    (otherwise the existing one is returned).
     */
    public boolean createDataFile(boolean okIfExists, boolean createIfExists) throws CDBException {
        if (signalExists(genericSignal.getId(), recordNumber)) {
            throw new CDBException("Signal already exists.");
        }
        ParameterList params = new ParameterList();
        params.put("collection_name", collectionName);
        params.put("record_number", recordNumber);
        params.put("data_source_id", genericSignal.getDataSourceId());
        DataFile read = client.getDataFile(params);
        if (read != null) {
            if (!okIfExists) {
                throw new CDBException("Data file already exists.");
            }
            if (!createIfExists) {
                dataFile = read;
                return false;
            }
        }
        dataFile = client.newDataFile(collectionName, recordNumber, genericSignal.getDataSourceId());        
        return true;
    }
    
    /**
     * Copy an existing HDF5 file to the correct destination.
     * 
     * @param path Local path of the HDF5 file.
     * @throws IOException 
     * 
     * If the destination exist, copying is prevented and exception thrown.
     */
    public void copyHdf5(String path) throws IOException
    {
        String destPath = dataFile.getFullPath();
        File f = new File(destPath);
        if(f.exists()) {
            throw new IOException("Cannot overwrite file by copyHdf5.");
        }
        FileChannel sourceChannel = null;
        FileChannel destChannel = null;
        try {
            sourceChannel = new FileInputStream(path).getChannel();
            destChannel = new FileOutputStream(destPath).getChannel();
            destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
        } finally {
            if (sourceChannel != null) sourceChannel.close();
            if (destChannel != null) destChannel.close();
        }
    }
    
    /**
     * Write data (with possible offset to append).
     * 
     * @param data Data to be written.
     * @param dataOffset Zero-based index of the first data element. Size is calculated automatically.
     *    If data Offset is null, starts from beginning (fails with existing file and dataset).
     * 
     * @throws cz.cas.ipp.compass.jycdb.util.Hdf5WriteException
     */
    public void writeData(Data data, long[] dataOffset) throws Hdf5WriteException, CDBException {
        if (dataFile == null) {
            throw new CDBException("File not created. Cannot write data.");
        }
        if (fileReady) {
            throw new CDBException("File is already closed. Cannot write data.");
        }
        Hdf5Utils.writeData(dataFile.getFullPath(), fileKey, data, dataOffset);        
    }
    
    /**
     * Tell CDB that you have finished writing data.
     * 
     * After this, you cannot write more data.
     */
    public void setFileReady() {
        client.setFileReady(dataFile.getId());
        fileReady = true;
    }
    
    private ParameterList createWriteParameterList(double time0) {
        ParameterList parameters = new ParameterList();
        parameters.put("generic_signal_id", genericSignal.getId());
        parameters.put("collection_name", fileKey);
        parameters.put("record_number", recordNumber);
        parameters.put("time0", time0);
        parameters.put("data_file_id", dataFile.getId());
        parameters.put("data_file_key", fileKey);
        if (gsParameters.size() > 0) {
            parameters.put("gs_parameters", gson.toJson(gsParameters));
        }
        if (daqParameters.size() > 0) {
            parameters.put("daq_parameters", gson.toJson(daqParameters));
        }
        return parameters;
    }
    
    /**
     * Check whether conditions are met for writing the signal.
     */
    private void writeCheck() throws CDBException {
        if (signalExists(genericSignal.getId(), recordNumber)) {
            throw new CDBException("Signal already exists.");
        }          
        if (!fileReady) {
            throw new CDBException("File not ready. Cannot write signal yet.");
        }        
    }
    
    /**
     * Write signal info to CDB.
     * 
     * Call this after you have finished with writing data and axes.
     * Can be used only for generic signal with valid DAQ attachment.
     */
    public void writeSignal(double time0) throws CDBException {
        writeCheck();
        
        // Check for the existence of channel attachment
        ChannelAttachment channelAttachment = client.getAttachment(genericSignal.getId());
        if (channelAttachment == null && requireChannelAttachment) {
            throw new CDBException("DAQ channel not found but required.");
        }
        
        ParameterList parameters = createWriteParameterList(time0);
        client.storeSignal(parameters);
    }
    
    /**
     * Write signal info to CDB for signals that have no DAQ attachment.
     * 
     * Usage is similar to writeSignal.
     */
    public void writeNotAttachedSignal(double time0, double coefficient) throws CDBException {
        writeCheck();
        
        ParameterList parameters = createWriteParameterList(time0);
        parameters.put("coefficient", coefficient);
        parameters.put("offset", 0.0);
        parameters.put("coefficient_V2unit", 1);
        client.storeSignal(parameters);
    }
    
    /**
     * Write one of the axes.
     * 
     * @param axis Index of the axis (0=time, 1,2,3,...=other axes)
     */
    public void writeAxis(int axis, double coefficient, double offset) throws CDBException {
        long genericSignalId = genericSignal.getAxisId(axis);
        
        if (genericSignalId == 0)
        {
            throw new CDBException("Axis signal does not exist.");
        }
        
        if (signalExists(genericSignalId, recordNumber)) {
            // This is OK, axis can be shared among signals.
            return;
        }
        client.storeLinearSignal(genericSignalId, recordNumber, 0, coefficient, offset);
    }
    
    public DataFile getDataFile() {
        return dataFile;
    }
    
    public String getFileKey() {
        return fileKey;
    }
    
    public long getRecordNumber() {
        return recordNumber;
    }
   
    public void addDaqParameter(String key, Object value) {
        daqParameters.put(key, value);
    }
    
    public void addGenericSignalParameter(String key, Object value) {
        gsParameters.put(key, value);
    }
    
    public GenericSignal getGenericSignal() {
        return genericSignal;
    }
}
