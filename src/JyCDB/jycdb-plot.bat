@echo off
call %CDB_PATH%\JyCDB\setup-env.bat
java -cp %CLASSPATH% -Dpython.cachedir=.jython-cache -Djava.library.path=lib cz.cas.ipp.compass.jycdb.plotting.DataPlotter %*
