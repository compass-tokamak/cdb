function [data] = jycdbread(strid)

try
    % Try to find already existing instance
    client = cz.cas.ipp.compass.jycdb.CDBClient.getInstance();

catch
    disp('Loading JyCDB libraries...')

    % Set up necessary paths
    if isunix
        % Ubuntu
        javaaddpath('/sw/jython/2.5.2/jython.jar');
        javaaddpath('/sw/CDB/latest/src/JyCDB/dist/JyCDB.jar');

    else
        % Windows
        javaaddpath('C:\jython2.5.2\jython.jar');
        javaaddpath('C:\jython2.5.2\compass_libs\pyCDB\src\JyCDB\dist\JyCDB.jar');
    end

    %
    try
        client = cz.cas.ipp.compass.jycdb.CDBClient.getInstance();
    catch
        error('CDB:JyCDBError', 'JyCDB library could not be initialized.')
    end
end

signal = client.getSignal(strid);
if isempty( signal )
    error('CDB:SignalError', ['Signal not found: ' strid ])
end

datafile = signal.getDataFile();
hdf5path = char(datafile.getFullPath())

try
    data.data = single(hdf5read(char(datafile.getFullPath()), char(datafile.getCollectionName())));
catch
    error('CDB:SignalError', ['HDF5 file `' hdf5path '` not found. Check that the path is correctly mounted.' ])
end

time0 = signal.getTime0();
samplelength = signal.getTimeAxis().getCoefficient();

tbegin = num2str( time0  / samplelength );

% string that evaluates to array of time axis
data.timestr = ['[' tbegin '+(1:' num2str(length(data.data)) ')]*' num2str( samplelength * 1e-3)];
