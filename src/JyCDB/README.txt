General description
-------------------
JyCDB is a wrapper around PyCDB (using Jython) that enables to use CDB from Java.
Apart from that, it offers a few utility functions to manipulate python, arrays and HDF5 data.

Installation instructions
-------------------------
Tested with: Jython 2.5.2

Required python packages for jython:
- ordereddict.py (copy to Lib)
- decorator.py (copy to Lib)
- pymysql

Required jars (included in distribution):
- hdf5 jars if you want to read data (otherwise not)
- jcommon & jfreechart (for graphing)

Required .so libraries:
- libjhdf5.so in the libs (jars are only wrappers around it)

Development instructions
------------------------
For building, you need ant, version >= 1.8 (http://ant.apache.org/)
In this directory, just type `ant` and everything should automatically build.

Testing instructions
--------------------
Run (after building):
./test.sh (or "ant test; ./test.sh")

You should not commit without successful tests.


Example usage
-------------
CDBClient client = CDBClient.getInstance();
long recordNumber = client.lastShotNumber(); // By the time nodes want to write data, the record number is already set.

In case of question, ask the main author of JyCDB package, Jan Pipek.