#!/usr/bin/env python

# Add colours to test output.
# Adopted from code by (c) 2011 Eric Evans <eevans@sym-link.com>
# (c) 2013 Jan Pipek

import re, sys

FAILURE_RE = re.compile('(.*Exception:|.*expected.*but|.*Failures:|.*failure|.*FAILURES|.*FAILED|ERROR).*')
OK_RE = re.compile('OK.*')
INFO_RE = re.compile('Time:.*')
LINE_RE = re.compile('\d+\) .*')

RED = "\033[1;31m%s\033[0m"
GREEN = "\033[1;32m%s\033[0m"
YELLOW = "\033[1;33m%s\033[0m"
BLUE = "\033[1;34m%s\033[0m"

def colorize(line):
    match = OK_RE.match(line)
    if match:
        sys.stdout.write(GREEN % line)      
        return True

    match = LINE_RE.match(line)
    if match:
        sys.stdout.write(YELLOW % line)      
        return True        

    match = FAILURE_RE.match(line)
    if match:
        sys.stdout.write(RED % line)      
        return True     

    match = INFO_RE.match(line)
    if match:
        sys.stdout.write(BLUE % line)      
        return True             

    return False

if __name__ == '__main__':
    line = sys.stdin.readline()
    while(line):
        if not colorize(line):
            sys.stdout.write(line)
        line = sys.stdin.readline()
        sys.stdout.flush()
