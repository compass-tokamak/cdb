;Working example of how to get data from CDB using JyCDB and HDF5
;by Jan Pipek (& Matej Peterka)
function cdb_get_client
  oJava = OBJ_NEW("IDLjavaObject$Static$CZ_CAS_IPP_COMPASS_JYCDB_CDBCLIENT", "cz.cas.ipp.compass.jycdb.CDBClient")
  cdb = oJava->getInstance()
  return, cdb
  end

function cdb_get_signal, str_id
  cdb = cdb_get_client()
  signal = cdb->getSignal(str_id)
  return, signal
  end


function cdb_get_signal_data, str_id
  signal = cdb_get_signal(str_id)
  df = signal->getDataFile()
  file_name = df->getFullPath()
  file_id = H5F_OPEN(file_name)
  dataset_id = H5D_OPEN(file_id, df->getCollectionName())
  data = H5D_Read(dataset_id)
  H5D_CLOSE, dataset_id
  H5F_CLOSE, file_id
  return, data
  end

; data = cdb_get_signal_data( 'FS:MARTE_NODE/PositionDataCollection2/position_R1:3940' )
data = cdb_get_signal_data( 'FS:PCIE_ATCA_ADC_01/BOARD_9/CHANNEL_013:3969' ) * 4.9e-9
plot, data
end