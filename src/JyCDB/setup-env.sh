if [ -z "$CDB_PATH" ]; then
    echo "ERROR: You have to set CDB_PATH environmental variable!"
    return -1;
else
    echo "Configuring CDB paths..."
    echo "Using CDB_PATH=${CDB_PATH}"
fi

export JYCDB_LIB_PATH="${CDB_PATH}/JyCDB/lib"
export CLASSPATH="${CLASSPATH}:${CDB_PATH}/JyCDB/dist/JyCDB.jar"
export CLASSPATH="${CLASSPATH}:${JYCDB_LIB_PATH}/jcommon-1.0.17.jar"
export CLASSPATH="${CLASSPATH}:${JYCDB_LIB_PATH}/jfreechart-1.0.14.jar"
export CLASSPATH="${CLASSPATH}:${JYCDB_LIB_PATH}/jhdf5.jar"
export CLASSPATH="${CLASSPATH}:${JYCDB_LIB_PATH}/jhdfobj.jar"
export CLASSPATH="${CLASSPATH}:${JYCDB_LIB_PATH}/jhdf5obj.jar"

echo "CLASSPATH sucessfully set."
