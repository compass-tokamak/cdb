#!python
#
# This script loads "channels.txt" file and creates DAQ channels as well as generic signals for channels found in it
#
# Warning: Data source is set to MARTe, each channel receives new time axis. Please use only for MARTe channels.
#
# Expected format (sample):
#    ...
#    MARTE_NODE.PositionDataCollection2.MFPS_Subs1
#    MARTE_NODE.PositionDataCollection2.IPR9
#    MARTE_NODE.PositionDataCollection2.IPR8
#    MARTE_NODE.PositionDataCollection2.Br_Subs2
#    ...
import pyCDB.DAQClient

client = pyCDB.DAQClient.CDBDAQClient()

for line in open("channels.txt"):
    data_source_id = client.get_data_source_id("MARTE")
    generic_signal_name = line.strip()
    generic_signal = client.get_generic_signal_references(generic_signal_name = generic_signal_name)
    if len(generic_signal) > 0:
        print "Generic signal %s already exists." % generic_signal_name
        print "Skipping..."
        continue
    (nodeuniqueid, boarduniqueid, channeluniqueid) = generic_signal_name.split(".")
    print "Adding %s : %s : %s..." % (nodeuniqueid, boarduniqueid, channeluniqueid)

    # Not really clever:
    time_axis_name = generic_signal_name + "_time_axis"
    client.create_generic_signal( generic_signal_name = time_axis_name, data_source_id = data_source_id, signal_type = "LINEAR", units = "ms")
    time_axis_id = client.get_generic_signal_id( generic_signal_name = time_axis_name)
    client.create_DAQ_channels(nodeuniqueid, boarduniqueid, channeluniqueid, data_source_id = data_source_id, time_axis_id = time_axis_id)
